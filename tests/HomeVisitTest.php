<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class HomeVisitTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testHome()
    {
    	$this->visit('/')
    		->see('SIGN IN');
    }

    // public function testAdminLogin(){
    // 	try {
    //        	$this->visit('/')
	   //            	->type('danamonmaster@email.com','email')
	   //            	->type('654321','password')
	   //            	->press('SIGN IN')
	   //            	->seePageIs('/home');
    //     } catch (\Exception $e) {
    //         $this->assertContains ("Received status code [403]",$e->getMessage());
    //     }
    // }

    public function testMasukFormUpdatePasswordPeserta()
    {
    	// $user = User::find('2'); // user as danamonmaster
       	// $this->actingAs($user)
       		$this->visit('/backend/report-user/')
              	->see('Update Password');
    }
}
