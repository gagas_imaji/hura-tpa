<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Sign in to start your session');
    }

    public function testVisitHome()
    {
        try {
            $this->visit('/home')
                ->see('PROJECT YANG SEDANG BERJALAN');
        } catch (\Exception $e) {
            $this->assertContains ("Received status code [403]",$e->getMessage());
        }
    }

    public function testAdminLogin(){

         $this->visit('/login')
              ->type('admin@email.com','email')
              ->type('654321','password')
              ->press('SIGN IN')
              ->seePageIs('/home');
    }
}
