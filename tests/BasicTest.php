<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BasicTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testLogin(){
    	$this->visit('/')
         ->type('danamonmaster@email.com', 'email')
         ->type('654321', 'password')
         ->press('SIGN IN')
         ->seePageIs('/home');
    }
}
