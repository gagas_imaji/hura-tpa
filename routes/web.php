<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//
Route::get('/phpinfo',function(){
		phpinfo();
	});
//Login with token
Route::get("user-token",'Auth\TokenController@login');
//
Route::get("/flush",'SurveyController@flush');

//Testing apache route
Route::get('/testing-link', 'TestingController@index');
Route::get('/testing-save-papi/{slug}/{section}', 'TestingController@saveToReportPapi');
Route::get('/testing-save-lai/{slug}/{section}', 'TestingController@saveToReport');
Route::get('/testing-load-section/{slug}', 'TestingController@load_by_section');
Route::get('/testing-load-tiki/{slug}/{section}', 'TestingController@getQuestion');

Route::get('/backend/raw-data', 'RawDataController@row_survey');
Route::get('/backend/raw-data/nilai/{slug}/{slug_project}', 'RawDataController@row_data_nilai');
Route::get('/backend/raw-data/nilai/{slug}/{slug_project}/{user_id}/{section_id}', 'RawDataController@row_data_jawaban');
Route::get('/backend/raw-data/jawaban/lai/{slug}/{slug_project}/{user_id}/{section_id}', 'RawDataController@row_jawaban_lai');
Route::get('/backend/raw-data/jawaban/tiki/{slug}/{slug_project}/{user_id}/{section_id}', 'RawDataController@row_jawaban_tiki');

//
Route::get('/dummy-report-lai/{corp_id}', 'CorporateController@createDummyReportLAI');
Route::get('/dummy-report-tiki/{corp_id}', 'CorporateController@createDummyReportTIKI');
Route::get('/dummy-report-wba/{corp_id}', 'CorporateController@createDummyReportWBA');

Route::post('error-report','ErrorController@saveError');

Route::get('/','Auth\LoginController@showLoginForm');
Route::get('/under-maintenance','HomeController@maintenance');
Route::get('/terms-and-conditions','HomeController@tnc');
Route::post('/user',array('as' => 'user.store','uses' => 'UserController@store'));
Route::post('/corporate',array('as' => 'corporate.store','uses' => 'CorporateController@store'));
Route::post('/import', 'QuestionController@import');

//project
Route::get('project', 'SurveyController@index_project');
Route::get('project/assign/{id}', 'SurveyController@assign_peserta');
Route::get('project/report-assign/{id}', 'SurveyController@report_assign_peserta');
Route::get('project/detail/{id}', 'SurveyController@show_project');
Route::get('project/edit/{id}', 'SurveyController@edit_project');
Route::get('project/hapus/{slug}', 'SurveyController@hapus_project');
Route::get('project/{slug}', 'SurveyController@detail_project_user');
Route::get('project/hapus-peserta/{id_project}/{id_user}', 'SurveyController@hapus_peserta_project');
Route::get('backend/history-project', 'BackendController@history_project');
Route::get('backend/history-project/{slug}', 'BackendController@history_survey_by_project');
Route::post('project-edit', 'SurveyController@do_edit_project');
Route::post('project-assign', 'SurveyController@do_assign');
Route::post('upload-user-assign', 'SurveyController@do_assign_upload');

//standar nilai project
Route::get('project/standar_nilai/{id}','StandarNilaiProjectController@set_standar_nilai');
Route::get('project/standar_nilai/{id}/edit','StandarNilaiProjectController@edit_standar_nilai');
Route::post('/standar_nilai/save','StandarNilaiProjectController@save_standar_nilai');
Route::post('/standar_nilai/update-save','StandarNilaiProjectController@update_standar_nilai');

Route::get('standar_nilai','StandarNilaiProjectController@index');
Route::get('standar_nilai/reset','StandarNilaiProjectController@revert_standar_nilai');
Route::post('/get-standar-nilai/{standar_nilai_id}', 'AjaxController@getNilaiStandar');

Route::get('standar_nilai/{id}/edit','AspekPsikographController@editStandarNilaiPsikograph');
Route::post('standar_nilai/save-standar-nilai','AspekPsikographController@saveStandarNilaiPsikograph');
Route::get('standar_nilai/tambah','AspekPsikographController@addStandarNilai');
Route::post('standar_nilai/create','AspekPsikographController@createStandarNilai');


//tools / survey route
Route::get('/survey/save-report/{slug}/{user_id}', 'SurveyController@saveToReport');
// Route::get('/survey/create', 'SurveyController@create');
Route::post('/survey/{slug}/{section}/save-temp', 'SurveyController@saveTemp');
Route::get('/survey/{slug}/intro', 'SurveyController@getInstruction');
Route::get('/survey/{slug}/{section}/instruction', 'SurveyController@getInstruction');
Route::get('/survey/{slug}/{section}/start', 'SurveyController@getQuestion');
Route::get('/history-survey', 'SurveyController@getHistorySurvey');
Route::post('/survey-add', 'SurveyController@addNewSurvey');
Route::post('/test-time-out', 'SurveyController@testTimeOut');
Route::post('/section-time-out', 'SurveyController@sectionTimeOut');
Route::get('/survey/delete-temp-section/{slug}', 'SurveyController@deleteTempSurvey');
Route::get('/survey/complete-section/{section}', 'SurveyController@completeSection');


//cache
Route::post('/survey/{slug}/{section}/set-result-cache', 'SurveyController@setResultCache');
Route::post('/survey/{slug}/{section}/set-result-cache-wpa', 'SurveyController@setResultCacheWPA');
Route::post('/survey/{slug}/{section}/set-result-cache-papi', 'SurveyController@setResultCachePapi');
Route::get('/survey/save-cache-to-db/{slug}/{section}', 'SurveyController@saveCacheToDataResult');
Route::get('/survey/save-cache-to-db-wpa/{slug}/{section}', 'SurveyController@saveCacheToDataResultWPA');
Route::get('/survey/save-cache-to-db-papi/{slug}/{section}', 'SurveyController@saveCacheToDataResultPapi');
Route::get('/start-next-section/{slug}/{new_section}', 'SurveyController@startNextSection');
Route::get('cek-sudah/{slug}/{section}', 'SurveyController@cekSudah');
Route::get('/go-finish/{slug}/{section}/{user_id}', 'SurveyController@finish');
Route::get('/clear-cache/{slug}/{section}', 'SurveyController@clearCacheResult');


//survey wpa route
Route::get('/load-soal-wpa/{slug}/{section}/{user_id}', 'AjaxController@loadSoalWPA');
Route::get('/survey/get-question-wpa/{slug}/{section}', 'SurveyController@getQuestionWPA');
Route::post('/survey/{slug}/{section}/next-statement-wpa', 'SurveyController@getNextQuestionWPA');
Route::get('/survey/save-report-wpa/{slug}/{section}/{user_id}', 'SurveyController@saveToReportWPA');
Route::post('/test-time-out-wpa', 'SurveyController@testTimeOutWPA');

//survey papi route
Route::get('/load-soal-wba/{slug}/{section}/{user_id}', 'AjaxController@loadSoalWBA');
Route::get('/survey/get-question-papi/{slug}/{section}', 'SurveyController@getQuestionPapi');
Route::get('/survey/save-report-papi/{slug}/{section}/{id_user}', 'SurveyController@saveToReportPapi');

//data table survey
Route::model('survey','\App\Survey');
Route::post('/survey/lists','SurveyController@lists');
Route::resource('/survey', 'SurveyController',['except' => ['store','edit']]);
Route::post('/survey/detail-update/{id}',array('as' => 'survey.detail-update','uses' => 'SurveyController@detailUpdate'));
Route::post('/survey',array('as' => 'survey.store','uses' => 'SurveyController@store'));
Route::get('/load-soal-tiki-lai/{slug}/{section}/{user_id}', 'AjaxController@loadSoalTikiLai');

//data table question by survey
Route::post('/question/create', 'QuestionController@index');
Route::post('/question/create-form', 'QuestionController@addQuestion');
Route::post('/question/{id}/lists','QuestionController@lists');
Route::resource('/question', 'QuestionController',['except' => ['store','edit']]);
Route::get('/question-by-survey/{id}','QuestionController@show');
Route::post('/question/detail-update/{id}',array('as' => 'question.detail-update','uses' => 'QuestionController@detailUpdate'));
Route::post('/question/answer-update/{id}',array('as' => 'question.answer-update','uses' => 'QuestionController@optionAnswerUpdate'));

//import
// Route::get('/import', 'QuestionController@import');
Route::post('/import-question','QuestionController@doImport');
Route::post('/import-question-to-section','QuestionController@doImportQuestionToSection');

//select chain divisi by corporate
Route::get('user/get-division/{id}', 'UserController@getDivision');

// Route::get('/corporate', 'CorporateController@getById');
Route::get('download/{filename}', function($filename)
{
    // Check if file exists in app/storage/file folder
    $file_path = public_path() .'/files/'. $filename;

    if (file_exists($file_path)){
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    }else{
        // Error
        return redirect()->back()->withErrors('Requested file does not exist on our server!');
    }
}) ->where('filename', '[A-Za-z0-9\-\_\.]+');

Route::group(['middleware' => 'guest'], function () {
	/*Route::get('/resend-activation','UserController@resendActivation');
	Route::post('/resend-activation',array('as' => 'user.resend-activation','uses' => 'UserController@postResendActivation'));
	Route::get('/activate-account/{token}','UserController@activateAccount');

	Route::get('/auth/{provider}', 'SocialLoginController@providerRedirect');
    Route::get('/auth/{provider}/callback', 'SocialLoginController@providerRedirectCallback');

	Route::get('/verify-purchase', 'AccountController@verifyPurchase');
	Route::post('/verify-purchase', 'AccountController@postVerifyPurchase');
	Route::resource('/install', 'AccountController',['only' => ['index', 'store']]);
	Route::get('/update','AccountController@updateApp');
	Route::post('/update',array('as' => 'update-app','uses' => 'AccountController@postUpdateApp'));*/
});

Auth::routes();

	/*Route::group(['middleware' => ['auth','web','account']],function(){
		Route::get('/verify-security','UserController@verifySecurity');
		Route::post('/verify-security',array('as' => 'user.verify-security','uses' => 'UserController@postVerifySecurity'));
	});*/

Route::group(['middleware' => ['auth','web','account','two_factor_auth','lock_screen','maintenance_mode']], function () {

	Route::get('/oauth/authorize',function(){
		return view('authorize');
	});

	#Route::get('/release-license','AccountController@releaseLicense');
	#Route::get('/check-update','AccountController@checkUpdate');
	Route::get('/home', 'HomeController@index');
	Route::post('/template/content','TemplateController@content',['middleware' => ['permission:enable_email_template']]);

	Route::get('/set-language/{locale}','LanguageController@setLanguage',['middleware' => ['permission:change-language']]);

	Route::group(['middleware' => ['permission:manage-email-log']], function () {
		Route::model('email','\App\Email');
		Route::post('/email/lists','EmailController@lists');
		Route::resource('/email', 'EmailController',['only' => ['index','show']]);
	});

	Route::group(['middleware' => ['permission:manage-configuration']], function() {
		Route::get('/configuration', 'ConfigurationController@index');
		Route::post('/configuration',array('as' => 'configuration.store','uses' => 'ConfigurationController@store'));
		Route::post('/configuration-logo',array('as' => 'configuration.logo','uses' => 'ConfigurationController@logo'));
		Route::post('/configuration-mail',array('as' => 'configuration.mail','uses' => 'ConfigurationController@mail'));
		Route::post('/configuration-sms',array('as' => 'configuration.sms','uses' => 'ConfigurationController@sms'));
	});

	Route::group(['middleware' => ['permission:manage-template']], function() {
		Route::model('template','\App\Template');
		Route::post('/template/lists','TemplateController@lists');
		Route::resource('/template', 'TemplateController');
	});

	Route::group(['middleware' => ['permission:manage-message']], function() {
		Route::get('/message', 'MessageController@index');
		Route::post('/load-message','MessageController@load');
		Route::post('/message/{type}/lists','MessageController@lists');
		Route::get('/message/forward/{token}','MessageController@forward');
		Route::post('/message', ['as' => 'message.store', 'uses' => 'MessageController@store']);
		Route::post('/message-reply/{id}', ['as' => 'message.reply', 'uses' => 'MessageController@reply']);
		Route::post('/message-forward/{token}', ['as' => 'message.post-forward', 'uses' => 'MessageController@postForward']);
		Route::get('/message/{token}/download','MessageController@download');
		Route::post('/message/starred','MessageController@starred');
		Route::get('/message/{token}', array('as' => 'message.view', 'uses' => 'MessageController@view'));
		Route::delete('/message/{id}/trash', array('as' => 'message.trash', 'uses' => 'MessageController@trash'));
		Route::post('/message/restore', array('as' => 'message.restore', 'uses' => 'MessageController@restore'));
		Route::delete('/message/{id}/delete', array('as' => 'message.destroy', 'uses' => 'MessageController@destroy'));
	});

	Route::group(['middleware' => ['permission:manage-todo']], function() {
		Route::model('todo','\App\Todo');
		Route::resource('/todo', 'TodoController');
	});

	Route::group(['middleware' => ['permission:manage-language']], function() {
		Route::post('/language/lists','LanguageController@lists');
		Route::resource('/language', 'LanguageController');
		Route::post('/language/addWords',array('as'=>'language.add-words','uses'=>'LanguageController@addWords'));
		Route::patch('/language/plugin/{locale}',array('as'=>'language.plugin','uses'=>'LanguageController@plugin'));
		Route::patch('/language/updateTranslation/{id}', ['as' => 'language.update-translation','uses' => 'LanguageController@updateTranslation']);
	});

	Route::group(['middleware' => ['permission:manage-backup']], function() {
		Route::model('backup','\App\Backup');
		Route::post('/backup/lists','BackupController@lists');
		Route::resource('/backup', 'BackupController',['only' => ['index','show','store','destroy']]);
	});

	Route::group(['middleware' => ['permission:manage-ip-filter']], function() {
		Route::model('ip_filter','\App\IpFilter');
		Route::post('/ip-filter/lists','IpFilterController@lists');
		Route::resource('/ip-filter', 'IpFilterController');
	});

	Route::group(['middleware' => ['permission:manage-custom-field']], function() {
		Route::model('custom_field','\App\CustomField');
		Route::post('/custom-field/lists','CustomFieldController@lists');
		Route::resource('/custom-field', 'CustomFieldController');
	});

	Route::group(['middleware' => ['permission:manage-role']], function() {
		Route::model('role','\App\Role');
		Route::post('/role/lists','RoleController@lists');
		Route::resource('/role', 'RoleController');
	});

	Route::group(['middleware' => ['permission:manage-permission']], function() {
		Route::model('permission','\App\Permission');
		Route::post('/permission/lists','PermissionController@lists');
		Route::resource('/permission', 'PermissionController');
		Route::get('/save-permission','PermissionController@permission');
		Route::post('/save-permission',array('as' => 'permission.save-permission','uses' => 'PermissionController@savePermission'));
	});

	Route::model('chat','\App\Chat');
	Route::resource('/chat', 'ChatController',['only' => 'store']);
	Route::post('/fetch-chat','ChatController@index');

	Route::get('/lock','HomeController@lock');
	Route::post('/lock',array('as' => 'unlock','uses' => 'HomeController@unlock'));

	Route::group(['middleware' => ['feature_available:enable_activity_log']],function() {
		Route::get('/activity-log','HomeController@activityLog');
		Route::post('/activity-log/lists','HomeController@activityLogList');
	});

	//corporate
	Route::model('corporate','\App\Corporate');
	Route::post('/corporate/lists','CorporateController@lists');
	Route::resource('/corporate', 'CorporateController');
	Route::post('/corporate/profile-update/{id}',array('as' => 'corporate.profile-update','uses' => 'CorporateController@profileUpdate'));
	Route::post('/corporate/avatar/{id}',array('as' => 'corporate.avatar','uses' => 'CorporateController@avatar'));
	Route::post('/corporate/custom-field-update/{id}',array('as' => 'corporate.custom-field-update','uses' => 'CorporateController@customFieldUpdate'));
	Route::get('/division/create/{id}','CorporateController@addDivision');
	Route::post('/division-add','CorporateController@addNewDivision');


	//simulasi
	Route::get('/survey/{slug}/{section}/{simulasi}', 'SurveyController@getSimulasi');

	//user
	Route::model('user','\App\User');
	Route::get('/user/history/{user_id}', 'SurveyController@get_project_diassign');
	Route::get('/user/history/{slug}/{user_id}', 'SurveyController@get_tools_diassign');
	Route::post('/user/lists','UserController@lists');
	Route::post('/user/reset-password/{user_id}','UserController@updatePassword');
	Route::get('/user/reset',function(){
		return view('user.update-password');
	});
	Route::post('/user/profile-update/{id}',array('as' => 'user.profile-update','uses' => 'UserController@profileUpdate'));
	Route::post('/user/social-update/{id}',array('as' => 'user.social-update','uses' => 'UserController@socialUpdate'));
	Route::post('/user/custom-field-update/{id}',array('as' => 'user.custom-field-update','uses' => 'UserController@customFieldUpdate'));
	Route::post('/user/avatar/{id}',array('as' => 'user.avatar','uses' => 'UserController@avatar'));
	Route::post('/change-user-status','UserController@changeStatus');
	Route::post('/force-change-user-password/{user_id}',array('as' => 'user.force-change-password','uses' => 'UserController@forceChangePassword'));
	Route::get('/change-password', 'UserController@changePassword');
	Route::post('/change-password',array('as'=>'change-password','uses' =>'UserController@doChangePassword'));
	Route::post('/user/email/{id}',array('as' => 'user.email', 'uses' => 'UserController@email'));

	// upload user Excel
	Route::get('/user/upload', 'UploaduserController@index');
	Route::get('/format-excel', 'UploaduserController@downloadFormat');
	Route::get('/format-excel-add-user', 'UploaduserController@downloadFormatAdd');
	Route::post('/upload-user', 'UploaduserController@upload');
	//end upload excel

	//backend route
	Route::get('backend/history', 'BackendController@history');
	Route::get('backend/report/{slug}', 'BackendController@reportUserBySurvey');
	Route::get('backend/detail-peserta-survey/{slug}', 'BackendController@getPesertaSurvey');
	Route::post('backend/survey-reminder', 'BackendController@surveyReminder');
	Route::get('backend/report-heat/{id}', 'BackendController@reportHEAT');
	Route::get('/backend/report-wpa-with-user/{slug}', 'BackendController@reportWPAPapiWithUser');
	Route::post('/backend/report-wpa-by-user/', 'BackendController@reportWPAByUser');
	Route::get('/backend/report-papi-with-user/{slug}', 'BackendController@reportWPAPapiWithUser');
	Route::get('/backend/user-report/', 'BackendController@rekapReportUser');

	//grafik admin
	Route::get('/backend/report/gti/{slug}/{id_user}', 'ReportController@gti_graph');
	Route::get('/backend/report/tiki/{slug}/{id_user}', 'ReportController@tiki_graph');
	Route::get('/backend/report/kostick/{slug}/{id_user}', 'ReportController@kostick_graph');
	Route::get('/backend/report/wpa/{slug}/{id_user}', 'ReportController@wpa_graph');
	Route::get('/backend/report/get-jpm/{slug}/{id_user}', 'ReportController@get_jpm');
	Route::get('/backend/report/{category}/{slug}/{user_id}', 'ReportController@get_report');

	//grafik user
	Route::get('user/report', 'UserReportController@dashboard');
	Route::get('user/report/gti/{slug}', 'UserReportController@gti_graph');
	Route::get('user/report/tiki/{slug}', 'UserReportController@tiki_graph');
	Route::get('user/report/kostick/{slug}', 'UserReportController@kostick_graph');
	Route::get('user/report/wpa/{slug}', 'UserReportController@wpa_graph');
	Route::get('user/report/get-jpm/{slug}', 'UserReportController@get_jpm');
	Route::get('user/report/{category}/{slug}', 'UserReportController@get_report');
	Route::post('user/report/jpm', array('as' => 'report-jpm','uses' => 'UserReportController@jpm'));

	//manage JPM
	Route::get('job-characteristic', 'JPMController@index');
	Route::post('/job-characteristic/lists','JPMController@lists');
	Route::get('job-characteristic/{id}', 'JPMController@show');
	Route::get('/create-job-characteristic', 'JPMController@create');
	Route::post('/job-characteristic',array('as' => 'job-characteristic.store','uses' => 'JPMController@store'));
	Route::model('job-characteristic','\App\Jobs');
	Route::resource('/job-characteristic', 'JPMController',['except' => ['store','edit']]);
	Route::resource('/user', 'UserController',['except' => ['store','edit']]);

  	//PDF Routes
  	Route::get('/print/gti/{slug}/{id_user}','PdfController@pdfGti');
	Route::get('/print/tiki/{slug}/{id_user}','PdfController@pdfTiki');
	Route::get('/print/wba/{slug}/{id_user}','PdfController@pdfWba');
	Route::get('/print/wpa/{slug}/{id_user}','PdfController@pdfWpa');
	Route::get('/print/all/{id_project}/{id_user}','PdfController@printAllReport');
	Route::get('/download/report/{slug}/{id_user}','PdfController@download_report_wpa'); //untuk download laporan tools dari storage

  	//excel
  	Route::post('export/excel-gti-tiki','ExcelController@excelGtiTiki');
  	Route::post('export/excel-wpa-papi','ExcelController@excelWpaPapi');

  	//aspek psikograph
  	Route::get('aspek-psikograph','AspekPsikographController@index');
  	Route::get('aspek-psikograph/kalkulasi/{user_id}/{project_id}','AspekPsikographController@kalkulasi');

  	// Laporan User
  	Route::get('backend/report-user/', "SurveyController@list_report_user");
  	Route::get('backend/report-user/{id}', "SurveyController@index_report_user");
  	Route::get('backend/report-user/generate-all-tpa/{project_id}', "SurveyController@generate_all_tpa");
  	Route::post('backend/report-user/{id}', "SurveyController@index_report_user");
  	Route::get('backend/report-user/detail/{user_id}/{project_id}', "SurveyController@report_user");
  	Route::get('backend/report-user/compare/{user_ids}/{project_id}', "SurveyController@compare_user");
  	Route::get('backend/report-user/print/{user_id}/{project_id}', "SurveyController@print_report_user");
  	Route::get('backend/report-user/print-pdf/{user_id}/{project_id}', "SurveyController@print_report_user_depan");
  	Route::post('backend/report-user/detail/kalkulasi/{user_id}/{project_id}', "SurveyController@kalkulasi_report_user");
  	Route::post('backend/report-user/detail/{user_id}/{project_id}/update-uraian', "SurveyController@update_uraian_user");
  	Route::post('backend/report-user/detail/{user_id}/{project_id}/update-nilai', "SurveyController@update_nilai_aspek");

  	//mapping database
  	Route::get('mapping/report-user', 'MappingController@report_user');
  	Route::get('mapping/kuota-project', 'MappingController@kuota_project');

  	//inject data project susulan
  	Route::get('inject/laporan-panjang', 'InjectDataController@inject_laporan_panjang');
  	Route::get('inject/laporan-subtest', 'InjectDataController@inject_laporan_subtest');
  	Route::get('inject/add-number-quest', 'InjectDataController@add_number_on_question');
  	Route::get('inject/add-opsi', 'InjectDataController@add_opsi_on_option_answer');

  	//inject data gabung project danamon
  	Route::get('inject/gabung-project-ho-trial/{id_project}', 'InjectDataController@gabung_project_ho_trial');
  	Route::get('inject/gabung-project-dev-snd/{id_project}', 'InjectDataController@gabung_project_development_program');

  	//serialize raw jawaban
  	Route::get('serialize/lai-tiki', 'InjectDataController@serialize_lai_tiki');
  	Route::get('serialize/wpa', 'InjectDataController@serialize_wpa');
  	Route::get('serialize/wba', 'InjectDataController@serialize_wba');

  	//raw input
  	Route::get('/backend/raw-input', 'RawDataController@raw_input_project');
  	Route::get('/backend/raw-input/TIKI/{slug_project}/{slug_tools}', 'RawDataController@raw_input_peserta_tiki');
  	Route::get('/backend/raw-input/LAI/{slug_project}/{slug_tools}', 'RawDataController@raw_input_peserta_lai');
  	Route::get('/backend/raw-input/WPA/{slug_project}/{slug_tools}', 'RawDataController@raw_input_peserta_wpa');
  	Route::get('/backend/raw-input/WBA/{slug_project}/{slug_tools}', 'RawDataController@raw_input_peserta_wba');
});
