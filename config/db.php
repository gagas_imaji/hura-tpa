<?php return array (
  'hostname' => env('DB_HOST', true),
  'database' => env('DB_DATABASE', true),
  'username' => env('DB_USERNAME', true),
  'password' =>env('DB_PASSWORD', true),
);