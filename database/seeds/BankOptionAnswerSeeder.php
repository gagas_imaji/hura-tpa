<?php

use Illuminate\Database\Seeder;

class BankOptionAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_option_answer')->insert([
        	//1
			[
	            'id_bank_question'	=> 1,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 1,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 1,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 1,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 1,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//2
			[
	            'id_bank_question'	=> 2,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 2,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 2,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 2,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 2,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//3
			[
	            'id_bank_question'	=> 3,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 3,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 3,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 3,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 3,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//4
			[
	            'id_bank_question'	=> 4,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 4,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 4,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 4,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 4,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//5
			[
	            'id_bank_question'	=> 5,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 5,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 5,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 5,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 5,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//6
			[
	            'id_bank_question'	=> 6,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 6,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 6,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 6,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 6,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//7
			[
	            'id_bank_question'	=> 7,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 7,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 7,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 7,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 7,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//8
			[
	            'id_bank_question'	=> 8,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 8,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 8,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 8,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 8,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//9
			[
	            'id_bank_question'	=> 9,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 9,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 9,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 9,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 9,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//10
			[
	            'id_bank_question'	=> 10,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 10,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 10,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 10,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 10,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//11
			[
	            'id_bank_question'	=> 11,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 11,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 11,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 11,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 11,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//12
			[
	            'id_bank_question'	=> 12,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 12,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 12,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 12,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 12,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//13
			[
	            'id_bank_question'	=> 13,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 13,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 13,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 13,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 13,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//14
			[
	            'id_bank_question'	=> 14,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 14,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 14,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 14,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 14,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//15
			[
	            'id_bank_question'	=> 15,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 15,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 15,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 15,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 15,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//16
			[
	            'id_bank_question'	=> 16,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 16,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 16,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 16,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 16,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//17
			[
	            'id_bank_question'	=> 17,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 17,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 17,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 17,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 17,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//18
			[
	            'id_bank_question'	=> 18,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 18,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 18,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 18,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 18,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//19
			[
	            'id_bank_question'	=> 19,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 19,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 19,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 19,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 19,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//20
			[
	            'id_bank_question'	=> 20,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 20,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 20,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 20,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 20,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],

        	//21
			[
	            'id_bank_question'	=> 21,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 21,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 21,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 21,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 21,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//22
			[
	            'id_bank_question'	=> 22,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 22,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 22,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 22,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 22,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//23
			[
	            'id_bank_question'	=> 23,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 23,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 23,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 23,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 23,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//24
			[
	            'id_bank_question'	=> 24,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 24,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 24,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 24,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 24,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//25
			[
	            'id_bank_question'	=> 25,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 25,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 25,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 25,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 25,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//26
			[
	            'id_bank_question'	=> 26,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 26,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 26,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 26,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 26,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//27
			[
	            'id_bank_question'	=> 27,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 27,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 27,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 27,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 27,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//28
			[
	            'id_bank_question'	=> 28,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 28,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 28,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 28,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 28,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//29
			[
	            'id_bank_question'	=> 29,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 29,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 29,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 29,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 29,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//30
			[
	            'id_bank_question'	=> 30,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 30,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 30,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 30,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 30,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//31
			[
	            'id_bank_question'	=> 31,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 31,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 31,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 31,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 31,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//32
			[
	            'id_bank_question'	=> 32,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 32,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 32,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 32,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 32,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//33
			[
	            'id_bank_question'	=> 33,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 33,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 33,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 33,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 33,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//34
			[
	            'id_bank_question'	=> 34,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 34,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 34,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 34,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 34,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//35
			[
	            'id_bank_question'	=> 35,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 35,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 35,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 35,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 35,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//36
			[
	            'id_bank_question'	=> 36,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 36,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 36,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 36,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 36,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//37
			[
	            'id_bank_question'	=> 37,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 37,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 37,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 37,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 37,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//38
			[
	            'id_bank_question'	=> 38,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 38,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 38,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 38,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 38,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//39
			[
	            'id_bank_question'	=> 39,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 39,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 39,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 39,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 39,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//40
			[
	            'id_bank_question'	=> 40,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 40,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 40,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 40,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 40,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//41
			[
	            'id_bank_question'	=> 41,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 41,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 41,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 41,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 41,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//42
			[
	            'id_bank_question'	=> 42,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 42,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 42,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 42,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 42,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//43
			[
	            'id_bank_question'	=> 43,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 43,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 43,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 43,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 43,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//44
			[
	            'id_bank_question'	=> 44,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 44,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 44,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 44,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 44,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//45
			[
	            'id_bank_question'	=> 45,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 45,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 45,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 45,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 45,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//46
			[
	            'id_bank_question'	=> 46,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 46,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 46,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 46,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 46,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//47
			[
	            'id_bank_question'	=> 47,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 47,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 47,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 47,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 47,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//48
			[
	            'id_bank_question'	=> 48,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 48,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 48,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 48,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 48,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//49
			[
	            'id_bank_question'	=> 49,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 49,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 49,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 49,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 49,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//50
			[
	            'id_bank_question'	=> 50,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 50,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 50,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 50,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 50,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//---------------------
        	//51
			[
	            'id_bank_question'	=> 51,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 51,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 51,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 51,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 51,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//52
			[
	            'id_bank_question'	=> 52,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 52,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 52,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 52,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 52,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//53
			[
	            'id_bank_question'	=> 53,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 53,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 53,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 53,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 53,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//54
			[
	            'id_bank_question'	=> 54,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 54,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 54,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 54,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 54,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//55
			[
	            'id_bank_question'	=> 55,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 55,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 55,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 55,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 55,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//56
			[
	            'id_bank_question'	=> 56,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 56,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 56,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 56,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 56,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	//57
			[
	            'id_bank_question'	=> 57,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 57,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 57,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 57,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 57,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],

        	//58
			[
	            'id_bank_question'	=> 58,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 58,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 58,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 58,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 58,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 58,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 58,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//59
			[
	            'id_bank_question'	=> 59,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 59,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 59,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 59,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 59,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 59,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 59,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//60
			[
	            'id_bank_question'	=> 60,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 60,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 60,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 60,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 60,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 60,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 60,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	
        	//61
			[
	            'id_bank_question'	=> 61,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 61,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 61,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 61,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 61,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 61,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 61,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],

        	//62
			[
	            'id_bank_question'	=> 62,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 62,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 62,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 62,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 62,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 62,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 62,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//63
			[
	            'id_bank_question'	=> 63,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 63,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 63,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 63,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 63,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 63,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 63,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//64
			[
	            'id_bank_question'	=> 64,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 64,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 64,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 64,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 64,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 64,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 64,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//65
			[
	            'id_bank_question'	=> 65,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 65,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 65,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 65,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 65,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 65,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 65,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//66
			[
	            'id_bank_question'	=> 66,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 66,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 66,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 66,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 66,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 66,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 66,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],

        	//67
			[
	            'id_bank_question'	=> 67,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 67,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 67,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 67,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 67,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 67,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 67,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],

        	//68
			[
	            'id_bank_question'	=> 68,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 68,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 68,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 68,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 68,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 68,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 68,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//69
			[
	            'id_bank_question'	=> 69,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 69,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 69,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 69,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 69,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 69,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 69,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//70
			[
	            'id_bank_question'	=> 70,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 70,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 70,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 70,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 70,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 70,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 70,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//71
			[
	            'id_bank_question'	=> 71,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 71,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 71,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 71,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 71,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 71,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 71,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],

        	//72
			[
	            'id_bank_question'	=> 72,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 72,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 72,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 72,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 72,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 72,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 72,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//73
			[
	            'id_bank_question'	=> 73,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 73,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 73,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 73,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 73,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 73,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 73,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
        	//74
			[
	            'id_bank_question'	=> 74,
	            'answer' 			=> 'Tidak pernah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 74,
	            'answer' 			=> 'Sekali setahun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 74,
	            'answer' 			=> 'Sekali dalam beberapa bulan',
	            'image'				=> '',
	            'point'				=> 2,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 74,
	            'answer' 			=> 'Sekali sebulan',
	            'image'				=> '',
	            'point'				=> 3,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 74,
	            'answer' 			=> 'Sekali seminggu',
	            'image'				=> '',
	            'point'				=> 4,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 74,
	            'answer' 			=> 'Hampir setiap hari',
	            'image'				=> '',
	            'point'				=> 5,
	            'hidden'			=> 0
        	],
        	[
	            'id_bank_question'	=> 74,
	            'answer' 			=> 'Setiap hari',
	            'image'				=> '',
	            'point'				=> 6,
	            'hidden'			=> 0
        	],
		]);
    }
}
