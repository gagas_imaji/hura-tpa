<?php

use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('division')->insert([
			[
                'id_corporate'  => 1,
	            'name' 			=> 'IT',
	            'slug' 			=> 'it'
        	],
            [
                'id_corporate'  => 2,
                'name'          => 'IT',
                'slug'          => 'it'
            ]
        ]);
    }
}
