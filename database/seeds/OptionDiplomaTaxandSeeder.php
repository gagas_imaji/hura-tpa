<?php

use Illuminate\Database\Seeder;

class OptionDiplomaTaxandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        DB::table('option_answer')->insert([
		[
		            'id_question'	=> 9,
		            'answer' 		=> 'Also I ! * *',
		            'image'			=> '',
		            'point'			=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 9,
		            'answer' 		=> 'So I am !',
		            'image'			=> '',
		            'point'			=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 9,
		            'answer' 		=> 'So am I !',
		            'image'			=> '',
		            'point'			=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 9,
		            'answer' 		=> 'As well !',
		            'image'			=> '',
		            'point'			=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 9,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>10,
		            'answer' 		=> 'How long *',
		            'image'			=> '',
		            'point'			=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>10,
		            'answer' 		=> 'When',
		            'image'			=> '',
		            'point'			=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>10,
		            'answer' 		=> 'How much',
		            'image'			=> '',
		            'point'			=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>10,
		            'answer' 		=> 'What time',
		            'image'			=> '',
		            'point'			=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 10,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>11,
		            'answer' 	=> 'well ! *',
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>11,
		            'answer' 	=> 'it !',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>11,
		            'answer' 	=> 'So !',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>11,
		            'answer' 	=> 'very much !',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 11,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>12,
		            'answer' 	=> 'would you like *',
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>12,
		            'answer' 	=> 'do you like',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>12,
		            'answer' 	=> 'would you',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>12,
		            'answer' 	=> "what's about",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 12,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>13,
		            'answer' 	=> "what's that for a car ? *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>13,
		            'answer' 	=> 'what for a car is that ?',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>13,
		            'answer' 	=> 'what kind of car is that ?',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>13,
		            'answer' 	=> "what's the kind of that car ?",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 13,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>14,
		            'answer' 	=> "I regret it. *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>14,
		            'answer' 	=>"I'm sorry not",
		            'image'	=> '',
		            'point'		=> 0,
			            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>14,
		            'answer' 	=> 'Not, sorry.',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>14,
		            'answer' 	=> "I'm afraid not",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 14,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>15,
		            'answer' 	=> "Neither have I *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>15,
		            'answer' 	=>"Neither did I",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>15,
		            'answer' 	=> 'So, never I.',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>15,
		            'answer' 	=> "I neither have",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 15,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>16,
		            'answer' 	=> "do you say 'hello' in Italian *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>16,
		            'answer' 	=>"is in Italian 'hello'",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>16,
		            'answer' 	=> 'you say "hello" in Italian',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>16,
		            'answer' 	=> "is said 'hello' in Italian",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 16,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>17,
		            'answer' 	=> "twelve *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>17,
		            'answer' 	=>"twelfth",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>17,
		            'answer' 	=> 'the twelfth of',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>17,
		            'answer' 	=> "twelve of",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 17,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>18,
		            'answer' 	=> "do you spell *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>18,
		            'answer' 	=>"is the spelling of'",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>18,
		            'answer' 	=> 'to write',
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>18,
		            'answer' 	=> "is writen",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 18,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>19,
		            'answer' 	=> "I'm leaving *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>19,
		            'answer' 	=>"Thank you'",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>19,
		            'answer' 	=> "I'm student",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>19,
		            'answer' 	=> "Very well, thank you.",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 19,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>20,
		            'answer' 	=> "I'm OK *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>20,
		            'answer' 	=>"Very good, thank you'",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>20,
		            'answer' 	=> "I'm an accountant",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>20,
		            'answer' 	=> "I'm busy at the moment.",
		            'image'	=> '',
		            'point'		=> 0,
			       'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 20,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>21,
		            'answer' 	=> "I'm fine *",
		            'image'	=> '',
		            'point'		=> 1,
			       'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>21,
		            'answer' 	=>"How do you do'",
		            'image'	=> '',
		            'point'		=> 0,
			       'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>21,
		            'answer' 	=> "It's OK",
		            'image'	=> '',
		            'point'		=> 0,
			       'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>21,
		            'answer' 	=> "My name's Dodi",
		            'image'	=> '',
		            'point'		=> 0,
			       'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 21,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>22,
		            'answer' 	=> "Not at all *",
		            'image'	=> '',
		            'point'		=> 1,
			       'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>22,
		            'answer' 	=>"Where is it ?'",
		            'image'	=> '',
		            'point'		=> 0,
			       'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>22,
		            'answer' 	=> "Thanks a lot",
		            'image'	=> '',
		            'point'		=> 0,
			            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>22,
		            'answer' 	=> "Down town",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 22,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>23,
		            'answer' 	=> "Since 1999 *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>23,
		            'answer' 	=>"Recently'",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>23,
		            'answer' 	=> "Last month",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>23,
		            'answer' 	=> "For 1999",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 23,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>24,
		            'answer' 	=> "Graduating in 1998 *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>24,
		            'answer' 	=>"I have graduated in 1998",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>24,
		            'answer' 	=> "I am graduating last year",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>24,
		            'answer' 	=> "I graduated last year",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 24,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],

	        	//
	        	[
		            'id_question'	=>25,
		            'answer' 	=> "I am politician *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>25,
		            'answer' 	=>"I am looking for a job",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>25,
		            'answer' 	=> "Accounting",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>25,
		            'answer' 	=> "To be a journalist",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 25,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=>26,
		            'answer' 	=> "I too *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>26,
		            'answer' 	=>"So, do I",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>26,
		            'answer' 	=> "I also like",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>26,
		            'answer' 	=> "I did too",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 26,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],

	        	//
	        	[
		            'id_question'	=>27,
		            'answer' 	=> "Neither have I *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>27,
		            'answer' 	=>"Neither did I",
		            'image'	=> '',
		            'point'		=> 0,
			       	'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>27,
		            'answer' 	=> "So, never I",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>27,
		            'answer' 	=> "I neither have",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 27,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        		//
	        	[
		            'id_question'	=>28,
		            'answer' 	=> "Would meet *",
		            'image'	=> '',
		            'point'		=> 1,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>28,
		            'answer' 	=>"Would have met",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>28,
		            'answer' 	=> "will meet",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=>28,
		            'answer' 	=> "will have meet",
		            'image'	=> '',
		            'point'		=> 0,
			        'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 28,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],

	        	//option no answer Grammar A
	        	[
		            'id_question'	=> 49,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 50,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 51,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 52,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 53,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 54,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 55,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 56,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 57,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 58,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],

	        	//option no answer Grammar B
	        	[
		            'id_question'	=> 59,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 60,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 61,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 62,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 63,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 64,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 65,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 66,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 67,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 68,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],

	        	//option Reading A
	        	[
		            'id_question'	=> 99,
		            'answer' 		=> 'The information revolution *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 99,
		            'answer' 		=> 'Cheaper computers',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 99,
		            'answer' 		=> 'Traveling by Highway in the 1990s',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 99,
		            'answer' 		=> 'The importance of news',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 99,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=> 100,
		            'answer' 		=> 'More than 10 times *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 100,
		            'answer' 		=> 'More than 15 times',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 100,
		            'answer' 		=> 'More than 5 times',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 100,
		            'answer' 		=> 'More than 20 times',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 100,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=> 101,
		            'answer' 		=> 'The information Highway *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 101,
		            'answer' 		=> 'The cable system',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 101,
		            'answer' 		=> 'The market potential',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 101,
		            'answer' 		=> 'The on-line services',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 101,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=> 102,
		            'answer' 		=> 'Increased services *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 102,
		            'answer' 		=> 'Better software',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 102,
		            'answer' 		=> 'Lower costs',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 102,
		            'answer' 		=> 'The information revolution',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	[
		            'id_question'	=> 102,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	//option Reading B
	        	[
		            'id_question'	=> 103,
		            'answer' 		=> 'Improve quality control *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 103,
		            'answer' 		=> 'Export more goods',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 103,
		            'answer' 		=> 'Build factories close to their customers',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 103,
		            'answer' 		=> 'Design better products',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 103,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=> 104,
		            'answer' 		=> 'Design consultants *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 104,
		            'answer' 		=> 'Customers',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 104,
		            'answer' 		=> 'Suppliers',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 104,
		            'answer' 		=> 'Engineers',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 104,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],

	        	//option Reading C
	        	[
		            'id_question'	=> 105,
		            'answer' 		=> 'Larger rooms *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 105,
		            'answer' 		=> 'Better amenities',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 105,
		            'answer' 		=> 'Loghter foods',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 105,
		            'answer' 		=> 'Protecting teh earth',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 105,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=> 106,
		            'answer' 		=> 'It’s a marketing gimmick *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 106,
		            'answer' 		=> 'It’s only affective on eco-tours',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 106,
		            'answer' 		=> 'It’s wise choice environmentally',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 106,
		            'answer' 		=> 'Hotels can set customers trends',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 106,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=> 107,
		            'answer' 		=> 'Fashion designers *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 107,
		            'answer' 		=> 'First-time visitors',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 107,
		            'answer' 		=> 'Environmentally conscious travelers',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 107,
		            'answer' 		=> 'Golf course owners',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 107,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
	        	[
		            'id_question'	=> 108,
		            'answer' 		=> 'To find litter *',
		            'image'			=> '',
		            'point'			=> 1,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 108,
		            'answer' 		=> 'To hear lectures on the environment',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 108,
		            'answer' 		=> 'To pay more than other travelers',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 108,
		            'answer' 		=> 'To carry their own food',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 0
	        	],
	        	[
		            'id_question'	=> 108,
		            'answer' 		=> 'No Answer',
		            'image'			=> '',
		            'point'			=> 0,
		            'hidden'		=> 1
	        	],
	        	//
        ]);
    }
}
