<?php

use Illuminate\Database\Seeder;

class WPAKarakteristikUmumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_wpa_karakteristik_umum')->insert([
        	[
        		'D' => 	'Aktif dan Optimis',
	        	'I'	=>	'Inspirator',
	        	'S'	=>	'Santai/ rileks',
	        	'C'	=>	'Ketepatan'
        	],
        	[
        		'D' => 	'Ambisius',
	        	'I'	=>	'Komunikator',
	        	'S'	=>	'Mengakomodasi berbagai hal',
	        	'C'	=>	'Kualitas'
        	],
        	[
        		'D' => 	'Ingin cepat mendapatkan hasil',
	        	'I'	=>	'Membuat kesan yang menyenangkan',
	        	'S'	=>	'Loyal',
	        	'C'	=>	'Rinci'
        	],
        	[
        		'D' => 	'Mengarahkan',
	        	'I'	=>	'Keinginan untuk membantu orang lain',
	        	'S'	=>	'Konsisten',
	        	'C'	=>	'Berpikir kritis'
        	],
        	[
        		'D' => 	'Keinginannya kuat',
	        	'I'	=>	'Senang menjalin kontak sosial',
	        	'S'	=>	'Sistematis',
	        	'C'	=>	'Hati-hati'
        	],
        	[
        		'D' => 	'Cepat mengambil keputusan',
	        	'I'	=>	'Antusias',
	        	'S'	=>	'Perilakunya mudah diramalkan',
	        	'C'	=>	'Analitis'
        	],
        	[
        		'D' => 	'Menantang',
	        	'I'	=>	'Demonstratif',
	        	'S'	=>	'Pendengar yang baik',
	        	'C'	=>	'Cermat'
        	],
        	[
        		'D' => 	'Independen',
	        	'I'	=>	'Persuasif',
	        	'S'	=>	'Tulus',
	        	'C'	=>	'Sensitif'
        	],
        	[
        		'D' => 	'Penentu',
	        	'I'	=>	'Optimis',
	        	'S'	=>	'Empati yang tinggi',
	        	'C'	=>	'Standar Tinggi'
        	],
        ]);
    }
}
