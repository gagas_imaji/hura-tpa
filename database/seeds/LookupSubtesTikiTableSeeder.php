<?php

use Illuminate\Database\Seeder;

class LookupSubtesTikiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_subtes_tiki')->insert(array(
        	array('min'=>0,'max'=>6,'nilai'=>2),
        	array('min'=>7,'max'=>12,'nilai'=>3),
        	array('min'=>13,'max'=>18,'nilai'=>4),
        	array('min'=>19,'max'=>24,'nilai'=>5),
        	array('min'=>25,'max'=>30,'nilai'=>6),
        ));
    }
}
