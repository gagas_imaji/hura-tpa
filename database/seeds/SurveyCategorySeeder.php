<?php

use Illuminate\Database\Seeder;

class SurveyCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey_category')->insert([
			[
	            'name' => 'Learning Agility Index',
	            'slug' => 'learning-agility-index'
        	],
        	[
	            'name' => 'Test Intelegensi Kolektif Indonesia',
	            'slug' => 'test-intelegensi-kolektif-indonesia'
        	],
        	[
	            'name' => 'Work Personality Analytics',
	            'slug' => 'work-personality-analytics'
        	],
        	[
	            'name' => 'Work Behavioural Assessment ',
	            'slug' => 'work-behavioural-assessment '
        	]
        ]);
    }
}
