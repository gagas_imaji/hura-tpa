<?php

use Illuminate\Database\Seeder;

class JobsAgreementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('job_agreement')->insert([
        [    
            'agreement'     =>'0',
            'pria'      	=>'2',
            'wanita'      	=>'0',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'1',
            'pria'      	=>'5',
            'wanita'      	=>'1',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'2',
            'pria'      	=>'8',
            'wanita'      	=>'4',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'3',
            'pria'      	=>'11',
            'wanita'      	=>'6',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'4',
            'pria'      	=>'15',
            'wanita'      	=>'7',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'5',
            'pria'      	=>'16',
            'wanita'      	=>'9',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'6',
            'pria'      	=>'24',
            'wanita'      	=>'13',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'7',
            'pria'      	=>'25',
            'wanita'      	=>'14',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'8',
            'pria'      	=>'25',
            'wanita'      	=>'15',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'9',
            'pria'      	=>'30',
            'wanita'      	=>'19',
            'created_at'    => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'10',
            'pria'      	=>'34',
            'wanita'      	=>'22',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'11',
            'pria'      	=>'38',
            'wanita'      	=>'26',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'12',
            'pria'      	=>'44',
            'wanita'      	=>'32',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'13',
            'pria'      	=>'44',
            'wanita'      	=>'39',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'14',
            'pria'      	=>'51',
            'wanita'      	=>'43',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'15',
            'pria'      	=>'52',
            'wanita'      	=>'46',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'16',
            'pria'      	=>'53',
            'wanita'      	=>'48',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'17',
            'pria'      	=>'55',
            'wanita'      	=>'50',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'18',
            'pria'      	=>'57',
            'wanita'      	=>'51',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'19',
            'pria'      	=>'78',
            'wanita'      	=>'53',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'20',
            'pria'      	=>'59',
            'wanita'      	=>'54',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'21',
            'pria'      	=>'62',
            'wanita'      	=>'60',
            'created_at'    => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'22',
            'pria'      	=>'72',
            'wanita'      	=>'68',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'23',
            'pria'      	=>'75',
            'wanita'      	=>'71',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'24',
            'pria'      	=>'89',
            'wanita'      	=>'84',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'25',
            'pria'      	=>'90',
            'wanita'      	=>'87',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'26',
            'pria'      	=>'92',
            'wanita'      	=>'90',
            'created_at'    => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'27',
            'pria'      	=>'97',
            'wanita'      	=>'97',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'agreement'     =>'28',
            'pria'      	=>'99',
            'wanita'      	=>'99',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ]
    ]);
    }
}
