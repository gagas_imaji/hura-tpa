<?php

use Illuminate\Database\Seeder;

class BankOptionAnswerTikiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	

    	//berhitung angka
        DB::table('bank_option_answer')->insert([
        	//1
			[
	            'id_bank_question'	=> 377,
	            'answer' 			=> 'pilihan1a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 377,
	            'answer' 			=> 'pilihan1b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 377,
	            'answer' 			=> 'pilihan1c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 377,
	            'answer' 			=> 'pilihan1d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//2
			[
	            'id_bank_question'	=> 378,
	            'answer' 			=> 'pilihan2a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 378,
	            'answer' 			=> 'pilihan2b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 378,
	            'answer' 			=> 'pilihan2c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 378,
	            'answer' 			=> 'pilihan2d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//3
			[
	            'id_bank_question'	=> 379,
	            'answer' 			=> 'pilihan3a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 379,
	            'answer' 			=> 'pilihan3b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 379,
	            'answer' 			=> 'pilihan3c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 379,
	            'answer' 			=> 'pilihan3d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//4
			[
	            'id_bank_question'	=> 380,
	            'answer' 			=> 'pilihan4a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 380,
	            'answer' 			=> 'pilihan4b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 380,
	            'answer' 			=> 'pilihan4c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 380,
	            'answer' 			=> 'pilihan4d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//5
			[
	            'id_bank_question'	=> 381,
	            'answer' 			=> 'pilihan5a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 381,
	            'answer' 			=> 'pilihan5b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 381,
	            'answer' 			=> 'pilihan5c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 381,
	            'answer' 			=> 'pilihan5d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//6
			[
	            'id_bank_question'	=> 382,
	            'answer' 			=> 'pilihan6a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 382,
	            'answer' 			=> 'pilihan6b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 382,
	            'answer' 			=> 'pilihan6c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 382,
	            'answer' 			=> 'pilihan6d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//7
			[
	            'id_bank_question'	=> 383,
	            'answer' 			=> 'pilihan7a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 383,
	            'answer' 			=> 'pilihan7b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 383,
	            'answer' 			=> 'pilihan7c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 383,
	            'answer' 			=> 'pilihan7d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//8
			[
	            'id_bank_question'	=> 384,
	            'answer' 			=> 'pilihan8a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 384,
	            'answer' 			=> 'pilihan8b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 384,
	            'answer' 			=> 'pilihan8c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 384,
	            'answer' 			=> 'pilihan8d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//9
			[
	            'id_bank_question'	=> 385,
	            'answer' 			=> 'pilihan9a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 385,
	            'answer' 			=> 'pilihan9b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 385,
	            'answer' 			=> 'pilihan9c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 385,
	            'answer' 			=> 'pilihan9d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//10
			[
	            'id_bank_question'	=> 386,
	            'answer' 			=> 'pilihan10a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 386,
	            'answer' 			=> 'pilihan10b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 386,
	            'answer' 			=> 'pilihan10c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 386,
	            'answer' 			=> 'pilihan10d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//11
			[
	            'id_bank_question'	=> 387,
	            'answer' 			=> 'pilihan11a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 387,
	            'answer' 			=> 'pilihan11b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 387,
	            'answer' 			=> 'pilihan11c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 387,
	            'answer' 			=> 'pilihan11d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//12
			[
	            'id_bank_question'	=> 388,
	            'answer' 			=> 'pilihan12a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 388,
	            'answer' 			=> 'pilihan12b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 388,
	            'answer' 			=> 'pilihan12c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 388,
	            'answer' 			=> 'pilihan12d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//13
			[
	            'id_bank_question'	=> 389,
	            'answer' 			=> 'pilihan13a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 389,
	            'answer' 			=> 'pilihan13b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 389,
	            'answer' 			=> 'pilihan13c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 389,
	            'answer' 			=> 'pilihan13d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//14
			[
	            'id_bank_question'	=> 390,
	            'answer' 			=> 'pilihan14a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 390,
	            'answer' 			=> 'pilihan14b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 390,
	            'answer' 			=> 'pilihan14c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 390,
	            'answer' 			=> 'pilihan14d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//15
			[
	            'id_bank_question'	=> 391,
	            'answer' 			=> 'pilihan15a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 391,
	            'answer' 			=> 'pilihan15b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 391,
	            'answer' 			=> 'pilihan15c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 391,
	            'answer' 			=> 'pilihan15d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//16
			[
	            'id_bank_question'	=> 392,
	            'answer' 			=> 'pilihan16a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 392,
	            'answer' 			=> 'pilihan16b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 392,
	            'answer' 			=> 'pilihan16c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 392,
	            'answer' 			=> 'pilihan16d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//17
			[
	            'id_bank_question'	=> 393,
	            'answer' 			=> 'pilihan17a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 393,
	            'answer' 			=> 'pilihan17b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 393,
	            'answer' 			=> 'pilihan17c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 393,
	            'answer' 			=> 'pilihan17d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//18
			[
	            'id_bank_question'	=> 394,
	            'answer' 			=> 'pilihan18a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 394,
	            'answer' 			=> 'pilihan18b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 394,
	            'answer' 			=> 'pilihan18c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 394,
	            'answer' 			=> 'pilihan18d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//19
			[
	            'id_bank_question'	=> 395,
	            'answer' 			=> 'pilihan19a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 395,
	            'answer' 			=> 'pilihan19b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 395,
	            'answer' 			=> 'pilihan19c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 395,
	            'answer' 			=> 'pilihan19d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//20
			[
	            'id_bank_question'	=> 396,
	            'answer' 			=> 'pilihan20a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 396,
	            'answer' 			=> 'pilihan20b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 396,
	            'answer' 			=> 'pilihan20c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 396,
	            'answer' 			=> 'pilihan20d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//21
			[
	            'id_bank_question'	=> 397,
	            'answer' 			=> 'pilihan21a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 397,
	            'answer' 			=> 'pilihan21b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 397,
	            'answer' 			=> 'pilihan21c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 397,
	            'answer' 			=> 'pilihan21d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//22
			[
	            'id_bank_question'	=> 398,
	            'answer' 			=> 'pilihan22a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 398,
	            'answer' 			=> 'pilihan22b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 398,
	            'answer' 			=> 'pilihan22c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 398,
	            'answer' 			=> 'pilihan22d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//23
			[
	            'id_bank_question'	=> 399,
	            'answer' 			=> 'pilihan23a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 399,
	            'answer' 			=> 'pilihan23b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 399,
	            'answer' 			=> 'pilihan23c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 399,
	            'answer' 			=> 'pilihan23d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//24
			[
	            'id_bank_question'	=> 400,
	            'answer' 			=> 'pilihan24a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 400,
	            'answer' 			=> 'pilihan24b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 400,
	            'answer' 			=> 'pilihan24c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 400,
	            'answer' 			=> 'pilihan24d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//25
			[
	            'id_bank_question'	=> 401,
	            'answer' 			=> 'pilihan25a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 401,
	            'answer' 			=> 'pilihan25b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 401,
	            'answer' 			=> 'pilihan25c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 401,
	            'answer' 			=> 'pilihan25d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//26
			[
	            'id_bank_question'	=> 402,
	            'answer' 			=> 'pilihan26a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 402,
	            'answer' 			=> 'pilihan26b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 402,
	            'answer' 			=> 'pilihan26c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 402,
	            'answer' 			=> 'pilihan26d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//27
			[
	            'id_bank_question'	=> 403,
	            'answer' 			=> 'pilihan27a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 403,
	            'answer' 			=> 'pilihan27b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 403,
	            'answer' 			=> 'pilihan27c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 403,
	            'answer' 			=> 'pilihan27d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//28
			[
	            'id_bank_question'	=> 404,
	            'answer' 			=> 'pilihan28a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 404,
	            'answer' 			=> 'pilihan28b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 404,
	            'answer' 			=> 'pilihan28c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 404,
	            'answer' 			=> 'pilihan28d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//29
			[
	            'id_bank_question'	=> 405,
	            'answer' 			=> 'pilihan29a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 405,
	            'answer' 			=> 'pilihan29b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 405,
	            'answer' 			=> 'pilihan29c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 405,
	            'answer' 			=> 'pilihan29d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//30
			[
	            'id_bank_question'	=> 406,
	            'answer' 			=> 'pilihan30a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 406,
	            'answer' 			=> 'pilihan30b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 406,
	            'answer' 			=> 'pilihan30c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 406,
	            'answer' 			=> 'pilihan30d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//31
			[
	            'id_bank_question'	=> 407,
	            'answer' 			=> 'pilihan31a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 407,
	            'answer' 			=> 'pilihan31b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 407,
	            'answer' 			=> 'pilihan31c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 407,
	            'answer' 			=> 'pilihan31d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//32
			[
	            'id_bank_question'	=> 408,
	            'answer' 			=> 'pilihan32a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 408,
	            'answer' 			=> 'pilihan32b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 408,
	            'answer' 			=> 'pilihan32c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 408,
	            'answer' 			=> 'pilihan32d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//33
			[
	            'id_bank_question'	=> 409,
	            'answer' 			=> 'pilihan33a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 409,
	            'answer' 			=> 'pilihan33b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 409,
	            'answer' 			=> 'pilihan33c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 409,
	            'answer' 			=> 'pilihan33d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//34
			[
	            'id_bank_question'	=> 410,
	            'answer' 			=> 'pilihan34a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 410,
	            'answer' 			=> 'pilihan34b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 410,
	            'answer' 			=> 'pilihan34c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 410,
	            'answer' 			=> 'pilihan34d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//35
			[
	            'id_bank_question'	=> 411,
	            'answer' 			=> 'pilihan35a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 411,
	            'answer' 			=> 'pilihan35b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 411,
	            'answer' 			=> 'pilihan35c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 411,
	            'answer' 			=> 'pilihan35d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//36
			[
	            'id_bank_question'	=> 412,
	            'answer' 			=> 'pilihan36a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 412,
	            'answer' 			=> 'pilihan36b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 412,
	            'answer' 			=> 'pilihan36c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 412,
	            'answer' 			=> 'pilihan36d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//37
			[
	            'id_bank_question'	=> 413,
	            'answer' 			=> 'pilihan37a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 413,
	            'answer' 			=> 'pilihan37b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 413,
	            'answer' 			=> 'pilihan37c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 413,
	            'answer' 			=> 'pilihan37d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//38
			[
	            'id_bank_question'	=> 414,
	            'answer' 			=> 'pilihan38a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 414,
	            'answer' 			=> 'pilihan38b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 414,
	            'answer' 			=> 'pilihan38c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 414,
	            'answer' 			=> 'pilihan38d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//39
			[
	            'id_bank_question'	=> 415,
	            'answer' 			=> 'pilihan39a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 415,
	            'answer' 			=> 'pilihan39b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 415,
	            'answer' 			=> 'pilihan39c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 415,
	            'answer' 			=> 'pilihan39d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//40
			[
	            'id_bank_question'	=> 416,
	            'answer' 			=> 'pilihan40a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 416,
	            'answer' 			=> 'pilihan40b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 416,
	            'answer' 			=> 'pilihan40c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 416,
	            'answer' 			=> 'pilihan40d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	]
        ]);
		
		//gabungan bagian
        DB::table('bank_option_answer')->insert([
        	//1
			[
	            'id_bank_question'	=> 417,
	            'answer' 			=> 'pilihan1a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 417,
	            'answer' 			=> 'pilihan1b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 417,
	            'answer' 			=> 'pilihan1c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 417,
	            'answer' 			=> 'pilihan1d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 417,
	            'answer' 			=> 'pilihan1e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 417,
	            'answer' 			=> 'pilihan1f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//2
			[
	            'id_bank_question'	=> 418,
	            'answer' 			=> 'pilihan2a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 418,
	            'answer' 			=> 'pilihan2b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 418,
	            'answer' 			=> 'pilihan2c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 418,
	            'answer' 			=> 'pilihan2d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 418,
	            'answer' 			=> 'pilihan2e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 418,
	            'answer' 			=> 'pilihan2f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//3
			[
	            'id_bank_question'	=> 419,
	            'answer' 			=> 'pilihan3a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 419,
	            'answer' 			=> 'pilihan3b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 419,
	            'answer' 			=> 'pilihan3c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 419,
	            'answer' 			=> 'pilihan3d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 419,
	            'answer' 			=> 'pilihan3e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 419,
	            'answer' 			=> 'pilihan3f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//4
			[
	            'id_bank_question'	=> 420,
	            'answer' 			=> 'pilihan4a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 420,
	            'answer' 			=> 'pilihan4b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 420,
	            'answer' 			=> 'pilihan4c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 420,
	            'answer' 			=> 'pilihan4d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 420,
	            'answer' 			=> 'pilihan4e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 420,
	            'answer' 			=> 'pilihan4f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//5
			[
	            'id_bank_question'	=> 421,
	            'answer' 			=> 'pilihan5a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 421,
	            'answer' 			=> 'pilihan5b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 421,
	            'answer' 			=> 'pilihan5c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 421,
	            'answer' 			=> 'pilihan5d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 421,
	            'answer' 			=> 'pilihan5e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 421,
	            'answer' 			=> 'pilihan5f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//6
			[
	            'id_bank_question'	=> 422,
	            'answer' 			=> 'pilihan6a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 422,
	            'answer' 			=> 'pilihan6b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 422,
	            'answer' 			=> 'pilihan6c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 422,
	            'answer' 			=> 'pilihan6d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 422,
	            'answer' 			=> 'pilihan6e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 422,
	            'answer' 			=> 'pilihan6f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//7
			[
	            'id_bank_question'	=> 423,
	            'answer' 			=> 'pilihan7a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 423,
	            'answer' 			=> 'pilihan7b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 423,
	            'answer' 			=> 'pilihan7c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 423,
	            'answer' 			=> 'pilihan7d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 423,
	            'answer' 			=> 'pilihan7e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 423,
	            'answer' 			=> 'pilihan7f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//8
			[
	            'id_bank_question'	=> 424,
	            'answer' 			=> 'pilihan8a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 424,
	            'answer' 			=> 'pilihan8b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 424,
	            'answer' 			=> 'pilihan8c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 424,
	            'answer' 			=> 'pilihan8d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 424,
	            'answer' 			=> 'pilihan8e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 424,
	            'answer' 			=> 'pilihan8f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//9
			[
	            'id_bank_question'	=> 425,
	            'answer' 			=> 'pilihan9a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 425,
	            'answer' 			=> 'pilihan9b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 425,
	            'answer' 			=> 'pilihan9c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 425,
	            'answer' 			=> 'pilihan9d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 425,
	            'answer' 			=> 'pilihan9e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 425,
	            'answer' 			=> 'pilihan9f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//10
			[
	            'id_bank_question'	=> 426,
	            'answer' 			=> 'pilihan10a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 426,
	            'answer' 			=> 'pilihan10b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 426,
	            'answer' 			=> 'pilihan10c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 426,
	            'answer' 			=> 'pilihan10d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 426,
	            'answer' 			=> 'pilihan10e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 426,
	            'answer' 			=> 'pilihan10f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//11
			[
	            'id_bank_question'	=> 427,
	            'answer' 			=> 'pilihan11a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 427,
	            'answer' 			=> 'pilihan11b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 427,
	            'answer' 			=> 'pilihan11c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 427,
	            'answer' 			=> 'pilihan11d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 427,
	            'answer' 			=> 'pilihan11e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 427,
	            'answer' 			=> 'pilihan11f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//12
			[
	            'id_bank_question'	=> 428,
	            'answer' 			=> 'pilihan12a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 428,
	            'answer' 			=> 'pilihan12b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 428,
	            'answer' 			=> 'pilihan12c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 428,
	            'answer' 			=> 'pilihan12d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 428,
	            'answer' 			=> 'pilihan12e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 428,
	            'answer' 			=> 'pilihan12f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//13
			[
	            'id_bank_question'	=> 429,
	            'answer' 			=> 'pilihan13a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 429,
	            'answer' 			=> 'pilihan13b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 429,
	            'answer' 			=> 'pilihan13c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 429,
	            'answer' 			=> 'pilihan13d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 429,
	            'answer' 			=> 'pilihan13e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 429,
	            'answer' 			=> 'pilihan13f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//14
			[
	            'id_bank_question'	=> 430,
	            'answer' 			=> 'pilihan14a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 430,
	            'answer' 			=> 'pilihan14b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 430,
	            'answer' 			=> 'pilihan14c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 430,
	            'answer' 			=> 'pilihan14d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 430,
	            'answer' 			=> 'pilihan14e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 430,
	            'answer' 			=> 'pilihan14f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//15
			[
	            'id_bank_question'	=> 431,
	            'answer' 			=> 'pilihan15a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 431,
	            'answer' 			=> 'pilihan15b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 431,
	            'answer' 			=> 'pilihan15c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 431,
	            'answer' 			=> 'pilihan15d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 431,
	            'answer' 			=> 'pilihan15e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 431,
	            'answer' 			=> 'pilihan15f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//16
			[
	            'id_bank_question'	=> 432,
	            'answer' 			=> 'pilihan16a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 432,
	            'answer' 			=> 'pilihan16b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 432,
	            'answer' 			=> 'pilihan16c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 432,
	            'answer' 			=> 'pilihan16d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 432,
	            'answer' 			=> 'pilihan16e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 432,
	            'answer' 			=> 'pilihan16f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//17
			[
	            'id_bank_question'	=> 433,
	            'answer' 			=> 'pilihan17a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 433,
	            'answer' 			=> 'pilihan17b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 433,
	            'answer' 			=> 'pilihan17c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 433,
	            'answer' 			=> 'pilihan17d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 433,
	            'answer' 			=> 'pilihan17e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 433,
	            'answer' 			=> 'pilihan17f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//18
			[
	            'id_bank_question'	=> 434,
	            'answer' 			=> 'pilihan18a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 434,
	            'answer' 			=> 'pilihan18b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 434,
	            'answer' 			=> 'pilihan18c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 434,
	            'answer' 			=> 'pilihan18d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 434,
	            'answer' 			=> 'pilihan18e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 434,
	            'answer' 			=> 'pilihan18f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//19
			[
	            'id_bank_question'	=> 435,
	            'answer' 			=> 'pilihan19a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 435,
	            'answer' 			=> 'pilihan19b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 435,
	            'answer' 			=> 'pilihan19c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 435,
	            'answer' 			=> 'pilihan19d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 435,
	            'answer' 			=> 'pilihan19e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 435,
	            'answer' 			=> 'pilihan19f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//10
			[
	            'id_bank_question'	=> 436,
	            'answer' 			=> 'pilihan20a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 436,
	            'answer' 			=> 'pilihan20b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 436,
	            'answer' 			=> 'pilihan20c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 436,
	            'answer' 			=> 'pilihan20d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 436,
	            'answer' 			=> 'pilihan20e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 436,
	            'answer' 			=> 'pilihan20f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//21
			[
	            'id_bank_question'	=> 437,
	            'answer' 			=> 'pilihan21a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 437,
	            'answer' 			=> 'pilihan21b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 437,
	            'answer' 			=> 'pilihan21c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 437,
	            'answer' 			=> 'pilihan21d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 437,
	            'answer' 			=> 'pilihan21e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 437,
	            'answer' 			=> 'pilihan21f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//22
			[
	            'id_bank_question'	=> 438,
	            'answer' 			=> 'pilihan22a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 438,
	            'answer' 			=> 'pilihan22b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 438,
	            'answer' 			=> 'pilihan22c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 438,
	            'answer' 			=> 'pilihan22d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 438,
	            'answer' 			=> 'pilihan22e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 438,
	            'answer' 			=> 'pilihan22f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//23
			[
	            'id_bank_question'	=> 439,
	            'answer' 			=> 'pilihan23a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 439,
	            'answer' 			=> 'pilihan23b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 439,
	            'answer' 			=> 'pilihan23c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 439,
	            'answer' 			=> 'pilihan23d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 439,
	            'answer' 			=> 'pilihan23e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 439,
	            'answer' 			=> 'pilihan23f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//24
			[
	            'id_bank_question'	=> 440,
	            'answer' 			=> 'pilihan24a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 440,
	            'answer' 			=> 'pilihan24b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 440,
	            'answer' 			=> 'pilihan24c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 440,
	            'answer' 			=> 'pilihan24d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 440,
	            'answer' 			=> 'pilihan24e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 440,
	            'answer' 			=> 'pilihan24f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//25
			[
	            'id_bank_question'	=> 441,
	            'answer' 			=> 'pilihan25a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 441,
	            'answer' 			=> 'pilihan25b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 441,
	            'answer' 			=> 'pilihan25c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 441,
	            'answer' 			=> 'pilihan25d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 441,
	            'answer' 			=> 'pilihan25e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 441,
	            'answer' 			=> 'pilihan25f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//26
			[
	            'id_bank_question'	=> 442,
	            'answer' 			=> 'pilihan26a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 442,
	            'answer' 			=> 'pilihan26b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 442,
	            'answer' 			=> 'pilihan26c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 442,
	            'answer' 			=> 'pilihan26d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 442,
	            'answer' 			=> 'pilihan26e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 442,
	            'answer' 			=> 'pilihan26f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	]
        ]);

		//hubungan kata
        DB::table('bank_option_answer')->insert([
        	//1
			[
	            'id_bank_question'	=> 443,
	            'answer' 			=> 'sedikit',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 443,
	            'answer' 			=> 'tepat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 443,
	            'answer' 			=> 'jernih',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 443,
	            'answer' 			=> 'banyak',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//2
			[
	            'id_bank_question'	=> 444,
	            'answer' 			=> 'pergi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 444,
	            'answer' 			=> 'berdiri',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 444,
	            'answer' 			=> 'datang',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 444,
	            'answer' 			=> 'beristirahat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//3
			[
	            'id_bank_question'	=> 445,
	            'answer' 			=> 'hasrat',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 445,
	            'answer' 			=> 'pengetahuan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 445,
	            'answer' 			=> 'keinginan',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 445,
	            'answer' 			=> 'kekuasaan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//4
			[
	            'id_bank_question'	=> 446,
	            'answer' 			=> 'jauh',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 446,
	            'answer' 			=> 'luas',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 446,
	            'answer' 			=> 'dekat',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 446,
	            'answer' 			=> 'nyaring',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//5
			[
	            'id_bank_question'	=> 447,
	            'answer' 			=> 'terik',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 447,
	            'answer' 			=> 'panas',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 447,
	            'answer' 			=> 'tawar',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 447,
	            'answer' 			=> 'sial',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//6
			[
	            'id_bank_question'	=> 448,
	            'answer' 			=> 'kelana',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 448,
	            'answer' 			=> 'pemberontak',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 448,
	            'answer' 			=> 'pelari',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 448,
	            'answer' 			=> 'pengembara',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//7
			[
	            'id_bank_question'	=> 449,
	            'answer' 			=> 'jasad',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 449,
	            'answer' 			=> 'ijazah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 449,
	            'answer' 			=> 'organisme',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 449,
	            'answer' 			=> 'alat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//8
			[
	            'id_bank_question'	=> 450,
	            'answer' 			=> 'kikir',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 450,
	            'answer' 			=> 'dermawan',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 450,
	            'answer' 			=> 'kurus',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 450,
	            'answer' 			=> 'mendewakan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//9
			[
	            'id_bank_question'	=> 451,
	            'answer' 			=> 'azas',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 451,
	            'answer' 			=> 'tamat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 451,
	            'answer' 			=> 'prinsip',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 451,
	            'answer' 			=> 'kekuasaan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//10
			[
	            'id_bank_question'	=> 452,
	            'answer' 			=> 'mencurigakan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 452,
	            'answer' 			=> 'malu',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 452,
	            'answer' 			=> 'haram',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 452,
	            'answer' 			=> 'halal',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//11
			[
	            'id_bank_question'	=> 453,
	            'answer' 			=> 'serasi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 453,
	            'answer' 			=> 'sesuai',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 453,
	            'answer' 			=> 'ceroboh',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 453,
	            'answer' 			=> 'kabur',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//12
			[
	            'id_bank_question'	=> 454,
	            'answer' 			=> 'menyala',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 454,
	            'answer' 			=> 'menyalak',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 454,
	            'answer' 			=> 'padam',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 454,
	            'answer' 			=> 'meradang',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//13
			[
	            'id_bank_question'	=> 455,
	            'answer' 			=> 'terpencil',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 455,
	            'answer' 			=> 'terpencar',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 455,
	            'answer' 			=> 'jinak',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 455,
	            'answer' 			=> 'terasing',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//14
			[
	            'id_bank_question'	=> 456,
	            'answer' 			=> 'ramah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 456,
	            'answer' 			=> 'lihai',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 456,
	            'answer' 			=> 'arif',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 456,
	            'answer' 			=> 'bijaksana',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//15
			[
	            'id_bank_question'	=> 457,
	            'answer' 			=> 'tongkat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 457,
	            'answer' 			=> 'taraf',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 457,
	            'answer' 			=> 'titik',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 457,
	            'answer' 			=> 'tingkat',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//16
			[
	            'id_bank_question'	=> 458,
	            'answer' 			=> 'megah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 458,
	            'answer' 			=> 'tegang',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 458,
	            'answer' 			=> 'malu',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 458,
	            'answer' 			=> 'kejang',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//17
			[
	            'id_bank_question'	=> 459,
	            'answer' 			=> 'untung',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 459,
	            'answer' 			=> 'jujur',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 459,
	            'answer' 			=> 'mujur',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 459,
	            'answer' 			=> 'urung',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//18
			[
	            'id_bank_question'	=> 460,
	            'answer' 			=> 'meradang',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 460,
	            'answer' 			=> 'murung',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 460,
	            'answer' 			=> 'mengampuni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 460,
	            'answer' 			=> 'gembira',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//19
			[
	            'id_bank_question'	=> 461,
	            'answer' 			=> 'lapang',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 461,
	            'answer' 			=> 'ruang',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 461,
	            'answer' 			=> 'panjang',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 461,
	            'answer' 			=> 'luas',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//20
			[
	            'id_bank_question'	=> 462,
	            'answer' 			=> 'memperbaiki',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 462,
	            'answer' 			=> 'menutupi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 462,
	            'answer' 			=> 'menyelubungi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 462,
	            'answer' 			=> 'mencari',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//21
			[
	            'id_bank_question'	=> 463,
	            'answer' 			=> 'sementara',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 463,
	            'answer' 			=> 'menggembalakan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 463,
	            'answer' 			=> 'menyeret',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 463,
	            'answer' 			=> 'kekal',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//22
			[
	            'id_bank_question'	=> 464,
	            'answer' 			=> 'nisbi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 464,
	            'answer' 			=> 'mutlak',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 464,
	            'answer' 			=> 'curiga',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 464,
	            'answer' 			=> 'malu',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//23
			[
	            'id_bank_question'	=> 465,
	            'answer' 			=> 'dispensasi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 465,
	            'answer' 			=> 'amnesti',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 465,
	            'answer' 			=> 'kelonggaran',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 465,
	            'answer' 			=> 'penyesalan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//24
			[
	            'id_bank_question'	=> 466,
	            'answer' 			=> 'marah',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 466,
	            'answer' 			=> 'melarat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 466,
	            'answer' 			=> 'gusar',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 466,
	            'answer' 			=> 'mengesalkan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//25
			[
	            'id_bank_question'	=> 467,
	            'answer' 			=> 'pengecut',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 467,
	            'answer' 			=> 'rendah hati',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 467,
	            'answer' 			=> 'kecil',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 467,
	            'answer' 			=> 'sombong',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//26
			[
	            'id_bank_question'	=> 468,
	            'answer' 			=> 'penyakit',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 468,
	            'answer' 			=> 'tobat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 468,
	            'answer' 			=> 'bisa',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 468,
	            'answer' 			=> 'racun',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//27
			[
	            'id_bank_question'	=> 469,
	            'answer' 			=> 'damai',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 469,
	            'answer' 			=> 'berseri',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 469,
	            'answer' 			=> 'tenang',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 469,
	            'answer' 			=> 'mulia',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//28
			[
	            'id_bank_question'	=> 470,
	            'answer' 			=> 'mengganggu',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 470,
	            'answer' 			=> 'mengangkat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 470,
	            'answer' 			=> 'mengangkut',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 470,
	            'answer' 			=> 'mengusik',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//29
			[
	            'id_bank_question'	=> 471,
	            'answer' 			=> 'menguji',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 471,
	            'answer' 			=> 'memuji',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 471,
	            'answer' 			=> 'menyanjung',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 471,
	            'answer' 			=> 'mendewakan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//30
			[
	            'id_bank_question'	=> 472,
	            'answer' 			=> 'vertikal',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 472,
	            'answer' 			=> 'lurus',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 472,
	            'answer' 			=> 'tegak lurus',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 472,
	            'answer' 			=> 'rata-rata',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//31
			[
	            'id_bank_question'	=> 473,
	            'answer' 			=> 'peraturan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 473,
	            'answer' 			=> 'kebiasaan',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 473,
	            'answer' 			=> 'kenangan',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 473,
	            'answer' 			=> 'adat',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//32
			[
	            'id_bank_question'	=> 474,
	            'answer' 			=> 'pendekar',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 474,
	            'answer' 			=> 'pelopor',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 474,
	            'answer' 			=> 'penyajak',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 474,
	            'answer' 			=> 'penyair',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//33
			[
	            'id_bank_question'	=> 475,
	            'answer' 			=> 'menarik',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 475,
	            'answer' 			=> 'asing',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 475,
	            'answer' 			=> 'aneh',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 475,
	            'answer' 			=> 'umum',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//34
			[
	            'id_bank_question'	=> 476,
	            'answer' 			=> 'bara',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 476,
	            'answer' 			=> 'suluh',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 476,
	            'answer' 			=> 'obor',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 476,
	            'answer' 			=> 'menara',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//35
			[
	            'id_bank_question'	=> 477,
	            'answer' 			=> 'cemar',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 477,
	            'answer' 			=> 'sedih',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 477,
	            'answer' 			=> 'cemas',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 477,
	            'answer' 			=> 'bersih',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//36
			[
	            'id_bank_question'	=> 478,
	            'answer' 			=> 'sedih',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 478,
	            'answer' 			=> 'lamban',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 478,
	            'answer' 			=> 'kasar',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 478,
	            'answer' 			=> 'hati-hati',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//37
			[
	            'id_bank_question'	=> 479,
	            'answer' 			=> 'penat',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 479,
	            'answer' 			=> 'kenyang',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 479,
	            'answer' 			=> 'penuh',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 479,
	            'answer' 			=> 'jenuh',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//38
			[
	            'id_bank_question'	=> 480,
	            'answer' 			=> 'tak bersemangat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 480,
	            'answer' 			=> 'ramah tamah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 480,
	            'answer' 			=> 'keranjingan',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 480,
	            'answer' 			=> 'berpantang',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//39
			[
	            'id_bank_question'	=> 481,
	            'answer' 			=> 'nila',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 481,
	            'answer' 			=> 'aib',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 481,
	            'answer' 			=> 'nista',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 481,
	            'answer' 			=> 'dosa',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//40
			[
	            'id_bank_question'	=> 482,
	            'answer' 			=> 'sombong',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 482,
	            'answer' 			=> 'terkendali',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 482,
	            'answer' 			=> 'lancang',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 482,
	            'answer' 			=> 'gila hormat',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        ]);

		//abstraksi non verbal
        DB::table('bank_option_answer')->insert([
        	//1
			[
	            'id_bank_question'	=> 483,
	            'answer' 			=> 'pilihan1a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 483,
	            'answer' 			=> 'pilihan1b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 483,
	            'answer' 			=> 'pilihan1c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 483,
	            'answer' 			=> 'pilihan1d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 483,
	            'answer' 			=> 'pilihan1e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 483,
	            'answer' 			=> 'pilihan1f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	//2
			[
	            'id_bank_question'	=> 484,
	            'answer' 			=> 'pilihan2a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 484,
	            'answer' 			=> 'pilihan2b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 484,
	            'answer' 			=> 'pilihan2c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 484,
	            'answer' 			=> 'pilihan2d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 484,
	            'answer' 			=> 'pilihan2e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 484,
	            'answer' 			=> 'pilihan2f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	//3
			[
	            'id_bank_question'	=> 485,
	            'answer' 			=> 'pilihan3a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 485,
	            'answer' 			=> 'pilihan3b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 485,
	            'answer' 			=> 'pilihan3c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 485,
	            'answer' 			=> 'pilihan3d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 485,
	            'answer' 			=> 'pilihan3e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 485,
	            'answer' 			=> 'pilihan3f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	
        	//4
			[
	            'id_bank_question'	=> 486,
	            'answer' 			=> 'pilihan4a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 486,
	            'answer' 			=> 'pilihan4b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 486,
	            'answer' 			=> 'pilihan4c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 486,
	            'answer' 			=> 'pilihan4d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 486,
	            'answer' 			=> 'pilihan4e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 486,
	            'answer' 			=> 'pilihan4f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	
        	//5
			[
	            'id_bank_question'	=> 487,
	            'answer' 			=> 'pilihan5a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 487,
	            'answer' 			=> 'pilihan5b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 487,
	            'answer' 			=> 'pilihan5c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 487,
	            'answer' 			=> 'pilihan5d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 487,
	            'answer' 			=> 'pilihan5e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 487,
	            'answer' 			=> 'pilihan5f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	
        	//6
			[
	            'id_bank_question'	=> 488,
	            'answer' 			=> 'pilihan6a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 488,
	            'answer' 			=> 'pilihan6b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 488,
	            'answer' 			=> 'pilihan6c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 488,
	            'answer' 			=> 'pilihan6d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 488,
	            'answer' 			=> 'pilihan6e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 488,
	            'answer' 			=> 'pilihan6f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	
        	//7
			[
	            'id_bank_question'	=> 489,
	            'answer' 			=> 'pilihan7a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 489,
	            'answer' 			=> 'pilihan7b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 489,
	            'answer' 			=> 'pilihan7c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 489,
	            'answer' 			=> 'pilihan7d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 489,
	            'answer' 			=> 'pilihan7e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 489,
	            'answer' 			=> 'pilihan7f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	
        	//8
			[
	            'id_bank_question'	=> 490,
	            'answer' 			=> 'pilihan8a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 490,
	            'answer' 			=> 'pilihan8b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 490,
	            'answer' 			=> 'pilihan8c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 490,
	            'answer' 			=> 'pilihan8d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 490,
	            'answer' 			=> 'pilihan8e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 490,
	            'answer' 			=> 'pilihan8f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	
        	//9
			[
	            'id_bank_question'	=> 491,
	            'answer' 			=> 'pilihan9a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'a'
        	],
        	[
	            'id_bank_question'	=> 491,
	            'answer' 			=> 'pilihan9b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'b'
        	],
        	[
	            'id_bank_question'	=> 491,
	            'answer' 			=> 'pilihan9c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'c'
        	],
        	[
	            'id_bank_question'	=> 491,
	            'answer' 			=> 'pilihan9d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'd'
        	],
        	[
	            'id_bank_question'	=> 491,
	            'answer' 			=> 'pilihan9e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=>  'e'
        	],
        	[
	            'id_bank_question'	=> 491,
	            'answer' 			=> 'pilihan9f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=>  'f'
        	],
        	
        	//10
			[
	            'id_bank_question'	=> 492,
	            'answer' 			=> 'pilihan10a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 492,
	            'answer' 			=> 'pilihan10b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 492,
	            'answer' 			=> 'pilihan10c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 492,
	            'answer' 			=> 'pilihan10d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 492,
	            'answer' 			=> 'pilihan10e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 492,
	            'answer' 			=> 'pilihan10f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//11
			[
	            'id_bank_question'	=> 493,
	            'answer' 			=> 'pilihan11a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 493,
	            'answer' 			=> 'pilihan11b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 493,
	            'answer' 			=> 'pilihan11c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 493,
	            'answer' 			=> 'pilihan11d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 493,
	            'answer' 			=> 'pilihan11e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 493,
	            'answer' 			=> 'pilihan11f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//12
			[
	            'id_bank_question'	=> 494,
	            'answer' 			=> 'pilihan12a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 494,
	            'answer' 			=> 'pilihan12b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 494,
	            'answer' 			=> 'pilihan12c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 494,
	            'answer' 			=> 'pilihan12d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 494,
	            'answer' 			=> 'pilihan12e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 494,
	            'answer' 			=> 'pilihan12f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//13
			[
	            'id_bank_question'	=> 495,
	            'answer' 			=> 'pilihan13a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 495,
	            'answer' 			=> 'pilihan13b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 495,
	            'answer' 			=> 'pilihan13c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 495,
	            'answer' 			=> 'pilihan13d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 495,
	            'answer' 			=> 'pilihan13e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 495,
	            'answer' 			=> 'pilihan13f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//14
			[
	            'id_bank_question'	=> 496,
	            'answer' 			=> 'pilihan14a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 496,
	            'answer' 			=> 'pilihan14b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 496,
	            'answer' 			=> 'pilihan14c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 496,
	            'answer' 			=> 'pilihan14d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 496,
	            'answer' 			=> 'pilihan14e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 496,
	            'answer' 			=> 'pilihan14f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//15
			[
	            'id_bank_question'	=> 497,
	            'answer' 			=> 'pilihan15a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 497,
	            'answer' 			=> 'pilihan15b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 497,
	            'answer' 			=> 'pilihan15c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 497,
	            'answer' 			=> 'pilihan15d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 497,
	            'answer' 			=> 'pilihan15e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 497,
	            'answer' 			=> 'pilihan15f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//16
			[
	            'id_bank_question'	=> 498,
	            'answer' 			=> 'pilihan16a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 498,
	            'answer' 			=> 'pilihan16b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 498,
	            'answer' 			=> 'pilihan16c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 498,
	            'answer' 			=> 'pilihan16d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 498,
	            'answer' 			=> 'pilihan16e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 498,
	            'answer' 			=> 'pilihan16f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//17
			[
	            'id_bank_question'	=> 499,
	            'answer' 			=> 'pilihan17a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 499,
	            'answer' 			=> 'pilihan17b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 499,
	            'answer' 			=> 'pilihan17c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 499,
	            'answer' 			=> 'pilihan17d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 499,
	            'answer' 			=> 'pilihan17e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 499,
	            'answer' 			=> 'pilihan17f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//18
			[
	            'id_bank_question'	=> 500,
	            'answer' 			=> 'pilihan18a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 500,
	            'answer' 			=> 'pilihan18b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 500,
	            'answer' 			=> 'pilihan18c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 500,
	            'answer' 			=> 'pilihan18d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 500,
	            'answer' 			=> 'pilihan18e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 500,
	            'answer' 			=> 'pilihan18f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//19
			[
	            'id_bank_question'	=> 501,
	            'answer' 			=> 'pilihan19a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 501,
	            'answer' 			=> 'pilihan19b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 501,
	            'answer' 			=> 'pilihan19c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 501,
	            'answer' 			=> 'pilihan19d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 501,
	            'answer' 			=> 'pilihan19e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 501,
	            'answer' 			=> 'pilihan19f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//20
			[
	            'id_bank_question'	=> 502,
	            'answer' 			=> 'pilihan20a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 502,
	            'answer' 			=> 'pilihan20b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 502,
	            'answer' 			=> 'pilihan20c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 502,
	            'answer' 			=> 'pilihan20d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 502,
	            'answer' 			=> 'pilihan20e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 502,
	            'answer' 			=> 'pilihan20f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//21
			[
	            'id_bank_question'	=> 503,
	            'answer' 			=> 'pilihan21a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 503,
	            'answer' 			=> 'pilihan21b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 503,
	            'answer' 			=> 'pilihan21c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 503,
	            'answer' 			=> 'pilihan21d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 503,
	            'answer' 			=> 'pilihan21e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 503,
	            'answer' 			=> 'pilihan21f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//22
			[
	            'id_bank_question'	=> 504,
	            'answer' 			=> 'pilihan22a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 504,
	            'answer' 			=> 'pilihan22b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 504,
	            'answer' 			=> 'pilihan22c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 504,
	            'answer' 			=> 'pilihan22d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 504,
	            'answer' 			=> 'pilihan22e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 504,
	            'answer' 			=> 'pilihan22f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	//23
			[
	            'id_bank_question'	=> 505,
	            'answer' 			=> 'pilihan23a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 505,
	            'answer' 			=> 'pilihan23b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 505,
	            'answer' 			=> 'pilihan23c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 505,
	            'answer' 			=> 'pilihan23d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 505,
	            'answer' 			=> 'pilihan23e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 505,
	            'answer' 			=> 'pilihan23f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//24
			[
	            'id_bank_question'	=> 506,
	            'answer' 			=> 'pilihan24a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 506,
	            'answer' 			=> 'pilihan24b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 506,
	            'answer' 			=> 'pilihan24c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 506,
	            'answer' 			=> 'pilihan24d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 506,
	            'answer' 			=> 'pilihan24e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 506,
	            'answer' 			=> 'pilihan24f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//25
			[
	            'id_bank_question'	=> 507,
	            'answer' 			=> 'pilihan25a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 507,
	            'answer' 			=> 'pilihan25b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 507,
	            'answer' 			=> 'pilihan25c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 507,
	            'answer' 			=> 'pilihan25d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 507,
	            'answer' 			=> 'pilihan25e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 507,
	            'answer' 			=> 'pilihan25f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//26
			[
	            'id_bank_question'	=> 508,
	            'answer' 			=> 'pilihan26a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 508,
	            'answer' 			=> 'pilihan26b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 508,
	            'answer' 			=> 'pilihan26c.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 508,
	            'answer' 			=> 'pilihan26d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 508,
	            'answer' 			=> 'pilihan26e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 508,
	            'answer' 			=> 'pilihan26f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//27
			[
	            'id_bank_question'	=> 509,
	            'answer' 			=> 'pilihan27a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 509,
	            'answer' 			=> 'pilihan27b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 509,
	            'answer' 			=> 'pilihan27c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 509,
	            'answer' 			=> 'pilihan27d.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 509,
	            'answer' 			=> 'pilihan27e.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 509,
	            'answer' 			=> 'pilihan27f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//28
			[
	            'id_bank_question'	=> 510,
	            'answer' 			=> 'pilihan28a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 510,
	            'answer' 			=> 'pilihan28b.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 510,
	            'answer' 			=> 'pilihan28c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 510,
	            'answer' 			=> 'pilihan28d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 510,
	            'answer' 			=> 'pilihan28e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 510,
	            'answer' 			=> 'pilihan28f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//29
			[
	            'id_bank_question'	=> 511,
	            'answer' 			=> 'pilihan29a.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 511,
	            'answer' 			=> 'pilihan29b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 511,
	            'answer' 			=> 'pilihan29c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 511,
	            'answer' 			=> 'pilihan29d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 511,
	            'answer' 			=> 'pilihan29e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 511,
	            'answer' 			=> 'pilihan29f.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	],
        	
        	//30
			[
	            'id_bank_question'	=> 512,
	            'answer' 			=> 'pilihan30a.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 512,
	            'answer' 			=> 'pilihan30b.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 512,
	            'answer' 			=> 'pilihan30c.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 512,
	            'answer' 			=> 'pilihan30d.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 512,
	            'answer' 			=> 'pilihan30e.png',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	[
	            'id_bank_question'	=> 512,
	            'answer' 			=> 'pilihan30f.png',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'f'
        	]
        ]);

    }
}
