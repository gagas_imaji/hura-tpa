<?php

use Illuminate\Database\Seeder;

class LookupDISCSistematikaKerjaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_disc_sistematika_kerja')->insert(array(
        	array('hasil_disc'=>'C','nilai'=>5),
        	array('hasil_disc'=>'CD','nilai'=>4),
        	array('hasil_disc'=>'CDI','nilai'=>4),
        	array('hasil_disc'=>'CDS','nilai'=>4),
        	array('hasil_disc'=>'CI','nilai'=>4),
        	array('hasil_disc'=>'CID','nilai'=>4),
        	array('hasil_disc'=>'CIS','nilai'=>4),
        	array('hasil_disc'=>'CS','nilai'=>5),
        	array('hasil_disc'=>'CSD','nilai'=>5),
        	array('hasil_disc'=>'CSI','nilai'=>4), 
        	array('hasil_disc'=>'D','nilai'=>3), 
        	array('hasil_disc'=>'DC','nilai'=>4), 
        	array('hasil_disc'=>'DCI','nilai'=>3), 
        	array('hasil_disc'=>'DCS','nilai'=>3), 
        	array('hasil_disc'=>'DI','nilai'=>2), 
        	array('hasil_disc'=>'DIC','nilai'=>3), 
        	array('hasil_disc'=>'DIS','nilai'=>3), 
        	array('hasil_disc'=>'DS','nilai'=>2), 
        	array('hasil_disc'=>'DSC','nilai'=>4), 
        	array('hasil_disc'=>'DSI','nilai'=>3), 
        	array('hasil_disc'=>'I','nilai'=>2),
        	array('hasil_disc'=>'IC','nilai'=>3), 
        	array('hasil_disc'=>'ICD','nilai'=>3), 
        	array('hasil_disc'=>'ICS','nilai'=>3), 
        	array('hasil_disc'=>'ID','nilai'=>3), 
        	array('hasil_disc'=>'IDC','nilai'=>4), 
        	array('hasil_disc'=>'IDS','nilai'=>3), 
        	array('hasil_disc'=>'IS','nilai'=>2), 
        	array('hasil_disc'=>'ISC','nilai'=>3), 
        	array('hasil_disc'=>'ISD','nilai'=>3), 
        	array('hasil_disc'=>'S','nilai'=>4),
        	array('hasil_disc'=>'SC','nilai'=>4), 
        	array('hasil_disc'=>'SCD','nilai'=>5),
        	array('hasil_disc'=>'SCI','nilai'=>4), 
        	array('hasil_disc'=>'SD','nilai'=>3), 
        	array('hasil_disc'=>'SDC','nilai'=>4), 
        	array('hasil_disc'=>'SDI','nilai'=>3), 
        	array('hasil_disc'=>'SI','nilai'=>3), 
        	array('hasil_disc'=>'SIC','nilai'=>4), 
        	array('hasil_disc'=>'SID','nilai'=>3)
        ));
    }
}
