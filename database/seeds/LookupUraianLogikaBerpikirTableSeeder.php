<?php

use Illuminate\Database\Seeder;

class LookupUraianLogikaBerpikirTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_logika_berpikir')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'Ia memiliki kemampuan berpikir yang sederhana sehingga terbatas pada hal-hal konkrit dan praktis.',
	            'desc2' => NULL,
	            'desc3' => NULL
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'Ia  memiliki kemampuan berpikir secara abstrak dan menyelesaikan masalah-masalah sederhana berdasarkan pengalamannya.',
	            'desc2' => NULL,
	            'desc3' => NULL
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'Ia mampu menyerap informasi dengan baik serta membuat berbagai alternatif pemecahannya.',
	            'desc2' => NULL,
	            'desc3' => NULL
        	],
            [
                'nilai' => 5,
                'desc1' => 'Ia mampu menguraikan informasi secara sistematis sehingga dapat menemukan hubungan sebab akibat dari suatu permasalahan',
                'desc2' => NULL,
                'desc3' => NULL
            ],
            [
                'nilai' => 6,
                'desc1' => 'Ia  mampu menggunakan pemikirannya untuk memberikan hipotesa atau perkiraan terbaik, dan secara sistematis menyimpulkan langkah-langkah terbaik guna pemecahan masalah. ',
                'desc2' => NULL,
                'desc3' => NULL
            ]

        ]);
    }
}
