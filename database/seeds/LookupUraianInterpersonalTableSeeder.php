<?php

use Illuminate\Database\Seeder;

class LookupUraianInterpersonalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_interpersonal')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'memiliki minat sosial yang rendah, sehingga ia  kurang suka dalam menjalin interaksi dan berkomunikasi dengan orang lain.',
	            'desc2' => 'kurang peka dan kurang terampil dalam berinteraksi. '
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'menunjukkan minat sosialnya tidak terlalu besar, komunikasi efektif hanya di lingkungan yang terbatas.',
	            'desc2' => 'memerlukan waktu untuk menjalin hubungan sosial meskipun sudah mampu memenuhi harapan lingkungan.'
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'memiliki minat sosial yang memadai. Ia mampu berinteraksi secara wajar dan diterima oleh lingkungan sosialnya.',
	            'desc2' => 'mampu memenuhi tuntutan sosial yang diharapkan dan menjalin relasi secara umum.'
        	],
            [
                'nilai' => 5,
                'desc1' => 'cukup peka terhadap kebutuhan orang lain, mampu berinteraksi secara cukup baik dan bisa diterima oleh lingkungan sosialnya.',
                'desc2' => 'orientasi terhadap kebutuhan interpersonalnya baik, responsif terhadap tuntutan dan norma lingkungan.'
            ],
            [
                'nilai' => 6,
                'desc1' => 'tanggap terhadap kebutuhan orang lain, dan bisa menyesuaikan pendekatannya secara efektif sehingga bisa diterima dengan baik oleh lingkungannya.',
                'desc2' => 'orientasi sosial yang positif diimbangi rasa percaya diri dan  kemampuan membangun hubungan yang harmonis.'
            ]

        ]);
    }
}
