<?php

use Illuminate\Database\Seeder;

class BankSurveyGTISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_survey')->insert([
        	
        	[
	            'category_survey_id' 	=> 1,
	            'title' 				=> 'Learning Agility Index',
	            'slug'					=> 'learning-agility-index', 
	            'description'			=> '-',
	            'instruction'			=> 'Tes ini memberikan gambaran ketangkasan dalam hal perilaku, kemampuan dan ambisi. Test ini terdiri dari 5 Subtest yang masing-masing memiliki instruksi dan waktu pengerjaan yang berbeda-beda',
	            'thankyou_text'			=> 'Terima kasih telah menyelesaikan Test ini',
	            'is_random'				=> 1,
	            'timer'					=> 0
        	]
        ]);
    }
}
