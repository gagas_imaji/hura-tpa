<?php

use Illuminate\Database\Seeder;

class BankQuestionTikiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
	    //berhitung angka
    	for($i = 1; $i <= 40; $i++){
	        DB::table('bank_question')->insert([
	        	[
		            'id_type_question' 	=> 1,
		            'id_bank_section' 	=> 13,
		            'question'			=> '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" class="tiki1soal'.$i.'" />',
		            'is_random'			=> 0,
		            'timer'				=> 0,
		            'is_mandatory'		=> 1,
		            'number'			=> $i
	        	]
	        ]);
    	}

	    //gabungan bagian
    	for($i = 1; $i <= 26; $i++){
	        DB::table('bank_question')->insert([
	        	[
		            'id_type_question' 	=> 2,
		            'id_bank_section' 	=> 14,
		            'question'			=> '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" class="tiki2soal'.$i.'" />',
		            'is_random'			=> 0,
		            'timer'				=> 0,
		            'is_mandatory'		=> 1,
		            'number'			=> $i
	        	]
	        ]);
    	}

    	//hubungan kata
    	for($i = 1; $i <= 40; $i++){
	        DB::table('bank_question')->insert([
	        	[
		            'id_type_question' 	=> 2,
		            'id_bank_section' 	=> 15,
		            'question'			=> '',
		            'is_random'			=> 0,
		            'timer'				=> 0,
		            'is_mandatory'		=> 1,
		            'number'			=> $i
	        	]
	        ]);
    	}

    	//abstraksi non verbal
    	for($i = 1; $i <= 30; $i++){
	        DB::table('bank_question')->insert([
	        	[
		            'id_type_question' 	=> 2,
		            'id_bank_section' 	=> 16,
		            'question'			=> '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAABVAQMAAACFL4Q2AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABxJREFUeNrtwYEAAAAAw6D5U1/gCFUBAAAAAPAMFUAAAYQO3CcAAAAASUVORK5CYII=" class="tiki4soal'.$i.'" />',
		            'is_random'			=> 0,
		            'timer'				=> 0,
		            'is_mandatory'		=> 1,
		            'number'			=> $i
	        	]
	        ]);
    	}
    }
}
