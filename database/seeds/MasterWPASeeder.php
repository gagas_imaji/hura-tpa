<?php

use Illuminate\Database\Seeder;

class MasterWPASeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_wpa')->insert([
        	[
	        	'number'		=> 1,
	        	'statement'		=> 'Mudah bergaul, menyenangkan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 1,
	        	'statement'		=> 'Mudah percaya kepada orang lain',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 1,
	        	'statement'		=> 'Suka berpetualang, pengambil resiko',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 1,
	        	'statement'		=> 'Penuh toleransi, menghormati orang lain',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> 'C'
        	],
        	//
        	[
	        	'number'		=> 2,
	        	'statement'		=> 'Berbicara lembut, pendiam/penyendiri',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 2,
	        	'statement'		=> 'Optimis, berfikir positif, memiliki visi/tujuan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 2,
	        	'statement'		=> 'Pusat perhatian, mudah bersosialisasi',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 2,
	        	'statement'		=> 'Pendamai, pembawa keharmonisan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	//
        	[
	        	'number'		=> 3,
	        	'statement'		=> 'Memberikan dorongan kepada orang lain',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 3,
	        	'statement'		=> 'Berusaha untuk selalu sempurna',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	[
	        	'number'		=> 3,
	        	'statement'		=> 'Menjadi bagian dari sebuah kelompok/tim',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 3,
	        	'statement'		=> 'Ingin mencapai tujuan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> '*'
        	],
        	//
        	[
	        	'number'		=> 4,
	        	'statement'		=> 'Mudah menjadi frustasi',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> 'C'
        	],
        	[
	        	'number'		=> 4,
	        	'statement'		=> 'Memendam perasaan, tertutup',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 4,
	        	'statement'		=> 'Menyampaikan pendapatnya, terbuka',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 4,
	        	'statement'		=> 'Berani menghadapi pihak oposisi',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	//
        	[
	        	'number'		=> 5,
	        	'statement'		=> 'Penuh semangat, banyak bicara',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 5,
	        	'statement'		=> 'Bertindak cepat, tegas',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 5,
	        	'statement'		=> 'Mencoba untuk menjaga kedamaian',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 5,
	        	'statement'		=> 'Mencoba untuk mengikuti aturan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	//
        	[
	        	'number'		=> 6,
	        	'statement'		=> 'Mengatur waktu dengan baik',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 6,
	        	'statement'		=> 'Seringkali terburu-buru, merasa tertekan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 6,
	        	'statement'		=> 'Berhubungan dengan orang lain adalah penting',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 6,
	        	'statement'		=> 'Senang menyelesaikan hal yang telah dimulai',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	//
        	[
	        	'number'		=>	7,
	        	'statement'		=> 'Menolak perubahan yang mendadak',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=>	7,
	        	'statement'		=> 'Cenderung terlalu banyak berjanji',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=>	7,
	        	'statement'		=> 'Menarik diri ketika di bawah tekanan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	[
	        	'number'		=>	7,
	        	'statement'		=> 'Tidak takut untuk konfrontasi langsung',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'D'
        	],
        	//
        	[
	        	'number'		=> 8,
	        	'statement'		=> 'Pendorong, pemberi semangat yang baik',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 8,
	        	'statement'		=> 'Pendengar yang baik',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 8,
	        	'statement'		=> 'Penganalisis yang baik',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> 'C'
        	],
        	[
	        	'number'		=> 8,
	        	'statement'		=> 'Pendelegasi yang baik',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	//
        	[
	        	'number'		=> 9,
	        	'statement'		=> 'Hasil adalah segalanya',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 9,
	        	'statement'		=> 'Lakukan dengan benar, ketepatan adalah penting',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> 'C'
        	],
        	[
	        	'number'		=> 9,
	        	'statement'		=> 'Buatlah sesuatu menjadi menyenangkan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 9,
	        	'statement'		=> 'Mari lakukan bersama-sama',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'S'
        	],
        	//
        	[
	        	'number'		=> 10,
	        	'statement'		=> 'Tidak tergantung orang lain',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	[
	        	'number'		=> 10,
	        	'statement'		=> 'Akan membeli mengikuti dorongan hati',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 10,
	        	'statement'		=> 'Akan menunggu dengan sabar',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 10,
	        	'statement'		=> 'Akan mengeluarkan uang untuk hal yang diinginkan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> '*'
        	],
        	//
        	[
	        	'number'		=> 11,
	        	'statement'		=> 'Ramah, mudah berteman',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 11,
	        	'statement'		=> 'Unik, mudah bosan terhadap rutinitas',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 11,
	        	'statement'		=> 'Aktif mengubah sesuatu',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 11,
	        	'statement'		=> 'Menginginkan sesuatu yang pasti',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> 'C'
        	],
        	//
        	[
	        	'number'		=> 12,
	        	'statement'		=> 'Tidak melawan, mengalah',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 12,
	        	'statement'		=> 'Menyukai hal rinci/detil',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 12,
	        	'statement'		=> 'Berubah di saat-saat terakhir',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 12,
	        	'statement'		=> 'Penuntuk, kasar',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	//
        	[
	        	'number'		=> 13,
	        	'statement'		=> 'Ingin maju',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 13,
	        	'statement'		=> 'Puas dengan apa yang ada, puas hati',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 13,
	        	'statement'		=> 'Terbuka mengungkapkan perasaan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 13,
	        	'statement'		=> 'Rendah hati, sederhana',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	//
        	[
	        	'number'		=> 14,
	        	'statement'		=> 'Tenang, suka menyendiri/pendiam',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> 'C'
        	],
        	[
	        	'number'		=> 14,
	        	'statement'		=> 'Gembira, periang',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 14,
	        	'statement'		=> 'Menyenangkan, ramah',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 14,
	        	'statement'		=> 'Tegas, berani',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	//
        	[
	        	'number'		=> 15,
	        	'statement'		=> 'Menghabiskan waktu dengan orang lain',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 15,
	        	'statement'		=> 'Merencanakan masa depan, penuh persiapan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 15,
	        	'statement'		=> 'Mencari tantangan baru',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 15,
	        	'statement'		=> 'Menerima penghargaan untuk tujuan yang tercapai',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	//
        	[
	        	'number'		=> 16,
	        	'statement'		=> 'Peraturan perlu diuji',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 16,
	        	'statement'		=> 'Peraturan membuat adil',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 16,
	        	'statement'		=> 'Peraturan membuat bosan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 16,
	        	'statement'		=> 'Peraturan membuat aman',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	//
        	[
	        	'number'		=> 17,
	        	'statement'		=> 'Pendidikan, budaya',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	[
	        	'number'		=> 17,
	        	'statement'		=> 'Prestasi, penghargaan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 17,
	        	'statement'		=> 'Keselamatan, keamanan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 17,
	        	'statement'		=> 'Bergaul, berkumpul dengan kelompok',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> '*'
        	],
        	//
        	[
	        	'number'		=> 18,
	        	'statement'		=> 'Memimpin, bertanggung jawab, pendekatan langsung',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 18,
	        	'statement'		=> 'Terbuka, antusias, bersemangat',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 18,
	        	'statement'		=> 'Mudah ditebak, konsisten',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 18,
	        	'statement'		=> 'Berhati-hati, waspada',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> '*'
        	],
        	//
        	[
	        	'number'		=> 19,
	        	'statement'		=> 'Tidak mudah dikalahkan/ditundukkan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 19,
	        	'statement'		=> 'Mengikuti keinginan/perintah pemimpin',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 19,
	        	'statement'		=> 'Bersemangat, periang',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 19,
	        	'statement'		=> 'Ingin teratur/rapi',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	//
        	[
	        	'number'		=> 20,
	        	'statement'		=> 'Saya akan memimpin orang lain',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 20,
	        	'statement'		=> 'Saya akan melaksanakannya',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 20,
	        	'statement'		=> 'Saya akan meyakinkan orang lain',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 20,
	        	'statement'		=> 'Saya akan mendapatkan faktanya',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> '*'
        	],
        	//
        	[
	        	'number'		=> 21,
	        	'statement'		=> 'Mendahulukan kepentingan orang lain',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 21,
	        	'statement'		=> 'Suka bersaing, suka tantangan, kompetitif',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 21,
	        	'statement'		=> 'Optimis, berfikir positif',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 21,
	        	'statement'		=> 'Berfikir logis, sistematis',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	//
        	[
	        	'number'		=> 22,
	        	'statement'		=> 'Menyenangkan orang, ramah',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 22,
	        	'statement'		=> 'Tertawa dengan keras, hidup',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 22,
	        	'statement'		=> 'Berani, tegas',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 22,
	        	'statement'		=> 'Pendiam/suka menyendiri',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> 'C'
        	],
        	//
        	[
	        	'number'		=> 23,
	        	'statement'		=> 'Menginginkan otoritas yang lebih',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'D'
        	],
        	[
	        	'number'		=> 23,
	        	'statement'		=> 'Menginginkan kesempatan baru',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 23,
	        	'statement'		=> 'Menghindari konflik',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'S',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 23,
	        	'statement'		=> 'Menginginkan arahan yang jelas',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'C'
        	],
        	//
        	[
	        	'number'		=> 24,
	        	'statement'		=> 'Dapat dipercaya/diandalkan',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> '*',
	        	'L'				=> 'S'
        	],
        	[
	        	'number'		=> 24,
	        	'statement'		=> 'Kreatif, unik',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'I',
	        	'L'				=> 'I'
        	],
        	[
	        	'number'		=> 24,
	        	'statement'		=> 'Berorientasi pada hasil',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'D',
	        	'L'				=> '*'
        	],
        	[
	        	'number'		=> 24,
	        	'statement'		=> 'Memegang standar yang tinggi, teliti',
	        	//'slug_section'	=> 'personality-profile-assessment',
	        	'M'				=> 'C',
	        	'L'				=> '*'
        	],
        	//
        ]);
    }
}
