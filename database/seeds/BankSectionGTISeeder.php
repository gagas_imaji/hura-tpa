<?php

use Illuminate\Database\Seeder;

class BankSectionGTISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_section')->insert([
            [
                'bank_survey_id'    => 3,
                'name'              => 'Subtest 1',
                'slug'              => 'subtest-1',
                'instruction'       => 'Tentukan ada berapa pasang huruf yang sama antara bagian atas dan bawah pada setiap soal.<br>Bekerjalah secepat dan seteliti mungkin karena waktunya sangat terbatas!',
                'timer'             => '180', //180
                'limit'             => '0'
            ], 
            [
                'bank_survey_id'    => 3,
                'name'              => 'Subtest 2',
                'slug'              => 'subtest-2',
                'instruction'       => 'Pilihlah satu jawaban yang benar sesuai pernyataan yang telah disediakan.<br/>Bekerjalah secepat dan seteliti mungkin karena waktunya sangat terbatas!',
                'timer'             => '240', //240
                'limit'             => '0'
            ], 
            [
                'bank_survey_id'    => 3,
                'name'              => 'Subtest 3',
                'slug'              => 'subtest-3',
                'instruction'       => 'Carilah selisih terjauh dari huruf yang berada di tengah dengan huruf yang berada di sebelah kiri atau kanan.<br/>Bekerjalah secepat dan seteliti mungkin karena waktunya sangat terbatas!',
                'timer'             => '240', //240
                'limit'             => '0'
            ], 
            [
                'bank_survey_id'    => 3,
                'name'              => 'Subtest 4',
                'slug'              => 'subtest-4',
                'instruction'       => 'Tentukan angka tertinggi dan terendah dari setiap perangkat bilangan yang terdiri dari 3 angka. Kemudian pilihlah dimana angka yang terjauh selisihnya dari angka yang berada di tengah.<br/>Bekerjalah secepat dan seteliti mungkin karena waktunya sangat terbatas!',
                'timer'             => '240', //240
                'limit'             => '0'
            ], 
            [
                'bank_survey_id'    => 3,
                'name'              => 'Subtest 5',
                'slug'              => 'subtest-5',
                'instruction'       => 'Berapa banyak bentuk-bentuk pada gambar yang sama seperti bentuk dasarnya (F) setelah bentuk-bentuk tersebut diputar.<br/>Bekerjalah secepat dan seteliti mungkin karena waktunya sangat terbatas!',
                'timer'             => '300', //300
                'limit'             => '0'
            ]
        ]);
    }
}
