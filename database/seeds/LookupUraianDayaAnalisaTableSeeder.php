<?php

use Illuminate\Database\Seeder;

class LookupUraianDayaAnalisaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_daya_analisa')->insert([
			[
	            'nilai' => 2,
                'desc1' => 'Ia membutuhkan waktu relatif lama untuk mempelajari hal baru.',
	            'desc2' => 'Ia membutuhkan waktu untuk menyerap dan menganalisa informasi baru.'
        	],
        	[
	            'nilai' => 3,
                'desc1' => 'Ia masih mampu berpikir secara logis dan menganalisa masalah yang dihadapi dalam pekerjaannya sehari-hari serta berusaha mencari solusi berdasarkan pengalaman yang dimilikinya.',
	            'desc2' => 'Pendekatan berpikirnya  terbatas hanya pada pemikiran logika sederhana dan praktis.'
        	],
        	[
	            'nilai' => 4,
                'desc1' => 'Ia mampu melakukan analisa secara proporsional untuk memecahkan masalah yang dihadapi secara memadai.',
	            'desc2' => 'Pendekatan berpikirnya  terbatas hanya pada pemikiran logika sederhana dan praktis.'
        	],
            [
                'nilai' => 5,
                'desc1' => 'Ia mampu menganalisa situasi dalam pekerjaan dengan cepat, memberikan solusi beragam serta daya nalarnya dapat diandalkan.',
                'desc2' => 'Ia dapat diandalkan untuk menganalisa masalah yang kompleks dan mampu mencari solusi yang beragam.'
            ],
            [
                'nilai' => 6,
                'desc1' => 'Ia dapat diandalkan untuk menganalisa masalah yang kompleks dan mampu mencari solusi kreatif. Ia juga mampu berpikir secara antisipatif.',
                'desc2' => 'Ia mampu memahami suatu konsep yang kompleks serta mampu menemukan solusi yang efektif untuk mengatasi berbagai masalah.'
            ]

        ]);
    }
}
