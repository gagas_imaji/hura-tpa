<?php

use Illuminate\Database\Seeder;

class LookupMatriksAkhirTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_matriks_akhir')->insert(array(
        	array('xy'=>2,'2'=>2,'3'=>3,'4'=>3,'5'=>3,'6'=>4),
        	array('xy'=>3,'2'=>3,'3'=>3,'4'=>3,'5'=>4,'6'=>4),
        	array('xy'=>4,'2'=>3,'3'=>3,'4'=>4,'5'=>4,'6'=>5),
        	array('xy'=>5,'2'=>3,'3'=>4,'4'=>4,'5'=>5,'6'=>5),
        	array('xy'=>6,'2'=>4,'3'=>4,'4'=>5,'5'=>5,'6'=>6),
        ));
    }
}
