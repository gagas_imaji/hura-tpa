<?php

use Illuminate\Database\Seeder;

class WPAKekuatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('lookup_wpa_kekuatan')->insert([
        	[
        		'D' => 	'Percaya diri',
	        	'I'	=>	'Hangat dan bersemangat',
	        	'S'	=>	'Tenang, cinta damai',
	        	'C'	=>	'Konservatif'
        	],
        	[
        		'D' => 	'Berkemauan keras',
	        	'I'	=>	'Berkharisma',
	        	'S'	=>	'Ramah',
	        	'C'	=>	'Pemikir'
        	],
        	[
        		'D' => 	'Tekun dan ulet',
	        	'I'	=>	'Antusias',
	        	'S'	=>	'Sopan santun',
	        	'C'	=>	'Sadar akan mutu'
        	],
        	[
        		'D' => 	'Berani dan tegar',
	        	'I'	=>	'Ekspresif',
	        	'S'	=>	'Baik hati/ lembut hati',
	        	'C'	=>	'Mematuhi aturan'
        	],
        	[
        		'D' => 	'Menghadapi hidup tanpa kompromi',
	        	'I'	=>	'Aktif',
	        	'S'	=>	'Tidak emosional',
	        	'C'	=>	'Diplomatis'
        	],
        	[
        		'D' => 	'Organisator dan promotor',
	        	'I'	=>	'Enerjik',
	        	'S'	=>	'Sabar',
	        	'C'	=>	'Waspada'
        	],
        	[
        		'D' => 	'Cepat bertindak',
	        	'I'	=>	'Meyakinkan',
	        	'S'	=>	'Setia',
	        	'C'	=>	'Perfeksionis'
        	],
        	[
        		'D' => 	'Berani memutuskan dalam keadaan mendesak',
	        	'I'	=>	'Memberikan semangat',
	        	'S'	=>	'Tekun',
	        	'C'	=>	'Senang pada detail'
        	],
        	[
        		'D' => 	'Mampu memberikan solusi',
	        	'I'	=>	'Hangat',
	        	'S'	=>	'Praktis, sederhana dalam bekerja',
	        	'C'	=>	'Senang pada rincian yang rumit'
        	],
        	[
        		'D' => 	'Memiliki inisiatif',
	        	'I'	=>	'Aktif berbicara',
	        	'S'	=>	'Keterampilan berinteraksi dengan orang lain',
	        	'C'	=>	'Mampu mengorganisasikan tugas'
        	],
        	[
        		'D' => 	'Mampu bekerja dengan cepat',
	        	'I'	=>	'Senang membina hubungan sosial dengan orang lain',
	        	'S'	=>	'Mampu melibatkan orang lain',
	        	'C'	=>	'Memiliki perencanaan jangka panjang'
        	],
        	[
        		'D' => 	'Memotivasi orang lain untuk bekerja',
	        	'I'	=>	'Penampilan yang modis',
	        	'S'	=>	'Menggunakan prinsip keseimbangan antara bekerja dan keluarga',
	        	'C'	=>	'Menentukan standar yang tinggi dan ideal'
        	],
        ]);
    }
}
