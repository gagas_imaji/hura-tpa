<?php

use Illuminate\Database\Seeder;

class JobAnalysisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_job_analysis')->insert([
            [    
                'questionnaire' => 'Bekerja langsung dibawah pengawasan (supervisi langsung)',
                'key'           => 'C',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Harus mampu berinteraksi secara mantap dengan orang yang baru dikenal',
                'key'           => 'I',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Persisten dalam mengikuti pola-pola kerja atau prosedur yang ditetapkan',
                'key'           => 'S',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Memiliki kemampuan untuk memotivasi orang lain',
                'key'           => 'I',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Harus berada terus menerus pada satu lokasi atau satu area',
                'key'           => 'S',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Mampu membujuk atau mempengaruhi orang lain',
                'key'           => 'I',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Terampil untuk mengerjakan pekerjaan yang berulang-ulang',
                'key'           => 'C',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Harus dapat menangani keberatan / penolakan',
                'key'           => 'D',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Mampu mengambil keputusan tanpa ada pola atau contoh sebelumnya',
                'key'           => 'D',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Harus mengikuti sistem dan prosedur secara tepat',
                'key'           => 'C',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Harus memiliki kemampuan mengutarakan sesuatu secara persuasif',
                'key'           => 'I',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Posisi ini memerlukan ketekunan untuk menyelesaikan tugas-tugas rutin',
                'key'           => 'S',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Posisi ini menuntut orang untuk puas/betah pada satu situasi kerja',
                'key'           => 'S',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => '"Membantu" orang lain dalam memecahkan persoalan yang sedang dihadapinya',
                'key'           => 'I',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Memerlukan kehati-hatian dan mempertimbangkan berbagai resiko',
                'key'           => 'C',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Menunjukkan sikap yang tidak menyebabkan permusuhan dan kooperatif',
                'key'           => 'C',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Posisi ini menuntut kreatifitas & mampu menghasilkan sesuatu yang baru',
                'key'           => 'D',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Terampil memusatkan perhatian pada hal-hal yang rinci dan detail',
                'key'           => 'C',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Membuat keputusan yang tidak populer dalam melaksanakan tugas',
                'key'           => 'D',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Konsisten dalam melaksanakan pekerjaan rutin',
                'key'           => 'S',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Memiliki kemampuan mengorganisir beberapa tipe atau karakter orang lain',
                'key'           => 'I',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Memerlukan kehati-hatian bila membuat komitmen terhadap kebijakan',
                'key'           => 'C',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Memiliki pandangan untuk merencanakan jauh ke depan, membuat visi dan target',
                'key'           => 'D',
                'created_at'	=> \Carbon\Carbon::now()
            ],
            [    
                'questionnaire' => 'Harus terampil menangani keluhan',
                'key'           => 'D',
                'created_at'	=> \Carbon\Carbon::now()
            ]
        ]);
    }
}
