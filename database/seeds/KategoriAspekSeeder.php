<?php

use Illuminate\Database\Seeder;

class KategoriAspekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori_aspeks')->insert(array(
                array('nama'=>'KEMAMPUAN INTELEKTUAL','slug'=>'kemampuan-intelektual'),
                array('nama'=>'SIKAP DAN CARA KERJA','slug'=>'sikap-dan-cara-kerja'),
                array('nama'=>'KEPRIBADIAN','slug'=>'kepribadian'),
          ));
    }
}
