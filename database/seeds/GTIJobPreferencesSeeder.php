<?php

use Illuminate\Database\Seeder;

class GTIJobPreferencesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_gti_job_preferences')->insert([
        	[
        		'match'		=> 1,
        		'job'		=> 'Clerical'
        	],
        	[
        		'match'		=> 1,
        		'job'		=> 'Secretarial'
        	],
        	[
        		'match'		=> 1,
        		'job'		=> 'Various Administration Tasks'
        	],
        	[
        		'match'		=> 1,
        		'job'		=> 'Sales & Customer Support'
        	],
        	[
        		'match'		=> 1,
        		'job'		=> 'Computer Operator'
        	],
        	[
        		'match'		=> 1,
        		'job'		=> 'Human Resource'
        	],
        	[
        		'match'		=> 1,
        		'job'		=> 'Quality Control'
        	],
        	[
        		'match'		=> 1,
        		'job'		=> 'Law'
        	],
        	[
        		'match'		=> 2,
        		'job'		=> 'Accounting'
        	],
        	[
        		'match'		=> 2,
        		'job'		=> 'Sales Representative'
        	],
        	[
        		'match'		=> 2,
        		'job'		=> 'Negotiator'
        	],
        	[
        		'match'		=> 2,
        		'job'		=> 'Supervisory Roles'
        	],
        	[
        		'match'		=> 2,
        		'job'		=> 'Logistic and Production Management'
        	],
        	[
        		'match'		=> 3,
        		'job'		=> 'Technical'
        	],
        	[
        		'match'		=> 3,
        		'job'		=> 'Mechanical'
        	],
        	[
        		'match'		=> 3,
        		'job'		=> 'Engineering'
        	],
        	[
        		'match'		=> 3,
        		'job'		=> 'Computer Hardware'
        	],
        	[
        		'match'		=> 3,
        		'job'		=> 'Architect'
        	],
        	[
        		'match'		=> 3,
        		'job'		=> 'Graphic Designer'
        	],
        	[
        		'match'		=> 3,
        		'job'		=> 'Machine Operator'
        	],
        	[
        		'match'		=> 4,
        		'job'		=> 'Managerial Profile for Executives and Graduates'
        	],
        ]);
    }
}
