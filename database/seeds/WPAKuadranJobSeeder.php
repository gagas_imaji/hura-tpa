<?php

use Illuminate\Database\Seeder;

class WPAKuadranJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('lookup_wpa_kuadran_job')->insert([
        	[
        		'jpm' 		=> 	-12,
	        	'kuadran'	=>	1
        	],
        	[
        		'jpm' 		=> 	-11,
	        	'kuadran'	=>	1
        	],
        	[
        		'jpm' 		=> 	-10,
	        	'kuadran'	=>	1
        	],
        	[
        		'jpm' 		=> 	-9,
	        	'kuadran'	=>	1
        	],
        	[
        		'jpm' 		=> 	-8,
	        	'kuadran'	=>	2
        	],
        	[
        		'jpm' 		=> 	-7,
	        	'kuadran'	=>	2
        	],
        	[
        		'jpm' 		=> 	-6,
	        	'kuadran'	=>	2
        	],
        	[
        		'jpm' 		=> 	-5,
	        	'kuadran'	=>	3
        	],
        	[
        		'jpm' 		=> 	-4,
	        	'kuadran'	=>	3
        	],
        	[
        		'jpm' 		=> 	-3,
	        	'kuadran'	=>	3
        	],
        	[
        		'jpm' 		=> 	-2,
	        	'kuadran'	=>	4
        	],
        	[
        		'jpm' 		=> 	-1,
	        	'kuadran'	=>	4
        	],
        	[
        		'jpm' 		=> 	0,
	        	'kuadran'	=>	4
        	],
        	[
        		'jpm' 		=> 	1,
	        	'kuadran'	=>	5
        	],
        	[
        		'jpm' 		=> 	2,
	        	'kuadran'	=>	5
        	],
        	[
        		'jpm' 		=> 	3,
	        	'kuadran'	=>	5
        	],
        	[
        		'jpm' 		=> 	4,
	        	'kuadran'	=>	6
        	],
        	[
        		'jpm' 		=> 	5,
	        	'kuadran'	=>	6
        	],
        	[
        		'jpm' 		=> 	6,
	        	'kuadran'	=>	6
        	],
        	[
        		'jpm' 		=> 	7,
	        	'kuadran'	=>	7
        	],
        	[
        		'jpm' 		=> 	8,
	        	'kuadran'	=>	7
        	],
        	[
        		'jpm' 		=> 	9,
	        	'kuadran'	=>	7
        	],
        	[
        		'jpm' 		=> 	10,
	        	'kuadran'	=>	8
        	],
        	[
        		'jpm' 		=> 	11,
	        	'kuadran'	=>	8
        	],
        	[
        		'jpm' 		=> 	12,
	        	'kuadran'	=>	8
        	],

        ]);
    }
}
