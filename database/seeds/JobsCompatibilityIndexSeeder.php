<?php

use Illuminate\Database\Seeder;

class JobsCompatibilityIndexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs_compatibility')->insert([
            [    
                'job_profil'     =>'1',
                'job_personal'   =>'1',
                'kriteria_value' =>'28'
            ],
            [    
                'job_profil'     =>'2',
                'job_personal'   =>'1',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'3',
                'job_personal'   =>'1',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'4',
                'job_personal'   =>'1',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'5',
                'job_personal'   =>'1',
                'kriteria_value' =>'12'
            ],
            [    
                'job_profil'     =>'6',
                'job_personal'   =>'1',
                'kriteria_value' =>'8'
            ],
            [    
                'job_profil'     =>'7',
                'job_personal'   =>'1',
                'kriteria_value' =>'4'
            ],
            [    
                'job_profil'     =>'8',
                'job_personal'   =>'1',
                'kriteria_value' =>'0'
            ],
            //
            [    
                'job_profil'     =>'1',
                'job_personal'   =>'2',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'2',
                'job_personal'   =>'2',
                'kriteria_value' =>'28'
            ],
            [    
                'job_profil'     =>'3',
                'job_personal'   =>'2',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'4',
                'job_personal'   =>'2',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'5',
                'job_personal'   =>'2',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'6',
                'job_personal'   =>'2',
                'kriteria_value' =>'12'
            ],
            [    
                'job_profil'     =>'7',
                'job_personal'   =>'2',
                'kriteria_value' =>'8'
            ],
            [    
                'job_profil'     =>'8',
                'job_personal'   =>'2',
                'kriteria_value' =>'4'
            ],
            //
            [    
                'job_profil'     =>'1',
                'job_personal'   =>'3',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'2',
                'job_personal'   =>'3',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'3',
                'job_personal'   =>'3',
                'kriteria_value' =>'28'
            ],
            [    
                'job_profil'     =>'4',
                'job_personal'   =>'3',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'5',
                'job_personal'   =>'3',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'6',
                'job_personal'   =>'3',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'7',
                'job_personal'   =>'3',
                'kriteria_value' =>'12'
            ],
            [    
                'job_profil'     =>'8',
                'job_personal'   =>'3',
                'kriteria_value' =>'8'
            ],
            //
            [    
                'job_profil'     =>'1',
                'job_personal'   =>'4',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'2',
                'job_personal'   =>'4',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'3',
                'job_personal'   =>'4',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'4',
                'job_personal'   =>'4',
                'kriteria_value' =>'28'
            ],
            [    
                'job_profil'     =>'5',
                'job_personal'   =>'4',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'6',
                'job_personal'   =>'4',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'7',
                'job_personal'   =>'4',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'8',
                'job_personal'   =>'4',
                'kriteria_value' =>'12'
            ],
            //
            [    
                'job_profil'     =>'1',
                'job_personal'   =>'5',
                'kriteria_value' =>'12'
            ],
            [    
                'job_profil'     =>'2',
                'job_personal'   =>'5',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'3',
                'job_personal'   =>'5',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'4',
                'job_personal'   =>'5',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'5',
                'job_personal'   =>'5',
                'kriteria_value' =>'28'
            ],
            [    
                'job_profil'     =>'6',
                'job_personal'   =>'5',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'7',
                'job_personal'   =>'5',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'8',
                'job_personal'   =>'5',
                'kriteria_value' =>'16'
            ],
            //
            [    
                'job_profil'     =>'1',
                'job_personal'   =>'6',
                'kriteria_value' =>'8'
            ],
            [    
                'job_profil'     =>'2',
                'job_personal'   =>'6',
                'kriteria_value' =>'12'
            ],
            [    
                'job_profil'     =>'3',
                'job_personal'   =>'6',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'4',
                'job_personal'   =>'6',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'5',
                'job_personal'   =>'6',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'6',
                'job_personal'   =>'6',
                'kriteria_value' =>'28'
            ],
            [    
                'job_profil'     =>'7',
                'job_personal'   =>'6',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'8',
                'job_personal'   =>'6',
                'kriteria_value' =>'20'
            ],
            //
            [    
                'job_profil'     =>'1',
                'job_personal'   =>'7',
                'kriteria_value' =>'4'
            ],
            [    
                'job_profil'     =>'2',
                'job_personal'   =>'7',
                'kriteria_value' =>'8'
            ],
            [    
                'job_profil'     =>'3',
                'job_personal'   =>'7',
                'kriteria_value' =>'12'
            ],
            [    
                'job_profil'     =>'4',
                'job_personal'   =>'7',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'5',
                'job_personal'   =>'7',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'6',
                'job_personal'   =>'7',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'7',
                'job_personal'   =>'7',
                'kriteria_value' =>'28'
            ],
            [    
                'job_profil'     =>'8',
                'job_personal'   =>'7',
                'kriteria_value' =>'24'
            ],
            //
            [    
                'job_profil'     =>'1',
                'job_personal'   =>'8',
                'kriteria_value' =>'0'
            ],
            [    
                'job_profil'     =>'2',
                'job_personal'   =>'8',
                'kriteria_value' =>'4'
            ],
            [    
                'job_profil'     =>'3',
                'job_personal'   =>'8',
                'kriteria_value' =>'8'
            ],
            [    
                'job_profil'     =>'4',
                'job_personal'   =>'8',
                'kriteria_value' =>'12'
            ],
            [    
                'job_profil'     =>'5',
                'job_personal'   =>'8',
                'kriteria_value' =>'16'
            ],
            [    
                'job_profil'     =>'6',
                'job_personal'   =>'8',
                'kriteria_value' =>'20'
            ],
            [    
                'job_profil'     =>'7',
                'job_personal'   =>'8',
                'kriteria_value' =>'24'
            ],
            [    
                'job_profil'     =>'8',
                'job_personal'   =>'8',
                'kriteria_value' =>'28'
            ],
        ]);
    }
}
