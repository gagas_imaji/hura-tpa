<?php

use Illuminate\Database\Seeder;

class BankSurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_survey')->insert([
        	[
	            'category_survey_id' 	=> 5,
	            'title' 				=> 'Kinerja',
	            'slug'					=> 'kinerja', 
	            'description'			=> 'Survey ini merupakan suatu proses penilaian kemajuan pekerjaan terhadap tujuan dan sasaran yang telah ditentukan sebelumnya',
	            'instruction'			=> 'Peserta survey wajib menjawab semua pertanyaan sesuai dengan keadaan sebenarnya yang dialami peserta.',
	            'thankyou_text'			=> 'Terima kasih telah menyelesaikan survey ini',
	            'is_random'				=> 0,
	            'timer'					=> 0
        	],
        	[
	            'category_survey_id' 	=> 5,
	            'title' 				=> 'Keterlibatan',
	            'slug'					=> 'keterlibatan', 
	            'description'			=> 'Survey ini dilakukan untuk memberikan harapan mengenai kualitas dan komitmen karyawan terhadap pekerjaan atau lingkungan dimana dia berada.',
	            'instruction'			=> 'Peserta survey wajib menjawab semua pertanyaan sesuai dengan keadaan sebenarnya yang dialami peserta.',
	            'thankyou_text'			=> 'Terima kasih telah menyelesaikan survey ini',
	            'is_random'				=> 0,
	            'timer'					=> 0
        	]
        ]);
    }
}
