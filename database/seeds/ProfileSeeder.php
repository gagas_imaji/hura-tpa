<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('profiles')->where('user_id', 1)->update(['id_corporate' => 1, 'id_division' => 1, 'gender' => 'pria']);
        DB::table('profiles')->where('user_id', '!=', 2)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->where('user_id', 3)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->where('user_id', 4)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'wanita']);
        // DB::table('profiles')->where('user_id', 5)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'wanita']);
        // DB::table('profiles')->where('user_id', 6)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->where('user_id', 7)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->where('user_id', 8)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'wanita']);
        // DB::table('profiles')->where('user_id', 9)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->where('user_id', 10)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'wanita']);
        // DB::table('profiles')->where('user_id', 11)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->where('user_id', 12)->update(['id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);

        // DB::table('users')->insert([
        //     [
        //         'email'     => 'maybankmaster@email.com',
        //         'username'  => 'master maybank',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank01@email.com',
        //         'username'  => 'user maybank 01',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank02@email.com',
        //         'username'  => 'user maybank 02',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank03@email.com',
        //         'username'  => 'user maybank 03',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank04@email.com',
        //         'username'  => 'user maybank 04',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank05@email.com',
        //         'username'  => 'user maybank 05',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank06@email.com',
        //         'username'  => 'user maybank 06',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank07@email.com',
        //         'username'  => 'user maybank 07',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank08@email.com',
        //         'username'  => 'user maybank 08',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank09@email.com',
        //         'username'  => 'user maybank 09',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
        //     [
        //         'email'     => 'maybank10@email.com',
        //         'username'  => 'user maybank 10',
        //         'password'  => bcrypt('654321'),
        //         'status'    => 1
        //     ],
            
        // ]);

        // DB::table('profiles')->insert(['user_id' => '2', 'first_name' => 'Master Maybank', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->insert(['user_id' => '3', 'first_name' => 'User Maybank 01', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->insert(['user_id' => '4', 'first_name' => 'User Maybank 02', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'wanita']);
        // DB::table('profiles')->insert(['user_id' => '5', 'first_name' => 'User Maybank 03', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'wanita']);
        // DB::table('profiles')->insert(['user_id' => '6', 'first_name' => 'User Maybank 04', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->insert(['user_id' => '7', 'first_name' => 'User Maybank 05', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->insert(['user_id' => '8', 'first_name' => 'User Maybank 06', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'wanita']);
        // DB::table('profiles')->insert(['user_id' => '9', 'first_name' => 'User Maybank 07', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->insert(['user_id' => '10', 'first_name' => 'User Maybank 08', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->insert(['user_id' => '11', 'first_name' => 'User Maybank 09', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);
        // DB::table('profiles')->insert(['user_id' => '12', 'first_name' => 'User Maybank 10', 'id_corporate' => 2, 'id_division' => 2, 'gender' => 'pria']);


        // DB::table('role_user')->insert([
        //     [
        //         'role_id' => 3,
        //         'user_id' => 2,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 3,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 4,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 5,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 6,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 7,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 8,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 9,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 10,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 11,
        //     ],
        //     [
        //         'role_id' => 2,
        //         'user_id' => 12,
        //     ],
        // ]);
    }
}

