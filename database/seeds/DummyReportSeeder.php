<?php

use Illuminate\Database\Seeder;

class DummyReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	//dummy report Gagas
    	for($i = 5; $i <= 8; $i++){
    		if($i != 7){
	       		DB::table('user_survey')->where('id_survey', $i)->update(['is_done' => 1]); 
	    		DB::table('survey')->where('id', $i)->update(['status' => 1]); 
    		}
    	}	

       	for($i = 2; $i <= 12; $i++){
       		if($i != 8){
		       	DB::table('survey_report')->insert([
					//GTI
					[
		                'survey_id'		=> 5,
			            'section_id'	=> 12,
			            'user_id' 		=> $i,
			            'score' 		=> rand(84, 131)
		        	],
		        	[
		                'survey_id'		=> 5,
			            'section_id'	=> 13,
			            'user_id' 		=> $i,
			            'score' 		=> rand(84, 131)
		        	],
		        	[
		                'survey_id'		=> 5,
			            'section_id'	=> 14,
			            'user_id' 		=> $i,
			            'score' 		=> rand(84, 131)
		        	],
		        	[
		                'survey_id'		=> 5,
			            'section_id'	=> 15,
			            'user_id' 		=> $i,
			            'score' 		=> rand(84, 131)
		        	],
		        	[
		                'survey_id'		=> 5,
			            'section_id'	=> 16,
			            'user_id' 		=> $i,
			            'score' 		=> rand(84, 131)
		        	],
		        	//TIKI
		        	[
		                'survey_id'		=> 6,
			            'section_id'	=> 17,
			            'user_id' 		=> $i,
			            'score' 		=> rand(0, 28)
		        	],
		        	[
		                'survey_id'		=> 6,
			            'section_id'	=> 18,
			            'user_id' 		=> $i,
			            'score' 		=> rand(4, 30)
		        	],
		        	[
		                'survey_id'		=> 6,
			            'section_id'	=> 19,
			            'user_id' 		=> $i,
			            'score' 		=> rand(0, 30)
		        	],
		        	[
		                'survey_id'		=> 6,
			            'section_id'	=> 20,
			            'user_id' 		=> $i,
			            'score' 		=> rand(0, 30)
		        	]
		        	//
		        ]);
       		
			
			// WPA
			// if($i == 9 || $i == 10 || $i == 11 || $i == 12){
		 //        DB::table('report_wpa')->insert([
			// 		[
		 //                'survey_id'		=> 7,
			//             'user_id' 		=> $i,
			//             'criteria'	=> 'most',
			//             'D' 		=> rand(13, 20),
			//             'I' 		=> rand(13, 20),
			//             'S' 		=> rand(12, 7),
			//             'C' 		=> rand(0, 3),
			//             'X' 		=> rand(0, 6),
			//             'hasil'		=> '',
			//             'type'		=> '',
			//             'total'		=> 24
		 //        	],
		 //        	[
		 //                'survey_id'		=> 7,
			//             'user_id' 		=> $i,
			//             'criteria'	=> 'lest',
			//             'D' 		=> rand(6, 11),
			//             'I' 		=> rand(0, 3),
			//             'S' 		=> rand(0, 6),
			//             'C' 		=> rand(3, 10),
			//             'X' 		=> rand(0, 12),
			//             'hasil'		=> '',
			//             'type'		=> '',
			//             'total'		=> 24
		 //        	],
		 //        	[
		 //                'survey_id'		=> 7,
			//             'user_id' 		=> $i,
			//             'criteria'	=> 'change',
			//             'D' 		=> rand(7, 13),
			//             'I' 		=> rand(5, 18),
			//             'S' 		=> rand(-18, -6),
			//             'C' 		=> rand(4, 17),
			//             'X' 		=> rand(0, 6),
			//             'hasil'		=> 'C',
			//             'type'		=> 'Objective Thinker',
			//             'total'		=> 24
		 //        	]

		 //        ]);
			// }

		        //PAPI
		        DB::table('report_papikostick')->insert([
					[
		                'survey_id'		=> 8,
			            'user_id' 		=> $i,
			            'N' 		=> rand(0, 9),
			            'G' 		=> rand(0, 9),
			            'A' 		=> rand(0, 9),
			            'L' 		=> rand(0, 9),
			            'P' 		=> rand(0, 9),
			            'I' 		=> rand(0, 9),
			            'T' 		=> rand(0, 9),
			            'V' 		=> rand(0, 9),
			            'X' 		=> rand(0, 9),
			            'S' 		=> rand(0, 9),
			            'B' 		=> rand(0, 9),
			            'O' 		=> rand(0, 9),
			            'R' 		=> rand(0, 9),
			            'D' 		=> rand(0, 9),
			            'C' 		=> rand(0, 9),
			            'Z' 		=> rand(0, 9),
			            'E' 		=> rand(0, 9),
			            'K' 		=> rand(0, 9),
			            'F' 		=> rand(0, 9),
			            'W' 		=> rand(0, 9),
		        	]

		        ]);
		    }
        }

        //khusus dummy sesuai excel untuk acuan perhitungan
    	DB::table('survey_report')->insert([
    		//GTI
    		[
                'survey_id'		=> 5,
	            'section_id'	=> 12,
	            'user_id' 		=> 8,
	            'score' 		=> 116
        	],
        	[
                'survey_id'		=> 5,
	            'section_id'	=> 13,
	            'user_id' 		=> 8,
	            'score' 		=> 88
        	],
        	[
                'survey_id'		=> 5,
	            'section_id'	=> 14,
	            'user_id' 		=> 8,
	            'score' 		=> 112
        	],
        	[
                'survey_id'		=> 5,
	            'section_id'	=> 15,
	            'user_id' 		=> 8,
	            'score' 		=> 104
        	],
        	[
                'survey_id'		=> 5,
	            'section_id'	=> 16,
	            'user_id' 		=> 8,
	            'score' 		=> 106
        	],

        	//TIKI
        	[
                'survey_id'		=> 6,
	            'section_id'	=> 17,
	            'user_id' 		=> 8,
	            'score' 		=> 35
        	],
        	[
                'survey_id'		=> 6,
	            'section_id'	=> 18,
	            'user_id' 		=> 8,
	            'score' 		=> 17
        	],
        	[
                'survey_id'		=> 6,
	            'section_id'	=> 19,
	            'user_id' 		=> 8,
	            'score' 		=> 37
        	],
        	[
                'survey_id'		=> 6,
	            'section_id'	=> 20,
	            'user_id' 		=> 8,
	            'score' 		=> 25
        	]
    	]);
    	//PAPI
        DB::table('report_papikostick')->insert([
			[
                'survey_id'		=> 8,
	            'user_id' 		=> 8,
	            'N' 		=> 7,
	            'G' 		=> 7,
	            'A' 		=> 2,
	            'L' 		=> 2,
	            'P' 		=> 2,
	            'I' 		=> 2,
	            'T' 		=> 3,
	            'V' 		=> 6,
	            'X' 		=> 3,
	            'S' 		=> 3,
	            'B' 		=> 6,
	            'O' 		=> 3,
	            'R' 		=> 7,
	            'D' 		=> 1,
	            'C' 		=> 7,
	            'Z' 		=> 2,
	            'E' 		=> 7,
	            'K' 		=> 8,
	            'F' 		=> 7,
	            'W' 		=> 7,
        	]

        ]);


       //dummy report SMS Finance
    	for($i = 13; $i <= 16; $i++){
    		if($i != 15){
	       		DB::table('user_survey')->where('id_survey', $i)->update(['is_done' => 1]); 
	    		DB::table('survey')->where('id', $i)->update(['status' => 1]); 
    			
    		}
    	}	

       	for($i = 13; $i <= 18; $i++){
	       	DB::table('survey_report')->insert([
				//GTI
				[
	                'survey_id'		=> 13,
		            'section_id'	=> 34,
		            'user_id' 		=> $i,
		            'score' 		=> rand(84, 131)
	        	],
	        	[
	                'survey_id'		=> 13,
		            'section_id'	=> 35,
		            'user_id' 		=> $i,
		            'score' 		=> rand(84, 131)
	        	],
	        	[
	                'survey_id'		=> 13,
		            'section_id'	=> 36,
		            'user_id' 		=> $i,
		            'score' 		=> rand(84, 131)
	        	],
	        	[
	                'survey_id'		=> 13,
		            'section_id'	=> 37,
		            'user_id' 		=> $i,
		            'score' 		=> rand(84, 131)
	        	],
	        	[
	                'survey_id'		=> 13,
		            'section_id'	=> 38,
		            'user_id' 		=> $i,
		            'score' 		=> rand(84, 131)
	        	],
	        	//TIKI
	        	[
	                'survey_id'		=> 14,
		            'section_id'	=> 39,
		            'user_id' 		=> $i,
		            'score' 		=> rand(0, 28)
	        	],
	        	[
	                'survey_id'		=> 14,
		            'section_id'	=> 40,
		            'user_id' 		=> $i,
		            'score' 		=> rand(4, 30)
	        	],
	        	[
	                'survey_id'		=> 14,
		            'section_id'	=> 41,
		            'user_id' 		=> $i,
		            'score' 		=> rand(0, 30)
	        	],
	        	[
	                'survey_id'		=> 14,
		            'section_id'	=> 42,
		            'user_id' 		=> $i,
		            'score' 		=> rand(0, 30)
	        	],
	        	//
	        ]);
			
			// WPA
			// if($i == 15 || $i == 16 || $i == 17 || $i == 18){
		 //        DB::table('report_wpa')->insert([
			// 		[
		 //                'survey_id'		=> 15,
			//             'user_id' 		=> $i,
			//             'criteria'	=> 'most',
			//             'D' 		=> rand(13, 20),
			//             'I' 		=> rand(13, 19),
			//             'S' 		=> rand(12, 7),
			//             'C' 		=> rand(0, 3),
			//             'X' 		=> rand(0, 6),
			//             'hasil'		=> '',
			//             'type'		=> '',
			//             'total'		=> 24
		 //        	],
		 //        	[
		 //                'survey_id'		=> 15,
			//             'user_id' 		=> $i,
			//             'criteria'	=> 'lest',
			//             'D' 		=> rand(6, 11),
			//             'I' 		=> rand(0, 3),
			//             'S' 		=> rand(0, 6),
			//             'C' 		=> rand(3, 10),
			//             'X' 		=> rand(0, 12),
			//             'hasil'		=> '',
			//             'type'		=> '',
			//             'total'		=> 24
		 //        	],
		 //        	[
		 //                'survey_id'		=> 15,
			//             'user_id' 		=> $i,
			//             'criteria'	=> 'change',
			//             'D' 		=> rand(7, 13),
			//             'I' 		=> rand(5, 18),
			//             'S' 		=> rand(-18, -6),
			//             'C' 		=> rand(4, 17),
			//             'X' 		=> rand(0, 6),
			//             'hasil'		=> 'C',
			//             'type'		=> 'Objective Thinker',
			//             'total'		=> 24
		 //        	]

		 //        ]);
			// }

	        //PAPI
	        DB::table('report_papikostick')->insert([
				[
	                'survey_id'		=> 16,
		            'user_id' 		=> $i,
		            'N' 		=> rand(0, 9),
		            'G' 		=> rand(0, 9),
		            'A' 		=> rand(0, 9),
		            'L' 		=> rand(0, 9),
		            'P' 		=> rand(0, 9),
		            'I' 		=> rand(0, 9),
		            'T' 		=> rand(0, 9),
		            'V' 		=> rand(0, 9),
		            'X' 		=> rand(0, 9),
		            'S' 		=> rand(0, 9),
		            'B' 		=> rand(0, 9),
		            'O' 		=> rand(0, 9),
		            'R' 		=> rand(0, 9),
		            'D' 		=> rand(0, 9),
		            'C' 		=> rand(0, 9),
		            'Z' 		=> rand(0, 9),
		            'E' 		=> rand(0, 9),
		            'K' 		=> rand(0, 9),
		            'F' 		=> rand(0, 9),
		            'W' 		=> rand(0, 9),
	        	]

	        ]);
       }


       //dummy report SMS Finance
       	DB::table('user_survey')->where('id_survey', 18)->update(['is_done' => 1]); 
		DB::table('survey')->where('id', 18)->update(['status' => 1]); 
       	for($i = 19; $i <= 22; $i++){
       		//PAPI
	        DB::table('report_papikostick')->insert([
				[
	                'survey_id'		=> 18,
		            'user_id' 		=> $i,
		            'N' 		=> rand(0, 9),
		            'G' 		=> rand(0, 9),
		            'A' 		=> rand(0, 9),
		            'L' 		=> rand(0, 9),
		            'P' 		=> rand(0, 9),
		            'I' 		=> rand(0, 9),
		            'T' 		=> rand(0, 9),
		            'V' 		=> rand(0, 9),
		            'X' 		=> rand(0, 9),
		            'S' 		=> rand(0, 9),
		            'B' 		=> rand(0, 9),
		            'O' 		=> rand(0, 9),
		            'R' 		=> rand(0, 9),
		            'D' 		=> rand(0, 9),
		            'C' 		=> rand(0, 9),
		            'Z' 		=> rand(0, 9),
		            'E' 		=> rand(0, 9),
		            'K' 		=> rand(0, 9),
		            'F' 		=> rand(0, 9),
		            'W' 		=> rand(0, 9),
	        	]

	        ]);
       	}
   	
    }
}
