<?php

use Illuminate\Database\Seeder;

class GTICriteriaReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_gti_criteria_report')->insert([
        	[
        		'min'		=> 0,
        		'max'		=> 73.9,
        		'criteria'	=> 'Very Low'
        	],
        	[
        		'min'		=> 74,
        		'max'		=> 91.9,
        		'criteria'	=> 'Below Average'
        	],
        	[
        		'min'		=> 92,
        		'max'		=> 105.9,
        		'criteria'	=> 'Average'
        	],
        	[
        		'min'		=> 106,
        		'max'		=> 128.9,
        		'criteria'	=> 'Above Average'
        	],
        	[
        		'min'		=> 129,
        		'max'		=> 150,
        		'criteria'	=> 'Excellent'
        	],
        ]);
    }
}
