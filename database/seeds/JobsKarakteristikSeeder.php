<?php

use Illuminate\Database\Seeder;

class JobsKarakteristikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('job_karakteristiks')->insert([
        [    
            'jobs_id'   =>'1',
            'D'      	=>'12',
            'I'      	=>'-6',
            'S'      	=>'3',
            'C'      	=>'7',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_id'   =>'2',
            'D'      	=>'6',
            'I'      	=>'-4',
            'S'      	=>'1',
            'C'      	=>'7',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_id'   =>'3',
            'D'      	=>'6',
            'I'      	=>'-3',
            'S'      	=>'10',
            'C'      	=>'7',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_id'   =>'4',
            'D'      	=>'-5',
            'I'      	=>'5',
            'S'      	=>'8',
            'C'      	=>'10',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_id'   =>'5',
            'D'      	=>'2',
            'I'      	=>'11',
            'S'      	=>'-1',
            'C'      	=>'2',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_id'   =>'6',
            'D'      	=>'-1',
            'I'      	=>'-3',
            'S'      	=>'4',
            'C'      	=>'10',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_id'   =>'7',
            'D'      	=>'-10',
            'I'      	=>'-11',
            'S'      	=>'6',
            'C'      	=>'8',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_id'   =>'8',
            'D'      	=>'-3',
            'I'      	=>'-7',
            'S'      	=>'6',
            'C'      	=>'9',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_id'   =>'9',
            'D'      	=>'-10',
            'I'      	=>'-11',
            'S'      	=>'3',
            'C'      	=>'8',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ]
        
    ]);
    }
}
