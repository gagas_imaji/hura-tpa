<?php
use Illuminate\Database\Seeder;
class ProfileWpa extends Seeder
{
/**
* Run the database seeds.
*
* @return void
*/
public function run()
{
DB::table('lookup_wpa_profile')->insert([
	[	
		"profile"=>"C"
		"description"=>" adalah seorang yang kritis, analitis, sistimatis, hati-hati dan memiliki standar kualitas yang tinggi. Dalam menghadapi persoalan, ia akan menganalisisnya secara mendalam, mengumpulkan bukti-bukti pendukung serta mampu menjabarkannya secara rinci. Hanya saja, perasaannya sensitif dan mudah tersinggung. Ia menganggap kritik atau perbedaan pendapat yang disampaikan kepadanya adalah suatu kelemahan diri karena ia menganggap bahwa dirinya selalu memberikan hasil yang sempurna. Ia juga tidak dapat mengekspresikan emosinya tersebut secara terbuka, ia lebih banyak memendamnya dan mengontrol perasaannya tersebut. Dalam lingkungan sosial, ia juga cenderung hanya menjadi pengikut saja karena kurang mampu berkomunikasi verbal, mempengaruhi orang lain. Ia lebih menyukai berkomunikasi dengan tulisan. Hal yang paling ditakuti adalah apabila terjadi konflik personal, dimana ia harus berhadapan dan berargumentasi dengan orang lain. Dalam situasi penuh tekanan, ia cenderung menarik diri bahkan menjadi keras kepala. Karakteristik umum dirinya adalah seorang yang kritis, analitis, sistimatis, hati-hati dan memiliki standar kualitas yang tinggi. Dalam menghadapi persoalan, ia akan menganalisisnya secara mendalam, mengumpulkan bukti-bukti pendukung serta mampu menjabarkannya secara rinci. Hanya saja, perasaannya sensitif dan mudah tersinggung. Ia menganggap kritik atau perbedaan pendapat yang disampaikan kepadanya adalah suatu kelemahan diri karena ia menganggap bahwa dirinya selalu memberikan hasil yang sempurna. Ia juga tidak dapat mengekspresikan emosinya tersebut secara terbuka, ia lebih banyak memendamnya dan mengontrol perasaannya tersebut. Dalam lingkungan sosial, ia juga cenderung hanya menjadi pengikut saja karena kurang mampu berkomunikasi verbal, mempengaruhi orang lain. Ia lebih menyukai berkomunikasi dengan tulisan. Hal yang paling ditakuti adalah apabila terjadi konflik personal, dimana ia harus berhadapan dan berargumentasi dengan orang lain. Dalam situasi penuh tekanan, ia cenderung menarik diri bahkan menjadi keras kepala."
	],
	[	
		"profile"=>"CD"
		"description"=>" pada dasarnya, ia adalah seorang pemikir yang kreatif, dengan tingkat intelektualitas yang memadai ia akan optimal dalam menjalankan pekerjaan yang banyak memerlukan kedalaman analisis dan pembuatan konsep-konsep pemikiran. Baginya kualitas adalah sangat penting, ia memiliki standar tinggi dalam bekerja. Akibatnya, ia akan menjadi cemas apabila hasil kerjanya dikritik atau dinilai negatif oleh orang lain. Ia juga menjadi kurang percaya pada hasil kerja orang lain bahkan seringkali ia menghabiskan waktunya untuk menyelesaikan sendiri tugas-tugasnya. Dalam mengambil keputusan, ia membuat keputusan berdasarkan fakta-fakta, bukan berdasarkan pada emosi, sehingga akan sulit bagi dirinya untuk memutuskan sesuatu yang belum didasarkan pada data-data yang ada. Ia memiliki standar tinggi, Ia merasa hanya dirinyalah yang dapat mengerjakan pekerjaannya dengan baik, menyebabkan seluruh waktunya tercurahkan pada penyelesaian tugas tersebut dan tidak memerlukan orang lain untuk membantu menyelesaikan tugasnya tersebut."
	],
	[	
		"profile"=>"CDI"
		"description"=>" menunjukkan orientasi terhadap tugas yang tinggi dan sangat sensitif terhadap problem. Tujuannya adalah memberikan hasil yang tepat dan menghindari kesalahan. Ia juga mampu berinisiatif melakukan perubahan dan perbaikan dikarenakan keahlian administrasinya yang cukup baik.<br>

			Ia membuat keputusan berdasarkan fakta-fakta, bukan berdasarkan pada emosi, sehingga akan sulit bagi dirinya untuk memutuskan sesuatu yang belum didasarkan pada data-data yang ada. Ia memiliki standar tinggi, Ia merasa hanya dirinyalah yang dapat mengerjakan pekerjaannya dengan baik, menyebabkan seluruh waktunya tercurahkan pada penyelesaian tugas tersebut dan tidak memerlukan orang lain untuk membantu menyelesaikan tugasnya tersebut. Pada situasi penuh tekanan ia cenderung menjadi 'keras kepala' dan menjadi 'agresif'. Dalam mengambil keputusan, ia membuat keputusan berdasarkan fakta-fakta, bukan berdasarkan pada emosi, sehingga akan sulit bagi dirinya untuk memutuskan sesuatu yang belum didasarkan pada data-data yang ada. Ia memiliki standar tinggi, Ia merasa hanya dirinyalah yang dapat mengerjakan pekerjaannya dengan baik, menyebabkan seluruh waktunya tercurahkan pada penyelesaian tugas tersebut dan tidak memerlukan orang lain untuk membantu menyelesaikan tugasnya tersebut."

	],
	[	
		"profile"=>"CDS"
		"description"=>" menunjukkan pribadi yang berorientasi pada penyelesaian tugas-tugas yang diberikan dengan kualitas tinggi dan juga hal tersebut ditampilkannya dalam bekerja. Ia seorang yang selalu memegang teguh peraturan dan kebijakan-kebijakan yang berlaku dan selalu mempertimbangkan sisi positif dan negatif dalam bekerja karena pola berpikirnya yang sitimatis dan logis. Kemampuan analisanya sangat dalam, ia cepat menangkap inti permasalahan dan memberikan berbagai pilihan untuk mendapatkan jalan keluarnya. Dalam bekerja, ia sangat mengacu pada aturan, sehingga dianggap tidak fleksibel. Ia membuat keputusan berdasarkan fakta dan data, bukan berdasarkan emosi. Ia tidak banyak berbicara dan tidak mudah mempercayai orang lain. Ia baru akan mempercayai orang lain setelah orang tersebut setelah menunjukkan pencapaian hasil seperti yang diharapkan dan sesuai dengan standar yang telah ditetapkannya. Ia bisa menjadi seorang inisiator atau perancang perubahan dalam organisasi karena bisa diandalkan untuk menciptakan konsep-konsep kreatif. <br>

Ketakutan terbesarnya adalah tidak ingin hasil karyanya dianggap tidak sempurna, dikritik atau dikoreksi oleh orang lain karena hal tersebut dianggap merendahkan kualitas dirinya yang ingin selalu dianggap sebagai seorang yang perfeksionis yang tidak pernah gagal. Ia juga akan mudah bosan dengan pekerjaan rutin atau administratif, dan dalam kondisi tertekan menjadi sinis atau merendahkan orang lain."

	],
	[	
		"profile"=>"CI"
		"description"=>" memiliki kecerdasan sosial yang baik, ia mampu merubah perilaku sesuai dengan tuntutan lingkungan. Ia adalah seorang yang mampu melakukan berbagai hal yang bervariasi sehingga sikapnya sangat efektif dan bisa memuaskan semua orang. Ia memiliki pribadi yang hangat, antusias dan senang berinteraksi dengan orang lain. Ia terampil memotivasi orang dan memberikan insight sehingga orang tersebut menemukan jalan keluarnya. Dalam pergaulannya, ia piawai dalam berkomunikasi dan meyakinkan orang lain. Ia juga dengan cepat akan dapat menyesuaikan diri dengan orang baru karena memiliki kemampuan adaptasi yang cepat. Ia sangat kompetitif dan memakai cara langsung untuk memperoleh hasil. Ia tidak memberi perintah, tetapi melibatkan orang lain ke dalam pekerjaan dengan memakai metode persuasif. Ia memancing kerjasama orang-orang disekeliling dengan memberi penjelasan tentang dasar dari aktifitas yang akan dilakukan. Ia piawai membantu orang lain memperoleh gambaran mengenai langkah-langkah yang perlu diambil dalam mewujudkan hasil yang diinginkan. Ia biasanya berbicara dengan mempergunakan rencana tindakan yang rinci yang sudah disiapkan dalam rangka memastikan kemajuan yang teratur. Karena begitu ingin meraih kemenangan, dirinya dapat menjadi tidak sabar bila standar yang ada tidak dijalankan. Ia adalah pemikir kritis dan mendalam serta mampu mengekspresikan kritik secara verbal.<br>

Namun demikian, karena selalu ingin disukai oleh orang lain, ia akan mengalami kesulitan pada saat harus menghukum atau memberikan tindakan-tindakan disiplin kepada orang lain."

	],
	[	
		"profile"=>"CID"
		"description"=>
	],
	[	
		"profile"=>"CIS"
		"description"=>
	],
	[	
		"profile"=>"CS"
		"description"=>
	],
	[	
		"profile"=>"CSD"
		"description"=>
	],
	[	
		"profile"=>"CSI"
		"description"=>
	],
	[	
		"profile"=>"D"
		"description"=>
	],
	[	
		"profile"=>"DC"
		"description"=>
	],
	[	
		"profile"=>"DCI"
		"description"=>
	],
	[	
		"profile"=>"DCS"
		"description"=>
	],
	[	
		"profile"=>"DI"
		"description"=>
	],
	[	
		"profile"=>"DIC"
		"description"=>
	],
	[	
		"profile"=>"DIS"
		"description"=>
	],
	[	
		"profile"=>"DS"
		"description"=>
	],
	[	
		"profile"=>"DSC"
		"description"=>
	],
	[	
		"profile"=>"DSI"
		"description"=>
	],
	[	
		"profile"=>"I"
		"description"=>
	],
	[	
		"profile"=>"IC"
		"description"=>
	],
	[	
		"profile"=>"ICD"
		"description"=>
	],
	[	
		"profile"=>"ICS"
		"description"=>
	],
	[	
		"profile"=>"ID"
		"description"=>
	],
	[	
		"profile"=>"IDC"
		"description"=>
	],
	[	
		"profile"=>"IDS"
		"description"=>
	],
	[	
		"profile"=>"IS"
		"description"=>
	],
	[	
		"profile"=>"ISC"
		"description"=>
	],
	[	
		"profile"=>"ISD"
		"description"=>
	],
	[	
		"profile"=>"S"
		"description"=>
	],
	[	
		"profile"=>"SC"
		"description"=>
	],
	[	
		"profile"=>"SCD"
		"description"=>
	],
	[	
		"profile"=>"SCI"
		"description"=>
	],
	[	
		"profile"=>"SD"
		"description"=>
	],
	[	
		"profile"=>"SDC"
		"description"=>
	],
	[	
		"profile"=>"SDI"
		"description"=>
	],
	[	
		"profile"=>"SI"
		"description"=>
	],
	[	
		"profile"=>"SIC"
		"description"=>
	],
	[	
		"profile"=>"SID"
		"description"=>
	],
]);
}
}

