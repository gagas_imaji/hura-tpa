<?php

use Illuminate\Database\Seeder;

class WPASuasanaEmosiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('lookup_wpa_suasana_emosi')->insert([
        	[
        		'D' => 	'Percaya diri',
	        	'I'	=>	'Hangat dan bersemangat',
	        	'S'	=>	'Tenang, cinta damai',
	        	'C'	=>	'Konservatif',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Berkemauan keras',
	        	'I'	=>	'Berkharisma',
	        	'S'	=>	'Ramah',
	        	'C'	=>	'Pemikir',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Tekun dan ulet',
	        	'I'	=>	'Antusias',
	        	'S'	=>	'Sopan santun',
	        	'C'	=>	'Sadar akan mutu',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Berani dan tegar',
	        	'I'	=>	'Ekspresif',
	        	'S'	=>	'Baik hati/ lembut hati',
	        	'C'	=>	'Mematuhi aturan',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Menghadapi hidup tanpa kompromi',
	        	'I'	=>	'Aktif',
	        	'S'	=>	'Tidak emosional',
	        	'C'	=>	'Diplomatis',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'',
	        	'I'	=>	'Enerjik',
	        	'S'	=>	'Sabar',
	        	'C'	=>	'Waspada',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'',
	        	'I'	=>	'Meyakinkan',
	        	'S'	=>	'Setia',
	        	'C'	=>	'',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'',
	        	'I'	=>	'Memberikan semangat',
	        	'S'	=>	'',
	        	'C'	=>	'',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'',
	        	'I'	=>	'Hangat',
	        	'S'	=>	'',
	        	'C'	=>	'',
	        	'category'	=> 'kekuatan'
        	],
        	//
        	[
        		'D' => 	'Pendiriannya sangat keras',
	        	'I'	=>	'Terlalu reaktif',
	        	'S'	=>	'Kurang percaya diri',
	        	'C'	=>	'Menyalahkan diri sendiri',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Terlalu cepat mengambil keputusan',
	        	'I'	=>	'Kurang tenang, tidak kalem',
	        	'S'	=>	'Cenderung pemalu',
	        	'C'	=>	'Cenderung Rigid',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Tidak sabaran, suka menekan',
	        	'I'	=>	'Cenderung melebih-lebihkan sesuatu',
	        	'S'	=>	'Pesimis, penakut, selalu khawatir',
	        	'C'	=>	'Pesimis, seringkali berpikir negatif',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Pendobrak, pemberontak',
	        	'I'	=>	'Pandai berpura-pura/basa basi',
	        	'S'	=>	'Kompromistis',
	        	'C'	=>	'Sedih tanpa alasan',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Kurang peka terhadap perasaan orang lain',
	        	'I'	=>	'Terlalu bergairah, meledak-ledak',
	        	'S'	=>	'Membenarkan diri sendiri',
	        	'C'	=>	'Sangat berhati-hati',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Tidak menyukai keluhan dan kesedihan',
	        	'I'	=>	'Keputusannya sering berdasarkan emosi',
	        	'S'	=>	'',
	        	'C'	=>	'Over Sensitif',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'',
	        	'I'	=>	'',
	        	'S'	=>	'',
	        	'C'	=>	'Sulit memutuskan',
	        	'category'	=> 'kelemahan'
        	]
        ]);
    }
}
