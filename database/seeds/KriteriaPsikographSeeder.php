<?php

use Illuminate\Database\Seeder;

class KriteriaPsikographSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kriteria_psikographs')->insert(array(
                array('nama'=>'Tidak Disarankan','slug'=>'tidak-disarankan','minimal'=>0,'maksimal'=>70),
                array('nama'=>'Dipertimbangkan','slug'=>'dipertimbangkan','minimal'=>70.10,'maksimal'=>90),
                array('nama'=>'Disarankan','slug'=>'disarankan','minimal'=>90.10,'maksimal'=>100),
          ));
    }
}
