<?php

use Illuminate\Database\Seeder;

class BankQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        DB::table('bank_question')->insert([
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 1,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> dapat merencanakan pekerjaannya agar selesai tepat waktu',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 1,
	            'question'			=> 'Perencanaan <label class="replace-name"><b>Saya</b></label> optimal',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 1,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mengingat hasil-hasil yang harus dia capai dalam pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 1,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mampu memisahkan isu-isu utama dari isu-isu sampingan dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 1,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> tahu bagaimana menentukan prioritas yang tepat',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 1,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mampu mengerjakan pekerjaannya dengan baik dengan waktu dan usaha sesedikit mungkin',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 1,
	            'question'			=> 'Kolaborasi dengan orang-orang lain sangat produktif bagi <label class="replace-name"><b>Saya</b></label>',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mampu memenuhi janji-janjinya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mampu memenuhi tanggung jawabnya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> 'Kolaborasi dengan orang-orang lain berjalan baik',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> 'Orang-orang lain dapat memahami <label class="replace-name"><b>Saya</b></label> dengan baik pada saat dia menyampaikan sesuatu',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> dapat memahami orang lain dengan baik pada saat mereka menyampaikan sesuatu',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> 'Komunikasi dengan orang lain menuntun <label class="replace-name"><b>Saya</b></label> pada hasil yang diinginkan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memunculkan ide-ide kreatif dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berinisiatif pada saat timbul masalah untuk dipecahkan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berinisiatif pada saat ada hal yang perlu diorganisasikan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memulai tugas baru secara mandiri setelah tugas-tugas terdahulu selesai',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> diminta untuk membantu pada saat diperlukan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> terbuka untuk kritik atas pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mencoba belajar dari umpan balik yang diberikan orang lain atas pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memilih tugas-tugas yang menantang bila ada',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> pikir para pelanggan/klien/pasien puas dengan pekerjaan dia',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mempertimbangkan keinginan para pelanggan/klien/pasien dalam pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mengambil tanggungjawab ekstra/tambahan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memulai tugas baru secara mandiri setelah tugas-tugas terdahulu selesai',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memilih tugas-tugas yang menantang bila ada',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berusaha agar pengetahuan pekerjaannya selalu terkini',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berusaha agar keahlian pekerjaannya selalu terkini',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memunculkan solusi-solusi kreatif untuk masalah-masalah baru',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> selalu mencari tantangan-tantangan baru dalam pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 2,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berpartisipasi aktif dalam pertemuan-pertemuan kerja',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berusaha agar pengetahuan pekerjaannya selalu terkini',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berusaha agar keahlian pekerjaannya selalu terkini',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> telah menunjukkan fleksibilitas',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mampu menanggulangi dengan baik situasi-situasi sulit atau kemunduran-kemunduran dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> pulih dengan cepat setelah situasi-situasi sulit atau kemunduran-kemunduran dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memunculkan solusi-solusi kreatif untuk masalah-masalah baru',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mampu menanggulangi dengan baik situasi-situasi tidak menentu dan tidak dapat dipastikan dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> dapat menyesuaikan diri dengan mudah terhadap perubahan-perubahan dalam pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berusaha agar pengetahuan pekerjaannya selalu terkini',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berusaha agar keahlian pekerjaannya selalu terkini',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> telah menunjukkan fleksibilitas',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mampu menanggulangi dengan baik situasi-situasi sulit atau kemunduran-kemunduran dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> pulih dengan cepat setelah situasi-situasi sulit atau kemunduran-kemunduran dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memunculkan solusi-solusi kreatif untuk masalah-masalah baru',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mampu menanggulangi dengan baik situasi-situasi tidak menentu dan tidak dapat dipastikan dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 3,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> dapat menyesuaikan diri dengan mudah terhadap perubahan-perubahan dalam pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> mengeluh mengenai hal-hal yang sepele dalam pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> membesarkan masalah lebih dari yang sebenarnya dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> memusatkan perhatian pada aspek-aspek negatif dari sebuah situasi kerja daripada aspek-aspek positifnya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berbicara dengan rekan-rekan kerja dia mengenai aspek-aspek negatif pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berbicara dengan orang-orang di luar organisasi dia  mengenai aspek-aspek negatif pekerjaannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> sengaja bekerja lambat',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> sengaja meninggalkan pekerjaannya agar orang lain harus menyelesaikannya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> berlaku kasar terhadap seseorang dalam pekerjaan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> bertengkar dengan rekan-rekan kerja, manajer atau para pelanggan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 4,
	            'question'			=> '<label class="replace-name"><b>Saya</b></label> sengaja berbuat kesalahan-kesalahan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 5,
	            'question'			=> 'Saat bekerja, saya merasa sangat bersemangat',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 5,
	            'question'			=> 'Saat bekerja, saya merasa kuat dan bersemangat',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 5,
	            'question'			=> 'Ketika saya bangun tidur di pagi hari, saya bersemangat untuk bekerja',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 5,
	            'question'			=> 'Saya dapat terus bekerja untuk durasi yang lama dalam satu waktu',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 5,
	            'question'			=> 'Saya orang yang ulet dalam bekerja',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 5,
	            'question'			=> 'Saya selalu gigih dalam bekerja, bahkan ketika sesuatu hal tidak berjalan lancar',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	//
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 6,
	            'question'			=> 'Saya melihat pekerjaan yang saya lakukan sangat bermakna dan memiliki tujuan yang jelas',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 6,
	            'question'			=> 'Saya merasa antusias mengerjakan pekerjaan saya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 6,
	            'question'			=> 'Pekerjaan saya menginspirasi diri saya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 6,
	            'question'			=> 'Saya bangga dengan pekerjaan yang saya jalani saat ini',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 6,
	            'question'			=> 'Bagi saya, pekerjaan saya menantang',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 7,
	            'question'			=> 'Waktu berlalu dengan cepat saat saya bekerja',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 7,
	            'question'			=> 'Ketika saya bekerja, saya lupa segala sesuatu di sekitar saya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 7,
	            'question'			=> 'Saya merasa bahagia ketika saya bekerja dengan keras',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 7,
	            'question'			=> 'Saya terbenam dalam pekerjaan saya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 7,
	            'question'			=> 'Saya mudah terhanyut dalam tugas yang saya kerjakan',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 7,
	            'question'			=> 'Sulit bagi saya untuk melepaskan diri dari pekerjaan saya',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1
        	],

        ]);
    }
}
