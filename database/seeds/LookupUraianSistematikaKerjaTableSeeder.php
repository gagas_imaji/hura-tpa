<?php

use Illuminate\Database\Seeder;

class LookupUraianSistematikaKerjaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_sistematika_kerja')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'Ia bekerja sesuai dengan kehendak hatinya. Cenderung bekerja tanpa perencanaan dan sistematika kerja yang jelas.',
	            'desc2' => 'Ia kurang mampu membuat perencanaan kerja secara teratur sehingga kesulitan dalam menentukan prioritas pekerjaan.'
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'Ia cenderung membutuhkan arahan dari orang lain untuk dapat bekerja secara teratur.',
	            'desc2' => 'Ia cukup mampu membuat perencanaan kerja secara teratur namun mengalami sedikit kesulitan dalam menentukan prioritas pekerjaan terutama dalam situasi mendesak.'
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'Ia mampu melakukan pekerjaan sesuai dengan prosedur dan alur kerja yang telah ditetapkan.',
	            'desc2' => 'Ia mampu untuk bekerja secara teratur dan mengorganisir tugasnya.'
        	],
            [
                'nilai' => 5,
                'desc1' => 'Ia mampu menata tugas secara sistematis, memperhatikan keteraturan dan sistematika kerja.',
                'desc2' => 'Mandiri mempersiapkan rencana kerja , Ia mampu melakukan antisipasi kendala dan bisa melakukan tindakan korektif. '
            ],
            [
                'nilai' => 6,
                'desc1' => 'Ia mampu menyusun dan merencanakan pekerjaan melalui tahapan kerja yang sistematis, serta memiliki berbagai alternatif rencana kerja.',
                'desc2' => 'Cara kerjanya teratur serta terencana. Ia memiliki kemampuan untuk mempersiapkan rencana alternatif dan memanfaatkannya secara efektif di pekerjaan.'
            ]

        ]);
    }
}
