<?php

use Illuminate\Database\Seeder;

class SurveyQuarterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey_quarter')->insert([
			[
	            'name' => 'Quarter 1',
	            'slug' => 'quarter-1'
        	],
        	[
	            'name' => 'Quarter 2',
	            'slug' => 'quarter-2'
        	],
        	[
	            'name' => 'Quarter 3',
	            'slug' => 'quarter-3'
        	],
        	[
	            'name' => 'Quarter 4',
	            'slug' => 'quarter-4'
        	],
        ]);
    }
}
