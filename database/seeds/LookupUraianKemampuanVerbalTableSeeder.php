<?php

use Illuminate\Database\Seeder;

class LookupUraianKemampuanVerbalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_kemampuan_verbal')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'Cenderung sulit untuk merumuskan ide secara utuh dan sistematis.',
	            'desc2' => 'Masih mampu memahami persoalan verbal yang sederhana,',
	            'desc3' => NULL,
	            'desc4' => NULL
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'Mampu memahami hal-hal yang bersifat verbal, lisan maupun tulisan, namun agak kesulitan untuk menyampaikan suatu gagasan.',
	            'desc2' => 'Mampu untuk menyerap informasi, namun memerlukan dorongan untuk menyampaikan suatu gagasan.',
	            'desc3' => NULL,
	            'desc4' => NULL
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'Mampu memahami hal-hal yang bersifat verbal, lisan maupun tulisan, juga cukup mampu untuk menyampaikan suatu gagasan.',
	            'desc2' => 'Sudah cukup mampu untuk menyampaikan suatu gagasan.',
	            'desc3' => NULL,
	            'desc4' => NULL
        	],
            [
                'nilai' => 5,
                'desc1' => 'Keterampilan verbalnya memadai, pola berpikirnya pragmatis dan realistis.',
                'desc2' => 'Mampu untuk mengungkapkan ide secara jelas.',
                'desc3' => 'Mampu menjelaskan hal-hal yang kompleks menjadi mudah dipahami',
                'desc4' => NULL
            ],
            [
                'nilai' => 6,
                'desc1' => 'Memiliki keterampilan dalam berpikir secara verbal dan realistis.',
                'desc2' => 'Dapat diandalkan dalam keterampilan verbalnya.',
                'desc3' => 'Mampu memahami persoalan verbal yang kompleks.',
                'desc4' => 'Terampil merumuskan ide secara komprehensif.'
            ]

        ]);
    }
}
