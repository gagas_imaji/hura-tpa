<?php

use Illuminate\Database\Seeder;

class ReportHEATSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey_report')->insert([
           //survey HEAT
            [
                'survey_id'     => 1,
                'user_id'       => 3,
                'score'         => 4.8,
                'section_id'    => 1
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 3,
                'score'         => 4.6,
                'section_id'    => 2
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 3,
                'score'         => 3.6,
                'section_id'    => 3
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 3,
                'score'         => 2.4,
                'section_id'    => 4
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 3,
                'score'         => 4.6,
                'section_id'    => 5
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 3,
                'score'         => 3.0,
                'section_id'    => 6
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 3,
                'score'         => 4.2,
                'section_id'    => 7
            ],
            //
            [
                'survey_id'     => 1,
                'user_id'       => 2,
                'score'         => 2.8,
                'section_id'    => 1
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 2,
                'score'         => 4.6,
                'section_id'    => 2
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 2,
                'score'         => 3.6,
                'section_id'    => 3
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 2,
                'score'         => 4.4,
                'section_id'    => 4
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 2,
                'score'         => 3.6,
                'section_id'    => 5
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 2,
                'score'         => 5.0,
                'section_id'    => 6
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 2,
                'score'         => 3.4,
                'section_id'    => 7
            ],
            //
            [
                'survey_id'     => 1,
                'user_id'       => 4,
                'score'         => 3.8,
                'section_id'    => 1
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 4,
                'score'         => 4.6,
                'section_id'    => 2
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 4,
                'score'         => 3.6,
                'section_id'    => 3
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 4,
                'score'         => 4.4,
                'section_id'    => 4
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 4,
                'score'         => 3.7,
                'section_id'    => 5
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 4,
                'score'         => 2.7,
                'section_id'    => 6
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 4,
                'score'         => 3.7,
                'section_id'    => 7
            ],
            //
            [
                'survey_id'     => 1,
                'user_id'       => 5,
                'score'         => 3.8,
                'section_id'    => 1
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 5,
                'score'         => 3.6,
                'section_id'    => 2
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 5,
                'score'         => 2.6,
                'section_id'    => 3
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 5,
                'score'         => 4.4,
                'section_id'    => 4
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 5,
                'score'         => 3.6,
                'section_id'    => 5
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 5,
                'score'         => 3.0,
                'section_id'    => 6
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 5,
                'score'         => 3.4,
                'section_id'    => 7
            ],
            //
            [
                'survey_id'     => 1,
                'user_id'       => 6,
                'score'         => 2.8,
                'section_id'    => 1
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 6,
                'score'         => 4.6,
                'section_id'    => 2
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 6,
                'score'         => 3.6,
                'section_id'    => 19
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 6,
                'score'         => 3.4,
                'section_id'    => 4
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 6,
                'score'         => 3.4,
                'section_id'    => 5
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 6,
                'score'         => 3.0,
                'section_id'    => 6
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 6,
                'score'         => 3.4,
                'section_id'    => 7
            ],
            //
            [
                'survey_id'     => 1,
                'user_id'       => 7,
                'score'         => 2.8,
                'section_id'    => 1
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 7,
                'score'         => 4.6,
                'section_id'    => 2
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 7,
                'score'         => 3.6,
                'section_id'    => 3
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 7,
                'score'         => 4.3,
                'section_id'    => 4
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 7,
                'score'         => 3.6,
                'section_id'    => 5
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 7,
                'score'         => 3.0,
                'section_id'    => 6
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 7,
                'score'         => 3.4,
                'section_id'    => 7
            ],
            //
            [
                'survey_id'     => 1,
                'user_id'       => 8,
                'score'         => 2.8,
                'section_id'    => 1
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 8,
                'score'         => 4.6,
                'section_id'    => 2
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 8,
                'score'         => 2.6,
                'section_id'    => 3
            ],
            [
                'survey_id'     => 1,
                'user_id'       => 8,
                'score'         => 3.4,
                'section_id'    => 4
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 8,
                'score'         => 3.6,
                'section_id'    => 5
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 8,
                'score'         => 1.0,
                'section_id'    => 6
            ],
            [
                'survey_id'     => 2,
                'user_id'       => 8,
                'score'         => 4.4,
                'section_id'    => 7
            ],
        ]);
    }
}
