<?php

use Illuminate\Database\Seeder;

class BobotNilaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_bobot_nilai')->insert(array(
			array("nama"=>'GTI1',"slug"=>'gti1','nilai'=>0.63),
			array("nama"=>'GTI2',"slug"=>'gti2','nilai'=>0.66),
			array("nama"=>'GTI3',"slug"=>'gti3','nilai'=>0.66),
			array("nama"=>'GTI4',"slug"=>'gti4','nilai'=>0.65),
			array("nama"=>'GTI5',"slug"=>'gti5','nilai'=>0.63),
			array("nama"=>'TIKI1',"slug"=>'tiki1','nilai'=>0.64),
			array("nama"=>'TIKI2',"slug"=>'tiki2','nilai'=>0.64),
			array("nama"=>'TIKI3',"slug"=>'tiki3','nilai'=>0.64),
            array("nama"=>'TIKI4',"slug"=>'tiki4','nilai'=>0.64),
            array("nama"=>'NUMERIKAL GTI',"slug"=>'numerikal_gti','nilai'=>0.65),
            array("nama"=>'C3 GTI',"slug"=>'c3_gti','nilai'=>0.78),
            array("nama"=>'C1 GTI',"slug"=>'c1_gti','nilai'=>0.73),
            array("nama"=>'C2 GTI',"slug"=>'c2_gti','nilai'=>0.99),
            array("nama"=>'C1 TIKI',"slug"=>'c1_tiki','nilai'=>0.85),
			array("nama"=>'C2 TIKI',"slug"=>'c2_tiki','nilai'=>0.72),
        ));
    }
}
