<?php

use Illuminate\Database\Seeder;

class BankOptionAnswerGTISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//subtest 1
        DB::table('bank_option_answer')->insert([
        	//1
			[
	            'id_bank_question'	=> 75,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 75,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 75,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 75,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 75,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//2
			[
	            'id_bank_question'	=> 76,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 76,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 76,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 76,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 76,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//3
			[
	            'id_bank_question'	=> 77,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 77,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 77,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 77,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 77,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//4
			[
	            'id_bank_question'	=> 78,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 78,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 78,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 78,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 78,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//5
			[
	            'id_bank_question'	=> 79,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 79,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 79,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 79,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 79,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//6
			[
	            'id_bank_question'	=> 80,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 80,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 80,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 80,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 80,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//7
			[
	            'id_bank_question'	=> 81,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 81,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 81,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 81,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 81,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//8
			[
	            'id_bank_question'	=> 82,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 82,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 82,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 82,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 82,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//9
			[
	            'id_bank_question'	=> 83,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 83,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 83,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 83,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 83,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//10
			[
	            'id_bank_question'	=> 84,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 84,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 84,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 84,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 84,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//11
			[
	            'id_bank_question'	=> 85,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 85,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 85,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 85,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 85,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//12
			[
	            'id_bank_question'	=> 86,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 86,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 86,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 86,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 86,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//13
			[
	            'id_bank_question'	=> 87,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 87,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 87,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 87,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 87,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//14
			[
	            'id_bank_question'	=> 88,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 88,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 88,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 88,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 88,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//15
			[
	            'id_bank_question'	=> 89,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 89,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 89,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 89,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 89,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//16
			[
	            'id_bank_question'	=> 90,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 90,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 90,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 90,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 90,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//17
			[
	            'id_bank_question'	=> 91,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 91,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 91,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 91,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 91,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//18
			[
	            'id_bank_question'	=> 92,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 92,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 92,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 92,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 92,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//19

			[
	            'id_bank_question'	=> 93,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 93,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 93,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 93,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 93,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//20
			[
	            'id_bank_question'	=> 94,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 94,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 94,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 94,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 94,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//21
			[
	            'id_bank_question'	=> 95,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 95,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 95,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 95,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 95,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//22
			[
	            'id_bank_question'	=> 96,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 96,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 96,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 96,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 96,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//23
			[
	            'id_bank_question'	=> 97,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 97,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 97,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 97,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 97,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//24
			[
	            'id_bank_question'	=> 98,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 98,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 98,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 98,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 98,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//25
			[
	            'id_bank_question'	=> 99,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 99,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 99,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 99,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 99,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//26
			[
	            'id_bank_question'	=> 100,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 100,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 100,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 100,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 100,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//27
			[
	            'id_bank_question'	=> 101,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 101,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 101,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 101,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 101,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//28
			[
	            'id_bank_question'	=> 102,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 102,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 102,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 102,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 102,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//29
			[
	            'id_bank_question'	=> 103,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 103,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 103,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 103,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 103,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//30
			[
	            'id_bank_question'	=> 104,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 104,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 104,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 104,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 104,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//31
			[
	            'id_bank_question'	=> 105,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 105,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 105,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 105,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 105,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//32
			[
	            'id_bank_question'	=> 106,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 106,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 106,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 106,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 106,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//33
			[
	            'id_bank_question'	=> 107,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 107,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 107,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 107,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 107,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//34
			[
	            'id_bank_question'	=> 108,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 108,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 108,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 108,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 108,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//35
			[
	            'id_bank_question'	=> 109,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 109,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 109,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 109,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 109,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//36
			[
	            'id_bank_question'	=> 110,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 110,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 110,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 110,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 110,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],

        	//37
			[
	            'id_bank_question'	=> 111,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 111,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 111,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 111,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 111,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//38
			[
	            'id_bank_question'	=> 112,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 112,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 112,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 112,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 112,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//39
			[
	            'id_bank_question'	=> 113,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 113,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 113,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 113,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 113,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//40
			[
	            'id_bank_question'	=> 114,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 114,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 114,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 114,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 114,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//41
			[
	            'id_bank_question'	=> 115,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 115,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 115,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 115,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 115,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//42
			[
	            'id_bank_question'	=> 116,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 116,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 116,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 116,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 116,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//43
			[
	            'id_bank_question'	=> 117,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 117,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 117,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 117,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 117,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//44
			[
	            'id_bank_question'	=> 118,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 118,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 118,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 118,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 118,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//45
			[
	            'id_bank_question'	=> 119,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 119,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 119,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 119,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 119,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//46
			[
	            'id_bank_question'	=> 120,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 120,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 120,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 120,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 120,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//47
			[
	            'id_bank_question'	=> 121,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 121,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 121,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 121,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 121,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//48
			[
	            'id_bank_question'	=> 122,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 122,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 122,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 122,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 122,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//49
			[
	            'id_bank_question'	=> 123,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 123,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 123,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 123,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 123,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//50
			[
	            'id_bank_question'	=> 124,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 124,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 124,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 124,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 124,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//51
			[
	            'id_bank_question'	=> 125,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 125,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 125,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 125,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 125,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//52
			[
	            'id_bank_question'	=> 126,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 126,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 126,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 126,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 126,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//53
			[
	            'id_bank_question'	=> 127,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 127,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 127,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 127,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 127,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//54
			[
	            'id_bank_question'	=> 128,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 128,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 128,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 128,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 128,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//55
			[
	            'id_bank_question'	=> 129,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 129,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 129,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 129,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 129,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//56
			[
	            'id_bank_question'	=> 130,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 130,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 130,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 130,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 130,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//57
			[
	            'id_bank_question'	=> 131,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 131,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 131,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 131,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 131,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//58
			[
	            'id_bank_question'	=> 132,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 132,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 132,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 132,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 132,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//59
			[
	            'id_bank_question'	=> 133,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 133,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 133,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 133,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 133,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        	//60
			[
	            'id_bank_question'	=> 134,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 134,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 134,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 134,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	[
	            'id_bank_question'	=> 134,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'e'
        	],
        ]);

		//subtest 2
		DB::table('bank_option_answer')->insert([
        	//1
			[
	            'id_bank_question'	=> 135,
	            'answer' 			=> 'Roy',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 135,
	            'answer' 			=> 'John',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 135,
	            'answer' 			=> 'Dani',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//2
			[
	            'id_bank_question'	=> 136,
	            'answer' 			=> 'Rani',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 136,
	            'answer' 			=> 'Susi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 136,
	            'answer' 			=> 'Rita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//3
			[
	            'id_bank_question'	=> 137,
	            'answer' 			=> 'Lina',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 137,
	            'answer' 			=> 'Laras',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 137,
	            'answer' 			=> 'Mona',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//4
			[
	            'id_bank_question'	=> 138,
	            'answer' 			=> 'Adam',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 138,
	            'answer' 			=> 'Ardi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 138,
	            'answer' 			=> 'Andi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//5
			[
	            'id_bank_question'	=> 139,
	            'answer' 			=> 'Yudi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 139,
	            'answer' 			=> 'Toni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 139,
	            'answer' 			=> 'Dodi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//6
			[
	            'id_bank_question'	=> 140,
	            'answer' 			=> 'Lisa',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 140,
	            'answer' 			=> 'Ina',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 140,
	            'answer' 			=> 'Anna',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 141,
	            'answer' 			=> 'Dito',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 141,
	            'answer' 			=> 'Beno',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 141,
	            'answer' 			=> 'Adam',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 142,
	            'answer' 			=> 'Elsa',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 142,
	            'answer' 			=> 'Lusi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 142,
	            'answer' 			=> 'Nora',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 143,
	            'answer' 			=> 'Olga',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 143,
	            'answer' 			=> 'Tami',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 143,
	            'answer' 			=> 'Ita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 144,
	            'answer' 			=> 'Dian',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 144,
	            'answer' 			=> 'Lina',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 144,
	            'answer' 			=> 'Sita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 145,
	            'answer' 			=> 'Tia',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 145,
	            'answer' 			=> 'Mira',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 145,
	            'answer' 			=> 'Lulu',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 146,
	            'answer' 			=> 'Kris',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 146,
	            'answer' 			=> 'Eric',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 146,
	            'answer' 			=> 'Edi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 147,
	            'answer' 			=> 'Andi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 147,
	            'answer' 			=> 'Anton',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 147,
	            'answer' 			=> 'Toni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 148,
	            'answer' 			=> 'Beni',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 148,
	            'answer' 			=> 'Roy',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 148,
	            'answer' 			=> 'David',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 149,
	            'answer' 			=> 'Rika',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 149,
	            'answer' 			=> 'Ina',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 149,
	            'answer' 			=> 'Tia',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 150,
	            'answer' 			=> 'Tio',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 150,
	            'answer' 			=> 'Paul',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 150,
	            'answer' 			=> 'Eric',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 151,
	            'answer' 			=> 'Rita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 151,
	            'answer' 			=> 'Rina',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 151,
	            'answer' 			=> 'Lisa',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 152,
	            'answer' 			=> 'Paul',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 152,
	            'answer' 			=> 'Joni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 152,
	            'answer' 			=> 'Soni',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 153,
	            'answer' 			=> 'Rini',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 153,
	            'answer' 			=> 'Elsa',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 153,
	            'answer' 			=> 'Lulu',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 154,
	            'answer' 			=> 'Anna',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 154,
	            'answer' 			=> 'Ita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 154,
	            'answer' 			=> 'Rita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 155,
	            'answer' 			=> 'Robi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 155,
	            'answer' 			=> 'Tomi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 155,
	            'answer' 			=> 'Ardi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 156,
	            'answer' 			=> 'Anto',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 156,
	            'answer' 			=> 'Dito',
	            'image'				=> 1,
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 156,
	            'answer' 			=> 'Soni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 157,
	            'answer' 			=> 'Olga',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 157,
	            'answer' 			=> 'Yani',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 157,
	            'answer' 			=> 'Rika',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 158,
	            'answer' 			=> 'Nadia',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 158,
	            'answer' 			=> 'Sita',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 158,
	            'answer' 			=> 'Della',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 159,
	            'answer' 			=> 'Robi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 159,
	            'answer' 			=> 'Soni',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 159,
	            'answer' 			=> 'Tomi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 160,
	            'answer' 			=> 'Elsa',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 160,
	            'answer' 			=> 'Susi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 160,
	            'answer' 			=> 'Mira',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 161,
	            'answer' 			=> 'Adi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 161,
	            'answer' 			=> 'Tio',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 161,
	            'answer' 			=> 'Roy',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 162,
	            'answer' 			=> 'Lisa',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 162,
	            'answer' 			=> 'Elly',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 162,
	            'answer' 			=> 'Diah',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 163,
	            'answer' 			=> 'Anna',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 163,
	            'answer' 			=> 'Ida',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 163,
	            'answer' 			=> 'Dewi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 164,
	            'answer' 			=> 'Edi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 164,
	            'answer' 			=> 'Erwin',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 164,
	            'answer' 			=> 'Soni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 165,
	            'answer' 			=> 'Tino',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 165,
	            'answer' 			=> 'Algi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 165,
	            'answer' 			=> 'Tono',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 166,
	            'answer' 			=> 'Lina',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 166,
	            'answer' 			=> 'Olga',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 166,
	            'answer' 			=> 'Nora',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 167,
	            'answer' 			=> 'Susi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 167,
	            'answer' 			=> 'Tina',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 167,
	            'answer' 			=> 'Tini',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 168,
	            'answer' 			=> 'Dewi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 168,
	            'answer' 			=> 'Dian',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 168,
	            'answer' 			=> 'Wati',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 169,
	            'answer' 			=> 'Bram',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 169,
	            'answer' 			=> 'Tio',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 169,
	            'answer' 			=> 'Adi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 170,
	            'answer' 			=> 'Ita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 170,
	            'answer' 			=> 'Ani',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 170,
	            'answer' 			=> 'Anna',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 171,
	            'answer' 			=> 'Adam',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 171,
	            'answer' 			=> 'Dani',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 171,
	            'answer' 			=> 'Doni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 172,
	            'answer' 			=> 'Didit',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 172,
	            'answer' 			=> 'Roy',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 172,
	            'answer' 			=> 'Roni',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 173,
	            'answer' 			=> 'Eric',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 173,
	            'answer' 			=> 'Toni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 173,
	            'answer' 			=> 'Paul',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 174,
	            'answer' 			=> 'Ita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 174,
	            'answer' 			=> 'Susi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 174,
	            'answer' 			=> 'Nora',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 175,
	            'answer' 			=> 'John',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 175,
	            'answer' 			=> 'Bima',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 175,
	            'answer' 			=> 'Toni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 176,
	            'answer' 			=> 'Paul',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 176,
	            'answer' 			=> 'Eric',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 176,
	            'answer' 			=> 'Farid',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 177,
	            'answer' 			=> 'Lia',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 177,
	            'answer' 			=> 'Yuli',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 177,
	            'answer' 			=> 'Tia',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 178,
	            'answer' 			=> 'Andi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 178,
	            'answer' 			=> 'Tomi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 178,
	            'answer' 			=> 'Roy',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 179,
	            'answer' 			=> 'Roy',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 179,
	            'answer' 			=> 'Roni',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 179,
	            'answer' 			=> 'Rino',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 180,
	            'answer' 			=> 'Lulu',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 180,
	            'answer' 			=> 'Susi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 180,
	            'answer' 			=> 'Lita',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 181,
	            'answer' 			=> 'Budi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 181,
	            'answer' 			=> 'Rio',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 181,
	            'answer' 			=> 'Tomi',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 182,
	            'answer' 			=> 'Beno',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 182,
	            'answer' 			=> 'Budi',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 182,
	            'answer' 			=> 'Tono',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 183,
	            'answer' 			=> 'Eva',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 183,
	            'answer' 			=> 'Tia',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 183,
	            'answer' 			=> 'Anna',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//
			[
	            'id_bank_question'	=> 184,
	            'answer' 			=> 'Rini',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 184,
	            'answer' 			=> 'Nora',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 184,
	            'answer' 			=> 'Tina',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	]
        ]);

		//subtest 3
		DB::table('bank_option_answer')->insert([
        	//1
			[
	            'id_bank_question'	=> 185,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0 ,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 185,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 185,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//2
			[
	            'id_bank_question'	=> 186,
	            'answer' 			=> 'G',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],

        	[
	            'id_bank_question'	=> 186,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 186,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//3
			[
	            'id_bank_question'	=> 187,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 187,
	            'answer' 			=> 'V',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 187,
	            'answer' 			=> 'Y',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//4
			[
	            'id_bank_question'	=> 188,
	            'answer' 			=> 'D',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 188,
	            'answer' 			=> 'G',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 188,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//5
			[
	            'id_bank_question'	=> 189,
	            'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 189,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 189,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//6
			[
	            'id_bank_question'	=> 190,
	            'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 190,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 190,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//7
			[
	            'id_bank_question'	=> 191,
	            'answer' 			=> 'D',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 191,
	            'answer' 			=> 'F',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 191,
	            'answer' 			=> 'I',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//8
			[
	            'id_bank_question'	=> 192,
	            'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 192,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 192,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//9
			[
	            'id_bank_question'	=> 193,
	            'answer' 			=> 'B',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 193,
	            'answer' 			=> 'D',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 193,
	            'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//10
			[
	            'id_bank_question'	=> 194,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 194,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 194,
	            'answer' 			=> 'W',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//11
			[
	            'id_bank_question'	=> 195,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 195,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 195,
	            'answer' 			=> 'V',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//12
			[
	            'id_bank_question'	=> 196,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 196,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 196,
	            'answer' 			=> 'V',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//13
			[
	            'id_bank_question'	=> 197,
	            'answer' 			=> 'F',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 197,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 197,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//14
			[
	            'id_bank_question'	=> 198,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 198,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 198,
	            'answer' 			=> 'W',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//15
			[
	            'id_bank_question'	=> 199,
	            'answer' 			=> 'E',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 199,
	            'answer' 			=> 'G',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 199,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//16
			[
	            'id_bank_question'	=> 200,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 200,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 200,
	            'answer' 			=> 'X',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//17
			[
	            'id_bank_question'	=> 201,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 201,
	            'answer' 			=> 'V',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 201,
	            'answer' 			=> 'X',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//18
			[
	            'id_bank_question'	=> 202,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 202,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 202,
	            'answer' 			=> 'X',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//19
			[
	            'id_bank_question'	=> 203,
	            'answer' 			=> 'G',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 203,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 203,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//20
			[
	            'id_bank_question'	=> 204,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 204,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 204,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//21
			[
	            'id_bank_question'	=> 205,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 205,
	           'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 205,
	            'answer' 			=> 'Y',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//22
			[
	            'id_bank_question'	=> 206,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 206,
	           'answer' 			=> 'V',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 206,
	            'answer' 			=> 'Y',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//23
			[
	            'id_bank_question'	=> 207,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 207,
	           'answer' 			=> 'X',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 207,
	            'answer' 			=> 'Z',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//24
			[
	            'id_bank_question'	=> 208,
	            'answer' 			=> 'C',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 208,
	           'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 208,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//25
			[
	            'id_bank_question'	=> 209,
	            'answer' 			=> 'G',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 209,
	           'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 209,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//26
			[
	            'id_bank_question'	=> 210,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 210,
	           'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 210,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//27
			[
	            'id_bank_question'	=> 211,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 211,
	           'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 211,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//28
			[
	            'id_bank_question'	=> 212,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 212,
	           'answer' 			=> 'T',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 212,
	            'answer' 			=> 'X',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//29
			[
	            'id_bank_question'	=> 213,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 213,
	           'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 213,
	            'answer' 			=> 'X',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//30
			[
	            'id_bank_question'	=> 214,
	            'answer' 			=> 'E',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 214,
	           'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 214,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//31
			[
	            'id_bank_question'	=> 215,
	            'answer' 			=> 'D',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 215,
	           'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 215,
	            'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//32
			[ 
	            'id_bank_question'	=> 216,
	           'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 216,
	           'answer' 			=> 'T',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 216,
	            'answer' 			=> 'V',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//33
			[
	            'id_bank_question'	=> 217,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 217,
	           'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 217,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//34
			[
	            'id_bank_question'	=> 218,
	            'answer' 			=> 'G',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 218,
	           'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 218,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//35
			[
	            'id_bank_question'	=> 219,
	            'answer' 			=> 'I',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[ 
	            'id_bank_question'	=> 219,
	           'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 219,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//36
			[
	            'id_bank_question'	=> 220,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 220,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 220,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//37
			[
	            'id_bank_question'	=> 221,
	            'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 221,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 221,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//38
			[
	            'id_bank_question'	=> 222,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 222,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 222,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//39
			[
	            'id_bank_question'	=> 223,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 223,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 223,
	            'answer' 			=> 'W',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//40
			[
	            'id_bank_question'	=> 224,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 224,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 224,
	            'answer' 			=> 'W',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//41
			[
	            'id_bank_question'	=> 225,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 225,
	            'answer' 			=> 'W',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 225,
	            'answer' 			=> 'Y',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//42
			[
	            'id_bank_question'	=> 226,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 226,
	            'answer' 			=> 'T',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 226,
	            'answer' 			=> 'V',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//43
			[
	            'id_bank_question'	=> 227,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 227,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 227,
	            'answer' 			=> 'T',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//44
			[
	            'id_bank_question'	=> 228,
	            'answer' 			=> 'G',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 228,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 228,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//45
			[
	            'id_bank_question'	=> 229,
	            'answer' 			=> 'F',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 229,
	            'answer' 			=> 'I',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 229,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//46
			[
	            'id_bank_question'	=> 230,
	            'answer' 			=> 'B',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 230,
	            'answer' 			=> 'D',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 230,
	            'answer' 			=> 'G',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//47
			[
	            'id_bank_question'	=> 231,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 231,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 231,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//48
			[
	            'id_bank_question'	=> 232,
	            'answer' 			=> 'F',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 232,
	            'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 232,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//49
			[
	            'id_bank_question'	=> 233,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 233,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 233,
	            'answer' 			=> 'Y',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//50
			[
	            'id_bank_question'	=> 234,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 234,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 234,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//51
			[
	            'id_bank_question'	=> 235,
	            'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 235,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 235,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//52
			[
	            'id_bank_question'	=> 236,
	            'answer' 			=> 'F',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 236,
	            'answer' 			=> 'I',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 236,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//53
			[
	            'id_bank_question'	=> 237,
	            'answer' 			=> 'C',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 237,
	            'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 237,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//54
			[
	            'id_bank_question'	=> 238,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 238,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 238,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//55
			[
	            'id_bank_question'	=> 239,
	            'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 239,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 239,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//56
			[
	            'id_bank_question'	=> 240,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 240,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 240,
	            'answer' 			=> 'Q',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//57
			[
	            'id_bank_question'	=> 241,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 241,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 241,
	            'answer' 			=> 'Z',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//58
			[
	            'id_bank_question'	=> 242,
	            'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 242,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 242,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//59
			[
	            'id_bank_question'	=> 243,
	            'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 243,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 243,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//60
			[
	            'id_bank_question'	=> 244,
	            'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 244,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 244,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//61
			[
	            'id_bank_question'	=> 245,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 245,
	            'answer' 			=> 'T',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 245,
	            'answer' 			=> 'X',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//62
			[
	            'id_bank_question'	=> 246,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 246,
	            'answer' 			=> 'N',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 246,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//63
			[
	            'id_bank_question'	=> 247,
	            'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 247,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 247,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//64
			[
	            'id_bank_question'	=> 248,
	            'answer' 			=> 'B',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 248,
	            'answer' 			=> 'F',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 248,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//65
			[
	            'id_bank_question'	=> 249,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 249,
	            'answer' 			=> 'W',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 249,
	            'answer' 			=> 'Y',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//66
			[
	            'id_bank_question'	=> 250,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 250,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 250,
	            'answer' 			=> 'V',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//67
			[
	            'id_bank_question'	=> 251,
	            'answer' 			=> 'A',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 251,
	            'answer' 			=> 'D',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 251,
	            'answer' 			=> 'H',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//68
			[
	            'id_bank_question'	=> 252,
	            'answer' 			=> 'F',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 252,
	            'answer' 			=> 'J',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 252,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//69
			[
	            'id_bank_question'	=> 253,
	            'answer' 			=> 'M',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 253,
	            'answer' 			=> 'R',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 253,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//70
			[
	            'id_bank_question'	=> 254,
	            'answer' 			=> 'S',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 254,
	            'answer' 			=> 'U',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 254,
	            'answer' 			=> 'Y',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//71
			[
	            'id_bank_question'	=> 255,
	            'answer' 			=> 'F',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 255,
	            'answer' 			=> 'K',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 255,
	            'answer' 			=> 'O',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//72
			[
	            'id_bank_question'	=> 256,
	            'answer' 			=> 'I',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 256,
	            'answer' 			=> 'L',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 256,
	            'answer' 			=> 'P',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        ]);
	
		//subtest 4
		DB::table('bank_option_answer')->insert([
			//1
			[
	            'id_bank_question'	=> 257,
	            'answer' 			=> '6',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 257,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 257,
	            'answer' 			=> '17',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//2
			[
	            'id_bank_question'	=> 258,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 258,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 258,
	            'answer' 			=> '7',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//3
			[
	            'id_bank_question'	=> 259,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 259,
	            'answer' 			=> '19',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 259,
	            'answer' 			=> '7',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//4
			[
	            'id_bank_question'	=> 260,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 260,
	            'answer' 			=> '5',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 260,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//5
			[
	            'id_bank_question'	=> 261,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 261,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 261,
	            'answer' 			=> '26',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//6
			[
	            'id_bank_question'	=> 262,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 262,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 262,
	            'answer' 			=> '10',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//7
			[
	            'id_bank_question'	=> 263,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 263,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 263,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//8
			[
	            'id_bank_question'	=> 264,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 264,
	            'answer' 			=> '19',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 264,
	            'answer' 			=> '24',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//9
			[
	            'id_bank_question'	=> 265,
	            'answer' 			=> '5',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 265,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 265,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//10
			[
	            'id_bank_question'	=> 266,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 266,
	            'answer' 			=> '11',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 266,
	            'answer' 			=> '17',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//11
			[
	            'id_bank_question'	=> 267,
	            'answer' 			=> '29',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 267,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 267,
	            'answer' 			=> '22',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//12
			[
	            'id_bank_question'	=> 268,
	            'answer' 			=> '28',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 268,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 268,
	            'answer' 			=> '23',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//13
			[
	            'id_bank_question'	=> 269,
	            'answer' 			=> '27',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 269,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 269,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//14
			[
	            'id_bank_question'	=> 270,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 270,
	            'answer' 			=> '27',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 270,
	            'answer' 			=> '22',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//15
			[
	            'id_bank_question'	=> 271,
	            'answer' 			=> '22',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 271,
	            'answer' 			=> '14',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 271,
	            'answer' 			=> '27',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//16
			[
	            'id_bank_question'	=> 272,
	            'answer' 			=> '29',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 272,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 272,
	            'answer' 			=> '22',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//17
			[
	            'id_bank_question'	=> 273,
	            'answer' 			=> '23',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 273,
	            'answer' 			=> '29',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 273,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//18
			[
	            'id_bank_question'	=> 274,
	            'answer' 			=> '30',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 274,
	            'answer' 			=> '24',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 274,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//19
			[
	            'id_bank_question'	=> 275,
	            'answer' 			=> '10',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 275,
	            'answer' 			=> '25',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 275,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//20
			[
	            'id_bank_question'	=> 276,
	            'answer' 			=> '26',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 276,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 276,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//21
			[
	            'id_bank_question'	=> 277,
	            'answer' 			=> '5',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 277,
	            'answer' 			=> '10',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 277,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//22
			[
	            'id_bank_question'	=> 278,
	            'answer' 			=> '17',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 278,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 278,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//23
			[
	            'id_bank_question'	=> 279,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 279,
	            'answer' 			=> '14',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 279,
	            'answer' 			=> '8',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//24
			[
	            'id_bank_question'	=> 280,
	            'answer' 			=> '10',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 280,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 280,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//25
			[
	            'id_bank_question'	=> 281,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 281,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 281,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//26
			[
	            'id_bank_question'	=> 282,
	            'answer' 			=> '23',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 282,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 282,
	            'answer' 			=> '10',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//27
			[
	            'id_bank_question'	=> 283,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 283,
	            'answer' 			=> '8',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 283,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//28
			[
	            'id_bank_question'	=> 284,
	            'answer' 			=> '28',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 284,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 284,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//29
			[
	            'id_bank_question'	=> 285,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 285,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 285,
	            'answer' 			=> '5',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//30
			[
	            'id_bank_question'	=> 286,
	            'answer' 			=> '23',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 286,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 286,
	            'answer' 			=> '29',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//31
			[
	            'id_bank_question'	=> 287,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 287,
	            'answer' 			=> '30',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 287,
	            'answer' 			=> '22',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//32
			[
	            'id_bank_question'	=> 288,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 288,
	            'answer' 			=> '6',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 288,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//33
			[
	            'id_bank_question'	=> 289,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 289,
	            'answer' 			=> '25',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 289,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//34
			[
	            'id_bank_question'	=> 290,
	            'answer' 			=> '15',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 290,
	            'answer' 			=> '10',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 290,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//35
			[
	            'id_bank_question'	=> 291,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 291,
	            'answer' 			=> '23',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 291,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//36
			[
	            'id_bank_question'	=> 292,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 292,
	            'answer' 			=> '29',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 292,
	            'answer' 			=> '23',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//37
			[
	            'id_bank_question'	=> 293,
	            'answer' 			=> '11',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 293,
	            'answer' 			=> '24',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 293,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//38
			[
	            'id_bank_question'	=> 294,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 294,
	            'answer' 			=> '6',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 294,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//39
			[
	            'id_bank_question'	=> 295,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 295,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 295,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//40
			[
	            'id_bank_question'	=> 296,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 296,
	            'answer' 			=> '25',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 296,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//41
			[
	            'id_bank_question'	=> 297,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 297,
	            'answer' 			=> '14',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 297,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//42
			[
	            'id_bank_question'	=> 298,
	            'answer' 			=> '27',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 298,
	            'answer' 			=> '14',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 298,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//43
			[
	            'id_bank_question'	=> 299,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 299,
	            'answer' 			=> '14',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 299,
	            'answer' 			=> '6',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//44
			[
	            'id_bank_question'	=> 300,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 300,
	            'answer' 			=> '24',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 300,
	            'answer' 			=> '11',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//45
			[
	            'id_bank_question'	=> 301,
	            'answer' 			=> '26',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 301,
	            'answer' 			=> '11',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 301,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//46
			[
	            'id_bank_question'	=> 302,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 302,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 302,
	            'answer' 			=> '14',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//47
			[
	            'id_bank_question'	=> 303,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 303,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 303,
	            'answer' 			=> '10',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//48
			[
	            'id_bank_question'	=> 304,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 304,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 304,
	            'answer' 			=> '27',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//49
			[
	            'id_bank_question'	=> 305,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 305,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 305,
	            'answer' 			=> '27',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//50
			[
	            'id_bank_question'	=> 306,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 306,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 306,
	            'answer' 			=> '14',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//51
			[
	            'id_bank_question'	=> 307,
	            'answer' 			=> '14',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 307,
	            'answer' 			=> '20',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 307,
	            'answer' 			=> '27',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//52
			[
	            'id_bank_question'	=> 308,
	            'answer' 			=> '16',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 308,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 308,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//53
			[
	            'id_bank_question'	=> 309,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 309,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 309,
	            'answer' 			=> '17',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//54
			[
	            'id_bank_question'	=> 310,
	            'answer' 			=> '23',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 310,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 310,
	            'answer' 			=> '12',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//55
			[
	            'id_bank_question'	=> 311,
	            'answer' 			=> '19',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 311,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 311,
	            'answer' 			=> '8',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//56
			[
	            'id_bank_question'	=> 312,
	            'answer' 			=> '25',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 312,
	            'answer' 			=> '10',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 312,
	            'answer' 			=> '18',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//57
			[
	            'id_bank_question'	=> 313,
	            'answer' 			=> '21',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 313,
	            'answer' 			=> '13',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 313,
	            'answer' 			=> '8',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//58
			[
	            'id_bank_question'	=> 314,
	            'answer' 			=> '24',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 314,
	            'answer' 			=> '29',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 314,
	            'answer' 			=> '17',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//59
			[
	            'id_bank_question'	=> 315,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 315,
	            'answer' 			=> '17',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 315,
	            'answer' 			=> '9',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	//60
			[
	            'id_bank_question'	=> 316,
	            'answer' 			=> '4',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 316,
	            'answer' 			=> '7',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 316,
	            'answer' 			=> '11',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],

		]);

		//subtest 5
		DB::table('bank_option_answer')->insert([
			//1
			[
	            'id_bank_question'	=> 317,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 317,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 317,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 317,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//2
			[
	            'id_bank_question'	=> 318,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 318,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 318,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 318,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//3
			[
	            'id_bank_question'	=> 319,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 319,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 319,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 319,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//4
			[
	            'id_bank_question'	=> 320,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 320,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 320,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 320,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//5
			[
	            'id_bank_question'	=> 321,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 321,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 321,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 321,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//6
			[
	            'id_bank_question'	=> 322,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 322,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 322,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 322,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//7
			[
	            'id_bank_question'	=> 323,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 323,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 323,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 323,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//8
			[
	            'id_bank_question'	=> 324,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 324,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 324,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 324,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//9
			[
	            'id_bank_question'	=> 325,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 325,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 325,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 325,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//10
			[
	            'id_bank_question'	=> 326,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 326,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 326,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 326,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//11
			[
	            'id_bank_question'	=> 327,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 327,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 327,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 327,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//12
			[
	            'id_bank_question'	=> 328,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 328,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 328,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 328,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//13
			[
	            'id_bank_question'	=> 329,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 329,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 329,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 329,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//14
			[
	            'id_bank_question'	=> 330,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 330,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 330,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 330,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//15
			[
	            'id_bank_question'	=> 331,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 331,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 331,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 331,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//16
			[
	            'id_bank_question'	=> 332,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 332,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 332,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 332,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//17
			[
	            'id_bank_question'	=> 333,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 333,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 333,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 333,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//18
			[
	            'id_bank_question'	=> 334,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 334,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 334,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 334,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//19
			[
	            'id_bank_question'	=> 335,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 335,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 335,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 335,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//20
			[
	            'id_bank_question'	=> 336,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 336,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 336,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 336,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//21
			[
	            'id_bank_question'	=> 337,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 337,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 337,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 337,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//22
			[
	            'id_bank_question'	=> 338,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 338,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 338,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 338,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//23
			[
	            'id_bank_question'	=> 339,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 339,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 339,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 339,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//24
			[
	            'id_bank_question'	=> 340,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 340,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 340,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 340,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//25
			[
	            'id_bank_question'	=> 341,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 341,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 341,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 341,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//26
			[
	            'id_bank_question'	=> 342,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 342,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 342,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 342,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	


        	//27
			[
	            'id_bank_question'	=> 343,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 343,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 343,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 343,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//28
			[
	            'id_bank_question'	=> 344,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 344,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 344,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 344,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//29
			[
	            'id_bank_question'	=> 345,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 345,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 345,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 345,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//30
			[
	            'id_bank_question'	=> 346,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 346,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 346,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 346,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//31
			[
	            'id_bank_question'	=> 347,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 347,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 347,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 347,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//32
			[
	            'id_bank_question'	=> 348,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 348,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 348,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 348,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//33
			[
	            'id_bank_question'	=> 349,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 349,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 349,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 349,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//34
			[
	            'id_bank_question'	=> 350,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 350,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 350,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 350,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//35
			[
	            'id_bank_question'	=> 351,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 351,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 351,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 351,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//36
			[
	            'id_bank_question'	=> 352,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 352,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 352,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 352,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//37
			[
	            'id_bank_question'	=> 353,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 353,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 353,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 353,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//38
			[
	            'id_bank_question'	=> 354,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 354,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 354,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 354,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//39
			[
	            'id_bank_question'	=> 355,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 355,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 355,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 355,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//40
			[
	            'id_bank_question'	=> 356,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 356,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 356,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 356,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//41
			[
	            'id_bank_question'	=> 357,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 357,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 357,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 357,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//42
			[
	            'id_bank_question'	=> 358,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 358,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 358,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 358,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//43
			[
	            'id_bank_question'	=> 359,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 359,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 359,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 359,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//44
			[
	            'id_bank_question'	=> 360,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 360,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 360,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 360,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//45
			[
	            'id_bank_question'	=> 361,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 361,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 361,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 361,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//46
			[
	            'id_bank_question'	=> 362,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 362,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 362,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 362,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//47
			[
	            'id_bank_question'	=> 363,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 363,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 363,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 363,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//48
			[
	            'id_bank_question'	=> 364,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 364,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 364,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 364,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//49
			[
	            'id_bank_question'	=> 365,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 365,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 365,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 365,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//50
			[
	            'id_bank_question'	=> 366,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 366,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 366,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 366,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//51
			[
	            'id_bank_question'	=> 367,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 367,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 367,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 367,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//52
			[
	            'id_bank_question'	=> 368,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 368,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 368,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 368,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//53
			[
	            'id_bank_question'	=> 369,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 369,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 369,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 369,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//54
			[
	            'id_bank_question'	=> 370,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 370,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 370,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 370,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//55
			[
	            'id_bank_question'	=> 371,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 371,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 371,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 371,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//56
			[
	            'id_bank_question'	=> 372,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 372,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 372,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 372,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//57
			[
	            'id_bank_question'	=> 373,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 373,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 373,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 373,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//58
			[
	            'id_bank_question'	=> 374,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 374,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 374,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 374,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//59
			[
	            'id_bank_question'	=> 375,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 375,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 375,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 375,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
        	//60
			[
	            'id_bank_question'	=> 376,
	            'answer' 			=> '0',
	            'image'				=> '',
	            'point'				=> 1,
	            'hidden'			=> 0,
	            'opsi'				=> 'a'
        	],
        	[
	            'id_bank_question'	=> 376,
	            'answer' 			=> '1',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'b'
        	],
        	[
	            'id_bank_question'	=> 376,
	            'answer' 			=> '2',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'c'
        	],
        	[
	            'id_bank_question'	=> 376,
	            'answer' 			=> '3',
	            'image'				=> '',
	            'point'				=> 0,
	            'hidden'			=> 0,
	            'opsi'				=> 'd'
        	],
		]);
    }
}
