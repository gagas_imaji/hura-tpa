<?php

use Illuminate\Database\Seeder;

class LookupTikiRawScoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_tiki_raw_score')->insert([
        	[
        		'raw_score'		=> 0,
        		'subtest1'			=> 0,
        		'subtest2'			=> 4,
        		'subtest3'			=> 0,
        		'subtest4'			=> 0
        	],
        	[
        		'raw_score'		=> 1,
        		'subtest1'			=> 0,
        		'subtest2'			=> 4,
        		'subtest3'			=> 0,
        		'subtest4'			=> 0
        	],
        	[
        		'raw_score'		=> 2,
        		'subtest1'			=> 0,
        		'subtest2'			=> 5,
        		'subtest3'			=> 0,
        		'subtest4'			=> 3
        	],
        	[
        		'raw_score'		=> 3,
        		'subtest1'			=> 0,
        		'subtest2'			=> 5,
        		'subtest3'			=> 0,
        		'subtest4'			=> 6
        	],
        	[
        		'raw_score'		=> 4,
        		'subtest1'			=> 0,
        		'subtest2'			=> 5,
        		'subtest3'			=> 1,
        		'subtest4'			=> 7
        	],
        	[
        		'raw_score'		=> 5,
        		'subtest1'			=> 1,
        		'subtest2'			=> 6,
        		'subtest3'			=> 1,
        		'subtest4'			=> 9
        	],
        	[
        		'raw_score'		=> 6,
        		'subtest1'			=> 1,
        		'subtest2'			=> 7,
        		'subtest3'			=> 1,
        		'subtest4'			=> 10
        	],
        	[
        		'raw_score'		=> 7,
        		'subtest1'			=> 1,
        		'subtest2'			=> 8,
        		'subtest3'			=> 2,
        		'subtest4'			=> 12
        	],
        	[
        		'raw_score'		=> 8,
        		'subtest1'			=> 2,
        		'subtest2'			=> 9,
        		'subtest3'			=> 2,
        		'subtest4'			=> 13
        	],
        	[
        		'raw_score'		=> 9,
        		'subtest1'			=> 3,
        		'subtest2'			=> 9,
        		'subtest3'			=> 3,
        		'subtest4'			=> 14
        	],
        	[
        		'raw_score'		=> 10,
        		'subtest1'			=> 4,
        		'subtest2'			=> 11,
        		'subtest3'			=> 4,
        		'subtest4'			=> 16
        	],
        	//
        	[
        		'raw_score'		=> 11,
        		'subtest1'			=> 5,
        		'subtest2'			=> 12,
        		'subtest3'			=> 4,
        		'subtest4'			=> 17
        	],
        	[
        		'raw_score'		=> 12,
        		'subtest1'			=> 5,
        		'subtest2'			=> 13,
        		'subtest3'			=> 5,
        		'subtest4'			=> 18
        	],
        	[
        		'raw_score'		=> 13,
        		'subtest1'			=> 7,
        		'subtest2'			=> 14,
        		'subtest3'			=> 5,
        		'subtest4'			=> 18
        	],
        	[
        		'raw_score'		=> 14,
        		'subtest1'			=> 8,
        		'subtest2'			=> 15,
        		'subtest3'			=> 6,
        		'subtest4'			=> 19
        	],
        	[
        		'raw_score'		=> 15,
        		'subtest1'			=> 8,
        		'subtest2'			=> 16,
        		'subtest3'			=> 7,
        		'subtest4'			=> 20
        	],
        	[
        		'raw_score'		=> 16,
        		'subtest1'			=> 9,
        		'subtest2'			=> 17,
        		'subtest3'			=> 7,
        		'subtest4'			=> 21
        	],
        	[
        		'raw_score'		=> 17,
        		'subtest1'			=> 10,
        		'subtest2'			=> 18,
        		'subtest3'			=> 8,
        		'subtest4'			=> 21
        	],
        	[
        		'raw_score'		=> 18,
        		'subtest1'			=> 10,
        		'subtest2'			=> 19,
        		'subtest3'			=> 8,
        		'subtest4'			=> 22
        	],
        	[
        		'raw_score'		=> 19,
        		'subtest1'			=> 11,
        		'subtest2'			=> 21,
        		'subtest3'			=> 9,
        		'subtest4'			=> 23
        	],
        	[
        		'raw_score'		=> 20,
        		'subtest1'			=> 11,
        		'subtest2'			=> 22,
        		'subtest3'			=> 10,
        		'subtest4'			=> 24
        	],
        	//
        	[
        		'raw_score'		=> 21,
        		'subtest1'			=> 12,
        		'subtest2'			=> 24,
        		'subtest3'			=> 10,
        		'subtest4'			=> 24
        	],
        	[
        		'raw_score'		=> 22,
        		'subtest1'			=> 13,
        		'subtest2'			=> 25,
        		'subtest3'			=> 11,
        		'subtest4'			=> 25
        	],
        	[
        		'raw_score'		=> 23,
        		'subtest1'			=> 13,
        		'subtest2'			=> 27,
        		'subtest3'			=> 12,
        		'subtest4'			=> 25
        	],
        	[
        		'raw_score'		=> 24,
        		'subtest1'			=> 14,
        		'subtest2'			=> 29,
        		'subtest3'			=> 12,
        		'subtest4'			=> 26
        	],
        	[
        		'raw_score'		=> 25,
        		'subtest1'			=> 15,
        		'subtest2'			=> 30,
        		'subtest3'			=> 13,
        		'subtest4'			=> 28
        	],
        	[
        		'raw_score'		=> 26,
        		'subtest1'			=> 15,
        		'subtest2'			=> 30,
        		'subtest3'			=> 14,
        		'subtest4'			=> 29
        	],
        	[
        		'raw_score'		=> 27,
        		'subtest1'			=> 16,
        		'subtest2'			=> null,
        		'subtest3'			=> 15,
        		'subtest4'			=> 30
        	],
        	[
        		'raw_score'		=> 28,
        		'subtest1'			=> 17,
        		'subtest2'			=> null,
        		'subtest3'			=> 15,
        		'subtest4'			=> 30
        	],
        	[
        		'raw_score'		=> 29,
        		'subtest1'			=> 17,
        		'subtest2'			=> null,
        		'subtest3'			=> 16,
        		'subtest4'			=> 30
        	],
        	[
        		'raw_score'		=> 30,
        		'subtest1'			=> 18,
        		'subtest2'			=> null,
        		'subtest3'			=> 17,
        		'subtest4'			=> 30
        	],
        	//
        	[
        		'raw_score'		=> 31,
        		'subtest1'			=> 19,
        		'subtest2'			=> null,
        		'subtest3'			=> 18,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 32,
        		'subtest1'			=> 19,
        		'subtest2'			=> null,
        		'subtest3'			=> 19,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 33,
        		'subtest1'			=> 20,
        		'subtest2'			=> null,
        		'subtest3'			=> 20,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 34,
        		'subtest1'			=> 21,
        		'subtest2'			=> null,
        		'subtest3'			=> 22,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 35,
        		'subtest1'			=> 22,
        		'subtest2'			=> null,
        		'subtest3'			=> 24,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 36,
        		'subtest1'			=> 22,
        		'subtest2'			=> null,
        		'subtest3'			=> 25,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 37,
        		'subtest1'			=> 23,
        		'subtest2'			=> null,
        		'subtest3'			=> 26,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 38,
        		'subtest1'			=> 24,
        		'subtest2'			=> null,
        		'subtest3'			=> 28,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 39,
        		'subtest1'			=> 26,
        		'subtest2'			=> null,
        		'subtest3'			=> 30,
        		'subtest4'			=> null
        	],
        	[
        		'raw_score'		=> 40,
        		'subtest1'			=> 28,
        		'subtest2'			=> null,
        		'subtest3'			=> 30,
        		'subtest4'			=> null
        	],
        ]);
    }
}
