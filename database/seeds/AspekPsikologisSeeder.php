<?php

use Illuminate\Database\Seeder;

class AspekPsikologisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Kalimat awal.....
        /* DB::table('aspek_psikologis')->insert(array(
                array('kategori_aspek_id'=>'1','nama_aspek'=>'Logika Berpikir','slug'=>'logika_berpikir','gambaran_taraf_rendah'=>'Lamban dalam mengolah dan menganalisa informasi baru untuk menemukan inti permasalahan serta sulit memberikan solusi.','gambaran_taraf_tinggi'=>'Kemampuan mengolah dan menganalisa informasi secara cepat dan tepat untuk menemukan inti permasalahan serta memberikan solusi yang sesuai.'),
                array('kategori_aspek_id'=>'1','nama_aspek'=>'Kemampuan Numerikal','slug'=>'kemampuan_numerikal','gambaran_taraf_rendah'=>'Keterbatasan dalam melakukan penalaran terhadap hitungan dan kurang mampu berpikir secara logis.','gambaran_taraf_tinggi'=>'Memiliki kemampuan yang baik untuk melakukan penalaran terhadap hitungan dan berpikir secara logis'),
                array('kategori_aspek_id'=>'1','nama_aspek'=>'Daya Analisa','slug'=>'daya_analisa','gambaran_taraf_rendah'=>'Kemampuan untuk berpikir terbatas  pada hal-hal konkrit dan praktis.','gambaran_taraf_tinggi'=>'Kemampuan untuk berpikir secara deduktif induktif serta menemukan hubungan sebab akibat dari suatu permasalahan.'),
                array('kategori_aspek_id'=>'1','nama_aspek'=>'Kemampuan Verbal','slug'=>'kemampuan_verbal','gambaran_taraf_rendah'=>'Kemampuan  memahami persoalan verbal yang sederhana, cenderung sulit untuk merumuskan ide secara utuh dan sistematis.','gambaran_taraf_tinggi'=>'Kemampuan  memahami persoalan verbal yang kompleks, serta merumuskan ide secara utuh dan sistematis.'),
                array('kategori_aspek_id'=>'2','nama_aspek'=>'Orientasi Hasil','slug'=>'orientasi_hasil','gambaran_taraf_rendah'=>'Sering membuat kesalahan yang tidak perlu. Kurang fokus terhadap detail. Tempo kerja lamban, bekerja kurang produktif.','gambaran_taraf_tinggi'=>'Bekerja cepat, akurat dan berorientasi pada kualitas hasil dan produktivitas.'),
                array('kategori_aspek_id'=>'2','nama_aspek'=>'Fleksibilitas','slug'=>'fleksibilitas','gambaran_taraf_rendah'=>'Cenderung kaku, tidak fleksibel dalam bekerja.','gambaran_taraf_tinggi'=>'Lebih mementingkan fleksibilitas daripada struktur.'),
                array('kategori_aspek_id'=>'2','nama_aspek'=>'Sistematika Kerja','slug'=>'sistematika_kerja','gambaran_taraf_rendah'=>'Kurang terbiasa bekerja secara terencana. Kurang memanfaatkan prosedur kerja yang ada.','gambaran_taraf_tinggi'=>'Mampu merencanakan dan bekerja secara efektif dan efisien.'),
                array('kategori_aspek_id'=>'3','nama_aspek'=>'Motivasi Berprestasi','slug'=>'motivasi_berprestasi','gambaran_taraf_rendah'=>'Kurang tergerak untuk mencapai prestasi yang terbaik. Kurang mampu bekerja keras. Mudah puas. Tidak tuntas.','gambaran_taraf_tinggi'=>'Kebutuhan untuk menghasilkan  prestasi yang terbaik.  Tampil lebih baik dari orang lain. Mau bekerja keras dan tuntas.'),
                array('kategori_aspek_id'=>'3','nama_aspek'=>'Kerjasama','slug'=>'kerjasama','gambaran_taraf_rendah'=>'Kebutuhan untuk berada dalam kelompok rendah. Tidak berorientasi pada pencapaian hasil bersama.','gambaran_taraf_tinggi'=>'Berorientasi pada pencapaian hasil kelompok.'),
                array('kategori_aspek_id'=>'3','nama_aspek'=>'Keterampilan Interpersonal','slug'=>'keterampilan_interpersonal','gambaran_taraf_rendah'=>'Minat sosial rendah.  Kurang terampil dalam berinteraksi.  Kurang mampu menyesuaikan diri. Kurang peka.','gambaran_taraf_tinggi'=>'Minat sosial tinggi.Peka terhadap kebutuhan orang lain. Mampu menempatkan diri secara proporsional di lingkungan.'),
            ));*/

        // Kalimat terbaru....
        DB::table('aspek_psikologis')->insert(array(
                array('kategori_aspek_id'=>'1','nama_aspek'=>'Logika Berpikir','slug'=>'logika_berpikir','gambaran_taraf_rendah'=>'Kemampuan berpikir terbatas pada hal konkrit dan praktis.','gambaran_taraf_tinggi'=>'Kemampuan mengolah dan menganalisa informasi secara cepat dan tepat untuk menemukan inti permasalahan serta memberikan solusi yang sesuai.'),
                array('kategori_aspek_id'=>'1','nama_aspek'=>'Kemampuan Numerikal','slug'=>'kemampuan_numerikal','gambaran_taraf_rendah'=>'Kemampuan penalaran dalam berhitung kurang memadai.','gambaran_taraf_tinggi'=>'Memiliki kemampuan yang baik untuk melakukan penalaran terhadap hitungan dan berpikir secara logis'),
                array('kategori_aspek_id'=>'1','nama_aspek'=>'Daya Analisa','slug'=>'daya_analisa','gambaran_taraf_rendah'=>'Butuh waktu relatif lama untuk menyimpulkan persoalan yang dihadapi.','gambaran_taraf_tinggi'=>'Kemampuan untuk berpikir secara deduktif induktif serta menemukan hubungan sebab akibat dari suatu permasalahan.'),
                array('kategori_aspek_id'=>'1','nama_aspek'=>'Kemampuan Verbal','slug'=>'kemampuan_verbal','gambaran_taraf_rendah'=>'Kemampuan memahami persoalan verbal yang sederhana, cenderung sulit merumuskan ide secara utuh dan sistematis.','gambaran_taraf_tinggi'=>'Kemampuan  memahami persoalan verbal yang kompleks, serta merumuskan ide secara utuh dan sistematis.'),
                array('kategori_aspek_id'=>'2','nama_aspek'=>'Orientasi Hasil','slug'=>'orientasi_hasil','gambaran_taraf_rendah'=>'Kurang sigap dalam bekerja, memiliki tempo kerja yang lamban.','gambaran_taraf_tinggi'=>'Bekerja cepat, akurat dan berorientasi pada kualitas hasil dan produktivitas.'),
                array('kategori_aspek_id'=>'2','nama_aspek'=>'Fleksibilitas','slug'=>'fleksibilitas','gambaran_taraf_rendah'=>'Memerlukan waktu yang cukup lama untuk mempelajari hal baru.','gambaran_taraf_tinggi'=>'Lebih mementingkan fleksibilitas daripada struktur.'),
                array('kategori_aspek_id'=>'2','nama_aspek'=>'Sistematika Kerja','slug'=>'sistematika_kerja','gambaran_taraf_rendah'=>'Bekerja sesuai dengan kehendak dirinya, kurang terarah.','gambaran_taraf_tinggi'=>'Mampu merencanakan dan bekerja secara efektif dan efisien.'),
                array('kategori_aspek_id'=>'3','nama_aspek'=>'Motivasi Berprestasi','slug'=>'motivasi_berprestasi','gambaran_taraf_rendah'=>'Mudah puas dengan apa yang sudah dikerjakan.','gambaran_taraf_tinggi'=>'Kebutuhan untuk menghasilkan  prestasi yang terbaik.  Tampil lebih baik dari orang lain. Mau bekerja keras dan tuntas.'),
                array('kategori_aspek_id'=>'3','nama_aspek'=>'Kerjasama','slug'=>'kerjasama','gambaran_taraf_rendah'=>'Lebih menyukai bekerja sendiri dalam menyelesaikan tugas.','gambaran_taraf_tinggi'=>'Berorientasi pada pencapaian hasil kelompok.'),
                array('kategori_aspek_id'=>'3','nama_aspek'=>'Keterampilan Interpersonal','slug'=>'keterampilan_interpersonal','gambaran_taraf_rendah'=>'Kurang peka terhadap interaksi sosial dan kemampuan komunikasi yang terbatas.','gambaran_taraf_tinggi'=>'Minat sosial tinggi.Peka terhadap kebutuhan orang lain. Mampu menempatkan diri secara proporsional di lingkungan.'),
            ));
    }
}
