<?php

use Illuminate\Database\Seeder;

class LookupUraianKemampuanNumerikalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_kemampuan_numerikal')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'Memiliki kemampuan numerikal yang lemah.',
	            'desc2' => 'Kurang mampu melakukan penalaran hitungan secara cepat.'
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'Memiliki kemampuan numerikal yang kurang memadai.',
	            'desc2' => 'Kemampuan berhitungnya masih terbatas pada penalaran sederhana.'
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'Kemampuan numerikalnya cukup memadai.',
	            'desc2' => 'Kemampuan berhitungnya cukup memadai.'
        	],
            [
                'nilai' => 5,
                'desc1' => 'Kemampuan numerikalnya cukup baik.',
                'desc2' => 'Kecepatan dan ketepatan numerikalnya cukup baik.'
            ],
            [
                'nilai' => 6,
                'desc1' => 'Kecepatan dan ketepatan numerikalnya dapat diandalkan,',
                'desc2' => 'Kemampuan konsentrasi dan numerikalnya sangat baik.'
            ]

        ]);
    }
}
