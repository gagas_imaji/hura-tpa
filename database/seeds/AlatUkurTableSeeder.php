<?php

use Illuminate\Database\Seeder;

class AlatUkurTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('alat_ukur')->insert(array(
              array("nama"=>'GTI1','slug'=>'gti1'),
              array("nama"=>'GTI2','slug'=>'gti2'),
              array("nama"=>'GTI3','slug'=>'gti3'),
              array("nama"=>'GTI4','slug'=>'gti4'),
              array("nama"=>'GTI5','slug'=>'gti5'),
              array("nama"=>'TIKI1','slug'=>'tiki1'),
              array("nama"=>'TIKI2','slug'=>'tiki2'),
              array("nama"=>'TIKI3','slug'=>'tiki3'),
              array("nama"=>'TIKI4','slug'=>'tiki4'),
              array("nama"=>'TIKI5','slug'=>'tiki5'),
              array("nama"=>'HEXACO1','slug'=>'hexaco1'),
              array("nama"=>'HEXACO2','slug'=>'hexaco2'),
              array("nama"=>'HEXACO3','slug'=>'hexaco3'),
              array("nama"=>'HEXACO4','slug'=>'hexaco4'),
              array("nama"=>'HEXACO5','slug'=>'hexaco5'),
              array("nama"=>'HEXACO6','slug'=>'hexaco6'),
              array("nama"=>'DISC1','slug'=>'disc1'),
              array("nama"=>'DISC2','slug'=>'disc2'),
              array("nama"=>'DISC3','slug'=>'disc3'),
              array("nama"=>'DISC4','slug'=>'disc4'),
              array("nama"=>'DISC5','slug'=>'disc5'),
              array("nama"=>'LEADERSHIP1','slug'=>'leadership1'),
              array("nama"=>'LEADERSHIP2','slug'=>'leadership2'),
              array("nama"=>'LEADERSHIP3','slug'=>'leadership3'),
              array("nama"=>'LEADERSHIP4','slug'=>'leadership4'),
              array("nama"=>'LEADERSHIP5','slug'=>'leadership5'),
        ));
    }
}
