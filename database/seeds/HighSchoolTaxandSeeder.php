<?php

use Illuminate\Database\Seeder;

class HighSchoolTaxandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('option_answer')->insert([
			[
				'id_question'	=> 29,
				'answer' 	=> 'Yes, I saw Amri',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 29,
				'answer' 	=> 'Yes, I saw',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 29,
				'answer' 	=> 'Yes, I did',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 29,
				'answer' 	=> 'Yes, I did see Amri',
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 29,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 30,
				'answer' 	=> 'yesterday',
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 30,
				'answer' 	=> 'last year',
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 30,
				'answer' 	=> 'tomorrow',
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 30,
				'answer' 	=> 'every day',
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],[
				'id_question'	=> 30,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 31,
				'answer' 	=> 'old',
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 31,
				'answer' 	=> 'older',
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 31,
				'answer' 	=> 'oldest',
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 31,
				'answer' 	=> 'more old',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 0	
	        ],[
				'id_question'	=> 31,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 32,
				'answer' 	=> "No, she doesn't have",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 32,
				'answer' 	=> "No, she have not",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 32,
				'answer' 	=> "No, she doesn't",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 32,
				'answer' 	=>"No, she isn't",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 32,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 33,
				'answer' 	=> "The Japan",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 33,
				'answer' 	=> "The Japans",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 33,
				'answer' 	=> "the Japanese",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 33,
				'answer' 	=>"the Japaneses",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 33,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 34,
				'answer' 	=> "Among",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 34,
				'answer' 	=> "With",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 34,
				'answer' 	=> "for",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 34,
				'answer' 	=>"of",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 34,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 35,
				'answer' 	=> "wrongly",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 35,
				'answer' 	=> "more wrongly",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 35,
				'answer' 	=> "wronglier",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        	],
	        	[
				'id_question'	=> 35,
				'answer' 	=>"most wrongly",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 35,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 36,
				'answer' 	=> "Yes, I will come back",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 36,
				'answer' 	=> "Yes, I will",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 36,
				'answer' 	=> "Yes, I will come",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 36,
				'answer' 	=>"Yes, I will do",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 36,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 37,
				'answer' 	=> "Yes, here",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 37,
				'answer' 	=> "Here is your book",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 37,
				'answer' 	=> "Yes, I do. It's here",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 37,
				'answer' 	=>"Here you book is",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 37,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 38,
				'answer' 	=> "and",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 38,
				'answer' 	=> "but",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 38,
				'answer' 	=> "or",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 38,
				'answer' 	=>"since",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 38,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 39,
				'answer' 	=> "belong",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 39,
				'answer' 	=> "belongs",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 39,
				'answer' 	=> "is belonging",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 39,
				'answer' 	=>"are belonging",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 39,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 40,
				'answer' 	=> "beautifull",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 40,
				'answer' 	=> "beautifully",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 40,
				'answer' 	=> "beauty",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 40,
				'answer' 	=>"beautiful",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 40,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 41,
				'answer' 	=> "fighting",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 41,
				'answer' 	=> "fight",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 41,
				'answer' 	=> "to fighting",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 41,
				'answer' 	=>"to fight",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 41,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 42,
				'answer' 	=> "Yes, I will be",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 42,
				'answer' 	=> "Yes, I will",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 42,
				'answer' 	=> "Yes, I will be waiting",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 42,
				'answer' 	=>"Yes, I will be waiting for",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 42,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 43,
				'answer' 	=> "Yes,here are five students there",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 43,
				'answer' 	=> "Yes, there are five students there",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 43,
				'answer' 	=> "Yes, there are five students here",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 43,
				'answer' 	=>"Yes, here five students here",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 43,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 44,
				'answer' 	=> "and",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 44,
				'answer' 	=> "or",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 44,
				'answer' 	=> "but",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 44,
				'answer' 	=>"while",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 44,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 45,
				'answer' 	=> "Them are waiting for me outside the building",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 45,
				'answer' 	=> "Them are waiting for I outside the building",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 45,
				'answer' 	=> "They are waiting for I outside the building",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 45,
				'answer' 	=>"They are waiting for me outside the building",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 45,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 46,
				'answer' 	=> "joining",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 46,
				'answer' 	=> "to join",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 46,
				'answer' 	=> "to joining",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 46,
				'answer' 	=>"join",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 46,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 47,
				'answer' 	=> "were",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 47,
				'answer' 	=> "is",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 47,
				'answer' 	=> "are",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 47,
				'answer' 	=>"was",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 47,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],
	        	//
	        [
				'id_question'	=> 48,
				'answer' 	=> "Amir is writing now ?",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 48,
				'answer' 	=> "Does Amir writing now ?",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 48,
				'answer' 	=> "Does Amir is writing now ?",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 48,
				'answer' 	=>"Is Amir writing now ?",
				'image'		=> '',
				'point'		=> 0,
			    'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 48,
				'answer' 	=> 'No Answer',
				'image'		=> '',
				'point'		=> 0,
				'hidden'		=> 1	
	        ],

	        //option no answer function 
	        [
	            'id_question'	=> 89,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 90,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 91,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 92,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 93,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 94,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 95,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 96,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 97,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        	[
	            'id_question'	=> 98,
	            'answer' 		=> 'No Answer',
	            'image'			=> '',
	            'point'			=> 0,
	            'hidden'		=> 0
        	],
        ]);
    }
}
