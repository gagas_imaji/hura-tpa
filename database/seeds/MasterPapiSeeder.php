<?php

use Illuminate\Database\Seeder;

class MasterPapiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_papikostick')->insert([
        	[
	        	'number'		=> 1,
	        	'statement'		=> 'Saya seorang pekerja keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 1,
	        	'statement'		=> 'Saya bukan seorang pemurung',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	[
	        	'number'		=> 2,
	        	'statement'		=> 'Saya suka bekerja lebih baik dari yang lain',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	[
	        	'number'		=> 2,
	        	'statement'		=> 'Saya suka menekuni pekerjaan yang saya lakukan sampai selesai',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        	[
	        	'number'		=> 3,
	        	'statement'		=> 'Saya suka memberi petunjuk kepada orang lain bagaimana melakukan sesuatu',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	[
	        	'number'		=> 3,
	        	'statement'		=> 'Saya ingin melakukan sesuatu sebaik mungkin',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	//
        	[
	        	'number'		=> 4,
	        	'statement'		=> 'Saya suka melakukan hal-hal yang lucu',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	[
	        	'number'		=> 4,
	        	'statement'		=> 'Saya senang memberi tahu orang apa yang harus di kerjakannya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	//
        	[
	        	'number'		=> 5,
	        	'statement'		=> 'Saya suka bergabung dengan kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	[
	        	'number'		=> 5,
	        	'statement'		=> 'Saya senang di perhatikan oleh kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	//
        	[
	        	'number'		=> 6,
	        	'statement'		=> 'Saya suka membina suatu hubungan persahabatan antar pribadi',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	[
	        	'number'		=> 6,
	        	'statement'		=> 'Saya suka berteman dengan suatu kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	//
        	[
	        	'number'		=> 7,
	        	'statement'		=> 'Saya cepat berubah jika saya rasa hal itu diperlukan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	[
	        	'number'		=> 7,
	        	'statement'		=> 'Saya berusaha membina hubungan yang akrab dengan teman saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	//
        	[
	        	'number'		=> 8,
	        	'statement'		=> 'Saya suka bergabung dengan kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	[
	        	'number'		=> 8,
	        	'statement'		=> 'Saya suka melakukan hal-hal yang baru dan berbeda',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	//
        	[
	        	'number'		=> 9,
	        	'statement'		=> 'Saya ingin atasan saya menyukai saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	[
	        	'number'		=> 9,
	        	'statement'		=> 'Saya suka memberi tahu orang jika mereka salah',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'K',
        	],
        	//
        	[
	        	'number'		=> 10,
	        	'statement'		=> 'Saya suka mengikuti petunjuk-petunjuk yang diberikan kepada saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 10,
	        	'statement'		=> 'Saya suka mendukung pendapat atasan saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	//

        	[
	        	'number'		=> 11,
	        	'statement'		=> 'Saya berusaha sangat keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 11,
	        	'statement'		=> 'Saya seorang teratur, saya menaruh semua barang pada tempatnya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	//
        	[
	        	'number'		=> 12,
	        	'statement'		=> 'Saya dapat membuat orang mau bekerja keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	[
	        	'number'		=> 12,
	        	'statement'		=> 'Saya tidak mudah marah',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	[
	        	'number'		=> 13,
	        	'statement'		=> 'Saya suka memberi tahu kelompok apa yang harus dikerjakan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	[
	        	'number'		=> 13,
	        	'statement'		=> 'Saya selalu menekuni suatu pekerjaan sampai selesai',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        	[
	        	'number'		=> 14,
	        	'statement'		=> 'Saya ingin tampil menarik dan menakjubkan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	[
	        	'number'		=> 14,
	        	'statement'		=> 'Saya ingin menjadi orang yang berhasil',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	//
        	[
	        	'number'		=> 15,
	        	'statement'		=> 'Saya ingin sesuai dan diterima dalam kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	[
	        	'number'		=> 15,
	        	'statement'		=> 'Saya suka membantu orang dalam mengambil sikap',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	//
        	[
	        	'number'		=> 16,
	        	'statement'		=> 'Saya cemas jika seseorang tidak menyukai saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	[
	        	'number'		=> 16,
	        	'statement'		=> 'Saya suka orang memperhatikan saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	//
        	[
	        	'number'		=> 17,
	        	'statement'		=> 'Saya suka mencoba hal-hal baru',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	[
	        	'number'		=> 17,
	        	'statement'		=> 'Saya lebih suka bekerja bersama orang lain daripada sendiri',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	//
        	[
	        	'number'		=> 18,
	        	'statement'		=> 'Saya kadang-kadang menyalahkan orang lain jika terjadi kesalahan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'K',
        	],
        	[
	        	'number'		=> 18,
	        	'statement'		=> 'Saya merasa terganggu jika ada yang tidak menyukai saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	//
        	[
	        	'number'		=> 19,
	        	'statement'		=> 'Saya suka mendukung pendapat atasan saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	[
	        	'number'		=> 19,
	        	'statement'		=> 'Saya suka mencoba pekerjaan-pekerjaan yang baru dan berbeda',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	//
        	[
	        	'number'		=> 20,
	        	'statement'		=> 'Saya menyukai petunjuk terperinci dalam menyelesaikan permasalahan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 20,
	        	'statement'		=> 'Saya suka memberi tahu bila orang lain membuat saya kesal',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'K',
        	],
        	//

        	[
	        	'number'		=> 21,
	        	'statement'		=> 'Saya selalu berusaha keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 21,
	        	'statement'		=> 'Saya suka melaksanakan setiap langkah dengan hati-hati',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	//
        	[
	        	'number'		=> 22,
	        	'statement'		=> 'Saya seorang pemimpin yang baik',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	[
	        	'number'		=> 22,
	        	'statement'		=> 'Saya dapat mengorganisir suatu pekerjaan dengan baik',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	//
        	[
	        	'number'		=> 23,
	        	'statement'		=> 'Saya mudah tersinggung',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	[
	        	'number'		=> 23,
	        	'statement'		=> 'Saya lambat dalam membuat keputusan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	[
	        	'number'		=> 24,
	        	'statement'		=> 'Dalam suatu kelompok saya lebih suka diam',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	[
	        	'number'		=> 24,
	        	'statement'		=> 'Saya suka mengerjakan beberapa pekerjaan sekaligus',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        	[
	        	'number'		=> 25,
	        	'statement'		=> 'Saya sangat suka bila saya di undang',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	[
	        	'number'		=> 25,
	        	'statement'		=> 'Saya ingin lebih baik dari yang lain dalam mengerjakan sesuatu',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	//
        	[
	        	'number'		=> 26,
	        	'statement'		=> 'Saya suka membina hubungan akrab dengan teman-teman saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	[
	        	'number'		=> 26,
	        	'statement'		=> 'Saya suka menasehati orang lain',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	//
        	[
	        	'number'		=> 27,
	        	'statement'		=> 'Saya suka melakukan hal-hal yang baru dan berbeda',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	[
	        	'number'		=> 27,
	        	'statement'		=> 'Saya suka menceritakan bagaimana saya berhasil melakukan sesuatu',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	//
        	[
	        	'number'		=> 28,
	        	'statement'		=> 'Bila saya betul, saya suka mempertahankannya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'K',
        	],
        	[
	        	'number'		=> 28,
	        	'statement'		=> 'Saya ingin diterima dan diakui dalam suatu kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	//
        	[
	        	'number'		=> 29,
	        	'statement'		=> 'Saya berusaha untuk tidak menjadi seorang yang berbeda',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	[
	        	'number'		=> 29,
	        	'statement'		=> 'Saya berusaha untuk dekat sekali dengan orang lain',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	//
        	[
	        	'number'		=> 30,
	        	'statement'		=> 'Saya senang diberi tahu bagaimana melakukan suatu pekerjaan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 30,
	        	'statement'		=> 'Saya mudah bosan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	//
        	//
        	[
	        	'number'		=> 31,
	        	'statement'		=> 'Saya bekerja keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 31,
	        	'statement'		=> 'Saya banyak berfikir dan membuat rencana',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	//
        	//
        	[
	        	'number'		=> 32,
	        	'statement'		=> 'Saya memimpin kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	[
	        	'number'		=> 32,
	        	'statement'		=> 'Detail (hal-hal kecil) menarik bagi saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	//
        	//
        	[
	        	'number'		=> 33,
	        	'statement'		=> 'Saya mengambil keputusan secara mudah dan cepat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	[
	        	'number'		=> 33,
	        	'statement'		=> 'Saya menyimpan barang-barang saya secara rapih dan teratur',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	//
        	//
        	[
	        	'number'		=> 34,
	        	'statement'		=> 'Biasanya saya bekerja dengan tergesa-gesa',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	[
	        	'number'		=> 34,
	        	'statement'		=> 'Saya jarang marah atau bersedih',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	//
        	[
	        	'number'		=> 35,
	        	'statement'		=> 'Saya ingin menjadi bagian dari kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	[
	        	'number'		=> 35,
	        	'statement'		=> 'Saya ingin melakukan satu pekerjaan pada satu saat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        	//
        	[
	        	'number'		=> 36,
	        	'statement'		=> 'Saya berusaha berteman secara akrab',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	[
	        	'number'		=> 36,
	        	'statement'		=> 'Saya berusaha sangat keras untuk menjadi yang terbaik',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	//
        	//
        	[
	        	'number'		=> 37,
	        	'statement'		=> 'Saya senang mode terbaru untuk pakaian atau mobil',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	[
	        	'number'		=> 37,
	        	'statement'		=> 'Saya senang bertanggung jawab untuk kepentingan orang lain',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	//
        	//
        	[
	        	'number'		=> 38,
	        	'statement'		=> 'Saya menyukai perdebatan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'K',
        	],
        	[
	        	'number'		=> 38,
	        	'statement'		=> 'Saya suka mendapatkan perhatian',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	//
        	//
        	[
	        	'number'		=> 39,
	        	'statement'		=> 'Saya suka mendukung orang-orang yang menjadi atasan saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	[
	        	'number'		=> 39,
	        	'statement'		=> 'Saya tertarik menjadi bagian dari kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	//
        	//
        	[
	        	'number'		=> 40,
	        	'statement'		=> 'Saya suka mengikuti peraturan dengan hati-hati',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 40,
	        	'statement'		=> 'Saya suka orang mengenal saya dengan baik',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	//
        	//
        	[
	        	'number'		=> 41,
	        	'statement'		=> 'Saya berusaha keras sekali',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 41,
	        	'statement'		=> 'Saya sangat ramah',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	//
        	//
        	[
	        	'number'		=> 42,
	        	'statement'		=> 'Orang menilai saya seorang pemimpin yang baik',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	[
	        	'number'		=> 42,
	        	'statement'		=> 'Saya berfikir panjang dan hati-hati',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	//
        	//
        	[
	        	'number'		=> 43,
	        	'statement'		=> 'Saya sering mengambil resiko/coba-coba',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	[
	        	'number'		=> 43,
	        	'statement'		=> 'Saya sering mengurus hal-hal kecil/detail',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	//
        	//
        	[
	        	'number'		=> 44,
	        	'statement'		=> 'Orang berpendapat bahwa saya bekerja cepat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	[
	        	'number'		=> 44,
	        	'statement'		=> 'Saya sering mengurus hal-hal kecil/detail',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	//
        	//
        	[
	        	'number'		=> 45,
	        	'statement'		=> 'Saya senang mengikuti pertandingan dan olah raga',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	[
	        	'number'		=> 45,
	        	'statement'		=> 'Saya mempunyai pribadi yang menyenangkan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	//
        	[
	        	'number'		=> 46,
	        	'statement'		=> 'Saya senang jika orang akrab dengan saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'O',
        	],
        	[
	        	'number'		=> 46,
	        	'statement'		=> 'Saya selalu berusaha untuk menyelesaikan sesuatu yang telah saya mulai',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        	//
        	[
	        	'number'		=> 47,
	        	'statement'		=> 'Saya senang bereksperimen dan mencoba hal-hal baru',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	[
	        	'number'		=> 47,
	        	'statement'		=> 'Saya suka melaksanakan suatu pekerjaan sulit dengan baik',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	//
        	//
        	[
	        	'number'		=> 48,
	        	'statement'		=> 'Saya suka diperlakukan secara adil',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'K',
        	],
        	[
	        	'number'		=> 48,
	        	'statement'		=> 'Saya suka memberi tahu orang lain bagaimana melaksanakan suatu pekerjaan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	//
        	//
        	[
	        	'number'		=> 49,
	        	'statement'		=> 'Saya suka melakukan apa yang diharapkan oleh saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	[
	        	'number'		=> 49,
	        	'statement'		=> 'Saya suka memperoleh perhatian',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	//
        	//
        	[
	        	'number'		=> 50,
	        	'statement'		=> 'Saya suka petunjuk-petunjuk terperinci untuk melaksanakan suatu pekerjaan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 50,
	        	'statement'		=> 'Saya senang berada bersama orang lain',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'B',
        	],
        	//
        	//
        	[
	        	'number'		=> 51,
	        	'statement'		=> 'Saya selalu berusaha menyelesaikan pekerjaan secara sempurna',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 51,
	        	'statement'		=> 'Orang mengatakan bahwa saya tidak mengenal lelah',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	//
        	//
        	[
	        	'number'		=> 52,
	        	'statement'		=> 'Saya tipe pemimpin',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	[
	        	'number'		=> 52,
	        	'statement'		=> 'Saya mudah berteman',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	//
        	//
        	[
	        	'number'		=> 53,
	        	'statement'		=> 'Saya selalu berspekulasi',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	[
	        	'number'		=> 53,
	        	'statement'		=> 'Saya banyak sekali berfikir',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	//
        	//
        	[
	        	'number'		=> 54,
	        	'statement'		=> 'Saya bekerja dengan kecepatan teratur dan tetap',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	[
	        	'number'		=> 54,
	        	'statement'		=> 'Saya senang bekerja dengan hal-hal kecil/terperinci',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	//
        	//
        	[
	        	'number'		=> 55,
	        	'statement'		=> 'Saya bersemangat untuk mengikuti berbagai pertandingan dalam olah raga',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	[
	        	'number'		=> 55,
	        	'statement'		=> 'Saya suka mengatur dan menyimpan barang-barang secara rapi dan teratur',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	//
        	//
        	[
	        	'number'		=> 56,
	        	'statement'		=> 'Saya dapat bergaul secara baik dengan semua orang',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	[
	        	'number'		=> 56,
	        	'statement'		=> 'Saya adalah seorang yang mempunyai pembawaan tenang',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	//
        	[
	        	'number'		=> 57,
	        	'statement'		=> 'Saya ingin bertemu dengan orang-orang baru dan melakukan hal-hal baru',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'Z',
        	],
        	[
	        	'number'		=> 57,
	        	'statement'		=> 'Saya selalu ingin menyelesaikan pekerjaan yang telah saya mulai',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        	//
        	[
	        	'number'		=> 58,
	        	'statement'		=> 'Saya biasanya mempertahankan pendapat yang saya yakini',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'K',
        	],
        	[
	        	'number'		=> 58,
	        	'statement'		=> 'Saya biasanya suka bekerja keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	//
        	//
        	[
	        	'number'		=> 59,
	        	'statement'		=> 'Saya suka saran-saran dari orang yang saya kagumi',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	[
	        	'number'		=> 59,
	        	'statement'		=> 'Saya senang diserahi tanggung jawab atas kelompok orang',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	//
        	//
        	[
	        	'number'		=> 60,
	        	'statement'		=> 'Saya biarkan diri saya banyak dipengaruhi orang lain',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 60,
	        	'statement'		=> 'Saya suka jika mendapat banyak perhatian',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'X',
        	],
        	//
        	//
        	[
	        	'number'		=> 61,
	        	'statement'		=> 'Saya berusaha bekerja keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 61,
	        	'statement'		=> 'Saya mengerjakan sesuatu secara cepat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	//
        	//
        	[
	        	'number'		=> 62,
	        	'statement'		=> 'Apabila saya bicara, kelompok mendengarkan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	[
	        	'number'		=> 62,
	        	'statement'		=> 'Saya terampil menggunakan perkakas atau (alat-alat)',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	//
        	//
        	[
	        	'number'		=> 63,
	        	'statement'		=> 'Saya lambat dalam membina hubungan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	[
	        	'number'		=> 63,
	        	'statement'		=> 'Saya lambat dalam mengambil keputusan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	//
        	//
        	[
	        	'number'		=> 64,
	        	'statement'		=> 'Saya biasanya makan secara cepat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	[
	        	'number'		=> 64,
	        	'statement'		=> 'Saya suka membaca',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	//
        	//
        	[
	        	'number'		=> 65,
	        	'statement'		=> 'Saya suka pekerjaan dimana saya banyak bergerak',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	[
	        	'number'		=> 65,
	        	'statement'		=> 'Saya suka pekerjaan yang harus dilakukan secara hati-hati',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	//
        	//
        	[
	        	'number'		=> 66,
	        	'statement'		=> 'Saya mencari teman sebanyak mungkin',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	[
	        	'number'		=> 66,
	        	'statement'		=> 'Apa yang sudah saya simpan, akan mudah saya temukan kembali',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	//
        	//
        	[
	        	'number'		=> 67,
	        	'statement'		=> 'Saya merencanakan jauh-jauh sebelumnya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	[
	        	'number'		=> 67,
	        	'statement'		=> 'Saya selalu menyenangkan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	//
        	[
	        	'number'		=> 68,
	        	'statement'		=> 'Saya mempertahankan nama baik saya dengan bangga',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'K',
        	],
        	[
	        	'number'		=> 68,
	        	'statement'		=> 'Saya terus menekuni suatu masalah sampai selesai',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        	//
        	[
	        	'number'		=> 69,
	        	'statement'		=> 'Saya suka mendukung orang-orang yang saya kagumi',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	[
	        	'number'		=> 69,
	        	'statement'		=> 'Saya ingin sukses',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	//
        	//
        	[
	        	'number'		=> 70,
	        	'statement'		=> 'Saya suka orang lain yang memutuskan untuk kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 70,
	        	'statement'		=> 'Saya suka membuat keputusan untuk kelompok',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'P',
        	],
        	//
        	//
        	[
	        	'number'		=> 71,
	        	'statement'		=> 'Saya selalu berusaha bekerja keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 71,
	        	'statement'		=> 'Saya mengambil keputusan secara cepat dan mudah',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	//
        	//
        	[
	        	'number'		=> 72,
	        	'statement'		=> 'Kelompok biasanya melakukan apa yang saya inginkan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	[
	        	'number'		=> 72,
	        	'statement'		=> 'Saya biasa terburu-buru',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	//
        	//
        	[
	        	'number'		=> 73,
	        	'statement'		=> 'Saya sering merasa lelah',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	[
	        	'number'		=> 73,
	        	'statement'		=> 'Saya lamban dalam menentukan sikap',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	//
        	//
        	[
	        	'number'		=> 74,
	        	'statement'		=> 'Saya bekerja cepat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	[
	        	'number'		=> 74,
	        	'statement'		=> 'Saya mudah berteman',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	//
        	//
        	[
	        	'number'		=> 75,
	        	'statement'		=> 'Saya biasanya mempunyai semangat dan tenaga',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	[
	        	'number'		=> 75,
	        	'statement'		=> 'Saya banyak menghabiskan waktu dengan berfikir',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	//
        	//
        	[
	        	'number'		=> 76,
	        	'statement'		=> 'Saya sangat ramah terhadap orang',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	[
	        	'number'		=> 76,
	        	'statement'		=> 'Saya suka pekerjaan yang memerlukan ketelitian',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	//
        	//
        	[
	        	'number'		=> 77,
	        	'statement'		=> 'Saya banyak berfikir dan merencanakan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	[
	        	'number'		=> 77,
	        	'statement'		=> 'Saya menyimpan segala sesuatu pada tempatnya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	//
        	//
        	[
	        	'number'		=> 78,
	        	'statement'		=> 'Saya suka pekerjaan yang harus memperhatikan hal-hal kecil (detail)',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	[
	        	'number'		=> 78,
	        	'statement'		=> 'Saya tidak mudah marah',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	//
        	[
	        	'number'		=> 79,
	        	'statement'		=> 'Saya suka mengikuti orang yang saya kagumi',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'F',
        	],
        	[
	        	'number'		=> 79,
	        	'statement'		=> 'Saya selalu menyelesaikan pekerjaan yang telah saya mulai',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        	//
        	[
	        	'number'		=> 80,
	        	'statement'		=> 'Saya suka petunjuk-petunjuk yang jelas',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 80,
	        	'statement'		=> 'Saya suka bekerja keras',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'A',
        	],
        	//
        	//
        	[
	        	'number'		=> 81,
	        	'statement'		=> 'Saya mengejar apa yang saya inginkan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'G',
        	],
        	[
	        	'number'		=> 81,
	        	'statement'		=> 'Saya seorang pemimpin yang baik',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	//
        	//
        	[
	        	'number'		=> 82,
	        	'statement'		=> 'Saya dapat membuat orang lain bekerja sesuai dengan yang saya inginkan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'L',
        	],
        	[
	        	'number'		=> 82,
	        	'statement'		=> 'Saya seorang yang tergolong santai dan beruntung',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	//
        	//
        	[
	        	'number'		=> 83,
	        	'statement'		=> 'Saya mengambil keputusan secara mudah dan amat cepat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'I',
        	],
        	[
	        	'number'		=> 83,
	        	'statement'		=> 'Saya bicara secara cepat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	//
        	//
        	[
	        	'number'		=> 84,
	        	'statement'		=> 'Saya biasanya bekerja cepat',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'T',
        	],
        	[
	        	'number'		=> 84,
	        	'statement'		=> 'Saya berolahraga secara teratur',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	//
        	//
        	[
	        	'number'		=> 85,
	        	'statement'		=> 'Saya tidak suka bertemu dengan orang',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'V',
        	],
        	[
	        	'number'		=> 85,
	        	'statement'		=> 'Saya cepat merasa lelah',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	//
        	//
        	[
	        	'number'		=> 86,
	        	'statement'		=> 'Saya mempunyai banyak sekali teman',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'S',
        	],
        	[
	        	'number'		=> 86,
	        	'statement'		=> 'Saya banyak menghabiskan waktu dengan berfikir',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	//
        	//
        	[
	        	'number'		=> 87,
	        	'statement'		=> 'Saya suka berjalan dengan teori',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'R',
        	],
        	[
	        	'number'		=> 87,
	        	'statement'		=> 'Saya suka bekerja dengan hal-hal yang terperinci',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	//
        	//
        	[
	        	'number'		=> 88,
	        	'statement'		=> 'Saya menikmati pekerjaan yang melibatkan hal-hal kecil (detail)',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'D',
        	],
        	[
	        	'number'		=> 88,
	        	'statement'		=> 'Saya suka mengorganisir pekerjaan saya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	//
        	//
        	[
	        	'number'		=> 89,
	        	'statement'		=> 'Saya menaruh barang pada tempatnya',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'C',
        	],
        	[
	        	'number'		=> 89,
	        	'statement'		=> 'Saya selalu menyenangkan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'E',
        	],
        	//
        	//
        	[
	        	'number'		=> 90,
	        	'statement'		=> 'Saya suka diberitahu tentang apa yang perlu dilakukan',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'W',
        	],
        	[
	        	'number'		=> 90,
	        	'statement'		=> 'Saya harus menyelesaikan apa yang saya mulai',
	        	//'slug_section'	=> 'papikostick',
	        	'key'			=> 'N',
        	],
        	//
        ]);
    }
}
