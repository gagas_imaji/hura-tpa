<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(CorporateSeeder::class);
       // $this->call(DivisionSeeder::class);
       // $this->call(ProfileSeeder::class);

       // $this->call(PermissionSeeder::class);
       // $this->call(PermissionRoleSeeder::class);

       // $this->call(MasterWPASeeder::class);
       // $this->call(MasterPapiSeeder::class);
       // $this->call(SurveyCategorySeeder::class);
       // $this->call(SurveyQuarterSeeder::class);
       // $this->call(SurveySeeder::class);
       // $this->call(SurveyGradeSeeder::class);
       // $this->call(SectionSurveySeeder::class);
       // $this->call(QuestionParagraphSeeder::class);
       // $this->call(QuestionTypeTableSeeder::class);
       // $this->call(QuestionSeeder::class);
       // $this->call(OptionAnswerSeeder::class);

       // $this->call(BankSurveySeeder::class);
       // $this->call(BankSectionSeeder::class);
       // $this->call(BankQuestionSeeder::class);
       // $this->call(BankOptionAnswerSeeder::class);

       // $this->call(BankSurveyGTISeeder::class);
       // $this->call(BankSectionGTISeeder::class);
       // $this->call(BankQuestionGTISeeder::class);
       // $this->call(BankOptionAnswerGTISeeder::class);

       // $this->call(BankSurveyTikiSeeder::class);
       // $this->call(BankSectionTikiSeeder::class);
       // $this->call(BankQuestionTikiSeeder::class);
       // $this->call(BankOptionAnswerTikiSeeder::class);

       // $this->call(UserSurveySeeder::class);
       // $this->call(ResultSeeder::class);
       // $this->call(SurveyReportSeeder::class);
       // $this->call(ReportHEATSeeder::class);

       //kebutuhan report
       // $this->call(AnalysisSeeder::class);
       // $this->call(GTQSeeder::class);
       // $this->call(GTIJobPreferencesSeeder::class);
       // $this->call(GTICriteriaReportSeeder::class);
       // $this->call(GTIKekuatanKelemahanSeeder::class);
       // $this->call(LookupTikiRawScoreSeeder::class);
       // $this->call(LookupTikiIQ::class);
       // $this->call(WPAKarakteristikPekerjaanSeeder::class);
       // $this->call(WPAKarakteristikUmumSeeder::class);
       // $this->call(WPAPerilakuKerjaSeeder::class);
       // $this->call(WPAKekuatanSeeder::class);
       // $this->call(WPAKelemahanSeeder::class);
       // $this->call(WPASuasanaEmosiSeeder::class);
       // $this->call(WPAUraianKepribadianSeeder::class);
       // $this->call(WPAKuadranUserSeeder::class);
       // $this->call(WPAKuadranJobSeeder::class);
       // $this->call(PapiAcuanSeeder::class);
       // $this->call(JobsSeeder::class);
       // $this->call(JobsAgreementSeeder::class);
       // $this->call(JobsKarakteristikSeeder::class);
       // $this->call(JobsCompatibilityIndexSeeder::class);
       // $this->call(JobAnalysisSeeder::class);

       //kebutuhan tpa
       // $this->call(KategoriAspekSeeder::class);
       // $this->call(AspekPsikologisSeeder::class);
       // $this->call(KriteriaPsikographSeeder::class);
       // $this->call(BobotNilaiTableSeeder::class);
       // $this->call(AlatUkurTableSeeder::class);
       // $this->call(LookupNormaGTITableSeeder::class);
       // $this->call(LookupSubtesTikiTableSeeder::class);
       // $this->call(LookupMatriksAkhirDuaFaktor::class);
       // $this->call(LookupDISCFleksibilitasTableSeeder::class);
       // $this->call(LookupDISCSistematikaKerjaTableSeeder::class);
       // $this->call(LookupDISCAchievmentTableSeeder::class);
       // $this->call(LookupDISCKerjasamaTableSeeder::class);
       // $this->call(LookupDISCInterpersonalTableSeeder::class);
       // $this->call(LookupUraianLogikaBerpikirTableSeeder::class);
       // $this->call(LookupUraianKemampuanNumerikalTableSeeder::class);
       $this->call(LookupUraianDayaAnalisaTableSeeder::class);
       // $this->call(LookupUraianKemampuanVerbalTableSeeder::class);
       // $this->call(LookupUraianOrientasiHasilTableSeeder::class);
       // $this->call(LookupUraianFleksibilitasTableSeeder::class);
       // $this->call(LookupUraianSistematikaKerjaTableSeeder::class);
       // $this->call(LookupUraianAchievementTableSeeder::class);
       // $this->call(LookupUraianKerjasamaTableSeeder::class);
       // $this->call(LookupUraianInterpersonalTableSeeder::class);
       $this->call(LookupUraianKecerdasanUmumTableSeeder::class);

    }
}
