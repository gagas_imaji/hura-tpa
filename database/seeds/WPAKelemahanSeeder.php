<?php

use Illuminate\Database\Seeder;

class WPAKelemahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_wpa_kelemahan')->insert([
        	[
        		'D' => 	'Pendiriannya sangat keras',
	        	'I'	=>	'Terlalu reaktif',
	        	'S'	=>	'Kurang percaya diri',
	        	'C'	=>	'Menyalahkan diri sendiri'
        	],
        	[
        		'D' => 	'Terlalu cepat mengambil keputusan',
	        	'I'	=>	'Kurang tenang, tidak kalem',
	        	'S'	=>	'Cenderung pemalu',
	        	'C'	=>	'Cenderung Rigid'
        	],
        	[
        		'D' => 	'Tidak sabaran, suka menekan',
	        	'I'	=>	'Cenderung melebih-lebihkan sesuatu',
	        	'S'	=>	'Pesimis, penakut, selalu khawatir',
	        	'C'	=>	'Pesimis, seringkali berpikir negatif'
        	],
        	[
        		'D' => 	'Pendobrak, pemberontak',
	        	'I'	=>	'Pandai berpura-pura/basa basi',
	        	'S'	=>	'Kompromistis',
	        	'C'	=>	'Sedih tanpa alasan'
        	],
        	[
        		'D' => 	'Kurang peka terhadap perasaan orang lain',
	        	'I'	=>	'Terlalu bergairah, meledak-ledak',
	        	'S'	=>	'Membenarkan diri sendiri',
	        	'C'	=>	'Sangat berhati-hati'
        	],
        	[
        		'D' => 	'Terlalu percaya diri',
	        	'I'	=>	'Keputusannya sering berdasarkan emosi',
	        	'S'	=>	'Lamban, statis',
	        	'C'	=>	'Over Sensitif'
        	],
        	[
        		'D' => 	'Kurang menghargai pendapat orang lain',
	        	'I'	=>	'Tidak teratur, acak-acakan',
	        	'S'	=>	'Terlalu hati-hati',
	        	'C'	=>	'Sulit memutuskan'
        	],
        	[
        		'D' => 	'Cenderung suka mengakali',
	        	'I'	=>	'Tidak ketat dalam disiplin diri',
	        	'S'	=>	'Cenderung menghindari resiko',
	        	'C'	=>	'Tidak tegas'
        	],
        	[
        		'D' => 	'Kurang analitis, jemu dengan hal detail',
	        	'I'	=>	'Banyak waktu terbuang karena mengobrol berlebihan',
	        	'S'	=>	'Menolak/takut pada perubahan',
	        	'C'	=>	'Terlalu banyak teori, kurang praktis'
        	],
        	[
        		'D' => 	'Terlalu ingin dianggap sebagai pemimpin',
	        	'I'	=>	'Tidak terorganisir',
	        	'S'	=>	'Sulit memutuskan sesuatu',
	        	'C'	=>	'Mudah tersinggung'
        	],
        	[
        		'D' => 	'Cenderung mendominasi',
	        	'I'	=>	'Sulit mengingat nama orang dan hal detail',
	        	'S'	=>	'Kurang menunjukkan antusiasme kerja',
	        	'C'	=>	'Terlalu banyak waktu digunakan untuk persiapan'
        	],
        	[
        		'D' => 	'Memaksakan kehendak',
	        	'I'	=>	'Melebih-lebihkan',
	        	'S'	=>	'Sulit menolak permintaan orang lain',
	        	'C'	=>	'Terlalu fokus pada hal detail'
        	],
        ]);
    }
}
