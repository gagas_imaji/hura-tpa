<?php

use Illuminate\Database\Seeder;

class BankSurveyTikiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_survey')->insert([
        	[
	            'category_survey_id' 	=> 2,
	            'title' 				=> 'Test Intelegensi Kolektif Indonesia',
	            'slug'					=> 'test-intelegensi-kolektif-indonesia', 
	            'description'			=> '-',
	            'instruction'			=> 'Tes ini bertujuan untuk mengungkap inteligensi dengan standar Indonesia. Test ini terdiri dari 4 Subtest yang masing-masing memiliki instruksi dan waktu pengerjaan yang berbeda-beda',
	            'thankyou_text'			=> 'Terima kasih telah menyelesaikan Test ini',
	            'is_random'				=> 1,
	            'timer'					=> 0
        	]
        ]);
    }
}
