<?php

use Illuminate\Database\Seeder;

class LookupUraianFleksibilitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_fleksibilitas')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'Dirinya memerlukan waktu untuk beradaptasi terhadap perubahan-perubahan yang terjadi dalam lingkungan kerja.',
	            'desc2' => 'Ia enggan untuk berubah, memerlukan dorongan yang kuat dari lingkungannya.',
	            'desc3' => NULL
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'Pola kerjanya terbatas pada lingkungan kerja yang stabil dan teratur.',
	            'desc2' => 'Cenderung kurang fleksibel dalam bekerja, selalu menggunakan pola-pola lama dan agak lambat untuk menyesuaikan diri terhadap pekerjaan baru.',
	            'desc3' => NULL
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'Dirinya mampu beradaptasi terhadap perubahan dan situasi baru sesuai dengan tuntutan yang dibutuhkan lingkungannya.',
	            'desc2' => 'Menunjukkan fleksibilitas yang cukup baik dalam bekerja, dirinya mampu menangkap dan menerima tugas-tugas yang baru secara proporsional.',
	            'desc3' => 'antusias terhadap perubahan dan senang mencari hal-hal baru meskipun masih selektif, mempertimbangkan manfaat bagi dirinya.'
        	],
            [
                'nilai' => 5,
                'desc1' => 'Mudah beradaptasi dan menyukai perubahan. Ia senang bekerja dalam kondisi kerja yang dinamis.',
                'desc2' => 'Ia mampu beradaptasi dengan berbagai pekerjaan yang baru dan terbuka terhadap perubahan yang terjadi di lingkungan kerjanya.',
                'desc3' => NULL
            ],
            [
                'nilai' => 6,
                'desc1' => 'Ia sangat menyukai perubahan, gagasan baru, dan variasi dalam bekerja. Fleksibel dalam berpikir serta mudah beradaptasi dengan situasi yang berbeda-beda.',
                'desc2' => 'Pola berpikirnya yang dinamis memungkinkannya untuk memberikan ide atau cara-cara baru dalam proses kerja.',
                'desc3' => NULL
            ]

        ]);
    }
}
