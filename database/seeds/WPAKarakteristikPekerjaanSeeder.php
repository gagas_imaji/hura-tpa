<?php

use Illuminate\Database\Seeder;

class WPAKarakteristikPekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_wpa_karakteristik_pekerjaan')->insert([
        	[
        		'D' => 	'Pekerjaan yang membutuhkan pengambilan keputusan secara cepat',
	        	'I'	=>	'Banyak berhubungan dengan orang lain',
	        	'S'	=>	'Pekerjaan yang menuntut kesabaran',
	        	'C'	=>	'Pekerjaan yang menuntut akurasi dan ketepatan'
        	],
        	[
        		'D' => 	'Pekerjaan yang berorientasi kepada hasil',
	        	'I'	=>	'Pekerjaan yang memerlukan pendekatan persuasif',
	        	'S'	=>	'Mengikuti prosedur baku',
	        	'C'	=>	'Menganalisa masalah secara mendalam'
        	],
        	[
        		'D' => 	'Pekerjaan yang kompetitif dan banyak tantangan',
	        	'I'	=>	'Melakukan verbalisasi yang inspiratif',
	        	'S'	=>	'Klarifikasi kebijakan sebelum melangkah',
	        	'C'	=>	'Pendekatan kritis dalam menyelesaikan masalah'
        	],
        	[
        		'D' => 	'Bebas dari pekerjaan detail dan spesifik',
	        	'I'	=>	'Aktif dan mobilitas tinggi',
	        	'S'	=>	'Mengumpulkan informasi, fakta dan data',
	        	'C'	=>	'Menjelaskan secara menyeluruh dan sistimatis tentang tugas'
        	],
        	[
        		'D' => 	'Menggunakan kekuasaan dan wewenang',
	        	'I'	=>	'Kesempatan untuk mempresentasikan konsep baru',
	        	'S'	=>	'Melakukan penyiapan bahan-bahan pendukung',
	        	'C'	=>	'Mentaati prosedur operasi standar'
        	],
        	[
        		'D' => 	'Mengambil suatu gagasan dan menjalankannya',
	        	'I'	=>	'Mengembangkan kegiatan baru yang berbeda',
	        	'S'	=>	'Menyimpan dan memeliharan dokumen',
	        	'C'	=>	'Merancang dan mengembangkan program'
        	],
        	[
        		'D' => 	'Beragam kegiatan',
	        	'I'	=>	'Pekerjaan yang cepat berubah-ubah',
	        	'S'	=>	'Pekerjaan rutin dan administratif',
	        	'C'	=>	'Mengumpulkan informasi secara rinci'
        	],
        	[
        		'D' => 	'Bebas dari pengawasan langsung',
	        	'I'	=>	'Pekerjaan yang melibatkan hubungan dengan orang lain',
	        	'S'	=>	'Melaksanakan, mengelola pekerjaan sesuai peraturan',
	        	'C'	=>	'Melakukan evaluasi program'
        	],
        	[
        		'D' => 	'Memunculkan ide-ide baru',
	        	'I'	=>	'Melakukan pembinaan dan arahan kepada orang lain',
	        	'S'	=>	'Pekerjaan yang memerlukan konsistensi dalam jangka waktu panjang',
	        	'C'	=>	'Verifikasi, validasi dan pengendalian pekerjaan'
        	],
        ]);
    }
}
