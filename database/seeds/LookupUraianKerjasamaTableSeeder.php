<?php

use Illuminate\Database\Seeder;

class LookupUraianKerjasamaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_kerjasama')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'lebih senang untuk bekerja sendiri daripada terlibat dalam kegiatan kelompok.',
	            'desc2' => 'cenderung pasif dalam menjalin interaksi bersama anggota kelompok.',
	            'desc3' => NULL
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'menjalin relasi dengan orang lain sekedar untuk memenuhi tuntutan normatif. Kurang memiliki rasa kebersamaan dalam kelompok.',
	            'desc2' => 'memiliki kesadaran mengenai pentingnya bekerja sama sekedar untuk memenuhi tuntutan pekerjaan, belum melibatkan diri secara mendalam.',
	            'desc3' => NULL
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'memiliki minat untuk bekerja sama dan memiliki keinginan untuk  berkontribusi positif serta  mampu  menjalankan  perannya sesuai tuntutan yang diberikan.',
	            'desc2' => 'memiliki kesadaran mengenai pentingnya bekerja sama dan bersedia mengikuti aturan kelompok demi kepentingan bersama.',
	            'desc3' => NULL
        	],
            [
                'nilai' => 5,
                'desc1' => 'berorientasi pada pencapaian hasil kelompok, aktif berkoordinasi.',
                'desc2' => 'aktif memberikan kontribusi dalam upaya pencapaian tujuan kelompok.',
                'desc3' => NULL
            ],
            [
                'nilai' => 6,
                'desc1' => 'berorientasi pada kelompok, bisa menjadi motor penggerak dan mengarahkan orang lain untuk kepentingan bersama.',
                'desc2' => 'berorientasi pada pencapaian hasil kelompok, mampu mendorong kelompok untuk bekerja secara sinergi demi mencapai tujuan bersama.',
                'desc3' => 'berorientasi pada pencapaian hasil kelompok, aktif berkoordinasi dan bisa bersinergi dengan orang lain.'
            ]

        ]);
    }
}
