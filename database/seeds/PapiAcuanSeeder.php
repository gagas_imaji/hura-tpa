<?php

use Illuminate\Database\Seeder;

class PapiAcuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_papi_acuan_report')->insert([
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'0',
	        	'positive'	=>	'kurang bertanggung jawab, komitmen kerjanya rendah, cenderung meninggalkan tugas sebelum tuntas, suka berpindah-pindah kerja',
	        	'negative'	=>	'komitmen rendah, cenderung meninggalkan tugas sebelum tuntas, konsentrasi mudah buyar, suka berpindah-pindah pekerjaan'
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'1',
	        	'positive'	=>	'kurang bertanggung jawab, komitmen kerjanya rendah, cenderung meninggalkan tugas sebelum tuntas, suka berpindah-pindah kerja',
	        	'negative'	=>	'komitmen rendah, cenderung meninggalkan tugas sebelum tuntas, konsentrasi mudah buyar, suka berpindah-pindah pekerjaan'
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'2',
	        	'positive'	=>	'kurang bertanggung jawab, komitmen kerjanya rendah, cenderung meninggalkan tugas sebelum tuntas, suka berpindah-pindah kerja',
	        	'negative'	=>	'komitmen rendah, cenderung meninggalkan tugas sebelum tuntas, konsentrasi mudah buyar, suka berpindah-pindah pekerjaan'
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'3',
	        	'positive'	=>	'memiliki komitmen untuk menyelesaikan tugas, tetapi bila memungkinkan akan mendelegasikan sebagian pekerjaannya kepada orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'4',
	        	'positive'	=>	'memiliki komitmen untuk menyelesaikan tugas, tetapi bila memungkinkan akan mendelegasikan sebagian pekerjaannya kepada orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'5',
	        	'positive'	=>	'memiliki komitmen untuk menyelesaikan tugas, tetapi bila memungkinkan akan mendelegasikan sebagian pekerjaannya kepada orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'6',
	        	'positive'	=>	'tekun, bertanggung jawab atas pekerjaan yang dibebankan kepadanya, menyelesaikan tugas satu persatu',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'7',
	        	'positive'	=>	'tekun, bertanggung jawab atas pekerjaan yang dibebankan kepadanya, menyelesaikan tugas satu persatu',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'8',
	        	'positive'	=>	'memiliki komitmen yang sangat tinggi terhadap tugas, sangat ingin menyelesaikan tugas, tekun dan tuntas dalam menangani pekerjaan',
	        	'negative'	=>	'perhatian terpaku pada satu tugas, sulit menangani beberapa pekerjaan sekaligus, sulit diinterupsi'
        	],
        	[
        		'key' 		=> 	'N',
	        	'count'		=>	'9',
	        	'positive'	=>	'memiliki komitmen yang sangat tinggi terhadap tugas, sangat ingin menyelesaikan tugas, tekun dan tuntas dalam menangani pekerjaan',
	        	'negative'	=>	'perhatian terpaku pada satu tugas, sulit menangani beberapa pekerjaan sekaligus, sulit diinterupsi'
        	],
        	//
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'0',
	        	'positive'	=>	'santai, bekerja adalah sesuatu yang menyenangkan bukan beban, termotivasi untuk mencari cara atau sistem yang mempermudah dirinya dalam menyelesaikan pekerjaan',
	        	'negative'	=>	'menghindari kerja keras sehingga menimbulkan kesan malas'
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'1',
	        	'positive'	=>	'santai, bekerja adalah sesuatu yang menyenangkan bukan beban, termotivasi untuk mencari cara atau sistem yang mempermudah dirinya dalam menyelesaikan pekerjaan',
	        	'negative'	=>	'menghindari kerja keras sehingga menimbulkan kesan malas'
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'2',
	        	'positive'	=>	'bekerja keras sesuai tuntutan tugasnya, menyalurkan usahanya untuk hal-hal yang bermanfaat atau menguntungkan bagi dirinya',
	        	'negative'	=>	'menghindari kerja keras sehingga menimbulkan kesan malas'
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'3',
	        	'positive'	=>	'bekerja keras sesuai tuntutan tugasnya, menyalurkan usahanya untuk hal-hal yang bermanfaat atau menguntungkan bagi dirinya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'4',
	        	'positive'	=>	'bekerja keras namun jelas tujuan yang ingin dicapainya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'5',
	        	'positive'	=>	'bekerja keras namun jelas tujuan yang ingin dicapainya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'6',
	        	'positive'	=>	'bekerja keras namun jelas tujuan yang ingin dicapainya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'7',
	        	'positive'	=>	'bekerja keras namun jelas tujuan yang ingin dicapainya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'8',
	        	'positive'	=>	'ingin tampil sebagai pekerja keras, sangat menyukai bila orang lain memandangnya sebagai pekerja keras',
	        	'negative'	=>	'cenderung menciptakan pekerjaan yang tidak perlu agar terlihat sibuk'
        	],
        	[
        		'key' 		=> 	'G',
	        	'count'		=>	'9',
	        	'positive'	=>	'ingin tampil sebagai pekerja keras, sangat menyukai bila orang lain memandangnya sebagai pekerja keras',
	        	'negative'	=>	'cenderung menciptakan pekerjaan yang tidak perlu agar terlihat sibuk'
        	],
        	//
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'0',
	        	'positive'	=>	'santai, bekerja adalah sesuatu yang menyenangkan bukan beban, termotivasi untuk mencari cara atau sistem yang mempermudah dirinya dalam menyelesaikan pekerjaan',
	        	'negative'	=>	'menghindari kerja keras sehingga menimbulkan kesan malas'
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'1',
	        	'positive'	=>	'santai, bekerja adalah sesuatu yang menyenangkan bukan beban, termotivasi untuk mencari cara atau sistem yang mempermudah dirinya dalam menyelesaikan pekerjaan',
	        	'negative'	=>	'menghindari kerja keras sehingga menimbulkan kesan malas'
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'2',
	        	'positive'	=>	'bekerja keras sesuai tuntutan tugasnya, menyalurkan usahanya untuk hal-hal yang bermanfaat atau menguntungkan bagi dirinya',
	        	'negative'	=>	'menghindari kerja keras sehingga menimbulkan kesan malas'
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'3',
	        	'positive'	=>	'bekerja keras sesuai tuntutan tugasnya, menyalurkan usahanya untuk hal-hal yang bermanfaat atau menguntungkan bagi dirinya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'4',
	        	'positive'	=>	'bekerja keras namun jelas tujuan yang ingin dicapainya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'5',
	        	'positive'	=>	'bekerja keras namun jelas tujuan yang ingin dicapainya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'6',
	        	'positive'	=>	'bekerja keras namun jelas tujuan yang ingin dicapainya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'7',
	        	'positive'	=>	'bekerja keras namun jelas tujuan yang ingin dicapainya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'8',
	        	'positive'	=>	'ingin tampil sebagai pekerja keras, sangat menyukai bila orang lain memandangnya sebagai pekerja keras',
	        	'negative'	=>	'cenderung menciptakan pekerjaan yang tidak perlu agar terlihat sibuk'
        	],
        	[
        		'key' 		=> 	'A',
	        	'count'		=>	'9',
	        	'positive'	=>	'ingin tampil sebagai pekerja keras, sangat menyukai bila orang lain memandangnya sebagai pekerja keras',
	        	'negative'	=>	'cenderung menciptakan pekerjaan yang tidak perlu agar terlihat sibuk'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'0',
	        	'positive'	=>	'puas dengan peran sebagai bawahan, memberikan kesempatan pada orang lain untuk memimpin, tidak mendominasi orang lain, pasif dalam kelompok',
	        	'negative'	=>	'tidak percaya diri, sama sekali tidak berniat berperan sebagai pemimpin, pasif dalam situasi kelompok'
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'1',
	        	'positive'	=>	'puas dengan peran sebagai bawahan, memberikan kesempatan pada orang lain untuk memimpin, tidak mendominasi orang lain, pasif dalam kelompok',
	        	'negative'	=>	'tidak percaya diri, sama sekali tidak berniat berperan sebagai pemimpin, pasif dalam situasi kelompok'
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'2',
	        	'positive'	=>	'tidak percaya diri dan tidak ingin memimpin atau mengawasi orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'3',
	        	'positive'	=>	'tidak percaya diri dan tidak ingin memimpin atau mengawasi orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'4',
	        	'positive'	=>	'kurang percaya diri dan kurang berniat menjadi pemimpin',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'5',
	        	'positive'	=>	'cukup percaya diri, tidak secara aktif mencari posisi kepemimpinan, akan tapi tidak juga akan menghindarinya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'6',
	        	'positive'	=>	'percaya diri dan ingin berperan sebagai pemimpin',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'7',
	        	'positive'	=>	'percaya diri dan ingin berperan sebagai pemimpin',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'8',
	        	'positive'	=>	'percaya diri dan ingin berperan sebagai pemimpin',
	        	'negative'	=>	'efektivitas kelompok, mungkin akan tampil angkuh atau terlalu percaya diri'
        	],
        	[
        		'key' 		=> 	'L',
	        	'count'		=>	'9',
	        	'positive'	=>	'sangat percaya diri untuk berperan sebagai pemimpin dan sangat mendambakan posisi kepemimpinan',
	        	'negative'	=>	'efektivitas kelompok, mungkin akan tampil angkuh atau terlalu percaya diri'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'0',
	        	'positive'	=>	'permisif, akan memberikan kesempatan pada orang lain untuk memimpin',
	        	'negative'	=>	'tidak mau mengngontrol orang lain dan tidak mau mempertanggungjawaban hasil kerja bawahannya'
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'1',
	        	'positive'	=>	'permisif, akan memberikan kesempatan pada orang lain untuk memimpin',
	        	'negative'	=>	'tidak mau mengngontrol orang lain dan tidak mau mempertanggungjawaban hasil kerja bawahannya'
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'2',
	        	'positive'	=>	'enggan mengkontrol orang lain dan tidak mau bertanggung jawab hasil kerja bawahannya; lebih memberikan kebebasan pada bawahannya untuk memilih cara sendiri dalam menyelesaikan tugan dan meminta bantuan untuk mempertanggungjawabkan hasilnya masing-masing',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'3',
	        	'positive'	=>	'enggan mengkontrol orang lain dan tidak mau bertanggung jawab hasil kerja bawahannya; lebih memberikan kebebasan pada bawahannya untuk memilih cara sendiri dalam menyelesaikan tugan dan meminta bantuan untuk mempertanggungjawabkan hasilnya masing-masing',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'4',
	        	'positive'	=>	'cenderung enggan melaksanakan fungsi pengarahan, pengendalian dan pengawasan; kurang aktif memanfaatkan kapasitas bawahan secara optimal, cenderung berkerja sendiri untuk mencapai tujuan kelompok',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'5',
	        	'positive'	=>	'bertanggung jawab, akan melakukan fungsi pengarahan, pengendalian, dan pengawasan, tapi tidak mendominasi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'6',
	        	'positive'	=>	'dominan dan bertanggung jawab, akan melakukan fungsi pengarahan, pengendalian dan pengawasan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'7',
	        	'positive'	=>	'dominan dan bertanggung jawab, akan melakukan fungsi pengarahan, pengendalian dan pengawasan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'8',
	        	'positive'	=>	'sangat dominan, suka mempengaruhi dan mengawasi orang lain, bertanggung jawab atas tindakan dan hasil kerja bawahannya',
	        	'negative'	=>	'posesif, tidak ingin berada dibawah kontrol orang lain, cemas bila tidak berada pada posisi pemimpin, sulit diajak kerjasaman dengan rekan yang sejajar kedudukannya'
        	],
        	[
        		'key' 		=> 	'P',
	        	'count'		=>	'9',
	        	'positive'	=>	'sangat dominan, suka mempengaruhi dan mengawasi orang lain, bertanggung jawab atas tindakan dan hasil kerja bawahannya',
	        	'negative'	=>	'posesif, tidak ingin berada dibawah kontrol orang lain, cemas bila tidak berada pada posisi pemimpin, sulit diajak kerjasaman dengan rekan yang sejajar kedudukannya'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'0',
	        	'positive'	=>	'sangat berhati-hati, memikirkan langkah-langkahnya secara bersungguh-sungguh',
	        	'negative'	=>	'lamban dalam mengambil keputusan, terlalu lama merenung, cenderung menghindar menggambil suatu keputusan'
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'1',
	        	'positive'	=>	'sangat berhati-hati, memikirkan langkah-langkahnya secara bersungguh-sungguh',
	        	'negative'	=>	'lamban dalam mengambil keputusan, terlalu lama merenung, cenderung menghindar menggambil suatu keputusan'
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'2',
	        	'positive'	=>	'enggan mengambil keputusan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'3',
	        	'positive'	=>	'enggan mengambil keputusan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'4',
	        	'positive'	=>	'berhati-hati dalam mengambil keputusan, cenderung peragu',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'5',
	        	'positive'	=>	'cukup percaya diri dalam mengambil keputusan, mau mengambil resiko, dapat memutuskan dengan cepat, mengikuti alur logika',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'6',
	        	'positive'	=>	'cukup percaya diri dalam mengambil keputusan, mau mengambil resiko, dapat memutuskan dengan cepat, mengikuti alur logika',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'7',
	        	'positive'	=>	'cukup percaya diri dalam mengambil keputusan, mau mengambil resiko, dapat memutuskan dengan cepat, mengikuti alur logika',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'8',
	        	'positive'	=>	'sangat yakin mengambil keputusan, cepat tanggap terhadap situasi, berani mengambil resiko, mau memanfaatkan kesempatan',
	        	'negative'	=>	'impulsif, dapat membuat keputusan yang tidak praktis atau tidak ekonomis, cenderung meloncat pada keputusan'
        	],
        	[
        		'key' 		=> 	'I',
	        	'count'		=>	'9',
	        	'positive'	=>	'sangat yakin mengambil keputusan, cepat tanggap terhadap situasi, berani mengambil resiko, mau memanfaatkan kesempatan',
	        	'negative'	=>	'impulsif, dapat membuat keputusan yang tidak praktis atau tidak ekonomis, cenderung meloncat pada keputusan'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'0',
	        	'positive'	=>	'sangat berhati-hati, memikirkan langkah-langkahnya secara bersungguh-sungguhsantai, kurang peduli akan waktu, kurang memiliki rasa urgensi, membuang-buang waktu, bukan perkerja yang tepat waktu',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'1',
	        	'positive'	=>	'santai, kurang peduli akan waktu, kurang memiliki rasa urgensi, membuang-buang waktu, bukan perkerja yang tepat waktu',
	        	'negative'	=>	'kurang peduli akan waktu, kurang memiliki rasa urgensi, membuang-buang waktu, bukan berkerja yang tepat waktu'
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'2',
	        	'positive'	=>	'santai, kurang peduli akan waktu, kurang memiliki rasa urgensi, membuang-buang waktu, bukan perkerja yang tepat waktu',
	        	'negative'	=>	'kurang peduli akan waktu, kurang memiliki rasa urgensi, membuang-buang waktu, bukan berkerja yang tepat waktu'
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'3',
	        	'positive'	=>	'santai, kurang peduli akan waktu, kurang memiliki rasa urgensi, membuang-buang waktu, bukan perkerja yang tepat waktu',
	        	'negative'	=>	'kurang peduli akan waktu, kurang memiliki rasa urgensi, membuang-buang waktu, bukan berkerja yang tepat waktu'
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'4',
	        	'positive'	=>	'kurang peduli akan waktu, kurang memiliki rasa urgensi, bukan perkerja yang tepat waktu',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'5',
	        	'positive'	=>	'cukup aktif dalam segi mental, dan menyesuaikan tempo kerjanya dengan tuntutan pekerjaan atau lingkungan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'6',
	        	'positive'	=>	'cukup aktif dalam segi mental, dan menyesuaikan tempo kerjanya dengan tuntutan pekerjaan atau lingkungan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'7',
	        	'positive'	=>	'cukup aktif dalam segi mental, dan menyesuaikan tempo kerjanya dengan tuntutan pekerjaan atau lingkungan',
	        	'negative'	=>	'tegang, cemas, impulsif, mungkin ceroboh, banyak gerak yang tidak perlu'
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'8',
	        	'positive'	=>	'cekatan, selalu siaga, berkerja cepat, tergesa-gesa untuk segera menyelesaikan tugas',
	        	'negative'	=>	'tegang, cemas, impulsif, mungkin ceroboh, banyak gerak yang tidak perlu'
        	],
        	[
        		'key' 		=> 	'T',
	        	'count'		=>	'9',
	        	'positive'	=>	'cekatan, selalu siaga, berkerja cepat, tergesa-gesa untuk segera menyelesaikan tugas',
	        	'negative'	=>	'tegang, cemas, impulsif, mungkin ceroboh, banyak gerak yang tidak perlu'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'0',
	        	'positive'	=>	'',
	        	'negative'	=>	'lamban, tidak tanggap, mudah lelah, daya tahan fisik lemah'
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'1',
	        	'positive'	=>	'',
	        	'negative'	=>	'lamban, tidak tanggap, mudah lelah, daya tahan fisik lemah'
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'2',
	        	'positive'	=>	'',
	        	'negative'	=>	'lamban, tidak tanggap, mudah lelah, daya tahan fisik lemah'
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'3',
	        	'positive'	=>	'aktif, mampu melaksanakan tugas-tugas dengan mobilitas tinggi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'4',
	        	'positive'	=>	'aktif, mampu melaksanakan tugas-tugas dengan mobilitas tinggi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'5',
	        	'positive'	=>	'aktif, mampu melaksanakan tugas-tugas dengan mobilitas tinggi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'6',
	        	'positive'	=>	'aktif, mampu melaksanakan tugas-tugas dengan mobilitas tinggi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'7',
	        	'positive'	=>	'menyukai aktivitas fisik, enerjik, memiliki stamina untuk menangani tugas-tugas berat, tidak mudah lelah',
	        	'negative'	=>	'tidak betah duduk lama atau kurang dapat berkonsentrasi dibelakang meja'
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'8',
	        	'positive'	=>	'menyukai aktivitas fisik, enerjik, memiliki stamina untuk menangani tugas-tugas berat, tidak mudah lelah',
	        	'negative'	=>	'tidak betah duduk lama atau kurang dapat berkonsentrasi dibelakang meja'
        	],
        	[
        		'key' 		=> 	'V',
	        	'count'		=>	'9',
	        	'positive'	=>	'menyukai aktivitas fisik, enerjik, memiliki stamina untuk menangani tugas-tugas berat, tidak mudah lelah',
	        	'negative'	=>	'tidak betah duduk lama atau kurang dapat berkonsentrasi dibelakang meja'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'0',
	        	'positive'	=>	'kaku dalam bergaul, tidak memerlukan kehadiran seseorang, canggung dalam lingkungan sosial',
	        	'negative'	=>	'menarik diri, kaku dalam bergaul, cangguh dalam lingkungan sosial, lebih memperhatikan hal-hal lain dari pada manusia'
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'1',
	        	'positive'	=>	'kaku dalam bergaul, tidak memerlukan kehadiran seseorang, canggung dalam lingkungan sosial',
	        	'negative'	=>	'menarik diri, kaku dalam bergaul, cangguh dalam lingkungan sosial, lebih memperhatikan hal-hal lain dari pada manusia'
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'2',
	        	'positive'	=>	'kaku dalam bergaul, tidak memerlukan kehadiran seseorang, canggung dalam lingkungan sosial',
	        	'negative'	=>	'menarik diri, kaku dalam bergaul, cangguh dalam lingkungan sosial, lebih memperhatikan hal-hal lain dari pada manusia'
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'3',
	        	'positive'	=>	'kurang percaya diri, kurang aktif dalam menjalin hubungan sosial',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'4',
	        	'positive'	=>	'kurang percaya diri, kurang aktif dalam menjalin hubungan sosial',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'5',
	        	'positive'	=>	'percaya diri dan sangat senang bergaul, menyukai interaksi sosial, bisa menciptakan situasi yang menyenangkan, mempunyai inisiatif dan mampu menjalin hubungan dan komunikasi, memperhatikan orang lain',
	        	'negative'	=>	'mungkin membuang-buang waktu untuk aktivitas sosial, kurang peduli akan penyelesaian tugas'
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'6',
	        	'positive'	=>	'percaya diri dan sangat senang bergaul, menyukai interaksi sosial, bisa menciptakan situasi yang menyenangkan, mempunyai inisiatif dan mampu menjalin hubungan dan komunikasi, memperhatikan orang lain',
	        	'negative'	=>	'mungkin membuang-buang waktu untuk aktivitas sosial, kurang peduli akan penyelesaian tugas'
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'7',
	        	'positive'	=>	'percaya diri dan sangat senang bergaul, menyukai interaksi sosial, bisa menciptakan situasi yang menyenangkan, mempunyai inisiatif dan mampu menjalin hubungan dan komunikasi, memperhatikan orang lain',
	        	'negative'	=>	'mungkin membuang-buang waktu untuk aktivitas sosial, kurang peduli akan penyelesaian tugas'
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'8',
	        	'positive'	=>	'menyukai aktivitas fisik, enerjik, memiliki stamina untuk menangani tugas-tugas berat, tidak mudah lelah',
	        	'negative'	=>	'mungkin membuang-buang waktu untuk aktivitas sosial, kurang peduli akan penyelesaian tugas'
        	],
        	[
        		'key' 		=> 	'S',
	        	'count'		=>	'9',
	        	'positive'	=>	'menyukai aktivitas fisik, enerjik, memiliki stamina untuk menangani tugas-tugas berat, tidak mudah lelah',
	        	'negative'	=>	'mungkin membuang-buang waktu untuk aktivitas sosial, kurang peduli akan penyelesaian tugas'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'0',
	        	'positive'	=>	'kaku dalam bergaul, tidak memerlukan kehadiran seseorang, canggung dalam lingkungan sosial',
	        	'negative'	=>	'menarik diri, kaku dalam bergaul, cangguh dalam lingkungan sosial, lebih memperhatikan hal-hal lain dari pada manusia'
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'1',
	        	'positive'	=>	'menyendiri, kurang peka akan sikap dan kebutuhan kelompok, sulit menyesuaikan diri',
	        	'negative'	=>	'menyendiri, kurang peka akan sikap dan kebutuhan kelompok, mungkin sulit menyesuaikan diri'
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'2',
	        	'positive'	=>	'menyendiri, kurang peka akan sikap dan kebutuhan kelompok, sulit menyesuaikan diri',
	        	'negative'	=>	'menyendiri, kurang peka akan sikap dan kebutuhan kelompok, mungkin sulit menyesuaikan diri'
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'3',
	        	'positive'	=>	'selektif dalam bergabung dengan kelompok, hanya mau berhubungan dengan kelompok di lingkungan kerja apabila bernilai dan sesuai minat, tidak mudah dipengaruhi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'4',
	        	'positive'	=>	'selektif dalam bergabung dengan kelompok, hanya mau berhubungan dengan kelompok di lingkungan kerja apabila bernilai dan sesuai minat, tidak mudah dipengaruhi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'5',
	        	'positive'	=>	'selektif dalam bergabung dengan kelompok, hanya mau berhubungan dengan kelompok di lingkungan kerja apabila bernilai dan sesuai minat, tidak mudah dipengaruhi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'6',
	        	'positive'	=>	'suka bergabung dengan kelompok, sadar akan sikap dan kebutuhan kelompok, suka berkerja dengan orang lain, ingin menjadi bagian dari kelompok, ingin disukai oleh lingkungan',
	        	'negative'	=>	'sangat tergantung dengan kelompok, dapat menjadi lebih memperhatikan kebutuhan kelompok dari padakebutuhan pekerjaan'
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'7',
	        	'positive'	=>	'suka bergabung dengan kelompok, sadar akan sikap dan kebutuhan kelompok, suka berkerja dengan orang lain, ingin menjadi bagian dari kelompok, ingin disukai oleh lingkungan',
	        	'negative'	=>	'sangat tergantung dengan kelompok, dapat menjadi lebih memperhatikan kebutuhan kelompok dari padakebutuhan pekerjaan'
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'8',
	        	'positive'	=>	'suka bergabung dengan kelompok, sadar akan sikap dan kebutuhan kelompok, suka berkerja dengan orang lain, ingin menjadi bagian dari kelompok, ingin disukai oleh lingkungan',
	        	'negative'	=>	'sangat tergantung dengan kelompok, dapat menjadi lebih memperhatikan kebutuhan kelompok dari padakebutuhan pekerjaan'
        	],
        	[
        		'key' 		=> 	'B',
	        	'count'		=>	'9',
	        	'positive'	=>	'suka bergabung dengan kelompok, sadar akan sikap dan kebutuhan kelompok, suka berkerja dengan orang lain, ingin menjadi bagian dari kelompok, ingin disukai oleh lingkungan',
	        	'negative'	=>	'sangat tergantung dengan kelompok, dapat menjadi lebih memperhatikan kebutuhan kelompok dari padakebutuhan pekerjaan'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'0',
	        	'positive'	=>	'menjaga jarak, lebih memperhatikan hal-hal yang bersifat formal, tidak mudah dipengaruhi oleh orang lain',
	        	'negative'	=>	'tampil dingin, tidak acuh, tidak ramah, suka berahasia, mungkin tidak sadar akan perasaan orang lain, dan mungkin dapat mengalami masalah kecocokan dengan orang lain'
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'1',
	        	'positive'	=>	'menjaga jarak, lebih memperhatikan hal-hal yang bersifat formal, tidak mudah dipengaruhi oleh orang lain',
	        	'negative'	=>	'tampil dingin, tidak acuh, tidak ramah, suka berahasia, mungkin tidak sadar akan perasaan orang lain, dan mungkin dapat mengalami masalah kecocokan dengan orang lain'
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'2',
	        	'positive'	=>	'menjaga jarak, lebih memperhatikan hal-hal yang bersifat formal, tidak mudah dipengaruhi oleh orang lain',
	        	'negative'	=>	'tampil dingin, tidak acuh, tidak ramah, suka berahasia, mungkin tidak sadar akan perasaan orang lain, dan mungkin dapat mengalami masalah kecocokan dengan orang lain'
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'3',
	        	'positive'	=>	'menghindari hubungan antara pribadi dalam lingkungan kerja, masih mampu menjaga jarak',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'4',
	        	'positive'	=>	'menghindari hubungan antara pribadi dalam lingkungan kerja, masih mampu menjaga jarak',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'5',
	        	'positive'	=>	'menghindari hubungan antara pribadi dalam lingkungan kerja, masih mampu menjaga jarak',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'6',
	        	'positive'	=>	'peka akan hubungan dengan orang lain, sangat memikirkan hal-hal yang dianggap penting oleh orang lain, suka menjalin hubungan persahabatan yang hangat dan tulus',
	        	'negative'	=>	'sangat perasa, mudah tersinggung, cenderung subyektif dalam hubungan dengan orang lain, dapat terlibat terlalu dalam dengan individu tertentu dilingkungan kerja, sangat tergan tung pada individu tertentu'
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'7',
	        	'positive'	=>	'peka akan hubungan dengan orang lain, sangat memikirkan hal-hal yang dianggap penting oleh orang lain, suka menjalin hubungan persahabatan yang hangat dan tulus',
	        	'negative'	=>	'sangat perasa, mudah tersinggung, cenderung subyektif dalam hubungan dengan orang lain, dapat terlibat terlalu dalam dengan individu tertentu dilingkungan kerja, sangat tergan tung pada individu tertentu'
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'8',
	        	'positive'	=>	'peka akan hubungan dengan orang lain, sangat memikirkan hal-hal yang dianggap penting oleh orang lain, suka menjalin hubungan persahabatan yang hangat dan tulus',
	        	'negative'	=>	'sangat perasa, mudah tersinggung, cenderung subyektif dalam hubungan dengan orang lain, dapat terlibat terlalu dalam dengan individu tertentu dilingkungan kerja, sangat tergan tung pada individu tertentu'
        	],
        	[
        		'key' 		=> 	'O',
	        	'count'		=>	'9',
	        	'positive'	=>	'peka akan hubungan dengan orang lain, sangat memikirkan hal-hal yang dianggap penting oleh orang lain, suka menjalin hubungan persahabatan yang hangat dan tulus',
	        	'negative'	=>	'sangat perasa, mudah tersinggung, cenderung subyektif dalam hubungan dengan orang lain, dapat terlibat terlalu dalam dengan individu tertentu dilingkungan kerja, sangat tergan tung pada individu tertentu'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'0',
	        	'positive'	=>	'sederhana, cenderung diam, pemalu, tidak suka menonjolkan diri',
	        	'negative'	=>	'terlalu sederhana, cenderung merendahkan kapasitas sendiri, tidak mencerminkan kepercayaan diri, pemalu'
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'1',
	        	'positive'	=>	'sederhana, cenderung diam, pemalu, tidak suka menonjolkan diri',
	        	'negative'	=>	'terlalu sederhana, cenderung merendahkan kapasitas sendiri, tidak mencerminkan kepercayaan diri, pemalu'
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'2',
	        	'positive'	=>	'sederhana rendah hati, tulus, tidak sombong, tidak salah menampilkan diri',
	        	'negative'	=>	'terlalu sederhana, cenderung merendahkan kapasitas sendiri, tidak mencerminkan kepercayaan diri, pemalu'
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'3',
	        	'positive'	=>	'sederhana rendah hati, tulus, tidak sombong, tidak salah menampilkan diri',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'4',
	        	'positive'	=>	'mengharapkan pengakuan lingkungan, tidak mau diabaikan tapi tidak mencari-cari perhatian',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'5',
	        	'positive'	=>	'mengharapkan pengakuan lingkungan, tidak mau diabaikan tapi tidak mencari-cari perhatian',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'6',
	        	'positive'	=>	'memiliki rasa bangga akan diri, senang menjadi pusat perhatian, mengharapkan penghargaan dari lingkungan',
	        	'negative'	=>	'mencari-cari perhatian dan suka menonjolkan diri'
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'7',
	        	'positive'	=>	'memiliki rasa bangga akan diri, senang menjadi pusat perhatian, mengharapkan penghargaan dari lingkungan',
	        	'negative'	=>	'mencari-cari perhatian dan suka menonjolkan diri'
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'8',
	        	'positive'	=>	'memiliki rasa bangga akan diri, senang menjadi pusat perhatian, mengharapkan penghargaan dari lingkungan',
	        	'negative'	=>	'mencari-cari perhatian dan suka menonjolkan diri'
        	],
        	[
        		'key' 		=> 	'X',
	        	'count'		=>	'9',
	        	'positive'	=>	'memiliki rasa bangga akan diri, senang menjadi pusat perhatian, mengharapkan penghargaan dari lingkungan',
	        	'negative'	=>	'mencari-cari perhatian dan suka menonjolkan diri'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'0',
	        	'positive'	=>	'fleksibel, kurang memperdulikan keteraturan, kerapihan, mudah beradaptasi dan tidak kaku',
	        	'negative'	=>	'tidak memperdulikan keteraturan dan kerapihan, cenderung ceroboh'
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'1',
	        	'positive'	=>	'fleksibel, kurang memperdulikan keteraturan, kerapihan, mudah beradaptasi dan tidak kaku',
	        	'negative'	=>	'tidak memperdulikan keteraturan dan kerapihan, cenderung ceroboh'
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'2',
	        	'positive'	=>	'fleksibel, kurang memperdulikan keteraturan, kerapihan, mudah beradaptasi dan tidak kaku',
	        	'negative'	=>	'tidak memperdulikan keteraturan dan kerapihan, cenderung ceroboh'
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'3',
	        	'positive'	=>	'cukup fleksibel namun masih memperhatikan keteraturan maupun sistimatika kerja',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'4',
	        	'positive'	=>	'cukup fleksibel namun masih memperhatikan keteraturan maupun sistimatika kerja',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'5',
	        	'positive'	=>	'memperhatikan keteraturan dan sistimatika kerja, tapi juga cukup fleksibel',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'6',
	        	'positive'	=>	'memperhatikan keteraturan dan sistimatika kerja, tapi juga cukup fleksibel',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'7',
	        	'positive'	=>	'sistimatis, memiliki metoda, rapi dan teratur, dapat menata tugas dengan baik',
	        	'negative'	=>	'cenderung kaku, tidak fleksibel'
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'8',
	        	'positive'	=>	'sistimatis, memiliki metoda, rapi dan teratur, dapat menata tugas dengan baik',
	        	'negative'	=>	'cenderung kaku, tidak fleksibel'
        	],
        	[
        		'key' 		=> 	'C',
	        	'count'		=>	'9',
	        	'positive'	=>	'sistimatis, memiliki metoda, rapi dan teratur, dapat menata tugas dengan baik',
	        	'negative'	=>	'cenderung kaku, tidak fleksibel'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'0',
	        	'positive'	=>	'melihat pekerjaan secara makro serta menghindari detail, bisa membedakan hal penting dan tidak penting, kurang teliti',
	        	'negative'	=>	'menghindari detail, bisa bertindak tanpa data yang cukup atau akurat, bertindak ceroboh untuk hal-hal yang memerlukan ketelitian/kecermatan'
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'1',
	        	'positive'	=>	'melihat pekerjaan secara makro serta menghindari detail, bisa membedakan hal penting dan tidak penting, kurang teliti',
	        	'negative'	=>	'menghindari detail, bisa bertindak tanpa data yang cukup atau akurat, bertindak ceroboh untuk hal-hal yang memerlukan ketelitian/kecermatan'
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'2',
	        	'positive'	=>	'cukup peduli akan akurasi dan kelengkapan data',
	        	'negative'	=>	'tidak memperdulikan keteraturan dan kerapihan, cenderung ceroboh'
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'3',
	        	'positive'	=>	'cukup peduli akan akurasi dan kelengkapan data',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'4',
	        	'positive'	=>	'cenderung menangani sendiri pekerjaan detail',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'5',
	        	'positive'	=>	'menangani sendiri pekerjaan detail',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'6',
	        	'positive'	=>	'menangani sendiri pekerjaan detail',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'7',
	        	'positive'	=>	'sangat menyukai detail, sangat peduli akan akurasi dan kelengkapan data, penuh perencanaan',
	        	'negative'	=>	'terpaku pada detail sehingga melupakan tujuan utama'
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'8',
	        	'positive'	=>	'sangat menyukai detail, sangat peduli akan akurasi dan kelengkapan data, penuh perencanaan',
	        	'negative'	=>	'terpaku pada detail sehingga melupakan tujuan utama'
        	],
        	[
        		'key' 		=> 	'D',
	        	'count'		=>	'9',
	        	'positive'	=>	'sangat menyukai detail, sangat peduli akan akurasi dan kelengkapan data, penuh perencanaan',
	        	'negative'	=>	'terpaku pada detail sehingga melupakan tujuan utama'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'0',
	        	'positive'	=>	'tipe pelaksana, bukan perencana, praktis, pragmatis, mengandalkan pengalaman masa lalu',
	        	'negative'	=>	'bekerja tanpa perencanaan, mengandalkan perasaan'
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'1',
	        	'positive'	=>	'tipe pelaksana, bukan perencana, praktis, pragmatis, mengandalkan pengalaman masa lalu',
	        	'negative'	=>	'bekerja tanpa perencanaan, mengandalkan perasaan'
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'2',
	        	'positive'	=>	'tipe pelaksana, bukan perencana, praktis, pragmatis, mengandalkan pengalaman masa lalu',
	        	'negative'	=>	'bekerja tanpa perencanaan, mengandalkan perasaan'
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'3',
	        	'positive'	=>	'tipe pelaksana, bukan perencana, praktis, pragmatis, mengandalkan pengalaman masa lalu',
	        	'negative'	=>	'bekerja tanpa perencanaan, mengandalkan perasaan'
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'4',
	        	'positive'	=>	'mempertimbangkan aspek teoritis (konsep atau pemikiran baru) dan aspek praktis (pengalaman) secara berimbang',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'5',
	        	'positive'	=>	'mempertimbangkan aspek teoritis (konsep atau pemikiran baru) dan aspek praktis (pengalaman) secara berimbang',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'6',
	        	'positive'	=>	'seringkali menganalisis problem secara mendalam, merujuk pada teori dan konsep',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'7',
	        	'positive'	=>	'seringkali menganalisis problem secara mendalam, merujuk pada teori dan konsep',
	        	'negative'	=>	'terpaku pada detail sehingga melupakan tujuan utama'
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'8',
	        	'positive'	=>	'tipe pemikir, sangat berminat pada gagasan, konsep, teori, mencari alternatif-alternatif baru, menyukai perencanaan',
	        	'negative'	=>	'mungkin sulit dimengerti oleh orang lain, terlalu teoritis dan praktis, terlalu mengawang, berbelit-belit'
        	],
        	[
        		'key' 		=> 	'R',
	        	'count'		=>	'9',
	        	'positive'	=>	'tipe pemikir, sangat berminat pada gagasan, konsep, teori, mencari alternatif-alternatif baru, menyukai perencanaan',
	        	'negative'	=>	'mungkin sulit dimengerti oleh orang lain, terlalu teoritis dan praktis, terlalu mengawang, berbelit-belit'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'0',
	        	'positive'	=>	'sangat terbuka, terus terang, kurang dapat mengendalikan emosi, reaktif',
	        	'negative'	=>	'tidak dapat mengendalikan emosi, cepat bereaksi, kurang mempunyai nilai yang mengharuskannya menahan emosi'
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'1',
	        	'positive'	=>	'sangat terbuka, terus terang, kurang dapat mengendalikan emosi, reaktif',
	        	'negative'	=>	'tidak dapat mengendalikan emosi, cepat bereaksi, kurang mempunyai nilai yang mengharuskannya menahan emosi'
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'2',
	        	'positive'	=>	'terbuka, mudah mengungkapkan pendapat atau perasaannya mengenai suatu hal kepada orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'3',
	        	'positive'	=>	'terbuka, mudah mengungkapkan pendapat atau perasaannya mengenai suatu hal kepada orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'4',
	        	'positive'	=>	'mampu mengungkapkan atau mampu menyimpan perasaan, dapat mengendalikan emosi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'5',
	        	'positive'	=>	'mampu mengungkapkan atau mampu menyimpan perasaan, dapat mengendalikan emosi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'6',
	        	'positive'	=>	'mampu mengungkapkan atau mampu menyimpan perasaan, dapat mengendalikan emosi',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'7',
	        	'positive'	=>	'mampu menyimpan pendapat atau perasaannya bagi diri sendiri, tenang, dapat mengendalikan emosi',
	        	'negative'	=>	'mungkin tampil pasif dan tak acuh, mungkin sulit mengungkapkan emosi, perasaan, pandangan'
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'8',
	        	'positive'	=>	'mampu menyimpan pendapat atau perasaannya bagi diri sendiri, tenang, dapat mengendalikan emosi',
	        	'negative'	=>	'mungkin tampil pasif dan tak acuh, mungkin sulit mengungkapkan emosi, perasaan, pandangan'
        	],
        	[
        		'key' 		=> 	'E',
	        	'count'		=>	'9',
	        	'positive'	=>	'mampu menyimpan pendapat atau perasaannya bagi diri sendiri, tenang, dapat mengendalikan emosi',
	        	'negative'	=>	'mungkin tampil pasif dan tak acuh, mungkin sulit mengungkapkan emosi, perasaan, pandangan'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'0',
	        	'positive'	=>	'sabar, menghindari konflik, pasif, menyembunyikan perasaan yang sesungguhnya',
	        	'negative'	=>	'menghindar dari konflik, pasif, menyembunyikan perasaannya sebenarnya, menghindari konfrontasi, lari dari konflik, tidak mau mengakui adanya konflik'
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'1',
	        	'positive'	=>	'sabar, menghindari konflik, pasif, menyembunyikan perasaan yang sesungguhnya',
	        	'negative'	=>	'menghindar dari konflik, pasif, menyembunyikan perasaannya sebenarnya, menghindari konfrontasi, lari dari konflik, tidak mau mengakui adanya konflik'
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'2',
	        	'positive'	=>	'lebih suka menghindari konflik, mampu melihat permasalahan dari sudut pandang orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'3',
	        	'positive'	=>	'lebih suka menghindari konflik, mampu melihat permasalahan dari sudut pandang orang lain',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'4',
	        	'positive'	=>	'tidak menghindari konflik, mau mendengarkan pandangan orang lain namun dapat menjadi keras kepala ketika mempertahankan pandangannya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'5',
	        	'positive'	=>	'tidak menghindari konflik, mau mendengarkan pandangan orang lain namun dapat menjadi keras kepala ketika mempertahankan pandangannya',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'6',
	        	'positive'	=>	'berani menghadapi konflik, mengungkapkan serta memaksakan pandangan dengan cara positif',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'7',
	        	'positive'	=>	'berani menghadapi konflik, mengungkapkan serta memaksakan pandangan dengan cara positif',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'8',
	        	'positive'	=>	'terbuka, jujur, terus terang, mempertahankan diri dengan agresif',
	        	'negative'	=>	'agresif, reaktif, mudah tersinggung, mudah meledak, curiga, berprasangka, suka berkelahi, berpikiran negatif'
        	],
        	[
        		'key' 		=> 	'K',
	        	'count'		=>	'9',
	        	'positive'	=>	'terbuka, jujur, terus terang, mempertahankan diri dengan agresif',
	        	'negative'	=>	'agresif, reaktif, mudah tersinggung, mudah meledak, curiga, berprasangka, suka berkelahi, berpikiran negatif'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'0',
	        	'positive'	=>	'mudah bekerja rutin tanpa merasa bosan, tidak membutuhkan variasi, menyukai lingkungan yang stabil',
	        	'negative'	=>	'konservatif, menolak perubahan, sulit menerima hal baru, tidak dapat beradaptasi dengan situasi yang berbeda-beda'
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'1',
	        	'positive'	=>	'mudah bekerja rutin tanpa merasa bosan, tidak membutuhkan variasi, menyukai lingkungan yang stabil',
	        	'negative'	=>	'konservatif, menolak perubahan, sulit menerima hal baru, tidak dapat beradaptasi dengan situasi yang berbeda-beda'
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'2',
	        	'positive'	=>	'enggan berubah, tidak siap untuk beradaptasi, hanya mau menerima perubahan jika alasannya jelas dan meyakinkan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'3',
	        	'positive'	=>	'enggan berubah, tidak siap untuk beradaptasi, hanya mau menerima perubahan jika alasannya jelas dan meyakinkan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'4',
	        	'positive'	=>	'mudah beradaptasi, cukup menyukai perubahan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'5',
	        	'positive'	=>	'mudah beradaptasi, cukup menyukai perubahan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'6',
	        	'positive'	=>	'antusias terhadap perubahan dan akan mencari hal-hal baru, tapi masih selektif',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'7',
	        	'positive'	=>	'antusias terhadap perubahan dan akan mencari hal-hal baru, tapi masih selektif',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'8',
	        	'positive'	=>	'sangat menyukai perubahan, gagasan baru, variasi, aktif mencari perubahan, antusias akan hal baru, fleksibel dan mudah beradaptasi dengan situasi yang berbeda',
	        	'negative'	=>	'gelisah, frustasi, mudah bosan; sangat membutuhkan variasi, tidak menyukai tugas atau situasi yang bersifat rutin atau monoton'
        	],
        	[
        		'key' 		=> 	'Z',
	        	'count'		=>	'9',
	        	'positive'	=>	'sangat menyukai perubahan, gagasan baru, variasi, aktif mencari perubahan, antusias akan hal baru, fleksibel dan mudah beradaptasi dengan situasi yang berbeda',
	        	'negative'	=>	'gelisah, frustasi, mudah bosan; sangat membutuhkan variasi, tidak menyukai tugas atau situasi yang bersifat rutin atau monoton'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'0',
	        	'positive'	=>	'mandiri, memiliki inisiatif, cenderung mengabaikan atau tidak memahami pentingnya peraturan atau prosedur',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'1',
	        	'positive'	=>	'mandiri, memiliki inisiatif, cenderung mengabaikan atau tidak memahami pentingnya peraturan atau prosedur',
	        	'negative'	=>	'tidak patuh, cenderung mengabaikan atau tidak memehami pentingnya peraturan atau prosedur, suka menciptakan peraturan sendiri yang mungkin bertentangan dengan peraturan yang telah ada'
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'2',
	        	'positive'	=>	'mandiri, memiliki inisiatif, cenderung mengabaikan atau tidak memahami pentingnya peraturan atau prosedur',
	        	'negative'	=>	'tidak patuh, cenderung mengabaikan atau tidak memehami pentingnya peraturan atau prosedur, suka menciptakan peraturan sendiri yang mungkin bertentangan dengan peraturan yang telah ada'
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'3',
	        	'positive'	=>	'mandiri, memiliki inisiatif, cenderung mengabaikan atau tidak memahami pentingnya peraturan atau prosedur',
	        	'negative'	=>	'tidak patuh, cenderung mengabaikan atau tidak memehami pentingnya peraturan atau prosedur, suka menciptakan peraturan sendiri yang mungkin bertentangan dengan peraturan yang telah ada'
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'4',
	        	'positive'	=>	'perlu pengarahan awal, menuntut standar tugas yang harus dilakukan dan ukuran keberhasilan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'5',
	        	'positive'	=>	'perlu pengarahan awal, menuntut standar tugas yang harus dilakukan dan ukuran keberhasilan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'6',
	        	'positive'	=>	'membutuhkan uraian rinci mengenai tugas dan batas tanggung jawab serta wewenang',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'7',
	        	'positive'	=>	'membutuhkan uraian rinci mengenai tugas dan batas tanggung jawab serta wewenang',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'8',
	        	'positive'	=>	'patuh pada kebijaksanaan, peraturan dan struktur organisasi, ingin segala sesuatu diuraikan secara rinci',
	        	'negative'	=>	'ingin segala sesuatunya diuraikan secara rinci, kurang memiliki innisyatif, bergantung pada organisasi, berhara "disuapi" instruksi'
        	],
        	[
        		'key' 		=> 	'W',
	        	'count'		=>	'9',
	        	'positive'	=>	'patuh pada kebijaksanaan, peraturan dan struktur organisasi, ingin segala sesuatu diuraikan secara rinci',
	        	'negative'	=>	'ingin segala sesuatunya diuraikan secara rinci, kurang memiliki innisyatif, bergantung pada organisasi, berhara "disuapi" instruksi'
        	],
        	//
        	//
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'0',
	        	'positive'	=>	'dapat berkerja sendiri tanpa bantuan orang lain, termotivasi secara internal bukan dari pujian dan otoritas',
	        	'negative'	=>	'mempertanyakan otoritas, cenderung tidak puas terhadap atasan, lebih didasari kepentingan pribadi'
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'1',
	        	'positive'	=>	'dapat berkerja sendiri tanpa bantuan orang lain, termotivasi secara internal bukan dari pujian dan otoritas',
	        	'negative'	=>	'mempertanyakan otoritas, cenderung tidak puas terhadap atasan, lebih didasari kepentingan pribadi'
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'2',
	        	'positive'	=>	'dapat berkerja sendiri tanpa bantuan orang lain, termotivasi secara internal bukan dari pujian dan otoritas',
	        	'negative'	=>	'mempertanyakan otoritas, cenderung tidak puas terhadap atasan, lebih didasari kepentingan pribadi'
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'3',
	        	'positive'	=>	'dapat berkerja sendiri tanpa bantuan orang lain, termotivasi secara internal bukan dari pujian dan otoritas',
	        	'negative'	=>	'mempertanyakan otoritas, cenderung tidak puas terhadap atasan, lebih didasari kepentingan pribadi'
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'4',
	        	'positive'	=>	'loyal pada perusahaan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'5',
	        	'positive'	=>	'loyal pada perusahaan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'6',
	        	'positive'	=>	'loyal pada perusahaan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'7',
	        	'positive'	=>	'loyal pada pribadi atasan',
	        	'negative'	=>	''
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'8',
	        	'positive'	=>	'loyal, berusaha dekat dengan pribadi atasan, ingin menyenangkan atasan, sadar akan harapan atasan dan dirinya',
	        	'negative'	=>	'terlalu memperhatikan cara menyenangkan atasan, tidak berani berpendirian lain, tidak mandiri'
        	],
        	[
        		'key' 		=> 	'F',
	        	'count'		=>	'9',
	        	'positive'	=>	'loyal, berusaha dekat dengan pribadi atasan, ingin menyenangkan atasan, sadar akan harapan atasan dan dirinya',
	        	'negative'	=>	'terlalu memperhatikan cara menyenangkan atasan, tidak berani berpendirian lain, tidak mandiri'
        	],
        	//
        ]);
    }
}
