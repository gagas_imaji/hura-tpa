<?php

use Illuminate\Database\Seeder;

class JobsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('jobs')->insert([
        [    
            'jobs_name'      =>'Accounting',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_name'      =>'Sekretaris',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_name'      =>'Supervisor',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_name'      =>'Head of Operation',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_name'      =>'HR MANAGER',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_name'      =>'Accounting Manager',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_name'      =>'Cleaning service operator',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_name'      =>'Field operation officer',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ],
        [    
            'jobs_name'      =>'Cleaning service operator',
            'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
        ]
    ]);
    }
}
