<?php

use Illuminate\Database\Seeder;

class WPAPerilakuKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_wpa_perilaku_kerja')->insert([
        	[
        		'D' => 	'Organisator dan promotor',
	        	'I'	=>	'Mampu memberi kesan awal positif',
	        	'S'	=>	'Tekun',
	        	'C'	=>	'Perfeksionis',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Cepat bertindak',
	        	'I'	=>	'Mudah menyesuaikan diri dalam situasi baru',
	        	'S'	=>	'Praktis, sederhana dalam bekerja',
	        	'C'	=>	'Senang pada detail',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Berani memutuskan dalam keadaan mendesak',
	        	'I'	=>	'Sanggup mengembangkan antusiasme dan gairah kerja',
	        	'S'	=>	'Konservatif',
	        	'C'	=>	'Senang pada rincian yang rumit',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Memiliki intuisi tajam',
	        	'I'	=>	'',
	        	'S'	=>	'Stabil, Konsisten',
	        	'C'	=>	'Tekun dan akurat',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Memberi inspirasi',
	        	'I'	=>	'',
	        	'S'	=>	'Rapi, tertib, teratur',
	        	'C'	=>	'Berbakat dan cerdas',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Berani mengambil resiko',
	        	'I'	=>	'',
	        	'S'	=>	'Membuat rencana sebelum bekerja',
	        	'C'	=>	'Kreatif dan memiliki intelektual tinggi',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Target oriented',
	        	'I'	=>	'',
	        	'S'	=>	'',
	        	'C'	=>	'Pemikir dan menganalisa',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Berusaha keras mencapai sasaran',
	        	'I'	=>	'',
	        	'S'	=>	'',
	        	'C'	=>	'',
	        	'category'	=> 'kekuatan'
        	],
        	[
        		'D' => 	'Menyukai pekerjaan di lapangan',
	        	'I'	=>	'',
	        	'S'	=>	'',
	        	'C'	=>	'',
	        	'category'	=> 'kekuatan'
        	],
        	//
        	[
        		'D' => 	'Terlalu percaya diri',
	        	'I'	=>	'Tidak teratur, acak-acakan',
	        	'S'	=>	'Lamban, statis',
	        	'C'	=>	'Tidak tegas',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Kurang menghargai pendapat orang lain',
	        	'I'	=>	'Tidak ketat dalam disiplin diri',
	        	'S'	=>	'Terlalu hati-hati',
	        	'C'	=>	'Terlalu banyak teori, kurang praktis',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Cenderung suka mengakali',
	        	'I'	=>	'Banyak waktu terbuang karena mengobrol berlebihan',
	        	'S'	=>	'Cenderung menghindari resiko',
	        	'C'	=>	'Terlalu banyak menganalisa',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Kurang analitis, jemu dengan hal detail',
	        	'I'	=>	'Kurang efektif untuk tugas yang ketat dan urgent',
	        	'S'	=>	'Menolak/takut pada perubahan',
	        	'C'	=>	'Ragu-ragu untuk memulai proyek baru',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Memaksa orang lain mengikuti jalan pikirannya',
	        	'I'	=>	'Mudah berubah-ubah',
	        	'S'	=>	'Membutuhkan jaminan dan kepastian',
	        	'C'	=>	'Ragu untuk mencipta/ berinovasi',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Sering terlalu cepat menyimpulkan sesuatu',
	        	'I'	=>	'',
	        	'S'	=>	'Perlu waktu untuk meyakinkan & menyetujui sesuatu',
	        	'C'	=>	'Terlalu banyak berpikir',
	        	'category'	=> 'kelemahan'
        	],
        	[
        		'D' => 	'Impulsif',
	        	'I'	=>	'',
	        	'S'	=>	'',
	        	'C'	=>	'Terlalu terpaku pada hal detail',
	        	'category'	=> 'kelemahan'
        	],
        ]);
    }
}
