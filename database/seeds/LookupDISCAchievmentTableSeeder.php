<?php

use Illuminate\Database\Seeder;

class LookupDISCAchievmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_disc_achievment')->insert(array(
        	array('hasil_disc'=>'C','nilai'=>3),
        	array('hasil_disc'=>'CD','nilai'=>4),
        	array('hasil_disc'=>'CDI','nilai'=>4),
        	array('hasil_disc'=>'CDS','nilai'=>4),
        	array('hasil_disc'=>'CI','nilai'=>3),
        	array('hasil_disc'=>'CID','nilai'=>5),
        	array('hasil_disc'=>'CIS','nilai'=>4),
        	array('hasil_disc'=>'CS','nilai'=>3),
        	array('hasil_disc'=>'CSD','nilai'=>5),
        	array('hasil_disc'=>'CSI','nilai'=>4), 
        	array('hasil_disc'=>'D','nilai'=>4), 
        	array('hasil_disc'=>'DC','nilai'=>5), 
        	array('hasil_disc'=>'DCI','nilai'=>6), 
        	array('hasil_disc'=>'DCS','nilai'=>5), 
        	array('hasil_disc'=>'DI','nilai'=>6), 
        	array('hasil_disc'=>'DIC','nilai'=>6), 
        	array('hasil_disc'=>'DIS','nilai'=>5), 
        	array('hasil_disc'=>'DS','nilai'=>4), 
        	array('hasil_disc'=>'DSC','nilai'=>5), 
        	array('hasil_disc'=>'DSI','nilai'=>4), 
        	array('hasil_disc'=>'I','nilai'=>3),
        	array('hasil_disc'=>'IC','nilai'=>3), 
        	array('hasil_disc'=>'ICD','nilai'=>6), 
        	array('hasil_disc'=>'ICS','nilai'=>4), 
        	array('hasil_disc'=>'ID','nilai'=>6), 
        	array('hasil_disc'=>'IDC','nilai'=>6), 
        	array('hasil_disc'=>'IDS','nilai'=>6), 
        	array('hasil_disc'=>'IS','nilai'=>3), 
        	array('hasil_disc'=>'ISC','nilai'=>4), 
        	array('hasil_disc'=>'ISD','nilai'=>4), 
        	array('hasil_disc'=>'S','nilai'=>3),
        	array('hasil_disc'=>'SC','nilai'=>3), 
        	array('hasil_disc'=>'SCD','nilai'=>4),
        	array('hasil_disc'=>'SCI','nilai'=>3), 
        	array('hasil_disc'=>'SD','nilai'=>4), 
        	array('hasil_disc'=>'SDC','nilai'=>4), 
        	array('hasil_disc'=>'SDI','nilai'=>3), 
        	array('hasil_disc'=>'SI','nilai'=>3), 
        	array('hasil_disc'=>'SIC','nilai'=>3), 
        	array('hasil_disc'=>'SID','nilai'=>3)
        ));
    }
}
