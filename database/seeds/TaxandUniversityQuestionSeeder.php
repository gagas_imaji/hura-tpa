<?php

use Illuminate\Database\Seeder;

class TaxandUniversityQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question')->insert([
        	//grammar a
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "Water pressure __________ cracks open small rocks but also breaks great slabs of stone from the faces of cliffs.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "New words are constantly being invented __________ new objects and concepts.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "Bricks baked in a kiln are much harder __________ that are dried in the sun.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "Exactly __________ humans domesticated animals is not known.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "Most comets have two kinds of tails, one made up of dust, __________  made up of electrically charged particles called plasma.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "The art of storytelling is __________ humanity",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "A cloud is a dense mass of __________ water vapor or ice particles.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "Dry farming is a type of agriculture used in areas __________ less than 20 inches of rainfall.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "Although drama is a form of literature, __________ from the other types in the way it is presented.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 14,
	            'question'			=> "Dragonflies remain stationary in the air while __________ their prey to come near.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],

        	//grammar b
        	[
	            'id_type_question' 	=> 3,
	            'id_section' 		=> 15,
	            'question'			=> "The accountants __________ (calculate) the tax refund now.",
	            'is_random'			=> 0,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 3,
	            'id_section' 		=> 15,
	            'question'			=> "She __________ (work) at PB&Co since 2001.",
	            'is_random'			=> 0,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 3,
	            'id_section' 		=> 15,
	            'question'			=> "He __________ submit the tax forms next week.",
	            'is_random'			=> 0,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 3,
	            'id_section' 		=> 15,
	            'question'			=> "They __________ (do) the due diligence of the company yesterday.",
	            'is_random'			=> 0,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 3,
	            'id_section' 		=> 15,
	            'question'			=> "They __________ never __________ (pay) their taxes since 1997",
	            'is_random'			=> 0,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],

        	//grammar c
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "In many cases, the formerly __________ origins of diseases have now been identified through modern scientific techniques.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "Freeing embedded fossils from rock has become less __________  for a geologist  who now have tiny vibrating drills capable of working with great speed and delicacy.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "Marine biologist, Sylvia Earle makes a career of expanding the limits of deep-sea mobility, making hitherto – impossible tasks __________ through the new technology designed by her company.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "Since many teachers today, draw on material from a variety of sources, disciplines, and ideologies for their lessons, their approach could best be called __________",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "Medieval kingdoms did not become constitutional republics overnight on the contrary, the change  was __________",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "People often find it difficult to __________ their errors.  They hate to admit they were wrong",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "My grandfather likes to __________ about his successful past",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "A judgment made before all the facts are known must be called  __________",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "The research is so __________ that it leaves no part of the issue unexamined.",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 16,
	            'question'			=> "At the age of forty-five, with a world-wide reputation and an as yet unbroken string of notable successes at the __________ of her career",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],

        	//matching question
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Acknowledge",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Alternative",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Strive",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Candid",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Yearn",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Comply",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Concise",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Drastic",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Compel",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_section' 		=> 17,
	            'question'			=> "Appropriate",
	            'is_random'			=> 1,
	            'timer'				=> rand(45,180),
	            'is_mandatory'		=> 1
        	],
        ]);
    }
}
