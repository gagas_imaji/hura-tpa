<?php

use Illuminate\Database\Seeder;

class LookupUraianAchievementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_achievement')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'kemauan kerjanya lemah. Ia bahkan seringkali kurang antusias dalam mengerjakan tugas-tugas yang dikerjakannya.',
	            'desc2' => 'mudah lelah, lesu dan tidak bersemangat, motivasi kerjanya rendah.',
	            'desc3' => 'dalam bekerja kurang menunjukkan semangatnya, mudah terganggu oleh lingkungan sekitar dan kurang fokus dalam bekerja.'
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'melakukan tugas-tugasnya berdasarkan tuntutan eksternal, sekedar memenuhi kewajiban sehingga hasilnya kurang maksimal.',
	            'desc2' => 'dalam bekerja sudah menunjukkan semangatnya namun belum konsisten.',
	            'desc3' => NULL
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'adalah seorang dengan dorongan berprestasi yang memadai dan dapat memberikan kontribusi di lingkungannya, meskipun terkadang masih memerlukan dukungan orang lain.',
	            'desc2' => 'dalam bekerja sudah menunjukkan semangat dan motivasi yang memadai, namun belum berusaha melebihi  target yang ditetapkan.',
	            'desc3' => NULL
        	],
            [
                'nilai' => 5,
                'desc1' => 'memiliki dorongan untuk berprestasi. Memiliki semangat untuk mencapai hasil kerja yang terbaik.',
                'desc2' => 'adalah seorang dengan dorongan berprestasi yang baik dan dapat memberikan kontribusi positif di  lingkungannya.',
                'desc3' => NULL
            ],
            [
                'nilai' => 6,
                'desc1' => 'adalah seorang yang konsisten dalam melakukan upaya untuk menjaga prestasi serta memiliki keinginan yang besar untuk menjadi seorang yang produktif dalam bekerja.',
                'desc2' => 'adalah seorang yang memiliki keinginan yang kuat untuk tampil unggul dan tidak mudah puas terhadap capaian hasil kerjanya.',
                'desc3' => NULL
            ]

        ]);
    }
}
