<?php

use Illuminate\Database\Seeder;

class GTIKekuatanKelemahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_gti_kekuatan_kelemahan')->insert([
        	[
        		'GTQ1'		=> 'Kecepatan dan keakuratan melihat kesalahan',
        		'GTQ2'		=> 'Kemampuan Logika dan argumentasi verbal',
        		'GTQ3'		=> 'Kemampuan fokus dan daya konsentrasi jangka panjang',
        		'GTQ4'		=> 'Keterampilan numerikal dan berhitung',
        		'GTQ5'		=> 'Pemecahan masalah secara mekanikal dan teknikal',
        	],
        	[
        		'GTQ1'		=> 'Melaksanakan tugas-tugas administratif',
        		'GTQ2'		=> 'Kemampuan memecahkan masalah',
        		'GTQ3'		=> 'Kemampuan konsentrasi',
        		'GTQ4'		=> 'Kemampuan aritmetis',
        		'GTQ5'		=> 'Kemampuan berpikir logis',
        	],
        	[
        		'GTQ1'		=> 'Cermat untuk tugas-tugas detail',
        		'GTQ2'		=> 'Negosiasi dan argumentasi',
        		'GTQ3'		=> 'Kemampuan atensi dan Fokus',
        		'GTQ4'		=> 'Berhubungan dengan angka-angka',
        		'GTQ5'		=> 'Kemampuan antisipatif',
        	],
        ]);
    }
}
