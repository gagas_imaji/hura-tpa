<?php

use Illuminate\Database\Seeder;

class QuestionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_type')->insert([
			[
	            'name' => 'Multiple Choice - Single Answer',
	            'slug' => 'multiple-choice---single-answer'
        	],
        	[
	            'name' => 'Multiple Choice - Multiple Answer',
	            'slug' => 'multiple-choice---multiple-answer'
        	],
        	[
	            'name' => 'Open Text',
	            'slug' => 'open-text'
        	],
            [
                'name' => 'Matching Question',
                'slug' => 'matching-question'
            ],
            [
                'name' => 'Essay',
                'slug' => 'essay'
            ]

        ]);
    }
}
