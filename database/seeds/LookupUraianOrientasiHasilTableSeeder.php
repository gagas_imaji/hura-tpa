<?php

use Illuminate\Database\Seeder;

class LookupUraianOrientasiHasilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_orientasi_hasil')->insert([
			[
	            'nilai' => 2,
	            'desc1' => 'cenderung kurang menunjukkan kesigapan dalam bekerja, kurang dapat diandalkan dan kurang teliti dalam bekerja.'
        	],
        	[
	            'nilai' => 3,
	            'desc1' => 'memerlukan waktu untuk dapat bekerja dengan cepat sesuai dengan tugasnya, kemauan kerjanya cukup memadai namun seringkali membuat kesalahan dalam bekerja.'
        	],
        	[
	            'nilai' => 4,
	            'desc1' => 'mampu bekerja secara sistematis, produktivitas kerjanya cukup memadai dan lebih memperhatikan hal-hal detail dalam bekerja.'
        	],
            [
                'nilai' => 5,
                'desc1' => 'mampu bekerja secara sistematis, produktivitas kerjanya memadai dan lebih memperhatikan hal-hal detail dalam bekerja.'
            ],
            [
                'nilai' => 6,
                'desc1' => 'produktivitas dan kemauan kerjanya sangat tinggi, serta kualitas kerjanya dapat diandalkan.'
            ]

        ]);
    }
}
