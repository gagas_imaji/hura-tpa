<?php

use Illuminate\Database\Seeder;

class LookupTikiIQ extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_tiki_iq')->insert([
            [
                'total'     => 4,
                'iq'        => 56
            ],
            [
                'total'     => 5,
                'iq'        => 56
            ],
            [
                'total'     => 6,
                'iq'        => 56
            ],
            [
                'total'     => 7,
                'iq'        => 56
            ],
            [
                'total'     => 8,
                'iq'        => 56
            ],
            [
                'total'     => 9,
                'iq'        => 56
            ],
            [
                'total'     => 10,
                'iq'        => 56
            ],
            [
                'total'     => 11,
                'iq'        => 56
            ],
            [
                'total'     => 12,
                'iq'        => 56
            ],
            [
                'total'     => 13,
                'iq'        => 56
            ],
            [
                'total'     => 14,
                'iq'        => 56
            ],
            [
                'total'     => 15,
                'iq'        => 56
            ],
            [
                'total'     => 16,
                'iq'        => 56
            ],
            [
                'total'     => 17,
                'iq'        => 56
            ],
            [
                'total'     => 18,
                'iq'        => 56
            ],
            [
                'total'     => 19,
                'iq'        => 56
            ],
            [
                'total'     => 20,
                'iq'        => 56
            ],
            [
                'total'     => 21,
                'iq'        => 56
            ],
            [
                'total'     => 22,
                'iq'        => 56
            ],
            [
                'total'     => 23,
                'iq'        => 56
            ],
            [
                'total'     => 24,
                'iq'        => 56
            ],
            [
                'total'     => 25,
                'iq'        => 56
            ],
            [
                'total'     => 26,
                'iq'        => 56
            ],
            [
                'total'     => 27,
                'iq'        => 56
            ],
            [
                'total'     => 28,
                'iq'        => 56
            ],

            [
                'total'     => 29,
                'iq'        => 56
            ],
            [
                'total'     => 30,
                'iq'        => 57
            ],
            [
                'total'     => 31,
                'iq'        => 58
            ],
            [
                'total'     => 32,
                'iq'        => 59
            ],
            [
                'total'     => 33,
                'iq'        => 60
            ],
            [
                'total'     => 34,
                'iq'        => 62
            ],
            [
                'total'     => 35,
                'iq'        => 63
            ],
            [
                'total'     => 36,
                'iq'        => 64
            ],
            [
                'total'     => 37,
                'iq'        => 65
            ],
            [
                'total'     => 38,
                'iq'        => 66
            ],
            [
                'total'     => 39,
                'iq'        => 68
            ],
            [
                'total'     => 40,
                'iq'        => 69
            ],
            [
                'total'     => 41,
                'iq'        => 70
            ],
            [
                'total'     => 42,
                'iq'        => 71
            ],
            [
                'total'     => 43,
                'iq'        => 72
            ],
            [
                'total'     => 44,
                'iq'        => 73
            ],
            [
                'total'     => 45,
                'iq'        => 74
            ],
            [
                'total'     => 46,
                'iq'        => 76
            ],
            [
                'total'     => 47,
                'iq'        => 77
            ],
            [
                'total'     => 48,
                'iq'        => 78
            ],
            [
                'total'     => 49,
                'iq'        => 79
            ],
            [
                'total'     => 50,
                'iq'        => 80
            ],
            [
                'total'     => 51,
                'iq'        => 81
            ],
            [
                'total'     => 52,
                'iq'        => 82
            ],
            [
                'total'     => 53,
                'iq'        => 84
            ],
            [
                'total'     => 54,
                'iq'        => 85
            ],
            [
                'total'     => 55,
                'iq'        => 86
            ],
            [
                'total'     => 56,
                'iq'        => 87
            ],
            [
                'total'     => 57,
                'iq'        => 88
            ],
            [
                'total'     => 58,
                'iq'        => 89
            ],
            [
                'total'     => 59,
                'iq'        => 90
            ],
            [
                'total'     => 60,
                'iq'        => 92
            ],
            [
                'total'     => 61,
                'iq'        => 93
            ],
            [
                'total'     => 62,
                'iq'        => 94
            ],
            [
                'total'     => 63,
                'iq'        => 95
            ],
            [
                'total'     => 64,
                'iq'        => 96
            ],
            [
                'total'     => 65,
                'iq'        => 97
            ],
            [
                'total'     => 66,
                'iq'        => 98
            ],
            [
                'total'     => 67,
                'iq'        => 99
            ],
            [
                'total'     => 68,
                'iq'        => 100
            ],
            [
                'total'     => 69,
                'iq'        => 102
            ],
            [
                'total'     => 70,
                'iq'        => 103
            ],
            [
                'total'     => 71,
                'iq'        => 104
            ],
            [
                'total'     => 72,
                'iq'        => 105
            ],
            [
                'total'     => 73,
                'iq'        => 106
            ],
            [
                'total'     => 74,
                'iq'        => 107
            ],
            [
                'total'     => 75,
                'iq'        => 109
            ],
            [
                'total'     => 76,
                'iq'        => 110
            ],
            [
                'total'     => 77,
                'iq'        => 111
            ],
            [
                'total'     => 78,
                'iq'        => 112
            ],
            [
                'total'     => 79,
                'iq'        => 113
            ],
            [
                'total'     => 80,
                'iq'        => 114
            ],
            [
                'total'     => 81,
                'iq'        => 115
            ],
            [
                'total'     => 82,
                'iq'        => 117
            ],
            [
                'total'     => 83,
                'iq'        => 118
            ],
            [
                'total'     => 84,
                'iq'        => 119
            ],
            [
                'total'     => 85,
                'iq'        => 120
            ],
            [
                'total'     => 86,
                'iq'        => 121
            ],
            [
                'total'     => 87,
                'iq'        => 122
            ],
            [
                'total'     => 88,
                'iq'        => 123
            ],
            [
                'total'     => 89,
                'iq'        => 125
            ],
            [
                'total'     => 90,
                'iq'        => 126
            ],
            [
                'total'     => 91,
                'iq'        => 127
            ],
            [
                'total'     => 92,
                'iq'        => 128
            ],
            [
                'total'     => 93,
                'iq'        => 129
            ],
            [
                'total'     => 94,
                'iq'        => 130
            ],
            [
                'total'     => 95,
                'iq'        => 131
            ],
            [
                'total'     => 96,
                'iq'        => 133
            ],
            [
                'total'     => 97,
                'iq'        => 134
            ],
            [
                'total'     => 98,
                'iq'        => 135
            ],
            [
                'total'     => 99,
                'iq'        => 136
            ],
            [
                'total'     => 100,
                'iq'        => 137
            ],
            [
                'total'     => 101,
                'iq'        => 138
            ],
            [
                'total'     => 102,
                'iq'        => 139
            ],
            [
                'total'     => 103,
                'iq'        => 141
            ],
            [
                'total'     => 104,
                'iq'        => 142
            ],
            [
                'total'     => 105,
                'iq'        => 143
            ],
            [
                'total'     => 106,
                'iq'        => 144
            ],
            [
                'total'     => 107,
                'iq'        => 145
            ]
        ]);
    }
}
