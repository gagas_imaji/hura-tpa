<?php

use Illuminate\Database\Seeder;

class CorporateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('corporate')->insert([
			[
	            'name' 			=> 'Gagas Imaji',
	            'description' 	=> '-',
	            'contact'		=> '-',
                'email'         => 'https://gagasimaji.net',
	            'image'			=> '',
        	],
            [
                'name'          => 'Maybank',
                'description'   => '-',
                'contact'       => '-',
                'email'         => '-',
                'image'         => '-',
            ]
        ]);
    }
}
