<?php

use Illuminate\Database\Seeder;

class LookupNormaGTITableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_norma_gti')->insert(array(
        	array('min'=>0,'max'=>84.9,'nilai'=>2,'criteria'=>'Very Low'),
        	array('min'=>85,'max'=>91.9,'nilai'=>3,'criteria'=>'Below Average'),
        	array('min'=>92,'max'=>105.9,'nilai'=>4,'criteria'=>'Average'),
        	array('min'=>106,'max'=>128.9,'nilai'=>5,'criteria'=>'Above Average'),
        	array('min'=>129,'max'=>150,'nilai'=>6,'criteria'=>'Excellent'),
        ));
    }
}
