<?php

use Illuminate\Database\Seeder;

class BankQuestionGTISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//subtest 1
    	for($i = 1; $i <= 60; $i++){
	        DB::table('bank_question')->insert([
	        	//subtest 1
	        	[
		            'id_type_question' 	=> 1,
		            'id_bank_section' 	=> 8,
		            'question'			=> '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" class="lai1soal'.$i.'" />',
		            'is_random'			=> 0,
		            'timer'				=> 0,
		            'is_mandatory'		=> 1,
		            'number'			=> $i
	        	]
	        ]);
    	}

        //subtest 2
        DB::table('bank_question')->insert([
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'John lebih lamban daripada Dani<br/>John lebih cepat daripada Roy<br/>Siapa yang terlamban?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 1
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Rani Lebih pendek daripada Susi<br/>Rani lebih tinggi daripada Rita<br/>Siapa yang tertinggi?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 2
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Laras lebih cerdas daripada Lina<br/>Laras lebih bodoh daripada Mona<br/>Siapa yang tercerdas?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 3
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Adam lebih sedih daripada Andi<br/>Ardi lebih sedih daripada Adam<br/>Siapa yang tersedih?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 4
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Toni lebih dingin daripada Yudi<br/>Dodi lebih dingin daripada Toni<br/>Siapa yang terdingin?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 5
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Anna lebih pendek daripada Lisa<br/>Lisa lebih pendek daripada Ina<br/>Siapa yang terpendek?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 6
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Dito lebih cerdas daripada Beno<br/>Beno lebih bodoh daripada Adam<br/>Siapa yang terbodoh?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 7
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Lusi lebih lamban daripada Nora<br/>Nora lebih lamban daripada Elsa<br/>Siapa yang tercepat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 8
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Olga lebih hangat daripada Ita<br/>Tami lebih hangat daripada Olga<br/>Siapa yang terhangat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 9
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Sita Lebih tinggi daripada Dian<br/>Lina lebih pendek daripada Dian<br/>Siapa yang terpendek?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 10
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Lulu lebih tinggi daripada Tia<br/>Mira lebih pendek daripada Tia<br/>Siapa yang terpendek?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 11
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Eric lebih gembira daripada Kris<br/>Eric lebih sedih daripada Edi<br/>Siapa yang tersedih?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 12
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Anton lebih kuat daripada Toni<br/>Toni lebih kuat daripada Andi<br/>Siapa yang terkuat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 13
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'David lebih tua daripada Beni<br/>Roy lebih tua daripada David<br/>Siapa yang termuda?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 14
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Tia lebih sedih daripada Ina<br/>Rika lebih gembira daripada Ina<br/>Siapa yang paling gembira?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 15
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Eric lebih lamban daripada Paul<br/>Tio lebih cepat daripada Paul<br/>Siapa yang tercepat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 16
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Rina lebih kuat daripada Rita<br/>Lisa lebih lemah daripada Rita<br/>Siapa yang terlemah?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 17
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Soni lebih cepat daripada Joni<br/>Joni lebih cepat daripada Paul<br/>Siapa yang tercepat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 18
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Rini lebih gemuk daripada Elsa<br/>Elsa lebih gemuk daripada Lulu<br/>Siapa yang tergemuk?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 19
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Rita lebih lemah daripada Ina<br/>Anna lebih kuat daripada Ina <br/>Siapa yang terkuat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 20
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Ardi lebih tua daripada Tomi<br/>Robi lebih tua daripada Ardi<br/>Siapa yang tertua?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 21
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Anto lebih cedas daripada Dito<br/>Anto lebih bodoh daripada Soni<br/>Siapa yang terbodoh?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 22
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Yani lebih dingin daripada Olga<br/>Olga lebih dingin daripada Rika<br/>Siapa yang terdingin?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 23
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Della lebih muda daripada Nadia<br/>Della lebih tua daripada Sita<br/>Siapa yang termuda?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 24
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Robby lebih muda daripada Tomi<br/>Tomi lebih muda daripada Soni<br/>Siapa yang tertua?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 25
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Elsa lebih buruk daripada Susi<br/>Mira lebih baik daripada Susi <br/>Siapa yang terbaik?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 26
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Adi lebih sedih daripada Tio<br/>Roy lebih sedih daripada Adi<br/>Siapa yang tersedih?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 27
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Diah lebih cerdas daripada Lisa<br/>Elli lebih cerdas daripada Diah<br/>Siapa yang terbodoh?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 28
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Ina lebih tinggi daripada Dewi<br/>Ina lebih pendek daripada Anna<br/>Siapa yang tertinggi?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 29
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Erwin lebih muda daripada Edi<br/>Erwin lebih tua daripada Soni<br/>Siapa yang tertua?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 30
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Algi lebih ringan daripada Tono<br/>Tino lebih berat daripada Tono<br/>Siapa yang terberat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 31
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Olga lebih kuat daripada Lina<br/>Nora lebih lemah daripada Lina<br/>Siapa yang terkuat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 32
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Tina lebih ringan daripada Tini<br/>Tina lebih berat daripada Susi<br/>Siapa yang terringan?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 33
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Wati lebih muda daripada Dewi<br/>Dian lebih tua daripada Dewi<br/>Siapa yang tertua?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 34
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Adi lebih ramping daripada Bram<br/>Bram lebih ramping daripada Tio<br/>Siapa yang teramping?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 35
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Anna lebih kuat daripada Ani<br/>Anna lebih lemah daripada Ita<br/>Siapa yang terlemah?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 36
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Dani lebih cerdas daripada Doni<br/>Dani lebih bodoh daripada Adam<br/>Siapa yang tercerdas?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 37
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Didit lebih tinggi daripada Roy<br/>Roy lebih tinggi daripada Roni<br/>Siapa yang terpendek?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 38
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Paul lebih baik daripada Eric<br/>Toni lebih buruk daripada Eric<br/>Siapa yang terbaik?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 39
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Susi lebih dingin daripada Nora<br/>Nora lebih dingin daripada Ita<br/>Siapa yang terdingin?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 40
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Bima lebih ringan daripada John<br/>Toni lebih ringan daripada Bima<br/>Siapa yang terberat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 41
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Farid lebih hangat daripada Paul<br/>Eric lebih hangat daripada Farid<br/>Siapa yang terhangat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 42
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Lia lebih lemah daripada Tia<br/>Lia lebih kuat daripada Yuli <br/>Siapa yang terkuat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 43
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Andi lebih ringan daripada Tomi<br/>Roy lebih berat daripada Tomi<br/>Siapa yang terberat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 44
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Roni lebih muda daripada Rino<br/>Roy lebih muda daripada Roni<br/>Siapa yang tertua?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 45
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Lulu Lebih cepat daripada Lita<br/>Susi lebih lamban daripada Lita<br/>Siapa yang terlamban?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 46
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Tomi Lebih muda daripada Budi<br/>Budi lebih muda daripada Rio<br/>Siapa yang tertua?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 47
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Beno Lebih tinggi daripada Budi<br/>Tono lebih tinggi daripada Beno<br/>Siapa yang terpendek?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 48
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Anna lebih tinggi daripada Eva<br/>Eva lebih tinggi daripada Tia<br/>Siapa yang tertinggi?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 49
        	],
        	[
	            'id_type_question' 	=> 1,
	            'id_bank_section' 	=> 9,
	            'question'			=> 'Tina lebih lamban daripada Nora<br/>Nora lebih lamban daripada Rini<br/>Siapa yang tercepat?',
	            'is_random'			=> 0,
	            'timer'				=> 0,
	            'is_mandatory'		=> 1,
	            'number'			=> 50
        	],
        ]);
		
		//subtest 3
		for($i = 1; $i <= 72; $i++){
			DB::table('bank_question')->insert([
	        	[
		            'id_type_question' 	=> 1,
		            'id_bank_section' 	=> 10,
		            'question'			=> '',
		            'is_random'			=> 0,
		            'timer'				=> 0,
		            'is_mandatory'		=> 1,
		            'number'			=> $i
	        	]
        	]);
		}

		//subtest 4
		for($i = 1; $i <= 60; $i++){
			DB::table('bank_question')->insert([
	        	[
		            'id_type_question' 	=> 1,
		            'id_bank_section' 	=> 11,
		            'question'			=> '',
		            'is_random'			=> 0,
		            'timer'				=> 0,
		            'is_mandatory'		=> 1,
		            'number'			=> $i
	        	]
        	]);
		}
	
       	//subtest 5
		for($i = 1; $i <= 60; $i++){
			DB::table('bank_question')->insert([
	        	[
		            'id_type_question' 	=> 1,
		            'id_bank_section' 	=> 12,
		            'question'			=> '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" class="lai5soal'.$i.'" />',
		            'is_random'			=> 0,
		            'timer'				=> 0,
		            'is_mandatory'		=> 1,
		            'number'			=> $i
	        	]
        	]);
		}
		
    }
}
