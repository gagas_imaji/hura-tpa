<?php

use Illuminate\Database\Seeder;

class LookupDISCFleksibilitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_disc_fleksibilitas')->insert(array(
        	array('hasil_disc'=>'C','nilai'=>2),
        	array('hasil_disc'=>'CD','nilai'=>2),
        	array('hasil_disc'=>'CDI','nilai'=>3),
        	array('hasil_disc'=>'CDS','nilai'=>3),
        	array('hasil_disc'=>'CI','nilai'=>4),
        	array('hasil_disc'=>'CID','nilai'=>4),
        	array('hasil_disc'=>'CIS','nilai'=>4),
        	array('hasil_disc'=>'CS','nilai'=>3),
        	array('hasil_disc'=>'CSD','nilai'=>3),
        	array('hasil_disc'=>'CSI','nilai'=>4), 
        	array('hasil_disc'=>'D','nilai'=>2), 
        	array('hasil_disc'=>'DC','nilai'=>2), 
        	array('hasil_disc'=>'DCI','nilai'=>3), 
        	array('hasil_disc'=>'DCS','nilai'=>3), 
        	array('hasil_disc'=>'DI','nilai'=>3), 
        	array('hasil_disc'=>'DIC','nilai'=>3), 
        	array('hasil_disc'=>'DIS','nilai'=>3), 
        	array('hasil_disc'=>'DS','nilai'=>3), 
        	array('hasil_disc'=>'DSC','nilai'=>3), 
        	array('hasil_disc'=>'DSI','nilai'=>4), 
        	array('hasil_disc'=>'I','nilai'=>5),
        	array('hasil_disc'=>'IC','nilai'=>4), 
        	array('hasil_disc'=>'ICD','nilai'=>4), 
        	array('hasil_disc'=>'ICS','nilai'=>4), 
        	array('hasil_disc'=>'ID','nilai'=>4), 
        	array('hasil_disc'=>'IDC','nilai'=>4), 
        	array('hasil_disc'=>'IDS','nilai'=>4), 
        	array('hasil_disc'=>'IS','nilai'=>5), 
        	array('hasil_disc'=>'ISC','nilai'=>4), 
        	array('hasil_disc'=>'ISD','nilai'=>4), 
        	array('hasil_disc'=>'S','nilai'=>4),
        	array('hasil_disc'=>'SC','nilai'=>4), 
        	array('hasil_disc'=>'SCD','nilai'=>3),
        	array('hasil_disc'=>'SCI','nilai'=>4), 
        	array('hasil_disc'=>'SD','nilai'=>3), 
        	array('hasil_disc'=>'SDC','nilai'=>3), 
        	array('hasil_disc'=>'SDI','nilai'=>4), 
        	array('hasil_disc'=>'SI','nilai'=>5), 
        	array('hasil_disc'=>'SIC','nilai'=>5), 
        	array('hasil_disc'=>'SID','nilai'=>4)
        ));
    }
}
