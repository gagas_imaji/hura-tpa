<?php

use Illuminate\Database\Seeder;

class BankSectionTikiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_section')->insert([
            [
                'bank_survey_id'    => 4,
                'name'              => 'Berhitung Angka',
                'slug'              => 'berhitung-angka',
                'instruction'       => 'Setiap soal disertai dengan kemungkinan jawaban a, b, c dan d. <br/> Salah satu diantaranya adalah jawaban yang benar dari setiap soal yang ada. <br/> Silahkan pilih salah satu jawaban yang menurut anda benar. <br/> Waktu pengerjaan subtest ini 7 menit.',
                'timer'             => '420', //420
                'limit'             => '0'
            ], 
            [
                'bank_survey_id'    => 4,
                'name'              => 'Gabungan Bagian',
                'slug'              => 'gabungan-bagian',
                'instruction'       => 'Gambar di sebelah atas dari setiap soal merupakan bentuk yang terpotong menjadi 3 bagian yang sudah ditentukan. <br/> Pada setiap soal terdapat 6 pilihan jawaban yang dua diantaranya terbuat dari bagian-bagian dari gambar pada soal. <br/> Pilihlah dua gambar yang dimaksud dari 6 (enam) pilihan yang ada. <br/> Waktu pengerjaan subtest ini 7 menit',
                'timer'             => '420', //420
                'limit'             => '0'
            ], 
            [
                'bank_survey_id'    => 4,
                'name'              => 'Hubungan Kata',
                'slug'              => 'hubungan-kata',
                'instruction'       => 'Test berikut ini terdiri dari kata-kata. Setiap soal terdiri dari 4 (empat) kata. Tentukan 2 (dua) buah kata yang memiliki kesamaan arti yang paling dekat atau yang mempunyai arti yang berlawanan. Waktu pengerjaan subtest ini 5 menit',
                'timer'             => '300', //300
                'limit'             => '0'
            ], 
            [
                'bank_survey_id'    => 4,
                'name'              => 'Abstraksi Non Verbal',
                'slug'              => 'abstraksi-non-verbal',
                'instruction'       => 'Gambar pada soal menunjukkan kesamaan dalam suatu hal atau segi tertentu. Tentukan 2 (dua) gambar yang menunjukkan kesamaan dengan 4 (empat) gambar pada soal dari 6 (enam) pilihan gambar yang ada. Waktu pengerjaan subtest ini 12 menit',
                'timer'             => '720', //720
                'limit'             => '0'
            ]
        ]);
    }
}
