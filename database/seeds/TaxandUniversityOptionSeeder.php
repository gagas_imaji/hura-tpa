<?php

use Illuminate\Database\Seeder;

class TaxandUniversityOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('option_answer')->insert([
        	//grammar a
        	[
				'id_question'	=> 109,
				'answer' 		=> 'either *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 109,
				'answer' 		=> 'not only',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 109,
				'answer' 		=> 'and so',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 109,
				'answer' 		=> 'moreover',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 109,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1	
	        ],
	        //
	        [
				'id_question'	=> 110,
				'answer' 		=> 'to describe *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 110,
				'answer' 		=> 'a description of',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 110,
				'answer' 		=> 'they describe',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 110,
				'answer' 		=> 'describe',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 110,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        //
			[
				'id_question'	=> 111,
				'answer' 		=> 'those *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 111,
				'answer' 		=> 'than do those',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 111,
				'answer' 		=> 'than those',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 111,
				'answer' 		=> 'ones',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 111,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        //
	        [
				'id_question'	=> 112,
				'answer' 		=> 'how *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 112,
				'answer' 		=> 'by means of',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 112,
				'answer' 		=> 'if',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 112,
				'answer' 		=> 'by which',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 112,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        //
	        [
				'id_question'	=> 113,
				'answer' 		=> 'one another *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 113,
				'answer' 		=> 'the other',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 113,
				'answer' 		=> 'other ones',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 113,
				'answer' 		=> 'each other',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 113,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        //
	        [
				'id_question'	=> 114,
				'answer' 		=> 'as old *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 114,
				'answer' 		=> 'old as',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 114,
				'answer' 		=> 'as old as',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 114,
				'answer' 		=> 'old',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 114,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        //
	        [
				'id_question'	=> 115,
				'answer' 		=> 'or *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 115,
				'answer' 		=> 'whether',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 115,
				'answer' 		=> 'both',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 115,
				'answer' 		=> 'either',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 115,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        //
	        [
				'id_question'	=> 116,
				'answer' 		=> 'there are *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 116,
				'answer' 		=> 'in which is',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 116,
				'answer' 		=> 'where there is',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 116,
				'answer' 		=> 'which has',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 116,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        //
	        [
				'id_question'	=> 117,
				'answer' 		=> 'it differs *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 117,
				'answer' 		=> 'is different',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 117,
				'answer' 		=> 'despite the difference',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 117,
				'answer' 		=> 'but it is different',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 117,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        //
	        [
				'id_question'	=> 118,
				'answer' 		=> 'waited for *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 118,
				'answer' 		=> 'they wait',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 118,
				'answer' 		=> 'waiting for',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 118,
				'answer' 		=> 'to wait',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 118,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
	        
			//grammar b
			[
				'id_question'	=> 119,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 120,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 121,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 122,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 123,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0
			],

			//grammar c
			[
				'id_question'	=> 124,
				'answer' 		=> 'useful *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 124,
				'answer' 		=> 'mysterious',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 124,
				'answer' 		=> 'cruel',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 124,
				'answer' 		=> 'notable',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 124,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 125,
				'answer' 		=> 'exploratory *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 125,
				'answer' 		=> 'conclusive',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 125,
				'answer' 		=> 'tedious',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 125,
				'answer' 		=> 'respected',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 125,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 126,
				'answer' 		=> 'famous *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 126,
				'answer' 		=> 'feasible',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 126,
				'answer' 		=> 'fantastic',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 126,
				'answer' 		=> 'captivating',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 126,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 127,
				'answer' 		=> 'eclectic  *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 127,
				'answer' 		=> 'simplistic',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 127,
				'answer' 		=> 'impromptu',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 127,
				'answer' 		=> 'dogmatic',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 127,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 128,
				'answer' 		=> 'unpopular  *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 128,
				'answer' 		=> 'unexpected',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 128,
				'answer' 		=> 'sufficient',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 128,
				'answer' 		=> 'gradual',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 128,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 129,
				'answer' 		=> 'compel  *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 129,
				'answer' 		=> 'acknowledge',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 129,
				'answer' 		=> 'appropriate',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 129,
				'answer' 		=> 'alternative ',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 129,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 130,
				'answer' 		=> 'illuminate  *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 130,
				'answer' 		=> 'reminisce',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 130,
				'answer' 		=> 'isolate',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 130,
				'answer' 		=> 'comply ',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 130,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 131,
				'answer' 		=> 'harsh  *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 131,
				'answer' 		=> 'sensible',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 131,
				'answer' 		=> 'fair',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 131,
				'answer' 		=> 'premature ',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 131,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 132,
				'answer' 		=> 'comprehensive  *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 132,
				'answer' 		=> 'sporadic',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 132,
				'answer' 		=> 'rewarding',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 132,
				'answer' 		=> 'economical ',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 132,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 133,
				'answer' 		=> 'paradigm  *',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 133,
				'answer' 		=> 'zenith',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 133,
				'answer' 		=> 'fiasco',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 133,
				'answer' 		=> 'periphery ',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 0	
	        ],
	        [
				'id_question'	=> 133,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//

			//matching question
			[
				'id_question'	=> 134,
				'answer' 		=> 'to confess or admit',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 134,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 135,
				'answer' 		=> 'choice',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 135,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 136,
				'answer' 		=> 'to want something very much',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 136,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 137,
				'answer' 		=> 'very honest',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 137,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 138,
				'answer' 		=> 'to try very hard to achieve',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 138,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 139,
				'answer' 		=> 'to do as commanded or asked',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 139,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 140,
				'answer' 		=> 'communicating much in a few clear words',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 140,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 141,
				'answer' 		=> 'extreme, harsh or intense',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 141,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 142,
				'answer' 		=> 'to force',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 142,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
			//
			[
				'id_question'	=> 143,
				'answer' 		=> 'proper, suitable to the situation',
				'image'			=> '',
				'point'			=> 1,
				'hidden'		=> 0
			],
			[
				'id_question'	=> 143,
				'answer' 		=> 'No Answer',
				'image'			=> '',
				'point'			=> 0,
				'hidden'		=> 1
			],
        ]);
    }
}
