<?php

use Illuminate\Database\Seeder;

class LookupUraianKecerdasanUmumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup_uraian_kecerdasan_umum')->insert([
			[
	            'nilai' => 2,
                'desc1' => ' memiliki kapasitas intelegensi yang tergolong Di Bawah Rata-rata. Dengan demikian maka dapat dikatakan bahwa ia memiliki kapasitas berpikir yang terbatas.'
        	],
        	[
	            'nilai' => 3,
                'desc1' => 'memiliki kapasitas intelegensi yang berada pada taraf Sedikit Di Bawah Rata-rata. Dengan demikian maka dapat dikatakan bahwa ia memiliki kapasitas berpikir yang kurang memadai.'
        	],
        	[
	            'nilai' => 4,
                'desc1' => 'memiliki kapasitas intelegensi yang berada pada taraf Rata-rata. Dengan demikian maka dapat dikatakan bahwa ia memiliki kapasitas berpikir yang cukup memadai.'
        	],
            [
                'nilai' => 5,
                'desc1' => 'memiliki kapasitas intelegensi yang berada pada taraf Sedikit Diatas Rata-rata. Dengan demikian maka dapat dikatakan bahwa ia memiliki kapasitas berpikir yang cukup baik.'
            ],
            [
                'nilai' => 6,
                'desc1' => 'memiliki kapasitas intelegensi yang berada pada taraf Diatas Rata-rata. Dengan demikian maka dapat dikatakan bahwa ia memiliki kapasitas berpikir yang sangat baik.'
            ]

        ]);
    }
}
