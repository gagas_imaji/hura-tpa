<?php

use Illuminate\Database\Seeder;

class BankSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_section')->insert([
            //HEAT
            [
                'bank_survey_id' 	=> 1,
                'name' 				=> 'Task Performance',
                'slug' 				=> 'task-performance'
            ],
            [
                'bank_survey_id' 	=> 1,
                'name' 				=> 'Contextual Performance',
                'slug' 				=> 'contextual-performance'
            ],
            [
                'bank_survey_id' 	=> 1,
                'name' 				=> 'Adaptive Performance',
                'slug' 				=> 'adaptive-performance'
            ],
            [
                'bank_survey_id' 	=> 1,
                'name' 				=> 'Counter Behaviour',
                'slug' 				=> 'counter-behaviour'
            ],
            [
                'bank_survey_id' 	=> 2,
                'name' 				=> 'Vigorous',
                'slug' 				=> 'vigorous'
            ],
            [
                'bank_survey_id' 	=> 2,
                'name' 				=> 'Dedication',
                'slug' 				=> 'dedication'
            ],
            [
                'bank_survey_id' 	=> 2,
                'name' 				=> 'Absorbtion',
                'slug' 				=> 'absorbtion'
            ]
        ]);
    }
}
