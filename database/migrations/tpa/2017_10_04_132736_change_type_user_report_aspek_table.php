<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeUserReportAspekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_report_aspeks', function (Blueprint $table) {
            $table->text('aspek_id')->nullable()->change();
            $table->text('nilai_aspek')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_report_aspeks', function (Blueprint $table) {
            $table->dropColumn('aspek_id');
            $table->dropColumn('nilai_aspek');
        });
    }
}
