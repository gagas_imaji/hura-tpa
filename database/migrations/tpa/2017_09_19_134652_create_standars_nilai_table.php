<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandarsNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standar_nilais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jabatan_id')->unsigned()->nullable();
            $table->string('nama_jabatan')->nullable();
            $table->string('slug_nama_jabatan')->nullable();
            $table->text('aspek_id')->nullable();
            $table->text('nilai_aspek')->nullable();
            $table->text('mandatory')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standar_nilais');
    }
}
