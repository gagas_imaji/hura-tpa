<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAspekPsikologisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspek_psikologis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kategori_aspek_id')->unsigned()->nullable();
            $table->string('nama_aspek',64)->nullable();
            $table->string('slug',64)->nullable();
            $table->text('gambaran_taraf_rendah')->nullable();
            $table->text('gambaran_taraf_tinggi')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspek_psikologis');
    }
}
