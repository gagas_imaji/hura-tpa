<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchiveUserReportAnalisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_user_report_analises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('jabatan_id')->unsigned()->nullable();
            $table->integer('user_report_id')->unsigned()->nullable();
            $table->double('kecocokan', 10, 2)->nullable();
            $table->integer('kriteria_psikograph_id')->unsigned()->nullable();
            $table->text('uraian_kepribadian')->nullable();
            $table->text('kekuatan')->nullable();
            $table->text('kelemahan')->nullable();
            $table->text('karakteristik_pekerjaan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_user_report_analises');
    }
}
