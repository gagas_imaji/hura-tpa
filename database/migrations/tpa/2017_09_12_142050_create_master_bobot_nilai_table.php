<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterBobotNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_bobot_nilai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alat_ukur_id')->unsigned()->nullable();
            $table->string('nama',32)->nullable();
            $table->string('slug',32)->nullable();
            $table->double('nilai', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_bobot_nilai');
    }
}
