<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterpretasiDiscTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interpretasi_disc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hasil_disc')->nullable();
            $table->tinyInteger('fleksibilitas_berpikir')->nullable();
            $table->tinyInteger('sistematika_kerja')->nullable();
            $table->tinyInteger('motivasi_berprestasi')->nullable();
            $table->text('deskripsi_motivasi_berprestasi_1')->nullable();
            $table->text('deskripsi_motivasi_berprestasi_2')->nullable();
            $table->tinyInteger('kerjasama')->nullable();
            $table->text('deskripsi_kerjasama_1')->nullable();
            $table->text('deskripsi_kerjasama_2')->nullable();
            $table->tinyInteger('keterampilan_interpersonal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interpretasi_disc');
    }
}
