<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookupGATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_GA', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('nilai_g')->nullable();
            $table->tinyInteger('nilai_a')->nullable();
            $table->tinyInteger('nilai_a0')->nullable();
            $table->tinyInteger('nilai_a1')->nullable();
            $table->tinyInteger('nilai_a2')->nullable();
            $table->tinyInteger('nilai_a3')->nullable();
            $table->tinyInteger('nilai_a4')->nullable();
            $table->tinyInteger('nilai_a5')->nullable();
            $table->tinyInteger('nilai_a6')->nullable();
            $table->tinyInteger('nilai_a7')->nullable();
            $table->tinyInteger('nilai_a8')->nullable();
            $table->tinyInteger('nilai_a9')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_GA');
    }
}
