<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookupNGTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_NG', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('nilai_n')->nullable();
            $table->tinyInteger('nilai_g')->nullable();
            $table->tinyInteger('nilai_g0')->nullable();
            $table->tinyInteger('nilai_g1')->nullable();
            $table->tinyInteger('nilai_g2')->nullable();
            $table->tinyInteger('nilai_g3')->nullable();
            $table->tinyInteger('nilai_g4')->nullable();
            $table->tinyInteger('nilai_g5')->nullable();
            $table->tinyInteger('nilai_g6')->nullable();
            $table->tinyInteger('nilai_g7')->nullable();
            $table->tinyInteger('nilai_g8')->nullable();
            $table->tinyInteger('nilai_g9')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_NG');
    }
}
