<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDesc2ToLookupUraianDayaAnalisaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lookup_uraian_daya_analisa', function (Blueprint $table) {
            $table->text('desc2')->after('desc1')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lookup_uraian_daya_analisa', function (Blueprint $table) {
            $table->dropColumn('desc2');
        });
    }
}
