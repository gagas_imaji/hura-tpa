<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookupWZTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_WZ', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('nilai_w')->nullable();
            $table->tinyInteger('nilai_z')->nullable();
            $table->tinyInteger('nilai_z0')->nullable();
            $table->tinyInteger('nilai_z1')->nullable();
            $table->tinyInteger('nilai_z2')->nullable();
            $table->tinyInteger('nilai_z3')->nullable();
            $table->tinyInteger('nilai_z4')->nullable();
            $table->tinyInteger('nilai_z5')->nullable();
            $table->tinyInteger('nilai_z6')->nullable();
            $table->tinyInteger('nilai_z7')->nullable();
            $table->tinyInteger('nilai_z8')->nullable();
            $table->tinyInteger('nilai_z9')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_WZ');
    }
}
