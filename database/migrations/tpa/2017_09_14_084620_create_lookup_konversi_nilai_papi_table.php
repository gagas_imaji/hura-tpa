<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookupKonversiNilaiPapiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_konversi_nilai_papi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_papi',32)->nullable();
            $table->text('deskripsi')->nullable();
            $table->tinyInteger('0')->nullable();
            $table->tinyInteger('1')->nullable();
            $table->tinyInteger('2')->nullable();
            $table->tinyInteger('3')->nullable();
            $table->tinyInteger('4')->nullable();
            $table->tinyInteger('5')->nullable();
            $table->tinyInteger('6')->nullable();
            $table->tinyInteger('7')->nullable();
            $table->tinyInteger('8')->nullable();
            $table->tinyInteger('9')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_konversi_nilai_papi');
    }
}
