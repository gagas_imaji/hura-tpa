<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookupSubtesTiki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_subtes_tiki', function (Blueprint $table) {
            $table->increments('id');
            $table->double('min', 10, 2)->nullable();
            $table->double('max', 10, 2)->nullable();
            $table->double('nilai', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_subtes_tiki');
    }
}
