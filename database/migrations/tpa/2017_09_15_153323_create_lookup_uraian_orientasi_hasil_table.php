<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookupUraianOrientasiHasilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_uraian_orientasi_hasil', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('nilai')->nullable();
            $table->text('desc1')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_uraian_orientasi_hasil');
    }
}
