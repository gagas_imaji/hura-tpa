<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLookupWpaKarakteristikUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_wpa_karakteristik_umum', function (Blueprint $table) {
            $table->increments('id');
            $table->text('D');
            $table->text('I');
            $table->text('S');
            $table->text('C');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_wpa_karakteristik_umum');
    }
}
