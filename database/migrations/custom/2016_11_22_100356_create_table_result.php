<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_answer')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('id_question')->unsigned();
            $table->float('point')->unsigned();
            $table->integer('is_checked')->default(1);
            $table->timestamps();

              $table->foreign('id_answer')
                  ->references('id')  
                  ->on('option_answer')
                  ->onDelete('cascade');

              /*$table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');*/

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result');
    }
}
