<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBankQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_question', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_type_question');
            $table->integer('id_bank_section');
            $table->text('question');
            $table->integer('is_random')->default(0);
            $table->integer('timer')->default(0);
            $table->integer('is_mandatory')->default(0);
            $table->integer('point')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_question');
    }
}
