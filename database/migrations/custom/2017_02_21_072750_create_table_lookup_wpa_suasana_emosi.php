<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLookupWpaSuasanaEmosi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_wpa_suasana_emosi', function (Blueprint $table) {
            $table->increments('id');
            $table->text('D')->nullable();
            $table->text('I')->nullable();
            $table->text('S')->nullable();
            $table->text('C')->nullable();
            $table->text('category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_wpa_suasana_emosi');
    }
}
