<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReportPapikostick extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_papikostick', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('survey_id');
            $table->integer('N');
            $table->integer('G');
            $table->integer('A');
            $table->integer('L');
            $table->integer('P');
            $table->integer('I');
            $table->integer('T');
            $table->integer('V');
            $table->integer('X');
            $table->integer('S');
            $table->integer('B');
            $table->integer('O');
            $table->integer('R');
            $table->integer('D');
            $table->integer('C');
            $table->integer('Z');
            $table->integer('E');
            $table->integer('K');
            $table->integer('F');
            $table->integer('W');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_papikostick');
    }
}
