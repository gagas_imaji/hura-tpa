<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBankSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_survey', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_survey_id')->default(5); //kategori HEAT 
            $table->string('title');
            $table->string('slug');
            $table->string('description');
            $table->string('instruction');
            $table->string('thankyou_text');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->integer("is_random")->default(0);
            $table->integer("timer")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_survey');
    }
}
