<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRawInputLaiTiki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_input', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('section_id');
            $table->integer('category_id');
            $table->integer('survey_id');
            $table->text('raw_input');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_input');
    }
}
