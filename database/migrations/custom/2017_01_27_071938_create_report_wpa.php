<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportWpa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_wpa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('survey_id');
            $table->text('most');
            $table->text('lest');
            $table->text('change');
            $table->string('hasil');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_wpa');
    }
}
