<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('tujuan')->default('-');
            $table->float('most_d')->default(0);
            $table->float('most_i')->default(0);
            $table->float('most_s')->default(0);
            $table->float('most_c')->default(0);
            $table->float('lest_d')->default(0);
            $table->float('lest_i')->default(0);
            $table->float('lest_s')->default(0);
            $table->float('lest_c')->default(0);
            $table->float('change_d')->default(0);
            $table->float('change_i')->default(0);
            $table->float('change_s')->default(0);
            $table->float('change_c')->default(0);
            $table->string('hasil')->default('-');
            $table->float('GTQ1')->default(0);
            $table->float('GTQ2')->default(0);
            $table->float('GTQ3')->default(0);
            $table->float('GTQ4')->default(0);
            $table->float('GTQ5')->default(0);
            $table->string('kriteria')->default('-');
            $table->float('SS1')->default(0);
            $table->float('SS2')->default(0);
            $table->float('SS3')->default(0);
            $table->float('SS4')->default(0);
            $table->float('jumlah')->default(0);
            $table->float('IQ')->default(0);
            $table->float('N')->default(0);
            $table->float('G')->default(0);
            $table->float('A')->default(0);
            $table->float('L')->default(0);
            $table->float('P')->default(0);
            $table->float('I')->default(0);
            $table->float('T')->default(0);
            $table->float('V')->default(0);
            $table->float('X')->default(0);
            $table->float('S')->default(0);
            $table->float('B')->default(0);
            $table->float('O')->default(0);
            $table->float('R')->default(0);
            $table->float('D')->default(0);
            $table->float('C')->default(0);
            $table->float('Z')->default(0);
            $table->float('E')->default(0);
            $table->float('K')->default(0);
            $table->float('F')->default(0);
            $table->float('W')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_report');
    }
}
