<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableJobKarakteristik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_karakteristiks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jobs_id');
            $table->integer('D');
            $table->integer('I');
            $table->integer('S');
            $table->integer('C');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_karakteristiks');
    }
}
