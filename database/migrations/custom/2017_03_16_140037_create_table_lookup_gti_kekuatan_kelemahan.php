<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLookupGtiKekuatanKelemahan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_gti_kekuatan_kelemahan', function (Blueprint $table) {
            $table->increments('id');
            $table->text('GTQ1');
            $table->text('GTQ2');
            $table->text('GTQ3');
            $table->text('GTQ4');
            $table->text('GTQ5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_gti_kekuatan_kelemahan');
    }
}
