<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSectionSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_survey', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->unsigned();
            $table->string("name");
            $table->string("slug");
            $table->text("instruction")->nullable();
            $table->integer("timer")->default(0);
            $table->integer("limit")->default(0);
            $table->timestamps();

              $table->foreign('survey_id')
                  ->references('id')
                  ->on('survey')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_survey');
    }
}
