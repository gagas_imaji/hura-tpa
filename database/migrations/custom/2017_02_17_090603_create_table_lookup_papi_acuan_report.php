<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLookupPapiAcuanReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_papi_acuan_report', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->integer('count');
            $table->text('positive')->nullable();
            $table->text('negative')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_papi_acuan_report');
        Schema::dropIfExists('papi_acuan_report');
    }
}
