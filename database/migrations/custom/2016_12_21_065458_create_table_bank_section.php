<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBankSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_section', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_survey_id')->unsigned();
            $table->string("name");
            $table->string("slug");
            $table->text("instruction")->nullable();
            $table->integer("timer")->default(0);
            $table->integer("limit")->default(0);
            $table->timestamps();

              $table->foreign('bank_survey_id')
                  ->references('id')
                  ->on('bank_survey')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_section');
    }
}
