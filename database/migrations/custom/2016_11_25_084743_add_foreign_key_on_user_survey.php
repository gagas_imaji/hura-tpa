<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyOnUserSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_survey', function (Blueprint $table) {
            $table->integer('id_survey')->unsigned();
            $table->integer('id_user')->unsigned(); 
            $table->integer('id_project')->unsigned(); 
            $table->integer('is_done')->default(0);
            $table->float('score_final')->default(0);
            $table->string('grade')->nullable();
            /*
            $table->foreign('id_survey')
                  ->references('id') 
                  ->on('survey')
                  ->onDelete('cascade');
                  
            $table->foreign('id_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_survey', function (Blueprint $table) {
            $table->dropColumn('id_survey');
            $table->dropColumn('id_user');
            $table->dropColumn('is_done');
            $table->dropColumn('score_final');
            $table->dropColumn('grade');
        });
    }
}
