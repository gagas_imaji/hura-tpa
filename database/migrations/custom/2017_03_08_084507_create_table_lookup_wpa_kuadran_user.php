<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLookupWpaKuadranUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_wpa_kuadran_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('grafik');
            $table->integer('total');
            $table->integer('D');
            $table->integer('I');
            $table->integer('S');
            $table->integer('C');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_wpa_kuadran_user');
    }
}
