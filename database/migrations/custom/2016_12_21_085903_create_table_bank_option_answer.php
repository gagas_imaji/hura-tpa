<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBankOptionAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_option_answer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_bank_question')->unsigned();
            $table->string('answer');
            $table->text('image');
            $table->integer('point')->default(0);
            $table->integer('hidden')->default(0);
            $table->timestamps();

               $table->foreign('id_bank_question')
                  ->references('id')
                  ->on('bank_question')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_option_answer');
    }
}
