<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_type_question')->unsigned();
            $table->integer('id_section')->unsigned();
            $table->text("question");
            $table->integer('is_random')->default(0);
            $table->integer('timer')->default(0);
            $table->integer('is_mandatory')->default(0);
            $table->float('point')->default(1);
            $table->timestamps();

             $table->foreign('id_type_question')
                  ->references('id')
                  ->on('question_type')
                  ->onDelete('cascade');

                $table->foreign('id_section')
                  ->references('id')
                  ->on('section_survey')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question');
    }
}
