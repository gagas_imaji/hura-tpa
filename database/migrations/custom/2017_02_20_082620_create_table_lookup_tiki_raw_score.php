<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLookupTikiRawScore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_tiki_raw_score', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('raw_score');
            $table->integer('subtest1')->nullable();
            $table->integer('subtest2')->nullable();
            $table->integer('subtest3')->nullable();
            $table->integer('subtest4')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_tiki_raw_score');
    }
}
