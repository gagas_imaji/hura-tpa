<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReportWpaUraianKepribadian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_wpa_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('survey_id');
            $table->text('karakteristik_pekerjaan');
            $table->text('karakteristik_umum');
            $table->text('kekuatan');
            $table->text('kelemahan');
            $table->text('perilaku_kerja_kekuatan');
            $table->text('perilaku_kerja_kelemahan');
            $table->text('suasana_emosi_kekuatan');
            $table->text('suasana_emosi_kelemahan');
            $table->text('uraian_kepribadian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_wpa_detail');
        Schema::dropIfExists('report_wpa_uraian_kepribadian');
        Schema::dropIfExists('report_wpa_karakteristik_umum');
        Schema::dropIfExists('report_wpa_karakteristik_pekerjaan');
        Schema::dropIfExists('report_wpa_perilaku_kerja_kekuatan');
        Schema::dropIfExists('report_wpa_perilaku_kerja_kelemahan');
        Schema::dropIfExists('report_wpa_suasana_emosi_kekuatan');
        Schema::dropIfExists('report_wpa_suasana_emosi_kelemahan');
        Schema::dropIfExists('report_wpa_kekuatan');
        Schema::dropIfExists('report_wpa_kelemahan');
    }
}
