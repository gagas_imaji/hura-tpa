<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trial_user extends CI_Controller {

     public function index() {
       $this->template->build('trial');
       $this->load->library('email');
       $this->load->helper('form');
    }

   public function request(){
       if($_POST){
           $data = array(
               "company_name"=>$this->input->post("name"),
               "hrd"=>$this->input->post("hrd"),
               "hrd_email"=>$this->input->post("hrd_email"),
               "direktur"=>$this->input->post("direktur"),
               "email_direktur"=>$this->input->post("email_direktur"),
               "manager"=>$this->input->post("manager"),
               "email_manager"=>$this->input->post("email_manager"),
               "staf_1"=>$this->input->post("staf_1"),
               "email_staf_1"=>$this->input->post("email_staf_1"),
               "staf_2"=>$this->input->post("staf_2"),
               "email_staf_2"=>$this->input->post("email_staf_2"),
               "staf_3"=>$this->input->post("staf_3"),
               "email_staf_3"=>$this->input->post("email_staf_3"),
               "staf_4"=>$this->input->post("staf_4"),
               "email_staf_4"=>$this->input->post("email_staf_4"),
           );
         
          $this->save_contact($this->input->post("hrd_email"),$data);
          redirect("/auth/after-regist");
       }else{
           redirect("404");
       }
   }
   private function notif_followup($email,$additional_data) {
        
        $var_email = array(
            "name" => $additional_data['hrd'],
        );
        
        $subject = "Permintaaan Akun Trial talenthub.id Anda <hello@talenthub.id> ";
        //ini unutuk menunjukan email ini dari siapa 
        $this->email->from('hello@talenthub.id', 'talenthub.id');
        //jelas ini email tujuan
        #$this->email->to($email);
        $this->email->to("aris@gagasimaji.com");
        $this->email->to("liliek@gagasimaji.com");
        //ini tentunya subject
        $this->email->subject($subject);
        //yup ini konten email
        $this->email->message($this->load->view('trial_template', $var_email, TRUE));
     
        $this->email->send();
    }
    public function save_contact($email,$data){
         $this->db->insert("trial_request", $data);
         #$this->notif_followup($email, $data);
    }
}
