/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    console.log(base_url);
    $("#search-btn").click(function () {
        var quarter = $("#filter-quarter").val();
        var level = $("#filter-level").val();
        var division = $("#filter-division").val();
        var participant = $("#filter-participant").val();
         console.log(base_url + "/backend/report/"+level+"|"+division+"|"+participant);
        var url = base_url + "/backend/report/"+ quarter + "---" + level + "---" + division + "---" + participant;
       window.location.replace(url);
    })
    $("#filter-division, #filter-level").change(function () {
        var level = $("#filter-level").val();
        var division = $("#filter-division").val();
        var PostData = {
//mengambil nilai dari parents saat terdapat perubahan
            'level': level,
            'division' : division
        };
        AjaxRequest(PostData);
    })

    $("#search-reset").click(function () {

        var url = base_url + "/backend/report/";
        window.location.replace(url);
    });

    function AjaxRequest(PostData) {
//mengirimkan data ke file php bernama response.php
       $('#filter-participant').html("");
        $.post(base_url + "/backend/lookup_user", PostData,
                function (results) {
                   $('#filter-participant').append(new Option("---------", 0, true, true));
//di looping berdasarkan jumlah yang didapat
                    $.each(results, function (key, val) {
                      if(val.last_name == null){
                          var lastname = " ";
                      }else{
                          var lastname = val.last_name;
                      }
                        $('#filter-participant').append(new Option(val.first_name+" "+lastname, val.id, true, true));
                    });
                   
                }, "json");
    }
})