$(function() {
	
	// page transition
    'use strict';
	
    $(document).ready(function () {
		
        // Init here.
        var $body = $('body'),
		$main = $('.tool-container'),
		$site = $('html, body'),
		transition = 'fade',
		smoothState;
		
        smoothState = $main.smoothState({
            onBefore: function($anchor, $container) {
                var current = $('[data-viewport]').first().data('viewport'),
				target = $anchor.data('target');
                current = current ? current : 0;
                target = target ? target : 0;
                if (current === target) {
                    transition = 'fade';
					} else if (current < target) {
                    transition = 'moveright';
					} else {
                    transition = 'moveleft';
				}
			},
            onStart: {
                duration: 400,
                render: function (url, $container) {
                    $main.attr('data-transition', transition);
                    $main.addClass('is-exiting');
                    $site.animate({scrollTop: 0});
				}
			},
            onReady: {
                duration: 0,
                render: function ($container, $newContent) {
                    $container.html($newContent);
                    $container.removeClass('is-exiting');
				}
			},
		}).data('smoothState');
		
	});
	
	
	
	

});

// keyboard shortcuts
$(document).keypress(function(e) {
	
	if(e.charCode == 97) {
        console.log("A");
        $('input:radio[id=A]').prop('checked',true);
		$('.option-text label').removeClass('active');
		$('input:radio[id=A]').parent().addClass('active'); 
		
		var checkboxesA;
		checkboxesA = $('input:checkbox[id=A]').change(function(){
			if(this.checked){
				$(this).parent().addClass('active'); 
			}	
		});		
		
	}
	
	if(e.charCode == 98) {
        console.log("B");
        $('input:radio[id=B]').prop('checked',true);
		$('.option-text label').removeClass('active');
		$('input:radio[id=B]').parent().addClass('active'); 

		var checkboxesB;
		checkboxesB = $('input:checkbox[id=B]').change(function(event){
			$('input:checkbox[id=B]').prop('checked',true);
			$('input:checkbox[id=B]').parent().addClass('active'); 
		});		
		
	}
	
	if(e.charCode == 99) {
        console.log("C");
        $('input:radio[id=C], input:checkbox[id=C]').prop('checked',true);
 		$('.option-text label').removeClass('active');
		$('input:radio[id=C], input:checkbox[id=C]').parent().addClass('active'); 
	}
	
	if(e.charCode == 100) {
        console.log("D");
        $('input:radio[id=D], input:checkbox[id=D]').prop('checked',true);
  		$('.option-text label').removeClass('active');
		$('input:radio[id=D], input:checkbox[id=D]').parent().addClass('active'); 
	}
	
	if(e.charCode == 101) {
        console.log("E");
        $('input:radio[id=E], input:checkbox[id=E]').prop('checked',true);
  		$('.option-text label').removeClass('active');
		$('input:radio[id=E], input:checkbox[id=E]').parent().addClass('active'); 
	}                  
	
	if(e.charCode == 102) {
        console.log("F");
        $('input:radio[id=F], input:checkbox[id=F]').prop('checked',true);
  		$('.option-text label').removeClass('active');
		$('input:radio[id=F], input:checkbox[id=F]').parent().addClass('active'); 
	}                  
	
	});	
