function performance() {
    var performance = $('.performance').val();
    var utrecht = $('.utrecht').val();
    var perf = [];
    var i=1;
    $(".performance").each(function (index) {
        perf.push(
                {
                    id: $(this).attr('n'),
                    x: parseFloat($(this).attr('x')),
                    y: i
                }
        )
        i++;
    });
   
    var spline = new CanvasJS.Chart("chartPerformance",
            {
                animationEnabled: true,
                title: {
                    text: "",
                    fontSize: 25
                },
                axisY: {
                    interlacedColor: "#f7f7f7",
                    gridColor: "#bebebe",
                    margin: 30,
                    minimum: 0,
                    maximum: 400

                },
                axisX: {
                    margin: 30,
                    minimum: 0,
                    maximum: 200
                },
                data: [
                    {
                        type: "spline", //change type to bar, line, area, pie, etc
                        showInLegend: true,
                        dataPoints: [
                            {x: 60, y: 0},
                            {x: 70, y: 16},
                            {x: 80, y: 30},
                            {x: 90, y: 300},
                            {x: 100, y: 340},
                            {x: 110, y: 130},
                            {x: 120, y: 20}
                        ]
                    },
                ],
                legend: {
                    cursor: "pointer",
                    itemclick: function (e) {
                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }
                        spline.render();
                    }
                }
            });

    spline.render();
}

function engagement() {
    var spline2 = new CanvasJS.Chart("chartEngagement",
            {
                animationEnabled: true,
                title: {
                    text: "",
                    fontSize: 18
                },
                axisY: {
                    interlacedColor: "#f7f7f7",
                    gridColor: "#bebebe",
                    margin: 30,
                    minimum: 0,
                    maximum: 400

                },
                axisX: {
                    margin: 30,
                    minimum: 0,
                    maximum: 200
                },
                data: [
                    {
                        type: "spline", //change type to bar, line, area, pie, etc
                        showInLegend: true,
                        dataPoints: [
                            {x: 60, y: 0},
                            {x: 70, y: 16},
                            {x: 80, y: 30},
                            {x: 90, y: 200},
                            {x: 100, y: 210},
                            {x: 110, y: 100},
                            {x: 120, y: 30},
                            {x: 130, y: 20},
                            {x: 140, y: 15}
                        ]
                    },
                ],
                legend: {
                    cursor: "pointer",
                    itemclick: function (e) {
                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }
                        spline2.render();
                    }
                }
            });

    spline2.render();
}

function report() {
    var performance = $('.performance').val();
    var utrecht = $('.utrecht').val();
    var perf = [];
    var utr = [];
    $(".performance").each(function (index) {
        perf.push(
                {
                    id: $(this).attr('n'),
                    x: parseFloat($(this).attr('x')),
                    y: parseFloat($(this).attr('y')),
                }
        )
    });

    
    var d = new Date();
    var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");
    current_month = d.getMonth();
    current_year = d.getFullYear();
    current = m_names[current_month] + '-' + current_year;
    var chart = new CanvasJS.Chart("chartContainer",
            {
                zoomEnabled: true,
                title: {
                    text: "Garfik Survey",
                    fontSize: 20

                },
                backgroundColor: "",
                animationEnabled: true,
                axisX: {
                    title: "Engagement",
                    minimum: 0,
                    //labelAngle: -40,
                    maximum: 6,
                    interval: 1,
                    labelFontSize: 14,
                    titleFontSize: 18
                },
                axisY: {
                    title: "Performance",
                    minimum: 0,
                    //labelAngle: -40,
                    maximum: 5,
                    interval: 1,
                    lineThickness: 1,
                    labelFontSize: 14,
                    titleFontSize: 18
                },
                data: [
                    {
                        
                        type: "scatter",
                        toolTipContent: "<span style='\"'color: {color};'\"'><strong>Name: </strong></span>{id}<br/><span style='\"'color: {color};'\"'><strong>Engagement: </strong></span>{x}<br/><span style='\"'color: {color};'\"'><strong>Performance: </strong></span>{y}<br/>  ",
                        dataPoints: perf
                    }

                ]
            });
    
    chart.render();
}


$(document).ready(function () {
    //performance();
    report();
    //engagement();
}) 