@extends('layouts.default')
    @section('breadcrumb')
            <div id="breadcrumb">
                <ol class="breadcrumb">
                    <li class="active">Home</li>
                </ol>
            </div>
    @stop

    @section('content')
        @if(!Entrust::hasRole(DEFAULT_ROLE))
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-line-chart fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $dashboard['participant'] }} %</div><br/>
                                <div>Tingkat partisipasi</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-folder-open-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $dashboard['total_project'] }}</div><br/>
                                <div>Total project</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-handshake-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $dashboard['credit'] }} </div><br/>
                                <div>Kredit saat kontrak</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-paper-plane-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $dashboard['peserta_diassign'] }}</div><br/>
                                <div>Peserta diassign ke project berjalan</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tags fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $dashboard['peserta_menyelesaikan'] }}</div><br/>
                                <div>Peserta menyelesaikan</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-cart-plus fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $dashboard['jatah'] }} </div><br/>
                                <div>Sisa yang dapat digunakan</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(Entrust::can('take-test'))
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Daftar Tools</div>
                    <p>Daftar test yang wajib anda ikuti.</p>
                </div>
            </div>
            <div class="panel panel-body">
                <div class="table-vertical">
                    <table id="surveyListTable" class="table table-hover table-bordered">
                        <tbody>
                            <?php $i = 0; ?>
                            <input type="hidden" name="count" id="count_survey" value="{{ $count }}" />
                            @if($survey_wajib -> count() != 0)
                                @foreach($survey_wajib as $surveys)
                                    <tr class="list-survey">
                                        <td class="td-day" id="day_left{{ $i }}">
                                            <input type="hidden" name="day_left" value="{!! $surveys->end_date !!}" id="end_date{{ $i }}" />
                                            <span class="td-day-date">00</span>
                                            <span class="td-day-left">hari lagi</span>
                                        </td>
                                        <td class="td-survey"><h4 class="survey-title">{!! $surveys->title !!}</h4></td>

                                        <td class="td-duration">
                                            @if($surveys -> timer != 0)
                                                <span class="text-muted">Durasi:</span>{!! $surveys -> timer/60 !!} menit
                                            @endif
                                        </td>


                                        <td class="td-action"><div id="btn-reminder"><a href="/survey/{{ $surveys->slug }}/{{ $section_id[$i] }}/intro" class="btn btn-sm btn-primary act-reminder"  target="_blank">Start Survey</a></div></td>
                                    </tr>
                                <?php $i++; ?>
                                @endforeach
                            @else
                                <tr class="list-survey">
                                    <td class="td-survey" colspan="5"><h6 class="survey-title">Tidak ada tes yang wajib Anda ikuti untuk saat ini</h6></td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        @endif

        @if(!Entrust::hasRole(DEFAULT_ROLE))

            <div class="panel panel-transparent">
                <div class="panel-heading"><div class="panel-title">Project yang sedang berjalan</div>
                <p>Berikut adalah daftar project yang sedang berjalan. </p>

                </div>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        @if($projects -> count() != 0)
                            <tr class="headings">
                                <th class="column-title">Nama </th>
                                <th class="column-title">Tanggal Awal </th>
                                <th class="column-title">Tanggal Akhir </th>
                                <th class="column-title">Status </th>
                            </tr>
                        @else
                            <tr class="headings">
                                <th class="column-title" colspan="4">Tidak ada project yang sedang berjalan saat ini </th>
                            </tr>
                        @endif
                    </thead>
                    <tbody>
                        @if($projects -> count() != 0)
                            @foreach($projects as $project)
                            <tr class="">
                                <td class=" ">
                                    <a href="/project/detail/{{ $project -> id }}"><strong> <span class="glyphicon glyphicon-search"></span> {{ $project -> name }}</strong></a>
                                </td>
                                <td class=" ">{{ $project -> start_date }}</td>
                                <td class=" ">{{ $project -> end_date }} </td>
                                @if($project -> is_done == 0)
                                    <td><span class="label label-warning">Belum Berjalan</span></td>
                                @elseif($project -> is_done == 1)
                                    <td><span class="label label-danger">Selesai</span></td>
                                @elseif($project -> is_done == 2)
                                    <td> <span class="label label-success"> Sedang Berjalan</span></td>
                                @endif


                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            
        @endif
        
        <div class="modal fade slide-up disable-scroll" id="reminder" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                            <h4 class="modal-title"><span class="semi-bold">Kirim Pengingat</span></h4>
                            <p class="p-b-10">Kirimkan pengingat kepada peserta agar segera mengikuti survey sebelum periode survey berakhir.</p>
                        </div>
                        <div class="modal-body">
                            <hr>
                            <form action="backend/survey-reminder" method="POST" id="reminder-survey" data-submit="noAjax">
                                {!! csrf_field() !!}
                                <p>Yang terhormat, [Nama Peserta]</p>
                                <p>Anda memiliki survey yang belum diikuti.</p>
                                <input type="hidden" id="id_schedule" name="id_schedule"><br>
                                <div class="form-group form-group-default">
                                    <label>Buat Pesan Tambahan</label>
                                    <textarea name="email_konten" class="form-control" rows="3" style="height: 175px;"></textarea>
                                </div>
                                <p>Silahkan <a href="/login">Login</a> ke talenthub.id untuk memulai survey.</p>
                                <hr>
                                <input type="submit" class="btn btn-submit btn-info" name="submit" value="Kirim">
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
            </div>
        </div>

    @stop

    @section('script')
        <script type="text/javascript">
            
        </script>
    @stop
