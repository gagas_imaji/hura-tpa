@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/corporate">Corporate</a></li>
                <li class="active">{!! $corporate->name !!}</li>
            </ol>

        </div>
	@stop

	@section('content')
		<div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Corporate</strong> {!!trans('messages.profile')!!}
                    </div>
                    <div class="panel-body full">
                        <div class="row">
                            <div class="col-md-4  col-md-offset-3" style="margin-top:20px;margin-bottom:20px;">{!! getAvatar($corporate->id,150) !!}</div>
                        </div>
                        <div class="responsive">
                            <table class="table table-stripped table-hover show-table">
                                <tbody>
                                    <tr>
                                        <th>{{trans('messages.name')}}</th>
                                        <td>{{$corporate->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{trans('messages.description')}}</th>
                                        <td>{{$corporate->description}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{trans('messages.contact')}}</th>
                                        <td>{{$corporate->contact}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{trans('messages.email')}}</th>
                                        <td>{{$corporate->email}}</td>
                                    </tr>

                                    @if(!config('config.login'))
                                    <tr>
                                        <th>{{trans('messages.username')}}</th>
                                        <td>{{$corporate->username}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
    			<div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general-tab" data-toggle="tab">{{trans('messages.general')}}</a>
                        </li>
                        <li><a href="#avatar-tab" data-toggle="tab">{{trans('messages.avatar')}}</a>
                        </li>
                        <li><a href="#custom-field-tab" data-toggle="tab">{{trans('messages.custom').' '.trans('messages.field')}}</a>
                        </li>
                        @if(config('config.enable_email_template'))
                            <li><a href="#email-tab" data-toggle="tab">{{trans('messages.email')}}</a>
                            </li>
                        @endif
                        <li><a href="#division-tab" data-toggle="tab">Division</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="general-tab">
                            <div class="panel panel-default m-t-10">
                                <div class="panel-heading">
                                    {{trans('messages.general')}}
                                </div>
                                <div class="panel-body">
                                    {!! Form::model($corporate,['method' => 'POST','route' => ['corporate.profile-update',$corporate->id] ,'class' => 'corporate-profile-form','id' => 'corporate-profile-form','data-no-form-clear' => 1]) !!}
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                {!! Form::label('name',trans('messages.name'),[])!!}
                                                {!! Form::input('text','name',$corporate->name,['class'=>'form-control','placeholder'=>trans('messages.name')])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('description',trans('messages.description'))!!}
                                                {!! Form::textarea('description',$corporate->description,['class'=>'form-control','placeholder'=>trans('messages.description')])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('contact',trans('messages.contact'))!!}
                                                {!! Form::input('text','contact',$corporate->contact,['class'=>'form-control','placeholder'=>trans('messages.contact')])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('email',trans('messages.email'))!!}
                                                {!! Form::input('text','email',$corporate->email,['class'=>'form-control','placeholder'=>trans('messages.email')])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('credit','Credit')!!}
                                                {!! Form::input('number','credit',$credit,['class'=>'form-control','placeholder'=>trans('messages.contact')], 'required')!!}
                                            </div>
                                            {!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-right', 'id' => 'update_profile_corp']) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade in" id="avatar-tab">
                            <div class="panel panel-default m-t-10">
                                <div class="panel-heading">
                                    {{trans('messages.avatar')}}
                                </div>
                                <div class="panel-body">
                                    {!! Form::model($corporate,['files' => true, 'method' => 'POST','route' => ['corporate.avatar',$corporate->id] ,'class' => 'user-avatar-form','id' => 'user-avatar-form','data-submit' => 'noAjax']) !!}
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="file" name="avatar" id="avatar" title="{!! trans('messages.select').' '.trans('messages.avatar') !!}" class="fileinput" data-buttonText="{!! trans('messages.select').' '.trans('messages.avatar') !!}">
                                            </div>
                                        </div>
                                    </div>
                                    @if($corporate->image && File::exists(config('constant.upload_path.avatar').$corporate->image))
                                    <div class="form-group">
                                        <img src="{!! URL::to(config('constant.upload_path.avatar').$corporate->image) !!}" width="150px" style="margin-left:20px;">
                                        <div class="checkbox">
                                            <label>
                                              <input name="remove_avatar" type="checkbox" class="switch-input" data-size="mini" data-on-text="Yes" data-off-text="No" value="1" data-off-value="0"> {!! trans('messages.remove').' '.trans('messages.avatar') !!}
                                            </label>
                                        </div>
                                    </div>
                                    @endif
                                    {!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary', 'id' => 'update_avatar_corp']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade in" id="custom-field-tab">
                            <div class="panel panel-default m-t-10">
                                <div class="panel-heading">
                                    {{trans('messages.custom').' '.trans('messages.field')}}
                                </div>
                                <div class="panel-body">

                                    {!! Form::model($corporate,['method' => 'POST','route' => ['corporate.custom-field-update',$corporate->id] ,'class' => 'user-custom-field-form','id' => 'user-custom-field-form','data-no-form-clear' => 1]) !!}
                                    {{ getCustomFields('corporate-registration-form',$custom_register_field_values) }}
                                    {!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-right', 'id' => 'custom_field_corp']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade in" id="division-tab">
                            <div class="panel panel-default m-t-10">
                                <div class="panel-heading">
                                    List Semua Divisi
                                </div>
                                <div class="panel-body">
                                    @if(Entrust::can('manage-corporate'))
                                        <div class="additional-btn">
                                            <a href="/division/create/{{$corporate->id}}" class="btn btn-sm btn-primary">{{trans('messages.add_new')}}</a>
                                        </div>
                                    @endif
                                    <table id="user_report" class="table table-bordered table-hover text-center">
                                        @if($rowset)
                                        <thead>
                                            <tr>
                                                @foreach($col_heads as $column)
                                                    <th>{{$column}}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1;?>
                                            @foreach($rowset as $row)
                                                <tr>
                                                    <td class="text-left">{{$i}}</td>
                                                    <td class="text-left">{{$row['name']}}</td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="2">Belum ada divisi terdaftar untuk coporate ini</td>
                                            </tr>
                                        </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop