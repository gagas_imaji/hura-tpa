@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/corporate">Corporate</a></li>
                <li class="active">{!! trans('messages.add_new') !!} Corporate</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-12"> 
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>{{trans('messages.add_new')}}</strong> Corporate
                    	<div class="additional-btn">
                    		<a href="/corporate" data-href="/corporate" class="btn btn-sm btn-primary">{{trans('messages.list_all')}}</a>
                    	</div>
                    </div>
                    <div class="panel-body">
                    	{!! Form::open(['route' => 'corporate.store', 'role' => 'form', 'class' => 'corporate-form','id' => 'corporate-form', 'data-submit' => 'noAjax']) !!}
                        <div class="col-lg-7">
                            <div class="form-group form-group-default required">
                        		{!! Form::label('name',trans('messages.name'),[])!!}
                                {!! Form::input('text','name','',['class'=>'form-control','placeholder'=>trans('messages.name')])!!}
                            </div>
                            <div class="form-group form-group-default">
                                {!! Form::label('description',trans('messages.description'))!!}
                                {!! Form::textarea('description','',['class'=>'form-control','placeholder'=>trans('messages.description')])!!}
                            </div>
                            <div class="form-group form-group-default">
                                {!! Form::label('description','Credit')!!}
                                {!! Form::number('credit','',['class'=>'form-control','placeholder'=>'Credit'])!!}
                            </div>
                            <div class="form-group form-group-default required">
                                {!! Form::label('contact',trans('messages.contact'))!!}
                                {!! Form::input('text','contact','',['class'=>'form-control','placeholder'=>trans('messages.contact')])!!}
                            </div>
                            <div class="form-group form-group-default">
                                {!! Form::label('email',trans('messages.email'))!!}
                                {!! Form::input('text','email','',['class'=>'form-control','placeholder'=>trans('messages.email')])!!}
                            </div>
                            
    						<div class="form-group">
    							{!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-left', 'id' => 'create_corp']) !!}
    						</div>
                        </div>
						{!! Form::close() !!}
                    </div>
                </div>
            </div>
		</div>
	@stop