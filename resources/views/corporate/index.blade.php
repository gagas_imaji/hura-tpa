@extends('layouts.default')

	@section('breadcrumb')
		 <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Corporate</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>{{trans('messages.list_all')}}</strong> Corporate
                        @if(Entrust::can('create-corporate'))
                    	<div class="additional-btn">
                    		<a href="/corporate/create" class="btn btn-sm btn-primary">{{trans('messages.add_new')}}</a>
                    	</div>
                        @endif
                    </div>
                    <div class="panel-body full">
                        @include('common.datatable',['table' => $table_data['corporate-table']])
                    </div>
                </div>
            </div>
		</div>
	@stop