@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/corporate">Corporate</a></li>
                <li class="active">{!! trans('messages.add_new') !!} Division</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-12"> 
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>{{trans('messages.add_new')}}</strong> Division</div>
                    <div class="panel-body">
                    	<form name="create_assessment" id="create_assessment" action="/division-add" method="post" enctype="multipart/form-data" data-submit="noAjax">
                        {!! csrf_field() !!}
                        <div class="col-lg-7">
                            {!! Form::hidden('id',$id,['class'=>'form-control'])!!}
                            <div class="form-group form-group-default required">
                                {!! Form::label('name',trans('messages.name'),[])!!}
                                {!! Form::input('text','name','',['class'=>'form-control','placeholder'=>trans('messages.name')])!!}
                            </div>
    						<div class="form-group">
    							{!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-left', 'id' => 'create_division']) !!}
    						</div>
                        </div>
						</form>
                    </div>
                </div>
            </div>
		</div>
	@stop