@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/aspek-psikograph">STANDAR NILAI ASPEK PSIKOGRAPH</a></li>
                <li class="active">Standar Nilai Project</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<div class="panel-title text-danger">
                    		<i class="fa fa-edit"></i> Tambah Standar Nilai
                    	</div> 
                    </div>
                    <div class="panel-body">
                		<form name="create_assessment" id="create_assessment" action="{{url('standar_nilai/create')}}" method="post" enctype="multipart/form-data" data-submit="noAjax" novalidate>
	                		{!! csrf_field() !!}
	                		<div class="col-lg-7">
								<div class="form-group form-group-default col-lg-12 required">
								    <label>Nama Jabatan</label>
									<input type="text" name="name" class="form-control" placeholder="Nama" value="" />
								</div>
								<table class="table table-hover no-footer table-bordered table-striped" id="list-aspek-psikograph">
		                            <thead>
		                                <tr class="headings">
		                                    <th>Aspek Psikograph</th>
		                                    <th>Kategori Aspek</th>
		                                    <th>Nilai</th>
		                                    <th>Mandatory</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                @if(!is_null($listAspeks))
		                                    @foreach($listAspeks as $row)
		                                    <tr>
		                                        <td>{{$row->nama_aspek}}</td>
		                                        <td><b>{{$row->tpaKategoriAspek->nama}}</b></td>
		                                        <td>
		                                        	<select class="form-control pilih_nilai_aspek" name="nilai_aspek_{{$row->id}}" id="nilai_aspek_{{$row->id}}">
		                                        		@for ($tr = 0; $tr <= 5; $tr++)
		                                        			<option value="{{$tr}}">{{$tr}}</option>
		                                        		@endfor
													</select>
		                                        </td>
		                                        <td>
		                                        	<input type="radio" class="mandatory_aspek_iya" name="mandatory_aspek_{{$row->id}}" id="mandatory_aspek_ya_{{$row->id}}" value="1"> Iya<br>
  													<input type="radio" class="mandatory_aspek_tidak" name="mandatory_aspek_{{$row->id}}" id="mandatory_aspek_tidak_{{$row->id}}" value="0" checked="checked"> Tidak<br>
		                                        </td>
		                                    </tr>
		                                    @endforeach
		                                @endif
		                            </tbody>
		                        </table>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
										{!! Form::submit(isset($buttonText) ? $buttonText : trans('messages.save'),['class' => 'btn btn-primary pull-left', 'id'=>'save_standar_nilai']) !!}
								</div>
							</div>
						</form>
					</div>
				</div>
						
            </div>
        </div>
        {!! Html::style('assets/css/jquery-ui.css') !!}
        {!! Html::script('assets/vendor/jquery/jquery-ui.min.js') !!}
        {!! Html::script('assets/vendor/select2/select2.min.js') !!}
        <script type="text/javascript">
	        $(document).ready(function() {
	            var table = $('#list-aspek-psikograph').DataTable({
	                    "columnDefs": [
	                    { "visible": false, "targets": 1 },
	                    { "width": "200px", "targets": 0 }
	                    ],
	                    "order": [[ 1, 'asc' ]],
	                    "bFilter": false,
	                    "searching": false,
	                    "paging": false,
	                    "bInfo": false,
	                    "bSort": false,
	                    "drawCallback": function ( settings ) {
	                        var api = this.api();
	                        var rows = api.rows( {page:'current'} ).nodes();
	                        var last=null;
	                        
	                        api.column(1, {page:'current'} ).data().each( function ( group, i ) {
	                            if ( last !== group ) {
	                                $(rows).eq( i ).before(
	                                '<tr class="group"><td colspan="3">'+group+'</td></tr>'
	                                );
	                                
	                                last = group;
	                            }
	                        } );
	                    }
	                });

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
					}
				});

	        });
        </script>
	@stop