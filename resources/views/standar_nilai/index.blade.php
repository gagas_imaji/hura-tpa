@extends('layouts.default')
    @section('inline-css')
        <style type="text/css">
            .modal .modal-body {
                padding-top: 25px;
            }

            #standar-nilai td.mandatory.text-center {
			    background-color: #ffe0b2 !important;
			}
            
        </style>
		{!! Html::style('assets/vendor/datatable-tpa/extensions/fixedcolumns/css/fixedColumns.bootstrap.min.css') !!}
    @stop
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Standar Nilai Psikograph</li>
            </ol>
        </div>
    @stop
    
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger">
                            <i class="fa fa-list"></i> Standar Nilai Psikograph
                        </div>
                    </div> 
                    <div class="panel-body full">
                        <table class="table table-hover no-footer table-bordered table-striped" id="standar-nilai" cellspacing="0" width="100%">
                            <thead>
                                <tr class="headings">
                                    <th rowspan="2">Nama Jabatan</th>
									<th colspan="{{ sizeof(App\AspekPsikologis::where('kategori_aspek_id','=',1)->get()) }}" class="text-center">KEMAMPUAN INTELEKTUAL</th>
									<th colspan="{{ sizeof(App\AspekPsikologis::where('kategori_aspek_id','=',2)->get()) }}" class="text-center">SIKAP DAN CARA KERJA</th>
									<th colspan="{{ sizeof(App\AspekPsikologis::where('kategori_aspek_id','=',3)->get()) }}" class="text-center">KEPRIBADIAN</th>
                                </tr>
                                <tr>
									@foreach($aspeks as $aspek)
										<th>{{ $aspek['nama_aspek'] }}</th>
									@endforeach
								</tr>
                            </thead>
                            <tbody>
                            	@foreach($datas as $data)
                                <tr>
                                	<td><i class="fa fa-tags" aria-hidden="true"></i> {{ $data->nama_jabatan}}</td>
                                	@foreach($aspeks as $aspek)
                                		<td class="{{isMandatory($data->id,$aspek['id'])}}">{{skorStandarNilai($data->id,$aspek['id'])}}</td>
                                	@endforeach
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        {!! Html::script('assets/vendor/datatable-tpa/extensions/responsive/js/dataTables.responsive.min.js') !!}
		{!! Html::script('assets/vendor/datatable-tpa/extensions/fixedcolumns/js/dataTables.fixedColumns.min.js') !!}
        <script type="text/javascript">

        $(document).ready(function() {
            var oTable = $('#standar-nilai').DataTable({
                    "columnDefs": [
                    { "width": "200px", "targets": 0 },
                    { "className": "text-center", "targets": [1,2,3,4,5,6,7,8,9,10] }
                    ],
                    "order": [[ 1, 'asc' ]],
                    "displayLength": 10,
                    "bSort": false,
                    "sScrollX": true,
                    "sScrollY": false,
                    "sScrollXInner": "150%",
					"bScrollCollapse": true,
                    "fixedColumns": true,
                    "colReorder": true
            });
        });
        </script>
    @stop