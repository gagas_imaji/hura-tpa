<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <div style="margin-top:0%;width:100%;">

    </div>
    <div style="padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;margin-top:3%;border-top:3px solid #ee0000;">

        <div style="background:#fff;height:50px;float: left;">



        </div>
    </div>

    <div style="margin-right: -15px;margin-left: -15px;">
        <div style="width:100%;">
            <p>
                Dear Candidates, <br/>
                Terima kasih atas partisipasi Anda dalam proses seleksi {{ $corporate_name }}. Untuk proses selanjutnya, Kami mengundang Anda untuk menyelesaikan online assessment dengan ketentuan sebagai berikut :
            </p>
            
            <p>
                <ol>
                    <li>Jadwal pelaksanaan online assessment dimulai {{ date('d-m-Y H:i:s', strtotime($start_date)) }}</li>
                    <li>Anda hanya dapat mengakses materi assessment online di : <a href="http://danamon.talentlytica.com/user-token?token={{$token}}"> http://danamon.talentlytica.com</a>
                    </li>
                    <li> Link online assessment hanya dapat diakses sampai dengan <b>tiga hari setelah email ini diterima.</b></li>
                    <li>Assessment online ini terdiri atas {{ $jlh_tools }} test. Anda <b>wajib mengikuti</b> seluruh test sesuai dengan jadwal di atas.</li>
                    <li>Seluruh materi assessment online adalah milik Talentlytica, sehingga Anda <b>dilarang</b> memberikan atau menyebarluaskan materi tes dalam <b>bentuk apapun</b> dan <b>kepada siapapun</b></li>
                    <li>Pastikan Anda menggunakan jaringan internet dan listrik yang stabil.</li>
                    <li>Untuk kenyamanan dalam mengikuti assessment online, kami merekomendasikan menggunakan PC/Laptop dengan memori minimal 1GB dengan browser Google Chrome atau Mozilla Firefox yang diupdate dengan versi terbaru. <b>Test online tidak dapat diakses selain dengan kedua browser tersebut.</b></li>
                    
                    <li>Pahami dengan seksama setiap instruksi dari masing-masing test.</li>
                    <li>Untuk informasi dan pertanyaan, silahkan menghubungi recruitment.hckp@danamon.co.id</li>
                </ol><br/>
                <p>Kami tegaskan bahwa {{ $corporate_name }} <b>tidak bertanggung jawab</b> terhadap segala bentuk resiko yang disebabkan oleh kelalaian peserta dalam keterlambatan mengakses tes atau karena sebab lain yang mengganggu seperti kualitas jaringan internet, listrik, ataupun hal lain yang mengganggu proses terlaksananya assessment online.</p>
                
            </p>

            <h4><i>Selamat mengerjakan dan semoga sukses.</i></h4>
            <center>
                <div  style="width: 400px; padding: 10px; margin: 10px; background-color: #44a9ff; color: white;">
                    <a href="http://danamon.talentlytica.com/user-token?token={{$token}}">
                        <h2>Klik disini untuk memulai test <br/> dengan login secara otomatis</h2>
                    </a>
                </div>
            </center>
        </div>
    </div>

    <footer>
        <div style="padding-top:10px;text-align:center;padding-bottom:0;margin-bottom:0">
            <em style="font-size:smaller;">
            This is a notification-only email address. Please do not reply to this message.
            </em>
        </div>

        <div style="padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;margin-top:1%;border-top:3px solid #ee0000;">
           <div style="margin-right:-15px;margin-left:-15px;"></div>
        </div>

    </footer>

</body>
</html>
