<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <div style="margin-top:0%;width:100%;">

    </div>
    <div style="padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;margin-top:3%;border-top:3px solid #ee0000;">

        <div style="background:#fff;height:50px;float: left;">



        </div>
    </div>

    <div style="margin-right: -15px;margin-left: -15px;">
        <div style="width:100%;">
            <p>
                Dear Candidates, <br/>
                Dengan ini kami sampaikan adanya perubahan jadwal Online Assessment {{ $project }} yang dilaksanakan {{ $corporate_name }} menjadi :
            </p>
            <center><h4>{{ date('d-m-Y H:i:s', strtotime($start_date))  }} sampai {{ date('d-m-Y H:i:s', strtotime($end_date)) }}</h4></center>
            <p>
                Demikian pengumuman ini kami sampaikan, untuk dapat dimaklumi. <br/>
                Terima kasih.
            </p>
        </div>
    </div>

    <footer>
        <div style="padding-top:10px;text-align:center;padding-bottom:0;margin-bottom:0">
            <em style="font-size:smaller;">
            This is a notification-only email address. Please do not reply to this message.
            </em>
        </div>

        <div style="padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;margin-top:1%;border-top:3px solid #ee0000;">
           <div style="margin-right:-15px;margin-left:-15px;"></div>
        </div>

    </footer>

</body>
</html>
