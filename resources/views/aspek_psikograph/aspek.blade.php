@extends('layouts.default')
    @section('inline-css')
        <style type="text/css">
            .modal .modal-body {
                padding-top: 25px;
            }

            #standar-nilai td.mandatory.text-center {
                background-color: #ffe0b2 !important;
            }
            
        </style>
        {!! Html::style('assets/vendor/datatable-tpa/extensions/fixedcolumns/css/fixedColumns.bootstrap.min.css') !!}
    @stop
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Standar Nilai Aspek Psikograph</li>
            </ol>
        </div>
    @stop
    
    @section('content')
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Aspek Psikograph</a></li>
            <li><a data-toggle="tab" href="#menu1">Standar Nilai Psikograph</a></li>
        </ul>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title text-danger">
                                    <i class="fa fa-list"></i> List Aspek Psikograph
                                </div>
                            </div> 
                            <div class="panel-body full">
                                <table class="table table-hover no-footer table-bordered table-striped" id="list-aspek-psikograph">
                                    <thead>
                                        <tr class="headings">
                                            <th>Aspek Psikograph</th>
                                            <th>Kategori Aspek</th>
                                            <th>Gambaran Taraf Rendah</th>
                                            <th>Gambaran Taraf Tinggi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!is_null($listAspeks))
                                            @foreach($listAspeks as $row)
                                            <tr>
                                                <td>{{$row->nama_aspek}}</td>
                                                <td><b>{{$row->tpaKategoriAspek->nama}}</b></td>
                                                <td>{{$row->gambaran_taraf_rendah}}</td>
                                                <td>{{$row->gambaran_taraf_tinggi}}</td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">
                                    <div class="panel-title text-danger">
                                        <i class="fa fa-list"></i> Standar Nilai Psikograph
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-3 col-sm-3 col-xs-6 text-right">
                                    <a href="{{url('standar_nilai/tambah')}}" title="Tambah Standar Nilai"><button class="btn btn-success" id="btn-tambah">Tambah Baru</button></a>
                                </div>
                            </div> 
                            <div class="panel-body full">
                                <table class="table table-hover no-footer table-bordered table-striped" id="standar-nilai" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="headings">
                                            <th rowspan="2">Nama Jabatan</th>
                                            <th colspan="{{ sizeof(App\AspekPsikologis::where('kategori_aspek_id','=',1)->get()) }}" class="text-center">KEMAMPUAN INTELEKTUAL</th>
                                            <th colspan="{{ sizeof(App\AspekPsikologis::where('kategori_aspek_id','=',2)->get()) }}" class="text-center">SIKAP DAN CARA KERJA</th>
                                            <th colspan="{{ sizeof(App\AspekPsikologis::where('kategori_aspek_id','=',3)->get()) }}" class="text-center">KEPRIBADIAN</th>
                                        </tr>
                                        <tr>
                                            @foreach($listAspeks as $aspek)
                                                <th>{{ $aspek->nama_aspek }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $data)
                                        <tr>
                                            <td><a href="{{url('standar_nilai/'.$data->id.'/edit')}}"><i class="fa fa-tags" aria-hidden="true"></i> {{ $data->nama_jabatan}}</a></td>
                                            @foreach($listAspeks as $aspek)
                                                <td class="{{isMandatory($data->id,$aspek->id)}}">{{skorStandarNilai($data->id,$aspek->id)}}</td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        {!! Html::script('assets/vendor/datatable-tpa/extensions/responsive/js/dataTables.responsive.min.js') !!}
        {!! Html::script('assets/vendor/datatable-tpa/extensions/fixedcolumns/js/dataTables.fixedColumns.min.js') !!}
        <script type="text/javascript">

        $(document).ready(function() {
            var oTable = $('#standar-nilai').DataTable({
                "columnDefs": [
                { "width": "200px", "targets": 0 },
                { "className": "text-center", "targets": [1,2,3,4,5,6,7,8,9,10] }
                ],
                "order": [[ 1, 'asc' ]],
                "displayLength": 10,
                "bSort": false,
                "sScrollX": true,
                "sScrollY": false,
                "sScrollXInner": "150%",
                "bScrollCollapse": true,
                "fixedColumns":  {
                    leftColumns: 1
                },
                "colReorder": true
            });

            var table = $('#list-aspek-psikograph').DataTable({
                "columnDefs": [
                { "visible": false, "targets": 1 },
                { "width": "220px", "targets": 0 }
                ],
                "order": [[ 1, 'asc' ]],
                "displayLength": 10,
                "bSort": false,
                "drawCallback": function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;
                    
                    api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="3">'+group+'</td></tr>'
                            );
                            
                            last = group;
                        }
                    } );
                }
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                 .columns.adjust()
                 .fixedColumns().relayout();
            });

        });
        </script>
    @stop