@extends('layouts.print')
@section('inline-css')

{!! Html::style('assets/css/print.css') !!}
@stop
@section('content')
<section class="content clearfix">
  <div class="container-fluid">
    <div class="block-header">
      <h2>Laporan Work Personality Analytics</h2>
    </div>
    <div class="wpa-result">
      <div class="print-user-info">
        <div class="row">
          <div class="col-xs-7">
            <div class="wpa-result-info">
              <dl class="dl-horizontal">
                <dt>Nama</dt>
                <dd>{!! get_firstname_by_id($data['id_user']) !!}</dd>
                <dt>NIP</dt>
                <dd>-</dd>
                <dt>Periode</dt>
                <dd>{{ date('Y-m-d', strtotime($data['start_date'])) }} - {{ date('Y-m-d', strtotime($data['end_date'])) }}</dd>
                <dt>Tanggal Pengerjaan</dt>
                <dd>{{ $data['date'] }}</dd>
              </dl>
            </div>
          </div>
          <div class="col-xs-5">
            <div class="wpa-result-summary">
              <p><strong>Tipe Kepribadian Anda:</strong></p>
              <h3>{{ strtoupper($data['result']) }} <span class="label label-info">{{ strtoupper($data['type_kepribadian']) }}</span></h3>
            </div>
          </div>
        </div>
      </div>
      <div class="wpa-result-chart">
        <div class="row">
          <div class="col-xs-4">
            <h5 class="text-center"><strong>MOST</strong></h5>
            <div id="most-graph" style="height: 200px;"></div>
          </div>
          <div class="col-xs-4">
            <h5 class="text-center"><strong>LEAST</strong></h5>
            <div id="least-graph" style="height: 200px;"></div>
          </div>
          <div class="col-xs-4">
            <h5 class="text-center"><strong>CHANGE</strong></h5>
            <div id="change-graph" style="height: 200px;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="wpa-result-desc">
      <div class="row">
        <div class="col col-sm-12 result-summary">
          <h3>Uraian Kepribadian</h3>
          {!! $data['uraian_kepribadian'] !!}
        </div>
      </div>
      <div class="page-break"></div>
      <div class="row">
        <div class="col-xs-4">
          <div class="card-panel pt40">
            <h5>Karakteristik Umum</h5>
            <ul class="browser-default">
              <?php $i = 0; ?>
              @foreach($data['karakteristik_umum'] as $key)
              @if($i < 5)
              <li>{!! $key !!}</li>
              @endif
              <?php $i++; ?>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="card-panel pt40">
            <h5>Perilaku Kerja <small>(Kekuatan)</small></h5>
            <ul class="browser-default">
              <?php $i = 0; ?>
              @foreach($data['perilaku_kerja_kekuatan'] as $key)
              @if($i < 5 && $key != '')
              <li>{!! $key !!}</li>
              @endif
              <?php $i++; ?>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="card-panel pt40">
            <h5>Perilaku Kerja <small>(Kelemahan)</small></h5>
            <ul class="browser-default">
              <?php $i = 0; ?>
              @foreach($data['perilaku_kerja_kelemahan'] as $key)
              @if($i < 5 && $key != '')
              <li>{!! $key !!}</li>
              @endif
              <?php $i++; ?>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <div class="card-panel">
            <h5>Suasana Emosi <small>(Kekuatan)</small></h5>
            <ul class="browser-default">
              <?php $i = 0; ?>
              @foreach($data['suasana_emosi_kekuatan'] as $key)
              @if($i < 5 && $key != '')
              <li>{!! $key !!}</li>
              @endif
              <?php $i++; ?>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="card-panel">
            <h5>Suasana Emosi <small>(Kelemahan)</small></h5>
            <ul class="browser-default">
              <?php $i = 0; ?>
              @foreach($data['suasana_emosi_kelemahan'] as $key)
              @if($i < 5 && $key != '')
              <li>{!! $key !!}</li>
              @endif
              <?php $i++; ?>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="card-panel">
            <h5>Kekuatan</h5>
            <ul class="browser-default">
              <?php $i = 0; ?>
              @foreach($data['kekuatan'] as $key)
              @if($i < 5 && $key != '')
              <li>{!! $key !!}</li>
              @endif
              <?php $i++; ?>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <div class="card-panel">
            <h5>Kelemahan</h5>
            <ul class="browser-default">
              <?php $i = 0; ?>
              @foreach($data['kelemahan'] as $key)
              @if($i < 5 && $key != '')
              <li>{!! $key !!}</li>
              @endif
              <?php $i++; ?>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="col-xs-8">
          <div class="card-panel">
            <h5>Karakteristik Pekerjaan</h5>
            <ul class="browser-default">
              <?php $i = 0; ?>
              @foreach($data['karakteristik_pekerjaan'] as $key)
              @if($i < 9)
              <li>{!! $key !!}</li>
              @endif
              <?php $i++; ?>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop
@section("inline-script")
{!! Html::script('assets/vendor/canvasjs/canvasjs.min.js') !!}
{!! Html::script('assets/vendor/canvasjs/jquery.canvasjs.min.js') !!}
<script>
  $(function() {
    $("#most-graph").CanvasJSChart({
      axisY: {
        
        includeZero: false,
        minimum: -8,
        maximum: 8,
        valueFormatString: " ",
        tickLength: 0,
            stripLines:[
            {
                value:0,
                thickness:4
            }
            ],
            valueFormatString:"####"
      },
      axisX: {
        interval: 1,
        gridDashType: "shortDot",
        gridThickness: 1
      },
      data: [
      {
        type: "line", //try changing to column, area
        toolTipContent: "{label}: {y}",
        dataPoints: [
                { label: "D",  y: {{ $data['most_d'] }} },
                { label: "I",  y: {{ $data['most_i'] }} },
                { label: "S",y: {{ $data['most_s'] }}},
                { label: "C",y: {{ $data['most_c'] }} },
        ]
      }
      ]
    });
    
    $("#least-graph").CanvasJSChart({
      axisY: {
        
        includeZero: false,
        minimum: -8,
        maximum: 8,
        valueFormatString: " ",
        tickLength: 0,
            stripLines:[
            {
                value:0,
                thickness:4
            }
            ],
            valueFormatString:"####"
      },
      axisX: {
        interval: 1,
        gridDashType: "shortDot",
        gridThickness: 1
      },
      data: [
      {
        type: "line", //try changing to column, area
        toolTipContent: "{label}: {y}",
        dataPoints: [
                { label: "D",  y: {{ $data['lest_d'] }} },
                { label: "I",  y: {{ $data['lest_i'] }} },
                { label: "S",y: {{ $data['lest_s'] }}},
                { label: "C",y: {{ $data['lest_c'] }} },
        ]
      }
      ]
    });
    
    $("#change-graph").CanvasJSChart({
      axisY: {
        
        includeZero: false,
        minimum: -8,
        maximum: 8,
        valueFormatString: " ",
        tickLength: 0,
            stripLines:[
            {
                value:0,
                thickness:4
            }
            ],
            valueFormatString:"####"
      },
      axisX: {
        interval: 1,
        gridDashType: "shortDot",
        gridThickness: 1
      },
      data: [
      {
        type: "line", //try changing to column, area
        toolTipContent: "{label}: {y}",
        dataPoints: [
                { label: "D",  y: {{ $data['change_d'] }} },
                { label: "I",  y: {{ $data['change_i'] }} },
                { label: "S",y: {{ $data['change_s'] }}},
                { label: "C",y: {{ $data['change_c'] }} },
        ]
      }
      ]
    });
  });
  
</script>
@stop
