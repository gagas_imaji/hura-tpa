@extends('layouts.print')
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan Learning Agility Index</h2>
		</div>
		<div class="print-user-info">
			<div class="row">
				<div class="col-xs-6">
					<div class="wpa-result-info">
						<dl class="dl-horizontal">
							<dt>Nama</dt>
							<dd>{!! get_firstname_by_id($report['id_user']) !!}</dd>
							<dt>NIP</dt>
							<dd>-</dd>
							<dt>Periode</dt>
							<dd>{{ date('Y-m-d', strtotime($report['start_date'])) }} - {{ date('Y-m-d', strtotime($report['end_date'])) }}</dd>
							<dt>Tanggal Pengerjaan</dt>
							<dd>{{ $report['date'] }}</dd>
						</dl>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="wpa-result-summary">
						<p><strong>Hasil GTQ:</strong></p>
						<h3>{{ $report['avg'] }} <span class="label label-info">{{ $report['criteria'] }}</span></h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<strong>Speed &amp; Accuracy </strong>
						<div class="progress" style="height: 25px">
							<div class="progress-bar {{ gtiStatus($report['subtest1']) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent($report['subtest1']) }}; height: 25px">
								<span class="skill"><i class="val">{{ $report['subtest1'] }}</i></span>
							</div>
						</div>
						<strong>Reasoning</strong>
						<div class="progress" style="height: 25px">
							<div class="progress-bar {{ gtiStatus( $report['subtest2'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $report['subtest2'] ) }}; height: 25px">
								<span class="skill"><i class="val">{{ $report['subtest2'] }}</i></span>
							</div>
						</div>
						<strong> Focus &amp; Attention </strong>
						<div class="progress" style="height: 25px">
							<div class="progress-bar {{ gtiStatus( $report['subtest3'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $report['subtest3'] ) }}; height: 25px">
								<span class="skill"><i class="val">{{ $report['subtest3'] }}</i></span>
							</div>
						</div>
						<strong>Numerical Ability</strong>
						<div class="progress" style="height: 25px">
							<div class="progress-bar {{ gtiStatus( $report['subtest4'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $report['subtest4'] ) }}; height: 25px">
								<span class="skill"><i class="val">{{ $report['subtest4'] }}</i></span>
							</div>
						</div>
						<strong>Technical Problem Solving </strong>
						<div class="progress" style="height: 25px">
							<div class="progress-bar {{ gtiStatus( $report['subtest5'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $report['subtest5'] ) }}; height: 25px">
								<span class="skill"><i class="val">{{ $report['subtest5'] }}</i></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="panel panel-default">
					<div class="panel-heading">Job</div>
					<div class="panel-body">
						<ul>
							@foreach($report['jobs'] as $jobs)
							<li>{{ $jobs -> job }}</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Keterangan</div>
					<div class="panel-body">
						<span class="label gti-low">Low</span>
						<span class="label gti-mid">Mid</span>
						<span class="label gti-high">High</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div class="panel panel-success">
					<div class="panel-heading">Kekuatan</div>
					<div class="panel-body">
						<ul>
							@if($kekuatan)
							@foreach($kekuatan as $key)
							@if($key)
							<li>{{ $key }}</li>
							@endif
							@endforeach
							@endif
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="panel panel-danger">
					<div class="panel-heading">Kelemahan</div>
					<div class="panel-body">
						<ul>
							@if($kelemahan)
							@foreach($kelemahan as $key)
							@if($key)
							<li>{{ $key }}</li>
							@endif
							@endforeach
							@endif
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
