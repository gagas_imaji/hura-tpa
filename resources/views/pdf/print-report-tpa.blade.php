@extends('layouts.print')
@section('inline-css')
{!! Html::style('assets/vendor/datatables/datatables.min.css') !!}
{!! Html::style('assets/vendor/datatables/DataTables-1.10.12/css/dataTables.bootstrap.css') !!}
{!! Html::style('assets/vendor/datatables/Buttons-1.2.2/css/buttons.dataTables.min.css') !!}
{!! Html::style('assets/vendor/datatables/Responsive-2.1.0/css/responsive.dataTables.min.css') !!}
<link href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
{!! Html::style('assets/css/print-tpa.css') !!}
@endsection
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card must-break">
					<div class="header">
						<div class="row">
							<div class="col-xs-12">
								<h3>Hasil Peserta - Talentlytica Psikograph Analytic</h3>
							</div>
						</div>
					</div>
					<br>
					<div class="body">
						<div class="row">
							<div class="col-xs-8">
								<div class="user-report-info">
									<div class="media">
										<div class="media-left">
											<img class="media-object" src="{{ asset('assets/image/'.$data['avatar']) }}" alt="{{ $data["nama"] }}">
										</div>
										<div class="media-body">
											<h4 class="media-heading">{{ $data["nama"] }}</h4>
											<dl class="dl-horizontal">
												<dt class="text-left">Nama Project</dt><dd>{{ $data["projectName"]}}</dd>
												<dt>Email</dt><dd>{{ $data["email"] }}</dd>
												<dt>Jabatan Tujuan</dt>
												@if(is_null($current_jabatan))
												<dd>Belum ada jabatan yang dituju</dd>
												@else
												<dd>{{$current_jabatan}}</dd>
												@endif
											</dl>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-4">
								<div class="user-report-recommend">
									<div class="recomendation-box">
										<label>Hasil Rekomendasi</label>
									</div>
									<div class="recomendation-box">
										@if (!is_null($data["rekomendasi"]))
											@if ($data["rekomendasi"]->id == 1)
												<span class="label label-danger label-hasil">{{ $data["rekomendasi"]->nama }}</span>
											@elseif ($data["rekomendasi"]->id == 2)
												<span class="label label-warning label-hasil">{{ $data["rekomendasi"]->nama }}</span>
											@elseif ($data["rekomendasi"]->id == 3)
												<span class="label label-success label-hasil">{{ $data["rekomendasi"]->nama }}</span>
											@else
												<span class="label label-default label-hasil">Data belum tersedia</span>
											@endif
										@elseif (!is_null($data["hasilKriteria"]))
											@if ($data["hasilKriteria"] == 1)
												<span class="label label-danger label-hasil">{{ getHasilKriteria($data["hasilKriteria"]) }}</span>
											@elseif ($data["hasilKriteria"] == 2)
												<span class="label label-warning label-hasil">{{ getHasilKriteria($data["hasilKriteria"]) }}</span>
											@elseif ($data["hasilKriteria"] == 3)
												<span class="label label-success label-hasil">{{ getHasilKriteria($data["hasilKriteria"]) }}</span>
											@else
												<span class="label label-default label-hasil">Data belum tersedia</span>
											@endif
										@else
											<span class="label label-default label-hasil">Data belum tersedia</span>
										@endif
									</div>
									<hr>
									<div class="recomendation-box">
										<label>Persentase Kecocokan</label>
									</div>
									<div class="recomendation-box">
										@if ($data["kecocokan"] >= 75)
											<span class="label label-success label-hasil">{{ $data["kecocokan"] }}%</span>
										@elseif ($data["kecocokan"] >= 25)
											<span class="label label-warning label-hasil">{{ $data["kecocokan"] }}%</span>
										@elseif ($data["kecocokan"] > 0)
											<span class="label label-danger label-hasil">{{ $data["kecocokan"] }}%</span>
										@elseif (is_null($data["kecocokan"]))
											<span class="label label-default label-hasil">Data belum tersedia</span>
										@else
											<span class="label label-default label-hasil">Data belum tersedia</span>
										@endif
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<h2>Analisis</h2>
							</div>
							<div class="col-sm-6">
								@if (!is_null($data["analisis"]))
									{!! $data["analisis"] !!}
								@else
									<div class="alert alert-warning">
										Data uraian <strong>Sdr. Nama Peserta</strong> belum tersedia.
									</div>
								@endif
							</div>
							<br>
							<div class="col-sm-6">
								<div class="strength no-break">
									<h5>Kelebihan</h5>
									@if (sizeof($data["kelebihan"])==0)
										<div class="empty-data">Data Belum Tersedia</div>
									@else
										<ol>
											@for ($i = 0; $i < 5; $i++)
											    <li>{{ $data["kelebihan"][$i] }}</li>
											@endfor
										</ol>
									@endif
								</div>
								<div class="weakness no-break">
									<h5>Kelemahan</h5>
									@if (sizeof($data["kelemahan"])==0)
										<div class="empty-data">Data Belum Tersedia</div>
									@else
										<ol>
											@for ($i = 0; $i < 5; $i++)
											    <li>{{ $data["kelemahan"][$i] }}</li>
											@endfor
										</ol>
									@endif
								</div>
								<div class="jobrec no-break">
									<h5>Karakteristik Pekerjaan yang Sesuai</h5>
									@if (sizeof($data["karakteristik"])==0)
										<div class="empty-data">Data Belum Tersedia</div>
									@else
										<ol>
											@for ($i = 0; $i < 6; $i++)
											    <li>{{ $data["karakteristik"][$i] }}</li>
											@endfor
										</ol>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card must-break">
					<div class="body">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel-heading">
									<h3 class="panel-title text-complete"><i class="fa fa-info"></i> Hasil Pemeriksaan Psikologis</h3>
								</div>
								<div class="panel-body">
									<table id="table_report" class="table table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th rowspan="2">Aspek Psikologis</th>
												<th rowspan="2">Kategori Psikologis</th>
												<th rowspan="2">Gambaran individu pada "taraf" Rendah</th>
												<th colspan="5" style="text-align: center">Taraf</th>
												<th rowspan="2">Gambaran individu pada "taraf" Tinggi</th>
											</tr>
											<tr>
												@for ($t = 1; $t <= 5; $t++)
													<th>{{ $t }}</th>
												@endfor
											</tr>
										</thead>
										<tbody>
											@foreach ($aspeks as $aspek)
												<tr>
													<td>{{ $aspek->nama_aspek }}</td>
													<td>{{ $aspek->tpaKategoriAspek->nama }}</td>
													<td style="font-size:11px">{{ $aspek->gambaran_taraf_rendah }}</td>
													@for ($tr = 1; $tr <= 5; $tr++)
														<td class="{{ get_class_standar_nilai($aspek->id, $data['posisiId'], $tr) }}">
															@if (count($data["nilais"]) > 0)
																{!! ($tr == ($data["nilais"][$aspek->id]-1) ? '<i class="fa fa-check"></i>' : '') !!}
															@endif
														</td>
													@endfor
													<td style="font-size:11px">{{ $aspek->gambaran_taraf_tinggi }}</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
@section('inline-script')
	<script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    {!! Html::script('assets/vendor/datatable-tpa/extensions/responsive/js/dataTables.responsive.min.js') !!}
	{!! Html::script('assets/vendor/datatable-tpa/extensions/fixedcolumns/js/dataTables.fixedColumns.min.js') !!}
	<script>
		$(document).ready(function() {
			$("#table_report").DataTable({
					"bSort" : false,
					"bPaginate": false,
					"info": false,
					"searching": false,
					"columnDefs": 	[
						{ "visible": false, "targets": 1 },
					],
					"order": [[ 1, 'asc' ]],
					"drawCallback": function ( settings ) {
			            var api = this.api();
			            var rows = api.rows( {page:'current'} ).nodes();
			            var last=null;

			            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
			                if ( last !== group ) {
			                    $(rows).eq( i ).before(
			                        '<tr class="group"><td colspan="10">'+group+'</td></tr>'
			                    );

			                    last = group;
			                }
			            } );
			        }
			});
		});
	</script>
@endsection