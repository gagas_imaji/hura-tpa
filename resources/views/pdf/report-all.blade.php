@extends('layouts.print')

@section('inline-css')
	{!! Html::style('assets/vendor/datatables/datatables.min.css') !!}
	{!! Html::style('assets/vendor/datatables/DataTables-1.10.12/css/dataTables.bootstrap.css') !!}
	{!! Html::style('assets/vendor/datatables/Buttons-1.2.2/css/buttons.dataTables.min.css') !!}
	{!! Html::style('assets/vendor/datatables/Responsive-2.1.0/css/responsive.dataTables.min.css') !!}
	<link href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	{!! Html::style('assets/css/print.css') !!}
	{!! Html::style('assets/css/print-tpa.css') !!}
@stop

@section('content')
	<section class="content clearfix">
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card must-break">
					<div class="cover-logo-client">
						<img src="{{ asset('assets/image/logo-danamon.png') }}" class="logo-client">
					</div>
					<div class="header cover-pdf">
						<div class="title-pdf">
							<h3>Assessment Report</h3>
						</div>
					</div>
					<div class="info-pdf">
						<div class="media">
							<div class="media-left">
								<img class="media-object" src="{{ asset('assets/image/'.$data['tpa']['avatar']) }}" alt="{{ $data['tpa']["nama"] }}">
							</div>
							<div class="media-body">
								<h4 class="media-heading">{{ $data['tpa']["nama"] }}</h4>
								<strong>Nama Project</strong>
								<p>{{ $data['tpa']["projectName"]}}</p>
								<strong>Email</strong>
								<p>{{ $data['tpa']["email"] }}</p>
								<strong>Jabatan Tujuan</strong>
								@if(is_null($data['tpa']['current_jabatan']))
									<p>Belum ada jabatan yang dituju</p>
								@else
									<p>{{$data['tpa']['current_jabatan']}}</p>
								@endif
							</div>
						</div>
					</div>
					<div class="cover-logo-talentlytica">
						<label class="provided">Provided by: </label>
						<img src="{{ asset('assets/image/logo-pdf.png') }}">
					</div>
				</div>

			</div>
		</div>
		@if($data['tpa'] != null)
			<div class="container-fluid">
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="card must-break">
							<div class="header header-report-tpa">
								<div class="row">
									<div class="col-xs-12">
										<h3>Laporan Talentlytica Psychograph Analytics</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="body body-pdf">
								<div class="row">
									<div class="col-xs-8">
										<div class="user-report-info">
											<div class="media">
												<div class="media-left">
													<img class="media-object" src="{{ asset('assets/image/'.$data['tpa']['avatar']) }}" alt="{{ $data['tpa']["nama"] }}">
												</div>
												<div class="media-body">
													<h4 class="media-heading">{{ $data['tpa']["nama"] }}</h4>
													<strong>Nama Project</strong>
													<p>{{ $data['tpa']["projectName"]}}</p>
													<strong>Email</strong>
													<p>{{ $data['tpa']["email"] }}</p>
													<strong>Jabatan Tujuan</strong>
													@if(is_null($data['tpa']['current_jabatan']))
														<p>Belum ada jabatan yang dituju</p>
													@else
														<p>{{$data['tpa']['current_jabatan']}}</p>
													@endif
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="user-report-recommend">
											<div class="recomendation-box">
												@if (!is_null($data['tpa']["rekomendasi"]))
													@if ($data['tpa']["rekomendasi"]->id == 1)
														<h3 style="color: #cd1e43;font-weight: bold;">{{ $data['tpa']["rekomendasi"]->nama }}</h3>
													@elseif ($data['tpa']["rekomendasi"]->id == 2)
														<h3 style="color: #f5a63e;font-weight: bold; ">{{ $data['tpa']["rekomendasi"]->nama }}</h3>
													@elseif ($data['tpa']["rekomendasi"]->id == 3)
														<h3 style="color: #69be44;font-weight: bold;">{{ $data['tpa']["rekomendasi"]->nama }}</h3>
													@else
														<h3 style="color: #7c8482;font-weight: bold;">Data belum tersedia</h3>
													@endif
												@elseif (!is_null($data['tpa']["hasilKriteria"]))
													@if ($data['tpa']["hasilKriteria"] == 1)
														<h3 style="color: #cd1e43;font-weight: bold;">{{ getHasilKriteria($data['tpa']["hasilKriteria"]) }}</h3>
													@elseif ($data['tpa']["hasilKriteria"] == 2)
														<h3 style="color: #f5a63e; font-weight: bold;">{{ getHasilKriteria($data['tpa']["hasilKriteria"]) }}</h3>
													@elseif ($data['tpa']["hasilKriteria"] == 3)
														<h3 style="color: #69be44;font-weight: bold;">{{ getHasilKriteria($data['tpa']["hasilKriteria"]) }}</h3>
													@else
														<h3 style="color: #7c8482;font-weight: bold;">Data belum tersedia</h3>
													@endif
												@else
													<h3 style="color: #7c8482;">Data belum tersedia</h3>
												@endif
											</div>
											<div class="recomendation-box">
												<strong>Hasil Rekomendasi</strong>
											</div>
											<div class="recomendation-box">
												@if ($data['tpa']["kecocokan"] >= 75)
													<h3 style="color: #69be44;font-weight: bold;">{{ $data['tpa']["kecocokan"] }}%</h3>
												@elseif ($data['tpa']["kecocokan"] >= 25)
													<h3 style="color: #f5a63e; font-weight: bold;">{{ $data['tpa']["kecocokan"] }}%</h3>
												@elseif ($data['tpa']["kecocokan"] > 0)
													<h3 style="color: #cd1e43;font-weight: bold;">{{ $data['tpa']["kecocokan"] }}%</h3>
												@elseif (is_null($data['tpa']["kecocokan"]))
													<h3 style="color: #7c8482;font-weight: bold;">Data belum tersedia</h3>
												@else
													<h3 style="color: #7c8482;font-weight: bold;">Data belum tersedia</h3>
												@endif
											</div>
											<div class="recomendation-box">
												<label>Persentase Kecocokan</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<h2>Uraian Kepribadian</h2>
									</div>
									<div class="col-sm-6">
										@if (!is_null($data['tpa']["analisis"]))
											{!! $data['tpa']["analisis"] !!}
										@else
											<div class="alert alert-warning">
												Data uraian <strong>Sdr. Nama Peserta</strong> belum tersedia.
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<div class="card must-break">
							<div class="header header-report-tpa">
								<div class="row">
									<div class="col-xs-12">
										<h3>Laporan Talentlytica Psychograph Analytics</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="body body-pdf">
								<div class="row">
									<div class="col-sm-6">
										<div class="strength no-break">
											<h5><strong>Kelebihan</strong></h5>
											@if (sizeof($data['tpa']["kelebihan"])==0)
												<div class="empty-data">Data Belum Tersedia</div>
											@else
												<ol>
													@for ($i = 0; $i < 5; $i++)
													    <li>{{ $data['tpa']["kelebihan"][$i] }}</li>
													@endfor
												</ol>
											@endif
										</div>
										<div class="weakness no-break">
											<h5><strong>Kelemahan</strong></h5>
											@if (sizeof($data['tpa']["kelemahan"])==0)
												<div class="empty-data">Data Belum Tersedia</div>
											@else
												<ol>
													@for ($i = 0; $i < 5; $i++)
													    <li>{{ $data['tpa']["kelemahan"][$i] }}</li>
													@endfor
												</ol>
											@endif
										</div>
										<div class="jobrec no-break">
											<h5><strong>Karakteristik Pekerjaan yang Sesuai</strong></h5>
											@if (sizeof($data['tpa']["karakteristik"])==0)
												<div class="empty-data">Data Belum Tersedia</div>
											@else
												<ol>
													@for ($i = 0; $i < 6; $i++)
													    <li>{{ $data['tpa']["karakteristik"][$i] }}</li>
													@endfor
												</ol>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card must-break">
							<div class="header header-report-tpa">
								<div class="row">
									<div class="col-xs-12">
										<h3>Laporan Talentlytica Psychograph Analytics</h3>
									</div>
								</div>
							</div>
							<div class="body body-pdf">
								<div class="row">
									<div class="col-xs-12">
										<div class="panel-heading">
											<h3 class="panel-title text-complete"><i class="fa fa-info"></i> Hasil Pemeriksaan Psikologis</h3>
										</div>
										<div class="panel-body">
											<table id="table_report" class="table table-bordered" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th rowspan="2">Aspek Psikologis</th>
														<th rowspan="2">Kategori Psikologis</th>
														<th rowspan="2">Gambaran individu pada "taraf" Rendah</th>
														<th colspan="5" style="text-align: center">Taraf</th>
														<th rowspan="2">Gambaran individu pada "taraf" Tinggi</th>
													</tr>
													<tr>
														@for ($t = 1; $t <= 5; $t++)
															<th>{{ $t }}</th>
														@endfor
													</tr>
												</thead>
												<tbody>
													@foreach ($data['tpa']['aspeks'] as $aspek)
														<tr>
															<td>{{ $aspek->nama_aspek }}</td>
															<td>{{ $aspek->tpaKategoriAspek->nama }}</td>
															<td style="font-size:11px">{{ $aspek->gambaran_taraf_rendah }}</td>
															@for ($tr = 1; $tr <= 5; $tr++)
																<td class="{{ get_class_standar_nilai($aspek->id, $data['tpa']['posisiId'], $tr) }}">
																	@if (count($data['tpa']["nilais"]) > 0)
																		{!! ($tr == ($data['tpa']["nilais"][$aspek->id]-1) ? '<i class="fa fa-check"></i>' : '') !!}
																	@endif
																</td>
															@endfor
															<td style="font-size:11px">{{ $aspek->gambaran_taraf_tinggi }}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif

		@if($data['lai'] != null)
		    <div class="container-fluid must-break">
		    	<div class="row clearfix">
		    		<div class="col-sm-12">
		    			<div class="card must-break">
		    				<div class="header header-report-lai">
								<div class="row">
									<div class="col-xs-12">
										<h3>Laporan Learning Agility Index</h3>
									</div>
								</div>
							</div>
							<br/>
							<div class="body body-pdf">
								<div class="row">
									<div class="col-xs-8">
										<div class="user-report-info">
											<div class="media">
												<div class="media-left">
													<img class="media-object" src="{{ asset('assets/image/'.$data['tpa']['avatar']) }}" alt="{{ $data['tpa']["nama"] }}">
												</div>
												<div class="media-body">
													<h4 class="media-heading">{{ $data['tpa']["nama"] }}</h4>
													<strong>Nama Project</strong>
													<p>{{ $data['tpa']["projectName"]}}</p>
													<strong>Email</strong>
													<p>{{ $data['tpa']["email"] }}</p>
													<strong>Tanggal Pengerjaan</strong>
													<p>{{ $data['lai']['date'] }}</p>
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-4">
										<div class="user-report-recommend">
											
											<div class="recomendation-box">
												<strong>Hasil GTQ</strong>
											</div>
											<div class="recomendation-box">
												<h3 style="font-weight: bold;">{{ $data['lai']['avg'] }} &nbsp;
												<span class="label label-info">{{ $data['lai']['criteria'] }}</span></h3>
											</div>
										</div>
									</div>
								</div>
								<br/><br/>
								<div class="row">
									<div class="col-xs-6">
										<div class="panel panel-default">
											<div class="panel-body">
												<strong>Technical Problem Solving </strong>
												<div class="progress" style="height: 25px">
													<div class="progress-bar {{ gtiStatus($data['lai']['subtest1']) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent($data['lai']['subtest1']) }}; height: 25px">
														<span class="skill"><i class="val">{{ $data['lai']['subtest1'] }}</i></span>
													</div>
												</div>
												<strong>Numerical Ability</strong>
												<div class="progress" style="height: 25px">
													<div class="progress-bar {{ gtiStatus( $data['lai']['subtest2'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $data['lai']['subtest2'] ) }}; height: 25px">
														<span class="skill"><i class="val">{{ $data['lai']['subtest2'] }}</i></span>
													</div>
												</div>
												<strong> Focus &amp; Attention </strong>
												<div class="progress" style="height: 25px">
													<div class="progress-bar {{ gtiStatus( $data['lai']['subtest3'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $data['lai']['subtest3'] ) }}; height: 25px">
														<span class="skill"><i class="val">{{ $data['lai']['subtest3'] }}</i></span>
													</div>
												</div>
												<strong>Reasoning</strong>
												<div class="progress" style="height: 25px">
													<div class="progress-bar {{ gtiStatus( $data['lai']['subtest4'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $data['lai']['subtest4'] ) }}; height: 25px">
														<span class="skill"><i class="val">{{ $data['lai']['subtest4'] }}</i></span>
													</div>
												</div>
												<strong>Speed &amp; Accuracy </strong>
												<div class="progress" style="height: 25px">
													<div class="progress-bar {{ gtiStatus( $data['lai']['subtest5'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $data['lai']['subtest5'] ) }}; height: 25px">
														<span class="skill"><i class="val">{{ $data['lai']['subtest5'] }}</i></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-6">
										<div class="panel panel-default">
											<div class="panel-heading">Posisi</div>
											<div class="panel-body">
												<ul>
													@foreach($data['lai']['jobs'] as $jobs)
													<li>{{ $jobs -> job }}</li>
													@endforeach
												</ul>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">Keterangan</div>
											<div class="panel-body">
												<span class="label gti-low">Low</span>
												<span class="label gti-mid">Mid</span>
												<span class="label gti-high">High</span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-6">
										<div class="panel panel-success">
											<div class="panel-heading">Kekuatan</div>
											<div class="panel-body">
												<ul>
													@if($data['lai']['kekuatan'])
														@foreach($data['lai']['kekuatan'] as $key)
															@if($key)
																<li>{{ $key }}</li>
															@endif
														@endforeach
													@else
														<p>Data tidak tersedia</p>
													@endif
												</ul>
											</div>
										</div>
									</div>
									<div class="col-xs-6">
										<div class="panel panel-danger">
											<div class="panel-heading">Kelemahan</div>
											<div class="panel-body">
												<ul>
													@if($data['lai']['kelemahan'])
														@foreach($data['lai']['kelemahan'] as $key)
															@if($key)
																<li>{{ $key }}</li>
															@endif
														@endforeach
													@else
														<p>Data tidak tersedia</p>
													@endif
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
		    			</div>
		    		</div>
		    	</div>
				<br/><br/>
				
				
			</div>
		@endif

		@if($data['tiki'] != null)
			<div class="container-fluid must-break">
				<div class="row clearfix">
		    		<div class="col-sm-12">
		    			<div class="card must-break">
		    				<div class="header header-report-tiki">
								<div class="row">
									<div class="col-xs-12">
										<h3>Laporan Test Intelegensi Kolektif Indonesia</h3>
									</div>
								</div>
							</div>
							<br/>
							<div class="body body-pdf">
								<div class="row">
									<div class="col-xs-8">
										<div class="user-report-info">
											<div class="media">
												<div class="media-left">
													<img class="media-object" src="{{ asset('assets/image/'.$data['tpa']['avatar']) }}" alt="{{ $data['tpa']["nama"] }}">
												</div>
												<div class="media-body">
													<h4 class="media-heading">{{ $data['tpa']["nama"] }}</h4>
													<strong>Nama Project</strong>
													<p>{{ $data['tpa']["projectName"]}}</p>
													<strong>Email</strong>
													<p>{{ $data['tpa']["email"] }}</p>
													<strong>Tanggal Pengerjaan</strong>
													<p>{{ $data['tiki']['date'] }}</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="user-report-recommend">
											
											<div class="recomendation-box">
												<strong>Taraf Kecerdasan (IQ)</strong>
											</div>
											<div class="recomendation-box">
												<h3 style="font-weight: bold;">{{ $data['tiki']['iq'] }} &nbsp;
												<span class="label label-info">{{ $data['tiki']['golongan_iq'] }}</span></h3>
											</div>
										</div>
									</div>
								</div>
								<br/><br/>
								<div class="tiki-summary">
									<div class="row tiki-summary-header">
										<div class="col-xs-5">
											<strong>Kriteria</strong>
										</div>
										<div class="col-xs-2">
											<strong>Nilai Standar</strong>
										</div>
										<div class="col-xs-5">
											&nbsp;
										</div>
									</div>
									<div class="row">
										<div class="col-xs-5">
											<h5>Berhitung Angka</h5>
											<p>Kecepatan dan ketepatan berhitung</p>
										</div>
										<div class="col-xs-2">
											<h4 class="text-center">{!! $data['tiki']['nilai_standar'][0] !!}</h4>
										</div>
										<div class="col-xs-5">
											<div class="progress" style="height: 25px">
												<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="28" style="width: {{ tikiPercent($data['tiki']['nilai_standar'][0], 28) }}; height: 25px">
													<span class="skill"><i class="val">{!! $data['tiki']['nilai_standar'][0] !!}</i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-5">
											<h5>Gabungan Bagian</h5>
											<p>Daya Konsentrasi (Sintesis dan Analisis)</p>
										</div>
										<div class="col-xs-2">
											<h4 class="text-center">{!! $data['tiki']['nilai_standar'][1] !!}</h4>
										</div>
										<div class="col-xs-5">
											<div class="progress" style="height: 25px">
												<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['tiki']['nilai_standar'][1], 30) }}; height: 25px">
													<span class="skill"><i class="val">{!! $data['tiki']['nilai_standar'][1] !!}</i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-5">
											<h5>Hubungan Kata</h5>
											<p>Penilaian realitas, berpikir, praktis-konkrit</p>
										</div>
										<div class="col-xs-2">
											<h4 class="text-center">{!! $data['tiki']['nilai_standar'][2] !!}</h4>
										</div>
										<div class="col-xs-5">
											<div class="progress" style="height: 25px">
												<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['tiki']['nilai_standar'][2], 30) }}; height: 25px">
													<span class="skill"><i class="val">{!! $data['tiki']['nilai_standar'][2] !!}</i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-5">
											<h5>Abstraksi Non Verbal</h5>
											<p>Kemampuan abstraksi, daya klasifikasi</p>
										</div>
										<div class="col-xs-2">
											<h4 class="text-center">{!! $data['tiki']['nilai_standar'][3] !!}</h4>
										</div>
										<div class="col-xs-5">
											<div class="progress" style="height: 25px">
												<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['tiki']['nilai_standar'][3], 30) }}; height: 25px">
													<span class="skill"><i class="val">{!! $data['tiki']['nilai_standar'][3] !!}</i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row tiki-summary-footer">
										<div class="col-xs-5">
											<h4>Jumlah Angka TiQi</h4>
										</div>
										<div class="col-xs-2">
											<h4 class="text-center"><strong>{!! $data['tiki']['total_tiki'] !!}</strong></h4>
										</div>
										
									</div>
								</div>
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-8">
											<h3>Kesimpulan</h3>
											<table class="table table-bordered">
												<tr>
													<td>1</td>
													<td>Space and Non-Verbal Reasoning</td>
													<td>F1 : <strong>{!! $data['tiki']['f1'] !!} {!! $data['tiki']['golongan_f1'] !!} </strong></td>
												</tr>
												<tr>
													<td>2</td>
													<td>General Scholastic Aptitude</td>
													<td>F2 : <strong>{!! $data['tiki']['f2'] !!} {!! $data['tiki']['golongan_f2'] !!} </strong></td>
												</tr>
												<tr>
													<td>3</td>
													<td>Speed And Accuracy</td>
													<td>F3 : <strong>{!! $data['tiki']['f3'] !!} {!! $data['tiki']['golongan_f3'] !!} </strong></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
		    			</div>
		    		</div>
		    	</div>
			</div>
		@endif

		@if($data['wpa'] != null)
			<div class="container-fluid must-break">
				<div class="row clearfix">
		    		<div class="col-sm-12">
		    			<div class="card must-break">
		    				<div class="header header-report-wpa">
								<div class="row">
									<div class="col-xs-12">
										<h3>Laporan Work Personality Analytics</h3>
									</div>
								</div>
							</div>
							<br/>
							<div class="body body-pdf">
								<div class="row">
									<div class="col-xs-8">
										<div class="user-report-info">
											<div class="media">
												<div class="media-left">
													<img class="media-object" src="{{ asset('assets/image/'.$data['tpa']['avatar']) }}" alt="{{ $data['tpa']["nama"] }}">
												</div>
												<div class="media-body">
													<h4 class="media-heading">{{ $data['tpa']["nama"] }}</h4>
													<strong>Nama Project</strong>
													<p>{{ $data['tpa']["projectName"]}}</p>
													<strong>Email</strong>
													<p>{{ $data['tpa']["email"] }}</p>
													<strong>Tanggal Pengerjaan</strong>
													<p>{{ $data['wpa']['date'] }}</p>
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-4">
										<div class="user-report-recommend">
											
											<div class="recomendation-box">
												<strong>Tipe Kepribadian</strong>
											</div>
											<div class="recomendation-box">
												<h3 style="font-weight: bold;">{{ strtoupper($data['wpa']['result']) }} &nbsp;
												<span class="label label-info">{{ strtoupper($data['wpa']['type_kepribadian']) }}</span></h3>
											</div>
										</div>
									</div>
								</div>
								<br/><br/>
							    <div class="wpa-result">
							      	<div class="wpa-result-chart">
								        <div class="row">
									        <div class="col-xs-4">
									            <h5 class="text-center"><strong>MOST</strong></h5>
									            <div id="most-graph" style="height: 200px;"></div>
									        </div>
									        <div class="col-xs-4">
									            <h5 class="text-center"><strong>LEAST</strong></h5>
									            <div id="least-graph" style="height: 200px;"></div>
									        </div>
									        <div class="col-xs-4">
									            <h5 class="text-center"><strong>CHANGE</strong></h5>
									            <div id="change-graph" style="height: 200px;"></div>
									        </div>
								        </div>
							      	</div>
							    </div>
							    <div class="wpa-result-desc">
							      	<div class="row">
								        <div class="col col-sm-12 result-summary">
								          	<h3>Uraian Kepribadian</h3>
								          	{!! $data['wpa']['uraian_kepribadian'] !!}
								        </div>
							      	</div>
							      	<div class="page-break"></div>
							      	<div class="row">
								        <div class="col-xs-4">
								          	<div class="card-panel pt40">
									            <h5 style="background-color: #31b968; color: white; ">Karakteristik Umum</h5>
									            <ul class="browser-default">
									              	<?php $i = 0; ?>
									              	@foreach($data['wpa']['karakteristik_umum'] as $key)
										              	@if($i < 5)
										              		<li>{!! $key !!}</li>
										              	@endif
										              	<?php $i++; ?>
									              	@endforeach
									            </ul>
								          	</div>
								        </div>
								        <div class="col-xs-4">
								          	<div class="card-panel pt40">
									            <h5 style="background-color: #31b968; color: white; ">Perilaku Kerja <small style="color: white;">(Kekuatan)</small></h5>
									            <ul class="browser-default">
									              	<?php $i = 0; ?>
									              	@foreach($data['wpa']['perilaku_kerja_kekuatan'] as $key)
										              	@if($i < 5 && $key != '')
										              		<li>{!! $key !!}</li>
										              	@endif
										              	<?php $i++; ?>
									              	@endforeach
									            </ul>
								          	</div>
								        </div>
								        <div class="col-xs-4">
								          	<div class="card-panel pt40">
									            <h5 style="background-color: #31b968; color: white; ">Perilaku Kerja <small style="color: white;">(Kelemahan)</small></h5>
									            <ul class="browser-default">
									              	<?php $i = 0; ?>
									              	@foreach($data['wpa']['perilaku_kerja_kelemahan'] as $key)
										              	@if($i < 5 && $key != '')
										              		<li>{!! $key !!}</li>
										              	@endif
										              	<?php $i++; ?>
									              	@endforeach
									            </ul>
								          	</div>
								        </div>
							      	</div>
							      	<div class="row">
							        <div class="col-xs-4">
							          	<div class="card-panel">
								            <h5 style="background-color: #31b968; color: white;">Suasana Emosi <small style="color: white;">(Kekuatan)</small></h5>
								            <ul class="browser-default">
								              	<?php $i = 0; ?>
								              	@foreach($data['wpa']['suasana_emosi_kekuatan'] as $key)
									              	@if($i < 5 && $key != '')
									              		<li>{!! $key !!}</li>
									              	@endif
									              	<?php $i++; ?>
								              	@endforeach
								            </ul>
							          	</div>
							        </div>
							        <div class="col-xs-4">
							          	<div class="card-panel">
							            <h5 style="background-color: #31b968; color: white;">Suasana Emosi <small style="color: white;">(Kelemahan)</small></h5>
								            <ul class="browser-default">
								              	<?php $i = 0; ?>
								              	@foreach($data['wpa']['suasana_emosi_kelemahan'] as $key)
									              	@if($i < 5 && $key != '')
									              		<li>{!! $key !!}</li>
									              	@endif
									              	<?php $i++; ?>
								              	@endforeach
								            </ul>
							          	</div>
							        </div>
							        <div class="col-xs-4">
							          	<div class="card-panel">
							            <h5 style="background-color: #31b968; color: white;">Kekuatan</h5>
								            <ul class="browser-default">
								              	<?php $i = 0; ?>
								              	@foreach($data['wpa']['kekuatan'] as $key)
									              	@if($i < 5 && $key != '')
									              		<li>{!! $key !!}</li>
									              	@endif
									              	<?php $i++; ?>
								              	@endforeach
								            </ul>
							          	</div>
							        </div>
							      </div>
							      <div class="row">
							        <div class="col-xs-4">
							          	<div class="card-panel">
							            <h5 style="background-color: #31b968; color: white;">Kelemahan</h5>
								            <ul class="browser-default">
								              	<?php $i = 0; ?>
								              	@foreach($data['wpa']['kelemahan'] as $key)
									              	@if($i < 5 && $key != '')
									              		<li>{!! $key !!}</li>
									              	@endif
									              	<?php $i++; ?>
								              	@endforeach
								            </ul>
							          	</div>
							        </div>
							        <div class="col-xs-8">
							          	<div class="card-panel">
							            <h5 style="background-color: #31b968; color: white;">Karakteristik Pekerjaan</h5>
								            <ul class="browser-default">
								              	<?php $i = 0; ?>
								              	@foreach($data['wpa']['karakteristik_pekerjaan'] as $key)
									              	@if($i < 9)
									              		<li>{!! $key !!}</li>
									              	@endif
									              	<?php $i++; ?>
								              	@endforeach
								            </ul>
							          	</div>
							        </div>
							      </div>
							    </div>
							</div>
		    			</div>
		    		</div>
		    	</div>
		  	</div>
	  	@endif

	  	@if($data['wba'] != null)
	  		<input type="hidden" name="wba" id="wba" value="1">
		  	<div class="container-flui must-break">
		  		<div class="row clearfix">
		    		<div class="col-sm-12">
		    			<div class="card must-break">
		    				<div class="header header-report-wba">
								<div class="row">
									<div class="col-xs-12">
										<h3>Laporan Work Behavioural Assessment</h3>
									</div>
								</div>
							</div>
							<br/>
							<div class="body body-pdf">
								<div class="row">
									<div class="col-xs-8">
										<div class="user-report-info">
											<div class="media">
												<div class="media-left">
													<img class="media-object" src="{{ asset('assets/image/'.$data['tpa']['avatar']) }}" alt="{{ $data['tpa']["nama"] }}">
												</div>
												<div class="media-body">
													<h4 class="media-heading">{{ $data['tpa']["nama"] }}</h4>
													<strong>Nama Project</strong>
													<p>{{ $data['tpa']["projectName"]}}</p>
													<strong>Email</strong>
													<p>{{ $data['tpa']["email"] }}</p>
													<strong>Tanggal Pengerjaan</strong>
													<p>{{ $data['wba']['date'] }}</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<br/><br/>
								<div class="row">
									<div class="col-xs-12">
										<table class="table table-bordered text-center">
											<thead>
												<tr>
													<th style="background-color: #c3c3c1;">N</th>
													<th style="background-color: #c3c3c1;">G</th>
													<th style="background-color: #c3c3c1;">A</th>
													<th style="background-color: #5a5a5a;color: #fff;">L</th>
													<th style="background-color: #5a5a5a;color: #fff;">P</th>
													<th style="background-color: #5a5a5a;color: #fff;">I</th>
													<th style="background-color: #f8dff9;">T</th>
													<th style="background-color: #f8dff9;">V</th>
													<th style="background-color: #fee426;">X</th>
													<th style="background-color: #fee426;">S</th>
													<th style="background-color: #fee426;">B</th>
													<th style="background-color: #fee426;">O</th>
													<th style="background-color: #a8c3f3;">R</th>
													<th style="background-color: #a8c3f3;">D</th>
													<th style="background-color: #a8c3f3;">C</th>
													<th style="background-color: #de0c26;color: #fff;">Z</th>
													<th style="background-color: #de0c26;color: #fff;">E</th>
													<th style="background-color: #de0c26;color: #fff;">K</th>
													<th style="background-color: #9ede0c;">F</th>
													<th style="background-color: #9ede0c;">W</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>{{ $data['wba']['N']}}</td>
													<td>{{ $data['wba']['G']}}</td>
													<td>{{ $data['wba']['A']}}</td>
													<td>{{ $data['wba']['L']}}</td>
													<td>{{ $data['wba']['P']}}</td>
													<td>{{ $data['wba']['I']}}</td>
													<td>{{ $data['wba']['T']}}</td>
													<td>{{ $data['wba']['V']}}</td>
													<td>{{ $data['wba']['X']}}</td>
													<td>{{ $data['wba']['S']}}</td>
													<td>{{ $data['wba']['B']}}</td>
													<td>{{ $data['wba']['O']}}</td>
													<td>{{ $data['wba']['R']}}</td>
													<td>{{ $data['wba']['D']}}</td>
													<td>{{ $data['wba']['C']}}</td>
													<td>{{ $data['wba']['Z']}}</td>
													<td>{{ $data['wba']['E']}}</td>
													<td>{{ $data['wba']['K']}}</td>
													<td>{{ $data['wba']['F']}}</td>
													<td>{{ $data['wba']['W']}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
						        <div class="row">
						            <div class="col-xs-12" >
						                <div class="papi-chart-container">
						                    <div class="papi-chart">
						                        <canvas id="myChart" width="250" height="250"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
		    			</div>
		    		</div>
		    	</div>
				<div class="page-break"></div>
				<div class="row pt40 body-pdf">
					<div class="col-xs-12">
						<table class="table table-bordered wba-desc">
							<tbody>
								<tr>
									<th style="background-color:  #c3c3c1; text-align: center;" colspan="2">Arah Kerja</th>
								</tr>
								@foreach($data['wba']['result']['N'] as $key)
									@if($key -> positive)
										<tr>
											<td>N</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['G'] as $key)
									@if($key -> positive)
										<tr>
											<td>G</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['A'] as $key)
									@if($key -> positive)
										<tr>
											<td>A</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach
								<tr>
									<th style="background-color:  #a8c3f3; text-align: center;" colspan="2">Gaya Kerja</th>
								</tr>

								@foreach($data['wba']['result']['R'] as $key)
									@if($key -> positive)
										<tr>
											<td>R</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['D'] as $key)
									@if($key -> positive)
										<tr>
											<td>D</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['C'] as $key)
									@if($key -> positive)
										<tr>
											<td>C</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach
								<tr>
									<th style="background-color:  #f8dff9; text-align: center;" colspan="2">Aktivitas / Tempo Kerja</th>
								</tr>
								@foreach($data['wba']['result']['T'] as $key)
									@if($key -> positive)
										<tr>
											<td>T</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['V'] as $key)
									@if($key -> positive)
										<tr>
											<td>V</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach
								<tr>
									<th style="background-color:  #9ede0c; text-align: center;" colspan="2">Keikutsertaan / Sikap Sebagai Bawahan</th>
								</tr>
								@foreach($data['wba']['result']['F'] as $key)
									@if($key -> positive)
										<tr>
											<td>F</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach
								@foreach($data['wba']['result']['W'] as $key)
									@if($key -> positive)
										<tr>
											<td>W</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="page-break"></div>
				<div class="row pt40 body-pdf">
					<div class="col-xs-12">
						<table class="table table-bordered wba-desc">
							<tbody>
								<tr>
									<th style="background-color:  #fee426; text-align: center;" colspan="2">Sikap Sosial</th>
								</tr>
								@foreach($data['wba']['result']['O'] as $key)
									@if($key -> positive)
										<tr>
											<td>O</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['B'] as $key)
									@if($key -> positive)
										<tr>
											<td>B</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['S'] as $key)
									@if($key -> positive)
										<tr>
											<td>S</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['X'] as $key)
									@if($key -> positive)
										<tr>
											<td>X</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach
								<tr>
									<th style="background-color:  #de0c26; color: white; text-align: center;" colspan="2">Tempramen</th>
								</tr>
								@foreach($data['wba']['result']['Z'] as $key)
									@if($key -> positive)
										<tr>
											<td>Z</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['E'] as $key)
									@if($key -> positive)
										<tr>
											<td>E</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['K'] as $key)
									@if($key -> positive)
										<tr>
											<td>K</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach
								<tr>
									<th style="background-color:  #5a5a5a; color: white; text-align: center;" colspan="2">Kepemimpinan</th>
								</tr>
								@foreach($data['wba']['result']['L'] as $key)
									@if($key -> positive)
										<tr>
											<td>L</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['P'] as $key)
									@if($key -> positive)
										<tr>
											<td>P</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach

								@foreach($data['wba']['result']['I'] as $key)
									@if($key -> positive)
										<tr>
											<td>I</td>
											<td>(+) {!! $key -> positive !!}</td>
										</tr>
									@endif
									@if($key -> negative)
										<tr>
											<td></td>
											<td>(-) {!! $key -> negative !!}</td>
										</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		@else
		<input type="hidden" name="wba" id="wba" value="0">
		@endif
	</section>
@stop

@section("inline-script")
	{!! Html::script('assets/vendor/canvasjs/canvasjs.min.js') !!}
	{!! Html::script('assets/vendor/canvasjs/jquery.canvasjs.min.js') !!}
	{!! Html::script('assets/vendor/chart/Chart.bundle.js') !!}
	<script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    {!! Html::script('assets/vendor/datatable-tpa/extensions/responsive/js/dataTables.responsive.min.js') !!}
	{!! Html::script('assets/vendor/datatable-tpa/extensions/fixedcolumns/js/dataTables.fixedColumns.min.js') !!}
	<script>
		$(document).ready(function() {
			$("#table_report").DataTable({
					"bSort" : false,
					"bPaginate": false,
					"info": false,
					"searching": false,
					"columnDefs": 	[
						{ "visible": false, "targets": 1 },
					],
					"order": [[ 1, 'asc' ]],
					"drawCallback": function ( settings ) {
			            var api = this.api();
			            var rows = api.rows( {page:'current'} ).nodes();
			            var last=null;

			            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
			                if ( last !== group ) {
			                    $(rows).eq( i ).before(
			                        '<tr class="group"><td colspan="10">'+group+'</td></tr>'
			                    );

			                    last = group;
			                }
			            } );
			        }
			});
		});
	</script>
	<script>
		  	$(function() {
			    $("#most-graph").CanvasJSChart({
			      	axisY: {
				        includeZero: false,
				        minimum: -8,
				        maximum: 8,
				        valueFormatString: " ",
				        tickLength: 0
			      	},
			      	axisX: {
			        	interval: 1
			      	},
			      	data: [{
				        type: "line", //try changing to column, area
				        toolTipContent: "{label}: {y}",
				        dataPoints: [
				            { label: "D",  y: {{ $data['wpa']['most_d'] }} },
				            { label: "I",  y: {{ $data['wpa']['most_i'] }} },
				            { label: "S",y: {{ $data['wpa']['most_s'] }}},
				            { label: "C",y: {{ $data['wpa']['most_c'] }} },
				        ]
				    }]
			    });
			    
			    $("#least-graph").CanvasJSChart({
			      	axisY: {
			        	includeZero: false,
			        	minimum: -8,
			        	maximum: 8,
			        	valueFormatString: " ",
			        	tickLength: 0
			      	},
			      	axisX: {
			        	interval: 1
			      	},
			      	data: [{
			        	type: "line", //try changing to column, area
			        	toolTipContent: "{label}: {y}",
			        	dataPoints: [
			                { label: "D",  y: {{ $data['wpa']['lest_d'] }} },
			                { label: "I",  y: {{ $data['wpa']['lest_i'] }} },
			                { label: "S",y: {{ $data['wpa']['lest_s'] }}},
			                { label: "C",y: {{ $data['wpa']['lest_c'] }} },
			        	]
			     	}]
			    });
			    
			    $("#change-graph").CanvasJSChart({
			      	axisY: {
			        	includeZero: false,
			        	minimum: -8,
			        	maximum: 8,
			        	valueFormatString: " ",
			        	tickLength: 0
			      	},
			      	axisX: {
			        		interval: 1
			      	},
			      	data: [{
			        	type: "line", //try changing to column, area
			        	toolTipContent: "{label}: {y}",
			        	dataPoints: [
			                { label: "D",  y: {{ $data['wpa']['change_d'] }} },
			                { label: "I",  y: {{ $data['wpa']['change_i'] }} },
			                { label: "S",y: {{ $data['wpa']['change_s'] }}},
			                { label: "C",y: {{ $data['wpa']['change_c'] }} },
			        	]
			      	}]
			    });
		  	});
		
	  	//=============================== WBA ===============================//

	  	if($('#wba').val() == 1){
		  	var ctx = document.getElementById("myChart");
			var myChart = new Chart(ctx, {
			    type: 'radar',
			    data: {
			        labels: ["N","G","A","L","P","I","T","V","X","S","B","O","R","D","C","Z","E","K","F","W"],
			        datasets: [{
			            label: '',
			            data: [
			                {{ $data['wba']['N'] }},
			                {{ $data['wba']['G'] }},
			                {{ $data['wba']['A'] }},
			                {{ $data['wba']['L'] }},
			                {{ $data['wba']['P'] }},
			                {{ $data['wba']['I'] }},
			                {{ $data['wba']['T'] }},
			                {{ $data['wba']['V'] }},
			                {{ $data['wba']['X'] }},
			                {{ $data['wba']['S'] }},
			                {{ $data['wba']['B'] }},
			                {{ $data['wba']['O'] }},
			                {{ $data['wba']['R'] }},
			                {{ $data['wba']['D'] }},
			                {{ $data['wba']['C'] }},
			                {{ ReverseIndexPapi($data['wba']['Z']) }},
			                {{ $data['wba']['E'] }},
			                {{ ReverseIndexPapi($data['wba']['K']) }},
			                {{ $data['wba']['F'] }},
			                {{ $data['wba']['W'] }}
			            ],
			            borderWidth: 3,
						pointBorderColor: "rgba(0, 0, 0, 1)",
			            pointBorderWidth: 2,
						borderColor: "rgba(0, 0, 0, 1)",
			        }]
			    },
			    options: {
			        tooltips: { enabled: false },
			        scale: {
			            ticks: {
			                beginAtZero :0,
			                min:0,
			                max  :10
			            }
			        },
			        title: {
			            display: false
			        }
			    }
			});
	  		
		myChart.options.legend.display = false;
	  	}
	</script>
@stop
