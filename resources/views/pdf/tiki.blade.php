@extends('layouts.print')
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan Test Intelegensi Kolektif Indonesia</h2>
		</div>
		<div class="print-user-info">
			<div class="row">
				<div class="col-xs-7">
					<div class="wpa-result-info">
						<dl class="dl-horizontal">
							<dt>Nama</dt>
							<dd>{!! get_firstname_by_id($data['id_user']) !!}</dd>
							<dt>NIP</dt>
							<dd>-</dd>
							<dt>Periode</dt>
							<dd>{{ date('Y-m-d', strtotime($data['start_date'])) }} - {{ date('Y-m-d', strtotime($data['end_date'])) }}</dd>
							<dt>Tanggal Pengerjaan</dt>
							<dd>{{ $data['date'] }}</dd>
						</dl>
					</div>
				</div>
				<div class="col-xs-5">
					<div class="wpa-result-summary">
						<p><strong>Taraf Kecerdasan (IQ):</strong></p>
						<h3>{{ $data['iq'] }} <span class="label label-info">{{ $data['golongan_iq'] }}</span></h3>
					</div>
				</div>
			</div>
		</div>
		<div class="tiki-summary">
			<div class="row tiki-summary-header">
				<div class="col-xs-5">
					<strong>Kriteria</strong>
				</div>
				<div class="col-xs-2">
					<strong>Nilai Standar</strong>
				</div>
				<div class="col-xs-5">
					&nbsp;
				</div>
			</div>
			<div class="row">
				<div class="col-xs-5">
					<h5>Berhitung Angka</h5>
					<p>Kecepatan dan ketepatan berhitung</p>
				</div>
				<div class="col-xs-2">
					<h4 class="text-center">{!! $data['nilai_standar'][0] !!}</h4>
				</div>
				<div class="col-xs-5">
					<div class="progress" style="height: 25px">
						<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="28" style="width: {{ tikiPercent($data['nilai_standar'][0], 28) }}; height: 25px">
							<span class="skill"><i class="val">{!! $data['nilai_standar'][0] !!}</i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-5">
					<h5>Gabungan Bagian</h5>
					<p>Daya Konsentrasi (Sintesis dan Analisis)</p>
				</div>
				<div class="col-xs-2">
					<h4 class="text-center">{!! $data['nilai_standar'][1] !!}</h4>
				</div>
				<div class="col-xs-5">
					<div class="progress" style="height: 25px">
						<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['nilai_standar'][1], 30) }}; height: 25px">
							<span class="skill"><i class="val">{!! $data['nilai_standar'][1] !!}</i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-5">
					<h5>Hubungan Kata</h5>
					<p>Penilaian realitas, berpikir, praktis-konkrit</p>
				</div>
				<div class="col-xs-2">
					<h4 class="text-center">{!! $data['nilai_standar'][2] !!}</h4>
				</div>
				<div class="col-xs-5">
					<div class="progress" style="height: 25px">
						<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['nilai_standar'][2], 30) }}; height: 25px">
							<span class="skill"><i class="val">{!! $data['nilai_standar'][2] !!}</i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-5">
					<h5>Abstraksi Non Verbal</h5>
					<p>Kemampuan abstraksi, daya klasifikasi</p>
				</div>
				<div class="col-xs-2">
					<h4 class="text-center">{!! $data['nilai_standar'][3] !!}</h4>
				</div>
				<div class="col-xs-5">
					<div class="progress" style="height: 25px">
						<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['nilai_standar'][3], 30) }}; height: 25px">
							<span class="skill"><i class="val">{!! $data['nilai_standar'][3] !!}</i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row tiki-summary-footer">
				<div class="col-xs-5">
					<h4>Jumlah Angka TiQi</h4>
				</div>
				<div class="col-xs-2">
					<h4 class="text-center"><strong>{!! $data['total_tiki'] !!}</strong></h4>
				</div>
				
			</div>
		</div>
	</div>
    <div class="col-xs-12">
		<div class="row">
			<div class="col-xs-8">
				<h3>Kesimpulan</h3>
				<table class="table table-bordered">
					<tr>
						<td>1</td>
						<td>Space and Non-Verbal Reasoning</td>
						<td>F1 : <strong>{!! $data['f1'] !!} {!! $data['golongan_f1'] !!} </strong></td>
					</tr>
					<tr>
						<td>2</td>
						<td>General Scholastic Aptitude</td>
						<td>F2 : <strong>{!! $data['f2'] !!} {!! $data['golongan_f2'] !!} </strong></td>
					</tr>
					<tr>
						<td>3</td>
						<td>Speed And Accuracy</td>
						<td>F3 : <strong>{!! $data['f3'] !!} {!! $data['golongan_f3'] !!} </strong></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</section>
@stop
