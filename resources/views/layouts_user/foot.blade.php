    <div id="js-var" style="visibility:none;" 
        data-toastr-position="{{config('config.notification_position')}}"
        data-something-error-message="{{trans('messages.something_error_message')}}"
        data-character-remaining="{{trans('messages.character_remaining')}}"
        data-textarea-limit="{{config('config.textarea_limit')}}"
        data-calendar-language="{!! config('lang.'.session('lang').'.calendar') !!}"
    ></div>
    
    {!! Html::script('assets/vendor/toastr/toastr.min.js') !!}
    @include('common.toastr_notification')
    @yield("inline-js")
    {{-- <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-81411131-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-81411131-2');
    </script> --}}

</body>
</html>
