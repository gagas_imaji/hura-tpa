@include('layouts_user.head')

<body>
    
	@include('layouts_user.header')

	@include('layouts_user.left_sidebar')

	@yield('content')

    @yield('script')
	
	@include('layouts_user.foot')