<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', '') }}</title>

    @if(isset($assets) && in_array('form-wizard',$assets))
        {!! Html::style('assets/vendor/form-wizard/form-wizard.css') !!}
    @endif

    <link href="<?php echo asset('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo asset('assets/vendor/materialize/css/materialize.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo asset('assets/css/animate.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo asset('assets/css/user/user-style.css'); ?>" rel="stylesheet" type="text/css"/>
    {!! Html::style('assets/vendor/toastr/toastr.min.css') !!}

    
    <link href="<?php echo asset('assets/css/chart.css'); ?>" rel="stylesheet" type="text/css"/>   
    <link rel="shortcut icon" type="image/png" href="<?php echo asset('assets/image/favicon.png'); ?>"/>
        @yield("inline-css")
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    
</head>