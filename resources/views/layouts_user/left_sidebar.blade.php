<section>
	<!-- Left Sidebar -->
	<aside id="leftsidebar" class="sidebar">
		<!-- User Info -->
		<div class="user-info">
			<div class="info-container">
				<i class="material-icons small white">perm_identity</i>
				<div class="name">{!! get_firstname() !!}</div>
				<div class="email">{!! get_email() !!}</div>
				<!-- <a data-toggle="modal" data-target="#myModal">Update Password</a> -->
				<a href="/user/reset">Update Password</a>
			</div>
		</div>
		<!-- #User Info -->
		
		<!-- Menu -->
		<div class="menu">
			<ul class="list">
				<li>
					<a href="{{ url('/home') }}" class="waves-effect">
						<i class="material-icons">home</i>
						<span>Home</span>
					</a>
				</li>
			</ul>
		</div>
		<!-- #Menu -->
		
		<!-- Footer -->
		<div class="legal">
			<div class="copyright">
				Copyright &copy; 2017 <strong>Talentlytica</strong>
			</div>
		</div>
		<!-- #Footer -->
		
	</aside>
	<!-- #END# Left Sidebar -->
	<!-- Modal -->
	<div class="modal fade slide-up" id="myModal" role="dialog">
	    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default m-t-10">
                        <div class="panel-heading">
                            {{trans('messages.update').' '.trans('messages.password')}}
                        </div>
                        <div class="panel-body">
                        	<form id="reset-password"  method="post" enctype="multipart/form-data" action="/user/reset-password/{{Auth::user()->id}}" data-submit="noAjax">
                        		{!! csrf_field() !!}
                                <div class="form-group">
                                    {!! Form::label('new_password',trans('messages.new').' '.trans('messages.password'),[])!!}
                                    {!! Form::input('password','new_password','',['class'=>'form-control '.(config('config.enable_password_strength_meter') ? 'password-strength' : ''),'placeholder'=>trans('messages.new').' '.trans('messages.password')])!!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('new_password_confirmation',trans('messages.confirm').' '.trans('messages.password'),[])!!}
                                    {!! Form::input('password','new_password_confirmation','',['class'=>'form-control','placeholder'=>trans('messages.confirm').' '.trans('messages.password')])!!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit(isset($buttonText) ? $buttonText : trans('messages.update'),['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                        	</form>
                        </div>
                    </div>
                </div>   
            </div>
	    </div>
	</div>
</section>