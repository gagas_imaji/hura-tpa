@extends('layouts.default_survey')

    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div>
        
    @stop
    
    @section('content')
        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}" id="id_user" />
        <input type="hidden" name="slug" value="{{ $slug }}" id="slug" />
        <input type="hidden" name="section" value="{{ $section }}" id="section" />
        <input type="hidden" name="section_name" value="{{ $section_name }}" id="section_name" />
        <input type="hidden" name="id_survey" value="{{ $id_survey }}" id="id_survey" />
        <div class="row">
         
            @foreach($questions as $question)
                {{ Form::hidden('id_question', $question->id_question, ['id' => 'id_question'])}}
                {{ Form::hidden('id_type_question', $question->id_type_question, ['id' => 'id_type_question'])}}
                {{ Form::hidden('is_mandatory', $question->is_mandatory, ['id' => 'is_mandatory'])}}
                
                <div class="col-lg-11 content-question">
                    <div class="panel panel-default">
                        <div class="panel-heading"> 
                            <div class="row"> 
                                <div class="header-left">
                                    <strong>{{ $question -> survey_title }}</strong>
                                    <div class="section-survey"><p>{{ $question -> name_section}}</p></div>
                                </div>
                                <div class="header-right pull-right">
                                    <h1 id="stopwatch_question" style="display: none;">00:00:00</h1>
                                    @if($question -> timer_survey/60 != 0) <!-- kalau ada timer survey -->
                                        <input type="hidden" name="timer_survey" value="{{ $question -> timer_survey - $used_timer}}" id="timer_survey" />
                                        <input type="hidden" name="timer_section" value="0" id="timer_section" />
                                        <input type="hidden" name="timer_question" value="0" id="timer_question" />
                                        <strong>
                                            Time Left : <span id="countdown_survey"></span>
                                        </strong>
                                    @else <!-- kalau ga ada timer survey -->
                                        <input type="hidden" name="timer_survey" value="0" id="timer_survey" />
                                        @if($question -> timer_section/60 != 0) <!-- kalau ada timer section -->
                                            <input type="hidden" name="timer_section" value="{{ $question -> timer_section - $used_timer}}" id="timer_section" />
                                            <input type="hidden" name="timer_question" value="0" id="timer_question" />
                                            <strong>
                                                Time Left : <span id="countdown_section"></span>
                                            </strong>
                                        @else
                                            <input type="hidden" name="timer_section" value="0" id="timer_section" />
                                            <input type="hidden" name="timer_question" value="{{ $question -> timer - $used_timer }}" id="timer_question" />
                                            @if($question -> timer/60 != 0)
                                                Time Left : <span id="countdown_question"></span>
                                            @endif
                                        @endif
                                        
                                    @endif

                                    

                                    <div>
                                        <a class="btn pop" rel="popover" href="jacascript:void()" data-original-title="Instruction" data-container="body" data-content="{!! $instruction !!}"  data-placement="bottom"><i class="fa fa-gears fa-fw"></i> Instruction</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body body-detail">
                            <div class="row">
                                @if($question -> id_type_question == 1)
                                    <div class="survey-question">
                                        <h3>{!! $question -> question !!}</h3>
                                    </div>
                                    <div class="survey-option">
                                        <?php $i = 0; ?>
                                        <h3>
                                        @foreach($option_answer as $option)
                                            <span style="margin-right: 20px;">   
                                                <input type="radio" name="answer" value="{{ $option -> id}}" id="id_answer<?php echo $i;?>" />&nbsp
                                                @if($option -> image == '')
                                                    {!! $option -> answer !!}
                                                @else
                                                    <img src="/assets/image/{{$section_slug}}/{{$option -> image}}" />
                                                @endif
                                            </span>
                                            <?php $i++;?>
                                        @endforeach
                                        </h3>
                                    </div>
                                @elseif($question -> id_type_question == 2)
                                    <div class="survey-question">
                                        <h3>{!! $question -> question !!}</h3>
                                        <p class="info-question">You can choose more than one answer</p>
                                    </div>
                                    <div class="survey-option">
                                    <?php $i = 0; ?>
                                    @foreach($option_answer as $option)
                                        <span style="margin-right: 20px;">
                                            <input type="checkbox" name="answer" value="{{ $option -> id}}" id="id_answer<?php echo $i;?>" /> &nbsp
                                            @if($option -> image == '')
                                                {!! $option -> answer !!}
                                            @else
                                                <img src="/assets/image/{{$section_slug}}/{{$option -> image}}" />
                                            @endif
                                        </span>
                                        <?php $i++;?>
                                    @endforeach
                                    </div>
                                @elseif($question -> id_type_question == 3 || $question -> id_type_question == 5)
                                    <div class="survey-question">
                                        <h3>{!! $question -> question !!}</h3>
                                    </div>
                                    <div class="survey-option">
                                        <p><textarea name="answer" id="id_answer0" class="form-control textarea-survey"></textarea></p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                @if($question -> id_type_question != 4)
                                    <div class="footer-left">
                                        <button class="btn btn-primary" id="skip">SKIP</button>
                                        <div class="btn btn-primary" id="next">NEXT</div>
                                        <button class="btn btn-primary" id="finish" style="display: none;">FINISH</button>
                                    </div>
                                    <div class="footer-right" style="width:200px;">
                                            <p class="question-answered">{!! $count !!} of {!! $total !!} answered</p>
                                        <div class="progress">
                                          <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="{!! $count !!}" aria-valuemin="0" aria-valuemax="{!! $total !!}" style="width: {!! $count/$total*100 !!}%">
                                            
                                          </div>
                                        </div>
                                    </div>
                                @else
                                    <center>
                                        <div class="btn btn-primary" id="next">NEXT</div>
                                    </center>
                                @endif
                            </div>
                        </div>                    
                    </div>
                </div>
            @endforeach
                                  
        </div>

        <div class="modal fade slide-up disable-scroll" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close fa-fw"></i>
                            </button>
                            <span class="label fs-30">It's Over</span>
                        </div>
                        <div class="modal-body">
                            <br>
                            <center>
                                <h3><span class="semi-bold">Waktu pengerjaan telah habis. Anda akan dialihkan ke halaman subtest berikutnya</span></h3>
                            </center>
                        </div>       
                    </div>
                    <!-- /.modal-content -->
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                //set all variable
                var id_question     = ($('#id_question').val());
                var id_type_question= ($('#id_type_question').val());
                var timer_survey    = $('#timer_survey').val();
                var timer_question  = $('#timer_question').val();
                var timer_section   = $('#timer_section').val();
                var id_user         = $('#id_user').val();
                var answer          = document.getElementsByName('answer');
                var length_answer   = answer.length;
                var slug            = $('#slug').val();
                var id_survey       = $('#id_survey').val();
                var count_survey    = $('#count_survey').val();
                var is_mandatory    = $('#is_mandatory').val();
                var section         = $('#section').val();
                var section_name    = $('#section_name').val();
                var i, j;


                //popover instruction
                $('a[rel=popover]').popover();

                //set timer survey
                if(timer_survey != 0){
                    countdown_survey("countdown_survey", timer_survey/60, 0);
                    function countdown_survey( elementName, minutes, seconds ){
                        if(timer_survey != 0){
                            var element_s, endTime_s, hours_s, mins_s, msLeft_s, time_s;

                            function twoDigits( n ){
                                return (n <= 9 ? "0" + n : n);
                            }

                            function updateTimer(){
                                msLeft_s = endTime_s - (+new Date);
                                if ( msLeft_s < 1000 ){
                                    element_s.innerHTML = "It's over";
                                    $('#skip').hide();
                                    $('#next').hide();
                                    var i;                  
                                    $('#myModal').modal(); 
                                    $.ajax({
                                        url : "/test-time-out",    
                                        data: {id_survey: id_survey},
                                        type: "POST"
                                    });
                                    setTimeout(function(){
                                        $('#myModal').modal('hide')
                                        // window.location = "/survey/save-report/"+slug+"/"+section;
                                    }, 5000);
                                } else {
                                    time_s = new Date( msLeft_s );
                                    hours_s = time_s.getUTCHours();
                                    mins_s = time_s.getUTCMinutes();
                                    element_s.innerHTML = (hours_s ? hours_s + ':' + twoDigits( mins_s ) : mins_s) + ':' + twoDigits( time_s.getUTCSeconds() );
                                    setTimeout( updateTimer, time_s.getUTCMilliseconds() + 500 );
                                }
                            }

                            element_s = document.getElementById( elementName );
                            endTime_s = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                            updateTimer();
                        }
                        
                    }
                }
                //set timer section
                if(timer_section != 0){
                    countdown_section("countdown_section", timer_section/60, 0);
                    function countdown_section( elementName, minutes, seconds ){
                        if(timer_section != 0){
                            var element_s, endTime_s, hours_s, mins_s, msLeft_s, time_s;

                            function twoDigits( n ){
                                return (n <= 9 ? "0" + n : n);
                            }

                            function updateTimer(){
                                msLeft_s = endTime_s - (+new Date);
                                if ( msLeft_s < 1000 ){
                                    element_s.innerHTML = "It's over";
                                    $('#skip').hide();
                                    $('#next').hide();
                                    var i;                  
                                    $('#myModal').modal(); 
                                    $.ajax({
                                        url : "/survey/delete-temp-section/"+slug,    
                                        type: "GET"/*,
                                        success :
                                        function(data){
                                            window.location.reload()
                                        }*/
                                    });
                                    setTimeout(function(){
                                        $('#myModal').modal('hide')
                                        window.location = "/survey/save-report/"+slug+"/"+section;                                       
                                    }, 5000);
                                } else {
                                    time_s = new Date( msLeft_s );
                                    hours_s = time_s.getUTCHours();
                                    mins_s = time_s.getUTCMinutes();
                                    element_s.innerHTML = (hours_s ? hours_s + ':' + twoDigits( mins_s ) : mins_s) + ':' + twoDigits( time_s.getUTCSeconds() );
                                    setTimeout( updateTimer, time_s.getUTCMilliseconds() + 500 );
                                }
                            }

                            element_s = document.getElementById( elementName );
                            endTime_s = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                            updateTimer();
                        }
                        
                    }
                }
                //set timer question
                countdown_question("countdown_question", timer_question/60, 0 );
                function countdown_question( elementName, minutes, seconds ){
                    if(timer_question != 0){
                        var element, endTime, hours, mins, msLeft, time;

                        function twoDigits( n ){
                            return (n <= 9 ? "0" + n : n);
                        }

                        function updateTimer(){
                            msLeft = endTime - (+new Date);
                            if ( msLeft < 1000 ){
                                element.innerHTML = "It's over";
                                $('#skip').hide();
                                $('#next').hide();
                                var i;
                                    $.ajax({
                                        url : "next-question",    
                                        data: {id_user: id_user, id_answer: '-', slug: slug, id_type_question: id_type_question, id_question: id_question, timer: timer_question, section: section, section_name: section_name},
                                        type: "POST",
                                        success :
                                        function(data){
                                            window.location.reload()
                                        }
                                    });
                                
                                // window.location.reload();
                            } else {
                                time = new Date( msLeft );
                                hours = time.getUTCHours();
                                mins = time.getUTCMinutes();
                                element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
                                setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
                            }
                        }

                        element = document.getElementById( elementName );
                        endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                        updateTimer();
                    }
                    
                }

                //get next question
                $("#next").click(function(){

                    //tambahkan used timer ke temp jika survey ada timer
                    if(timer_survey != 0 || timer_section != 0){
                        clearTimeout(t);
                        if(stop_minutes != 0){ //convert minutes to seconds
                            stop_seconds = stop_seconds + (stop_minutes*60);
                        }
                        if(stop_hours != 0){
                            stop_seconds = stop_seconds + (stop_hours*60*60);
                        }

                        $.ajax({
                            url : "save-temp",    
                            data: {survey_id: id_survey, question_id: id_question, user_id: id_user,  slug: slug, timer: stop_seconds, section: section},
                            type: "POST"/*,
                            success :
                            function(data){
                                window.location.reload()
                            }*/
                        });
                    }
                    var i;
                    clearTimeout(t);
                    if(stop_minutes != 0){ //convert minutes to seconds
                        stop_seconds = stop_seconds + (stop_minutes*60);
                    }
                    if(stop_hours != 0){
                        stop_seconds = stop_seconds + (stop_hours*60*60);
                    }
                    //open text
                    if(length_answer == 1){ 
                            //jika soal mandatory dan user tidak menjawab, maka alert
                            if(is_mandatory == 1){ 
                                if(answer[0].value == ''){ 
                                    alert('Silahkan jawab terlebih dahulu');
                                    return false;
                                }
                            }
                            
                            $.ajax({
                                url : "next-question",    
                                data: {id_user: id_user, id_answer: answer[0].value, slug: slug, id_type_question: id_type_question, id_question: id_question, timer: stop_seconds, section: section, section_name: section_name},
                                type: "POST",
                                success :
                                function(data){
                                    window.location.reload()
                                }
                            });
                        
                        
                    }else{ //multiple choice
                        var counta = 0;
                        for(i = 0; i < length_answer; i++){
                            if(answer[i].checked){
                                $.ajax({
                                    url : "next-question",    
                                    data: {id_user: id_user, id_answer: answer[i].value, slug: slug, id_type_question: id_type_question, id_question: id_question, timer: stop_seconds, section: section, section_name: section_name},
                                    type: "POST",
                                    success :
                                    function(data){
                                        window.location.reload()
                                    }
                                });
                            }else{
                                counta++;
                            }
                        }

                        //jika soal mandatory dan user tidak menjawab maka alert
                        if(is_mandatory == 1 && counta == length_answer){ 
                                alert('Silahkan jawab terlebih dahulu');
                                return false;
                        }
                    }
                });

                //skip 
                $('#skip').click(function(){
                    //tambahkan used timer ke temp jika survey ada timer
                    if(timer_survey != 0 || timer_section != 0){
                        clearTimeout(t);
                        if(stop_minutes != 0){ //convert minutes to seconds
                            stop_seconds = stop_seconds + (stop_minutes*60);
                        }
                        if(stop_hours != 0){
                            stop_seconds = stop_seconds + (stop_hours*60*60);
                        }

                        $.ajax({
                            url : "save-temp",    
                            data: {survey_id: id_survey, question_id: id_question, user_id: id_user,  slug: slug, timer: stop_seconds, section: section},
                            type: "POST",
                            success :
                            function(data){
                                window.location.reload()
                            }
                        });
                    }else{
                        
                        if(timer_question == 0){
                            window.location.reload()
                        }else{
                            clearTimeout(t);
                            if(stop_minutes != 0){ //convert minutes to seconds
                                stop_seconds = stop_seconds + (stop_minutes*60);
                            }
                            if(stop_hours != 0){
                                stop_seconds = stop_seconds + (stop_hours*60*60);
                            }

                            $.ajax({
                                url : "save-temp",    
                                data: {survey_id: id_survey, question_id: id_question, user_id: id_user,  slug: slug, timer: stop_seconds, section: section},
                                type: "POST",
                                success :
                                function(data){
                                    window.location.reload()
                                }
                            });
                            
                        }
                    }

                });

                //stopwatch_question
                var stopwatch_question = document.getElementById('stopwatch_question'),
                start = document.getElementById('start'),
                stop = document.getElementById('stop'),
                clear = document.getElementById('clear'),
                stop_seconds = 0, stop_minutes = 0, stop_hours = 0,
                t;
                function add() {
                    stop_seconds++;
                    if (stop_seconds >= 60) {
                        stop_seconds = 0;
                        stop_minutes++;
                        if (stop_minutes >= 60) {
                            stop_minutes = 0;
                            stop_hours++;
                        }
                    }
                    
                    stopwatch_question.textContent = (stop_hours ? (stop_hours > 9 ? stop_hours : "0" + stop_hours) : "00") + ":" + (stop_minutes ? (stop_minutes > 9 ? stop_minutes : "0" + stop_minutes) : "00") + ":" + (stop_seconds > 9 ? stop_seconds : "0" + stop_seconds);

                    timers();
                }
                function timers() {
                    t = setTimeout(add, 1000);
                }

                timers();
            });
        </script>
    @stop