@extends('layouts.default')
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/project">Kelola Project</a></li>
                <li class="active">{!! $detail['nama'] !!}</li>
            </ol>
        </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><i class="fa fa-info fa-fw"></i> Info Project</strong>
                    </div>
                    <div class="panel-body full">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-check-square-o fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge">{{ $sudah }}</div><br/>
                                                <div>Sudah Menyelesaikan</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-spinner fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge">{{ $sebagian }}</div><br/>
                                                <div>Mengerjakan Sebagian</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-window-close-o fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge">{{ $belum }}</div><br/>
                                                <div>Belum Pernah Mengerjakan</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-stripped table-hover show-table">
                            <tbody>
                                <tr>
                                    <th>Nama</th>
                                    <td>{!!$detail['nama']!!}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Awal</th>
                                    <td>{!!$detail['tanggal_awal']!!}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Akhir</th>
                                    <td>{!!$detail['tanggal_akhir']!!}</td>
                                </tr>
                                <tr>
                                    <th>Deskripsi</th>
                                    <td>{!!$detail['deskripsi']!!}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    @if($detail['is_done'] == 0)
                                        <td>
                                            <span class="label label-warning">Belum berjalan</span>
                                        </td>
                                    @elseif($detail['is_done'] == 1)
                                        <td>
                                            <span class="label label-danger">Selesai</span>
                                        </td>
                                    @elseif($detail['is_done'] == 2)
                                        <td>
                                            <span class="label label-success">Sedang berjalan</span>
                                        </td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><i class="fa fa-tasks fa-fw"></i>  Tools Project ({{ count($detail['tools'])}} tools)</strong>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-hover show-table">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Random Pertanyaan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($detail['tools'] as $tools)
                                    <tr>
                                        <td>
                                           {!! $tools['judul'] !!}
                                        </td>
                                        <td>
                                            @if($tools['is_random'] == 0)
                                                <span class="label label-danger"> Tidak</span>
                                            @elseif($tools['is_random'] == 1)
                                                <span class="label label-info"> Random</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($tools['status'] == 0)
                                                <span class="label label-warning pull-left"> Belum berjalan</span>
                                            @elseif($tools['status'] == 1)
                                                <span class="label label-danger pull-left"> Selesai</span>
                                            @elseif($tools['status'] == 2)
                                                <span class="label label-success pull-left"> Sedang berjalan</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><i class="fa fa-user fa-fw"></i>  Peserta Project</strong>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-striped table-bordered" id="list-peserta-project">
                            <thead>
                              <tr class="headings">
                                    <th class="text-center">Nama Peserta</th>
                                    <th class="text-center">Status Penyelesaian</th>
                                    <th class="text-center">Pilihan</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $row)
                                        <tr>
                                            <td>{!! $row['name'] !!}</td>
                                            <td>
                                                {!! $row['status'] !!}
                                                @foreach($row['ceklis'] as $key)
                                                    {!! $key !!}
                                                @endforeach
                                            </td>
                                            <td>
                                                {!! $row['delete'] !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

        $(document).ready(function() {
            $('#list-peserta-project').DataTable();

        });
        </script>
    @stop