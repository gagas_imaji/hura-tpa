@extends('layouts.default_survey')

@section('breadcrumb')
    <div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="/home">{!! trans('messages.home') !!}</a></li>
            <li class="active">Tools</li>
        </ol>

    </div>
     
@stop

@section('content')
    
    <input type="hidden" name="id_user" value="{{ Auth::user()->id }}" id="id_user" />
    <input type="hidden" name="slug" value="{{ $slug }}" id="slug" />
    <input type="hidden" name="section" value="{{ $section }}" id="section" />
    <input type="hidden" name="section_name" value="{{ $data['section_name'] }}" id="section_name" />
    <input type="hidden" name="id_survey" value="{{ $data['id_survey'] }}" id="id_survey" />
    {{ Form::hidden('number', $data['number'][0], ['id' => 'number'])}}
    

    <div id="tool-container" class="container animated fadeIn">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-title animated fadeInDown">
                        <h6 class="left grey-text text-lighten-1">{!! $data['section_name'] !!}</h6>
                        <a class="tooltipped" data-position="bottom" data-delay="20" data-tooltip="{!! $data['instruction'] !!}" data-html="true"><i class="material-icons">info</i></a>

                        <span class="grey-text text-lighten-1" id="stopwatch_question" >00:00:00</span>
                        <input type="hidden" name="timer_survey" value="{{ $data['timer_survey'] - $used_timer }}" id="timer_survey" />

                        <span class="grey-text text-lighten-1" style="display: none;">Sisa waktu:</span> <span id="countdown_survey" class="grey-text" style="display: none;"></span>
                    </div>                                  
                        
                    <div class="card-content tool-question slides">
                        <?php $index_question = 0;?>
                        @foreach($data['content'] as $content)
                        <div slide-id="{!!$index_question!!}" class="slide <?php echo ($index_question == 0 ? 'active' : '')?>">
                            <div class="tool-question wpa-question">
                                <table class="table tool-option radio-grid">
                                    <thead>
                                        <tr>
                                            <th class="option-label">Sesuai</th>
                                            <th class="option-label">Tak Sesuai</th>
                                            <th class="statement-label">Pernyataan</th>
                                        </tr>   
                                    </thead>
                                    <tbody>
                                    @for($i = 0; $i < 4; $i++)
                                        <tr data-toggle="buttons">
                                            <td class="option-item option-most">
                                                <label class="btn btn-tool ma-icon thumb_up waves-effect waves-light">
                                                    <input type="radio" name="most" value="{{ $content['most'][$i] }}" id="most" autocomplete="off" data-number="{{$data['number'][$index_question]}}" onChange="$.AdminUser.most.activate({{$data['number'][$index_question]}})">
                                                </label>
                                            </td>
                                            <td class="option-item option-least">
                                                <label class="btn btn-tool ma-icon thumb_down waves-effect waves-light">
                                                    <input type="radio" name="lest" value="{{ $content['lest'][$i] }}" id="lest" autocomplete="off" data-number="{{$data['number'][$index_question]}}" onChange="$.AdminUser.lest.activate({{$data['number'][$index_question]}})">
                                                </label>
                                            </td>
                                            <td class="option-statement">{!! $content['statement'][$i] !!}</td>
                                        </tr>
                                    @endfor
                                        <tr style="display: none;" id="warning{{$data['number'][$index_question]}}" ><td colspan="3" class="warning alert alert-warning">Mohon untuk tidak memilih statement yang sama pada kolom Sesuai dan Tak Sesuai</td></tr>
                                    </tbody>
                                </table>    
                            </div>
                            <div class="question-action-box text-center">
                                <div class="question-action" style="display:none;">
                                    <button class="btn waves-effect waves-light next" id="next_{{$index_question}}" onClick="saveCache({!!$data['number'][$index_question]!!})">Lanjut</button> 
                                </div>
                            </div>
                        </div>
                        <?php $index_question++; ?>
                        @endforeach
                    </div>
                    <div class="card-action animated fadeInUp">
                        <div class="row">
<!--                             <div class="tool-action col l6">
                                <button id="prev" class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="20" data-tooltip="Soal sebelumnya" id="prev"><i class="material-icons">keyboard_arrow_left</i></button>
                                <button id="skip" class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="20" data-tooltip="Lewati soal ini" id="skip"><i class="material-icons">skip_next</i></button>
                            </div> -->

                            <div class="tool-progress col l3 right text-right">
                                <div class="question-answered grey-text text-lighten-1">
                                    <input type="hidden" name="count_answered" value="0" id="val_answered"/>
                                    <input type="hidden" name="total" value="24" id="val_total"/>
                                    <span id="count_answered">0</span>
                                    dari 24 soal dijawab
                                    <div class="progress">
                                        <div class="determinate" style="width: 0%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                                      
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="length_question" value="{{ $index_question }}" id="length_question" />    
@stop

@section('script')
    <script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo asset('assets/js/materialize.min.js'); ?>"></script>
    <script src="<?php echo asset('assets/js/tool-page.js'); ?>"></script>
    <script type="text/javascript">
        $.ajaxSetup({
           headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
        });

        $.AdminUser = {};

        var most            = document.getElementsByName('most');
        var lest            = document.getElementsByName('lest');
        var length          = most.length;
        var timer_survey    = $('#timer_survey').val();
        var id_user         = $('#id_user').val();
        var slug            = $('#slug').val();
        var id_survey       = $('#id_survey').val();
        var section         = $('#section').val();
        var section_name    = $('#section_name').val();
        var length_question = $('#length_question').val();
        var key_most        = [];
        var key_lest        = [];
        var count_answered  = 0;
        var gotonext        = 0; 
        var check_most      = 0;
        var check_lest      = 0;
        var used            = 0;
        var list_number     = []; //untuk pengecekan nomor yang sudah dijawab. jika ada yang sama, maka count_answered tidak bertambah
        var temp_gagal      = [];
        var cache_gagal     = [];
        var i, j;

        $.AdminUser.most = {
            activate: function checkStatementMost(active_number){

                //check same statement on most and lest
                for(i = 0; i < length; i++){
                    if(most[i].dataset.number == active_number){
                        //hapus class active untuk yang tidak dipilih
                        most[i].parentElement.setAttribute('class', 'btn btn-tool ma-icon thumb_up waves-effect waves-light'); 

                        if(most[i].checked == true){
                            //tambah class active untuk yang dipilih
                            most[i].parentElement.setAttribute('class', 'btn btn-tool ma-icon thumb_up waves-effect waves-light active');
                            check_most = 1;
                            //check kolom lest, jika checked maka pindah ke most
                            if(lest[i].checked == true){
                                lest[i].checked = false;
                                check_lest = 0;
                                $(".question-action").fadeOut();
                                $('#warning'+active_number).show('medium');
                            }
                        }   
                    }
                }
                if(check_most == 1 && check_lest == 1){
                    $(".question-action").show().addClass('animated fadeInUp');
                }
            },

        }
        
        $.AdminUser.lest = {
            activate: function checkStatementLest(active_number){
        
            //check same statement on most and lest
            for(i = 0; i < length; i++){
                if(lest[i].dataset.number == active_number){
                    //hapus class active untuk yang tidak dipilih
                    lest[i].parentElement.setAttribute('class', 'btn btn-tool ma-icon thumb_down waves-effect waves-light'); 

                    if(lest[i].checked == true){
                        //tambah class active untuk yang dipilih
                        lest[i].parentElement.setAttribute('class', 'btn btn-tool ma-icon thumb_down waves-effect waves-light active');
                        check_lest = 1;
                        if(most[i].checked == true){
                            most[i].checked = false;
                            check_most = 0;
                            $(".question-action").fadeOut();
                            $('#warning'+active_number).show('medium');
                        }
                    }
                }
            }
            if(check_most == 1 && check_lest == 1){
                $(".question-action").show().addClass('animated fadeInUp');
            }
        }  
        }

         

        //save answer to cache
        function saveCache(number){
            gotonext = 0;
            
            simpan_temporary_subtest(number);

            var count_null_answer        = 0; //jumlah untuk pengecekan sudah ada jawaban yang dipilih apa belum
            var index_answered_most      = 0; //index sejumlah jawaban jika jawaban lebih dari 1
            var index_answered_lest      = 0; //index sejumlah jawaban jika jawaban lebih dari 1
            
            for(i = 0; i < length; i++){
                if(most[i].checked){
                    if(most[i].dataset.number == number){
                        key_most = most[i].value;
                        index_answered_most++;
                    }
                }
                if(lest[i].checked){
                    if(lest[i].dataset.number == number){
                        key_lest = lest[i].value;
                        index_answered_lest++;
                    }
                }
            }
            if(index_answered_most != 0 && index_answered_lest != 0){

                list_number.push(number);
                count_answered++;

                //check jika nomor yang disubmit ada yang sama, maka count_answered tidak jadi ditambah (user edit jawaban)
                for(j = 0; j < list_number.length-1; j++){
                    if(list_number[j] == number){
                        count_answered--;
                    }
                }

                //untuk keperluan progress bar
                $('#val_answered').val(count_answered);
                width = ($('#val_answered').val()/$('#val_total').val())*100;
                element_answered = document.getElementById('count_answered');
                element_answered.innerHTML = count_answered;
                $('.determinate').css('width', width+"%");

                //kondisi jika semua pertanyaan sudah dijawab, maka tampilkan loader
                if($('#val_answered').val() == $('#val_total').val()){
                    $('.slide').fadeOut('medium');
                    $('.loadings').fadeIn('medium');
                    $('.loadings').addClass('active');
                }
                
                simpan_jawaban_ke_cache(number, key_most, key_lest);
                gotonext = 1;
            }

            //jika user tidak menjawab kedua pilihan maka alert
            if(index_answered_most == 0 || index_answered_lest == 0){ 
                alert('Silahkan pilih pernyataan sesuai dan tidak sesuai');
                return false;
            }
            used = 0;
        }

        function simpan_temporary_subtest(number){
            $.ajax({
                url : "save-temp",    
                data: {survey_id: id_survey, question_id: number, user_id: id_user,  slug: slug, timer: used, section: section},
                type: "POST",
                error: function(){
                    var isi_temp = [number, used];
                    temp_gagal.push(isi_temp);
                }
            });
        }

        function hapus_temporary_subtest(){
            $.ajax({
                url : "/survey/delete-temp-section/"+slug,    
                type: "GET"
            });
        }

        function simpan_jawaban_ke_cache(number, key_most, key_lest){
            $.ajax({
                url : "set-result-cache-wpa",    
                data: {survey_id: id_survey, number: number, slug: slug, section: section, section_name: section_name, key_most: key_most, key_lest: key_lest, length_question: length_question, id_user: id_user},
                type: "POST",
                success :
                function(result){
                    if(result === 'finish'){
                        $('.slide').fadeOut('medium');
                        $('.loadings').fadeIn('medium');
                        $('.loadings').addClass('active');
                        hapus_temporary_subtest();
                        subtest_selesai();

                    }
                },
                error: function(){
                    var isi_cache = [number, key_most, key_lest];
                    cache_gagal.push(isi_cache);
                } 
            });
        }

        function subtest_selesai(){
            $.ajax({
                url : "/survey/save-cache-to-db-wpa/"+slug+'/'+section,    
                type: "GET", 
                success :
                function(finish){
                    window.location.href = '/go-finish/'+slug+'/'+section+'/'+id_user;
                },
                error: function(){
                    subtest_selesai();
                }
            });
        }
        
        //stopwatch_question
        var stopwatch_question = document.getElementById('stopwatch_question'),
        start = document.getElementById('start'),
        stop = document.getElementById('stop'),
        clear = document.getElementById('clear'),
        stop_seconds = 0, stop_minutes = 0, stop_hours = 0,
        t;
        function add() {
            stop_seconds++;
            used++;
            if (stop_seconds >= 60) {
                stop_seconds = 0;
                stop_minutes++;
                if (stop_minutes >= 60) {
                    stop_minutes = 0;
                    stop_hours++;
                }
            }
            
            stopwatch_question.textContent = (stop_hours ? (stop_hours > 9 ? stop_hours : "0" + stop_hours) : "00") + ":" + (stop_minutes ? (stop_minutes > 9 ? stop_minutes : "0" + stop_minutes) : "00") + ":" + (stop_seconds > 9 ? stop_seconds : "0" + stop_seconds);

            timers();
        }
        function timers() {
            t = setTimeout(add, 1000);
        }

        timers();
            
        $(document).ready(function(){
            if (performance.navigation.type == 1) {
                window.location.href = "/clear-cache/"+slug+'/'+section;
            } else {
            }                

            //setting slider
            var getslideHeight = $('.slide.active').height();
                
            $('.slides').css({
                height: getslideHeight
            });
            
            function calcslideHeight() {
                getslideHeight = $('.slide.active').height();
                
                $('.slides').css({
                    height: getslideHeight
                });
            }
            

            //go to next slider
            var slideItem = $('.slide'),
            slideCurrentItem = slideItem.filter('.active');
            
            $('#skip').on('click', function(e) {
                e.preventDefault();
                
                var nextItem = slideCurrentItem.next();
                
                slideCurrentItem.removeClass('active');
                $('.tool-statement').removeClass('animated fadeIn');
                $('.option-text').removeClass('animated flipInX');
                $(".question-action").hide();
                
                if (nextItem.length) {
                    
                    slideCurrentItem = nextItem.addClass('active');
                    slideCurrentItem.find('.option-text').addClass('animated flipInX');
                    
                    } else {
                    slideCurrentItem = slideItem.first().addClass('active');
                    slideCurrentItem.find('.option-text').addClass('animated flipInX');
                }
                
                calcslideHeight();
            });
            
            $('.next').on('click', function(e) {
                if(gotonext == 1){

                    //reset jumlah most lest yang di check untuk hidden tombol next
                    check_lest = 0;
                    check_most = 0;
                    e.preventDefault();
                    
                    var nextItem = slideCurrentItem.next();
                    
                    slideCurrentItem.removeClass('active');
                    $('.tool-statement').removeClass('animated fadeIn');
                    $('.option-text').removeClass('animated flipInX');
                    $(".question-action").hide();
                    
                    if (nextItem.length) {
                        
                        slideCurrentItem = nextItem.addClass('active');
                        slideCurrentItem.find('.option-text').addClass('animated flipInX');
                        
                        } else {
                        slideCurrentItem = slideItem.first().addClass('active');
                        slideCurrentItem.find('.option-text').addClass('animated flipInX');
                    }
                    
                    calcslideHeight();
                }
            });

            $('#prev').on('click', function(e) {
                e.preventDefault();
                
                var prevItem = slideCurrentItem.prev();
                
                slideCurrentItem.removeClass('active');
                $('.tool-statement').removeClass('animated fadeIn');
                $('.option-text').removeClass('animated flipInX');
                $(".question-action").hide();
                
                if (prevItem.length) {
                    slideCurrentItem = prevItem.addClass('active');
                    slideCurrentItem.find('.option-text').addClass('animated flipInX');
                    } else {
                    slideCurrentItem = slideItem.last().addClass('active');
                    slideCurrentItem.find('.option-text').addClass('animated flipInX');
                }
                
                calcslideHeight();
            });

            
            $(function () {
                $.AdminUser.most.activate();
                $.AdminUser.lest.activate();
                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
            });

            $('.resend').click(function(){
                $('info-resend').hide();
                $('process-resend').fadeIn('medium');
                $('.process-resend').addClass('active');

                for(i = 0; i < temp_gagal.length; i++){
                    $.ajax({
                        url : "save-temp",    
                        data: {survey_id: id_survey, question_id: temp_gagal[i][0], user_id: id_user,  slug: slug, timer: temp_gagal[i][0], section: section},
                        type: "POST"
                    });
                }

                for(i = 0; i < cache_gagal.length; i++){
                    $.ajax({
                        url : "set-result-cache-wpa",    
                        data: {survey_id: id_survey, number: cache_gagal[i][0], slug: slug, section: section, section_name: section_name, key_most: cache_gagal[i][1], key_lest: cache_gagal[i][2], length_question: length_question, id_user: id_user},
                        type: "POST",
                        success :
                        function(result){
                            if(result === 'finish'){
                                $('.slide').fadeOut('medium');
                                $('.loadings').fadeIn('medium');
                                $('.loadings').addClass('active');
                                hapus_temporary_subtest();
                                subtest_selesai();
                            }
                        }
                    });
                }
            });
        });

    </script>
@stop