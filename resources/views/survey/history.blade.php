@extends('layouts.default')

	@section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Riwayat Tools</li>
            </ol>

        </div>
		
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-11 content-question">
                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <div class="row"> 
                            <div class="header-left">
                                <strong>Riwayat Tools</strong>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body body-detail">
                        <div class="row">
                                <center>
                                    <table id="surveyListTable" class="table table-hover table-noborder">
                                        <tbody>
                                        @if($history -> count() == 0)
                                            <h4>Anda Belum memiliki riwayat survey</h4>
                                        @else
                                            <?php $i = 1; ?>
                                            <tr>
                                                <th></th>
                                                <th>Nama Tools</th> 
                                                <th>Batas Tools</th>
                                            </tr>
                                            @foreach($history as $histories)
                                            <tr class="list-survey">
                                                <td class="td-day">
                                                    <span class="td-day-date">{{$i}}</span>
                                                </td>
                                                <td class="td-survey"><h4 class="survey-title">{{ $histories->title }}</h4><p class="survey-desc">{{ $histories->description }}</p></td> 
                                                <td><h4 class="survey-title"> {{ $histories->end_date }}</h4></td>
                                            </tr>
                                            <?php $i++; ?>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </center>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
	@stop
