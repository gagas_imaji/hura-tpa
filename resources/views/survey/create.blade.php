@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/project">Kelola Project</a></li>
                <li class="active">Tambah Project Baru</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<div class="panel-title text-danger">
                    		<i class="fa fa-plus"></i> Create Project
                    	</div> 
                    </div>
                    <div class="panel-body">
                		<form name="create_assessment" id="create_assessment" action="/survey-add" method="post" enctype="multipart/form-data" data-submit="noAjax" novalidate>
	                		{!! csrf_field() !!}
	                		<div class="col-lg-7">
	                			
				                <div class="form-group form-group-default col-lg-12 required">
								    {!! Form::label('name', 'Nama Project', [])!!}
									{!! Form::text('name', '',['class'=>'form-control', 'id' => 'name', 'required'=>'required'])!!}
								</div>
								<div class="form-group form-group-default col-lg-12 required">
								    {!! Form::label('description', 'Deskripsi Project', [])!!}
									{!! Form::text('description', '',['class'=>'form-control', 'id' => 'description', 'required'=>'required'])!!}
								</div>
								<div class="form-group form-group-default col-lg-6 required">
								    {!! Form::label('started', 'Tanggal Awal', [])!!}
									{!! Form::text('started', '',['class'=>'form-control', 'id' => 'started', 'required'=>'required'])!!}
								</div>
								<div class="form-group form-group-default col-lg-6 required">
								    {!! Form::label('ended', 'Tanggal Akhir', [])!!}
									{!! Form::text('ended', '',['class'=>'form-control', 'id' => 'ended', 'required'=>'required'])!!}
										
								</div>
								<div class="form-group form-group-default">
								    <input type="checkbox" name="randomized" id="randomized" value="1" />
									{!! Form::label('randomized', 'Random Pertanyaan', ['id'=>'randomized'])!!}
								</div>
								<div class="form-group form-group-default col-lg-12 required">
									{!! Form::label('tools', 'Tools yang dipakai', [])!!}
									<input type="checkbox" id="select_all_tools" /> Pilih Semua
									<select multiple="multiple" id="tools" name="tools[]" class="form-control">
										@foreach($survey_category as $key)
											<option value="{!! $key['id'] !!}" id="{!! $key['id'] !!}">{!! $key['name'] !!}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
										{!! Form::submit(isset($buttonText) ? $buttonText : trans('messages.save'),['class' => 'btn btn-primary pull-left', 'id'=>'create_survey']) !!}
								</div>
							</div>
						</form>
					</div>
				</div>
						
            </div>
        </div>
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" type="text/css" media="all">
        <script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script type="text/javascript">
        	$(document).ready(function(){

				var i;

        		//ketika pilih survey heat ada warning error, maka field lain tetap hide
        		if($('#category').val() == 1 || $('#category').val() == 2 || $('#category').val() == 3 || $('#category').val() == 4 || $('#category').val() == 5){
    				$('.title').hide('slow');
    				$('.description').hide('slow');
    				$('.instruction').hide('slow');
    				$('.thankyou').hide('slow');
    				$('.countdown').hide('slow');
    				$('.section').hide('slow');
        		}

        		//multiselect tools / select all
        		$("#tools").select2();
        		$("#select_all_tools").click(function(){
				    if($("#select_all_tools").is(':checked') ){
				        $("#tools > option").prop("selected",true);
				        $("#tools").trigger("change");
				    }else{
				        $("#tools > option").prop("selected", false);
				        $("#tools").trigger("change");
				     }
				});

				//survey detail show hide
        		$('#category').change(function(){
        			if($('#category').val() == 1 || $('#category').val() == 2 || $('#category').val() == 3 || $('#category').val() == 4 || $('#category').val() == 5 ){
        				$('.title').hide('slow');
        				$('.description').hide('slow');
        				$('.instruction').hide('slow');
        				$('.thankyou').hide('slow');
        				$('.countdown').hide('slow');
        				$('.section').hide('slow');
        			}else{
        				$('.title').show('slow');
        				$('.description').show('slow');
        				$('.instruction').show('slow');
        				$('.thankyou').show('slow');
        				$('.countdown').show('slow');
        				$('.section').show('slow');
        			}
        		});
	        	
        	});
        	

	        //set calendar
        	var dateToday = new Date();
			var dates = $("#started, #ended").datepicker({
			    defaultDate: "+1w",
			    changeMonth: true,
			    numberOfMonths: 1,
			    minDate: dateToday/*,
			    onSelect: function(selectedDate) {
			    	var dateMin = $('#started').datepicker("getDate");
			        var option = this.id == "started" ? "minDate" : "maxDate",
			            date = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1);
			        dates.not(this).datepicker("option", option, date);
			    }*/
			    
			});
        </script>
	@stop