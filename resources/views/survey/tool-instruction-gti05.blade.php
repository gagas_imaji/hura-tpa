@extends('layouts.default_survey')

	@section('content')
		<div class="container animated fadeIn" id="tool-container">
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content tool-instruction">
							<div class="row">
								<div class="col l12">
									<h5><strong>Instruksi</strong></h5>
									<h4>Learning Agility Index</h4>
									<p class="marginbottom20">Subtest 5</p>
									<p class="text-justify marginbottom20">
										Tes ini menggunakan dua bentuk dasar, huruf F yang diatas dan dibawah ini adalah berbeda antara satu dengan yang lainnya meski pada bentuk awalnya sama yakni huruf F.
									</p>
									<p class="text-center">
										<img class="marginbottom20 responsive-img no-border" src="<?php echo asset('assets/image/simulasi/contoh-soal5.gif'); ?>">
									</p>
									<p class="text-justify">
										Di bawah ini sebenarnya adalah contoh yang SAMA antara bentuk yang ada setelah diputar 90&deg;, 180&deg; atau 270&deg;. Perputaran tersebut bisa searah jarum jam (ke kanan) atau berlawanan arah jarum jam (ke kiri).
									</p>
									<p class="text-center">
										<img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/simulasi/contoh-soal5a.gif'); ?>">
									</p>
									<p class="text-justify">
										Perhatikan contoh berikut. Berapa banyak bentuk-bentuk di bawah ini yang sama seperti bentuk atasnya setelah bentuk-bentuk itu diputar?
									</p>
									<p class="text-center">
										<img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/simulasi/contoh-soal5b.gif'); ?>">
									</p>
									<p class="text-justify">
										Perhatikan contoh lainnya.
									</p>
									<p class="text-center">
										<img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/simulasi/contoh-soal5c.gif'); ?>">
									</p>
									<p class="text-justify">
										Pastikan Anda memahami cara menjawabnya dengan benar. <strong>Anda harus menunjukkan berapa pasangan yang sama</strong>.
									</p>
									<div class="info">
                                        <span class="label label-warning" id="load">Sistem sedang menyimpan jawaban Anda</span>
                                        <span class="label label-danger" style="display: none;" id="failed">Gagal menyimpan jawaban, silahkan refresh halaman ini untuk memproses ulang</span>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                              aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:45%" id="myBar">
                                            </div>
                                        </div>
                                    </div>
									<p class="margintop40">
										<button class="btn waves-effect waves-light" id="start" disabled>
                                            Coba Contoh Soal <i class="material-icons right">exit_to_app</i>
                                        </button>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>										
		</div>
		<input type="hidden" name="slug" value="{!! $data['slug'] !!}" id="slug">
        <input type="hidden" name="section" value="{!! $data['section'] !!}" id="section">
        <input type="hidden" name="has_result" value="{!! $data['has_result'] !!}" id="has_result">
        <input type="hidden" name="old_section" value="{!! $data['old_section'] !!}" id="old_section">
	@stop
	
	@section('script')
		<!-- JS -->
		<script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/materialize.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script>

		<script type="text/javascript">

            $(document).ready(function(){
                $(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });

                var slug = $('#slug').val();
                var section = $('#section').val();
                var has_result = $('#has_result').val();
                var old_section = $('#old_section').val();
                var elem = document.getElementById("myBar");

                if(has_result == 'yes'){
	                $.ajax({
	                    url : "/survey/save-cache-to-db/"+slug+'/'+old_section,
	                    type: "GET", 
	                    success :
	                    function(finish){
	                    	console.log(finish);
	                        if(finish == 'true'){
	                            elem.style.width = '100%';
	                            $('#start').prop('disabled', false);
	                            $('.info').fadeOut('slow');
	                        }else if(finish == 'false'){
	                            $('#load').hide();
	                            $('#failed').fadeIn();
	                        }
	                    }
	                });
	            }else{
                	elem.style.width = '100%';
                    $('#start').prop('disabled', false);
                    $('.info').fadeOut('slow');
                }

                $('#start').click(function(){
                    window.location.href = '/survey/'+slug+'/'+section+'/tool-test-sample-gti05';
                });
            });
        </script>
	@stop													