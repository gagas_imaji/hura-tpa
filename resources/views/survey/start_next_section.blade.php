@extends('layouts.default_survey')

	@section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div>
		
	@stop
	
	@section('content')

        <div id="tool-container" class="tool-container">
            <div class="tool-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body tool-panel-body">
                                    <div class="row">
                                        <div class="col-xs-2 col-xs-offset-5">
                                            <img class="marginbottom20 img-responsive" src="<?php echo asset('assets/image/example.png'); ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 text-center">
                                            <h3><strong>Anda telah menyelesaikan subtest ini.</strong></h3>
                                            <p>Silahkan tekan tombol [LANJUT] untuk memulai subtest berikutnya.</p>
                                            <h3><strong>Instruction</strong></h3>
                                            <p class="description">
                                                {!! $instruction !!}
                                            </p>
                                            <p class="margintop40"><a href="/survey/{{ $slug }}/{{ $new_section }}/start" class="btn btn-primary start" id="start" data-target="1">Lanjut</a></p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div> 

	@stop

    @section('script')
        <!-- JS -->
        <script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/js/materialize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });
            });
        </script>
    @stop
        