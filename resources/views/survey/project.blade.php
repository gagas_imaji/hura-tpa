@extends('layouts.default')
    @section('inline-css')
        <style type="text/css">
            .modal .modal-body {
                padding-top: 25px;
            }
            
        </style>
    @stop
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Kelola Project</li>
            </ol>

        </div> 
        
    @stop
    
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger">
                            <i class="fa fa-list"></i> List Semua Project
                        </div> 
                    @if(Entrust::can('create-survey'))
                        <div class="additional-btn">
                            <a href="/survey/create" class="btn btn-sm btn-primary">Tambah Project</a>
                        </div>
                        @endif
                    </div> 
                    <div class="panel-body full">
                        <table class="table table-hover table-striped table-bordered" id="list-project">
                            <thead>
                              <tr class="headings">
                                    <th class="text-center">Id</th>
                                    <th class="text-center">Nama Project</th>
                                    <th class="text-center">Tanggal Awal</th>
                                    <th class="text-center">Tanggal Akhir</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">User Diassign</th>
                                    <!-- <th class="text-center">Pembuat</th> -->
                                    <th class="action"></th>
                              </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $row)
                                        <tr>
                                            <td class="text-center">{!! $row['id'] !!}</td>
                                            <td>{!! $row['nama_project'] !!}</td>
                                            <td class="text-center">{!! $row['tanggal_awal'] !!}</td>
                                            <td class="text-center">{!! $row['tanggal_akhir'] !!}</td>
                                            <td class="text-center">{!! $row['status'] !!}</td>
                                            <td class="text-center">{!! $row['total_user_assigned'] !!}</td>
                                            <td class="text-center">{!! $row['pilihan'] !!}</td>
                                        </tr>

                                    @endforeach
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
        <script type="text/javascript">

        $(document).ready(function() {
            $('#list-project').dataTable({
                "columnDefs": [ {
                    "targets"  : 'action',
                    "orderable": false,
                    "order": []
                }],
                "dom": 'Bfrtip',
                "buttons": [ {
                    extend: "excel",
                    text: "Download .xlsx",
                    filename: "List Project"
                } ],

            });
        });
        </script>
    @stop