@extends('layouts.default')
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/project">Kelola Project</a></li>
                <li class="active">Assign Peserta Project</li>
            </ol>
        </div>
        
    @stop
    @section('content')
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger">
                            <i class="fa fa-send-o"></i> Assign peserta {{ $data['info_project']['name'] }}
                        </div>
                    </div>
                    <div class="panel-body full">
                        @if($data['jatah'] != 0)
                            <form id="assign_project"  method="post" enctype="multipart/form-data" action="/project-assign" data-submit="noAjax">
                                {!! csrf_field() !!}
                                <div class="box-body col-lg-6">
                                    <div class="col-lg-12">
                                        <div class="hasil-rekomendasi">
                                            <h4>Jumlah peserta yang dapat Anda assign saat ini adalah <span class="label label-info">{{ $data['jatah'] }}</span></h4>
                                        </div>
                                    </div>
                                    @if(date("Y-m-d h:i:s") > $data['info_project']['end_date'])
                                        <div class="col-lg-12">
                                            <label class="alert alert-danger text-center">Periode project sudah berakhir, silahkan perpanjang periode terlebih dahulu</label>
                                        </div>
                                    @else
                                    <input type="hidden" name="id" value="{{ $data['info_project']['id'] }}"/>
                                    <input type="hidden" name="jatah" value="{{ $data['jatah'] }}"/>
                                    <input type="hidden" name="jlh_peserta_available" value="{{$data['list_peserta'] -> count()}}" id="jlh_peserta_available"/>
                                    <input type="hidden" name="participant[]" />
                                    @if($data['list_peserta'] -> count() != 0)

                                        <div class="form-group form-group-default required col-lg-12">
                                            {!! Form::label('participant', 'Peserta Project', [])!!}
                                            <select name="metode" id="metode" class="form-control">
                                                <option>-- Pilih Metode --</option>
                                                <option value="1"> Upload Peserta</option>
                                                <option value="2"> Pilih Peserta</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group form-group-default required col-lg-12" id="pilih_peserta" style="display: none;">
                                            <label>Pilih Peserta</label>
                                            <input type="checkbox" id="select_all" /> Pilih Semua
                                            <select multiple="multiple" id="participant" name="participant[]" style="width: 400px;">
                                                @foreach($data['list_peserta'] as $users)
                                                    <option value="{!! $users['id'] !!}" id="{!! $users['id'] !!}">{!! $users['first_name'] !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                        <div class="form-group form-group-default col-lg-12 required alert-warning">
                                            <div class="form-control">
                                                <span class="">Semua peserta sudah di assign ke project ini</span>
                                            </div>
                                        </div>
                                    @endif
                                        <button type="submit" class="btn btn-primary pull-left" id="assign">Assign</button>
                                    @endif
                                </div>
                            </form>
                            <div class="box-body col-lg-8">
                                <div id="upload_peserta" style="display: none;">
                                    {!! Form::open(['url' => 'upload-user-assign','role' => 'form', 'class'=>'','files' => true,'id'=>'upload-form','data-submit'=>'noAjax']) !!}
                                        <div class="form-group form-group-default required col-lg-12">
                                            <a href="{{ url("format-excel") }}">Unduh Format .xlsx</a> 
                                            <label for="file-excel">Assign Peserta Baru Melalui File (.xlsx ) </label>
                                            <input type="hidden" name="id_project" id="id_project" class="form-control" value="{{ $data['info_project']['id'] }}">
                                            <input type="hidden" name="jatah" value="{{ $data['jatah'] }}"/>
                                            <input type="file" name="file" id="file-excel" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            {!! Form::submit('Assign',['class' => 'btn btn-primary pull-left', 'id' => 'create_user']) !!}
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @else
                            <h4><span class="alert alert-danger">Kuota Anda sudah habis. Anda tidak dapat assign peserta untuk saat ini.</span></h4>
                        @endif
                    </div>
                </div> 
            </div>
        </div>

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" type="text/css" media="all">
    <script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $('#metode').change(function(){
                if($('#metode').val() == 1){
                    $('#upload_peserta').show("fast");
                    $('#pilih_peserta').hide("fast");
                    $('#assign').hide("fast");
                }else if($('#metode').val() == 2){
                    $('#pilih_peserta').show("fast");
                    $('#assign').show("fast");
                    $('#upload_peserta').hide("fast");
                }
            });

            if($('#jlh_peserta_available').val() == 0){
                $('#upload_peserta').show("fast");
                $('#pilih_peserta').hide("fast");
                $('#assign').hide("fast");
            }
            //multiselect participant / select all
            $("#participant").select2();
            $("#select_all").click(function(){
                if($("#select_all").is(':checked') == true){
                    $("#participant > option").prop("selected",true);
                    $("#participant").trigger("change");
                }else{
                    $("#participant > option").prop("selected", false);
                    $("#participant").trigger("change");
                 }
            });
        });
    </script>
    @stop
