@extends('layouts.default_survey')

    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div>
        
    @stop
    
    @section('content')
        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}" id="id_user" />
        <input type="hidden" name="slug" value="{{ $slug }}" id="slug" />
        <input type="hidden" name="section" value="{{ $section }}" id="section" />
        <input type="hidden" name="section_name" value="{{ $section_name }}" id="section_name" />
        <input type="hidden" name="id_survey" value="{{ $id_survey }}" id="id_survey" />
        <div id="tool-container" class="container animated fadeIn">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-title animated fadeInDown">
                            <h6 class="left grey-text text-lighten-1">{{ $section_name}}</h6>
                            <a class="tooltipped" data-position="bottom" data-delay="20" data-tooltip="{!! $instruction !!}"><i class="material-icons">info</i></a>
                            <h1 id="stopwatch_question" style="display: none;">00:00:00</h1>
                            @if($timer_survey/60 != 0) <!-- kalau ada timer survey -->
                                <input type="hidden" name="timer_survey" value="{{ $timer_survey - $used_timer}}" id="timer_survey" />
                                <input type="hidden" name="timer_section" value="0" id="timer_section" />
                                <input type="hidden" name="timer_question" value="0" id="timer_question" />
                                <span class="grey-text text-lighten-1">Time Left: <span id="countdown_section" class="grey-text"></span></span>
                            @else <!-- kalau ga ada timer survey -->
                                <input type="hidden" name="timer_survey" value="0" id="timer_survey" />
                                @if($timer_section/60 != 0) <!-- kalau ada timer section -->
                                    <input type="hidden" name="timer_section" value="{{ $timer_section - $used_timer}}" id="timer_section" />
                                    <input type="hidden" name="timer_question" value="0" id="timer_question" />
                                    <span class="grey-text text-lighten-1">Time Left: <span id="countdown_section" class="grey-text"></span></span>
                                @else
                                    <input type="hidden" name="timer_section" value="0" id="timer_section" />
                                    <input type="hidden" name="timer_question" value="{{ $timer - $used_timer }}" id="timer_question" />
                                    @if($timer/60 != 0)
                                        <span class="grey-text text-lighten-1">Time Left: <span id="countdown_section" class="grey-text"></span></span>
                                    @endif
                                @endif
                                
                            @endif   
                        </div>  
                        <div class="card-content tool-question slides"> 
                            <?php $index_question = 0;?>
                            @foreach($questions as $question)
                                {{ Form::hidden('id_type_question', $id_question[$index_question], ['id' => 'id_type_question'])}}
                                {{ Form::hidden('is_mandatory', $question->is_mandatory, ['id' => 'is_mandatory'])}}
                                <div slide-id="{!!$index_question!!}" class="slide active">
                                    @if($question -> id_type_question == 1) 
                                        <div class="tool-statement">
                                            <p class="flow-text">{!! $content_question[$index_question] !!}</p>
                                        </div>
                                        <div class="tool-option multiple-choice" data-toggle="buttons">
                                            <ul class="clearfix">
                                                <?php $index_option = 0; $alpha = 'A'?>
                                                @foreach($option_answer[$index_question] as $option)

                                                    <li class=" <?php echo (is_numeric($option -> answer) ? ($section_name == 'Subtest 4' ? 'option-text' : 'option-text option-numeric') : ($option -> image != '' ? 'option-text option-img' : ' option-text')); ?> animated flipInX" onClick="getNext({!!$index_question!!})">
                                                        <label class="btn btn-tool">
                                                            <input type="radio" name="answer" value="{{ $option -> id}}" id="id_answer<?php echo $index_option;?>" autocomplete="off" class="">
                                                            <span class="order-alpha">{{$alpha}}</span>
                                                            <span class="option-item">
                                                                @if($option -> image == '')
                                                                    {!! $option -> answer !!}
                                                                @else
                                                                    <img src="/assets/image/{{$section_slug}}/{{$option -> image}}" />
                                                                @endif
                                                            </span> 

                                                        </label>
                                                    </li>

                                                    <?php $index_option++; $alpha++;?>
                                                @endforeach
                                            </ul>                                               
                                        </div>
                                        <div class="question-action-box">
                                            <div class="question-action" style="display:none;">
                                                <button class="btn waves-effect waves-light next" id="next-{!!$index_question!!}" onClick="saveCache({!!$id_question[$index_question]!!})">Next</button> 
                                                <span class="grey-text">or press <strong>ENTER</strong></span>
                                            </div>
                                        </div>
                                    @elseif($question -> id_type_question == 2)
                                        <div class="tool-statement">
                                            <p class="flow-text">{!! $question -> question !!}</p>
                                            <p class="info-question">You can choose more than one answer</p>
                                        </div>
                                        <div class="tool-option" data-toggle="buttons">
                                            <ul class=" option-text">
                                                <?php $index_option = 0; $alpha = 'A'?>
                                                @foreach($option_answer[$index_question] as $option)
                                                    <li class=" <?php echo (is_numeric($option -> answer) ? ($section_name == 'Subtest 4' ? 'option-text' : 'option-text option-numeric') : ($option -> image != '' ? 'option-text option-img' : ' option-text')); ?> animated flipInX" onClick="getNext({!!$index_question!!})">
                                                        <label class="btn btn-tool">
                                                            <input type="checkbox" name="answer" value="{{ $option -> id}}" id="id_answer<?php echo $index_option;?>" autocomplete="off">
                                                            <span class="order-alpha">{{$alpha}}</span>
                                                            <span class="option-item">
                                                                @if($option -> image == '')
                                                                    {!! $option -> answer !!}
                                                                @else
                                                                    <img src="/assets/image/{{$section_slug}}/{{$option -> image}}" />
                                                                @endif
                                                            </span> 
                                                        </label>
                                                    </li>

                                                    <?php $index_option++; $alpha++;?>
                                                @endforeach
                                            </ul>                                               
                                        </div>
                                        <div class="question-action-box">
                                            <div class="question-action" style="display:none;">
                                                <button class="btn waves-effect waves-light next" id="next-{!!$index_question!!}" onClick="saveCache({!!$question->id_question!!})">Next</button> 
                                                <span class="grey-text">or press <strong>ENTER</strong></span>
                                            </div>
                                        </div>
                                    @endif
                                                                       
                                </div>    

                            <?php $index_question++;?>            
                            @endforeach
                        </div>
                        <div class="card-action animated fadeInUp">
                            <div class="row">
                                <div class="tool-action col l6">
                                    <button id="prev" class="btn-floating waves-effect waves-light"><i class="material-icons">keyboard_arrow_left</i></button>
                                    <button id="skip" class="btn-floating waves-effect waves-light"><i class="material-icons">skip_next</i></button>
                                </div>
                                <div class="tool-progress col l3 right text-right">
                                    <div class="question-answered grey-text text-lighten-1">{!! $count !!} of {!! $total !!} answered</div>
                                    <div class="progress">
                                        <div class="determinate" style="width: {!! $count/$total*100 !!}%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>    
        <input type="hidden" name="length_question" value="{{ $index_question }}" id="length_question" />      

        <div class="modal fade slide-up disable-scroll" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close fa-fw"></i>
                            </button>
                            <span class="label fs-30">It's Over</span>
                        </div>
                        <div class="modal-body">
                            <br>
                            <center>
                                <h3><span class="semi-bold">Waktu pengerjaan telah habis. Anda akan dialihkan ke halaman subtest berikutnya</span></h3>
                            </center>
                        </div>       
                    </div>
                    <!-- /.modal-content -->
                </div>
            </div>
        </div>
    @stop


    @section('script')
        <script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo asset('assets/js/materialize.min.js'); ?>"></script>

        <script type="text/javascript">
            $.ajaxSetup({
               headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
            });
            //set all variable
            var id_type_question= ($('#id_type_question').val());
            var timer_survey    = $('#timer_survey').val();
            var timer_question  = $('#timer_question').val();
            var timer_section   = $('#timer_section').val();
            var id_user         = $('#id_user').val();
            var answer          = document.getElementsByName('answer');
            var length_answer   = answer.length;
            var length_question = $('#length_question').val();
            var slug            = $('#slug').val();
            var id_survey       = $('#id_survey').val();
            var count_survey    = $('#count_survey').val();
            var is_mandatory    = $('#is_mandatory').val();
            var section         = $('#section').val();
            var section_name    = $('#section_name').val();
            var i, j;


            //show next button
            $('.btn-tool>input[type=radio]').change(function () {
                    if ($(this).is(':checked')) {
                        $(".question-action").show().addClass('animated fadeInUp');
                        } else {
                        $(".question-action").hide('slow');
                    }
                }); 
            //aini
            function getNext(index){
                $('#next-'+index).fadeIn('medium');
            }

            //save to cache
            function saveCache(id_question){
                //tambahkan used timer ke temp jika survey ada timer
                if(timer_survey != 0 || timer_section != 0){
                    clearTimeout(t);
                    if(stop_minutes != 0){ //convert minutes to seconds
                        stop_seconds = stop_seconds + (stop_minutes*60);
                    }
                    if(stop_hours != 0){
                        stop_seconds = stop_seconds + (stop_hours*60*60);
                    }
                    $.ajax({
                        url : "save-temp",    
                        data: {survey_id: id_survey, question_id: id_question, user_id: id_user,  slug: slug, timer: stop_seconds, section: section},
                        type: "POST"/*,
                        success :
                        function(data){
                            window.location.reload()
                        }*/
                    });
                }
                clearTimeout(t);
                if(stop_minutes != 0){ //convert minutes to seconds
                    stop_seconds = stop_seconds + (stop_minutes*60);
                }
                if(stop_hours != 0){
                    stop_seconds = stop_seconds + (stop_hours*60*60);
                }

                var counta = 0;
                for(i = 0; i < length_answer; i++){
                    if(answer[i].checked){
                        $.ajax({
                            url : "put-to-cache",    
                            data: {id_option: answer[i].value, id_question: id_question, timer: stop_seconds, length_question: length_question, slug: slug, section: section, section_name: section_name, id_type_question: id_type_question},
                            type: "POST",
                            success :
                            function(data){
                                if(data.finish == 'next'){
                                    window.location.href = '/start-next-section/'+data.slug+'/'+data.new_section;
                                }else if(data.finish == 'finish'){
                                    window.location.href = '/go-finish/'+data.slug;
                                }
                            }
                        });
                    }else{
                        counta++;
                    }
                }

                //jika soal mandatory dan user tidak menjawab maka alert
                if(is_mandatory == 1 && counta == length_answer){ 
                        alert('Silahkan jawab terlebih dahulu');
                        return false;
                }
                
            }

                //set timer survey
                if(timer_survey != 0){
                    countdown_survey("countdown_survey", timer_survey/60, 0);
                    function countdown_survey( elementName, minutes, seconds ){
                        if(timer_survey != 0){
                            var element_s, endTime_s, hours_s, mins_s, msLeft_s, time_s;

                            function twoDigits( n ){
                                return (n <= 9 ? "0" + n : n);
                            }

                            function updateTimer(){
                                msLeft_s = endTime_s - (+new Date);
                                if ( msLeft_s < 1000 ){
                                    element_s.innerHTML = "It's over";
                                    $('#skip').hide();
                                    $('#next').hide();
                                    var i;                  
                                    $('#myModal').modal(); 
                                    $.ajax({
                                        url : "/test-time-out",    
                                        data: {id_survey: id_survey},
                                        type: "POST"
                                    });
                                    setTimeout(function(){
                                        $('#myModal').modal('hide')
                                        // window.location = "/survey/save-report/"+slug+"/"+section;
                                    }, 5000);
                                } else {
                                    time_s = new Date( msLeft_s );
                                    hours_s = time_s.getUTCHours();
                                    mins_s = time_s.getUTCMinutes();
                                    element_s.innerHTML = (hours_s ? hours_s + ':' + twoDigits( mins_s ) : mins_s) + ':' + twoDigits( time_s.getUTCSeconds() );
                                    setTimeout( updateTimer, time_s.getUTCMilliseconds() + 500 );
                                }
                            }

                            element_s = document.getElementById( elementName );
                            endTime_s = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                            updateTimer();
                        }
                        
                    }
                }

                //set timer section
                if(timer_section != 0){

                    countdown_section("countdown_section", timer_section/60, 0);
                    function countdown_section( elementName, minutes, seconds ){
                        if(timer_section != 0){
                            var element_s, endTime_s, hours_s, mins_s, msLeft_s, time_s;

                            function twoDigits( n ){
                                return (n <= 9 ? "0" + n : n);
                            }

                            function updateTimer(){
                                msLeft_s = endTime_s - (+new Date);
                                if ( msLeft_s < 1000 ){
                                    element_s.innerHTML = "It's over";
                                    $('#skip').hide();
                                    $('#next').hide();
                                    var i;                  
                                    $('#myModal').modal(); 
                                    $.ajax({
                                        url : "/survey/delete-temp-section/"+slug,    
                                        type: "GET"/*,
                                        success :
                                        function(data){
                                            window.location.reload()
                                        }*/
                                    });
                                    setTimeout(function(){
                                        $('#myModal').modal('hide')
                                        window.location = "/survey/save-report/"+slug+"/"+section;                                       
                                    }, 5000);
                                } else {
                                    time_s = new Date( msLeft_s );
                                    hours_s = time_s.getUTCHours();
                                    mins_s = time_s.getUTCMinutes();
                                    element_s.innerHTML = (hours_s ? hours_s + ':' + twoDigits( mins_s ) : mins_s) + ':' + twoDigits( time_s.getUTCSeconds() );
                                    setTimeout( updateTimer, time_s.getUTCMilliseconds() + 500 );
                                }
                            }

                            element_s = document.getElementById( elementName );
                            endTime_s = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                            updateTimer();
                        }
                        
                    }
                }

                //stopwatch_question
                var stopwatch_question = document.getElementById('stopwatch_question'),
                start = document.getElementById('start'),
                stop = document.getElementById('stop'),
                clear = document.getElementById('clear'),
                stop_seconds = 0, stop_minutes = 0, stop_hours = 0,
                t;
                function add() {
                    stop_seconds++;
                    if (stop_seconds >= 60) {
                        stop_seconds = 0;
                        stop_minutes++;
                        if (stop_minutes >= 60) {
                            stop_minutes = 0;
                            stop_hours++;
                        }
                    }
                    
                    stopwatch_question.textContent = (stop_hours ? (stop_hours > 9 ? stop_hours : "0" + stop_hours) : "00") + ":" + (stop_minutes ? (stop_minutes > 9 ? stop_minutes : "0" + stop_minutes) : "00") + ":" + (stop_seconds > 9 ? stop_seconds : "0" + stop_seconds);

                    timers();
                }
                function timers() {
                    t = setTimeout(add, 1000);
                }

                timers();


                //slide
                    
                var getslideHeight = $('.slide.active').height();
                
                $('.slides').css({
                    height: getslideHeight
                });
                
                function calcslideHeight() {
                    getslideHeight = $('.slide.active').height();
                    
                    $('.slides').css({
                        height: getslideHeight
                    });
                }
                
                
                var slideItem = $('.slide'),
                slideCurrentItem = slideItem.filter('.active');
                
                
        </script>
    @stop