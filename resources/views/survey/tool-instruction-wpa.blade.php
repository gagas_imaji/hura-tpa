@extends('layouts.default_survey')

	@section('content')
			<div class="container animated fadeIn" id="tool-container">
				<div class="row">
					<div class="col s12">
						<div class="card">
							<div class="card-content tool-instruction">
								<div class="row">
									<div class="col l12">
										<h5><strong>Instruksi</strong></h5>
										<h4 class="marginbottom20">Work Personality Analytics</h4>
										<p class="text-justify">
											Mulailah dengan membayangkan kondisi yang Anda pilih untuk direspon, contoh di rumah, kantor atau sosial. Pilihlah salah satu statement yang PALING menggambarkan diri Anda di kolom 'Sesuai' dan salah satu statement yang PALING TIDAK menggambarkan diri Anda di kolom 'Tak Sesuai'. Satu statement yang sama tidak bisa mewakili dua jawaban 'Sesuai' dan 'Tak Sesuai' secara bersamaan.
										</p>
										<p class="text-justify">
											Perhatikan contoh berikut:
										</p>
										<p class="text-center">
											<img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/simulasi/contoh-soal13.gif'); ?>">
										</p>
										<div class="info">
	                                        <span class="label label-warning" id="load">Pertanyaan sedang di proses</span>
	                                        <span class="label label-danger" style="display: none;" id="failed">Gagal memproses pertanyaan</span>
	                                        <div class="progress">
	                                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
	                                              aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:45%" id="myBar">
	                                            </div>
	                                        </div>
	                                        
	                                    </div>
										<p class="margintop40">
											<button class="btn waves-effect waves-light" id="start" disabled>
	                                            Saya Mengerti, Mulai Tes <i class="material-icons right">exit_to_app</i>
	                                        </button>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>										
			</div>
			<input type="hidden" name="slug" value="{!! $data['slug'] !!}" id="slug">
	        <input type="hidden" name="section" value="{!! $data['section'] !!}" id="section">
	        <input type="hidden" name="user_id" value="{!! $data['user_id'] !!}" id="user_id">
	@stop
	
	@section('script')
		<!-- JS -->
		<script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/materialize.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script>
		<script type="text/javascript">
	        $(document).ready(function(){
	            $(function () {
	                setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
	            });

	            var slug = $('#slug').val();
                var section = $('#section').val();
                var user_id = $('#user_id').val();
                var elem = document.getElementById("myBar");

                $.ajax({
                    url : "/load-soal-wpa/"+slug+'/'+section+'/'+user_id,
                    type: "GET", 
                    success :
                    function(finish){
                        if(finish == 'true'){
                            elem.style.width = '100%';
                            $('#start').prop('disabled', false);
                            $('.info').fadeOut('slow');
                        }else if(finish == 'false'){
                        	$('#load').hide();
                        	$('#failed').fadeIn();
                        }
                    }
                });

                $('#start').click(function(){
                    window.location.href = '/survey/'+slug+'/'+section+'/start';
                });
	        });
	    </script>
	@stop								