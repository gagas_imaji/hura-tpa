@extends('layouts.default')
	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/project">Kelola Project</a></li>
                <li class="active">Tambah Project Baru</li>
            </ol>
        </div>
	@stop
	@section('content')
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
                    	<div class="panel-title text-danger">
                    		<i class="fa fa-edit"></i> Edit Data Project
                    	</div> 
                    </div>
                    <div class="panel-body">
						<form method="post" enctype="multipart/form-data" action="/project-edit" id="project-edit" data-submit="noAjax">
							{!! csrf_field() !!}
						  	<div class="box-body">
								<input type="hidden" name="id" class="form-control" placeholder="Id" value="{{$data['id']}}" />
								<div class="form-group form-group-default col-lg-12 required">
									<label>Nama Project</label>
									<input type="text" name="name" class="form-control" placeholder="Nama" value="{{$data['name']}}" />
								</div>
								<div class="form-group form-group-default col-lg-12 required">
									<label>Deskripsi Project</label>
									<textarea name="description" class="form-control" placeholder="Deskripsi" >{{$data['description']}}</textarea>
								</div>
								<div class="form-group form-group-default col-lg-6 required">
									<label>Tanggal Awal</label>
									<input type="text" class="form-control" name="start_date" id="started" value="{{$data['start_date']}}">
								</div>
								<div class="form-group form-group-default col-lg-6 required">
									<label>Tanggal Akhir</label>
									<input type="text" class="form-control" name="end_date" id="ended" value="{{$data['end_date']}}"
								</div>
							</div>
							<div class="box-footer">
								<div class=" col-lg-12">
									<button type="submit" class="btn btn-primary pull-left">Simpan</button>

								</div>
							</div>
						</form>
                    	
                    </div>
				</div>
			</div>
		</div>
	    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" type="text/css" media="all">
	    <script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
	    <script type="text/javascript">
		        //set calendar
		    	var dateToday = new Date();
				var dates = $("#started, #ended").datepicker({
				    defaultDate: "+1w",
				    changeMonth: true,
				    numberOfMonths: 1,
				    minDate: dateToday/*,
	                onSelect: function(selectedDate) {
	                    var dateMin = $('#started').datepicker("getDate");
	                    var option = this.id == "started" ? "minDate" : "maxDate",
	                        date = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1);
	                    dates.not(this).datepicker("option", option, date);
	                }*/
				});
	    </script>
	@stop
