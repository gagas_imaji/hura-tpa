@extends('layouts.default_survey')

	@section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div>
		
	@stop
	
	@section('content')
        <div class="container animated fadeIn" id="tool-container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content tool-instruction">
                            <div class="row">
                                <div class="col l2 m4 offset-l5 offset-m4">
                                    <img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/example.png'); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="info">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                          aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width:55%" id="myBar">
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-10 col-md-offset-1 text-center" id="load-save">
                                    <h5>Jawaban Anda sedang diproses</h5>
                                    <span>Mohon untuk tidak menutup halaman browser Anda</span>
                                </div>
                                <div class="col-md-10 col-md-offset-1 text-center" id="thanks" style="display: none;">
                                    <h5>{!! $data['thankyou_text'] !!}</h5>
                                    <p><a href="/home">Silahkan kerjakan test berikutnya.</a></p>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>  
            </div>                                      
        </div>
        <input type="hidden" name="slug" value="{!! $data['slug'] !!}" id="slug" />
        <input type="hidden" name="section" value="{!! $data['section'] !!}" id="section" />
        <input type="hidden" name="category" value="{!! $data['category'] !!}" id="category" />
        <input type="hidden" name="id_user" value="{!! $data['id_user'] !!}" id="id_user" />
	@stop

    @section('script')
        <script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/js/materialize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });

                var slug = $('#slug').val();
                var section = $('#section').val();
                var id_user = $('#id_user').val();
                var category= $('#category').val();

                var elem = document.getElementById("myBar");
                var url;
                // localStorage.clear();

                if(category == 1 || category == 2){
                    url = '/survey/save-report/'+slug+'/'+id_user;
                }else if(category == 3){
                    url = '/survey/save-report-wpa/'+slug+'/'+section+'/'+id_user;
                }else if(category == 4){
                    url = '/survey/save-report-papi/'+slug+'/'+section+'/'+id_user;
                }
                $.ajax({
                    url : url,
                    type: "GET", 
                    success :
                    function(finish){
                        if(finish === 'true'){
                            localStorage.setItem(id_user+slug, 'done');
                            elem.style.width = '100%';
                            $('#home').prop('disabled', false);
                            $('.info').fadeOut('slow');
                            $('#load-save').fadeOut('slow');
                            $('#thanks').fadeIn('slow');
                        }
                    }
                });

                $('#home').click(function(){
                    window.close();
                });
            });

            window.onbeforeunload = function (e) {
                var slug = $('#slug').val();
                var id_user = $('#id_user').val();
                e = e || window.event;
            //     // For IE and Firefox prior to version 4
            //     if (e) {
            //         e.returnValue = 'Sure?';
            //     }

            //     // For Safari
            var local = localStorage.getItem(id_user+slug);
                if(local == 'done'){

                }else{
                    return 'Sure?';
                }
            };
        </script>
    @stop
