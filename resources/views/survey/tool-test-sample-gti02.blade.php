@extends('layouts.default_survey')

	@section('content')
		<div class="container animated fadeIn" id="tool-container">
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-title animated fadeInDown">
							<h6 class="left grey-text text-lighten-1">Contoh Soal Subtest 2</h6>
							<a class="tooltipped" data-position="bottom" data-delay="20" data-tooltip="Setiap pertanyaan mengenai siapa yang lebih berat atau lebih ringan, siapa lebih tinggi atau lebih pendek, dan perbandingan-perbandingan lainnya dengan orang disekitarnya."><i class="material-icons">info</i></a>
						</div>
						<div class="card-content tool-question slides">							
							
							<div slide-id="1" class="slide active">
								<div class="tool-statement">
									<p class="flow-text">Doni lebih cepat dari Toni<br>Toni lebih cepat dari Roni<br>Siapa yang terlamban?</p>
								</div>
								<div class="tool-option multiple-choice" data-toggle="buttons">
									<ul class="clearfix">
										<li class="option-text">
											<label class="btn btn-tool waves-effect waves-light opt-false">
												<input type="radio" name="options" id="A" autocomplete="off">
												<span class="order-alpha">a</span>
												<span class="option-item">Doni</span> 
											</label>
										</li>
										<li class="option-text">
											<label class="btn btn-tool waves-effect waves-light opt-false">
												<input type="radio" name="options" id="B" autocomplete="off">
												<span class="order-alpha">b</span>
												<span class="option-item">Toni</span> 
											</label>
										</li>
										<li class="option-text">
											<label class="btn btn-tool waves-effect waves-light opt-true">
												<input type="radio" name="options" id="C" autocomplete="off">
												<span class="order-alpha">c</span>
												<span class="option-item">Roni</span> 
											</label>
										</li>
									</ul>												
								</div>
								<div class="question-action-box text-center">
									<div class="question-action" style="display:none;">
										<button class="btn btn-enter waves-effect waves-light next" id="test_benar">Anda Benar, Lanjut</button> 
									</div>
									<div class="question-wrong alert red accent-2 white-text" style="display:none;">
									Pilihan jawaban SALAH. Silakan pilih jawaban lainnya.</div>
								</div>
							</div>
							
							<div slide-id="2" class="slide">
								<div class="tool-statement">
									<p class="flow-text">Sam lebih kaya dari Don<br>Tim lebih miskin dari Don<br>Siapa yang termiskin?</p>
								</div>
								<div class="tool-option multiple-choice" data-toggle="buttons">
									<ul class="clearfix">
										<li class="option-text">
											<label class="btn btn-tool waves-effect waves-light opt-false">
												<input type="radio" name="options" id="A" autocomplete="off">
												<span class="order-alpha">a</span>
												<span class="option-item">Don</span> 
											</label>
										</li>
										<li class="option-text">
											<label class="btn btn-tool waves-effect waves-light opt-false">
												<input type="radio" name="options" id="B" autocomplete="off">
												<span class="order-alpha">b</span>
												<span class="option-item">Sam</span> 
											</label>
										</li>
										<li class="option-text">
											<label class="btn btn-tool waves-effect waves-light opt-true">
												<input type="radio" name="options" id="C" autocomplete="off">
												<span class="order-alpha">c</span>
												<span class="option-item">Tim</span> 
											</label>
										</li>
									</ul>												
								</div>
								<div class="info">
                                    <span class="label label-warning" id="load">Pertanyaan sedang di proses</span>
                                    <span class="label label-danger" style="display: none;" id="failed">Gagal memproses pertanyaan</span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                          aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:45%" id="myBar">
                                        </div>
                                    </div>
                                </div>
								<div class="question-action-box text-center">
									<div class="question-action">
										<button class="btn-large btn-enter waves-effect waves-light animated bounceIn" id="start" disabled>Saya Mengerti, Mulai Tes <i class="material-icons right">exit_to_app</i></button>
									</div>
									<div class="question-wrong alert red accent-2 white-text" style="display:none;">
									Pilihan jawaban SALAH. Silakan pilih jawaban lainnya.</div>
								</div>
							</div>
															
						</div>							
					</div>
				</div>	
			</div>										
		</div>
		<input type="hidden" name="slug" value="{!! $data['slug'] !!}" id="slug">
        <input type="hidden" name="section" value="{!! $data['section'] !!}" id="section">
        <input type="hidden" name="user_id" value="{!! $data['user_id'] !!}" id="user_id">	
	@stop

	@section('script')		
		<!-- JS -->
		<script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/materialize.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script>
		
		<script type="text/javascript">

			$(document).ready(function() {

				//script load soal
				var slug = $('#slug').val();
                var section = $('#section').val();
                var user_id = $('#user_id').val();
                var elem = document.getElementById("myBar");

                $.ajax({
                    url : "/load-soal-tiki-lai/"+slug+'/'+section+'/'+user_id,
                    type: "GET", 
                    success :
                    function(finish){
                    	console.log(finish);
                        if(finish == 'true'){
                            elem.style.width = '100%';
                            $('#start').prop('disabled', false);
                            $('.info').fadeOut('slow');
                        }else if(finish == 'false'){
                            $('#load').hide();
                            $('#failed').fadeIn();
                        }
                    }
                });

                $('#start').click(function(){
                    window.location.href = '/survey/'+slug+'/'+section+'/start';
                });

                //script simulasi
				$('.btn-tool>input[type=radio]').change(function () {
					if ($('.btn-tool.opt-true>input[type=radio]').is(':checked')) {
						$(".btn-tool.opt-false").addClass("disabled");
						$(".question-action").show().addClass('animated fadeInUp');
						$(".question-wrong").hide();
						} else {
						$(".question-action").hide();
						$(".question-wrong").show().addClass('animated fadeInUp');
					}
				});	
				var getslideHeight = $('.slide.active').height();
				
				$('.slides').css({
					height: getslideHeight
				});
				
				function calcslideHeight() {
					getslideHeight = $('.slide.active').height();
					
					$('.slides').css({
						height: getslideHeight
					});
				}
				
				
				var slideItem = $('.slide'),
				slideCurrentItem = slideItem.filter('.active');
				
				$('.next').on('click', function(e) {
					e.preventDefault();
					
					var nextItem = slideCurrentItem.next();
					
					slideCurrentItem.removeClass('active');
					$('.tool-statement').removeClass('animated fadeIn');
					$('.option-text').removeClass('animated flipInX');
					$(".question-action").hide();
					
					if (nextItem.length) {
						
						slideCurrentItem = nextItem.addClass('active');
						slideCurrentItem.find('.option-text').addClass('animated flipInX');
						$(".btn-tool.opt-false").removeClass("disabled");
						$(".btn-tool.opt-false>input[type=radio]").addClass("active");
						
						} else {
						slideCurrentItem = slideItem.first().addClass('active');
						slideCurrentItem.find('.option-text').addClass('animated flipInX');
					}
					
					calcslideHeight();
				});
				
				$(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });			
			});
			
		</script>
	@stop