@extends('layouts.default_survey')

    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div> 
        
    @stop
    
    @section('content')
        <div class="container animated fadeIn" id="tool-container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content tool-instruction">
                            <div class="row">
                                <div class="col l2 m4 offset-l5 offset-m4">
                                    <img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/example.png'); ?>">
                                </div>
                                <div class="col l12">
                                    <h4>{!! $data['title'] !!}</h4>
                                    <p>{!! $data['section_name'] !!}</p>
                                    <p class="flow-text"><strong>Instruksi</strong></p>
                                    <p class="description">
                                       {!! $data['section_instruction'] !!}
                                    </p>
                                <p class="margintop40"><a href="/survey/{{ $data['slug'] }}/{{ $data['section'] }}/start" class="btn waves-effect waves-light animsition-link" id="start">simulasi</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>                                      
        </div>

    @stop

    @section('script')
        <script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <!-- <script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script> -->
        <script type="text/javascript">
            $(document).ready(function(){
                $(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });
            });
        </script>
    @stop