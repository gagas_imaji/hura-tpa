@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/project">Project</a></li>
                <li><a href="/project/detail/{{$project->id}}">{{$project->name}}</a></li>
                <li class="active">{!! $survey->title !!}</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-md-12">
    			<div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"> 
                            <a href="#detail-tab" data-toggle="tab">Detail Tools</a>
                        </li>
                        <!-- <li>
                            <a href="#update-tab" data-toggle="tab">Update</a>
                        </li>
                        <li><a href="#question-tab" data-toggle="tab">Questions</a>
                        </li> -->
                    </ul>
                    <div class="tab-content"> 
                        <div class="tab-pane fade in active col-md-8" id="detail-tab">
                            <div class="panel panel-default m-t-10">
                                <div class="panel-heading">
                                    <strong>Detail </strong>Tools
                                </div>
                                <div class="panel-body">
                                   
                                    <div class="responsive">
                                        <table class="table table-stripped table-hover show-table">
                                            <tbody>
                                                <tr>
                                                    <th>{!!trans('messages.title')!!}</th>
                                                    <td>{!!$survey->title!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Start Date</th>
                                                    <td>{!!toWord($survey->start_date)!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>End Date</th>
                                                    <td>{!!$survey->end_date!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Timer Survey</th>
                                                    <td>{!!gmdate("H:i:s", $survey->timer)!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Description</th>
                                                    <td>{!!$survey->description!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Instruction</th>
                                                    <td>{!!$survey->instruction!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Thank You Text</th>
                                                    <td>{!!$survey->thankyou_text!!}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-heading">
                                    <strong>Detail </strong>Section
                                </div>
                                <div class="panel-body">
                                    <div class="responsive">   
                                        <table class="table table-hover show-table">
                                                <tr>
                                                    <th>Name Section</th>
                                                    <th>Instruction</th>
                                                    <th>Limit Question</th>
                                                    <th>Timer (H:i:s)</th>
                                                </tr>
                                            <tbody> 
                                                @foreach($section as $sections)
                                                    <tr>
                                                        <td>{!! $sections -> name !!}</td>
                                                        <td>{!! $sections -> instruction !!}</td>
                                                        <td>{!! $sections -> limit !!}</td>
                                                        <td>{!! gmdate("H:i:s", $sections -> timer) !!}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="tab-pane fade in col-md-8" id="update-tab">
                            <div class="panel panel-default m-t-10">
                                <div class="panel-heading">
                                    <strong>Update </strong>Detail Tools
                                </div>
                                <div class="panel-body">
                                    {!! Form::model($survey,['method' => 'POST','route' => ['survey.detail-update',$survey->id] ,'class' => 'survey-detail-form','id' => 'survey-detail-form','data-no-form-clear' => 1, 'data-submit'=>'noAjax']) !!}
                                    {!! csrf_field() !!}
                                    <div class="row">
                                        <div class="col-md-12">
                                        
                                            <div class="form-group form-group-default ">
                                                {!! Form::label('title',trans('messages.title'),[])!!}
                                                {!! Form::input('text','title',$survey->title,['class'=>'form-control','placeholder'=>trans('messages.title')])!!}
                                            </div>
                                            <div class="form-group form-group-default ">
                                                {!! Form::label('description',trans('messages.description'))!!}
                                                {!! Form::textarea('description',$survey->description,['class'=>'form-control','placeholder'=>trans('messages.description')])!!}
                                            </div>
                                            <div class="form-group form-group-default ">
                                                {!! Form::label('instruction','Instruction')!!}
                                                {!! Form::textarea('instruction',$survey->instruction,['class'=>'form-control','placeholder'=>trans('messages.instruction')])!!}
                                            </div>
                                            <div class="form-group form-group-default ">
                                                {!! Form::label('thankyou_text','Thank You Text')!!}
                                                {!! Form::textarea('thankyou_text',$survey->thankyou_text,['class'=>'form-control','placeholder'=>'Thank you text'])!!}
                                            </div>
                                            <div class="form-group form-group-default ">
                                                {!! Form::label('start_date','Start Date')!!}
                                                {!! Form::input('text','start_date',$survey->start_date,['class'=>'form-control','placeholder'=>'Start Date', 'id'=>'started'])!!}
                                            </div>
                                            <div class="form-group form-group-default ">
                                                {!! Form::label('end_date','End Date')!!}
                                                {!! Form::input('text','end_date',$survey->end_date,['class'=>'form-control','placeholder'=>'End Date', 'id'=>'ended'])!!}
                                            </div>
                                            <?php $i = 0;?>
                                            @foreach($section as $section)
                                                <div class="form-group form-group-default col-md-12">
                                                    {!! Form::hidden('section_id[]',$section->id, [])!!}
                                                    <div class="form-group form-group-default col-md-9">
                                                        {!! Form::label('section_name','Section Name '.($i+1)) !!}
                                                        {!! Form::input('text','section_name[]',$section->name, ['class'=>'form-control','placeholder'=>'Section Name', 'id'=>'section_name', 'required' => 'required'])!!}
                                                    </div>
                                                    <div class="form-group form-group-default col-md-3">
                                                        {!! Form::label('section_limit','Limit Question') !!}
                                                        {!! Form::number('section_limit[]',$section->limit, ['class'=>'form-control','placeholder'=>'Limit Question', 'id'=>'section_limit', 'required' => 'required'])!!}
                                                    </div>
                                                    <div class="form-group form-group-default col-md-9">
                                                        {!! Form::label('section_instruction','Section Instruction') !!}
                                                        {!! Form::textarea('section_instruction[]',$section->instruction, ['class'=>'form-control','placeholder'=>'Limit Question', 'id'=>'section_instruction', 'required' => 'required'])!!}
                                                    </div>
                                                    <div class="form-group form-group-default col-md-3">
                                                        {!! Form::label('section_timer','Timer (seconds)') !!}
                                                        {!! Form::number('section_timer[]',$section->timer, ['class'=>'form-control','placeholder'=>'Limit Question', 'id'=>'section_timer', 'required' => 'required'])!!}
                                                    </div>
                                                </div>
                                                <?php $i++;?>
                                            @endforeach
                                            {!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-right', 'id' => 'update_survey']) !!}
                                        </div>
                                    </div>                                        
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade in" id="question-tab">
                            <div class="panel panel-default m-t-10">
                                <div class="panel-heading">
                                    <strong>List All </strong>Question

                                    @if(Entrust::can('create-question'))
                                    <div class="additional-btn">

                                        <form name="myForm" id="myForm" action="/question/create" method="post" enctype="multipart/form-data" id="import" data-submit="noAjax">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="id_survey" value="{{ $survey->id }}">
                                            <input type="submit" name="add_new" value="Add New" class="btn btn-sm btn-primary" id="add_new_question">
                                        </form>
                                        
                                    </div>
                                    @endif

                                </div>
                                <div class="panel-body full">
                                    @include('common.datatable',['table' => $table_data['question-table']])
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div> 
            </div>
        </div>
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" type="text/css" media="all">
        <script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script type="text/javascript">
            var dateToday = new Date();
            var dates = $("#started, #ended").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                minDate: dateToday/*,
                onSelect: function(selectedDate) {
                    var dateMin = $('#started').datepicker("getDate");
                    var option = this.id == "started" ? "minDate" : "maxDate",
                        date = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1);
                    dates.not(this).datepicker("option", option, date);
                }*/
                
            });
        </script>
    @stop