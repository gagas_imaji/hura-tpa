@extends('layouts.default_survey')

    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div>
        
    @stop
    
    @section('content')
        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}" id="id_user" />
        <input type="hidden" name="id_survey" value="{{ $slug }}" id="id_survey" />
        <div class="container animated fadeIn" id="tool-container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content tool-instruction">
                            <div class="row">
                                <div class="col l2 m4 offset-l5 offset-m4">
                                    <img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/example.png'); ?>">
                                </div>
                                <div class="col l12">
                                    <h4>{!! $info['title'] !!}</h4>
                                    <p class="description">
                                       {!! $info['description'] !!}
                                    </p>
                                <p class="margintop40"><a href="/survey/{{ $slug }}/{{ $section }}/instruction" class="btn btn-primary start" id="start" data-target="1">Lanjutkan</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>                                      
        </div>

    @stop

    @section('script')
        <script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });
            });
        </script>
    @stop