@extends('layouts.default')

    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div>
        
    @stop
    
    @section('content')    
        <div class="row">        
            <input type="hidden" name="id_user" value="{{ Auth::user()->id }}" id="id_user" />
            <input type="hidden" name="slug" value="{{ $slug }}" id="slug" />
            <input type="hidden" name="section" value="{{ $section }}" id="section" />
            <input type="hidden" name="section_name" value="{{ $section_name }}" id="section_name" />
            <input type="hidden" name="id_survey" value="{{ $id_survey }}" id="id_survey" />
            <input type="hidden" name="timer" value="{{ $timer }}" />

            <div class="col-lg-11 content-question">
                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <div class="row"> 
                            <div class="header-left">
                                <strong>Blank in Paragraph</strong> 
                                <p>&nbsp</p>                                   
                            </div>
                            <div class="header-right pull-right">
                                @if($timer_survey/60 != 0)
                                    <input type="hidden" name="timer_survey" value="{{ $timer_survey - $used_timer}}" id="timer_survey" />
                                    <strong>
                                        Time Left : <span id="countdown_survey"></span>
                                    </strong>

                                    <div>
                                        <a class="btn pop" rel="popover" href="jacascript:void()" data-original-title="Instruction" data-container="body" data-content="{!! $instruction !!}"  data-placement="bottom"><i class="fa fa-gears fa-fw"></i> Instruction</a>
                                    </div> 

                                    <input type="hidden" name="timer_question" value="0" id="timer_question" />
                                    <h1 id="stopwatch_question"  style="display: none;">00:00:00</h1>
                                @else
                                    <input type="hidden" name="timer_survey" value="{{ $timer_survey - $used_timer}}" id="timer_survey" />

                                    <h1 id="stopwatch_question" style="display: none;">00:00:00</h1>
                                    <div>
                                        <a class="btn pop" rel="popover" href="jacascript:void()" data-original-title="Instruction" data-container="body" data-content="{!! $instruction !!}"  data-placement="bottom"><i class="fa fa-gears fa-fw"></i> Instruction</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="panel-body body-detail">
                        <div class="row">  
                            <div class="survey-question">
                                <?php $i = 0;?>
                                @foreach($option_answer as $option)
                                    <input type="text" name="option" value="{{ $option['answer'] }}" disabled="disabled" style="width:140px; text-align: center;" />
                                    <?php $i++;?>
                                @endforeach
                            </div> <br>
                            <?php $j = 0;?>
                            <p style="text-align: justify;">
                                <div class="survey-question">
                                    @foreach($questions as $question)
                                        {{ Form::hidden('id_questions', $question->id_question, ['id' => 'id_question'.$j])}}
                                        {{ Form::hidden('id_type_question[]', $question->id_type_question, ['id' => 'id_type_question'])}}
                                        {{ Form::hidden('is_mandatory[]', $question->is_mandatory, ['id' => 'is_mandatory'])}}
                                                {!! $question -> question !!} ({!! $j+1 !!}) <input type="text" name="answers" id="id_answer{{$j}}" style="width: 100px; border-top: 0px; border-left: 0px; border-right: 0px; text-align: center;" />
                                            
                                            <?php $j++;?>
                                    @endforeach
                                </div>
                            </p>
                            <input type="hidden" name="jlh_soal" value="{{ $j }}" id="jlh_soal" />
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                                <center>
                                    <button class="btn btn-primary" id="submit">NEXT</button>
                                </center>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                //set all variable
                var id_type_question= ($('#id_type_question').val());
                var id_user         = $('#id_user').val();
                var slug            = $('#slug').val();
                var is_mandatory    = $('#is_mandatory').val();
                var section         = $('#section').val();
                var section_name    = $('#section_name').val();
                var timer_survey    = $('#timer_survey').val();
                var id_question     = $('#id_question').val();
                var id_survey       = $('#id_survey').val();
                var i, j;

                //popover instruction
                $('a[rel=popover]').popover();
                
                if(timer_survey != 0){

                    //set timer survey
                    countdown_survey("countdown_survey", timer_survey/60, 0);
                    function countdown_survey( elementName, minutes, seconds ){
                        if(timer_survey != 0){
                            var element_s, endTime_s, hours_s, mins_s, msLeft_s, time_s;

                            function twoDigits( n ){
                                return (n <= 9 ? "0" + n : n);
                            }

                            function updateTimer(){
                                msLeft_s = endTime_s - (+new Date);
                                if ( msLeft_s < 1000 ){
                                    element_s.innerHTML = "It's over";
                                    $('#skip').hide();
                                    $('#next').hide();
                                    var i;                  
                                    $('#myModal').modal();              
                                    $.ajax({
                                        url : "/test-time-out",    
                                        data: {id_survey: id_survey},
                                        type: "POST"
                                    });
                                    setTimeout(function(){
                                        $('#myModal').modal('hide')
                                        // window.location = "/survey/save-report/"+slug+"/"+section;
                                    }, 5000);
                                } else {
                                    time_s = new Date( msLeft_s );
                                    hours_s = time_s.getUTCHours();
                                    mins_s = time_s.getUTCMinutes();
                                    element_s.innerHTML = (hours_s ? hours_s + ':' + twoDigits( mins_s ) : mins_s) + ':' + twoDigits( time_s.getUTCSeconds() );
                                    setTimeout( updateTimer, time_s.getUTCMilliseconds() + 500 );
                                }
                            }

                            element_s = document.getElementById( elementName );
                            endTime_s = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                            updateTimer();
                        }
                        
                    }
                }


                $('#submit').click(function(){
                    if(timer_survey != 0){
                        clearTimeout(t);
                        if(stop_minutes != 0){ //convert minutes to seconds
                            stop_seconds = stop_seconds + (stop_minutes*60);
                        }
                        if(stop_hours != 0){
                            stop_seconds = stop_seconds + (stop_hours*60*60);
                        }
                        $.ajax({
                            url : "save-temp",    
                            data: {survey_id: id_survey, question_id: id_question, user_id: id_user,  slug: slug, timer: stop_seconds, section: section},
                            type: "POST"
                        });
                    }
                    var i;
                    clearTimeout(t);
                    if(stop_minutes != 0){ //convert minutes to seconds
                        stop_seconds = stop_seconds + (stop_minutes*60);
                    }
                    if(stop_hours != 0){
                        stop_seconds = stop_seconds + (stop_hours*60*60);
                    }

                    var length_soal = $('#jlh_soal').val();
                    var answers     = [];
                    var questions   = [];
                    var id_questions= document.getElementsByName('id_questions');
                    var id_answer   = document.getElementsByName('answers');
                    var counta = 0;
                    for(i = 0; i < length_soal; i++){
                        questions[i]    = id_questions[i].value;
                        if(id_answer[i].value != ''){
                            answers[counta]      = id_answer[i].value;
                            counta++;
                        }                        
                    }
                    //jika soal mandatory dan user tidak menjawab maka alert
                    if(is_mandatory == 1 && answers.length < length_soal){ 
                        alert('Silahkan jawab semua pertanyaan');
                    }

                    $.ajax({
                        url : "next-question",    
                        data: {id_user: id_user, id_answer: answers, slug: slug, id_type_question: id_type_question, id_question: questions, timer: 0, section: section, section_name: section_name},
                        type: "POST",
                        success :
                        function(data){
                            window.location.href='/survey/save-report/'+slug+'/'+section
                        }
                    });
                });

                //stopwatch_question
                var stopwatch_question = document.getElementById('stopwatch_question'),
                start = document.getElementById('start'),
                stop = document.getElementById('stop'),
                clear = document.getElementById('clear'),
                stop_seconds = 0, stop_minutes = 0, stop_hours = 0,
                t;
                function add() {
                    stop_seconds++;
                    if (stop_seconds >= 60) {
                        stop_seconds = 0;
                        stop_minutes++;
                        if (stop_minutes >= 60) {
                            stop_minutes = 0;
                            stop_hours++;
                        }
                    }
                    
                    stopwatch_question.textContent = (stop_hours ? (stop_hours > 9 ? stop_hours : "0" + stop_hours) : "00") + ":" + (stop_minutes ? (stop_minutes > 9 ? stop_minutes : "0" + stop_minutes) : "00") + ":" + (stop_seconds > 9 ? stop_seconds : "0" + stop_seconds);

                    timers();
                }

                function timers() {
                    t = setTimeout(add, 1000);
                }

                timers();
            });
    </script>
    @stop