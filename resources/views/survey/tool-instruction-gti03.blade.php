@extends('layouts.default_survey')

	@section('content')
		<div class="container animated fadeIn" id="tool-container">
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content tool-instruction">
							<div class="row">
								<div class="col l12">
									<h5><strong>Instruksi</strong></h5>
									<h4>Learning Agility Index</h4>
									<p class="marginbottom20">Subtest 3</p>
									<p class="text-justify">
										Pada saat mengerjakan tes ini, Anda harus <strong>mengingat urutan huruf</strong>.
									</p>
									<p class="text-justify">
										Perhatikan setiap kotak terdiri dari tiga buah huruf. Ingat selalu urutan huruf itu. Kemudian pilihlah satu huruf terjauh selisihnya dari huruf yang terletak ditengah.
									</p>
									<p class="text-justify">
										Perhatikan contoh berikut:
									</p>
									<p class="text-center">
										<img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/simulasi/contoh-soal3a.png'); ?>">
									</p>
									<p class="text-justify">
										Jika diurutkan secara benar maka huruf B ada diurutan pertama, kemudian huruf E dan terakhir huruf I berdasarkan urutan hurufnya.
									</p>
									<p class="text-justify">
										Tugas Anda adalah mencari selisih yang terjauh dari huruf yang ada di tengah (huruf E) dengan huruf yang ada di sebelah kiri (huruf B) atau dengan huruf yang ada di sebelah kanan (huruf I).
									</p>
									<p class="text-center">
										<img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/simulasi/contoh-soal3.gif'); ?>">
									</p>
									<p class="text-justify">
										Jawaban yang benar adalah <strong>huruf I</strong> karena memiliki selisih yang lebih besar (3 huruf) yaitu huruf F;G;H. Sedangkan antara huruf B dan huruf E hanya memiliki selisih 2 huruf C dan D. Oleh karena itu pilihlah kotak I.
									</p>
									<div class="info">
                                        <span class="label label-warning" id="load">Sistem sedang menyimpan jawaban Anda</span>
                                        <span class="label label-danger" style="display: none;" id="failed">Gagal menyimpan jawaban, silahkan refresh halaman ini untuk memproses ulang</span>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                              aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:45%" id="myBar">
                                            </div>
                                        </div>
                                    </div>

									<p class="margintop40">
										<button class="btn waves-effect waves-light" id="start" disabled>
                                            Coba Contoh Soal <i class="material-icons right">exit_to_app</i>
                                        </button>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>										
		</div>
		<input type="hidden" name="slug" value="{!! $data['slug'] !!}" id="slug">
        <input type="hidden" name="section" value="{!! $data['section'] !!}" id="section">
        <input type="hidden" name="has_result" value="{!! $data['has_result'] !!}" id="has_result">
        <input type="hidden" name="old_section" value="{!! $data['old_section'] !!}" id="old_section">
	@stop
	
	@section('script')
		<!-- JS -->
		<script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/materialize.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script>

		<script type="text/javascript">

            $(document).ready(function(){
                $(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });

                var slug = $('#slug').val();
                var section = $('#section').val();
                var has_result = $('#has_result').val();
                var old_section = $('#old_section').val();
                var elem = document.getElementById("myBar");

                if(has_result == 'yes'){
	                $.ajax({
	                    url : "/survey/save-cache-to-db/"+slug+'/'+old_section,
	                    type: "GET", 
	                    success :
	                    function(finish){
	                    	console.log(finish);
	                        if(finish == 'true'){
	                            elem.style.width = '100%';
	                            $('#start').prop('disabled', false);
	                            $('.info').fadeOut('slow');
	                        }else if(finish == 'false'){
	                            $('#load').hide();
	                            $('#failed').fadeIn();
	                        }
	                    }
	                });
	            }else{
                	elem.style.width = '100%';
                    $('#start').prop('disabled', false);
                    $('.info').fadeOut('slow');
                }

                $('#start').click(function(){
                    window.location.href = '/survey/'+slug+'/'+section+'/tool-test-sample-gti03';
                });
            });
        </script>
	@stop
									