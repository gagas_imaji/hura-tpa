@extends('layouts.default')
    @foreach($question as $question)
    	@section('breadcrumb')
    		<div id="breadcrumb">
                <ol class="breadcrumb">
                    <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                    <li><a href="/survey">Survey</a></li>
                    <li><a href="/survey/{{$question->survey_id}}">{!! $question->title !!}</a></li>
                    <li class="active">Detail Question</li>
                </ol>

            </div>
    	@stop
    	
    	@section('content')
    		<div class="row">
                <div class="col-md-12">
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#detail-question-tab" data-toggle="tab">Detail Question</a>
                            </li>
                            <li>
                                <a href="#update-tab" data-toggle="tab">Update Question</a>
                            </li>
                        </ul>
                        <div class="tab-content col-md-6">
                            <div class="panel panel-default m-t-10" id="detail-question-tab">
                                <div class="panel-heading">
                                    <strong>Detail</strong> Question
                                </div>
                                <div class="panel-body full">
                                    
                                    <div class="responsive">
                                        <table class="table table-stripped table-hover show-table">
                                            <tbody>
                                                <tr>
                                                    <th>Type Question</th>
                                                    <td>{!!$question->type!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Section</th>
                                                    <td>{!!toWord($question->section)!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Question</th>
                                                    <td>{!!$question->question!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Random</th>
                                                    @if($question->is_random == 1)
                                                        <td>Yes</td>
                                                    @else
                                                        <td>No</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th>Timer</th>
                                                    <td>{!!$question->timer!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Mandatory</th>
                                                    @if($question->is_mandatory == 1)
                                                        <td>Yes</td>
                                                    @else
                                                        <td>No</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th>Point</th>
                                                    <td>{!!$question->point!!}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content col-md-6">
                            <div class="panel panel-default m-t-10" id="detail-question-tab">
                                <div class="panel-heading">
                                    <strong>Option </strong>Answer
                                </div>
                                <div class="panel-body full">
                                    
                                    <div class="responsive">
                                        <table class="table table-stripped table-hover show-table">
                                            <tbody>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Answer</th>
                                                    <th>Image</th>
                                                    <th>Point</th>
                                                </tr>
                                                <?php $no = 1; ?>
                                                @foreach($option_answer as $option)
                                                    <tr>
                                                        <td>{!! $no !!}</td>
                                                        <td>{!! $option['answer'] !!}</td>
                                                        <td>{!! $option['image'] !!}</td>
                                                        <td>{!! $option['point'] !!}</td>

                                                    </tr>
                                                <?php $no++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" type="text/css" media="all">
            <script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
            <script type="text/javascript">
                var dateToday = new Date();
                var dates = $("#started, #ended").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    minDate: dateToday/*,
                    onSelect: function(selectedDate) {
                        var dateMin = $('#started').datepicker("getDate");
                        var option = this.id == "started" ? "minDate" : "maxDate",
                            date = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1);
                        dates.not(this).datepicker("option", option, date);
                    }*/
                    
                });
            </script>
        @stop
    @endforeach