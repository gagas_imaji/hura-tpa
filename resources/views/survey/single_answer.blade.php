@extends('layouts.default')

	@section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div>
		
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-11">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="header-left">
                                <strong>Employee Engagement Tools</strong>
                                <div class="section-survey"><p>Section 1: The Work Self</p></div>
                                
                            </div>
                            <div class="header-right">
                                Time Left : 05:49
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        <div class="row">
                        <center>
                            <div class="survey-question"><h3>1.  How long have you been working for the company? *</h3></div>
                            <div class="survey-option">
                                <p class="radio-survey"><input type="radio" name="answer" value="0"> 2 years or less</p>
                                <p class="radio-survey"><input type="radio" name="answer" value="1"> 3 to 5 years</p>
                                <p class="radio-survey"><input type="radio" name="answer" value="0"> 6 to 10 years</p>
                                <p class="radio-survey"><input type="radio" name="answer" value="0"> 11 to 15 years</p>
                                <p class="radio-survey"><input type="radio" name="answer" value="0"> 16 or more years</p>
                            </div>
                        </center>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="footer-left">
                                <p class="question-answered"> 3 of 10 answered</p><!-- Jumlah soal terjawab-->
                                <div class="status-bar">
                                    <progress value="3" max="10">></progress>
                                </div> <!-- Progres bar -->
                            </div>
                            <div class="footer-right">
                                <input type="submit" name="prev" value="PREVIOUS" class="btn" />
                                <input type="submit" name="next" value="NEXT" class="btn" />
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
	@stop