@extends('layouts.default_survey')

	@section('content')
		<div class="container animated fadeIn" id="tool-container">
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content tool-instruction">
							<div class="row">
								<div class="col l12">
									<h5><strong>Instruksi</strong></h5>
									<h4>Tes Intelegensi Kolektif Indonesia</h4>
									<p class="marginbottom20">Abstraksi Non Verbal</p>
									<p class="text-justify">
										Dalam tes ini setiap soal terdiri dari 10 gambar. Empat gambar pada bagian atas soal menunjukkan kesamaan dalam suatu hal atau segi tertentu. Dua diantara enam gambar pada baris paling bawah menunjukkan kesamaan dengan keempat gambar diatasnya.
									</p>
									<p class="text-justify">
										Carilah kedua gambar yang dimaksudkan itu. Apabila kedua gambar itu sudah ditemukan, pilih kotak yang berisi gambar yang sesuai tersebut.
									</p>
									<p class="text-justify">
										Perhatikan contoh berikut:
									</p>
									<p class="text-center">
										<img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/simulasi/contoh-soal11.gif'); ?>">
									</p>
									<p class="text-justify">
										Pada contoh di atas dapat dilihat bahwa 4 gambar yang pertama semuanya adalah segi empat atau bujur sangkar. Dari 6 gambar dibawahnya, hanya gambar pada kotak pertama dan kotak keenam saja yang menunjukkan kesamaan itu, yaitu berupa segi empat atau bujur sangkar. Oleh karena itu pilihlah kotak pertama dan kotak keenam. 
									</p>
									<p class="text-justify">
										Perhatikan contoh lainnya:
									</p>
									<p class="text-center">
										<img class="marginbottom20 responsive-img" src="<?php echo asset('assets/image/simulasi/contoh-soal12.gif'); ?>">
									</p>
									<p class="text-justify">
										Keempat gambar paling atas semuanya dihitamkan dengan cara yang sama. Diantara 6 gambar dibawahnya, hanya gambar pada kotak ketiga dan kelima yang dihitamkan dengan cara yang sama. Oleh karena itu pilihlah kotak ketiga dan kelima.
									</p>
									<p class="text-justify">
										<strong>Waktu : 12 menit</strong>
									</p>
									<div class="info">
                                        <span class="label label-warning" id="load">Sistem sedang menyimpan jawaban Anda</span>
                                        <span class="label label-danger" style="display: none;" id="failed">Gagal menyimpan jawaban, silahkan refresh halaman ini untuk memproses ulang</span>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                              aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:45%" id="myBar">
                                            </div>
                                        </div>
                                    </div>
									<p class="margintop40">
										<button class="btn waves-effect waves-light" id="start" disabled>
                                            Coba Contoh Soal <i class="material-icons right">exit_to_app</i>
                                        </button>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>										
		</div>
		<input type="hidden" name="slug" value="{!! $data['slug'] !!}" id="slug">
        <input type="hidden" name="section" value="{!! $data['section'] !!}" id="section">
        <input type="hidden" name="has_result" value="{!! $data['has_result'] !!}" id="has_result">
        <input type="hidden" name="old_section" value="{!! $data['old_section'] !!}" id="old_section">
	@stop
	
	@section('script')
		<!-- JS -->
		<script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/materialize.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script>

		<script type="text/javascript">

            $(document).ready(function(){
                $(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });

                var slug = $('#slug').val();
                var section = $('#section').val();
                var has_result = $('#has_result').val();
                var old_section = $('#old_section').val();
                var elem = document.getElementById("myBar");

                if(has_result == 'yes'){
	                $.ajax({
	                    url : "/survey/save-cache-to-db/"+slug+'/'+old_section,
	                    type: "GET", 
	                    success :
	                    function(finish){
	                    	console.log(finish);
	                        if(finish == 'true'){
	                            elem.style.width = '100%';
	                            $('#start').prop('disabled', false);
	                            $('.info').fadeOut('slow');
	                        }else if(finish == 'false'){
	                            $('#load').hide();
	                            $('#failed').fadeIn();
	                        }
	                    }
	                });
	            }else{
                	elem.style.width = '100%';
                    $('#start').prop('disabled', false);
                    $('.info').fadeOut('slow');
                }

                $('#start').click(function(){
                    window.location.href = '/survey/'+slug+'/'+section+'/tool-test-sample-tiki04';
                });
            });
        </script>
	@stop								