@extends('layouts.default')
    @section('inline-css')
        <style type="text/css">
            .modal .modal-body {
                padding-top: 25px;
            }
            
        </style>
    @stop
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/project">Kelola Project</a></li>
                <li class="active">List Tools</li>
            </ol>

        </div> 
        
    @stop
    
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>List Semua</strong> Tools
                    
                    </div> 
                    <div class="panel-body full">
                        @include('common.datatable',['table' => $table_data['survey-table']])
                    </div>
                </div>
            </div>
        </div>
    @stop