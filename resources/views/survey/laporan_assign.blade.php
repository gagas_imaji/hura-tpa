@extends('layouts.default')
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/project">Kelola Project</a></li>
                <li class="active">{!! $info_project['name'] !!}</li>
            </ol>
        </div>
    @stop 
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><i class="fa fa-user fa-fw"></i>  Laporan Assign Peserta {!! $info_project['name'] !!}</strong>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-striped table-bordered" id="list-peserta-project">
                            <thead>
                              <tr class="headings">
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Status Assign</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if($result_assign)
                                    @foreach($result_assign as $row)
                                        <tr>
                                            <td>{!! $row['email'] !!}</td>
                                            <td>
                                                @if($row['result'] == true)
                                                    <span class='label label-success'>Berhasil</span>
                                                @else
                                                    <span class='label label-success'>Gagal</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

        $(document).ready(function() {
            $('#list-peserta-project').DataTable();

        });
        </script>
    @stop