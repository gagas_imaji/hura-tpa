@extends('error_layout.default')
@section('content')
<div class="container">
            <div class="row">
                <div class="col s8 offset-s2">
                    <div class="card">
                        <div class="card-panel center">
                            <h4 class="teal-text">Laporan Gangguan Terkirim</h4>
                            <p>Terimakasih telah mengirimkan laporan gangguan yang telah Anda alami saat menggunakan aplikasi kami. Tim Developer kami akan segera menindaklanjuti laporan tersebut.</p>
                            <p>Jika Anda memiliki pertanyaan lebih lanjut atau ingin berbicara dengan tim kami, jangan ragu untuk menghubungi kami melalui email di:</p>
                            <h5 class="teal-text"><strong>hello@talenthub.id</strong></h5>
                            <br>
                            <p><a href="{{  url('/') }}">Kembali ke halaman depan</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop

@section('script')
	<script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js'); ?>" type="text/javascript"></script>
@stop