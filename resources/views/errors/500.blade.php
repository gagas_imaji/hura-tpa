@extends('error_layout.default')
@section('content')
<div class="container">
    <div class="row">
        <div class="col m6 s12">
            <p><img src=" {{  url('assets/image/error-icon.png') }}"></p>
            <h1 class="teal-text text-lighten-5">Oh?</h1>
            <p class="white-text">Sepertinya ada yang salah pada aplikasi kami.</p>
            <p class="white-text">Silakan coba salah satu langkah berikut:</p>
            <ol class="white-text">
                <li>Reload halaman ini dengan cara mengklik tombol refresh/reload pada browser Anda.</li>
                <li>Hapus cache pada browser.</li>
            </ol>
        </div>
        <div class="col m6 s12">
            <div class="card">
                <div class="card-panel">
                    
                    {!! Form::open(['url' => 'error-report',"method"=>"POST","class"=>"",'id'=>"error-message","data-submit"=>"noAJax"]) !!}
                        <p class="teal-text text-darken-3"><strong>Jika belum berhasil juga, mohon kirim pesan atau keluhan Anda melalui formulir berikut:<strong></p>
                        <div class="row">
                            <div class="col s12">
                                <div class="form-group input-field">
                                    <input id="name" type="text" name="name">
                                    <label for="name">Nama</label>
                                </div>
                            </div>
                            <div class="col s12">
                                <div class="form-group input-field">
                                    <input id="email" type="email" name="email" class="validate" required>
                                    <label class="required" for="email">Email</label>
                                </div>
                            </div>
                            <div class="col s12">
                                <div class="form-group input-field">
                                    <select id="error-cause" name="clause" class="validate" required>
                                        <option value="" disabled selected>pilih opsi</option>
                                        <option value="kik-tombol">setelah mengklik tombol atau link saat menggunakan aplikasi ini</option>
                                        <option value="external-url">setelah mengklik tautan dari website lain</option>
                                        <option value="lain-lain">lainnya...</option>
                                    </select>
                                    <label class="required">Bagaimana Anda bisa sampai disini?</label>
                                </div>
                            </div>
                            <div class="col s12">
                                <div class="form-group input-field">
                                    <textarea id="message" class="materialize-textarea" name="detail"></textarea>

                                    <textarea id="error-real" name="error-real" class="materialize-textarea" style="display: none;">{!! 'message = '.$report->getMessage().'on'. 'File = '.$report->getFile().'@ Line = '.$report->getLine() !!}</textarea>
                                    <label for="message">Pesan atau keluhan lain:</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s6 right text-right">
                                <button class="btn btn-sm waves-effect waves-light" type="submit">Kirim</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
<script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/vendor/jquery-validation/jquery.validate.min.js'); ?>"></script>

<script>
            $(document).ready(function(){
                $('#error-message').validate({
                    highlight: function (input) {
                        $(input).parents('.input-field').addClass('error');
                    },
                    unhighlight: function (input) {
                        $(input).parents('.input-field').removeClass('error');
                    },
                    errorPlacement: function (error, element) {
                        $(element).parents('.form-group').append(error);
                    }
                });
                
                $('select').material_select();
            })
        </script>   
@stop