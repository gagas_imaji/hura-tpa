@extends('layouts.default')
    @section('inline-css')
        <style type="text/css">
            .modal .modal-body {
                padding-top: 25px;
            }

        </style>
    @stop
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">List Project</li>
            </ol>

        </div>

    @stop

    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger">
                            <i class="fa fa-list"></i> List Semua Project
                        </div>
                    {{--<!-- @if(Entrust::can('create-survey'))
                        <div class="additional-btn">
                            <a href="/survey/create" class="btn btn-sm btn-primary">Tambah Project</a>
                        </div>
                        @endif -->--}}
                    </div>
                    <div class="panel-body full">
                        <table class="table table-hover no-footer" id="list-projects">
                            <thead>
                              <tr class="headings">
                                    <th>Id</th>
                                    <th>Nama Project</th>
                                    <th>Tanggal Awal</th>
                                    <th>Tanggal Akhir</th>
                                    <th>Status</th>
                                    {{-- <!-- <th class="action"></th> --> --}}
                              </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $row)
                                        <tr>
                                            <td>{!! $row['id'] !!}</td>
                                            <td><a href="{{ url('backend/report-user/'.$row["id"]) }}"><i class="glyphicon glyphicon-search"></i> {!! $row['nama_project'] !!}</a></td>
                                            <td>{!! $row['tanggal_awal'] !!}</td>
                                            <td>{!! $row['tanggal_akhir'] !!}</td>
                                            <td>{!! $row['status'] !!}</td>
                                            {{-- <!-- <td class="text-center">{!! $row['pilihan'] !!}</td> --> --}}
                                        </tr>

                                    @endforeach
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

        $(document).ready(function() {
            $('#list-projects').dataTable({
                "columnDefs": [ {
                    "targets"  : 'action',
                    "orderable": false,
                    "order": []
                }]

            });

        });
        </script>
    @stop