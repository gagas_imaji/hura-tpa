@extends('layouts.default')

@section('inline-css')
	{{-- {!! Html::style("assets/backend/css/pages.css") !!} --}}
	{!! Html::style('assets/vendor/wysiwyg/css/ui/trumbowyg.css') !!}
	{!! Html::style('assets/vendor/radios-to-slider/radios-to-slider.min.css') !!}
@endsection

@section('breadcrumb')
	<div id="breadcrumb">
		<ol class="breadcrumb">
			<li><a href="/home">{!! trans('messages.home') !!}</a></li>
			<li><a href="{{ url('backend/report-user') }}">List Project</a></li>
			<li><a href="{{ url('backend/report-user/'.$data['project_id'])}}">{{ $data["nama_project"] }}</a></li>
			<li class="active">Report User</li>
			<li class="active">{{ $data["nama"] }}</li>
		</ol>
	</div>
@endsection

@section('content')
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title text-complete"><i class="fa fa-info"></i> Report User</h3>
			</div>
			<div class="row">
				<div class="panel-body">
					<div class="col-sm-8 report-info">
						<div class="media">
							<div class="media-left">
								<img class="media-object" src="{{ asset('assets/image/'.$data["avatar"]) }}" alt="{{ $data["nama"] }}">
							</div>
							<div class="media-body">
								<h3 class="media-heading">{{ $data["nama"] }}</h3>
								<dl class="dl-horizontal">
								  <dt>Email </dt> <dd>: {{ $data["email"] }}</dd>
								  <dt>Sign Up Date </dt> <dd>: {{ $data["sign_up"] }}</dd>
								  <dt>Last Login </dt> <dd>: {{ $data["last_login"] }}</dd>
								</dl>
							</div>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="hasil-rekomendasi">
							<h3>Hasil Rekomendasi</h3>
							@if (!is_null($data["rekomendasi"]))
								@if ($data["rekomendasi"]->id == 1)
									<span class="label label-danger">{{ $data["rekomendasi"]->nama }}</span>
								@elseif ($data["rekomendasi"]->id == 2)
									<span class="label label-warning">{{ $data["rekomendasi"]->nama }}</span>
								@elseif ($data["rekomendasi"]->id == 3)
									<span class="label label-success">{{ $data["rekomendasi"]->nama }}</span>
								@else
									<span class="label label-default">Data belum tersedia</span>
								@endif
							@elseif (!is_null($data["hasilKriteria"]))
								@if ($data["hasilKriteria"] == 1)
									<span class="label label-danger">{{ getHasilKriteria($data["hasilKriteria"]) }}</span>
								@elseif ($data["hasilKriteria"] == 2)
									<span class="label label-warning">{{ getHasilKriteria($data["hasilKriteria"]) }}</span>
								@elseif ($data["hasilKriteria"] == 3)
									<span class="label label-success">{{ getHasilKriteria($data["hasilKriteria"]) }}</span>
								@else
									<span class="label label-default">Data belum tersedia</span>
								@endif
							@else
								<span class="label label-default">Data belum tersedia</span>
							@endif
						</div>
						<div class="hasil-rekomendasi">
							<h3>Persentase Kecocokan</h3>
							@if ($data["kecocokan"] >= 75)
								<span class="label label-success">{{ $data["kecocokan"] }}%</span>
							@elseif ($data["kecocokan"] >= 25)
								<span class="label label-warning">{{ $data["kecocokan"] }}%</span>
							@elseif ($data["kecocokan"] > 0)
								<span class="label label-danger">{{ $data["kecocokan"] }}%</span>
							@elseif (is_null($data["kecocokan"]))
								<span class="label label-default">Data belum tersedia</span>
							@else
								<span class="label label-default">Data belum tersedia</span>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="panel-body">
					<div class="col-lg-7 col-md-3 col-sm-3 col-xs-6">
						<div class="media">							
							<div class="media-body">
								<form method="GET" action="{{url('backend/report-user/print/'.$data['user_id'].'/'.$data['project_id'])}}" id="form-cetak-peserta" class="form-cetak-peserta" data-submit="noAjax">
									{!! csrf_field() !!}
									<button type="submit" class="btn btn-success" id="btn-print">Cetak Hasil Laporan</button>
									<input type="hidden" name="refUserIdPrint" value="{{$data['user_id']}}">
									<input type="hidden" name="projectIdPrint" value="{{$data['project_id']}}">
									<input type="hidden" name="promosiIdPrint" id="promosiIdPrint" value="{{$current_jabatan}}">
									@if(!is_null($data["rekomendasi"]))
									<input type="hidden" name="hasilKriteriaPrint" id="hasilKriteriaPrint" value="{{$data['rekomendasi']->id}}">
									@else
									<input type="hidden" name="hasilKriteriaPrint" id="hasilKriteriaPrint" value="{{$data['hasilKriteria']}}">
									@endif
									<input type="hidden" name="totalNilaiPrint" id="totalNilaiPrint" value="{{ $data['kecocokan'] }}">
								</form>
							</div>
						</div>
					</div>
					<div class="col-lg-5 col-md-3 col-sm-3 col-xs-6">
						<form method="POST" action="{{url('backend/report-user/detail/kalkulasi/'.$data['user_id'].'/'.$data['project_id'])}}" id="form-kalkulasi-peserta" class="form-kalkulasi-peserta" data-submit="noAjax">
							<div class="col-md-8">
								<select class="form-control select2 pilih-posisi" name="posisi" id="posisi" required>
									<option value="default">-- Silahkan Pilih Jabatan Tujuan --</option>
					                @foreach($listJabatan as $posisi)
					                	<option value="{{$posisi->id}}" <?php echo ($posisi->id == $current_jabatan ? 'selected' : '');?>>{{$posisi->nama_jabatan}}</option>
					                @endforeach
					            </select>
				            </div>
				            <div class="col-md-4">
				            	<button class="btn btn-success" type="submit" id="btn-proses">Proses</button>
				            	{!! csrf_field() !!}
				            	<input type="hidden" name="refUserId" value="{{$data['user_id']}}">
								<input type="hidden" name="projectId" value="{{$data['project_id']}}">
				            </div>
				        </form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title text-complete"><i class="fa fa-info"></i> Hasil Pemeriksaan Psikologis</h3>
			</div>
			<div class="panel-body">
				<table id="table_report" class="table table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th rowspan="2">Aspek Psikologis</th>
							<th rowspan="2">Kategori Psikologis</th>
							<th rowspan="2">Gambaran individu pada "taraf" Rendah</th>
							<th colspan="5">Taraf</th>
							<th rowspan="2">Gambaran individu pada "taraf" Tinggi</th>
						</tr>
						<tr>
							@for ($t = 1; $t <= 5; $t++)
								<th>{{ $t }}</th>
							@endfor
						</tr>
					</thead>
					<tbody>
						@foreach ($aspeks as $aspek)
							<tr>
								<td>{{ $aspek->nama_aspek }}</td>
								<td>{{ $aspek->tpaKategoriAspek->nama }}</td>
								<td>{{ $aspek->gambaran_taraf_rendah }}</td>
								@for ($tr = 1; $tr <= 5; $tr++)
									<td class="{{ get_class_standar_nilai($aspek->id, $current_jabatan, $tr) }}">
										@if (count($data["nilais"]) > 0)
											<a href="#" class="editscore" data-toggle="tooltip" title="Edit Score" id="editscore-{{$aspek->id}}" data-content="{{$aspek->id}}-{{$data["nilais"][$aspek->id]-1}}-{{ $data["nama"] }}-{{ $aspek->nama_aspek }}-{{$data["user_id"]}}-{{$data["project_id"]}}-{{$current_jabatan}}"> {!! ($tr == ($data["nilais"][$aspek->id]-1) ? '<i class="fa fa-check"></i>' : '') !!} </a>
										@endif
									</td>
								@endfor
								<td>{{ $aspek->gambaran_taraf_tinggi }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title text-complete"><i class="fa fa-info"></i> Uraian Kepribadian</h3>
			</div>
			<div class="panel-body">
				<div class="col-sm-6 box-analisis">
					<p class="text-right">
						<a class="btn btn-xs btn-warning" id="editAnalisisPeserta" class="editAnalisisPeserta" data-content="{{$data['user_id']}}-{{$data['project_id']}}-{{$data['nama']}}">EDIT</a>
					</p>
					@if (!is_null($data["analisis"]))
						{!! $data["analisis"] !!}
					@endif
				</div>
				<div class="col-sm-6 box-detail-analisis">
					<div class="col-xs-12 bg-success-lighter">
						<h4>Kelebihan</h4>
						@if (sizeof($data["kelebihan"])>0)
						<ol>
							@for ($i = 0; $i < count($data["kelebihan"]); $i++)
							    <li>{{ $data["kelebihan"][$i] }}</li>
							@endfor
						</ol>
						@endif
					</div>

					<div class="col-xs-12 bg-danger-lighter">
						<h4>Kelemahan</h4>
						@if (sizeof($data["kelemahan"])>0)
						<ol>
							@for ($i = 0; $i < count($data["kelemahan"]); $i++)
							    <li>{{ $data["kelemahan"][$i] }}</li>
							@endfor
						</ol>
						@endif
					</div>

					<div class="col-xs-12 bg-complete-lighter">
						<h4>Karakteristik Pekerjaan yang Sesuai</h4>
						@if (sizeof($data["karakteristik"])>0)
						<ol>
							@for ($i = 0; $i < 6; $i++)
							    <li>{{ $data["karakteristik"][$i] }}</li>
							@endfor
						</ol>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="editAnalisis" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form method="POST" enctype="multipart/form-data" data-submit="noAjax" action="{{url('backend/report-user/detail/'.$data['user_id'].'/'.$data['project_id'].'/update-uraian')}}" id="form-update-uraian">
					<div class="modal-header">
						<h4 class="modal-title" id="editAnalisisLabel">Edit Hasil Analisis - <span id="uraian_nama_peserta"></span></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div class="form-line">
								@if(!is_null($data["analisis"]))
								<textarea rows="10" class="form-control wysiwyg" name="uraian" required>
									{!! $data["analisis"] !!}
								</textarea>
								@else
								<textarea rows="10" class="form-control wysiwyg" name="uraian" required>
								</textarea>
								@endif
							</div>
						</div>
					</div>
					<input type="hidden" name="idProject" id="idProject" value="">
					<input type="hidden" name="idUserUraian" id="idUserUraian" value="">
					{!! csrf_field() !!}
					<div class="modal-footer">
						<button id="save_uraian" type="submit" class="btn btn-success">SAVE CHANGES</button>
						<button type="button" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="editNilai" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form class="form-horizontal" method="post" data-submit="noAjax" enctype="multipart/form-data" action="{{url('backend/report-user/detail/'.$data['user_id'].'/'.$data['project_id'].'/update-nilai')}}" id="form-edit-nilai">
					<div class="modal-header">
						<h4 class="modal-title" id="editNilaiLabel">
							Edit Nilai - <span id="nama_peserta">Peserta</span>
						</h4>
						<h4 class="modal-title" id="editNilaiLabel">
							{{-- <span id="kategori_aspek">Kemampuan Intelektual</span> -  --}}
							<span id="nama_aspek">Logika Berfikir</span>
						</h4>
					</div>
					<div class="modal-body">
						<div id="radios" class="m-t-20">
							<input id="option1" name="options" type="radio" value="1">
							<label for="option1">1</label>

							<input id="option2" name="options" type="radio" value="2">
							<label for="option2">2</label>

							<input id="option3" name="options" type="radio" value="3">
							<label for="option3">3</label>

							<input id="option4" name="options" type="radio" value="4">
							<label for="option4">4</label>

							<input id="option5" name="options" type="radio" value="5">
							<label for="option5">5</label>
						</div>
					</div>
					<input type="hidden" name="projectId" id="projectId" value="">
					<input type="hidden" name="idAspek" id="idAspek" value="">
					<input type="hidden" name="idUser" id="idUser" value="">
					<input type="hidden" name="idJabatan" id="idJabatan" value="">

					{!! csrf_field() !!}
					<div class="modal-footer">
						<button id="save_nilai" type="submit" class="btn btn-success bg-cyan waves-effect">SAVE CHANGES</button>
						<button type="button" class="btn btn-danger bg-cyan waves-effect" data-dismiss="modal">CLOSE</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
@section('inline-js')
	{!! Html::script('assets/vendor/wysiwyg/js/trumbowyg.min.js') !!}
	{!! Html::script('assets/vendor/radios-to-slider/jquery.radios-to-slider.min.js') !!}
	<script>
		$(document).ready(function() {
			$("#table_report").DataTable({
					"bSort" : false,
					"bPaginate": false,
					"info": false,
					"columnDefs": 	[
						{ "visible": false, "targets": 1 },
					],
					"order": [[ 1, 'asc' ]],
					"drawCallback": function ( settings ) {
			            var api = this.api();
			            var rows = api.rows( {page:'current'} ).nodes();
			            var last=null;

			            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
			                if ( last !== group ) {
			                    $(rows).eq( i ).before(
			                        '<tr class="group"><td colspan="10">'+group+'</td></tr>'
			                    );

			                    last = group;
			                }
			            } );
			        }
			});

			$("#posisi").select2({});
			$('.wysiwyg').trumbowyg();

			$('#editAnalisisPeserta').on("click",function(){
		        var param = $(this).attr("data-content");
		        var data = param.split("-");
		        var userId = data[0];
		        var projectId = data[1];
		        var namaPeserta = data[2];
		        $('#idProject').val(projectId);
		        $('#idUserUraian').val(userId);
		        $('#uraian_nama_peserta').text(namaPeserta);
		        $('#editAnalisis').modal('show');
		    });

		    $('#editNilai').on('shown.bs.modal', function () {
				$('#radios').radiosToSlider({animation: true,});
			})

		    $('.editscore').on("click",function(){
		    	var param = $(this).attr("data-content");
    			var data  = param.split("-");
    			console.log(data);
    			var aspekId  = data[0];
    			var oldNilai = data[1];
    			var namaPeserta = data[2];
    			var namaAspek = data[3];
    			var userId = data[4];
    			var projectId = data[5];
    			var jabatanId = data[6];
    			$('#nama_peserta').text(namaPeserta);
    			$('#option'+oldNilai).prop("checked", true);
    			$('#nama_aspek').text(namaAspek);
    			$('#idUser').val(userId);
    			$('#projectId').val(projectId);
    			$('#idAspek').val(aspekId);
    			$('#idJabatan').val(jabatanId);
		    	$('#editNilai').modal('show');
		    });
		});
	</script>
@endsection