@extends('layouts.default')

@section('inline-css')
	{{ Html::style("assets/vendor/jquery-datatables-checkboxes/css/dataTables.checkboxes.css") }}
	{!! Html::style("assets/vendor/ion-slider/css/ion.rangeSlider.css") !!}
	{!! Html::style("assets/vendor/ion-slider/css/ion.rangeSlider.skinModern.css") !!}
@endsection

@section('breadcrumb')
	<div id="breadcrumb">
		<ol class="breadcrumb">
		    <li><a href="{{ url('home') }}">{!! trans('messages.home') !!}</a></li>
		    <li><a href="{{ url('backend/report-user') }}">List Project</a></li>
		    <li class="active">{!! $detail['nama'] !!}</li>
		</ol>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-title text-danger">
						<i class="fa fa-info"></i> Info Project
					</div>
				</div>
				<div class="panel-body">
					<table class="table table-stripped table-hover show-table">
						<tbody>
							<tr>
								<th>Nama</th>
								<td class="text-right">{!!$detail['nama']!!}</td>
							</tr>
							<tr>
								<th>Tanggal Awal</th>
								<td class="text-right">{!!$detail['tanggal_awal']!!}</td>
							</tr>
							<tr>
								<th>Tanggal Akhir</th>
								<td class="text-right">{!!$detail['tanggal_akhir']!!}</td>
							</tr>
							<tr>
								<th>Deskripsi</th>
								<td class="text-right">{!!$detail['deskripsi']!!}</td>
							</tr>
							<tr>
								<th>Status</th>
                                <td>
	                                {!!$status_project!!}
                                </td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<strong><i class="fa fa-tasks fa-fw"></i>  Tools Project ({{ count($detail['tools'])}} tools)</strong>
				</div>
				<div class="panel-body full">
					<table class="table table-hover show-table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Random Pertanyaan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($detail['tools'] as $tools)
                                <tr>
                                    <td>
                                       {!! $tools['judul'] !!}
                                    </td>
                                    <td>
                                        @if($tools['is_random'] == 0)
                                            <span class="label label-danger"> Tidak</span>
                                        @elseif($tools['is_random'] == 1)
                                            <span class="label label-info"> Random</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($tools['status'] == 0)
                                            <span class="label label-warning pull-left"> Belum berjalan</span>
                                        @elseif($tools['status'] == 1)
                                            <span class="label label-danger pull-left"> Selesai</span>
                                        @elseif($tools['status'] == 2)
                                            <span class="label label-success pull-left"> Sedang berjalan</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<strong><i class="fa fa-user fa-fw"></i>  Peserta Project</strong>
				</div>
				<div class="panel-body">
					<div class="panel panel-default">
						<div class="panel-body">
						{!! Form::open(["method"=> "GET", "url"=>"backend/report-user/".$detail["id"] , "id"=>"user_report_filter", 'data-submit'=>"noAjax"]) !!}
								<p>Filter Berdasarkan :</p>
								<div class="col-sm-4">
									<div class="form-group">
										<label>Tingkat Keccocokan</label>
										{!! Form::text("tingkat_kecocokan", '', ["class"=> 'form-control slider', "id"=>"slider_kecocokan"]) !!}
									</div>
								</div>
								{{-- <div class="col-xs-12">
									<button class="btn btn-primary">Filter</button>
								</div> --}}
						{!! Form::close() !!}

						</div>
					</div>
					<button class="btn btn-success" id="compare_button">Compare</button>
					<a href="{{url('backend/report-user/generate-all-tpa/'.$detail['id'])}}" title="Generate TPA">
						<button class="btn btn-success" id="generate_all_tpa" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Calculate">Generate All TPA</button>
					</a>
					<table id="list_perserta_project" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th></th>
								<th class="text-center">Nama Peserta</th>
								<th class="text-center">Status</th>
								<th class="text-center">Download</th>
								<th class="text-center">Tingkat Kecocokan</th>
							</tr>
						</thead>
						<tbody>
						@if ($rowset)
							@foreach ($rowset as $row)
								<tr class="user-report-info">
									<td>
										{!!$row['status-check']!!}
									</td>
									<td>
										<div>
											<div class="media">
												<div class="media-left">
													@if ($row['status-test']==1)
														<a href="{{ url('backend/report-user/detail/'.$row['user_id']."/".$detail["id"]) }}"><img class="media-object" src="{!!asset('assets/image/'.$row["avatar"])!!}" alt="{!! $row['name'] !!}"></a>
													@else
														<img class="media-object" src="{!!asset('assets/image/'.$row["avatar"])!!}" alt="{!! $row['name'] !!}">
													@endif
												</div>
												<div class="media-body">													
													{!!$row['status-test-user']!!}
													<p>
														<b>Email :</b> {!! $row['email'] !!}
													</p>
												</div>
											</div>
										</div>
									</td>
									<td class="text-center">
										<p>
											{!! $row['status'] !!}
										</p>
									</td>
									<td class="text-center">
										<p>{!! $row['print-all'] !!}</p>
										<p>
											@foreach($row['ceklis'] as $key)
                                                {!! $key !!}
                                            @endforeach
										</p>
										<p>
											@foreach($row['view'] as $key)
                                                {!! $key !!}
                                            @endforeach
										</p>
									</td>
									<td class="text-center">
									@if ($row['status-test']==1)
										@if ($row['tingkat_kecocokan'] == 'Data belum tersedia')
										<a href="{{ url('backend/report-user/detail/'.$row['user_id']."/".$detail["id"]) }}">Klik disini untuk melihat kecocokan.</a>
										@else
										<a href="{{ url('backend/report-user/detail/'.$row['user_id']."/".$detail["id"]) }}"><span class="label label-warning" style="font-size:16px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Laporan">{{ $row['tingkat_kecocokan'] }}</span></a>
										<a href="{{url('backend/report-user/print-pdf/'.$row['user_id'].'/'.$detail['id'])}}" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download PDF">
                                          <i class="fa fa-download fa-fw"></i>
                                        </a>
										@endif
									@else
										{{ $row['tingkat_kecocokan'] }}
									@endif
									</td>
								</tr>
							@endforeach
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('inline-js')
	{{ Html::script("assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js") }}
	{{ Html::script("assets/vendor/jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js") }}
	{{ Html::script("assets/vendor/ion-slider/js/ion-rangeSlider/ion.rangeSlider.min.js") }}
	<script>
		$.fn.dataTable.ext.search.push(
		    function( settings, data, dataIndex ) {
		    	var range = $("#slider_kecocokan").val().split(";");
		        var min = range[0];
		        var max = range[1];
		        var cocok = parseFloat( data[4] ) || 0; // use data for the cocok column
		        if ( (isNaN( min )&&isNaN( max )) || (isNaN(min)&&cocok<=max) || (min<=cocok&&isNaN(max)) || (min<=cocok&&cocok<=max) || ( min==0 && cocok == 0)  )
		        {
		            return true;
		        }
		        return false;
		    }
		);
		$(document).ready(function() {

			$('#generate_all_tpa').on('click', function() {
			    var $this = $(this);
				$this.button('loading');
				// setTimeout(function() {
				//    $this.button('reset');
				// }, 8000);
			});

			$(".slider").ionRangeSlider({
				type: 'double',
				grid: false,
				min: 0,
				max: 100
			});

			var table_user = $("#list_perserta_project").DataTable({
				bPaginate : true,
				"columnDefs":
				[
					{ "orderable": false, "targets": 0 }
				]
			});

			$("#slider_kecocokan").change(function(event) {
				console.log("change");
				table_user.draw();
			});

			$("#compare_button").click(function(event) {

				var count = $('.compare-cb:checkbox:checked').length;
				if (count <= 1){
					toastr.error('Pastikan anda telah memilih 2 peserta', 'Alert', {timeOut: 5000});
				}else if(count > 2){
					toastr.error('Maksimal user yang bisa dibandingkan 2', 'Alert', {timeOut: 5000});
				}else{
					var checked = [];
					var project_id = {{ is_null($detail['id']) ? "null" : $detail['id'] }};
					$('.compare-cb:checkbox:checked').each(function() {
						checked.push($(this).val());
					});
					var join = checked.join("-");
					var to_href = join+"/"+project_id;
					$(location).attr('href', "{{ url('backend/report-user/compare/') }}/"+to_href);
				}
			});

			$('.compare-cb').change(function(){
				if($(this).is(':checked'))
				{
					count = $(".compare-cb:checkbox:checked").length;
					if(count > 2 )
				    {
				        this.checked = false;
				        toastr.error('Maksimal user yang bisa dibandingkan 2', 'Alert', {timeOut: 5000});
					}
				}
			});
		});
	</script>
@endsection