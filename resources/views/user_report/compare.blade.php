@extends('layouts.default')

@section('inline-css')

@endsection

@section('breadcrumb')
	<div id="breadcrumb">
		<ol class="breadcrumb">
		    <li><a href="{{ url('home') }}">{!! trans('messages.home') !!}</a></li>
		    <li><a href="{{ url('backend/report-user') }}">List Project</a></li>
		    <li class="active">Compare User</li>
		</ol>
	</div>
@endsection

@section('content')
	@foreach ($data as $d)
		<div class="col-sm-6 compare-col">
			<div class="col-sm-12 compare-data">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title text-complete"><i class="fa fa-info"></i> Hasil Report {{ $d['nama'] }}</h3>
					</div>
					<div class="panel-body">
						<div class="col-sm-12 report-info compare">
							<div class="media">
								<div class="media-left">
									<img class="media-object" src="{{ asset('assets/image/default-avatar.png') }}" alt="{{ $d['nama'] }}">
								</div>
								<div class="media-body">
									<h3 class="media-heading">{{ $d['nama'] }}</h3>
									<dl class="dl-horizontal">
									  <dt>Email </dt> <dd>: {{ $d['email'] }}</dd>
									  <dt>Sign Up Date </dt> <dd>: {{ $d["sign_up"] }}</dd>
									  <dt>Last Login </dt> <dd>: {{ $d["last_login"] }}</dd>
									</dl>
								</div>
							</div>

							<hr>

							<div class="hasil-rekomendasi">
								<b>Hasil Rekomendasi :</b>
								@if (!is_null($d["rekomendasi"]))
									@if ($d["rekomendasi"]->id == 1)
										<span class="label label-danger">{{ $d["rekomendasi"]->nama }}</span>
									@elseif ($d["rekomendasi"]->id == 2)
										<span class="label label-warning">{{ $d["rekomendasi"]->nama }}</span>
									@elseif ($d["rekomendasi"]->id == 3)
										<span class="label label-success">{{ $d["rekomendasi"]->nama }}</span>
									@else
										<span class="label label-default">Data belum tersedia</span>
									@endif
								@else
									<span class="label label-default">Data belum tersedia</span>
								@endif
							</div>

							<div class="hasil-rekomendasi">
								<b>Persentase Kecocokan :</b>
								@if ($d["kecocokan"] >= 75)
									<span class="label label-success">{{ $d["kecocokan"] }}%</span>
								@elseif ($d["kecocokan"] >= 50)
									<span class="label label-warning">{{ $d["kecocokan"] }}%</span>
								@elseif ($d["kecocokan"] > 0)
									<span class="label label-danger">{{ $d["kecocokan"] }}%</span>
								@elseif (is_null($d["kecocokan"]))
									<span class="label label-default">Data belum tersedia</span>
								@else
									<span class="label label-default">Data belum tersedia</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-12 compare-data">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title text-complete"><i class="fa fa-info"></i> Hasil Pemeriksaan Psikologis</h3>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table_report" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Aspek Psikologis</th>
									<th>Kategori Psikologis</th>
									<th>Nilai</th>
								</tr>
							</thead>
							@foreach ($aspeks as $aspek)
								<tr>
									<td>{{ $aspek->nama_aspek }}</td>
									<td>{{ $aspek->tpaKategoriAspek->nama }}</td>
									<td class="text-center">
										@if (count($d["nilais"]) > 1)
											<span class="badge badge-info">{{ $d["nilais"][$aspek->id]-1 }}</span>
										@else
											<span class="badge badge-info">null</span>
										@endif
									</td>
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>

			<div class="col-sm-12 compare-data">
				<div class="panel panel-default">
					<div class="panel-body">
						<h3 class="panel-title text-complete"><i class="fa fa-info"></i> Analisis {{ $d['nama'] }}</h3>
						<div class="col-sm-12 box-analisis">
							<div class="p-analisis">
								@if (!is_null($d["analisis"]))
									{!! $d["analisis"] !!}
								@endif
							</div>
						</div>
						<div class="col-sm-12 box-detail-analisis">
							<div class="col-xs-12 bg-success-lighter">
								<h4>Kelebihan</h4>
								@if (sizeof($d["kelebihan"])>0)
								<ol>
									@for ($i = 0; $i < 5; $i++)
									    <li>{{ $d["kelebihan"][$i] }}</li>
									@endfor
								</ol>
								@endif
							</div>

							<div class="col-xs-12 bg-danger-lighter">
								<h4>Kelemahan</h4>
								@if (sizeof($d["kelemahan"])>0)
								<ol>
									@for ($i = 0; $i < 5; $i++)
									    <li>{{ $d["kelemahan"][$i] }}</li>
									@endfor
								</ol>
								@endif
							</div>

							<div class="col-xs-12 bg-complete-lighter">
								<h4>Karakteristik Pekerjaan yang Sesuai</h4>
								@if (sizeof($d["karakteristik"])>0)
								<ol>
									@for ($i = 0; $i < 6; $i++)
									    <li>{{ $d["karakteristik"][$i] }}</li>
									@endfor
								</ol>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach
	<div class="row">
		<div class="col-xs-12">
			&nbsp;
			{{-- Supaya ga bocor ke bawah --}}
		</div>
	</div>
@endsection

@section('inline-js')

	<script>
		$(document).ready(function() {
			$(".table_report").DataTable({
					"bSort" : false,
					"bPaginate": false,
					"info": false,
					"searching" : false,
					"columnDefs": 	[
						{ "visible": false, "targets": 1 },
					],
					"order": [[ 1, 'asc' ]],
					"drawCallback": function ( settings ) {
			            var api = this.api();
			            var rows = api.rows( {page:'current'} ).nodes();
			            var last=null;

			            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
			                if ( last !== group ) {
			                    $(rows).eq( i ).before(
			                        '<tr class="group"><td colspan="4">'+group+'</td></tr>'
			                    );

			                    last = group;
			                }
			            } );
			        }
			});


		});
	</script>
@endsection