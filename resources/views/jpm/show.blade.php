@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/job-characteristic">Manajemen JPM</a></li>
                <li class="active">{!! get_name_of_job($data['job_id']) !!}</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Detail </strong>Karakteristik Pekerjaan
                    </div>
                    <div class="panel-body full">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-3" style="margin-top:20px;margin-bottom:20px;"></div>
                        </div>
                        <div class="responsive">
                            <table class="table table-stripped table-hover show-table">
                                <tbody>
                                    <tr>
                                        <th colspan="4">Nama</th>
                                        <td>{{ get_name_of_job($data['job_id']) }}</td>
                                    </tr>
                                    <tr>
                                        <th>D</th>
                                        <th>I</th>
                                        <th>S</th>
                                        <th>C</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td>{{$data['D']}}</td>
                                        <td>{{$data['I']}}</td>
                                        <td>{{$data['S']}}</td>
                                        <td>{{$data['C']}}</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="wpa-result-chart">
                                <div class="row">
                                    <div class="col col-md-8" >
                                        <div id="jpm-graph" style="height: 400px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    @stop

    @section("inline-js")
        {!! Html::script('assets/vendor/canvasjs/canvasjs.min.js') !!}
        {!! Html::script('assets/vendor/canvasjs/jquery.canvasjs.min.js') !!}
        <script>
        $(function() {
            $("#jpm-graph").CanvasJSChart({
                title: {
                    text: "Karakteristik Pekerjaan"
                },
                axisY: {
                    
                    includeZero: false,
                    minimum: -8,
                    maximum: 8,
                    valueFormatString: " ",
                     tickLength: 0
                },
                axisX: {
                    interval: 1
                },
                data: [
                {
                    type: "line", //try changing to column, area
                    toolTipContent: "{label}: {y}",
                    dataPoints: [
                        { label: "D",  y: {{ ConvertJPMIndex($data['D']) }} },
                        { label: "I",  y: {{ ConvertJPMIndex($data['I']) }} },
                        { label: "S",  y: {{ ConvertJPMIndex($data['S']) }} },
                        { label: "C",  y: {{ ConvertJPMIndex($data['C']) }} },
                    ]
                }
                ]
            });
        });
        </script>
    @stop