@extends('layouts.default')

@section('breadcrumb')
	<div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="/home">{!! trans('messages.home') !!}</a></li>
            <li><a href="/job-characteristic">Manajemen JPM</a></li>
            <li class="active">Tambah Karakteristik Pekerjaan</li>
        </ol>

    </div>
@stop

@section('content')
	<div class="row">
        <div class="col-lg-12"> 
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Tambah </strong>Karakteristik Pekerjaan
                	<div class="additional-btn">
                		<a href="/job-characteristic" data-href="/job-characteristic" class="btn btn-sm btn-primary">List Pekerjaan</a>
                	</div>
                </div>
                <div class="panel-body">

                    <div class="col-lg-9">
                        <div class="alert alert-warning">
                            <h5><i class="fa fa-warning fa-fw"></i> Petunjuk</h5>
                            <p>Dibawah ini ada 24 pernyataan, berikan penilaian berdasarkan tingkat keseringannya.</p>
                            <ul>
                                <li><strong>Sangat Jarang</strong> : Kurang dari 25% waktunya digunakan untuk melakukan pekerjaan yang dimaksud</li>
                                <li><strong>Jarang</strong> : 25% - 50 % waktunya digunakan untuk melakukan pekerjaan yang dimaksud</li>
                                <li><strong>Sering</strong> : 51% - 75% waktunya digunakan untuk melakukan pekerjaan yang dimaksud</li>
                                <li><strong>Sangat Sering</strong> : Lebih dari 75% waktunya digunakan untuk melakukan pekerjaan yang dimaksud</li>
                            </ul>
                        </div>
                        {!! Form::open(['route' => 'job-characteristic.store', 'role' => 'form', 'class' => 'job-characteristic-form','id' => 'job-characteristic-form', 'data-submit' => 'noAjax']) !!}
                            <div class="form-group form-group-default required">
                        		{!! Form::label('nama','Nama Pekerjaan/Posisi',[])!!}
                                {!! Form::input('text','nama','',['class'=>'form-control','placeholder'=>'Nama'])!!}
                            </div>
                            <?php $i = 0;?>
                            @foreach($data['questionnaire'] as $key)
                                <div class="form-group form-group-default required">
                                    <p>{{ $key }}</p>
                                    <ul>
                                        <li><input type="radio" name="answer_{{$i}}" value="-2" id="id_answer"> Sangat Jarang</li>
                                        <li><input type="radio" name="answer_{{$i}}" value="-1" id="id_answer"> Jarang</li>
                                        <li><input type="radio" name="answer_{{$i}}" value="1" id="id_answer"> Sering</li>
                                        <li><input type="radio" name="answer_{{$i}}" value="2" id="id_answer"> Sangat Sering</li>
                                    </ul>
                                    
                                    
                                </div>
                            <?php $i++;?>
                            @endforeach
    						<div class="form-group">
    							{!! Form::submit('Simpan',['class' => 'btn btn-primary pull-left', 'id' => 'create_job_characteristic']) !!}
    						</div>
						{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
	</div>
@stop