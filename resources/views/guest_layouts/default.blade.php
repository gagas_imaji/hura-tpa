@include('guest_layouts.head')

<body class="login-page">
   
    @yield('content')

    @yield('script')
@include('guest_layouts.foot')