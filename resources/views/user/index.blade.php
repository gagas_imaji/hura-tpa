@extends('layouts.default')
    @section('inline-css')
        <style type="text/css">
            .modal .modal-body {
                padding-top: 25px;
            }

        </style>
    @stop

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Manajemen {!! trans('messages.user') !!}</li>
            </ol>

        </div>
	@stop

	@section('content')
		<div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger"><i class="fa fa-users fa-fw"></i>  List Semua User</div>
                        @if(Entrust::can('create-user'))
                    	<div class="additional-btn">
                    		<a href="/user/create" class="btn btn-sm btn-primary">Tambah User</a>
                            <!-- <a href="/user/upload" class="btn btn-sm btn-primary">Upload User</a> -->
                    	</div>
                        @endif
                    </div>
                    <div class="panel-body full">
                        @include('common.datatable',['table' => $table_data['user-table']])
                    </div>
                </div>
            </div>
		</div>
	@stop
