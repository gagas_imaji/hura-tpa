@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/user">Manajemen {!! trans('messages.user') !!}</a></li>
			    <li class="active">Tambah User Baru</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<div class="additional-btn">
                    		<a href="/user" data-href="/user" class="btn btn-sm btn-primary">List Semua User</a>
                    	</div>
                    </div>
                    <div class="panel-body">
                    	{!! Form::open(['route' => 'user.store','role' => 'form', 'class' => 'user-form','id' => "user-form", 'data-submit' => 'noAjax']) !!}
						@include('auth._register_form')
						<div class="form-group">
							{!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-right', 'id' => 'create_user']) !!}
						</div>
						{!! Form::close() !!}
                    </div>
                </div>
            </div>
		</div>
	@stop

    