@extends('layouts_user.default')
    
    @section('content')

        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <h2>Update Password </h2><span class="label label-info">Minimal 6 karakter</span><br/>
                </div>
                <div class="panel-body">
                    <form id="reset-password"  method="post" enctype="multipart/form-data" action="/user/reset-password/{{Auth::user()->id}}" data-submit="noAjax">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            {!! Form::label('new_password',trans('messages.new').' '.trans('messages.password'),[])!!}
                            {!! Form::input('password','new_password','',['class'=>'form-control '.(config('config.enable_password_strength_meter') ? 'password-strength' : ''),'placeholder'=>trans('messages.new').' '.trans('messages.password')])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('new_password_confirmation',trans('messages.confirm').' '.trans('messages.password'),[])!!}
                            {!! Form::input('password','new_password_confirmation','',['class'=>'form-control','placeholder'=>trans('messages.confirm').' '.trans('messages.password')])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit(isset($buttonText) ? $buttonText : trans('messages.update'),['class' => 'btn btn-primary pull-left']) !!}
                        </div>
                    </form>
                </div>        
            </div>
        </section>

    @stop

    @section('script')
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery-validation/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?php echo asset('assets/js/user/user-page.js'); ?>"></script>

        <script type="text/javascript">
            //day left 
            var timer           = $('#timer').val();
            var count_survey    = $('#count_survey').val();
            var i;

            for(i = 0; i < count_survey; i++){
                var end_date    = $('#end_date'+i).val();
                function getTimeRemaining(endtime){
                    var t       = Date.parse(endtime) - Date.parse(new Date());
                    var days    = Math.floor( t/(1000*60*60*24) );
                    return {
                        'total': t,
                        'days': days+1
                    }
                }

                function initTime(identifier, endtime){
                    var timer   = document.getElementById('day_left'+i);
                    var daySpan = timer.querySelector('.day-date');
                  
                    var timeInterval = setInterval(function(){
                        var t = getTimeRemaining(endtime);
                        daySpan.innerHTML = t.days;
                    
                        if(t.total<=0){
                            clearInterval(timeInterval);
                        }
                    }, 1000);
                }

                initTime('timer', end_date);
            }
        </script>
    @stop