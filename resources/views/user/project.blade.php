@extends('layouts_user.default')
    
    @section('content')

        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <h2>Selamat datang, {!! get_firstname() !!}!</h2>
                    @if($data['count_project_wajib'] != 0)
                        <p>Anda memiliki {{ $data['count_project_wajib'] }} assessment yang wajib Anda ikuti. Silakan klik tombol DETIL untuk melihat  tes yang tersedia.</p>
                    @endif
                </div>
                <div class="active-tool-list">
                    <div class="row">
                        @if($data['count_project_wajib'] != 0)
                            <table class="table table-striped bulk_action">
                                <thead>
                                  <tr class="headings">
                                    <th class="column-title">Nama Assessment</th>
                                    <th class="column-title">Tanggal Awal </th>
                                    <th class="column-title text-center">Detil</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['project_wajib'] as $project)
                                    <tr class="">
                                        <td class=" "> 
                                            <strong> {{ $project['name'] }}</strong>
                                        </td>
                                        <td class=" ">{{ $project['start_date'] }}</td> 
                                        <td class="text-center"> 
                                            <a href="#" class="btn btn-success btn-xs disclaimer" data-toggle="tooltip" title="Edit Score" id="disclaimer-{{ $project['slug'] }}" data-content="{{ $project['slug'] }}+{{ $project['name'] }}">
                                                <span class="glyphicon glyphicon-search"> </span> Detil
                                            </a>
                                            <!-- <a href="/project/{{ $project['slug'] }}" class="btn btn-success btn-xs">
                                                <span class="glyphicon glyphicon-search"> </span> Detil
                                            </a> -->
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <h6 class="survey-title alert alert-neutral">Tidak ada assessment yang wajib Anda ikuti untuk saat ini.</h6>
                        @endif             
                    </div>              
                </div>          
            </div>
            <!-- Modal -->
            @if($data['count_project_wajib'] != 0)
                    <div class="modal fade" id="disclaimer" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-md" style="margin-top: 120px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" id="close">&times;</button>
                                    <h5 class="modal-title">Pernyataan</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="panel panel-default m-t-10">
                                        <div class="panel-heading">
                                            <span id="project-name">Project</span>
                                        </div>
                                        <div class="panel-body">
                                            <p style="font-size: 15px;">
                                               Dengan ini Saya {!! get_firstname() !!} bertanggung jawab penuh atas apa yang Saya kerjakan bahwa semua berdasarkan kemampuan Saya. Saya bersedia menerima sanksi apabila Saya melakukan kecurangan atau ketidakprofesionalan dalam mengerjakan online assessment ini.
                                            </p> <br/>
                                            
                                        </div>
                                    </div>
                                </div>   
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-xs pull-right" data-dismiss="modal" style="margin-left: 5px;" id="tidak-setuju"><span class="glyphicon glyphicon-remove"></span> Tidak Setuju</button>
                                    <div class="btn btn-success btn-xs pull-right" id="setuju">
                                        <input type="hidden" name="slug" id="slug-setuju">
                                        <span class="glyphicon glyphicon-ok"></span> Setuju
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
            @endif
        </section>

    @stop

    @section('script')
        <script src="<?php echo asset('assets/vendor/jquery/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery-validation/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?php echo asset('assets/js/user/user-page.js'); ?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.modal').modal({
                    dismissible: true
                });
                $('.disclaimer').on("click",function(){
                    var param   = $(this).attr("data-content");
                    var data    = param.split("+");
                    var slug    = data[0];
                    var name    = data[1];
                    $('#project-name').text(name);
                    $('#slug-setuju').val(slug);
                    $('#disclaimer').modal('open');
                });

                $('#setuju').click(function(){
                    window.location.href = "/project/"+$('#slug-setuju').val();
                });

                $('#tidak-setuju, #close').click(function(){
                    $('#disclaimer').modal('close');
                });
            });

        </script>
    @stop