@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/user">{!! trans('messages.user') !!}</a></li>
                <li class="active">Upload</li>
            </ol>

        </div>
	@stop

	@section('content')
  <div class="row">
          <div class="col-lg-12">
              <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="additional-btn">
                      <a href="/user" data-href="/user" class="btn btn-sm btn-primary">List Semua User</a>
                    </div>
                  </div>
                  <div class="panel-body">
                    {!! Form::open(['url' => 'upload-user','role' => 'form', 'class'=>'','files' => true,'id'=>'upload-form','data-submit'=>'noAjax']) !!}
                    <div class="form-group form-group-default required">
                        <label for="excel">File (.xlsx ) <a href="{{ url("format-excel-add-user") }}">Unduh Format .xlsx</a></label>
                        <input type="file" name="file" id="file-excel" class="form-control">
                    </div>


										<div class="form-group">
                      {!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-right', 'id' => 'create_user']) !!}
                    </div>
                      {!! Form::close() !!}
                  </div>
              </div>
          </div>
  </div>
    @stop
