@extends('layouts_user.default')

    @section('content')

        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <h2>Assessment : {!! $detail['nama'] !!}</h2>
                    @if($detail['tools'])
                        <p>Anda memiliki {!! $detail['count_tools'] !!} test yang wajib Anda ikuti. Silakan klik tombol MULAI TEST pada masing-masing test untuk memulai.</p>
                    @endif
                </div>
                <div class="active-tool-list">
                    <div class="row">
                        <input type="hidden" name="count" id="count_survey" value="{{ count($detail['tools']) }}" />
                        @if($detail['tools'])
                            <?php $i = 0; ?>
                            @foreach($detail['tools'] as $surveys)
                                <div class="col l3 m4 s12">
                                    <div class="card sticky-action">
                                        <div class="card-image waves-effect waves-block waves-light">
                                            <img class="activator" src="<?php echo asset('assets/image/survey-'.$surveys['category_survey_id'].'.jpg'); ?>">
                                            <span class="card-title activator white-text">{!! $surveys['title'] !!}<i class="material-icons right">more_vert</i></span>
                                        </div>
                                        <div class="card-action clearfix" id="day_left{{ $i }}">
                                            <input type="hidden" name="day_left" value="{!! date("m/d/Y", strtotime($surveys['end_date']) )  !!}" id="end_date{{ $i }}" />
                                            <!-- <p class="days-left left day-date">00</p>
                                            <p class="days-left left">&nbsp;hari lagi</p> -->
                                            <p class="right text-right"><a href="/survey/{{ $surveys['slug'] }}/intro">Mulai Test</a></p>
                                        </div>
                                        <div class="card-reveal">
                                            <span class="card-title grey-text text-darken-4">{!! $surveys['title'] !!}<i class="material-icons right ">close</i></span>
                                            <p>{!! $surveys['instruction'] !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        @else
                            <h6 class="survey-title alert alert-neutral">Tidak ada tes yang wajib Anda ikuti untuk saat ini.</h6>
                        @endif
                    </div>
                </div>
            </div>
        </section>

    @stop

    @section('script')
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery-validation/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?php echo asset('assets/js/user/user-page.js'); ?>"></script>

        <script type="text/javascript">
            
        </script>
    @stop
