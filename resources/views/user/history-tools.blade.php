@extends('layouts.default')
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/project">Manajemen User</a></li>
                <li><a href="/user/history/{{$user_id}}">{{ get_firstname_by_id($user_id) }}</a></li>
                <li class="active">{{ $detail['nama'] }}</li>
            </ol>
        </div>
    @stop 
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><i class="fa fa-info fa-fw"></i> Info Project</strong>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-stripped table-hover show-table">
                            <tbody>
                                <tr>
                                    <th>Nama</th>
                                    <td>{!!$detail['nama']!!}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Awal</th>
                                    <td>{!!$detail['tanggal_awal']!!}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Akhir</th>
                                    <td>{!!$detail['tanggal_akhir']!!}</td>
                                </tr>
                                <tr>
                                    <th>Deskripsi</th>
                                    <td>{!!$detail['deskripsi']!!}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    @if($detail['is_done'] == 0)
                                        <td>
                                            <span class="label label-warning">Belum berjalan</span>
                                        </td>
                                    @elseif($detail['is_done'] == 1)
                                        <td>
                                            <span class="label label-danger">Selesai</span>
                                        </td>
                                    @elseif($detail['is_done'] == 2)
                                        <td>
                                            <span class="label label-success">Sedang berjalan</span>
                                        </td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><i class="fa fa-tasks fa-fw"></i>  Tools Project ({{ count($detail['tools'])}} tools)</strong>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-hover show-table">
                            <thead>
                                <tr>
                                    <th>Nama Tools</th>
                                    <th>Status Penyelesaian</th>
                                </tr>
                            </thead>
                            <tbody> 
                                @foreach($detail['tools'] as $tools)
                                    <tr>
                                        <td>
                                           {!! $tools['judul'] !!}
                                        </td>
                                        <td>
                                            @if($tools['status'] == 0)
                                                <span class="label label-danger pull-left"> <i class="fa fa-times fa-fw"></i> Belum Menyelesaikan</span>
                                            @elseif($tools['status'] == 1)
                                                <a href="/backend/report/{{$tools['category']}}/{{$tools['slug']}}/{{$user_id}}"><span class="label label-success pull-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Laporan"> <i class="fa fa-check fa-fw"></i> Sudah Menyelesaikan</span></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    @stop