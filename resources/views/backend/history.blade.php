@extends('layouts.default')
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/backend/history-project">Riwayat Project</a></li>
                <li class="active">{{$info_project['name']}}</li>
            </ol>
        </div>
    @stop 

    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger"><i class="fa fa-area-chart fa-fw"></i>  Riwayat Tools</div>
                        @if($history_survey -> count() != 0)
                            <p>Laporan untuk riwayat tools telah tersedia. Klik "Lihat Hasil Tools" untuk melihat laporan menyeluruh tools tersebut.</p> 
                        @else
                            <p>Belum ada laporan untuk saat ini</p> 
                        @endif
                    </div>
                    <div class="panel-body full">          
                        @if($history_survey -> count() != 0)
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Nama Tools</th>
                                        <th class="text-center">Batas Akhir</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($history_survey as $key)
                                    <tr>
                                        <td>{!! $key['title'] !!}</td>
                                        <td class="text-center">
                                            {!! $key['end_date'] !!}
                                        </td>
                                        <td class="text-right">
                                            @if($key['category_survey_id'] == 1 || $key['category_survey_id'] == 2)
                                                {!! Form::open(['url' => 'export/excel-gti-tiki', 'method' => 'post',"class"=>"form-horizontal" ,'id'=>'excel-gti-tiki', 'data-submit'=>'noAjax']) !!}
                                                   <input type="hidden" name="slug" value="{!!$key['slug']!!}">
                                                   <input type="submit" class="btn btn-success" value="Download .xlsx">
                                                    <a href="/backend/report/{!! $key['slug'] !!}" class="btn btn-sm btn-primary act-reminder">Lihat Hasil Tools</a>
                                                {!! Form::close() !!}
                                            @elseif($key['category_survey_id'] == 3)
                                                {!! Form::open(['url' => 'export/excel-wpa-papi', 'method' => 'post',"class"=>"form-horizontal" ,'id'=>'excel-wpa-papi', 'data-submit'=>'noAjax']) !!}
                                                    <input type="hidden" name="slug" value="{!!$key['slug']!!}">
                                                    <input type="submit" class="btn btn-success" value="Download .xlsx">
                                                <a href="/backend/report/{!! $key['slug'] !!}" class="btn btn-sm btn-primary act-reminder">Lihat Hasil Tools</a>
                                                 {!! Form::close() !!}
                                            @else
                                                <a href="/backend/report/{!! $key['slug'] !!}" class="btn btn-sm btn-primary act-reminder">Lihat Hasil Tools</a>
                                            @endif


                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @stop
