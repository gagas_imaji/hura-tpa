@extends('layouts.default')
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/backend/history-project">Riwayat Project</a></li>
                <li class="active">Hasil Tools {!! $survey['title'] !!}</li>
            </ol>
        </div>
    @stop

    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if($rowset)
                            <div class="panel-title text-danger"><i class="fa fa-area-chart fa-fw"></i>  Hasil Tools Peserta</div>
                            <p>Berikut detail penilaian untuk tools {!! $survey['title'] !!}</p>

                            {!! Form::open(['url' => 'export/excel-gti-tiki', 'method' => 'post',"class"=>"form-horizontal" ,'id'=>'excel-gti-tiki', 'data-submit'=>'noAjax']) !!}
                               <input type="hidden" name="slug" value="{!!$slug!!}">
                               <input type="submit" class="btn btn-info" value="Download .xlsx">
                            {!! Form::close() !!}
                        @else
                            <div class="panel-title text-danger">
                                Penilaian Tools <strong>{!! $survey['title'] !!}</strong> masih belum lengkap
                            </div>
                        @endif
                    </div>
                    <div class="panel-body full">
                        <table class="table table-hover table-bordered" id="list-laporan-user">
                            <thead>
                                <tr class="headings">
                                    <th class="text-center">Nama</th>
                                    <th class="text-center"><?php echo ($survey['category_id'] == 1 ? 'Nilai GTQ' : 'IQ'); ?></th>
                                    <th class="text-center">Kriteria</th>
                                    <th class="action"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $report)
                                    <tr>
                                        <td>{!! $report['name'] !!}</td>
                                        <td class="text-center">
                                            {!! $report['nilai'] !!}
                                        </td>
                                        <td class="text-center">
                                            {!! $report['hasil'] !!}
                                        </td>
                                        <td class="text-center">
                                            <a href="/backend/report/{!!$survey['category_id']!!}/{!!$slug!!}/{!!$report['user_id']!!}" class="btn btn-xs btn-primary act-reminder" id="detail_{{$report['user_id']}}">
                                                <span class="pull-right">Lihat Detail</span>
                                            </a>
                                            @if($survey['category_id'] == 1)
                                                <a href="{{ url('print/gti/') }}/{{ $slug }}/{{ $report['user_id'] }}" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download PDF">
                                                  <i class="fa fa-download fa-fw"></i>
                                                </a>
                                            @else
                                                <a href="{{ url('print/tiki/') }}/{{ $slug }}/{{ $report['user_id'] }}" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download PDF">
                                                  <i class="fa fa-download fa-fw"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center text-danger">Mohon lengkapi dan himbau tim anda untuk menyelesaikan survey ini</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
        
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('#list-laporan-user').DataTable({
                    "columnDefs": [ {
                        "targets"  : 'action',
                        "orderable": false,
                        "order": []
                    }]
                });

            });
        </script>
    @stop
