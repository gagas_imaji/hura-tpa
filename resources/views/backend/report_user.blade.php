@extends('layouts.default')

    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Laporan User</li>
            </ol>

        </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger"><i class="fa fa-line-chart fa-fw"></i>  Laporan User</div>
                    </div>

                    <div class="panel-body full">
                        <form name="myForm" id="myForm" action="/backend/user-report" method="get" enctype="multipart/form-data" id="import" data-submit="noAjax">
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <select name="filter" class="form-control select-container full-width" id="filter">
                                        <option value="0">- Filter Project -</option>
                                        <option value="all">- Semua Project -</option>
                                        @if($filter_project!=null)
                                            @foreach($filter_project as $filter)
                                                @if($filter['id'] == $request_filter)
                                                    <option value="{{ $filter['id'] }}" selected="selected">{{ $filter['name'] }}</option>
                                                @else
                                                    <option value="{{ $filter['id'] }}">{{ $filter['name'] }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-lg-2 form-group">
                                    <input type="submit" class="btn btn-info" value="Filter Project">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body full">
                        <table id="user_report" class="table table-bordered table-hover text-center">
                            <thead>
                                <tr>
                                    <th colspan="3">Data User</th>
                                    <th colspan="4">Most</th>
                                    <th colspan="4">Less</th>
                                    <th colspan="4">Change</th>
                                    <th>DISC</th>
                                    <th colspan="6">LAI</th>
                                    <th colspan="6">TIKI</th>
                                    <th colspan="20">WBA</th>
                                    <th colspan="13">TPA</th>
                                </tr>
                                <tr>
                                    @foreach($col_heads as $column)
                                        <th>{{$column}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                            @if($rowset!=null)
                                @foreach($rowset as $row)
                                    <tr>
                                        <td class="text-left">{{$row['email']}}</td>
                                        <td class="text-left">{{$row['first_name']}}</td>
                                        <td>{{$row['tujuan']}}</td>
                                        <td>{{$row['most_d']}}</td>
                                        <td>{{$row['most_i']}}</td>
                                        <td>{{$row['most_s']}}</td>
                                        <td>{{$row['most_c']}}</td>
                                        <td>{{$row['lest_d']}}</td>
                                        <td>{{$row['lest_i']}}</td>
                                        <td>{{$row['lest_s']}}</td>
                                        <td>{{$row['lest_c']}}</td>
                                        <td>{{$row['change_d']}}</td>
                                        <td>{{$row['change_i']}}</td>
                                        <td>{{$row['change_s']}}</td>
                                        <td>{{$row['change_c']}}</td>
                                        <td>{{$row['hasil']}}</td>
                                        <td>{{$row['GTQ1']}}</td>
                                        <td>{{$row['GTQ2']}}</td>
                                        <td>{{$row['GTQ3']}}</td>
                                        <td>{{$row['GTQ4']}}</td>
                                        <td>{{$row['GTQ5']}}</td>
                                        <td>{{$row['average']}}</td>
                                        <td>{{$row['SS1']}}</td>
                                        <td>{{$row['SS2']}}</td>
                                        <td>{{$row['SS3']}}</td>
                                        <td>{{$row['SS4']}}</td>
                                        <td>{{$row['jumlah']}}</td>
                                        <td>{{$row['IQ']}}</td>
                                        <td>{{$row['N']}}</td>
                                        <td>{{$row['G']}}</td>
                                        <td>{{$row['A']}}</td>
                                        <td>{{$row['L']}}</td>
                                        <td>{{$row['P']}}</td>
                                        <td>{{$row['I']}}</td>
                                        <td>{{$row['T']}}</td>
                                        <td>{{$row['V']}}</td>
                                        <td>{{$row['X']}}</td>
                                        <td>{{$row['S']}}</td>
                                        <td>{{$row['B']}}</td>
                                        <td>{{$row['O']}}</td>
                                        <td>{{$row['R']}}</td>
                                        <td>{{$row['D']}}</td>
                                        <td>{{$row['C']}}</td>
                                        <td>{{9-$row['Z']}}</td>
                                        <td>{{$row['E']}}</td>
                                        <td>{{9-$row['K']}}</td>
                                        <td>{{$row['F']}}</td>
                                        <td>{{$row['W']}}</td>
                                        @if($row['log_berpikir']==null || $row['log_berpikir']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['log_berpikir']}}</td>
                                        @endif
                                        @if($row['k_numerikal']==null || $row['k_numerikal']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['k_numerikal']}}</td>
                                        @endif
                                        @if($row['d_analisa']==null || $row['d_analisa']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['d_analisa']}}</td>
                                        @endif
                                        @if($row['k_verbal']==null || $row['k_verbal']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['k_verbal']}}</td>
                                        @endif
                                        @if($row['o_hasil']==null || $row['o_hasil']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['o_hasil']}}</td>
                                        @endif
                                        @if($row['fleks']==null || $row['fleks']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['fleks']}}</td>
                                        @endif
                                        @if($row['s_kerja']==null || $row['s_kerja']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['s_kerja']}}</td>
                                        @endif
                                        @if($row['m_prestasi']==null || $row['m_prestasi']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['m_prestasi']}}</td>
                                        @endif
                                        @if($row['kerjasama']==null || $row['kerjasama']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['kerjasama']}}</td>
                                        @endif
                                        @if($row['k_interpersonal']==null || $row['k_interpersonal']<0)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['k_interpersonal']}}</td>
                                        @endif
                                        @if($row['pil_jabatan']==null)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['pil_jabatan']}}</td>
                                        @endif
                                        @if($row['prosen_kecocokan']==null)
                                            <td>-</td>
                                        @else
                                            <td>{{$row['prosen_kecocokan']}}</td>
                                        @endif
                                        @if($row['hasil_rekomendasi']==1)
                                            <td>Tidak Disarankan</td>
                                        @elseif($row['hasil_rekomendasi']==2)
                                            <td>Dipertimbangkan</td>
                                        @elseif($row['hasil_rekomendasi']==3)
                                            <td>Disarankan</td>
                                        @else
                                            <td>-</td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $('#user_report').DataTable( {
                "scrollX": true,
                "fixedColumns":  {
                    leftColumns: 1
                },
                "columnDefs": [
                    { className: "wpa_column", "targets": [ 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ] },
                    { className: "lai_column", "targets": [ 16, 17, 18, 19, 20, 21 ] },
                    { className: "tiki_column", "targets": [ 22, 23, 24, 25, 26, 27 ] },
                    { className: "wba_column", "targets": [ 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 ] },
                    { className: "tpa_column", "targets": [ 48,49,50,51,52,53,54,55,56,57,58,59,60 ] }
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'columnToggle',
                        text: 'WPA',
                        columns: '.wpa_column'
                    },
                    {
                        extend: 'columnToggle',
                        text: 'LAI',
                        columns: '.lai_column'
                    },
                    {
                        extend: 'columnToggle',
                        text: 'TIKI',
                        columns: '.tiki_column'
                    },
                    {
                        extend: 'columnToggle',
                        text: 'WBA',
                        columns: '.wba_column'
                    },
                    {
                        extend: 'columnToggle',
                        text: 'TPA',
                        columns: '.tpa_column'
                    },
                    {
                        extend: "excel",
                        text: "Download .xlsx",
                        filename: "Score User Project - {{ $projectname }} - Jabatan - {{ $namajabatan }}",
                        exportOptions: {
                            columns: ':visible'
                        }
                    }
                ]
            } );
        } );
    </script>
    @stop
