@extends('layouts.default')

@section('content')
    <div class="row" >
        <div class="col col-md-12 panel panel-default">
            <div class="panel-body">
                <div id="breadcrumb">
                    <ol class="breadcrumb">
                        <li><a href="/home">Home</a></li>
                        <li><a href="/laporan">Laporan</a></li>
                        <li class="active">{{ $info_project['nama'] }}</li>
                    </ol>
                </div>
                <div class="col col-md-12">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3><i class="fa fa-area-chart"></i> Laporan {{ $info_project['nama'] }}</h3>
                        </div>
                        <div class="panel-body">
                            
                            <?php $i = 0; ?>
                            <div class="col col-md-4">
                                @foreach($history_survey as $key)
                                    <table class="table jambo_table bulk_action table-bordered">
                                        <thead>
                                          <tr class="headings">
                                            <th class="column-title">
                                                {{ $key['judul'] }}  
                                            </th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $key['deskripsi'] }}</td> 
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="/laporan/tools/{{$info_project['slug']}}/{{ $key['slug']}}" class="btn btn-success pull-right">
                                                    <i class="fa fa-search"></i> Lihat Laporan
                                                </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <?php $i++; ?>
                                    </table>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
