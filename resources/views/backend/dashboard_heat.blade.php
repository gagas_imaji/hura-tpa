<div class="panel-heading">
    <div class="panel-title text-danger">Hasil Survey Terbaru</div>
    <p>Laporan untuk Survey Kinerja dan Keterlibatan Pegawai telah tersedia. Klik tanda panah untuk melihat rincian singkat masing-masing survey dan klik "Hasil Survey Terbaru" untuk melihat laporan menyeluruh survey tersebut.</p>
</div>
    <div class="panel panel-body col-md-14">
        <div class="row">
            <div class="col-md-6 panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-7 padding-top-20">
                                <div class="panel-title fs-15">Kinerja</div>
                                <div class="progress ">
                                    @if (round($current_report['avg_perf'], 2) >= 85)
                                        <?php $labelinfo = "info" ?>
                                    @elseif (round($current_report['avg_perf'], 2) >= 70 && round($current_report['avg_perf'], 2) <= 84)
                                        <?php $labelinfo = "success" ?>
                                    @elseif (round($current_report['avg_perf'], 2) >= 60 && round($current_report['avg_perf'], 2) <= 69)
                                        <?php $labelinfo = "warning" ?>
                                    @elseif (round($current_report['avg_perf'], 2) <= 59)
                                        <?php $labelinfo = "danger" ?>
                                    @endif
                                    <div class="progress-bar progress-bar-{!! $labelinfo !!}" role="progressbar" aria-valuenow="{!! round($current_report['avg_perf'], 2) !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $current_report['avg_perf'] !!}%"></div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <h3><div class="report-text smaller pull-right">
                                    {!! round($current_report['avg_perf'], 1) !!}%
                                <a class="collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><i class="fa fa-caret-down fa-fw"></i></a>
                                </div></h3>
                            </div>
                        </div>
                    </div>
                    <div id="collapseOne" class="collapse">
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Section Name</th>
                                        <th>Section Score</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($current_report['performance'] as $row)
                                        <tr>
                                            <td>
                                                <?php echo $row['name']; ?>
                                            </td>
                                            <td>
                                                <div class="col-md-3 ml-25">
                                                    <strong class="tbl-percent">{!! convert_percent(round($row['score'], 2), 5) !!}%</strong>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="progress ">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{!! convert_percent(round($row['score'], 2), 5) !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! convert_percent(round($row['score'], 2), 5) !!}%"></div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div id="chartPerformance" style="height: 400px; display: none"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                                <div class="col-md-7 padding-top-20">
                                    <div class="panel-title fs-15">Keterlibatan</div>
                                    <div class="progress ">
                                        @if (round($current_report['avg_eng'], 2) >= 85)
                                            <?php $labelinfo = "info" ?>
                                        @elseif (round($current_report['avg_eng'], 2) >= 70 && round($current_report['avg_eng'], 2) <= 84)
                                            <?php $labelinfo = "success" ?>
                                        @elseif (round($current_report['avg_eng'], 2) >= 60 && round($current_report['avg_eng'], 2) <= 69)
                                            <?php $labelinfo = "warning" ?>
                                        @elseif (round($current_report['avg_eng'], 2) <= 59)
                                            <?php $labelinfo = "danger" ?>
                                        @endif
                                        <div class="progress-bar progress-bar-{!! $labelinfo !!}" role="progressbar" aria-valuenow="{!! round($current_report['avg_eng'], 2) !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $current_report['avg_eng'] !!}%"></div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h3>
                                    <div class="report-text smaller pull-right">
                                        {!! round($current_report['avg_eng'], 2) !!}%
                                        <a class="collapsed" data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7"><i class="fa fa-caret-down fa-fw"></i></a>
                                    </div>
                                    </h3>
                                </div>
                        </div>
                    </div>
                    <div id="collapse7" class="collapse">
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Section Name</th>
                                        <th>Section Score</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach ($current_report['engagement'] as $row)
                                            <tr>
                                                <td style="width: 163px"><?php echo $row['name']; ?></td>
                                                <td>
                                                    <div class="col-md-3 ml-25">
                                                        <strong class="tbl-percent"><?php echo convert_percent(round($row['score'], 2), 5) ?>%</strong>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="progress ">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{!! convert_percent(round($row['score'], 2), 5) !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! convert_percent(round($row['score'], 2), 5) !!}%"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                </tbody>
                            </table>
                            <div id="chartEngagement" style="height: 400px; display: none;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center padding-top-20">
                <a href="backend/report-heat/{!!$id_survey!!}" class="btn btn-sm btn-info" id="result-link">Hasil Survey Terbaru</a> 
            </div>
        </div>
    </div>