@extends('layouts.default')
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li class="active">Riwayat Project</li>
            </ol>
        </div>
    @stop 
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title text-danger"><i class="fa fa-area-chart fa-fw"></i>  Riwayat Project</span>
                        @if($rowset)
                            <p>Laporan untuk riwayat tools telah tersedia. Klik "Lihat Laporan" untuk melihat laporan menyeluruh tools tersebut.</p> 
                        @else
                            <p></p> 
                        @endif
                    </div>
                    <div class="panel-body full"> 
                        <table class="table table-hover table-bordered" id="list-history-project">
                            <thead>
                                <tr class="headings">
                                    <th class="text-center">Nama Project</th>
                                    <th class="text-center">Batas Akhir</th>
                                    <th class="action"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $row)
                                        <tr>
                                            <td>{!! $row['nama_project'] !!}</td>
                                            <td class="text-center">{!! $row['batas_akhir'] !!}</td>
                                            <td class="text-center">{!! $row['detail'] !!}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3" class="alert alert-info">Anda belum memiliki laporan project. Silahkan himbau peserta untuk menyelesaikan project yang ada.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('#list-history-project').DataTable({
                    "columnDefs": [ {
                        "targets"  : 'action',
                        "orderable": false,
                        "order": []
                    }]

                });

            });
        </script>
    @stop
