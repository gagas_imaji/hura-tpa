@extends('layouts.default')

<style>
      .carousel-inner > .item > img,
      .carousel-inner > .item > a > img {
          width: 70%;
          margin: auto;
      }
</style>

@section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Dashboard</a></li>
                <li class="active">Laporan Survey HEAT</li>
            </ol>
        </div>
@stop
@section('content')
           

<!-- Main content -->
<section class="content">

    <div class="panel panel-transparent">
        <div class="panel-heading padding-bottom-20">
            <div class="panel-title">
                @if(($data['result']))
                    Hasil Survey HEAT untuk {!! ucfirst($data['corporate_name']) !!}
                @else
                    Penilaian saat ini masih belum lengkap. Mohon Lengkapi dan Himbau Tim anda untuk melengkapi Assestmen ini
                @endif
            </div>
        </div>
        <div class="panel-body">
            <div class="row padding-top-30">
                <div class="col-md-6">
                    <div class="caption-full">
                        @if (($data['result'])) 
                            @foreach ($data['result'] as $key => $row) 
                                <input type="hidden" value="x:{!! round($row['x'], 2) !!} y:{!! round($row['y'], 2) !!}" x="{!! round($row['x'], 2) !!}" y="{!! round($row['y'], 2) !!}" n="{!! $row['name'] !!}" class="performance" id="{!! $key !!}" />
                            @endforeach
                        @endif
                        <div id="chartContainer" style="height: 500px; width: 100%">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="margin-top-30">
                        <div class="list-group report-group">
                            <button type="button" class="list-group-item" data-toggle="modal" data-target="#engaged">
                                <span class="report-text"><?php echo round($data['detail_statistik']['engaged']/100, 2) ?>%</span>
                                 <span class="fs-15 text-uppercase"><span class="legend-report egd">E</span>Hero </span>
                                <span class="label pull-right">Lihat Detil</span>
                            </button>
                            <button type="button" class="list-group-item" data-toggle="modal" data-target="#almostengage">
                                <span class="report-text"><?php echo round($data['detail_statistik']['almostengage']/100, 2) ?>%</span>
                                <span class="fs-15 text-uppercase"><span class="legend-report ae">E</span>Harvest</span>
                                <span class="label pull-right">Lihat Detil</span>
                            </button>
                            <button type="button" class="list-group-item" data-toggle="modal" data-target="#hamster">
                                <span class="report-text"><?php echo round($data['detail_statistik']['hamster']/100, 2) ?>%</span>
                                <span class="fs-15 text-uppercase"><span class="legend-report hm">E</span>  Hunter</span>
                                <span class="label pull-right">Lihat Detil</span>
                            </button>
                            <button type="button" class="list-group-item" data-toggle="modal" data-target="#crash">
                                <span class="report-text"><?php echo round($data['detail_statistik']['crash']/100, 2) ?>%</span>
                                <span class="fs-15 text-uppercase"><span class="legend-report crs">E</span> Hamster </span>
                                <span class="label pull-right">Lihat Detil</span>
                            </button>
                            <button type="button" class="list-group-item" data-toggle="modal" data-target="#disengaged">
                                <span class="report-text"><?php echo round($data['detail_statistik']['disengaged']/100, 2) ?>%</span>
                               <span class="fs-15 text-uppercase"><span class="legend-report deg">E</span> Hobo </span>
                                <span class="label pull-right">Lihat Detil</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row padding-top-30">
                <div class="col-md-6 panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-8 padding-top-20">
                                    <div class="panel-title fs-15">Kinerja</div>
                                    <div class="progress ">
                                        @if (round($data['report']['avg_perf'], 2) >= 85) 
                                            <?php $labelinfo = "blue" ?>
                                        @elseif (round($data['report']['avg_perf'], 2) >= 70 && round($data['report']['avg_perf'], 2) <= 84) 
                                            <?php $labelinfo = "green" ?>
                                        @elseif (round($data['report']['avg_perf'], 2) >= 60 && round($data['report']['avg_perf'], 2) <= 69) 
                                            <?php $labelinfo = "yellow" ?>
                                        @elseif (round($data['report']['avg_perf'], 2) <= 59) 
                                            <?php $labelinfo = "red" ?>
                                        @endif
                                        <div class="progress-bar progress-bar-{!! $labelinfo !!}" role="progressbar" aria-valuenow="{!! round($data['report']['avg_perf'], 2) !!}%" aria-valuemin="0" aria-valuemax="100" style="width: {!! round($data['report']['avg_perf'], 2) !!}%"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="report-text smaller pull-right">
                                        <h3><?php echo round($data['report']['avg_perf'], 1) ?>%
                                        <a class="collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><i class="fa fa-caret-down fa-fw"></i></a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="collapseOne" class="collapse in">
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th style="padding-left: 0px;">Skor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (($data['report']['performance'])) : ?>
                                            <?php foreach ($data['report']['performance'] as $row) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $row['name']; ?>
                                                    </td>
                                                    <td style="padding: 20px 0px;">
                                                        <div class="col-md-12 ml-15">
                                                            <strong class="tbl-percent-sm"><?php echo convert_percent(round($row['score'], 2), 5) ?>%</strong>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="progress progress_sm">
                                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{!! convert_percent(round($row['score'], 2), 5) !!}%" aria-valuemin="0" aria-valuemax="100" style="width: {!! convert_percent(round($row['score'], 2), 5) !!}%"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-8 padding-top-20">
                                    <div class="panel-title fs-15">Keterlibatan</div>
                                    <div class="progress ">
                                        @if (round($data['report']['avg_eng'], 2) >= 85) 
                                            <?php $labelinfo = "info" ?>
                                        @elseif (round($data['report']['avg_eng'], 2) >= 70 && round($data['report']['avg_eng'], 2) <= 84) 
                                            <?php $labelinfo = "sucess" ?>
                                        @elseif (round($data['report']['avg_eng'], 2) >= 60 && round($data['report']['avg_eng'], 2) <= 69) 
                                            <?php $labelinfo = "warning" ?>
                                        @elseif (round($data['report']['avg_eng'], 2) <= 59) 
                                            <?php $labelinfo = "danger" ?>
                                        @endif
                                        <div class="progress-bar progress-bar-{!! $labelinfo !!}" role="progressbar" aria-valuenow="{!! round($data['report']['avg_eng'], 2) !!}%" aria-valuemin="0" aria-valuemax="100" style="width: {!! round($data['report']['avg_eng'], 2) !!}%"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="report-text smaller pull-right">
                                        <h3><?php echo round($data['report']['avg_eng'], 2) ?>%
                                        <a class="collapsed" data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7"><i class="fa fa-caret-down fa-fw"></i></a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="collapse7" class="collapse in">
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th style="padding-left: 0px;">Skor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (($data['report']['engagement'])) 
                                            @foreach ($data['report']['engagement'] as $row) 
                                                <tr>
                                                    <td>
                                                        {!! $row['name'] !!}
                                                    </td>
                                                    <td style="padding: 20px 0px;">
                                                        <div class="col-md-12 ml-15">
                                                            <strong class="tbl-percent-sm">{!! convert_percent(round($row['score'], 2), 5) !!}%</strong>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="progress progress_sm">
                                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{!! convert_percent(round($row['score'], 2), 5) !!}%" aria-valuemin="0" aria-valuemax="100" style="width: {!! convert_percent(round($row['score'], 2), 5) !!}%"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="engaged" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span class="semi-bold">Analisis</span></h5>
                    <p class="p-b-10 mt-15">Analisis lengkap tentang survey yang telah dilakukan</p>
                </div>
                <div class="modal-body">
                    <span class="label fs-30">{!! $data['detail_statistik']['engaged']/100 !!}% Hero</span>
                    <br><br>
                    {!! $data['analysis']['engaged'] !!}<br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Divisi</th>
                                <th>Performance</th>
                                <th>Engagement</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (($data['list_engaged'])) 
                                @foreach ($data['list_engaged'] as $key => $row) 
                                    <tr>
                                        <td>{!! $row['first_name'] !!}</td>
                                        <td>{!! ucfirst($row['division']) !!}</td>
                                        <td>{!! ucfirst($row['level_name']) !!}</td>
                                        <td>{!! round($data['result'][$row['id']]['y'], 2) !!}</td>
                                        <td>{!! round($data['result'][$row['id']]['x'], 2) !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>       
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
</div>

<div class="modal fade slide-up disable-scroll" id="almostengage" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span class="semi-bold">Analisis</span></h5>
                    <p class="p-b-10 mt-15">Analisis lengkap tentang survey yang telah dilakukan</p>
                </div>
                <div class="modal-body">
                    <span class="label fs-30">{!! $data['detail_statistik']['almostengage']/100 !!}% Harvest</span>
                    <br><br>
                    {!! $data['analysis']['almostengage'] !!}<br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Divisi</th>
                                <th>Performance</th>
                                <th>Engagement</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (($data['list_almostengage'])) 
                                @foreach ($data['list_almostengage'] as $key => $row) 
                                    <tr>
                                        <td>{!! $row->first_name !!}</td>
                                        <td>{!! ucfirst($row->division) !!}</td>
                                        <td>{!! round($data['result'][$row->id]['y'], 2) !!}</td>
                                        <td>{!! round($data['result'][$row->id]['x'], 2) !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>       
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
</div>

<div class="modal fade slide-up disable-scroll" id="hamster" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span class="semi-bold">Analisis</span></h5>
                    <p class="p-b-10 mt-15">Analisis lengkap tentang survey yang telah dilakukan</p>
                </div>
                <div class="modal-body">
                    <span class="label fs-30">{!! $data['detail_statistik']['hamster']/100 !!}% Hunter</span>
                    <br><br>
                    {!! $data['analysis']['hamster'] !!}<br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Divisi</th>
                                <th>Performance</th>
                                <th>Engagement</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (($data['list_hamster'])) 
                                @foreach ($data['list_hamster'] as $key) 
                                    <tr>
                                        <td>{!! $row->first_name !!}</td>
                                        <td>{!! ucfirst($row->division) !!}</td>
                                        <td>{!! round($data['result'][$row->id]['y'], 2) !!}</td>
                                        <td>{!! round($data['result'][$row->id]['x'], 2) !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>       
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
</div>

<div class="modal fade slide-up disable-scroll" id="crash" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span class="semi-bold">Analisis</span></h5>
                    <p class="p-b-10 mt-15">Analisis lengkap tentang survey yang telah dilakukan</p>
                </div>
                <div class="modal-body">
                    <span class="label fs-30">{!! $data['detail_statistik']['crash']/100 !!}% Hamster</span>
                    <br><br>
                    {!! $data['analysis']['crash'] !!}<br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Divisi</th>
                                <th>Performance</th>
                                <th>Engagement</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (($data['list_crash'])) 
                                @foreach ($data['list_crash'] as $key => $row) 
                                    <tr>
                                        <td>{!! $row->first_name !!}</td>
                                        <td>{!! ucfirst($row->division) !!}</td>
                                        <td>{!! round($data['result'][$row->id]['y'], 2) !!}</td>
                                        <td>{!! round($data['result'][$row->id]['x'], 2) !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>       
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
</div>

<div class="modal fade slide-up disable-scroll" id="disengaged" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span class="semi-bold">Analisis</span></h5>
                    <p class="p-b-10 mt-15">Analisis lengkap tentang survey yang telah dilakukan</p>
                </div>
                <div class="modal-body">
                    <span class="label fs-30">{!! $data['detail_statistik']['disengaged']/100 !!}% Hobo</span>
                    <br><br>
                    {!! $data['analysis']['disengaged'] !!}<br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Divisi</th>
                                <th>Performance</th>
                                <th>Engagement</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (($data['list_disengaged'])) 
                                @foreach ($data['list_disengaged'] as $key => $row) 
                                    <tr>
                                        <td>{!! $row->first_name !!}</td>
                                        <td>{!! ucfirst($row->division) !!}</td>
                                        <td>{!! round($data['result'][$row->id]['y'], 2) !!}</td>
                                        <td>{!! round($data['result'][$row->id]['x'], 2) !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>       
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo asset('assets/js/canvasjs.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('assets/js/result_admin.js'); ?>"></script>
<script>
    // $(document).ready(function(){
    function report() {
        var performance = $('.performance').val();
        var perf = [];
        $(".performance").each(function (index) {
            perf.push(
                {
                    id: $(this).attr('n'),
                    x: parseFloat($(this).attr('x')),
                    y: parseFloat($(this).attr('y')),
                    color : $(this).attr('c')
                }
            )
        });

        var d = new Date();
        var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
        current_month = d.getMonth();
        current_year = d.getFullYear();
        current = m_names[current_month] + '-' + current_year;
        var chart = new CanvasJS.Chart("chartContainer",
                {
                    zoomEnabled: true,
                    title: {
                        text: "Grafik Survey",
                        fontSize: 20
                    },
                    backgroundColor: "",
                    animationEnabled: true,
                    axisX: {
                        title: "Engagement",
                        minimum: 0,
                     /*   labelFormatter: function ( e ) {
                               return "";  
                         }  ,*/
                        gridThickness : 0,
                        tickThickness : 1, 
                        lineThickness: 1,
                        //labelAngle: -40,
                        maximum: 6,
                        interval: 1,
                        labelFontSize: 14,
                        titleFontSize: 18
                    },
                    axisY: {
                        title: "Performance",
                        minimum: 0,
                      /*  labelFormatter: function ( e ) {
                               return "";  
                         }  ,*/
                        //labelAngle: -40,
                        lineThickness: 1,
                         gridThickness : 0,
                        tickThickness : 1,
                        maximum: 5,
                        interval: 1,
                        labelFontSize: 14,
                        titleFontSize: 18
                    },
                    data: [
                        {
                            
                            type: "scatter",
                          
                      /*      toolTipContent: "<span style='\"'color: {color};'\"'><strong>Name: </strong></span>{id}<br/><span style='\"'color: {color};'\"'><strong>Engagement: </strong></span>{x}<br/><span style='\"'color: {color};'\"'><strong>Performance: </strong></span>{y}<br/>  ", */
                           toolTipContent: "<span style='\"'color: {color};'\"'><strong>ID: </strong></span>{id}<br/><span style='\"'color: {color};'\"'><strong>Engagement: </strong></span>{x}<br/><span style='\"'color: {color};'\"'><strong>Performance: </strong></span>{y}<br/>  ",
                            dataPoints: perf
                        }

                    ]
                });
        
        chart.render();
    }

// });
</script>
@stop