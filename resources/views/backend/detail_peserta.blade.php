@extends('layouts.default')
<style>
      .carousel-inner > .item > img,
      .carousel-inner > .item > a > img {
          width: 70%;
          margin: auto;
      }
</style>

@section('breadcrumb')
    <div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="/home">Dashboard</a></li>
            <li class="active">Detail Peserta Survey</li>
        </ol>
    </div>
@stop

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->
        <div class="panel panel-transparent">
            <div id="id-info"></div>
            <div class="panel-heading">
                <div class="panel-title text-danger">Detail Peserta Survey {!! $survey['title'] !!} </div>
                <p>Berikut daftar seluruh peserta yang mengikuti survey {!! $survey['title'] !!} </p> 
            </div>
            <div class="panel panel-body">          
                <div class="row">
                    <table class="table table-hover table-noborder">
                        <tr>
                            <th>No</th>
                            <th>Nama </th>
                            <th>Divisi</th>
                            <th>Status</th>
                        </tr>
                        <?php $i = 1; ?>
                        @foreach($user as $data)
                        <tr>
                            <td>{!! $i !!}</td>
                            <td>{!! $data['first_name'] !!}</td>
                            <td>{!! $data['divisi'] !!}</td>
                            <td>
                                @if($data['is_done'] == 1)
                                    <span class="label label-success">Sudah ikuti survey</span>
                                @else
                                    <span class="label label-danger">Belum ikuti survey</span>
                                @endif
                            </td>
                        </tr>
                        <?php $i++; ?>
                            @endforeach
                    </table>
                    <center>{!! $user->appends(Input::except('page'))->render() !!}</center>
                </div>
            </div>
        </div>    
    </section>
    <script type="text/javascript">
        
    </script>
@stop
