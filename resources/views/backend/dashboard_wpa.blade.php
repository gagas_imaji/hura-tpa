<div class="panel-heading">
    <div class="panel-title text-danger">Hasil Tools Terbaru</div>
    <p>Laporan untuk test telah tersedia. Klik tanda panah untuk melihat rincian singkat masing-masing test.</p>
</div>

<div class="panel panel-body">
    <div class="col-md-6 panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8 padding-top-20">
                        <div class="panel-title fs-15">{!! $report['name'] !!}</div>
                        <div class="progress">
                            @if (round($report['avg_survey'], 2) >= 850) 
                                <?php $labelinfo = "info" ?>
                            @elseif (round($report['avg_survey'], 2) >= 700 && round($report['avg_survey'], 2) <= 840) 
                                <?php $labelinfo = "success" ?>
                            @elseif (round($report['avg_survey'], 2) >= 600 && round($report['avg_survey'], 2) <= 690) 
                                <?php $labelinfo = "warning" ?>
                            @elseif (round($report['avg_survey'], 2) <= 590) 
                                <?php $labelinfo = "danger" ?>
                            @endif
                            <div class="progress-bar progress-bar-{!! $labelinfo !!}" role="progressbar" aria-valuenow="{!! round($report['avg_survey'], 2) !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $report['avg_survey']/100*100 !!}%" > </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="report-text smaller pull-right">
                            <h3><?php echo round($report['avg_survey'], 2) ?>%
                            <a class="collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><i class="fa fa-caret-down fa-fw"></i></a></h3>
                            <label>Rata-rata</label>
                        </div>
                    </div>
                </div>
            </div>
            <div id="collapseOne" class="collapse">
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Divisi</th>
                                <th class="pull-right">Skor Rata-rata</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Dominance
                                </td>
                                <td>
                                    <div class="col-md-3 ml-25 pull-right">
                                        <strong class="tbl-percent">{!! round($report['d'], 2) !!}</strong>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Influence
                                </td>
                                <td>
                                    <div class="col-md-3 ml-25 pull-right">
                                        <strong class="tbl-percent">{!! round($report['i'], 2) !!}</strong>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Steadiness
                                </td>
                                <td>
                                    <div class="col-md-3 ml-25 pull-right">
                                        <strong class="tbl-percent">{!! round($report['s'], 2) !!}</strong>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Complience
                                </td>
                                <td>
                                    <div class="col-md-3 ml-25 pull-right">
                                        <strong class="tbl-percent">{!! round($report['c'], 2) !!}</strong>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <h3></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center padding-top-20">
        <p class="padding-top-10"><a href="backend/history" class="btn btn-sm btn-info">Lihat Riwayat Tools</a></p>
    </div>
</div>
