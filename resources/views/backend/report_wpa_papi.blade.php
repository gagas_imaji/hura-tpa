@extends('layouts.default')
    @section('breadcrumb')
            <div id="breadcrumb">
                <ol class="breadcrumb">
                    <li><a href="/home">Home</a></li>
                    <li><a href="/backend/history-project">Riwayat Project</a></li>
                    <li class="active">Hasil Tools <?php echo ($survey['category_id'] == 3 ? 'Work Personality Analytics' : ($survey['category_id'] == 4 ? 'Work Behavioural Assessment' : '')); ?></li>
                </ol>
            </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-md-12 panel panel-default">
                <div class="panel-heading">
                    @if($rowset)
                        <div class="panel-title text-danger"><i class="fa fa-area-chart fa-fw"></i>  Hasil Tools Peserta</div>
                        <p>Berikut detil penilaian untuk tools {!! $survey['title'] !!}</p>

                        @if($survey['category_id'] == 3 )
                            {!! Form::open(['url' => 'export/excel-wpa-papi', 'method' => 'post',"class"=>"form-horizontal" ,'id'=>'excel-wpa-papi', 'data-submit'=>'noAjax']) !!}
                                <input type="hidden" name="slug" value="{!!$slug!!}">
                                <input type="submit" class="btn btn-info" value="Download .xlsx">
                             {!! Form::close() !!}
                        @endif
                    @else
                        <div class="panel-title text-danger">Penilaian tools <strong>{!! $survey['title'] !!}</strong> masih belum lengkap</div>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="responsive">                
                        <table class="table table-bordered table-hover" id="list-laporan-wpa-papi">
                            <thead>
                                <tr class="headings">
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Hasil</th>
                                    <th class="text-center">Tipe Kepribadian</th>
                                    <th class="action"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $report)
                                    <tr>
                                        <td>{!! $report['first_name'] !!}</td>
                                        <td class="text-center">{!! $report['hasil'] !!}</td>
                                        <td class="text-center">{!! $report['type'] !!}</td>
                                        <td class="text-center">
                                            <a href="/backend/report/{!!$survey['category_id']!!}/{!!$slug!!}/{!!$report['user_id']!!}" class="btn btn-xs btn-primary" id="detail_{{$report['user_id']}}" >
                                                <span class="pull-right">Lihat Detil</span>
                                            </a>
                                            @if($survey['category_id'] == 3)
                                                <a href="{{ url('print/wpa/') }}/{{ $slug }}/{{ $report['user_id'] }}" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download PDF">
                                                  <i class="fa fa-download fa-fw"></i>
                                                </a>
                                            @elseif($survey['category_id'] == 4)
                                                <a href="{{ url('print/wba/') }}/{{ $slug }}/{{ $report['user_id'] }}" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download PDF">
                                                  <i class="fa fa-download fa-fw"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="4" class="text-center text-danger">Mohon lengkapi dan himbau tim anda untuk menyelesaikan survey ini</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('#list-laporan-wpa-papi').DataTable({
                    "columnDefs": [ {
                        "targets"  : 'action',
                        "orderable": false,
                        "order": []
                    }]
                });

            });
        </script>
    @stop
