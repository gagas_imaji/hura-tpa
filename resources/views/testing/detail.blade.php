@extends('layouts.default_survey')

    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li class="active">Tools</li>
            </ol>

        </div>
        
    @stop
    
    @section('content')
        
        <input type="hidden" name="id_user" value="{{ 0 }}" id="id_user" />
        <input type="hidden" name="slug" value="{{ $slug }}" id="slug" />
        <input type="hidden" name="section" value="{{ $section }}" id="section" />
        <input type="hidden" name="section_name" value="{{ $section_name }}" id="section_name" />
        <input type="hidden" name="id_survey" value="{{ $id_survey }}" id="id_survey" />
        <div id="tool-container" class="container animated fadeIn">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-title animated fadeInDown">
                            <h6 class="left grey-text text-lighten-1">{{ $section_name}}</h6>
                            <a class="tooltipped" data-position="bottom" data-delay="20" data-tooltip="{!! $instruction !!}" data-html="true"><i class="material-icons">info</i></a>
                            <h1 id="stopwatch_question" style="display: none;">00:00:00</h1>
                            @if($timer_survey/60 != 0) <!-- kalau ada timer survey -->
                                <input type="hidden" name="timer_survey" value="{{ $timer_survey - $used_timer}}" id="timer_survey" />
                                <input type="hidden" name="timer_section" value="0" id="timer_section" />
                                <input type="hidden" name="timer_question" value="0" id="timer_question" />
                                <span class="grey-text text-lighten-1">Sisa waktu: <span id="countdown_section" class="grey-text"></span></span>
                            @else <!-- kalau ga ada timer survey -->
                                <input type="hidden" name="timer_survey" value="0" id="timer_survey" />
                                @if($timer_section/60 != 0) <!-- kalau ada timer section -->
                                    <input type="hidden" name="timer_section" value="{{ $timer_section - $used_timer}}" id="timer_section" />
                                    <input type="hidden" name="timer_question" value="0" id="timer_question" />
                                    <span class="grey-text text-lighten-1">Sisa waktu: <span id="countdown_section" class="grey-text"></span></span>
                                @else
                                    <input type="hidden" name="timer_section" value="0" id="timer_section" />
                                    <input type="hidden" name="timer_question" value="{{ $timer - $used_timer }}" id="timer_question" />
                                    @if($timer/60 != 0)
                                        <span class="grey-text text-lighten-1">Sisa waktu: <span id="countdown_section" class="grey-text"></span></span>
                                    @endif
                                @endif
                                
                            @endif   
                        </div>  
                        <div class="card-content tool-question slides"> 
                            <div class="page-loader-wrapper" style="display: none; text-align: center;">
                                <div class="loader">
                                    <div class="preloader-wrapper">
                                        <div class="spinner-layer spinner-green-only">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Please wait...</p>
                                </div>
                            </div>
                            <?php $index_question = 0;?>
                            @foreach($content_question as $question)
                                <div slide-id="{!!$index_question!!}" class="slide <?php echo ($index_question == 0 ? 'active' : '')?>" >
                                {{ Form::hidden('id_type_question', $question['id_type_question'], ['id' => 'id_type_question'])}}
                                {{ Form::hidden('is_mandatory', $question['is_mandatory'], ['id' => 'is_mandatory'])}}
                                    @if($question['id_type_question'] == 1) 
                                        <div class="tool-statement">
                                            <p class="flow-text">{!! $question['question'] !!}</p>
                                        </div>
                                        <div class="tool-option multiple-choice" data-toggle="buttons">
                                            <ul class="clearfix">
                                                <?php $index_option = 0; $alpha = 'A'?>
                                                @foreach($option_answer[$index_question] as $option)
                                                    @if($section_name == 'Subtest 3')
                                                        @if($index_option == 1)
                                                            <li class="option-text option-numeric">
                                                                <label class="btn btn-tool waves-effect waves-light disabled">
                                                                    <span class="option-item">
                                                                        @if($option -> image == '')
                                                                            {!! $option -> answer !!}
                                                                        @else
                                                                            <img src="/assets/image/{{$folder_image}}/{{$option -> image}}" />
                                                                        @endif
                                                                    </span> 

                                                                </label>
                                                            </li>
                                                            <?php $alpha--;?>

                                                        @else

                                                            <li class="option-text option-numeric animated flipInX ">
                                                                <label class="btn btn-tool waves-effect waves-light">
                                                                    <input type="radio" name="answer" value="{{ $option -> id}}" id="id_answer<?php echo $index_option;?>" autocomplete="off" data-id_question="{{$id_question[$index_question]}}" class="default">
                                                                    <span class="order-alpha" style="display: none;">{{$alpha}}</span>
                                                                    <span class="option-item">
                                                                        @if($option -> image == '')
                                                                            {!! $option -> answer !!}
                                                                        @else
                                                                            <img src="/assets/image/{{$folder_image}}/{{$option -> image}}" />
                                                                        @endif
                                                                    </span> 

                                                                </label>
                                                            </li>
                                                            <?php $alpha++;?>
                                                        @endif
                                                        <?php $index_option++;?>
                                                    @else
                                                        <li class="option-text <?php echo (is_numeric($option -> answer) ? ($section_name == 'Subtest 4' ? '' : ' option-numeric') : ($option -> image != '' ? 'option-img' : '')); ?> animated flipInX" >
                                                            <label class="btn btn-tool waves-effect waves-light">
                                                                <input type="radio" name="answer" value="{{ $option -> id}}" id="id_answer<?php echo $index_option;?>" autocomplete="off" data-id_question="{{$id_question[$index_question]}}">
                                                                <span class="order-alpha" style="display: none;">{{$alpha}}</span>
                                                                <span class="option-item">
                                                                    @if($option -> image == '')
                                                                        {!! $option -> answer !!}
                                                                    @else
                                                                        <img src="/assets/image/{{$folder_image}}/{{$option -> image}}" />
                                                                    @endif
                                                                </span> 

                                                            </label>
                                                        </li>
                                                    <?php $index_option++; $alpha++;?>
                                                    @endif
                                                @endforeach
                                            </ul>                                               
                                        </div>
                                        <div class="question-action-box text-center">
                                            <div class="question-action" style="display:none;">
                                                <button class="btn waves-effect waves-light next" id="next_{{$index_question}}"onClick="saveCache({!!$id_question[$index_question]!!})">Lanjut</button> 
                                            </div>
                                        </div>
                                    @elseif($question['id_type_question'] == 2)
                                        <div class="tool-statement">
                                            <p class="flow-text">{!! $question['question'] !!}</p>
                                        </div>
                                        <div class="tool-option multiple-choice" data-toggle="buttons">
                                            <ul class="clearfix">
                                                <?php $index_option = 0; $alpha = 'A'?>
                                                @foreach($option_answer[$index_question] as $option)
                                                    <li class="option-text <?php echo (is_numeric($option -> answer) ? ($section_name == 'Subtest 4' ? '' : 'option-numeric') : ($option -> image != '' ? ' option-img' : '')); ?> animated flipInX" >
                                                        <label class="btn btn-tool waves-effect waves-light">
                                                            <input type="checkbox" name="answer" value="{{ $option -> id}}" id="id_answer<?php echo $index_option;?>" autocomplete="off" data-id_question="{{$id_question[$index_question]}}" onChange="show_next({!!$id_question[$index_question]!!})">
                                                            <span class="order-alpha" style="display: none;">{{$alpha}}</span>
                                                            <span class="option-item">
                                                                @if($option -> image == '')
                                                                    {!! $option -> answer !!}
                                                                @else
                                                                    <img src="/assets/image/{{$folder_image}}/{{$option -> image}}" />
                                                                @endif
                                                            </span> 
                                                        </label>
                                                    </li>
                                                    <?php $index_option++; $alpha++;?>
                                                @endforeach
                                            </ul>                                               
                                        </div>
                                        <div class="question-action-box text-center">
                                            <div class="question-action" style="display: none;">
                                                <button class="btn waves-effect waves-light next" id="next_{{$index_question}}" onClick="saveCache({!!$id_question[$index_question]!!})">Lanjut</button> 
                                            </div>
                                        </div>
                                    @endif
                                </div>    
                            <?php $index_question++; ?>            
                            @endforeach
                        </div>
                        <div class="card-action animated fadeInUp">
                            <div class="row">
                                <div class="tool-action col l6">
                                    <button id="prev" class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="20" data-tooltip="Soal sebelumnya" id="prev"><i class="material-icons">keyboard_arrow_left</i></button>
                                    <button id="skip" class="btn-floating waves-effect waves-light tooltipped" data-position="top" data-delay="20" data-tooltip="Lewati soal ini" id="skip"><i class="material-icons">skip_next</i></button>
                                </div>
                                <div class="tool-progress col l3 right text-right">
                                    <div class="question-answered grey-text text-lighten-1">
                                        <input type="hidden" name="count_answered" value="{{$count}}" id="val_answered"/>
                                        <input type="hidden" name="total" value="{{$total}}" id="val_total"/>
                                        <span id="count_answered">0</span>
                                         dari  {!! $total !!} soal dijawab
                                    </div>
                                    <div class="progress">
                                        <div class="determinate" style="width: 0%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>    
        <input type="hidden" name="length_question" value="{{ $index_question }}" id="length_question" />      
        
    @stop


    @section('script')
        <script src="<?php echo asset('assets/vendor/pace/js/pace.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/modernizr/modernizr.custom.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/smoothState/jquery.smoothState.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/js/materialize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/js/tool-page.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">

            //set token
            $.ajaxSetup({
               headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
            });

            //set all variable
            var id_type_question= ($('#id_type_question').val());
            var timer_survey    = $('#timer_survey').val();
            var timer_question  = $('#timer_question').val();
            var timer_section   = $('#timer_section').val();
            var id_user         = $('#id_user').val();
            var option          = document.getElementsByName('answer');
            var length_answer   = option.length;
            var length_question = $('#length_question').val();
            var slug            = $('#slug').val();
            var id_survey       = $('#id_survey').val();
            var count_survey    = $('#count_survey').val();
            var is_mandatory    = $('#is_mandatory').val();
            var section         = $('#section').val();
            var section_name    = $('#section_name').val();
            var count_answered  = 0;    
            var witdh_progress  = 0;
            var used            = 0;
            var element_answered;
            var list_number     = []; //untuk pengecekan nomor yang sudah dijawab. jika ada yang sama, maka count_answered tidak bertambah 
            var answer          = [];
            var i, j;

            //set css option answer
            $('.option-numeric').parent().addClass('list-inline');
            $('.option-img').parent().addClass('list-inline');

            //set timer survey
            if(timer_survey != 0){
                countdown_survey("countdown_survey", timer_survey/60, 0);
                function countdown_survey( elementName, minutes, seconds ){
                    if(timer_survey != 0){
                        var element_s, endTime_s, hours_s, mins_s, msLeft_s, time_s;

                        function twoDigits( n ){
                            return (n <= 9 ? "0" + n : n);
                        }

                        function updateTimer(){
                            msLeft_s = endTime_s - (+new Date);
                            used++;
                            if ( msLeft_s < 1000 ){
                                element_s.innerHTML = "Waktu habis";
                                $('#skip').hide();
                                $('#next').hide();
                                var i;                  
                                $('#myModal').modal(); 
                                $.ajax({
                                    url : "/test-time-out",    
                                    data: {id_survey: id_survey},
                                    type: "POST"
                                });
                                setTimeout(function(){
                                    $('#myModal').modal('hide')
                                    // window.location = "/survey/save-report/"+slug+"/"+section;
                                }, 5000);
                            } else {
                                time_s = new Date( msLeft_s );
                                hours_s = time_s.getUTCHours();
                                mins_s = time_s.getUTCMinutes();
                                element_s.innerHTML = (hours_s ? hours_s + ':' + twoDigits( mins_s ) : mins_s) + ':' + twoDigits( time_s.getUTCSeconds() );
                                setTimeout( updateTimer, time_s.getUTCMilliseconds() + 500 );
                            }
                        }

                        element_s = document.getElementById( elementName );
                        endTime_s = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                        
                        updateTimer();
                    }
                    
                }
            }

            //set timer section
            if(timer_section != 0){
                countdown_section("countdown_section", timer_section/60, 0);
                function countdown_section( elementName, minutes, seconds ){
                    if(timer_section != 0){
                        var element_s, endTime_s, hours_s, mins_s, msLeft_s, time_s;

                        function twoDigits( n ){
                            return (n <= 9 ? "0" + n : n);
                        }
                        function updateTimer(){
                            msLeft_s = endTime_s - (+new Date);
                            used++;
                            if ( msLeft_s < 1000 ){

                                element_s.innerHTML = "Waktu habis";
                                $('#skip').hide();
                                $('#next').hide();
                                $('#prev').hide();
                                var i;                  
                                // $('#myModal').modal(); 
                                $('.slide').removeClass('active');
                               
                                setTimeout(function () {
                                    $('.page-loader-wrapper').addClass('active');
                                    $('.page-loader-wrapper').fadeIn('medium');
                                }, 5000);
                                
                                $.ajax({
                                    url : "/survey/delete-temp-section/"+slug,    
                                    type: "GET"
                                });
                                
                                $.ajax({
                                    url : "/survey/save-cache-to-db/"+slug+"/"+section,    
                                    type: "GET",
                                    success :

                                    function(data){
                                        console.log(data);
                                        if(data.finish == 'next'){
                                            window.location.href = '/start-next-section/'+data.slug+'/'+data.new_section;
                                        }else if(data.finish == 'finish'){
                                            window.location.href = '/go-finish/'+data.slug;
                                        }
                                    }
                                });
                                

                            } else {
                                time_s = new Date( msLeft_s );
                                hours_s = time_s.getUTCHours();
                                mins_s = time_s.getUTCMinutes();
                                element_s.innerHTML = (hours_s ? hours_s + ':' + twoDigits( mins_s ) : mins_s) + ':' + twoDigits( time_s.getUTCSeconds() );
                                setTimeout( updateTimer, time_s.getUTCMilliseconds() + 500 );
                            }
                        }

                        element_s = document.getElementById( elementName );
                        endTime_s = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                        updateTimer();
                    }
                    
                }
            }

            //stopwatch_question
            var stopwatch_question = document.getElementById('stopwatch_question'),
            start = document.getElementById('start'),
            stop = document.getElementById('stop'),
            clear = document.getElementById('clear'),
            stop_seconds = 0, stop_minutes = 0, stop_hours = 0,
            t;
            function add() {
                stop_seconds++;
                if (stop_seconds >= 60) {
                    stop_seconds = 0;
                    stop_minutes++;
                    if (stop_minutes >= 60) {
                        stop_minutes = 0;
                        stop_hours++;
                    }
                }
                
                stopwatch_question.textContent = (stop_hours ? (stop_hours > 9 ? stop_hours : "0" + stop_hours) : "00") + ":" + (stop_minutes ? (stop_minutes > 9 ? stop_minutes : "0" + stop_minutes) : "00") + ":" + (stop_seconds > 9 ? stop_seconds : "0" + stop_seconds);

                timers();
            }

            function timers() {
                t = setTimeout(add, 1000);
            }

            timers();

            //save answer to cache
            function saveCache(id_question){
                //tambahkan used timer ke temp jika survey ada timer
                if(timer_survey != 0 || timer_section != 0){
                    // clearTimeout(t);
                    // if(stop_minutes != 0){ //convert minutes to seconds
                    //     stop_seconds = stop_seconds + (stop_minutes*60);
                    // }
                    // if(stop_hours != 0){
                    //     stop_seconds = stop_seconds + (stop_hours*60*60);
                    // }
                    $.ajax({
                        url : "save-temp",    
                        data: {survey_id: id_survey, question_id: id_question, user_id: id_user,  slug: slug, timer: used, section: section},
                        type: "POST"/*,
                        success :
                        function(data){
                            window.location.reload()
                        }*/
                    });
                }
                clearTimeout(t);
                if(stop_minutes != 0){ //convert minutes to seconds
                    stop_seconds = stop_seconds + (stop_minutes*60);
                }
                if(stop_hours != 0){
                    stop_seconds = stop_seconds + (stop_hours*60*60);
                }

                var count_null_answer   = 0; //jumlah untuk pengecekan sudah ada jawaban yang dipilih apa belum
                var index_answered      = 0; //index sejumlah jawaban jika jawaban lebih dari 1
                for(i = 0; i < length_answer; i++){
                    if(option[i].checked){
                        if(option[i].dataset.id_question == id_question){
                            answer[index_answered] = option[i].value;
                            index_answered++;
                        }
                    }else{
                        count_null_answer++;
                    }
                }
                if(answer != null){
                    list_number.push(id_question);
                    count_answered++;

                    //check jika nomor yang disubmit ada yang sama, maka count_answered tidak jadi ditambah (user edit jawaban)
                    for(j = 0; j < list_number.length-1; j++){
                        if(list_number[j] == id_question){
                            count_answered--;
                        }
                    }
                    
                    $('#val_answered').val(count_answered);
                    width = ($('#val_answered').val()/$('#val_total').val())*100;
                    element_answered = document.getElementById('count_answered');
                    element_answered.innerHTML = count_answered;
                    $('.determinate').css('width', width+"%");

                    if($('#val_answered').val() == $('#val_total').val()){
                        $('.slide').fadeOut('medium');
                        $('.page-loader-wrapper').fadeIn('medium');
                        $('.page-loader-wrapper').addClass('active');
                    }
                    
                    $.ajax({
                        url : "set-result-cache",    
                        data: {id_option: answer, id_question: id_question, timer: stop_seconds, length_question: length_question, slug: slug, section: section, section_name: section_name, id_type_question: id_type_question},
                        type: "POST",
                        success :
                        function(data){
                            if(data.finish == 'next'){
                                timer_section = 0;
                                window.location.href = '/start-next-section/'+data.slug+'/'+data.new_section;
                            }else if(data.finish == 'finish'){
                                timer_section = 0;
                                window.location.href = '/go-finish/'+data.slug;
                            }

                        }
                    });
                }
                

                //jika soal mandatory dan user tidak menjawab maka alert
                if(is_mandatory == 1 && count_null_answer == length_answer){ 
                        alert('Silahkan jawab terlebih dahulu');
                        return false;
                }
                used = 0;
            }
             

            function show_next(id_question){
                var checked = 0;
                var answer = document.getElementsByName('answer');
                for(i = 0; i < answer.length; i++){
                    if(answer[i].dataset.id_question == id_question){
                        if (answer[i].checked == true) {
                            checked++;
                            $(".question-action").show().addClass('animated fadeInUp');
                        }
                    }
                }
                if(checked == 2){
                    $(".question-action").show().addClass('animated fadeInUp');
                }else{
                    $(".question-action").hide();
                    
                }
            
            }

            $(document).ready(function() {
                    
                /*$(window).bind('beforeunload',function(){
                    return 'Semua jawaban yang sudah ada pilih akan hilang. Apakah anda yakin?'

                });*/

                if (performance.navigation.type == 1) {
                    $.ajax({
                        url : "/clear-cache/"+slug,    
                        type: "GET"
                    });
                } else {
                    $.ajax({
                        url : '/cek-sudah/'+slug+'/'+section,    
                        data: {},
                        type: "GET",
                        success :
                        function(data){

                            if(data === 'done'){
                                window.location.reload();
                            }else if (data === 'yet'){
                            }

                        }
                    });
                }

                //show next button
                $('.btn-tool>input[type=radio]').change(function () {
                    if ($(this).is(':checked')) {
                        $(".question-action").show().addClass('animated fadeInUp');
                    } else {
                        $(".question-action").hide('slow');
                    }
                }); 
                // $('.btn-tool>input[type=checkbox]').change(function () {
                //     if ($(this).is(':checked')) {
                //         checked++;
                //         $(".question-action").show().addClass('animated fadeInUp');
                //     } else {
                //         checked--;
                //         // $(".question-action").hide('slow');
                        
                //         if(checked < 2){
                //             $(".question-action").hide('slow');
                //         }else if((checked >= 2)){
                //             $(".question-action").show().addClass('animated fadeInUp');
                //         }
                //         console.log(checked);
                    
                //     }
                //     if(checked < 2){
                //         $(".question-action").hide('slow');
                //     }else if((checked >= 2)){
                //         $(".question-action").show().addClass('animated fadeInUp');
                //     }
                //         console.log(checked);
                // });

                //setting slider
                var getslideHeight = $('.slide.active').height();
                    
                $('.slides').css({
                    height: getslideHeight
                });
                
                function calcslideHeight() {
                    getslideHeight = $('.slide.active').height();
                    
                    $('.slides').css({
                        height: getslideHeight
                    });
                }
                

                //go to next slider
                var slideItem = $('.slide'),
                slideCurrentItem = slideItem.filter('.active');
                $('#skip').on('click', function(e) {
                    e.preventDefault();
                    // var ceks = slideCurrentItem.find('button');
                    // if(ceks.click){
                        
                    // }else{
                    //     slideCurrentItem.find('label').removeClass('active');
                    // }
                    var nextItem = slideCurrentItem.next();
                    
                    slideCurrentItem.removeClass('active');
                    $('.tool-statement').removeClass('animated fadeIn');
                    $('.option-text').removeClass('animated flipInX');
                    $(".question-action").hide();
                    
                    if (nextItem.length) {
                        
                        slideCurrentItem = nextItem.addClass('active');
                        slideCurrentItem.find('.option-text').addClass('animated flipInX');
                        
                        } else {
                        slideCurrentItem = slideItem.first().addClass('active');
                        slideCurrentItem.find('.option-text').addClass('animated flipInX');
                    }
                    
                    calcslideHeight();
                });
                
                $('.next').on('click', function(e) {
                    e.preventDefault();
                    
                    var nextItem = slideCurrentItem.next();
                    
                    slideCurrentItem.removeClass('active');
                    $('.tool-statement').removeClass('animated fadeIn');
                    $('.option-text').removeClass('animated flipInX');
                    $(".question-action").hide();
                    
                    if (nextItem.length) {
                        
                        slideCurrentItem = nextItem.addClass('active');
                        slideCurrentItem.find('.option-text').addClass('animated flipInX');
                        
                        } else {
                        slideCurrentItem = slideItem.first().addClass('active');
                        slideCurrentItem.find('.option-text').addClass('animated flipInX');
                    }
                    
                    calcslideHeight();
                });

                $('#prev').on('click', function(e) {
                    e.preventDefault();
                    
                    var prevItem = slideCurrentItem.prev();
                    
                    slideCurrentItem.removeClass('active');
                    $('.tool-statement').removeClass('animated fadeIn');
                    $('.option-text').removeClass('animated flipInX');
                    $(".question-action").hide();
                    
                    if (prevItem.length) {
                        slideCurrentItem = prevItem.addClass('active');
                        slideCurrentItem.find('.option-text').addClass('animated flipInX');
                        } else {
                        slideCurrentItem = slideItem.last().addClass('active');
                        slideCurrentItem.find('.option-text').addClass('animated flipInX');
                    }
                    
                    calcslideHeight();
                });

                $(function () {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 500);
                });
            });     
        </script>
    @stop