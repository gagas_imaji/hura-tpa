@extends('layouts.default')
    @section('breadcrumb')
            <div id="breadcrumb">
                <ol class="breadcrumb">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
    @stop

    @section('content')
        <div class="row">
            <a href="/testing-save-papi/{!! $data['wba']['slug'] !!}/{!! $data['wba']['id_section'] !!}">
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-spinner fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"></div><br/>
                                <div>Simpan Hasil WBA</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            <a href="/testing-load-section/{!! $data['lai']['slug'] !!}">
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-spinner fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"></div><br/>
                                <div>Simpan Hasil LAI</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            <a href="/testing-load-section/{!! $data['tiki']['slug'] !!}">
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-hourglass-2 fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"></div><br/>
                                <div>Load test TIKI</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>

    @stop

    @section('script')
        <script type="text/javascript">
            
        </script>
    @stop
