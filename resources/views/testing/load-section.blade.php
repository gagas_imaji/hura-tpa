@extends('layouts.default')
    @section('breadcrumb')
            <div id="breadcrumb">
                <ol class="breadcrumb">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
    @stop

    @section('content')
        <div class="row">
            @foreach($data['section'] as $section)
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"></div><br/>
                                    <div><a href="/testing-load-tiki/{!! $data['slug'] !!}/{!! $section['id'] !!}">{!! $section['name'] !!}</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            
        </div>

    @stop

    @section('script')
        <script type="text/javascript">
            
        </script>
    @stop
