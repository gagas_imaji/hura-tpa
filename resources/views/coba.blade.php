<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Chart.js Responsive Line Chart Demo</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="css/style.css">

</head>

<body>
  <div class="container">
  <h2>Chart.js Responsive multiLine Chart JPM</h2>
  <select name="" id="" class="form-control">
    <option value="">tess</option>
    <option value="">tess</option>
    <option value="">tess</option>
  </select>

    <div class="row">
                    <div class="col l4 m4 s12" >
                        <canvas id="mostJP-graph" style="height: 400px;"></canvas>
                    </div>
                    <div class="col l4 m4 s12">
                       <div id="leastJP-graph" style="height: 400px;"></div>
                    </div>
                    <div class="col l4 m4 s12">
                       <div id="changeJP-graph" style="height: 400px;"></div>
                    </div>
                </div>  
    </div>


    <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
    <script>var lineChartData = {
    labels: ["Data 1", "Data 2", "Data 3", "Data 4", "Data 5", "Data 6", "Data 7"],
    datasets: [{
        fillColor: "rgba(220,220,220,0)",
        strokeColor: "rgba(217,13,13,1)",
        pointColor: "rgba(217,13,13,1)",
        data: [200, 30, 80, 20, 40, 10, 60]
    }, {
        fillColor: "rgba(151,187,205,0)",
        strokeColor: "rgba(17,17,225,1)",
        pointColor: "rgba(17,17,225,1)",
        data: [60, 10, 40, 30, 80, 30, 20]
    }]

}

Chart.defaults.global.animationSteps = 50;
Chart.defaults.global.tooltipYPadding = 16;
Chart.defaults.global.tooltipCornerRadius = 0;
Chart.defaults.global.tooltipTitleFontStyle = "normal";
Chart.defaults.global.tooltipFillColor = "rgba(0,160,0,0.8)";
Chart.defaults.global.animationEasing = "easeOutBounce";
Chart.defaults.global.responsive = true;
Chart.defaults.global.scaleLineColor = "black";
Chart.defaults.global.scaleFontSize = 16;

var ctx = document.getElementById("mostJP-graph").getContext("2d");
var LineChartDemo = new Chart(ctx).Line(lineChartData, {
    pointDotRadius: 10,
    bezierCurve: false,
    scaleShowVerticalLines: false,
    scaleGridLineColor: "black"
});</script>

</body>
</html>
