@extends('layouts.default')

	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
			    <li><a href="/survey">Survey</a></li>
			    <li><a href="/survey/{{$id_survey}}">{!! $name_survey !!}</a></li>
			    <li class="active">{!! trans('messages.add_new').' '!!} Question</li>
            </ol>

        </div>
	@stop
	
	@section('content')
		<div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<strong>Question Settings</strong>
                    </div>
                    <div class="panel-body">
                    	<form action="">
                    		
                    	</form>
                    	<div class="col-lg-7">
                    		<div class="col-lg-12">
                    			{!! Form::hidden('id_survey',$id_survey,['class' => 'form-control'])!!}
		                    	<div class="form-group form-group-default" id="question">
								    {!! Form::label('type', 'Question Type *', [])!!}
								    <select name="type" class="form-control" id="question_type">
								    	<option value="-">Select One</option>
								    @foreach($option as $option)
								    	<option value="{!! $option['id'] !!}">{!! $option['name'] !!}</option>
								    @endforeach
								    </select>
									
								</div>
								<div class="form-group form-group-default">
								    {!! Form::label('text', 'Question Text *', [])!!}
									{!! Form::textarea('description', '',['class'=>'form-control','placeholder'=>'Question'])!!}
								</div>
								<div class="form-group form-group-default">
								    {!! Form::label('help', 'Help Text',[])!!}
								    {!! Form::text('help','',['class' => 'form-control', 'placeholder' => 'Help text'])!!}
								</div>
								<div class="form-group form-group-default">
								    <input type="checkbox" name="countdown" id="countdown" />
								    {!! Form::label('countdown', 'Enable countdown',[])!!}
								    <div class="col-lg-12">
										<span class="countdown" style="display: none;">
										    <div class="col-lg-6 row-form">
												{!! Form::label('minutes', 'minutes',[])!!}
												{!! Form::number('minutes','', ['class' => 'form-control', 'placeholder' => '0']) !!}
											</div>
											<div class="col-lg-6 row-form">
												{!! Form::label('seconds', 'seconds',[])!!}
												{!! Form::number('seconds','', ['class' => 'form-control', 'placeholder' => '0']) !!}
											</div>
									    </span>
									</div>
								</div>
							</div>
							
							<div class="col-lg-12">
								<div class="form-group form-group-default">
								    {!! Form::label('section', 'Question Section *', [])!!}
								    {!! Form::select('type', $section, null, array('class'=>'form-control'))!!}
								</div>
								<hr/>
							</div>

							<!-- Jika pertanyaan type multiple choice -->
							<div class="col-lg-12 single_multiple" id="single_multiple" style="display: none;">
								{!! Form::label('option', 'Answer/Option',[])!!}
							    <div class="form-group">
									<div class="col-lg-5 row-form">
										<strong>Option Answer</strong>
									</div>
									<div class="col-lg-2 row-form">
										<strong>Point</strong>
									</div>
									<div class="col-lg-5 row-form">
										&nbsp
									</div>
									
								</div>
							    @for($i = 0; $i < 4; $i++)
									<div class="form-group" id="div_option_{{$i}}">
										<div class="col-lg-5 row-form">
											{!! Form::text('option[]','',['class' => 'form-control', 'placeholder' => 'Option '.($i+1)])!!}
										</div>
										<div class="col-lg-2 row-form ">
											{!! Form::number('point[]','', ['class' => 'form-control', 'placeholder'=>'0']) !!}
										</div>
										<div class="col-lg-2 row-form">
											<a href="javascript:delete_option({{$i}})" class="btn btn-danger pull-left"> Delete</a>
											
										</div>
									</div>
								@endfor
							</div>
							<div class="col-lg-12 single_multiple" style="display: none;">
								<div class="form-group">
								    {!! Form::submit('Add New Option',['class' => 'btn btn-primary pull-left', 'id'=> 'add-option']) !!}
								</div><br/><br/>
								<div class="form-group">
								    {!! Form::checkbox('random','')!!}
								    {!! Form::label('random', 'Randomized Option',[])!!}
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									{!! Form::submit(isset($buttonText) ? $buttonText : trans('messages.save'),['class' => 'btn btn-primary pull-left', 'id' => 'create_question']) !!}
								</div>
							</div>	
							<div class="col-lg-7" id="open_text" style="display: none;">
								<div class="form-group form-group-default">
										{!! Form::label('text', 'Answer Text *', [])!!}
										{!! Form::textarea('option','',['class' => 'form-control', 'placeholder' => 'Answer'])!!}
										{!! Form::hidden('point',1, ['class' => 'form-control', 'placeholder'=>'0']) !!}
								</div>

								<div class="form-group">
									<div class="col-lg-7 row-form">
								    {!! Form::label('input-type', 'Option View',[])!!}
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-7 row-form">
									    {!! Form::radio('input-type','')!!}
									    {!! Form::label('input-type', 'Text',[])!!}
								    </div>

								</div>
								<div class="form-group">
									<div class="col-lg-7 row-form">
									    {!! Form::radio('input-type','')!!}
									    {!! Form::label('input-type', 'Number Only',[])!!}
								    </div>

								</div>
							</div>
								
						</div>

						<div class="col-sm-5">
							<div class="panel panel-default">
			                    <div class="panel-heading">
			                    	<div class="panel-title text-danger">
			                    		<i class="fa fa-cog fa-fw"></i> General Survey Settings
			                    	</div>
			                    </div>
			                    <div class="panel-body">
		                    		<div class="form-group form-group-default">
									    <input type="checkbox" name="mandatory" id="mandatory" />
										{!! Form::label('mandatory', 'Question is mandatory', [])!!}
									</div>
			                    </div>
			                 </div>
						</div>
					</div>
                    </div>
                </div>
            </div>
		</div>

		<script type="text/javascript">
			var limit = 6; // Max questions
		    var count = 4; // There are 3 questions already
			$(document).ready(function(){

				var i;

				//option answer by question type
				$('#question_type').change(function(){
					var question_type = $('#question_type').val();
					if(question_type == 1 || question_type == 2){
						$('.single_multiple').show('medium');
						$('#open_text').hide('medium');
					}else if(question_type == 3){
						$('.single_multiple').hide('medium');
						$('#open_text').show('medium');
					}else{
						$('.single_multiple').hide('medium');
						$('#open_text').hide('medium');
					}
				});

				//countdown option
		        $('#countdown').click(function(){
		            $('.countdown').toggle('medium');
		        });

		        //add option/answer
		        $('#add-option').click(function(){
		            // Get the question form element
		            var option = document.getElementById('single_multiple');

		            if (option){
		                if (count < limit){

		                	//create div form-group
		                	var div_form_group 		= document.createElement('div');
		                	div_form_group.setAttribute('class', 'form-group');
		                	div_form_group.setAttribute('id', 'div_option_'+count);

		                	//create div col-lg-5 row-form
		                	var div_col_lg_5 		= document.createElement('div');
		                	div_col_lg_5.setAttribute('class', 'col-lg-5 row-form');

		                    // Create the new text box option
		                    var new_text_option      = document.createElement('input');
		                    new_text_option.setAttribute('class', 'form-control');
		                    new_text_option.type        = 'text';
		                    new_text_option.name        = 'option[]';
		                    new_text_option.placeholder = 'Option '+(count+1);
		                   
		                   	//create div col-lg-2 row-form
		                   	var div_col_lg_2 		= document.createElement('div');
		                	div_col_lg_2.setAttribute('class', 'col-lg-2 row-form');

		                	//create the new text box point
		                	var new_text_point      = document.createElement('input');
		                    new_text_point.setAttribute('class', 'form-control');
		                    new_text_point.type        = 'number';
		                    new_text_point.name        = 'point[]';
		                    new_text_point.placeholder = '0 ';

		                    //create div col-lg-3
		                    var div_col_lg_3 		= document.createElement('div');
		                	div_col_lg_3.setAttribute('class', 'col-lg-3 row-form');

		                	//create the new button image
		                	var new_text_image      	= document.createElement('input');
		                    new_text_image.setAttribute('class', 'btn btn-primary pull-right');
		                    new_text_image.type        	= 'submit';
		                    new_text_image.value 		= 'Upload Image';

		                    //create div col-lg-delete
		                    var div_col_lg_delete 		= document.createElement('div');
		                	div_col_lg_delete.setAttribute('class', 'col-lg-2 row-form');

		                	//create the new button delete
		                	var new_text_delete      	= document.createElement('a');
		                    new_text_delete.setAttribute('class', 'btn btn-danger pull-left');
		                    new_text_delete.setAttribute('id', 'delete'+count);
		                    new_text_delete.setAttribute('onClick', 'delete_option('+(count)+')');
		                    new_text_delete.value 		= 'Delete';

		                    if (new_text_option){
		                        // Add the new elements to the form
		                        option.appendChild(div_form_group);

		                        div_form_group.appendChild(div_col_lg_5);
		                        div_col_lg_5.appendChild(new_text_option);

		                        div_form_group.appendChild(div_col_lg_2);
		                        div_col_lg_2.appendChild(new_text_point);

		                        /*div_form_group.appendChild(div_col_lg_3);
		                        div_col_lg_3.appendChild(new_text_image);*/

		                        div_form_group.appendChild(div_col_lg_delete);
		                        div_col_lg_delete.appendChild(new_text_delete);
		                        new_text_delete.append('Delete');
		                        count++;
		                    }
		                }
		                else{
		                    alert('Option/Answer limit reached');
		                }
		            }
		        });

		        

			});
			function delete_option(index){
		        var option_delete = document.getElementById('div_option_'+index);
		        if(option_delete){
		        	option_delete.remove();
		        	count--;
		        }
		    }
		</script>
	@stop