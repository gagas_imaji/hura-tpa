@extends('layouts.default')

    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/survey">Survey</a></li>
                <li class="active">Import Question</li>
            </ol>

        </div>
    @stop
    
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <div class="row"> 
                            <div class="header-left">
                                <strong>Import Question</strong>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        <form name="myForm" id="myForm" onSubmit="return validateForm()" action="import-question-to-section" method="post" enctype="multipart/form-data" id="import" data-submit="noAjax">
                        {!! csrf_field() !!}
                            {!! Form::hidden('id_survey',$id_survey,['class' => 'form-control'])!!}
                            <div class="col-lg-7" id="form_content">
                                <div class="form-group form-group-default required">
                                    <label>Section</label>
                                    <select name="section" class="form-control select2-container full-width" id="section" onchange="check_section(this.options[this.selectedIndex].getAttribute('sections'))">
                                        <option value="0">-- Select Section --</option>
                                        @foreach($section as $section)
                                            <option value="{{$section['id']}}" sections="{{ $section['name']}}">{{ $section['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group-default required">
                                    <label>Type Question</label>
                                    <select name="question_type" class="form-control select2-container full-width" onchange="linkDownload()" id="question_type">
                                        <option value="0">-- Select Type Question --</option>
                                        @foreach($question_type as $type)
                                            <option value="{{$type['id']}}">{{ $type['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group-default required number_choice" style="display: none;">
                                    <label>Number of choice</label>
                                    <select name="number_choice" class="form-control select2-container full-width" onchange="downloadMultiple()" id="number_choice" required="required">
                                        <option value="0">-- Select Number of Choice --</option>
                                        @for($i = 2; $i <= 7; $i++)
                                            <option value="{{$i}}"> {{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group form-group-default" id="multiple_download_2" style="display: none;">
                                    <label>Format File CSV</label>
                                    <h5><a href="download/multiple_format_file.csv">Download Format Multiple Choice 2 Option Answer</a></h5>
                                </div>
                                <div class="form-group form-group-default" id="multiple_download_3" style="display: none;">
                                    <label>Format File CSV</label>
                                    <h5><a href="download/multiple_format_file.csv">Download Format Multiple Choice 3 Option Answer</a></h5>
                                </div>
                                <div class="form-group form-group-default" id="multiple_download_4" style="display: none;">
                                    <label>Format File CSV</label>
                                    <h5><a href="download/multiple_format_file.csv">Download Format Multiple Choice 4 Option Answer</a></h5>
                                </div>
                                <div class="form-group form-group-default" id="multiple_download_5" style="display: none;">
                                    <label>Format File CSV</label>
                                    <h5><a href="download/multiple_format_file.csv">Download Format Multiple Choice 5 Option Answer</a></h5>
                                </div>
                                <div class="form-group form-group-default" id="multiple_download_6" style="display: none;">
                                    <label>Format File CSV</label>
                                    <h5><a href="download/multiple_format_file.csv">Download Format Multiple Choice 6 Option Answer</a></h5>
                                </div>
                                <div class="form-group form-group-default" id="multiple_download_7" style="display: none;">
                                    <label>Format File CSV</label>
                                    <h5><a href="download/multiple_format_file.csv">Download Format Multiple Choice 7 Option Answer</a></h5>
                                </div>
                                <div class="form-group form-group-default" id="opentext_download" style="display: none;">
                                    <label>Format File CSV</label>
                                    <h5><a href="download/opentext_format_file.csv">Download Format Open Text</a></h5>
                                </div>
                                <div class="form-group form-group-default" id="matching_download" style="display: none;">
                                    <label>Format File CSV</label>
                                    <h5><a href="download/matching_format_file.csv">Download Format Matching Question</a></h5>
                                </div>
                                <div class="form-group required">
                                    <label>File Import</label>
                                    {!! Form::file('file_question', array('id'=>'file_question', 'required'=>'required'))!!}
                                </div>
                                <div class="form-group required">
                                    {!! Form::submit('Import', array('class'=>'btn btn-primary', 'name' => 'submit'))!!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                        
            </div>
        </div>
        <script type="text/javascript">
        
        //download format question
        function linkDownload(){
            if($('#question_type').val() == 1 || $('#question_type').val() == 2){
                $('.number_choice').show('medium');
                $('#opentext_download').hide('medium');
                $('#multiple_download').hide('medium');
                $('#matching_download').hide('medium');
            }else if($('#question_type').val() == 3){
                $('#opentext_download').show('medium');
                $('#multiple_download').hide('medium');
                $('#matching_download').hide('medium');
                $('.number_choice').hide('medium');
            }else if($('#question_type').val() == 4){
                $('#matching_download').show('medium');
                $('#multiple_download').hide('medium');
                $('#opentext_download').hide('medium');
                $('.number_choice').hide('medium');
            }
        }

        function downloadMultiple(){
            if($('#number_choice').val() == 2){
                $('#multiple_download_2').show('medium');
                $('#multiple_download_3').hide('medium');
                $('#multiple_download_4').hide('medium');
                $('#multiple_download_5').hide('medium');
                $('#multiple_download_6').hide('medium');
                $('#multiple_download_7').hide('medium');
            }else if($('#number_choice').val() == 3){
                $('#multiple_download_2').hide('medium');
                $('#multiple_download_3').show('medium');
                $('#multiple_download_4').hide('medium');
                $('#multiple_download_5').hide('medium');
                $('#multiple_download_6').hide('medium');
                $('#multiple_download_7').hide('medium');
            }else if($('#number_choice').val() == 4){
                $('#multiple_download_2').hide('medium');
                $('#multiple_download_3').hide('medium');
                $('#multiple_download_4').show('medium');
                $('#multiple_download_5').hide('medium');
                $('#multiple_download_6').hide('medium');
                $('#multiple_download_7').hide('medium');
            }else if($('#number_choice').val() == 5){
                $('#multiple_download_2').hide('medium');
                $('#multiple_download_3').hide('medium');
                $('#multiple_download_4').hide('medium');
                $('#multiple_download_5').show('medium');
                $('#multiple_download_6').hide('medium');
                $('#multiple_download_7').hide('medium');
            }else if($('#number_choice').val() == 6){
                $('#multiple_download_2').hide('medium');
                $('#multiple_download_3').hide('medium');
                $('#multiple_download_4').hide('medium');
                $('#multiple_download_5').hide('medium');
                $('#multiple_download_6').show('medium');
                $('#multiple_download_7').hide('medium');
            }else if($('#number_choice').val() == 7){
                $('#multiple_download_2').hide('medium');
                $('#multiple_download_3').hide('medium');
                $('#multiple_download_4').hide('medium');
                $('#multiple_download_5').hide('medium');
                $('#multiple_download_6').hide('medium');
                $('#multiple_download_7').show('medium');
            }
        }

        //show input paragraph if reading section
        function check_section(name){
            if(name == 'Reading Text'){
                var div_form_group = document.getElementById('form_content');
                if(div_form_group){
                    var div         = document.createElement('div');
                    div.setAttribute('class', 'form-group required');
                    div.setAttribute('id', 'div_paragraph');

                    var label       = document.createElement('label');

                    var newText         = document.createElement('textarea');
                    newText.setAttribute('class', 'form-control full-width');
                    newText.setAttribute('placeholder', 'Article Text');
                    newText.setAttribute('required', 'required');
                    newText.type        = 'textarea';
                    newText.name        = 'paragraph';

                    if(div){
                        // Add the new elements to the form
                        div_form_group.appendChild(div);
                        div.appendChild(label);
                        label.append('Create Article');
                        div.appendChild(newText);
                        
                    }
                }
            }else{
                var paragraph = document.getElementById('div_paragraph');
                if(paragraph){
                    paragraph.remove();
                }
            }
        }
        //validasi form (hanya file .csv yang diijinkan)
        function validateForm()
        {
            function hasExtension(inputID, exts) {                
                var fileName = document.getElementById(inputID).value;
                return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
            }

            if(!hasExtension('file_question', ['.csv'])){
                alert("Only csv files that allowed");
                return false;
            }

            if($('#question_type').val() == 0){
                alert('Please select the types of questions');
                return false;
            }

        }
        </script>
    @stop