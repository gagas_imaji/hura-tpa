@extends('layouts.default')
	@section('breadcrumb')
		<div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                <li><a href="/survey">Survey</a></li>
                <li><a href="/survey/{{$survey['id']}}">{!! $survey['title'] !!}</a></li>
                <li class="active">Question</li>
            </ol>

        </div>
	@stop
	
	@section('content') 
		<div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Add New Question</strong>
                    </div>
                    <div class="panel-body">
                            
                        {!! Form::open(['url' => '/import', 'method' => 'post', 'class' => 'question-form','id' => "question-form", 'data-submit'=>'noAjax']) !!}
                                
                                {!! Form::hidden('id_survey',$survey['id']) !!}

                            <div class="form-group">
                            {!! Form::submit('Import Question', ['class' => 'btn btn-primary pull-left'])!!}
                            </div>
                        {!! Form::close() !!}
                    
                        <!-- {!! Form::open(['url' => '/question/create-form', 'method' => 'post', 'class' => 'question-form','id' => "question-form", 'data-submit'=>'noAjax']) !!}
                            
                                {!! Form::hidden('id_survey',$survey['id']) !!}

                            <div class="form-group">
                            {!! Form::submit('Add Question', ['class' => 'btn btn-primary pull-right'])!!}
                            </div>
                        {!! Form::close() !!} -->

                            
                    </div>
                   
                </div>
            </div>
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>List of Questions</strong>
                    </div>
                    <div class="panel panel-body">
                        
                    </div>
                </div>
            </div>
        </div>
	@stop
