@extends('layouts.default')
    @foreach($question as $question)
    	@section('breadcrumb')
    		<div id="breadcrumb">
                <ol class="breadcrumb">
                    <li><a href="/home">{!! trans('messages.home') !!}</a></li>
                    <li><a href="/survey">Survey</a></li>
                    <li><a href="/survey/{{$question->survey_id}}">{!! $question->title !!}</a></li>
                    <li class="active">Detail Question</li>
                </ol>

            </div>
    	@stop
    	
    	@section('content')
    		<div class="row">
                <div class="col-md-12">
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <!-- <li><a href="/survey/{!!$question->survey_id!!}">Survey</a>
                            </li> -->
                            <li class="active"><a href="#detail-question-tab" data-toggle="tab">Detail Question</a>
                            </li>
                            <li><a href="#update-question-tab" data-toggle="tab">Update Question</a></li>
                            <li><a href="#update-answer-tab" data-toggle="tab">Update Option Answer</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="detail-question-tab">
                                <div class="panel panel-default col-md-6">
                                        <div class="panel-heading">
                                            <strong>Detail</strong> Question
                                        </div>
                                        <div class="panel-body full">
                                            
                                            <div class="responsive">
                                                <table class="table table-stripped table-hover show-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Type Question</th>
                                                            <td>{!!$question->type!!}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Section</th>
                                                            <td>{!!toWord($question->section)!!}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Question</th>
                                                            <td>{!!$question->question!!}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Random</th>
                                                            @if($question->is_random == 1)
                                                                <td>Yes</td>
                                                            @else
                                                                <td>No</td>
                                                            @endif
                                                        </tr>
                                                        <tr>
                                                            <th>Timer</th>
                                                            <td>{!!$question->timer!!}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Mandatory</th>
                                                            @if($question->is_mandatory == 1)
                                                                <td>Yes</td>
                                                            @else
                                                                <td>No</td>
                                                            @endif
                                                        </tr>
                                                        <tr>
                                                            <th>Point</th>
                                                            <td>{!!$question->point!!}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                </div>
                                <div class="panel panel-default col-md-5 pull-right">
                                        <div class="panel-heading">
                                            <strong>Option </strong>Answer
                                        </div>
                                        <div class="panel-body full">
                                            
                                            <div class="responsive">
                                                <table class="table table-stripped table-hover show-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Answer</th>
                                                            <th>Image</th>
                                                            <th>Point</th>
                                                        </tr>
                                                        <?php $no = 1; ?>
                                                        @foreach($option_answer as $option)
                                                            <tr>
                                                                <td>{!! $no !!}</td>
                                                                <td>{!! $option['answer'] !!}</td>
                                                                <td>{!! $option['image'] !!}</td>
                                                                <td>{!! $option['point'] !!}</td>

                                                            </tr>
                                                        <?php $no++; ?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="tab-pane fade in" id="update-question-tab">
                                <div class="panel panel-default col-md-6" >
                                    <div class="panel-heading">
                                        <strong>Update </strong>Detail Question
                                    </div>
                                    <div class="panel-body full">
                                        {!! Form::model($question,['method' => 'POST','route' => ['question.detail-update',$question->id] ,'class' => 'question-detail-form','id' => 'question-detail-form','data-no-form-clear' => 1, 'data-submit'=>'noAjax']) !!}
                                        {!! csrf_field() !!}
                                        <div class="row">
                                            {!! Form::hidden('id_question[]',$question->id,['class'=>'form-control'])!!}
                                            <div class="form-group">
                                                {!! Form::label('question','Question',[])!!}
                                                {!! Form::input('text','question',$question->question,['class'=>'form-control','placeholder'=>'Question text'])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('is_random','Random Answer',[])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::select('is_random',['0'=>'No', '1'=>'Yes'], null, ['class'=>'form-control'])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('timer','Timer',[])!!}
                                                {!! Form::number('timer',$question->timer,['class'=>'form-control','placeholder'=>'0'])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('is_mandatory','Mandatory',[])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::select('is_mandatory',['0'=>'No', '1'=>'Yes'], null, ['class'=>'form-control'])!!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('point','Point',[])!!}
                                                {!! Form::text('point',$question->point,['class'=>'form-control getDecimal','placeholder'=>'Point Question', 'data-toggle'=>'tooltip', 'title'=>'use point . as a separator'])!!}

                                            </div>
                                            {!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-right']) !!}
                                        </div>                                        
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade in" id="update-answer-tab">
                                <div class="panel panel-default col-md-6" >
                                    <div class="panel-heading">
                                        <strong>Update </strong>Detail Option Answer
                                    </div>
                                    <div class="panel-body full">
                                        {!! Form::model($question,['method' => 'POST','route' => ['question.answer-update',$question->id] ,'class' => 'answer-detail-form','id' => 'answer-detail-form','data-no-form-clear' => 1, 'data-submit'=>'noAjax']) !!}
                                        {!! csrf_field() !!}
                                        <div class="row">
                                            
                                            <table class="table table-stripped table-hover show-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Answer</th>
                                                            <th>Image</th>
                                                            <th>Point</th>
                                                        </tr>
                                                        <?php $no = 1; ?>
                                                        @foreach($option_answer as $option)
                                                            {!! Form::hidden('id_option_answer[]',$option->id,['class'=>'form-control'])!!}
                                                            <tr>
                                                                <td>{!! Form::input('text','option_answer[]',$option->answer,['class'=>'form-control','placeholder'=>'Answer text'])!!}
                                                                </td>
                                                                <td>{!! $option['image'] !!}</td>
                                                                <td>{!! Form::text('point_option_answer[]',$option->point,['class'=>'form-control getDecimal','placeholder'=>'Answer text', 'style'=>'width: 50px', 'data-toggle'=>'tooltip', 'title'=>'use point . as a separator'])!!}</td>

                                                            </tr>
                                                        <?php $no++; ?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            {!! Form::submit(trans('messages.save'),['class' => 'btn btn-primary pull-right']) !!}
                                        </div>                                        
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" type="text/css" media="all">
            <script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
            <script type="text/javascript">
            $(document).ready(function () {
                $('.getDecimal').on('keyup', function(event){
                    var inputField = $(this);
                    var value = inputField.val();
                    //any character except for numbers, point
                    var invalid = value.match(/[^0-9.]+/g); 
                    if(invalid){
                        alert('Incorrect format! Only numbers allowed (separated by point)');
                    }
                });
            });
                var dateToday = new Date();
                var dates = $("#started, #ended").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    minDate: dateToday/*,
                    onSelect: function(selectedDate) {
                        var dateMin = $('#started').datepicker("getDate");
                        var option = this.id == "started" ? "minDate" : "maxDate",
                            date = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1);
                        dates.not(this).datepicker("option", option, date);
                    }*/
                    
                });
            </script>
        @stop
    @endforeach