<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title>500 - Internal Server Error</title>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-touch-fullscreen" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="default">
		<meta content="" name="description" />
		<meta content="" name="author" />
		
		
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
		<link href="<?php echo asset('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo asset('assets/vendor/materialize/css/materialize.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo asset('assets/css/animate.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo asset('assets/css/user/error/user-style.css'); ?>" rel="stylesheet" type="text/css"/>   
	</head>
	
	<body id="internal-error">
		@yield('content')
		
	</body>
	@yield('script')
</html>