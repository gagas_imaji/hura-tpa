@extends('layouts.default')
@section('inline-css')
<style>
    .material-icons.md-36 { font-size: 36px; }
    .material-icons.md-48 { font-size: 48px; }
    .card-panel.small {font-size: 100%;height:300px;}
    .card-panel.flat {box-shadow: none;}
    .card-panel h6 {
        font-size: 18px;
        font-weight: 300;
        padding-bottm:10px;
    }
    .wpa-result-desc {margin: 60px 0;}
    .wpa-result-summary, .wpa-result-info {min-height: 140px;}
    .wpa-result-summary h2 {margin:0;}
    .wpa-result-summary .h6 {margin:0;}
    .wpa-result-chart .col {overflow:hidden;}
    .wpa-result-chart #most-graph, .wpa-result-chart #least-graph, .wpa-result-chart #change-graph {
        position: relative;
        top: 10px;
    }
    @media (min-width: 768px) {
    .wpa-result-info dl {margin-bottom:0;}
    .wpa-result-info dt {text-align:left;width:150px;}
    .wpa-result-info dd {margin-left:120px;}
    }
    .canvasjs-chart-credit {display:none!important;}
    .card-panel.grey{
        background-color: #fff;
        margin-bottom: 30px;
        padding: 20px;
    }
</style>
@stop
@section('breadcrumb')
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<div id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/backend/history-project">Riwayat Project</a></li>
        <li><a href="/backend/report-wpa-with-user/{{$data['slug']}}">Hasil Test Work Personality Analytics</a></li>
        <li class="active">Hasil Test {!! get_firstname_by_id($data['id_user']) !!}</li>
    </ol>
</div>
@stop
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan Work Personality Analytics</h2>
            <a href="{{ url('print/wpa/') }}/{{ $data['slug'] }}/{{ $data['id_user'] }}" class="btn btn-info">
              Download PDF
            </a>
        </div>
        <div class="wpa-result">
            <div class="row">
                <div class="col col-sm-12 col-md-7 col-lg-8 ">
                    <div class="wpa-result-info card-panel grey lighten-4 flat">
                        <dl class="dl-horizontal">
                            <dt>Nama</dt>
                            <dd>{!! get_firstname_by_id($data['id_user']) !!}</dd>
                            <dt>Tanggal Pengerjaan</dt>
                            <dd>{{ $data['date'] }}</dd>
                        </dl>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-5 col-lg-4">
                    <div class="wpa-result-summary card-panel grey lighten-2 flat">
                        <h6>Tipe Kepribadian Anda:</h6>
                        <h3>{{ strtoupper($data['result']) }}</h3>
                        <strong>{{ $data['type_kepribadian']}}</strong>
                    </div>
                </div>
            </div>
            <div class="wpa-result-chart">
                <div class="row">
                    <div class="col col-sm-12 col-md-4 col-lg-4" >
                        <div id="most-graph" style="height: 400px;"></div>
                    </div>
                    <div class="col col-sm-12 col-md-4 col-lg-4">
                       <div id="least-graph" style="height: 400px;"></div>
                    </div>
                    <div class="col col-sm-12 col-md-4 col-lg-4">
                       <div id="change-graph" style="height: 400px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpa-result-desc">
            <div class="row">
                <div class="col col-sm-12">
                    <h6>Uraian Kepribadian</h6>
                    {!! $data['uraian_kepribadian'] !!}
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">settings</i>
                        <h6>Karakteristik Umum</h6>
                        <ul class="browser-default">
                            @if($data['karakteristik_umum'] != '-')
                                <?php $i = 0; ?>
                                @foreach($data['karakteristik_umum'] as $key)
                                    @if($i < 5)
                                        <li>{!! $key !!}</li>
                                    @endif
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">star</i>
                        <h6>Perilaku Kerja <span class="teal-text">(Kekuatan)</span></h6>
                        <ul class="browser-default">
                            @if($data['perilaku_kerja_kekuatan'] != '-')
                                <?php $i = 0; ?>
                                @foreach($data['perilaku_kerja_kekuatan'] as $key)
                                    @if($i < 5 && $key != '')
                                        <li>{!! $key !!}</li>
                                    @endif
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">star_border</i>
                        <h6>Perilaku Kerja <span class="red-text">(Kelemahan)</span></h6>
                        <ul class="browser-default">
                            @if($data['perilaku_kerja_kelemahan'] != '-')
                                <?php $i = 0; ?>
                                @foreach($data['perilaku_kerja_kelemahan'] as $key)
                                    @if($i < 5 && $key != '')
                                        <li>{!! $key !!}</li>
                                    @endif
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">sentiment_very_satisfied</i>
                        <h6>Suasana Emosi <span class="teal-text">(Kekuatan)</span></h6>
                        <ul class="browser-default">
                            @if($data['suasana_emosi_kekuatan'] != '-')
                                <?php $i = 0; ?>
                                @foreach($data['suasana_emosi_kekuatan'] as $key)
                                    @if($i < 5 && $key != '')
                                        <li>{!! $key !!}</li>
                                    @endif
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">sentiment_very_dissatisfied</i>
                        <h6>Suasana Emosi <span class="red-text">(Kelemahan)</span></h6>
                        <ul class="browser-default">
                            @if($data['suasana_emosi_kelemahan'] != '-')
                                <?php $i = 0; ?>
                                @foreach($data['suasana_emosi_kelemahan'] as $key)
                                    @if($i < 5 && $key != '')
                                        <li>{!! $key !!}</li>
                                    @endif
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">my_location</i>
                        <h6>Kekuatan</h6>
                        <ul class="browser-default">
                            @if($data['kekuatan'] != '-')
                                <?php $i = 0; ?>
                                @foreach($data['kekuatan'] as $key)
                                    @if($i < 5 && $key != '')
                                        <li>{!! $key !!}</li>
                                    @endif
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">my_location</i>
                        <h6>Kelemahan</h6>
                        <ul class="browser-default">
                            @if($data['kelemahan'] != '-')
                                <?php $i = 0; ?>
                                @foreach($data['kelemahan'] as $key)
                                    @if($i < 5 && $key != '')
                                        <li>{!! $key !!}</li>
                                    @endif
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-8">
                    <div class="card-panel grey lighten-4 flat">
                        <i class="material-icons md-48">widgets</i>
                        <h6>Karakteristik Pekerjaan</h6>
                        <ul class="browser-default">
                            @if($data['karakteristik_pekerjaan'] != '-')
                                <?php $i = 0; ?>
                                @foreach($data['karakteristik_pekerjaan'] as $key)
                                    @if($i < 9)
                                        <li>{!! $key !!}</li>
                                    @endif
                                    <?php $i++; ?>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section("inline-js")
        {!! Html::script('assets/vendor/canvasjs/canvasjs.min.js') !!}
        {!! Html::script('assets/vendor/canvasjs/jquery.canvasjs.min.js') !!}
<script>
$(function() {
    $("#most-graph").CanvasJSChart({
        title: {
            text: "MOST"
        },
        axisY: {

            includeZero: false,
            minimum: -8,
            maximum: 8,
            valueFormatString: " ",
            tickLength: 0,
            stripLines:[
            {
                value:0,
                thickness:4
            }
            ],
            valueFormatString:"####"
        },
        axisX: {
            interval: 1,
            gridDashType: "shortDot",
            gridThickness: 1
        },
        data: [
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['most_d'] }} },
                { label: "I",  y: {{ $data['most_i'] }} },
                { label: "S",y: {{ $data['most_s'] }}},
                { label: "C",y: {{ $data['most_c'] }} },
            ]
        }
        ]
    });

    $("#least-graph").CanvasJSChart({
        title: {
            text: "LEAST"
        },
        axisY: {

            includeZero: false,
            minimum: -8,
            maximum: 8,
            valueFormatString: " ",
            tickLength: 0,
            stripLines:[
            {
                value:0,
                thickness:4
            }
            ],
            valueFormatString:"####"
        },
        axisX: {
            interval: 1,
            gridDashType: "shortDot",
            gridThickness: 1
        },
        data: [
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['lest_d'] }} },
                { label: "I",  y: {{ $data['lest_i'] }} },
                { label: "S",y: {{ $data['lest_s'] }}},
                { label: "C",y: {{ $data['lest_c'] }} },
            ]
        }
        ]
    });

    $("#change-graph").CanvasJSChart({
        title: {
            text: "CHANGE"
        },
        axisY: {

            includeZero: false,
            minimum: -8,
            maximum: 8,
            valueFormatString: " ",
            tickLength: 0,
            stripLines:[
            {
                value:0,
                thickness:4
            }
            ],
            valueFormatString:"####"
        },
        axisX: {
            interval: 1,
            gridDashType: "shortDot",
            gridThickness: 1
        },
        data: [
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['change_d'] }} },
                { label: "I",  y: {{ $data['change_i'] }} },
                { label: "S",y: {{ $data['change_s'] }}},
                { label: "C",y: {{ $data['change_c'] }} },
            ]
        }
        ]
    });
});

</script>
@stop
