@extends('layouts.default')
@section('inline-css')
<style>

    .gti-low{
        background-color:  #f80404  ;
    }
     .gti-mid{
        background-color:  #f2bd0a   ;
    }

     .gti-high{
        background-color:  #12e700    ;
    }

    #page-wrapper{
        background-color: #2b303b !important;
    }
</style>
@stop
@section('breadcrumb')
<div id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/backend/history-project">Riwayat Project</a></li>
        <li><a href="/backend/report/{{$report['slug']}}">Hasil Tools {!! $report['title'] !!}</a></li>
        <li class="active">{!! get_firstname_by_id($report['id_user']) !!}</li>
    </ol>
</div>
@stop
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan Learning Agility Index</h2>
            <a href="{{ url('print/gti/') }}/{{ $report['slug'] }}/{{ $report['id_user'] }}" class="btn btn-info">
              Download PDF
            </a>
        </div>
        <hr>
        <div class="row">
            <div class="col col-sm-12 col-md-7 col-lg-8">
                <div class="wpa-result-info card-panel grey lighten-4 flat">
                    <dl class="dl-horizontal">
                        <dt>Nama</dt>
                        <dd>{!! get_firstname_by_id($report['id_user']) !!}</dd>
                        <dt>Tanggal Pengerjaan</dt>
                        <dd>{{ $report['date'] }}</dd>
                     </dl>
                </div>
            </div>
            <div class="col col-sm-12 col-md-5 col-lg-4">
                <div class="wpa-result-summary card-panel grey lighten-2 flat">
                    <h6>Hasil GTQ :</h6>
                    <h3>{{ $report['avg'] }}</h3>
                    <strong>{{ $report['criteria'] }}</strong>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-8">

                <strong>Speed & Accuracy </strong>
                <div class="progress" style="height: 25px">
                    <div class="progress-bar {{ gtiStatus($report['subtest1']) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent($report['subtest1']) }}; height: 25px">
                        <span class="skill"><i class="val">{{ $report['subtest1'] }}</i></span>
                    </div>
                </div><br>
                <strong>Reasoning</strong>
                <div class="progress" style="height: 25px">
                    <div class="progress-bar {{ gtiStatus( $report['subtest2'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $report['subtest2'] ) }}; height: 25px">
                        <span class="skill"><i class="val">{{ $report['subtest2'] }}</i></span>
                    </div>
                </div><br>
                <strong> Focus & Attention </strong>
                <div class="progress" style="height: 25px">
                    <div class="progress-bar {{ gtiStatus( $report['subtest3'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $report['subtest3'] ) }}; height: 25px">
                        <span class="skill"><i class="val">{{ $report['subtest3'] }}</i></span>
                    </div>
                </div><br>
                <strong>Numerical Ability</strong>
                <div class="progress" style="height: 25px">
                    <div class="progress-bar {{ gtiStatus( $report['subtest4'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $report['subtest4'] ) }}; height: 25px">
                        <span class="skill"><i class="val">{{ $report['subtest4'] }}</i></span>
                    </div>
                </div><br>
                <strong>Technical Problem Solving </strong>
                <div class="progress" style="height: 25px">
                    <div class="progress-bar {{ gtiStatus( $report['subtest5'] ) }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="150" style="width: {{ gtiPercent( $report['subtest5'] ) }}; height: 25px">
                        <span class="skill"><i class="val">{{ $report['subtest5'] }}</i></span>
                    </div>
                </div><br>
            </div>
            <div class="col-xs-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Job</div>
                    <div class="panel-body">
                        <ul>
                            @foreach($report['jobs'] as $jobs)
                                <li>{{ $jobs -> job }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Keterangan</div>
                    <div class="panel-body">
                        <span class="label  gti-low"> Low  </span>
                        <span class="label  gti-mid"> Mid </span>
                        <span class="label  gti-high"> High </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
             <div class="panel panel-success">
                  <div class="panel-heading">Kekuatan</div>
                  <div class="panel-body">
                      <ul>
                        @if($kekuatan)
                            @foreach($kekuatan as $key)
                                @if($key)
                                <li>{{ $key }}</li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                  </div>
                </div>
            </div>
            <div class="col-xs-6">
             <div class="panel panel-danger">
                  <div class="panel-heading">Kelemahan</div>
                  <div class="panel-body">
                      <ul>
                        @if($kelemahan)
                            @foreach($kelemahan as $key)
                                @if($key)
                                <li>{{ $key }}</li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
