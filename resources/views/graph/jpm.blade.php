@extends('layouts.default')
@section('inline-css')
<style>
    .material-icons.md-36 { font-size: 36px; }
    .material-icons.md-48 { font-size: 48px; }
    .card-panel.small {font-size: 100%;height:300px;}
    .card-panel.flat {box-shadow: none;}
    .card-panel h6 {
        font-size: 18px;
        font-weight: 300;
        padding-bottm:10px;
    }
    .wpa-result-desc {margin: 60px 0;}
    .wpa-result-summary, .wpa-result-info {min-height: 140px;}
    .wpa-result-summary h2 {margin:0;}
    .wpa-result-summary .h6 {margin:0;}
    .wpa-result-chart .chart-box {overflow:hidden;}
    .wpa-result-chart #most-graph, .wpa-result-chart #least-graph, .wpa-result-chart #change-graph,  .wpa-result-chart #jpm-graph{
        position: relative;
        top: 10px;
    }
    @media (min-width: 768px) {
    .wpa-result-info dl {margin-bottom:0;}
    .wpa-result-info dt {text-align:left;width:150px;}
    .wpa-result-info dd {margin-left:120px;}
    }
    .canvasjs-chart-credit {display:none!important;}
    .card-panel.grey{
        background-color: #fff;
        margin-bottom: 30px;
        padding: 20px;
    }
</style>
@stop
@section('breadcrumb')
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<div id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="/home">Dashboard</a></li>
        <li><a href="/backend/history-project">Riwayat Test</a></li>
        <li><a href="/backend/report-wpa-with-user/{{$data['slug']}}">Hasil Test Personality Profile Assessment</a></li>
        <li class="active">Hasil JPM {!! get_firstname_by_id($data['id_user']) !!}</li>
    </ol>
</div>
@stop
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Job Person Match</h2>
        </div>
        <div class="wpa-result">
            <div class="row">
                <div class="col col-sm-12 col-md-7 col-lg-8 ">
                    <div class="wpa-result-info card-panel grey lighten-4 flat">
                        <dl class="dl-horizontal">
                            <dt>Nama</dt>
                            <dd>{!! get_firstname_by_id($data['id_user']) !!}</dd>
                            <dt>NIP</dt>
                            <dd>-</dd>
                            <dt>Periode</dt>
                            <dd>{{ date('Y-m-d', strtotime($data['start_date'])) }} - {{ date('Y-m-d', strtotime($data['end_date'])) }}</dd>
                            <dt>Tanggal Pengerjaan</dt>
                            <dd>{{ $data['date'] }}</dd>
                        </dl>
                    </div>
                </div>
                <div class="col col-sm-12 col-md-5 col-lg-4">
                    <div class="wpa-result-summary card-panel grey lighten-2 flat">
                        <h6>Tipe Kepribadian Anda:</h6>
                        <h3>{{ strtoupper($data['result']) }}</h3>
                        <strong>{{ $data['type_kepribadian']}}</strong>
                    </div>
                </div>
            </div>
            <div class="wpa-result-chart">
                <div class="row">
                    <div class="col-md-4">
                        <form name="filterJob" id="filterJob" action="/backend/report/get-jpm/{{$data['slug']}}/{{$data['id_user']}}" method="get" enctype="multipart/form-data" id="import" data-submit="noAjax">
                            <div class="form-group">
                             <h6>Profil Pekerjaan</h6>
                                <select class="form-control" id="cmb_jobProf" name="profile">
                                    @foreach($data['job_profile'] as $dataJP)
                                        @if($dataJP->id == $data['request'])
                                            <option value="{{$dataJP->id}}" selected="selected">{{$dataJP->jobs_name}}</option>       
                                        @else
                                            <option value="{{$dataJP->id}}">{{$dataJP->jobs_name}}</option>
                                        @endif
                                   @endforeach 
                                </select>
                            </div>
                            <div class="form-group">
                                {!! Form::submit(isset($buttonText) ? $buttonText : 'Lihat',['class' => 'btn btn-primary pull-left', 'id'=>'filter_jpm']) !!}
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-sm-12 col-md-3">
                        <div class="chart-box">
                            <div id="most-graph" style="height: 400px;"></div>
                        </div>
                        <h5 class="text-center">JPM : {{ round($data['agreement_1'], 2) }}%</h5>
                    </div>
                    <div class="col col-sm-12 col-md-3">
                        <div class="chart-box">
                            <div id="least-graph" style="height: 400px;"></div>
                        </div>
                       <h5 class="text-center">JPM : {{ round($data['agreement_2'], 2) }}%</h5>
                    </div>
                    <div class="col col-sm-12 col-md-3">
                        <div class="chart-box">
                            <div id="change-graph" style="height: 400px;"></div>
                        </div>
                       <h5 class="text-center">JPM : {{ round($data['agreement_3'], 2) }}%</h5>
                    </div>
                    <div class="col col-sm-12 col-md-3">
                        <div class="chart-box">
                            <div id="jpm-graph" style="height: 400px;"></div>
                        </div>
                       <h5 class="text-center">JPM : {{ round($data['result_jpm'], 2) }}%</h5>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>
@stop
@section("inline-js")
        {!! Html::script('assets/vendor/canvasjs/canvasjs.min.js') !!}
        {!! Html::script('assets/vendor/canvasjs/jquery.canvasjs.min.js') !!}
<script>

$(function() {
    $("#most-graph").CanvasJSChart({
        title: {
            text: "MOST"
        },
        axisY: {
            
            includeZero: false,
            minimum: -8,
            maximum: 8,
            interval: 2,
            labelFormatter: function(e){
                if(e.value == 0){
                    return  e.value;
                }else{
                    return '';
                }
            },
            tickLength: 0
        },
        axisX: {
            interval: 1
        },
        toolTip:{
            enabled: false,       
            animationEnabled: true 
        },
        data: [
        {   
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['most_d'] }}},
                { label: "I",  y: {{ $data['most_i'] }} },
                { label: "S",y: {{ $data['most_s'] }} },
                { label: "C",y: {{ $data['most_c'] }} }
            ]
        },
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['jpm_d'] }} },
                { label: "I",  y: {{ $data['jpm_i'] }} },
                { label: "S",  y: {{ $data['jpm_s'] }} },
                { label: "C",  y: {{ $data['jpm_c'] }} }
            ]
        }

        ]
    });

    $("#least-graph").CanvasJSChart({
        title: {
            text: "LEAST"
        },
        axisY: {
          
            includeZero: false,
            minimum: -8,
            maximum: 8,
            interval: 2,
            labelFormatter: function(e){
                if(e.value == 0){
                    return  e.value;
                }else{
                    return '';
                }
            },
            tickLength: 0
        },
        axisX: {
            interval: 1
        },
        toolTip:{
            enabled: false,       
            animationEnabled: true 
        },
        data: [
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['lest_d'] }}},
                { label: "I",  y: {{ $data['lest_i'] }} },
                { label: "S",y: {{ $data['lest_s'] }}},
                { label: "C",y: {{ $data['lest_c'] }} }
            ]
        },
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['jpm_d'] }} },
                { label: "I",  y: {{ $data['jpm_i'] }} },
                { label: "S",  y: {{ $data['jpm_s'] }} },
                { label: "C",  y: {{ $data['jpm_c'] }} }
            ]
        }
        ]
    });

    $("#change-graph").CanvasJSChart({
        title: {
            text: "CHANGE"
        },
        axisY: {
          
            includeZero: false,
            minimum: -8,
            maximum: 8,
            interval: 2,
            labelFormatter: function(e){
                if(e.value == 0){
                    return  e.value;
                }else{
                    return '';
                }
            },
            tickLength: 0
        },
        axisX: {
            interval: 1
        },
        toolTip:{
            enabled: false,       
            animationEnabled: true 
        },
        data: [
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['change_d'] }} },
                { label: "I",  y: {{ $data['change_i'] }} },
                { label: "S",  y: {{ $data['change_s'] }} },
                { label: "C",  y: {{ $data['change_c'] }} }
            ]
        },
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['jpm_d'] }} },
                { label: "I",  y: {{ $data['jpm_i'] }} },
                { label: "S",  y: {{ $data['jpm_s'] }} },
                { label: "C",  y: {{ $data['jpm_c'] }} }
            ]
        }
        ]
    });

    $("#jpm-graph").CanvasJSChart({
        title: {
            text: "Job Person Match"
        },
        axisY: {
            minimum: 0,
            maximum: 100,
            lineThickness: 0,
            tickThickness: 0,
            interval: 10
        },
        axisX: {
            
            
            tickThickness: 0,
            interval: 1,
        },
        animationEnabled: true,
        legend: {
            verticalAlign: "bottom",
            horizontalAlign: "center"
        },
        data: [
        
        {
            type: "column", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: 'JPM', y: {{ round($data['result_jpm'], 2) }} }
            ]
        }

        ]
    });
});

</script>
@stop