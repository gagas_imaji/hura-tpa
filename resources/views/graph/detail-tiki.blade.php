@extends('layouts.default')
@section('breadcrumb')
<div id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/backend/history-project">Riwayat Project</a></li>
        <li><a href="/backend/report/{{$data['slug']}}">Hasil Tools {!! $data['title'] !!}</a></li>
        <li class="active">{!! get_firstname_by_id($data['id_user']) !!}</li>
    </ol>
</div>
@stop
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan Test Intelegensi Kolektif Indonesia</h2>
            <a href="{{ url('print/tiki/') }}/{{ $data['slug'] }}/{{ $data['id_user'] }}" class="btn btn-info">
              Download PDF
            </a>
        </div>
        <hr>
        <div class="row">
            <div class="col col-sm-12 col-md-7 col-lg-8">
                <div class="wpa-result-info card-panel grey lighten-4 flat">
                    <dl class="dl-horizontal">
                        <dt>Nama</dt>
                        <dd>{!! get_firstname_by_id($data['id_user']) !!}</dd>
                        <dt>Tanggal Pengerjaan</dt>
                        <dd>{{ $data['date'] }}</dd>
                     </dl>
                </div>
            </div>
            <div class="col col-sm-12 col-md-5 col-lg-4">
                <div class="wpa-result-summary card-panel grey lighten-2 flat">
                    <h6>Taraf Kecerdasan (IQ) :</h6>
                    <h3>{{ $data['iq'] }}</h3>
                    <strong>{{ $data['golongan_iq'] }}</strong>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-4">
                <strong>&nbsp;</strong><hr>
            </div>
             <div class="col-xs-2">
                <strong>Nilai Standar</strong><hr>
            </div>
            <div class="col-xs-6">
                <strong>#</strong><hr>
            </div>
        </div>
         <div class="row">
            <div class="col-xs-4">
                  <strong>Berhitung Angka</strong><p>Kecepatan dan ketepatan berhitung</p>   <hr>
            </div>
             <div class="col-xs-2">
                <strong>{!! $data['nilai_standar'][0] !!}</strong><hr style="margin-top: 40px;">
            </div>
            <div class="col-xs-6">
                <div class="progress" style="height: 25px">
                    <div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="28" style="width: {{ tikiPercent($data['nilai_standar'][0], 28) }}; height: 25px">
                        <span class="skill"><i class="val">{!! $data['nilai_standar'][0] !!}</i></span>
                    </div>
                </div>
                <hr style="margin-top: 23px;">
            </div>
        </div>
         <div class="row">
            <div class="col-xs-4">
                <strong>Gabungan bagian</strong><p>Daya Konsentrasi ( Sintesis dan Analisis )</p>   <hr>
            </div>
             <div class="col-xs-2">
                <strong>{!! $data['nilai_standar'][1] !!}</strong><hr style="margin-top: 40px;">
            </div>
            <div class="col-xs-6">
                <div class="progress" style="height: 25px">
                    <div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['nilai_standar'][1], 30) }}; height: 25px">
                        <span class="skill"><i class="val">{!! $data['nilai_standar'][1] !!}</i></span>
                    </div>
                </div>
                <hr style="margin-top: 23px;">
            </div>
        </div>
         <div class="row">
            <div class="col-xs-4">
                   <strong>Hubungan Kata   </strong> <p>Penilaian realitas, berpikir, praktis-konkrit</p>    <hr>
            </div>
             <div class="col-xs-2">
                <strong>{!! $data['nilai_standar'][2] !!}</strong><hr style="margin-top: 40px;">
            </div>
            <div class="col-xs-6">
                <div class="progress" style="height: 25px">
                    <div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['nilai_standar'][2], 30) }}; height: 25px">
                        <span class="skill"><i class="val">{!! $data['nilai_standar'][2] !!}</i></span>
                    </div>
                </div>
                <hr style="margin-top: 23px;">
            </div>
        </div>
         <div class="row">
            <div class="col-xs-4">
                 <strong>Abstraksi Non Verbal   </strong> <p>Kemampuan abstraksi, daya klasifikasi</p>   <hr>
            </div>
             <div class="col-xs-2">
                <strong>{!! $data['nilai_standar'][3] !!}</strong><hr style="margin-top: 40px;">
            </div>
            <div class="col-xs-6">
                <div class="progress" style="height: 25px">
                    <div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width: {{ tikiPercent($data['nilai_standar'][3], 30) }}; height: 25px">
                        <span class="skill"><i class="val">{!! $data['nilai_standar'][3] !!}</i></span>
                    </div>
                </div>
                <hr style="margin-top: 23px;">
            </div>
        </div>
           <div class="row">
            <div class="col-xs-4">
                 <strong>Jumlah Angka TiQi</strong>   <hr>
            </div>
             <div class="col-xs-2">
                <strong>{!! $data['total_tiki'] !!}</strong><hr >
            </div>

        </div>
    </div>
    <div class="col-xs-12">
    <div class="row">
        <div class="col-xs-6">
            <strong>Kesimpulan</strong>
            <table class="table table-hover table-noborder">

                <tr>
                    <td>1</td>
                    <td>Space and Non-Verbal Reasoning</td>
                    <td>F1  : <strong>{!! $data['f1'] !!} {!! $data['golongan_f1'] !!} </strong></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>General Scholastic Aptitude</td>
                    <td>F2  : <strong>{!! $data['f2'] !!} {!! $data['golongan_f2'] !!} </strong></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Speed And Accuracy</td>
                    <td>F3  : <strong>{!! $data['f3'] !!} {!! $data['golongan_f3'] !!} </strong></td>
                </tr>
            </table>
        </div>
    </div>
    </div>
</section>
@stop
