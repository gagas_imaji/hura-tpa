@include('layouts.head')
<body class="skin-black" >
    <div class="wrapper">
            @include('layouts.header')
			
            @include('layouts.sidebar')
			
        <div class="content-wrapper">
			
            @yield('breadcrumb')
            
            @include('common.message')
			
			<section class="content">
				@yield('content')
			</section>
		</div>
		@yield('script')
	    <div id="overlay"></div>
	    <div class="modal fade" id="myModal" role="basic" aria-hidden="true">
	        <div class="modal-dialog modal-lg">
	            <div class="modal-content">
				</div>
			</div>
		</div>
		@include('layouts.foot')
	</div>
