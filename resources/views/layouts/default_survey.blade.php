@include('layouts.head_survey')
<body class="blue-grey lighten-5">
	<div class="page-loader-wrapper active" style="text-align: center;">
        <div class="loader">
            <div class="preloader-wrapper">
                <div class="spinner-layer pl-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Mohon tunggu...</p>
        </div>
    </div>		    
    <div class="page-loader-wrapper loadings" style="text-align: center; display: none;">
        <div class="loader info-resend">
            <div class="preloader-wrapper">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Mohon tunggu...</p>
            Klik tombol di bawah jika proses berlangsung lebih dari 1 menit <br/> <br/>
            <button class="btn resend">Resend</button>
        </div>
        <div class="loader process-resend" style="display: none;">
            <div class="preloader-wrapper">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Sedang mengirim ulang data</p>
        </div>
    </div>
	<main>
		@yield('content')
	</main>

    <div id="overlay"></div>
    <div class="modal fade" id="myModal" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
			</div>
		</div>
	</div>
	@include('layouts.foot_survey')

