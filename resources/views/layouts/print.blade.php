<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Detil Laporan</title>


    {!! Html::style('assets/vendor/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('assets/css/chart.css') !!}
      @yield('inline-css')
</head>

<body>
    @yield('content')

    {!! Html::script('assets/vendor/jquery/jquery.min.js') !!}
    {!! Html::script('assets/vendor/bootstrap/js/bootstrap.min.js') !!}
    @yield('inline-script')
</body>

</html>
