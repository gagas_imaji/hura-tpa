 <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!--<link rel="apple-touch-icon" href="pages/ico/60.png">
		<link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
		<link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
		<link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
	<link rel="icon" type="image/x-icon" href="favicon.ico" />-->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<link href="<?php echo asset('assets/vendor/survey/pace/css/pace-theme-min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo asset('assets/vendor/survey/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="<?php echo asset('assets/css/materialize.min.css'); ?>"  />
	<link href="<?php echo asset('assets/css/animate.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo asset('assets/css/tools.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo asset('assets/css/sprites.css'); ?>" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" type="image/png" href="<?php echo asset('assets/image/favicon.png'); ?>"/>
    <title>{!! config('config.application_name') ? : config('constants.default_title') !!}</title>
    <!--[if lt IE 9]>
    	<script src="<?php echo asset('assets/js/html5shiv.js'); ?>"></script>
  	<![endif]-->
</head>


