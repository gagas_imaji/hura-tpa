    
    
    @yield('script')
    <!-- Global site tag (gtag.js) - Google Analytics Danamon Talentlytica on SSO-->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-68533947-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-68533947-2');
    </script>
</body>
</html>