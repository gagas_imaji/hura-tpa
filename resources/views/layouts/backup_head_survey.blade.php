 <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<link href="<?php echo asset('assets/vendor/pace/css/pace-theme-min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" />
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<link href="<?php echo asset('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="<?php echo asset('assets/css/materialize.min.css'); ?>"  />
	<link href="<?php echo asset('assets/css/animate.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo asset('assets/css/tools.css'); ?>" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="<?php echo asset('assets/css/style.css'); ?>"  />	

    <title>{!! config('config.application_name') ? : config('constants.default_title') !!}</title>


</head>


