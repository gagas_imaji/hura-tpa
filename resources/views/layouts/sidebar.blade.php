            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu">
                         <li class="treeview"><a href="/home"><i class="fa fa-home fa-fw" class="detailed"></i> {{trans('messages.home') }}</a></li>

                        @if(Entrust::can('manage-role'))
                            <li><a href="/role"><i class="fa fa-user fa-fw"></i> {{trans('messages.user').' '.trans('messages.role') }}</a></li>
                        @endif
                        @if(Entrust::can('manage-permission'))
                            <li><a href="/permission"><i class="fa fa-lock fa-fw"></i> {{trans('messages.permission') }}</a></li>
                        @endif
                        @if(Entrust::can('manage-custom-field') && config('config.enable_custom_field'))
                            <li><a href="/custom-field"><i class="fa fa-wrench fa-fw"></i> {{trans('messages.custom').' '.trans('messages.field') }}</a></li>
                        @endif
                        @if(Entrust::can('manage-language') && config('config.multilingual'))
                            <li><a href="/language"><i class="fa fa-globe fa-fw"></i> {{trans('messages.language') }}</a></li>
                        @endif
                        @if(Entrust::can('manage-template') && config('config.enable_email_template'))
                            <li><a href="/template"><i class="fa fa-envelope fa-fw"></i> {{trans('messages.email').' '.trans('messages.template') }}</a></li>
                        @endif
                        @if(config('config.enable_activity_log'))
                        <li><a href="/activity-log"><i class="fa fa-bars fa-fw"></i> {{trans('messages.activity').' '.trans('messages.log') }}</a></li>
                        @endif
                        @if(Entrust::can('manage-message') && config('config.enable_message'))
                        <li><a href="/message"><i class="fa fa-paper-plane fa-fw"></i> {{trans('messages.message') }}</a></li>
                        @endif
                        @if(Entrust::can('manage-ip-filter') && config('config.enable_ip_filter'))
                            <li><a href="/ip-filter"><i class="fa fa-ellipsis-v fa-fw"></i> IP {{trans('messages.filter') }}</a></li>
                        @endif

                        <!-- ---------------------------------------------------------------------------------- -->

                        @if(Entrust::can('manage-corporate'))
                            <li><a href="/corporate"><i class="fa fa-building-o fa-fw"></i> Corporate</a></li>
                        @endif
                        @if(Entrust::can('manage-survey'))
                            <li><a href="/aspek-psikograph"><i class="fa fa-bar-chart fa-fw"></i> Standar Nilai Aspek Psikograph </a></li>
                            <li><a href="/project"><i class="fa fa-list fa-fw"></i> Kelola Project </a></li>
                            <!-- <li><a href="/import"><i class="fa fa-file fa-fw"></i> Import Survey </a></li> -->
                            <li><a href="/backend/history-project"><i class="fa fa-area-chart fa-fw"></i> Riwayat Project</a></li>
                            <li><a href="/backend/user-report"><i class="fa fa-line-chart fa-fw"></i> Score User</a></li>
                            <!-- <li><a href="/standar_nilai"><i class="fa fa-sliders fa-fw"></i> Standar Nilai Psikograph</a></li> -->
                        @endif
                        <!-- @if(Entrust::can('create-question'))
                            <li><a href="/question"><i class="fa fa-tasks fa-fw"></i> Create Question </a></li>
                        @endif -->
                        @if(Entrust::can('take-test'))
                            <li><a href="/history-survey"><i class="fa fa-calendar-check-o fa-fw"></i> History Tools</a></li>
                        @endif
                        @if(Entrust::can('manage-user'))
                            <li><a href="/user"><i class="fa fa-users fa-fw"></i> Manajemen User</a></li>
                        @endif
                        @if(Entrust::can('manage-jpm'))
                            <li><a href="/job-characteristic"><i class="fa fa-users fa-fw"></i> Manajemen JPM</a></li>
                        @endif

                        @if (true)
                            <li><a href="{{ url('backend/report-user') }}"><i class="fa fa-users fa-fw"></i> Laporan User</a></li>
                        @endif
                    </ul>
                </section>
            </aside>