
                                <div class="form-group form-group-default required">
                                    <label for="name">Nama</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Nama User">
                                </div>
                                <div class="form-group form-group-default required">
                                    <label for="email">Jenis Kelamin</label>
                                    <select name="gender" class="form-control">
                                        <option value="pria">Pria</option>
                                        <option value="wanita">Wanita</option>
                                    </select>
                                </div>
                                <div class="form-group form-group-default required">
                                    <label for="email">{{trans('messages.email')}}</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="{!! trans('messages.email') !!}">
                                </div>
                                @if(!config('config.login'))
                                <div class="form-group form-group-default">
                                    <label for="email">{{trans('messages.username')}}</label>
                                    <input type="text" name="username" id="username" class="form-control" placeholder="{!! trans('messages.username') !!}">
                                </div>
                                @endif
                                <div class="form-group form-group-default required">
                                    <label for="email">{{trans('messages.password')}}</label>
                                    <input type="password" name="password" id="password" class="form-control {{(config('config.enable_password_strength_meter') ? 'password-strength' : '')}}" placeholder="{!! trans('messages.password') !!}">
                                </div>
                                <div class="form-group form-group-default required">
                                    <label for="email">Ulangi {{trans('messages.password')}}</label>
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{!! trans('messages.confirm').' '.trans('messages.password') !!}">
                                </div>
                                @if(Entrust::hasRole(DEFAULT_ROLE))
                                    <div class="form-group form-group-default required">
                                        <label for="corporate">Corporate</label>
                                        <select name="corporate" class="form-control" id="corporate">
                                            @foreach($corporate as $corp)
                                                <option value="{{ $corp->id }}">{{ $corp->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <input type="hidden" name="corporate" id="corporate" class="form-control" value="{!! $user_corporate['id'] !!}" />
                                    </div>
                                @endif
                                @if(Entrust::hasRole(DEFAULT_ROLE))
                                    <div class="form-group form-group-default required">
                                        <label for="division">Divisi</label>
                                        <select name="division" class="form-control" id="division">
                                            <option></option>
                                        </select>
                                    </div>
                                @else
                                    <div class="form-group form-group-default required">
                                        <label for="division">Divisi</label>
                                        {!! Form::select('division', $division, null, array('class'=>'form-control select2-container full-width', 'id'=>'division'))!!}
                                    </div>
                                @endif
                                {{ getCustomFields('user-registration-form') }}
                                <!-- @if(Auth::check())
                                <div class="form-group">
                                    <input name="send_welcome_email" type="checkbox" class="switch-input" data-size="mini" data-on-text="Yes" data-off-text="No" value="1"> {{trans('messages.send')}} welcome email
                                </div>
                                @endif -->
                                @if(config('config.enable_tnc'))
                                <div class="form-group form-group-default">
                                    <input name="tnc" type="checkbox" class="switch-input" data-size="mini" data-on-text="Yes" data-off-text="No" value="1"> I accept <a href="/terms-and-conditions">Terms & Conditions</a>.
                                </div>
                                @endif
                                @if(config('config.enable_recaptcha') && !Auth::check())
                                <div class="g-recaptcha" data-sitekey="{{config('config.recaptcha_key')}}"></div>
                                <br />
                                @endif

<script type="text/javascript">
    $('#corporate').change(function()
    {
        var id_corp = $('#corporate').val();
        
        var view_data;

        $.ajax({
            url : "get-division/"+id_corp,    
            data: {id_corporate: id_corp},
            type: "GET", 
            success :
            function(respon){
                doWork(respon);                
            }
        });

        //fungsi select chain, nambahin option divisi berdasarkan corporate
        function doWork(data){ 
            var $div = $('#division');
            var i;
            $div.find('option').remove().end();
            for(i = 0; i < data.length; i++){
                $div.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
            }

        }
    });

</script>