@extends('guest_layouts.default')

    @section('content')

        @if(config('config.logo') && File::exists(config('constant.upload_path.logo').config('config.logo')))
            <div class="logo text-center">
                <img src="{{ url('assets/image') }}/logo-danamon.png" class="logo-image" alt="Logo">
            </div>
        @endif

        @if(!getMode())
            @include('common.notification',['message' => 'You are free to perform all actions. The demo gets reset in every 30 minutes.' ,'type' => 'danger'])
        @endif

        
        
        @if(!getMode())
        <div class="row" style="margin-bottom: 15px;">
            <h4 class="text-center">For Demo Purpose</h4>
            <div class="col-md-6">
                <a href="#" data-username="admin" data-email="support@wmlab.in" data-password="123456" class="btn btn-block btn-primary login-as">Login as Admin</a>
            </div>
            <div class="col-md-6">
                <a href="#" data-username="user" data-email="accounts@wmlab.in" data-password="123456" class="btn btn-block btn-danger login-as">Login as User</a>
            </div>
        </div>
        @endif

        <div class="login-box">
        <div class="logo"><a href="" style="text-align: left;"><img src="{{ url('assets/image') }}/logo-danamon.png" class="logo-image" alt="Logo"></a></div>
            <div class="card">
                <div class="card-panel">
                    <form role="form" action="{!! URL::to('/login') !!}" method="post" class="login-form" id="sign_in" data-submit="noAjax">
                        {!! csrf_field() !!}
                        <div class="msg">Sign in to start your session</div>
                        
                        @if(config('config.login'))
                            <div class="row">
                                <div class="form-group col s12">
                                    <div class="input-field">
                                        <input id="email" name="email" type="email" class="validate" required>
                                        <label for="email">{!! trans('messages.email') !!}</label>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="form-group col s12">
                                    <div class="input-field">
                                        <input id="username" name="username" type="username" class="validate" required>
                                        <label for="username">{!! trans('messages.username') !!}</label>
                                    </div>
                                </div>
                            </div>
                        @endif
                            <div class="row">
                                <div class="form-group col s12">
                                    <div class="input-field">
                                        <input id="password" name="password" type="password" class="validate" required>
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                        </div>
                        @if(config('config.enable_remember_me'))
                            <div class="row">
                                <div class="form-group col s12">
                                    <div class="input-field">
                                        <input name="remember" type="checkbox" class="switch-input" data-size="mini" data-on-text="Yes" data-off-text="No" value="1"> {!! trans('messages.remember_me') !!}
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col s6 right text-right"><button class="btn btn-sm waves-effect waves-light" type="submit" id="sign_in">SIGN IN</button></div>
                            @if(config('config.enable_forgot_password'))
                                <div class="col s6 forgot-password">
                                    <a href="/password/reset">Forgot Password?</a>
                                </div>
                            @endif
                        </div>

                        @if(config('config.enable_user_registration'))
                            <div class="col s6 forgot-password">
                                <a href="/register" class="btn btn-block btn-danger">{!! trans('messages.create').' '.trans('messages.account') !!}?</a>
                            </div>
                        @endif
                    </form>
                    
                </div>
            </div>
        </div>

            @if(config('config.enable_social_login'))
            <hr class="login-social-or"/>
            <div class="text-center">
                <p style="font-weight:bold;">Or</p>
                @foreach(config('constant.social_login_provider') as $provider)
                    @if(config('config.enable_'.$provider.'_login'))
                    <a class="btn btn-social btn-{{$provider.(($provider == 'google') ? '-plus' : '')}}" href="/auth/{{$provider}}">
                        <i class="fa fa-{{$provider}}"></i> {{toWord($provider)}}
                    </a>
                    @endif
                @endforeach
            </div>
            @endif
        <!-- <div class="credit" style="margin-top: 10px;">{{config('config.credit')}}</div> -->
    @stop

    @section('script')
        <script src="<?php echo asset('assets/vendor/jquery/jquery-3.1.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo asset('assets/vendor/jquery-validation/jquery.validate.min.js'); ?>"></script>
        
        <script>
            $(function () {
                $('#sign_in').validate({
                    highlight: function (input) {
                        console.log(input);
                        $(input).parents('.input-field').addClass('error');
                    },
                    unhighlight: function (input) {
                        $(input).parents('.input-field').removeClass('error');
                    },
                    errorPlacement: function (error, element) {
                        $(element).parents('.form-group').append(error);
                    }
                });
            });     
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics Danamon Talentlytica on SSO-->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-68533947-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-68533947-2');
        </script>
    @stop