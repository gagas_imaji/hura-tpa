@extends('layouts.default')

@section('breadcrumb')
<div id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/backend/raw-data">Raw Data Tools</a></li>
        <li><a href="/backend/raw-data/nilai/{{$info['slug']}}/{{$info['slug_project']}}">Row User - Test Intelegensi Kolektif Indonesia</a></li>
        <li>{{ $info['section_name'] }}</li>

    </ol>
</div>
@stop
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan {{ get_firstname_by_id($info['user_id']) }}</h2>
        </div>
        <hr>
        <div class="">
            <table class="table table-bordered" id="raw-jawaban-tiki">
                @if($data != '-')
                    <thead>
                      <tr class="headings">
                        <th>No</th>
                        <th>Pertanyaan</th>
                        <th>Jawaban</th>
                        <th>Kunci</th>
                        <th>Hasil</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                        
                        @foreach($data as $key)
                            <tr>
                                <td>{!! $key['nomor'] !!}</td>
                                <td style="width: 200px;">{!! $key['pertanyaan'] !!}</td>
                                <td>
                                    @foreach($key['jawaban'] as $jawaban)

                                        @if($info['section_name'] != 'Hubungan Kata')
                                            <img src="/assets/image/{{$info['folder_image']}}/{!! $jawaban !!}" /> <br/>
                                        @else
                                            {!! $jawaban !!} <br/>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($key['kunci'] as $kunci)
                                        @if($info['section_name'] != 'Hubungan Kata')
                                            <img src="/assets/image/{{$info['folder_image']}}/{!! $kunci !!}" /> <br/>
                                        @else
                                            {!! $kunci !!} <br/>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($key['hasil'] as $kunci)
                                        @if($kunci == 0)
                                            <p><span class="label label-danger"> salah</span> </p>
                                        @else
                                            <p><span class="label label-success"> benar</span> </p>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                            <?php $i++;?>
                        @endforeach
                        
                    </tbody>
                    @else   
                        <tr>
                            <td colspan="5" class="text-center">Peserta tidak menjawab pertanyaan pada subtest ini</td>
                        </tr>
                    @endif
                </table>

        </div>
    </div>
</section>
<script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $('#raw-jawaban-tiki').DataTable({
            "columnDefs": [ {
                "targets"  : 'action',
                "orderable": false,
                "order": []
            }],
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Data tidak ditemukan",
                "info": "Halaman _PAGE_ / _PAGES_",
                "infoEmpty": "Data tidak tersedia",
                "infoFiltered": "(difilter dari _MAX_ data)",
                "search": "Cari",
                "paginate": {
                    "previous": "&lt;",
                    "next": "&gt;"
                },
            },
            "dom": 'Bfrtip',
            "buttons": [ {
                extend: "excel",
                text: "Download .xlsx",
                filename: "Raw Data LAI - "
            } ],
            "pageLength": 10
        });

    });
</script>
@stop