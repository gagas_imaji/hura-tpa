@extends('layouts.default')
    @section('breadcrumb')
        <div id="breadcrumb">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/backend/raw-data">Raw Data Tools</a></li>
                <li class="active">Row User - Learning Agility Index</li>
            </ol>
        </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger">Laporan Learning Agility Index - {!! $info_project['name'] !!}</div>
                    </div>
                    <div class="panel-body full">
                        <table class="table" id="list-raw-lai">
                            <thead>
                              <tr class="headings">
                                <th class="text-center">Nama Peserta</th>
                                <th class="text-center">Subtest 1</th>
                                <th class="text-center">Subtest 2</th>
                                <th class="text-center">Subtest 3</th>
                                <th class="text-center">Subtest 4</th>
                                <th class="text-center">Subtest 5</th>
                                <th class="action"></th>
                              </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $key)
                                        <tr class="">
                                            <td>{!! $key['first_name'] !!}</td>
                                            <td class="text-center">{!! $key['subtest_1'] !!}</td>
                                            <td class="text-center">{!! $key['subtest_2'] !!}</td>
                                            <td class="text-center">{!! $key['subtest_3'] !!}</td>
                                            <td class="text-center">{!! $key['subtest_4'] !!}</td>
                                            <td class="text-center">{!! $key['subtest_5'] !!}</td>
                                            <td class="text-center">{!! $key['kesimpulan'] !!}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('#list-raw-lai').DataTable({
                    "columnDefs": [ {
                        "targets"  : 'action',
                        "orderable": false,
                        "order": []
                    }],
                    "language": {
                        "lengthMenu": "Tampilkan _MENU_ data",
                        "zeroRecords": "Data tidak ditemukan",
                        "info": "Halaman _PAGE_ / _PAGES_",
                        "infoEmpty": "Data tidak tersedia",
                        "infoFiltered": "(difilter dari _MAX_ data)",
                        "search": "Cari",
                        "paginate": {
                            "previous": "&lt;",
                            "next": "&gt;"
                        },
                    },
                    "dom": 'Bfrtip',
                    "buttons": [ {
                        extend: "excel",
                        text: "Download .xlsx",
                        filename: "Raw Data LAI - {!! $info_project['name'] !!}"
                    } ],
                    "pageLength": 10
                });

            });
        </script>
    @stop