@extends('layouts.default')

@section('breadcrumb')
<div id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/backend/raw-data">Raw Data Tools</a></li>
        <li><a href="/backend/raw-data/nilai/{{$info['slug']}}/{{$info['slug_project']}}">Row User - Learning Agility Index</a></li>
        <li>{{ $info['section_name'] }}</li>

    </ol>
</div>
@stop
@section('content')
<section class="content clearfix">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan {{ get_firstname_by_id($info['user_id']) }}</h2>
        </div>
        <hr>
        <div class="">
            <table class="table table-bordered" id="raw-jawaban-lai">
                @if($data != '-')
                    <thead> 
                      <tr class="headings">
                        <th class="text-center">No</th>
                        <th class="text-center">Pertanyaan</th>
                        <th class="text-center">Jawaban</th>
                        <th class="text-center">Kunci</th>
                        <th class="text-center">Hasil</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                        @foreach($data as $key)
                            <tr>
                                <td class="text-center">{!! $key['nomor'] !!}</td>
                                <td>{!! $key['pertanyaan'] !!}</td>
                                <td class="text-center">{!! $key['jawaban'] !!}</td>
                                <td class="text-center">
                                    @foreach($key['kunci'] as $kunci)
                                        {!! $kunci !!} <br/>
                                    @endforeach
                                    </td>
                                <td class="text-center">
                                @if($key['hasil'] == 0)
                                    <span class="label label-danger"> salah</span>
                                @else

                                    <span class="label label-success"> benar</span>
                                @endif
                                </td>
                            </tr>
                            <?php $i++;?>
                        @endforeach
                    </tbody>
                    @else   
                        <tr>
                            <td colspan="5" class="text-center">Peserta tidak menjawab pertanyaan pada subtest ini</td>
                        </tr>
                    @endif
                </table>
        </div>
    </div>
</section>
<script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $('#raw-jawaban-lai').DataTable({
            "columnDefs": [ {
                "targets"  : 'action',
                "orderable": false,
                "order": []
            }],
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Data tidak ditemukan",
                "info": "Halaman _PAGE_ / _PAGES_",
                "infoEmpty": "Data tidak tersedia",
                "infoFiltered": "(difilter dari _MAX_ data)",
                "search": "Cari",
                "paginate": {
                    "previous": "&lt;",
                    "next": "&gt;"
                },
            },
            "dom": 'Bfrtip',
            "buttons": [ {
                extend: "excel",
                text: "Download .xlsx",
                filename: "Raw Data {{ get_firstname_by_id($info['user_id']) }} - {{ $info['section_name'] }}"
            } ],
            "pageLength": 10
        });

    });
</script>
@stop