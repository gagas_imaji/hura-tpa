@extends('layouts.default')

    @section('breadcrumb')
    <div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li class="active">Raw Data Tools</li>
        </ol>
    </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger">Row Data Laporan Tools</div>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-hover table-bordered" id="list-raw-tools">
                            <thead>
                              <tr class="headings">
                                <th class="text-center">Project</th>
                                <th class="text-center">Judul Tools</th>
                                <th class="action"></th>
                              </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $key)
                                        <tr class="">
                                            <td>{!! $key['name'] !!}</td>
                                            <td>{!! $key['title'] !!}</td>
                                            <td class="text-center">{!! $key['detail'] !!}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('#list-raw-tools').DataTable({
                    "columnDefs": [ {
                        "targets"  : 'action',
                        "orderable": false,
                        "order": []
                    }]
                });

            });
        </script>
    @stop