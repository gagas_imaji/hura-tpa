@extends('layouts_user.default')
    
@section('breadcrumb')
<div id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="/home">Dashboard</a></li>
        <li><a href="/backend/history">Riwayat Tools</a></li>
        <li class="active">Hasil Tools PAPIKOSTICK</li>
    </ol>
</div>
@stop
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan Work Behavioural Assessment</h2>
        </div>
        <hr>
        <div class="row">
            <div class="col s12 m7 l12">
                <div class="wpa-result-info card-panel grey lighten-4 flat">
                    <dl class="dl-horizontal">
                        <dt>Nama</dt>
                        <dd>{!! get_firstname() !!}</dd>
                        <dt>NIP</dt>
                        <dd>-</dd>
                        <dt>Periode</dt>
                        <dd>{{ date('Y-m-d', strtotime($data['start_date'])) }} - {{ date('Y-m-d', strtotime($data['end_date'])) }}</dd>
                        <dt>Tanggal Pengerjaan</dt> 
                        <dd>{{ $data['date'] }}</dd>
                     </dl>
                </div>
            </div>
        </div>
        <hr>
        
         <div class="row">
             <div class="col-xs-12">
             <table class="table">
                 <thead>
                     <tr>
                         <th style="background-color:  #c3c3c1;">N</th>
                         <th style="background-color:  #c3c3c1;">G</th>
                         <th style="background-color:  #c3c3c1;">A</th>
                         <th style="background-color:  #5a5a5a ;">L</th>
                         <th style="background-color:  #5a5a5a ;">P</th>
                         <th style="background-color:  #5a5a5a ;">I</th>
                         <th style="background-color: #f8dff9;">T</th>
                         <th style="background-color: #f8dff9;">V</th>
                         <th style="background-color: #fee426;">X</th>
                         <th style="background-color: #fee426;">S</th>
                         <th style="background-color: #fee426;">B</th>
                         <th style="background-color: #fee426;">O</th>
                         <th style="background-color: #a8c3f3;">R</th>
                         <th style="background-color: #a8c3f3;">D</th>
                         <th style="background-color: #a8c3f3;">C</th>
                         <th style="background-color: #de0c26;">Z</th>
                         <th style="background-color: #de0c26;">E</th>
                         <th style="background-color: #de0c26;">K</th>
                         <th style="background-color: #9ede0c;">F</th>
                         <th style="background-color: #9ede0c;">W</th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <td>{{ $data['N']}}</td>
                         <td>{{ $data['G']}}</td>
                         <td>{{ $data['A']}}</td>
                         <td>{{ $data['L']}}</td>
                         <td>{{ $data['P']}}</td>
                         <td>{{ $data['I']}}</td>
                         <td>{{ $data['T']}}</td>
                         <td>{{ $data['V']}}</td>
                         <td>{{ $data['X']}}</td>
                         <td>{{ $data['S']}}</td>
                         <td>{{ $data['B']}}</td>
                         <td>{{ $data['O']}}</td>
                         <td>{{ $data['R']}}</td>
                         <td>{{ $data['D']}}</td>
                         <td>{{ $data['C']}}</td>
                         <td>{{ $data['Z']}}</td>
                         <td>{{ $data['E']}}</td>
                         <td>{{ $data['K']}}</td>
                         <td>{{ $data['F']}}</td>
                         <td>{{ $data['W']}}</td>
                     </tr>
                 </tbody>
             </table>
             </div>
        </div>
        <div class="row">
            <div class="col-xs-12" >
                <div class="papi-chart-container">
                    <div class="papi-chart">
                        <canvas id="chartPapi" width="250" height="250"></canvas>

                    </div>
                    
                </div>
            </div>
        </div>
        <div class="row">
             <div class="col-xs-12">
                 <table class="table">
                    <tr>
                        <th style="background-color:  #c3c3c1; text-align: center;" colspan="2">Arah Kerja</th>
                    </tr>
                    @foreach($data['result']['N'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>N</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['G'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>G</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['A'] as $key)
                        @if($key -> positive)
                            <tr>
                                <td>A</td>
                                <td>(+) {!! $key -> positive !!}</td>
                            </tr>
                        @endif
                        @if($key -> negative)
                            <tr>
                                <td></td>
                                <td>(-) {!! $key -> negative !!}</td>
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <th style="background-color:  #a8c3f3; text-align: center;" colspan="2">Gaya Kerja</th>
                    </tr>
                    @foreach($data['result']['R'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>R</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['D'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>D</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['C'] as $key)
                        @if($key -> positive)
                            <tr>
                                <td>C</td>
                                <td>(+) {!! $key -> positive !!}</td>
                            </tr>
                        @endif
                        @if($key -> negative)
                            <tr>
                                <td></td>
                                <td>(-) {!! $key -> negative !!}</td>
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <th style="background-color:  #f8dff9; text-align: center;" colspan="2">Aktivitas / Tempo Kerja</th>
                    </tr>
                    @foreach($data['result']['T'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>T</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['V'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>V</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    <tr>
                        <th style="background-color:  #9ede0c; text-align: center;" colspan="2">Keikutsertaan / Sikap Sebagai Bawahan</th>
                    </tr>
                    @foreach($data['result']['F'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>F</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['W'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>W</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    <tr>
                        <th style="background-color:  #fee426; text-align: center;" colspan="2">Sikap Sosial</th>
                    </tr>
                    @foreach($data['result']['O'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>O</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['B'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>B</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['S'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>S</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['X'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>X</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    <tr>
                        <th style="background-color:  #de0c26; color: white; text-align: center;" colspan="2">Tempramen</th>
                    </tr>
                    @foreach($data['result']['Z'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>Z</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['E'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>E</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['K'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>K</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    <tr>
                        <th style="background-color:  #5a5a5a; color: white; text-align: center;" colspan="2">Kepemimpinan</th>
                    </tr>
                    @foreach($data['result']['L'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>L</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['P'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>P</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                    @foreach($data['result']['I'] as $key)
                        @if($key -> positive)
                        <tr>
                            <td>I</td>
                            <td>(+) {!! $key -> positive !!}</td>
                        </tr>
                        @endif
                        @if($key -> negative)
                        <tr>
                            <td></td>
                            <td>(-) {!! $key -> negative !!}</td>
                        </tr>
                        @endif
                    @endforeach
                 </table>
             </div>
        </div>
        
        
    </div>
</section>
@stop
@section("inline-js")

      {!! Html::script('assets/vendor/jquery/jquery.min.js') !!}
<script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/js/user/user-page.js'); ?>"></script>
        {!! Html::script('assets/vendor/chart/Chart.bundle.js') !!}
<script>
var ctx = document.getElementById("chartPapi");
var myChart = new Chart(ctx, {
    type: 'radar',
    data: {
        labels: ["N","G","A","L","P","I","T","V","X","S","B","O","R","D","C","Z","E","K","F","W"],
        datasets: [{
            label: '',
            data: [
                {{ $data['N'] }},
                {{ $data['G'] }},
                {{ $data['A'] }},
                {{ $data['L'] }},
                {{ $data['P'] }},
                {{ $data['I'] }},
                {{ $data['T'] }},
                {{ $data['V'] }},
                {{ $data['X'] }},
                {{ $data['S'] }},
                {{ $data['B'] }},
                {{ $data['O'] }},
                {{ $data['R'] }},
                {{ $data['D'] }},
                {{ $data['C'] }},
                {{ ReverseIndexPapi($data['Z']) }},
                {{ $data['E'] }},
                {{ ReverseIndexPapi($data['K']) }},
                {{ $data['F'] }},
                {{ $data['W'] }}],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        tooltips: { enabled: false },
        scale: {
            ticks: {
                beginAtZero :0,
                min:0,
                max:10,
                display: false
            }
        },
        title: { 
            display: false
        }
               
    }
});
myChart.options.legend.display = false;  

</script>
@stop