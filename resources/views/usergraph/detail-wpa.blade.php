@extends('layouts_user.default')


@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan Work Personality Analytics</h2>
        </div>
        <div class="wpa-result">
            <div class="row">
                <div class="col s12 m7 l8">
                    <div class="wpa-result-info card-panel grey lighten-4 flat">
                        <dl class="dl-horizontal">
                            <dt>Nama</dt>
                            <dd>{!! get_firstname() !!}</dd>
                            <dt>NIP</dt>
                            <dd>-</dd>
                            <dt>Periode</dt>
                            <dd>{{ date('Y-m-d', strtotime($data['start_date'])) }} - {{ date('Y-m-d', strtotime($data['end_date'])) }}</dd>
                            <dt>Tanggal Pengerjaan</dt> 
                            <dd>{{ $data['date'] }}</dd>
                         </dl>
                         
                    </div>
                 
                </div>
                <div class="col s12 m5 l4">
                    <div class="wpa-result-summary card-panel grey lighten-2 flat">
                        <h6>Tipe Kepribadian Anda:</h6>
                        <h3>{{ strtoupper($data['result']) }}</h3>
                        <strong>{{ $data['type_kepribadian']}}</strong>
                    </div>
                </div>
            </div>
            
            <div class="wpa-result-chart">
                <div class="row">
                    <div class="col l4 m4 s12" >
                        <div id="most-graph" style="height: 400px;"></div>
                    </div>
                    <div class="col l4 m4 s12">
                       <div id="least-graph" style="height: 400px;"></div>
                    </div>
                    <div class="col l4 m4 s12">
                       <div id="change-graph" style="height: 400px;"></div>
                    </div>
                </div>

            </div>

                

        </div>
        <div class="wpa-result-desc">
            <div class="row">
                <div class="col s12">
                    <div class="card-panel grey lighten-4 flat">
                        <i class="material-icons md-48">person</i>
                        <h6>Uraian Kepribadian</h6>
                            {!! get_firstname() !!} {!! $data['uraian_kepribadian'] !!}
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">settings</i>
                        <h6>Karakteristik Umum</h6>
                        <ul class="browser-default">
                            <?php $i = 0; ?>
                            @foreach($data['karakteristik_umum'] as $key)
                                @if($i < 5)
                                    <li>{!! $key !!}</li>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">star</i>
                        <h6>Perilaku Kerja <span class="teal-text">(Kekuatan)</span></h6>
                        <ul class="browser-default">
                            <?php $i = 0; ?>
                            @foreach($data['perilaku_kerja_kekuatan'] as $key)
                                @if($i < 5 && $key != '')
                                    <li>{!! $key !!}</li>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
             
                <div class="col s12 m6 l4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">star_border</i>
                        <h6>Perilaku Kerja <span class="red-text">(Kelemahan)</span></h6>
                        <ul class="browser-default">
                            <?php $i = 0; ?>
                            @foreach($data['perilaku_kerja_kelemahan'] as $key)
                                @if($i < 5 && $key != '')
                                    <li>{!! $key !!}</li>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">sentiment_very_satisfied</i>
                        <h6>Suasana Emosi <span class="teal-text">(Kekuatan)</span></h6>
                        <ul class="browser-default">
                            <?php $i = 0; ?>
                            @foreach($data['suasana_emosi_kekuatan'] as $key)
                                @if($i < 5 && $key != '')
                                    <li>{!! $key !!}</li>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">sentiment_very_dissatisfied</i>
                        <h6>Suasana Emosi <span class="red-text">(Kelemahan)</span></h6>
                        <ul class="browser-default">
                            <?php $i = 0; ?>
                            @foreach($data['suasana_emosi_kelemahan'] as $key)
                                @if($i < 5 && $key != '')
                                    <li>{!! $key !!}</li>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">my_location</i>
                        <h6>Kekuatan</h6>
                        <ul class="browser-default">
                            <?php $i = 0; ?>
                            @foreach($data['kekuatan'] as $key)
                                @if($i < 5 && $key != '')
                                    <li>{!! $key !!}</li>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel small grey lighten-4 flat">
                        <i class="material-icons md-48">my_location</i>
                        <h6>Kelemahan</h6>
                        <ul class="browser-default">
                            <?php $i = 0; ?>
                            @foreach($data['kelemahan'] as $key)
                                @if($i < 5 && $key != '')
                                    <li>{!! $key !!}</li>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col s12 m6 l8">
                    <div class="card-panel grey lighten-4 flat">
                        <i class="material-icons md-48">widgets</i>
                        <h6>Karakteristik Pekerjaan</h6>
                        <ul class="browser-default">
                            <?php $i = 0; ?>
                            @foreach($data['karakteristik_pekerjaan'] as $key)
                                @if($i < 9)
                                    <li>{!! $key !!}</li>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section("inline-js")

    {!! Html::script('assets/vendor/jquery/jquery.min.js') !!}
    <script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo asset('assets/js/user/user-page.js'); ?>"></script>
    <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/canvasjs.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/jquery.canvasjs.js') !!}
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css')!!}
    {!! Html::style('plugins/select2/select2.min.css')!!} 
    
  
<script>

$.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });



$(function() {
    $("#most-graph").CanvasJSChart({
        title: {
            text: "MOST"
        },
        axisY: {
            
            includeZero: false,
            minimum: -8,
            maximum: 8,
            interval: 2,
            labelFormatter: function(e){
                if(e.value == 0){
                    return  e.value;
                }else{
                    return '';
                }
            },
            tickLength: 0
        },
        axisX: {
            interval: 1
        },
        data: [
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['most_d'] }}},
                { label: "I",  y: {{ $data['most_i'] }} },
                { label: "S",y: {{ $data['most_s'] }} },
                { label: "C",y: {{ $data['most_c'] }} },
            ]
        }
        ]
    });

    $("#least-graph").CanvasJSChart({
        title: {
            text: "LEAST"
        },
        axisY: {
          
            includeZero: false,
            minimum: -8,
            maximum: 8,
            interval: 2,
            labelFormatter: function(e){
                if(e.value == 0){
                    return  e.value;
                }else{
                    return '';
                }
            },
            tickLength: 0
        },
        axisX: {
            interval: 1
        },
        data: [
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['lest_d'] }}},
                { label: "I",  y: {{ $data['lest_i'] }} },
                { label: "S",y: {{ $data['lest_s'] }}},
                { label: "C",y: {{ $data['lest_c'] }} },
            ]
        }
        ]
    });

    $("#change-graph").CanvasJSChart({
        title: {
            text: "CHANGE"
        },
        axisY: {
          
            includeZero: false,
            minimum: -8,
            maximum: 8,
            interval: 2,
            labelFormatter: function(e){
                if(e.value == 0){
                    return  e.value;
                }else{
                    return '';
                }
            },
            tickLength: 0
        },
        axisX: {
            interval: 1
        },
        data: [
        {
            type: "line", //try changing to column, area
            toolTipContent: "{label}: {y}",
            dataPoints: [
                { label: "D",  y: {{ $data['change_d'] }} },
                { label: "I",  y: {{ $data['change_i'] }} },
                { label: "S",y: {{ $data['change_s'] }}},
                { label: "C",y: {{ $data['change_c'] }} },
            ]
        }
        ]
    });
});

</script>
@stop