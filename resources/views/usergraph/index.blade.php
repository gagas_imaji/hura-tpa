@extends('layouts_user.default')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Laporan</h2>
        </div>
        <div class="active-tool-list report-tool-list">
            <div class="row">
                @if($history -> count() == 0)
                    <h6 class="survey-title alert alert-neutral">Anda belum memiliki riwayat survey.</h6>
                @else
                    <?php $jpm = false;?>
                    @foreach($history as $histories)
                        <div class="col l3 m4 s12"> 
                            <div class="card"> 
                                <div class="card-image">
                                    <a href="/user/report/{{ $histories -> category_survey_id }}/{{$histories -> slug}}" >
                                        <img src="<?php echo asset('assets/image/survey-'.$histories->category_survey_id.'.jpg'); ?>">
                                        <span class="card-title white-text">
                                            <small><i class="material-icons">insert_chart</i> {{date('d-m-Y', strtotime($histories -> updated_at))}}</small>
                                            {{ $histories->title }}
                                        </span>
                                    </a>
                                </div>
                                
                            </div>              
                        </div>    
                    @endforeach
                    <!-- @if($jpm_link)
                        @foreach($jpm_link as $link)
                        <div class="col l3 m4 s12">
                            <div class="card">
                                <div class="card-image">
                                    <a href="/user/report/get-jpm/{{$link['slug']}}">
                                        <img src="<?php echo asset('assets/image/survey-3.jpg'); ?>">
                                        <span class="card-title white-text">
                                            <small><i class="material-icons">multiline_chart</i> {{date('d-m-Y', strtotime($link['updated_at']))}}
                                            </small>
                                            Job Person Match
                                        </span>
                                    </a>
                                </div>
                            </div>              
                        </div>   
                        @endforeach 
                    @endif -->
                @endif
                          
            </div>              
        </div>          
    </div>
</section>
@stop
@section("inline-js")
{!! Html::script('assets/vendor/jquery/jquery.min.js') !!}
<script src="<?php echo asset('assets/vendor/materialize/js/materialize.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/js/user/user-page.js'); ?>"></script>
@stop