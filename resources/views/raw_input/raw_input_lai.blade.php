@extends('layouts.default')

    @section('breadcrumb')
    <div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li><a href="/backend/raw-input">Raw Input Project</a></li>
            <li class="active">{{ $info_project['name'] }} - LAI</li>
        </ol>
    </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger"><i class="fa fa-file-excel-o" aria-hidden="true"></i> {{ $info_project['name'] }} - LAI</div>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-bordered" id="list-raw-tools-lai">
                            <thead>
                              <tr class="headings">
                                <th class="text-center" rowspan="2" width="250px" style="background-color: #36d7ff;">Nama Peserta</th>
                                <th class="text-center nomor" colspan="60" style="background-color: #fff072">Subtest 1</th>
                                <th class="text-center nomor" colspan="50" style="background-color: #fff072">Subtest 2</th>
                                <th class="text-center nomor" colspan="72" style="background-color: #fff072">Subtest 3</th>
                                <th class="text-center nomor" colspan="60" style="background-color: #fff072">Subtest 4</th>
                                <th class="text-center nomor" colspan="60" style="background-color: #fff072">Subtest 5</th>
                              </tr>
                                <tr>
                                    @for($i = 1; $i <= 60; $i++)
                                        <th class="text-center nomor" style="background-color: #fff072">{{ $i }}</th>
                                    @endfor
                                    @for($i = 1; $i <= 50; $i++)
                                        <th class="text-center nomor" style="background-color: #fff072">{{ $i }}</th>
                                    @endfor
                                    @for($i = 1; $i <= 72; $i++)
                                        <th class="text-center nomor" style="background-color: #fff072">{{ $i }}</th>
                                    @endfor
                                    @for($i = 1; $i <= 60; $i++)
                                        <th class="text-center nomor" style="background-color: #fff072">{{ $i }}</th>
                                    @endfor
                                    @for($i = 1; $i <= 60; $i++)
                                        <th class="text-center nomor" style="background-color: #fff072">{{ $i }}</th>
                                    @endfor
                                </tr>
                            </thead>
                            <tbody>
                                @if($rowset!=null)
                                    @foreach($rowset as $key)
                                        <tr class="">
                                            <td style="background-color: #68defc;">{!! $key['nama_peserta'] !!}</td>
                                            @foreach($key['subtest_1'] as $subtest1)
                                                @if($subtest1 != '-')
                                                    <td>{!! join(',', $subtest1) !!}</td>
                                                @else
                                                    <td>-</td>
                                                @endif
                                            @endforeach
                                            @foreach($key['subtest_2'] as $subtest2)
                                                @if($subtest2 != '-')
                                                    <td>{!! join(',', $subtest2) !!}</td>
                                                @else
                                                    <td>-</td>
                                                @endif
                                            @endforeach
                                            @foreach($key['subtest_3'] as $subtest3)
                                                @if($subtest3 != '-')
                                                    <td>{!! join(',', $subtest3) !!}</td>
                                                @else
                                                    <td>-</td>
                                                @endif
                                            @endforeach
                                            @foreach($key['subtest_4'] as $subtest4)
                                                @if($subtest4 != '-')
                                                    <td>{!! join(',', $subtest4) !!}</td>
                                                @else
                                                    <td>-</td>
                                                @endif
                                            @endforeach
                                            @foreach($key['subtest_5'] as $subtest5)
                                                @if($subtest5 != '-')
                                                    <td>{!! join(',', $subtest5) !!}</td>
                                                @else
                                                    <td>-</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
        <script type="text/javascript">

        $(document).ready(function() {
            $('#list-raw-tools-lai').DataTable({
                "fixedColumns": true,
                "scrollX": true,
                "language": {
                    "lengthMenu": "Tampilkan _MENU_ data",
                    "zeroRecords": "Data tidak ditemukan",
                    "info": "Halaman _PAGE_ / _PAGES_",
                    "infoEmpty": "Data tidak tersedia",
                    "infoFiltered": "(difilter dari _MAX_ data)",
                    "search": "Cari",
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    },
                },
                "dom": 'Bfrtip',
                "buttons": [ {
                    extend: "excel",
                    text: "Download .xlsx",
                    filename: "Raw Input - {{ $info_project['name'] }} - {{ $info_tools['title'] }}"
                } ],
                "pageLength": 5,
                "columnDefs": [ {
                    "targets"  : 'nomor',
                    "orderable": false,
                    "order": []
                }]
            });

        });
        </script>
    @stop