@extends('layouts.default')

    @section('breadcrumb')
    <div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li class="active">Raw Input Project</li>
        </ol>
    </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger"><i class="fa fa-copy" aria-hidden="true"></i> Raw Input Project</div>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-bordered table-hover" id="list-raw-tools">
                            <thead>
                              <tr class="headings">
                                <th class="text-center">Nama Project</th>
                                <th class="text-center action">Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if($rowset)
                                    @foreach($rowset as $key)
                                        <tr class="">
                                            <td>{!! $key['name'] !!}</td>
                                            <td class="text-center">
                                                @foreach($key['tools'] as $keys)
                                                    {!! $keys !!}
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendor/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('#list-raw-tools').DataTable({
                    "columnDefs": [ {
                        "targets"  : 'nomor',
                        "orderable": false,
                        "order": []
                    }]
                });

            });
        </script>
    @stop