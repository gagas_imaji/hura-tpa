@extends('layouts.default')

    @section('breadcrumb')
    <div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li><a href="/backend/raw-input">Raw Input Project</a></li>
            <li class="active">{{ $info_project['name'] }} - WPA</li>
        </ol>
    </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger"><i class="fa fa-file-excel-o" aria-hidden="true"></i> {{ $info_project['name'] }} - WPA</div>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-bordered" id="list-raw-tools-tiki">
                            <thead>
                                <tr class="headings">
                                    <th class="text-center" rowspan="2" width="250px" style="background-color: #36d7ff;">Nama Peserta</th>
                                    @for($i = 1; $i <= 24; $i++)
                                        <th class="text-center nomor" colspan="2" style="background-color: #fff072">{{ $i }}</th>
                                    @endfor
                                </tr>
                                <tr>
                                     @for($i = 1; $i <= 24; $i++)
                                        <th class="text-center nomor" style="background-color: #fff072">Most</th>
                                        <th class="text-center text-danger nomor" style="background-color: #fff072">Lest</th>
                                    @endfor
                                </tr>
                            </thead>
                            <tbody>
                                @if($rowset!=null)
                                    @foreach($rowset as $key)
                                        <tr class="">
                                            <td style="background-color: #36d7ff;">{!! $key['nama_peserta'] !!}</td>
                                            @foreach($key['jawaban'] as $jwbn)
                                                <td class="text-center">{!! $jwbn['M'] !!}</td>
                                                <td class="text-center">{!! $jwbn['L'] !!}</td>
                                            @endforeach
                                            
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
        <script type="text/javascript">

        $(document).ready(function() {
            $('#list-raw-tools-tiki').DataTable({
                "fixedColumns": true,
                "scrollX": true,
                "language": {
                    "lengthMenu": "Tampilkan _MENU_ data",
                    "zeroRecords": "Data tidak ditemukan",
                    "info": "Halaman _PAGE_ / _PAGES_",
                    "infoEmpty": "Data tidak tersedia",
                    "infoFiltered": "(difilter dari _MAX_ data)",
                    "search": "Cari",
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    },
                },
                "dom": 'Bfrtip',
                "buttons": [ {
                    extend: "excel",
                    text: "Download .xlsx",
                    filename: "Raw Input - {{ $info_project['name'] }} - {{ $info_tools['title'] }}"
                } ],
                "pageLength": 10,
                "columnDefs": [ {
                    "targets"  : 'nomor',
                    "orderable": false,
                    "order": []
                }]
            });

        });
        </script>
    @stop