@extends('layouts.default')

    @section('breadcrumb')
    <div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li><a href="/backend/raw-input">Raw Input Project</a></li>
            <li class="active">{{ $info_project['name'] }} - WBA</li>
        </ol>
    </div>
    @stop
    @section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-danger"><i class="fa fa-file-excel-o" aria-hidden="true"></i> {{ $info_project['name'] }} - WBA</div>
                    </div>
                    <div class="panel-body full">
                        <table class="table table-bordered" id="list-raw-tools-wba">
                            <thead>
                                <tr class="headings">
                                    <th class="text-center" width="250px" style="background-color: #36d7ff;">Nama Peserta</th>
                                    @for($i = 1; $i <= 90; $i++)
                                        <th class="text-center nomor" style="background-color: #fff072">{{ $i }}</th>
                                    @endfor
                                </tr>
                            </thead>
                            <tbody>
                                @if($rowset!=null)
                                    @foreach($rowset as $key)
                                        <tr class="">
                                            <td style="background-color: #36d7ff;">{!! $key['nama_peserta'] !!}</td>
                                            @foreach($key['jawaban'] as $jwbn)
                                                <td class="text-center">{!! $jwbn !!}</td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
        <script type="text/javascript">

        $(document).ready(function() {
            $('#list-raw-tools-wba').DataTable({
                "fixedColumns": true,
                "scrollX": true,
                "language": {
                    "lengthMenu": "Tampilkan _MENU_ data",
                    "zeroRecords": "Data tidak ditemukan",
                    "info": "Halaman _PAGE_ / _PAGES_",
                    "infoEmpty": "Data tidak tersedia",
                    "infoFiltered": "(difilter dari _MAX_ data)",
                    "search": "Cari",
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    },
                },
                "dom": 'Bfrtip',
                "buttons": [ {
                    extend: "excel",
                    text: "Download .xlsx",
                    filename: "Raw Input - {{ $info_project['name'] }} - {{ $info_tools['title'] }}"
                } ],
                "pageLength": 5,
                "columnDefs": [ {
                    "targets"  : 'nomor',
                    "orderable": false,
                    "order": []
                }]
            });

        });
        </script>
    @stop