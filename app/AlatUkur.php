<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlatUkur extends Model
{
    use SoftDeletes;

    protected $table = "alat_ukur";
    protected $guarded = ['id'];

    public $fillable = ['nama', 'slug'];
}
