<?php namespace App\Talenthub\Provider;
 
use Illuminate\Support\ServiceProvider;
  
class TalenthubServiceProvider extends ServiceProvider {

    public function register()
    {
         // $this->app->bind('App\Trukita\Repository\CustomUserInterface', 'App\Trukita\Repository\Eloquent\CustomUserRepository');
         // $this->app->bind('App\Trukita\Repository\OrderInterface', 'App\Trukita\Repository\Eloquent\OrderRepository');
         // $this->app->bind('App\Trukita\Repository\BidInterface', 'App\Trukita\Repository\Eloquent\BidRepository');
         // $this->app->bind('App\Trukita\Repository\PriceInterface', 'App\Trukita\Repository\Eloquent\PriceRepository');

    	$this->app->bind(
            'App\Talenthub\Repository\CorporateRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\CorporateRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\SurveyRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\SurveyRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\QuestionRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\QuestionRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\OptionAnswerRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\OptionAnswerRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\ResultRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\ResultRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\JPMRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\JPMRepository'
        );

        //testing apache benchmark

        $this->app->bind(
            'App\Talenthub\Repository\TestingCorporateRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\TestingCorporateRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\TestingSurveyRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\TestingSurveyRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\TestingQuestionRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\TestingQuestionRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\TestingOptionAnswerRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\TestingOptionAnswerRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\TestingResultRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\TestingResultRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\TestingJPMRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\TestingJPMRepository'
        );

        $this->app->bind(
            'App\Talenthub\Repository\StandarNilaiRepositoryInterface',
            'App\Talenthub\Repository\Eloquent\StandarNilaiRepository'
        );

    }
}
