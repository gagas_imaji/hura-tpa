<?php

namespace App\Talenthub\Repository;

use App\Question;

interface QuestionRepositoryInterface
{

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */ 
    public function get_question_survey($slug, $section);

    public function save_answer($data);

    public function get_total_question($section);

    public function get_list_question_by_survey($id);

    public function get_detail_question_by_survey($id);
} 
