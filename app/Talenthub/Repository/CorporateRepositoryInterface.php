<?php

namespace App\Talenthub\Repository;

use App\Corporate;

interface CorporateRepositoryInterface
{

    /**
     * @param      $id
     * @param null $type 
     * @return mixed
     */ 
    public function getById($id);

    public function getAll();

    public function getCorporateByUser($id);
} 
