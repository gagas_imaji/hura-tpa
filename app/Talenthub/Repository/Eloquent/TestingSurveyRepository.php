<?php

namespace App\Talenthub\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use DB;
use App\Talenthub\Repository\TestingSurveyRepositoryInterface;
use App\Survey;
use App\UserSurvey;
use App\UserReport;
use App\TempProcessSurvey;
use App\SectionSurvey;
use App\OptionAnswer;
use App\BankSurvey;
use App\BankSection;
use App\BankQuestion;
use App\BankOptionAnswer;
use App\SurveyGrade;
use App\BankWPA;
use App\BankPapikostick;
use App\Project;
use App\Profile;
use App\UserProject;
use App\User;
use App\RoleUser;

class TestingSurveyRepository extends AbstractRepository implements TestingSurveyRepositoryInterface
{


    protected $model;


    public function __construct(Survey $survey, UserSurvey $user_survey, SectionSurvey $section_survey, BankSurvey $bank_survey, BankSection $bank_section, BankQuestion $bank_question, BankOptionAnswer $bank_option, SurveyGrade $survey_grade, BankWPA $bank_wpa, BankPapikostick $bank_papi)
    {
        $this -> survey         = $survey;
        $this -> user_survey    = $user_survey;
        $this -> section_survey = $section_survey;
        $this -> bank_survey    = $bank_survey;
        $this -> bank_section   = $bank_section;
        $this -> bank_question  = $bank_question;
        $this -> bank_option    = $bank_option;
        $this -> survey_grade   = $survey_grade;
        $this -> bank_wpa       = $bank_wpa;
        $this -> bank_papi      = $bank_papi;
    }


    public function get_daftar_survey(){
        $survey = $this -> user_survey -> distinct() -> join('survey', 'survey.id', '=', 'user_survey.id_survey')
                -> where('id_user', '=', 0)
                -> where('is_done', '=', '0')
                -> where('survey.status', '!=', '1')
                -> orderBy('end_date', 'asc')
                -> get();

        return $survey;
    }

    public function get_ongoing_survey(){
        $survey = $this -> survey -> select('survey.id', 'survey.end_date', 'survey.slug', 'survey.title')
                -> where('end_date', '>=', Carbon::now()->toDateString())
                -> where('status', '!=', '1')
                -> where('user_id', '=', 0)
                -> orderBy('end_date', 'asc') -> get();

        return $survey;
    }

    public function get_info_survey_by_slug($slug){
        $description = $this -> survey -> where('slug', '=', $slug) -> first();

        return $description;
    }

    public function survey_complete($slug){
        $id = $this -> survey -> where('slug', '=', $slug) ->get();
        foreach ($id as $key) {
            $id_survey = $key -> id;
        }

        $this -> user_survey -> where('id_survey', '=', $id_survey) -> where('id_user', '=', 0)
        -> update(['is_done' => 1]);
    }

    public function save_temp($data){
        try{

            //save to temp_process_survey
            $temp = new TempProcessSurvey;
            $temp -> survey_id      = $data['survey_id'];
            $temp -> question_id    = $data['question_id'];
            $temp -> user_id        = $data['user_id'];
            $temp -> timer          = $data['timer'];

            $temp -> save();
            return true;
        }catch(exception $e){
            return false;
        }
    }

    public function get_temp($id){
        $temp   = TempProcessSurvey::where('survey_id', '=', $id) -> where('user_id', '=', 0) -> sum('timer');
        return $temp;
    }

    public function delete_temp_survey($slug){
        $id     = $this -> survey -> where('slug', '=', $slug) -> first();
        $id_survey = $id -> id;

        DB::table('temp_process_survey') -> where('survey_id', '=', $id_survey) -> where('user_id', '=', 0)
        -> delete();
    }

    public function getHistory(){
        $survey = $this -> survey -> join('user_survey', 'user_survey.id_survey', '=', 'survey.id')
                -> where('user_survey.is_done', '=', 1)
                -> where('id_user', '=', 0) -> orderBy('user_survey.updated_at', 'desc') -> get();

        return $survey;
    }

    public function save_survey($data){
        DB::beginTransaction();
        try{
            $survey = DB::table('survey')->insertGetId([
                'category_survey_id'   => $data['category'],
                'id_project'           => $data['id_project'],
                // 'id_quarter'           => $data['quarter'],
                'title'                => $data['title'],
                'slug'                 => $data['slug'],
                'description'          => $data['description'],
                'instruction'          => $data['instruction'],
                'thankyou_text'        => $data['thankyou'],
                'start_date'           => $data['started'],
                'end_date'             => $data['ended'],
                'is_random'            => $data['randomized'],
                'timer'                => $data['timer'],
                'user_id'              => 0,
                'created_at'           => Carbon::now()
            ]);

            //add section survey
            $i = 0;
            foreach ($data['section'] as $key) {
                if($key != ''){ //antisipasi jika ada section yang kosong maka tidak dimasukkan ke db
                    if($data['limit'][$i] < 0){
                        $data['limit'][$i] = 0;
                    }

                    //set timer
                    if($data['hours_section'][$i] > 0){
                        $data['hours_section'][$i] = $data['hours_section'][$i]*60*60;
                    }
                    if($data['minutes_section'][$i] > 0){
                        $data['minutes_section'][$i] = $data['minutes_section'][$i]*60;
                    }
                    $timer   = $data['hours_section'][$i] + $data['minutes_section'][$i] + $data['seconds_section'][$i];
                    $section = DB::table('section_survey')->insert([
                        'survey_id'         => $survey,
                        'name'              => $key,
                        'slug'              => strtolower(str_replace(' ', '-', $key)),
                        'limit'             => $data['limit'][$i],
                        'instruction'       => $data['instruction_section'][$i],
                        'timer'             => $timer,
                        'created_at'        => Carbon::now()
                    ]);
                }
                $i++;
            }

            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }


    public function save_wpa_survey($data){
        $data['title']  = "Work Personality Analytics";
        $data['slug']   = strtolower(str_replace(' ', '-', $data['title'].'-'.strtotime('now')));
        $data['description']    = "Test ini bertujuan untuk melihat/mendeskripsikan bagaimana kecenderungan seseorang bertingkah laku.";
        $data['instruction']    = "Tes ini dirancang untuk menguji kepribadian berdasarkan perilaku khas sehari-hari.";
        $data['thankyou']  = "Terima kasih telah menyelesaikan test ini.";

        DB::beginTransaction();
        try{

            $survey = DB::table('survey')->insertGetId([
                'category_survey_id'   => $data['category'],
                // 'id_quarter'           => $data['quarter'],
                'id_project'           => $data['id_project'],
                'title'                => $data['title'],
                'slug'                 => $data['slug'],
                'description'          => $data['description'],
                'instruction'          => $data['instruction'],
                'thankyou_text'        => $data['thankyou'],
                'start_date'           => $data['started'],
                'end_date'             => $data['ended'],
                'is_random'            => $data['randomized'],
                'timer'                => 900, // 15 menit
                'user_id'              => 0,
                'created_at'           => Carbon::now()
            ]);

            //add section survey
            $section = DB::table('section_survey')->insertGetId([
                'survey_id'         => $survey,
                'name'              => $data['title'],
                'slug'              => strtolower(str_replace(' ', '-', $data['title'])),
                'limit'             => 5,
                'instruction'       => "Mulailah dengan membayangkan kondisi yang Anda pilih untuk direspon, contoh di rumah, kantor atau sosial. Pilihlah salah satu statement yang PALING menggambarkan diri Anda di kolom 'Most' dan salah satu statement yang PALING TIDAK menggambarkan diri Anda di kolom 'Lest'. Satu statement yang sama tidak bisa mewakili dua jawaban MOST dan LEST secara bersamaan",
                'created_at'        => Carbon::now()
            ]);

            //ambil data bank_wpa
            $bank_wpa = $this -> bank_wpa -> get();
            foreach ($bank_wpa as $key) {

                $master_wpa = DB::table('master_wpa')->insertGetId([
                    'id_section'    => $section,
                    'number'        => $key['number'],
                    'statement'     => $key['statement'],
                    'M'             => $key['M'],
                    'L'             => $key['L'],
                    'created_at'    => Carbon::now()
                ]);
            }

            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function save_papi_survey($data){
        $data['title']  = "Work Behavioural Assessment";
        $data['slug']   = strtolower(str_replace(' ', '-', $data['title'].'-'.strtotime('now')));
        $data['description']    = "-";
        $data['instruction']    = "Tes kepribadian yang tercermin dalam tingkah laku yang didasarkan pada kategorisasi.";
        $data['thankyou']       = "Terima kasih telah menyelesaikan test ini.";

        DB::beginTransaction();
        try{

            $survey = DB::table('survey')->insertGetId([
                'category_survey_id'   => $data['category'],
                'id_project'           => $data['id_project'],
                // 'id_quarter'           => $data['quarter'],
                'title'                => $data['title'],
                'slug'                 => $data['slug'],
                'description'          => $data['description'],
                'instruction'          => $data['instruction'],
                'thankyou_text'        => $data['thankyou'],
                'start_date'           => $data['started'],
                'end_date'             => $data['ended'],
                'is_random'            => $data['randomized'],
                'timer'                => 0, // 30 menit
                'user_id'              => 0,
                'created_at'           => Carbon::now()
            ]);

            //add section survey
            $section = DB::table('section_survey')->insertGetId([
                'survey_id'         => $survey,
                'name'              => $data['title'],
                'slug'              => strtolower(str_replace(' ', '-', $data['title'])),
                'limit'             => 5,
                'instruction'       => "Mulailah dengan membayangkan kondisi yang Anda pilih untuk direspon dalam lingkungan kerja. Pilihlah salah satu pernyataan yang PALING menggambarkan diri Anda.",
                'created_at'           => Carbon::now()
            ]);

            //ambil data bank_papi
            $bank_papi = $this -> bank_papi -> get();
            foreach ($bank_papi as $key) {

                $master_wpa = DB::table('master_papikostick')->insertGetId([
                    'id_section'    => $section,
                    'number'        => $key['number'],
                    'statement'     => $key['statement'],
                    'key'           => $key['key'],
                    'created_at'    => Carbon::now()
                ]);
            }

            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function save_from_bank_survey($data){
        DB::beginTransaction();
        try{
            //ambil data bank_survey
            $bank_survey = $this -> bank_survey -> where('category_survey_id', '=', $data['category']) -> get();
            foreach ($bank_survey as $bank_survey) {

                //tarik data bank_survey ke tabel survey
                $survey = DB::table('survey')->insertGetId([
                    'category_survey_id'   => $data['category'],
                    'id_project'           => $data['id_project'],
                    // 'id_quarter'           => $data['quarter'],
                    'title'                => $bank_survey['title'],
                    'slug'                 => $bank_survey['slug'].'-'.strtotime('now'),
                    'description'          => $bank_survey['description'],
                    'instruction'          => $bank_survey['instruction'],
                    'thankyou_text'        => $bank_survey['thankyou_text'],
                    'start_date'           => $data['started'],
                    'end_date'             => $data['ended'],
                    'is_random'            => $data['randomized'],
                    'timer'                => $bank_survey['timer'],
                    'user_id'              => 0,
                    'created_at'           => Carbon::now()
                ]);

                //ambil data bank_section
                $bank_section = $this -> bank_section -> where('bank_survey_id', '=', $bank_survey['id']) -> get();
                foreach ($bank_section as $bank_section) {

                    //tarik data bank_section ke tabel section_survey
                    $section = DB::table('section_survey')->insertGetId([
                        'survey_id'    => $survey,
                        'name'         => $bank_section['name'],
                        'slug'         => $bank_section['slug'].'-'.strtotime('now'),
                        'instruction'  => $bank_section['instruction'],
                        'timer'        => $bank_section['timer'],
                        'limit'        => $bank_section['limit'],
                        'created_at'   => Carbon::now()
                    ]);

                    //ambil data bank_question
                    $bank_question = $this -> bank_question -> where('id_bank_section', '=', $bank_section['id']) -> get();
                    foreach ($bank_question as $bank_question) {
                        //tarik data bank_question ke tabel question
                        $question = DB::table('question')->insertGetId([
                            'id_type_question'  => $bank_question['id_type_question'],
                            'id_section'        => $section,
                            'question'          => $bank_question['question'],
                            'is_random'         => $bank_question['is_random'],
                            'timer'             => $bank_question['timer'],
                            'is_mandatory'      => $bank_question['is_mandatory'],
                            'created_at'        => Carbon::now()
                        ]);

                        //ambil data bank_option_answer
                        $bank_option = $this -> bank_option -> where('id_bank_question', '=', $bank_question['id']) -> get();
                        foreach($bank_option as $bank_option){

                            $extension = '';
                            //cek apakah opsi merupakan image
                            if(strlen($bank_option['answer']) > 4){ // 4 (empat) untuk ekstensi
                                for($i = strlen($bank_option['answer'])-4; $i < strlen($bank_option['answer']); $i++){
                                    $extension .= $bank_option['answer'][$i];
                                }
                            }

                            if($extension == '.png' || $extension == '.jpg' || $extension == '.PNG' || $extension == '.JPG'){
                                $image = $bank_option['answer'];
                            }else{
                                $image = '';
                            }

                            //tarik data bank_option ke tabel option_answer
                            $option_answer = DB::table('option_answer')->insertGetId([
                                'id_question'   => $question,
                                'answer'        => $bank_option['answer'],
                                'image'         => $image,
                                'point'         => $bank_option['point'],
                                'hidden'        => 0,
                                'created_at'    => Carbon::now()
                            ]);
                        }
                    }
                }
            }

            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function get_first_section_on_survey($id){
        $section = $this -> section_survey -> select('id', 'survey_id')->where('survey_id', '=', $id)
                ->whereNotIn('id', DB::table('survey_report')->select('section_id')->where('user_id', '=', 0)->where('survey_id', '=', $id))
                ->first();
        return $section;
    }

    public function get_next_section($slug, $section){
        $id = DB::table('survey') -> where('slug', '=', $slug) -> first();
        $next_section = $this -> section_survey -> select('section_survey.id', 'section_survey.instruction') ->join('survey', 'survey.id', '=', 'section_survey.survey_id') -> where('survey.slug', '=', $slug) -> where('section_survey.id', '>', $section) -> whereNotIn('section_survey.id', DB::table('survey_report')->select('section_id')->where('user_id', '=', 0)->where('survey_id', '=', $id->id)) -> limit(1)->get();
        return $next_section;
    }

    public function get_count_answered($section){
        $count  = OptionAnswer::select('*') -> join('result', 'result.id_answer', 'option_answer.id')
                -> join('question', 'question.id', '=', 'option_answer.id_question')
                -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                -> where('question.id_section', '=', $section)
                -> where('result.user_id', '=', 0) -> get();

        return $count;
    }

    public function get_survey_by_id($id){
        $survey     = $this -> survey -> where('id', '=', $id)->first();

        return $survey;
    }

    public function get_list_survey_data_table(){
        $survey     = Survey::select('survey.id as id', 'survey.title', 'survey_category.name', 'survey.start_date', 'survey.end_date', 'survey.status', 'survey.created_at')
            -> join('survey_category', 'survey_category.id', '=', 'survey.category_survey_id')
            -> where('survey.user_id', '=', 0) -> orderBy('survey.created_at', 'desc') -> get();
        return $survey;
    }

    public function check_quarter_survey($category, $quarter){

        $year       = [ Carbon::now()->startOfYear()->toDateTimeString(), Carbon::now()->endOfYear()->toDateTimeString() ];
        $survey     = $this -> survey -> where('category_survey_id', '=', $category)
                    -> where('id_quarter', '=', $quarter)
                    -> where('user_id', '=', 0)
                    -> whereBetween('end_date', $year) //fungsi untuk kondisi data yang ditampilkan berdasarkan end_date yang dibuat persaat ini sampai setahun ke belakang.
                    -> get();

        return $survey;
    }

    public function get_peserta_survey($slug){
        $user = $this -> user_survey -> distinct() -> select('profiles.first_name', 'division.name as divisi', 'user_survey.is_done', 'survey.title')
        -> join('survey', 'survey.id', '=', 'user_survey.id_survey')
        -> join('profiles', 'profiles.user_id', '=', 'user_survey.id_user')
        -> join('division', 'division.id', '=', 'profiles.id_division')
        -> where('survey.slug', '=', $slug) -> orderBy('profiles.id') -> paginate(10);
        return $user;
    }

    public function get_id_survey_by_slug($slug){
        $id = $this -> survey -> select('id') -> where('slug', '=', $slug) -> first();
        return $id['id'];
    }

    //untuk helper report_survey
    public function get_survey_category(){

    }

    public function get_survey_quarter(){
        $data = DB::table('survey_quarter') -> get();
        $i = 0;
        foreach ($data as $key) {
            $quarter[$i]['id'] = $key ->id;
            $quarter[$i]['name'] = $key ->name;
            $i++;
        }
        return $quarter;
    }

    public function get_last_history_survey(){
        $history_survey = $this -> survey -> where('status', '=', 1) -> where('user_id', '=', 0)
            -> orderBy('end_date', 'desc') -> limit(1) -> get();

        return $history_survey;
    }

    public function get_all_history_survey(){
        $history_survey = $this -> survey -> whereIn('id', DB::table('survey_report')->select('survey_id')->join('survey', 'survey.id', '=', 'survey_report.survey_id')->where('survey.user_id', '=', 0))
                        -> orWhereIn('id', DB::table('report_wpa')->select('survey_id')->join('survey', 'survey.id', '=', 'report_wpa.survey_id')->where('survey.user_id', '=', 0))
                        -> orWhereIn('id', DB::table('report_papikostick')->select('survey_id')->join('survey', 'survey.id', '=', 'report_papikostick.survey_id')->where('survey.user_id', '=', 0))
                        -> where('user_id', '=', 0)
                        -> orderBy('updated_at', 'desc') -> get();

        return $history_survey;
    }

    public function get_quarter_of_survey($id){
        $data = $this -> survey -> select('survey_quarter.name')
                -> join('survey_quarter', 'survey_quarter.id', '=', 'survey.id_quarter')
                -> where('survey.id', '=', $id) -> get();

        foreach ($data as $key) {
            $quarter = $key['name'];
        }
        return $quarter;
    }

    public function get_info_section($id){
        $section = $this -> section_survey -> whereId($id) -> first();

        return $section;
    }

    public function insert_paragraph($paragraph, $section){
        try{
            $option_answer = DB::table('question_paragraph')->insertGetId([
                'section_id'    => $section,
                'paragraph'     => $paragraph
            ]);
            return true;
        }catch(exception $e){
            return false;
        }
    }

    public function check_grade_survey($id){
        $data = $this -> survey_grade -> where('id_survey', '=', $id) -> get();
        if($data -> count() == 0){
            return false;
        }else{
            return true;
        }
    }

    public function get_grade($score, $id_survey){
        $data = $this -> survey_grade -> where('min', '<', $score)
                -> where('max', '>', $score)
                -> where('id_survey', '=', $id_survey)
                -> get();

        foreach ($data as $key) {
            $grade = $key['grade'];
        }

        return $grade;
    }

    public function update_grade_user($id_survey, $grade, $score_final){
        $this -> user_survey -> where('id_user', '=', 0)
                -> where('id_survey', '=', $id_survey)
                -> update(['grade' => $grade, 'score_final' => $score_final]);
    }

    public function get_all_section_on_survey($id){
        $section = $this -> section_survey -> where('survey_id', '=', $id) -> get();

        return $section;
    }

    public function update_section_info_on_survey($id, $name, $limit, $section_instruction, $section_timer){
        for($i = 0; $i < count($id); $i++){
            if($limit[$i] == null || $limit[$i] < 0){
                $limit[$i] = 0;
            }
            $this -> section_survey -> where('id', '=', $id[$i]) -> update(['name' => $name[$i], 'limit' => $limit[$i], 'instruction' => $section_instruction[$i], 'timer' => $section_timer[$i]]);
        }
    }

    public function get_unfinished_section($id_survey){
        $section = $this -> section_survey -> select('section_survey.id')
                    -> whereNotIn('section_survey.id', DB::table('survey_report') -> select('survey_report.section_id') -> where('survey_report.survey_id', '=', $id_survey) -> where('survey_report.user_id', '=', 0))
                    -> where('section_survey.survey_id', '=', $id_survey)
                    -> get();

        return $section;

    }

    public function get_participant_done(){
        $data = DB::table('user_project') -> select(DB::raw('count(user_project.id_user) as count')) -> join('project', 'project.id', '=', 'user_project.id_project') -> where('user_project.is_done', '=', 1) -> where('project.user_id', '=', 0)-> first();

        $participant = $data -> count;

        return $participant;
    }

    public function get_all_participant(){
        $data = DB::table('user_project') -> select(DB::raw('count(user_project.id_user) as count')) -> join('project', 'project.id', '=', 'user_project.id_project') -> where('project.user_id', '=', 0)-> first();
        $participant = $data -> count;

        return $participant;
    }

    //============================================ TAMBAHAN HURA VERSI 1,5 ============================================//

    public function save_project($data){
        DB::beginTransaction();
        try{
            $project = new Project;
            $project -> name            = $data['name'];
            $project -> user_id         = $data['user_id'];
            $project -> slug            = $data['slug'];
            $project -> start_date      = $data['started'];
            $project -> end_date        = $data['ended'];
            $project -> description     = $data['description'];
            $project -> save();

            DB::commit();
            return $project->id;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function get_all_project(){
        $project = Project::latest() -> where('user_id', '=', 0) -> paginate(10);
        return $project;
    }

    public function get_info_project_by_id($id){
        $info = Project::where('id', '=', $id) -> first();

        return $info;
    }

    public function get_peserta_available($id){
        $id_corporate = DB::table('profiles') -> where('user_id', '=', 0) -> first() -> id_corporate;

        $peserta = Profile::select('profiles.*')
            -> join('users', 'users.id', '=', 'profiles.user_id')
            -> join('role_user', 'role_user.user_id', '=', 'users.id')
            -> where('role_user.role_id', '=', 2)
            -> where('profiles.id_corporate', '=', $id_corporate)
            -> whereNotIn('users.id', DB::table('user_project') -> select('user_project.id_user') -> where('id_project', '=', $id))
            -> orderBy('users.id') -> get();
        return $peserta;
    }

    public function get_list_survey_by_project($id){
        $survey = $this -> survey -> where('id_project', '=', $id) -> get();
        return $survey;
    }

     public function assign_project($data, $id){

        DB::beginTransaction();
        try{

            $assign_project = new UserProject;
            $assign_project -> id_user = $id;
            $assign_project -> id_project = $data['id'];
            $assign_project -> is_done    = 0;
            $assign_project -> save();

            foreach ($data['survey'] as $key) {
                if($id != ""){
                    $assign_survey = new UserSurvey;
                    $assign_survey -> id_project = $data['id'];
                    $assign_survey -> id_user = $id;
                    $assign_survey -> id_survey  = $key;
                    $assign_survey -> save();
                }
            }

            $check_invite_report = DB::table('user_report') -> select(DB::raw('count(user_report.user_id) as count')) -> where('user_id', '=', $id) -> first();

            if($check_invite_report -> count == 0){
                $user_survey = new UserReport;
                $user_survey -> user_id        = $id;
                $user_survey -> save();
            }

            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function get_detail_project($id_project, $where = []){ //untuk master
        $project = Project::select('project.*', 'survey.id as id_tools', 'survey.title as title', 'survey.id as id_survey', 'survey.slug as slug_survey', 'survey.instruction as instruction_survey', 'survey.status as status_survey', 'survey.is_random')
                    -> join('survey', 'survey.id_project', '=', 'project.id')
                    -> where('project.id', '=', $id_project) -> get();
        return $project;
    }

    public function get_peserta_project($id){ //list peserta yang di assign ke atu project

        $peserta = UserProject::select('profiles.first_name', 'profiles.user_id as id_user', 'user_project.is_done')
                -> join('profiles', 'user_project.id_user', '=', 'profiles.id')
                -> where('user_project.id_project', '=', $id) -> orderBy('profiles.user_id') -> paginate(10);

        return $peserta;
    }

    public function get_info_project_by_slug($slug){
        $info = Project::where('slug', '=', $slug) -> first();

        return $info;
    }

    public function edit_project($data){
        DB::beginTransaction();
        try{

            $old_project = Project::findOrFail($data['id']);
            if($old_project){
                $old_project -> name            = $data['name'];
                $old_project -> user_id         = $data['user_id'];
                $old_project -> slug            = $data['slug'];
                $old_project -> start_date      = $data['start_date'];
                $old_project -> end_date        = $data['end_date'];
                $old_project -> description     = $data['description'];
                $old_project -> save();

                $old_survey = $this -> survey -> where('id_project', '=', $data['id']) -> get();
                foreach($old_survey as $key){
                    $old_survey = $this -> survey ->  findOrFail($key['id']);
                    $old_survey -> start_date   = $data['start_date'];
                    $old_survey -> end_date     = $data['end_date'];
                    $old_survey -> save();
                }
                DB::commit();
                return $old_survey;

            }else{
                throw new \Exception(FALSE);
            }


        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function is_registered($email){
        $user = User::where('email', '=', $email)-> first();

        if($user == null){
            return false;
        }else{
            return $user->id;
        }
    }

    public function insert_new_user($email,$corporate = ''){
        DB::beginTransaction();
        try{

            $first_name     = explode("@", $email);
            if($corporate == "")
            {
                $info_user      = Profile::where('user_id', '=', 0)->first();
                $corporate_id = $info_user->id_corporate;
                $division_id = $info_user->id_division;
            }else{
              $corporate_id =$corporate;
              $division_id = $corporate;
            }


            $user           = new User;
            $user->email    = $email;
            $user->username = (config('config.login')) ? null : $user->email;
            $user->password = bcrypt('654321');
            $user->status   = 'active';
            $user->save();

            $profile = new Profile;
            $profile->user()->associate($user);
            $profile->first_name    = $first_name[0];
            $profile->id_corporate  = $corporate_id;
            $profile->id_division   = $division_id;
            $profile->save();

            $role   = new RoleUser;
            $role->role_id = 2;
            $role->user_id = $user->id;
            $role->save();


            DB::commit();
            return $user;

        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function is_assigned($data, $id){
        $is_assigned = UserProject::where('id_project', '=', $data['id']) -> where('id_user', '=', $id)->first();

        if($is_assigned){
            return true;
        }else{
            return false;
        }
    }

    public function hapus_project($id){
        DB::beginTransaction();
        try{
            $project = Project::findOrFail($id);
            if($project){
                $user_project   = UserProject::where('id_project', '=', $id) -> get();
                $survey         = Survey::where('id_project', '=', $id) -> get();

                //hapus user survey dan survey terkait
                if($survey){
                    foreach ($survey as $key) {
                        $user_survey = UserSurvey::where('id_survey', '=', $key['id'])->get();
                        if($user_survey){
                            foreach ($user_survey as $keys) {
                                UserSurvey::where('id', '=', $keys['id'])->delete();
                            }
                        }
                        Survey::where('id', '=', $key['id'])->delete();
                    }
                }

                //hapus user_project terkait
                if($user_project){
                    foreach ($user_project as $key) {
                        UserProject::where('id', '=', $key['id'])->delete();
                    }
                }

                //hapus project terkait
                $project -> delete();
                DB::commit();
                return true;
            }else{
                throw new \Exception(FALSE);
            }
        }catch (\Exception $e){
            DB::rollback();
            // something went wrong
            return $e;
        }
    }

    public function get_project_wajib($id){
        $project_wajib = UserProject::select('user_project.id', 'project.id as id_project', 'project.name', 'project.slug', 'project.start_date', 'project.end_date', 'project.description', 'project.is_done') -> join('project', 'project.id', '=', 'user_project.id_project')
                -> where('user_project.id_user', '=', $id)
                -> where('user_project.is_done', '=', 0)
                // -> where('project.status', '=', 1)
                -> where('project.is_done', '=', 2)
                -> orderBy('end_date', 'asc')
                -> get();

        return $project_wajib;
    }

    public function get_survey_user_by_project($id_project, $id_peserta){
        $survey = $this -> user_survey -> where('id_project', '=', $id_project) -> where('id_user', '=', $id_peserta) -> get();

        return $survey;
    }

    public function get_survey_user_done_by_project($id_project, $id_peserta){
        $done = $this -> user_survey -> where('id_project', '=', $id_project) -> where('id_user', '=', $id_peserta) -> where('is_done', '=', 1) -> get();

        return $done;
    }

    public function get_detail_project_wajib($id_project, $id_peserta){ //untuk user
        $project = Project::select('project.*') -> where('project.id', '=', $id_project) -> first();
        return $project;
    }

    public function get_survey_wajib_by_project($id_project, $id_peserta){ //untuk user
        $project = $this -> user_survey -> select('survey.id', 'survey.title', 'survey.slug', 'survey.category_survey_id', 'survey.instruction', 'survey.status', 'survey.is_random', 'survey.end_date')
                    -> join('survey', 'survey.id', '=', 'user_survey.id_survey')
                    -> join('project', 'user_survey.id_project', '=', 'project.id')
                    -> where('user_survey.id_user', '=', $id_peserta)
                    -> where('user_survey.is_done', '=', 0)
                    -> where('user_survey.id_project', '=', $id_project) -> get();
        return $project;
    }

    public function get_all_history_project($id_user){
        $history_project = Project::select('project.*')
                -> join('survey', 'survey.id_project', '=', 'project.id')
                -> whereIn('survey.id', DB::table('survey_report')->select('survey_id')->join('survey', 'survey.id', '=', 'survey_report.survey_id')->where('project.user_id', '=', $id_user))
                -> orWhereIn('survey.id', DB::table('report_wpa')->select('survey_id')->join('survey', 'survey.id', '=', 'report_wpa.survey_id')->where('project.user_id', '=', $id_user))
                -> orWhereIn('survey.id', DB::table('report_papikostick')->select('survey_id')->join('survey', 'survey.id', '=', 'report_papikostick.survey_id')->where('project.user_id', '=', $id_user))
                -> where('project.user_id', '=', $id_user)
                -> orderBy('project.end_date', 'desc') -> groupBy('project.id', 'project.name', 'project.slug', 'project.description', 'project.start_date', 'project.end_date', 'project.status', 'project.is_done', 'project.user_id', 'project.created_at', 'project.updated_at') -> paginate(15);

        return $history_project;
    }

    public function get_all_history_survey_by_project($id_project, $id_user){
        $history_project = $this -> survey -> select('survey.*')
                -> join('project', 'survey.id_project', '=', 'project.id')
                -> whereIn('survey.id', DB::table('survey_report')->select('survey_id')->join('survey', 'survey.id', '=', 'survey_report.survey_id')->where('project.user_id', '=', $id_user)->where('survey.id_project', '=', $id_project))
                -> orWhereIn('survey.id', DB::table('report_wpa')->select('survey_id')->join('survey', 'survey.id', '=', 'report_wpa.survey_id')->where('project.user_id', '=', $id_user)->where('survey.id_project', '=', $id_project))
                -> orWhereIn('survey.id', DB::table('report_papikostick')->select('survey_id')->join('survey', 'survey.id', '=', 'report_papikostick.survey_id')->where('project.user_id', '=', $id_user)->where('survey.id_project', '=', $id_project))
                -> where('project.user_id', '=', $id_user)
                -> where('project.id', '=', $id_project)
                -> orderBy('project.end_date', 'desc') -> get();

        return $history_project;
    }

    public function set_kuota_terpakai($kuota){
        DB::table('kuota_survey')->insert([
            'user_id'   => 0,
            'jlh_survey'=> $kuota['survey'],
            'jlh_user'  => $kuota['user'],
            'total'     => $kuota['terpakai']
        ]);

        return true;
    }

    //============================================ TESTING APACHE BENCHMARK ============================================//

    public function get_slug_section($id){
        $data = $this -> survey -> select('survey.*', 'section_survey.id as id_section') -> join('section_survey', 'section_survey.survey_id', '=', 'survey.id') -> where('survey.user_id', '=', 0) -> where('survey.category_survey_id', '=', $id) -> first();
        return $data;
    }

    public function get_section_survey($id){
        $data = $this -> section_survey -> where('survey_id', '=', $id) -> get();

        return $data;
    }

    // ========================================== Tambahan Anggoro TPA
}
