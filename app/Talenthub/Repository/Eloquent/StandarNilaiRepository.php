<?php

namespace App\Talenthub\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use DB;
use App\Talenthub\Repository\StandarNilaiRepositoryInterface;
use App\Survey;
use App\UserSurvey;
use App\UserReport;
use App\TempProcessSurvey;
use App\SectionSurvey;
use App\OptionAnswer;
use App\BankSurvey;
use App\BankSection;
use App\BankQuestion;
use App\BankOptionAnswer;
use App\SurveyGrade;
use App\BankWPA;
use App\BankPapikostick;
use App\Project;
use App\Profile;
use App\UserProject;
use App\User;
use App\RoleUser;
use App\StandarNilaiProject;
use App\StandarNilai;
use App\AspekPsikologis;
use Illuminate\Support\Str;

class StandarNilaiRepository extends AbstractRepository implements StandarNilaiRepositoryInterface
{

    protected $model;

    public function __construct(Survey $survey, UserSurvey $user_survey, SectionSurvey $section_survey, BankSurvey $bank_survey, BankSection $bank_section, BankQuestion $bank_question, BankOptionAnswer $bank_option, SurveyGrade $survey_grade, BankWPA $bank_wpa, BankPapikostick $bank_papi, StandarNilaiProject $standar_nilai_project, StandarNilai $standar_nilai, AspekPsikologis $aspek_psikologis)
    {
        $this->survey           = $survey;
        $this->user_survey      = $user_survey;
        $this->section_survey   = $section_survey;
        $this->bank_survey      = $bank_survey;
        $this->bank_section     = $bank_section;
        $this->bank_question    = $bank_question;
        $this->bank_option      = $bank_option;
        $this->survey_grade     = $survey_grade;
        $this->bank_wpa         = $bank_wpa;
        $this->bank_papi        = $bank_papi;
        $this->standar_nilai    = $standar_nilai;
        $this->aspek_psikologis = $aspek_psikologis;
        $this->standar_nilai_project = $standar_nilai_project;
    }

    public function save_temp($data)
    {
        DB::beginTransaction();
        try{
            $listAspek = $this->aspek_psikologis->get();
            $i=1;
            foreach ($listAspek as $key => $value) {
                $aspek_id[$i] = $value->id;
                $i++;
            }

            $i=1;
            foreach ($listAspek as $key => $value) {
                $nilai_aspek[$i] = $data['nilai_aspek_'.$value->id];
                $i++;
            }

            $i=1;
            foreach ($listAspek as $key => $value) {
                $mandatory[$i] = $data['mandatory_aspek_'.$value->id];
                $i++;
            }

            $json_aspek_id    = json_encode($aspek_id);
            $json_nilai_aspek = json_encode($nilai_aspek);
            $json_mandatory   = json_encode($mandatory);

            //Check Standar Nilai untuk posisi yang dipilih
            $posisi = $data['posisi'];
            $checkStandarNilaiProject = StandarNilai::where('id',$posisi)->get();

            if (sizeof($checkStandarNilaiProject)>0) {
                $standarNilaiProject                    = new StandarNilaiProject;
                $standarNilaiProject->project_id        = $data['project_id'];
                $standarNilaiProject->standar_nilai_id  = $posisi;
                $standarNilaiProject->save();
            } else {
                $standar                    = new StandarNilai;
                $standar->nama_jabatan      = $data['posisi'];
                $standar->slug_nama_jabatan = Str::slug($data['posisi']);
                $standar->aspek_id          = $json_aspek_id;
                $standar->nilai_aspek       = $json_nilai_aspek;
                $standar->mandatory         = $json_mandatory;
                $standar->save();

                $standarNilaiProject = StandarNilaiProject::updateOrCreate(
                        ['project_id'=>$data['project_id']],
                        ['project_id'=>$data['project_id'],'standar_nilai_id'=>$standar->id]
                    );
            }

            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollBack();
            return false;
        }
    }

    public function get_list_standar_nilai_by_project($id)
    {
        $listStandarNilai = $this->standar_nilai_project->with('tpaProject','tpaStandarNilai')->where('project_id',$id)->first();
        if (sizeof($listStandarNilai)>0) {
            return $listStandarNilai;
        } else {
            return false;
        }
    }

    public function update_save_temp($data)
    {
        DB::beginTransaction();
        try{
            $listAspek = $this->aspek_psikologis->get();
            $i=1;
            foreach ($listAspek as $key => $value) {
                $aspek_id[$i] = $value->id;
                $i++;
            }

            $i=1;
            foreach ($listAspek as $key => $value) {
                $nilai_aspek[$i] = $data['nilai_aspek_'.$value->id];
                $i++;
            }

            $i=1;
            foreach ($listAspek as $key => $value) {
                $mandatory[$i] = $data['mandatory_aspek_'.$value->id];
                $i++;
            }

            $json_aspek_id    = json_encode($aspek_id);
            $json_nilai_aspek = json_encode($nilai_aspek);
            $json_mandatory   = json_encode($mandatory);

            //Check Standar Nilai untuk posisi yang dipilih
            $posisi = $data['posisi'];
            $standar = StandarNilai::where('id',$posisi)->first();
            if (sizeof($standar)>0) {
                $standar->nama_jabatan      = $standar->nama_jabatan;
                $standar->slug_nama_jabatan = Str::slug($standar->nama_jabatan);
                $standar->aspek_id          = $json_aspek_id;
                $standar->nilai_aspek       = $json_nilai_aspek;
                $standar->mandatory         = $json_mandatory;
                $standar->save();

                $standarNilaiProject = StandarNilaiProject::updateOrCreate(
                        ['project_id'=>$data['project_id']],
                        ['project_id'=>$data['project_id'],'standar_nilai_id'=>$standar->id]
                    );
            } else {
                $standar                    = new StandarNilai;
                $standar->nama_jabatan      = $data['posisi'];
                $standar->slug_nama_jabatan = Str::slug($data['posisi']);
                $standar->aspek_id          = $json_aspek_id;
                $standar->nilai_aspek       = $json_nilai_aspek;
                $standar->mandatory         = $json_mandatory;
                $standar->save();

                $standarNilaiProject = StandarNilaiProject::updateOrCreate(
                    ['project_id'=>$data['project_id']],
                    ['project_id'=>$data['project_id'],'standar_nilai_id'=>$standar->id]
                    );
            }
            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollBack();
            return false;
        }
    }

    public function update_save_standar($data)
    {
        DB::beginTransaction();
        try{
            $listAspek = $this->aspek_psikologis->get();
            $i=1;
            foreach ($listAspek as $key => $value) {
                $aspek_id[$i] = $value->id;
                $i++;
            }

            $i=1;
            foreach ($listAspek as $key => $value) {
                $nilai_aspek[$i] = $data['nilai_aspek_'.$value->id];
                $i++;
            }

            $i=1;
            foreach ($listAspek as $key => $value) {
                $mandatory[$i] = $data['mandatory_aspek_'.$value->id];
                $i++;
            }

            $json_aspek_id    = json_encode($aspek_id);
            $json_nilai_aspek = json_encode($nilai_aspek);
            $json_mandatory   = json_encode($mandatory);

            //Check Standar Nilai untuk posisi yang dipilih
            $posisi = $data['posisi'];
            $standar = StandarNilai::where('id',$posisi)->first();
            if (sizeof($standar)>0) {
                $standar->nama_jabatan      = $data['name'];
                $standar->slug_nama_jabatan = Str::slug($data['name']);
                $standar->aspek_id          = $json_aspek_id;
                $standar->nilai_aspek       = $json_nilai_aspek;
                $standar->mandatory         = $json_mandatory;
                $standar->save();
            }
            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollBack();
            return false;
        }
    }

    public function create_standar_nilai($data)
    {
        DB::beginTransaction();
        try{
            $listAspek = $this->aspek_psikologis->get();
            $i=1;
            foreach ($listAspek as $key => $value) {
                $aspek_id[$i] = $value->id;
                $i++;
            }

            $i=1;
            foreach ($listAspek as $key => $value) {
                $nilai_aspek[$i] = $data['nilai_aspek_'.$value->id];
                $i++;
            }

            $i=1;
            foreach ($listAspek as $key => $value) {
                $mandatory[$i] = $data['mandatory_aspek_'.$value->id];
                $i++;
            }

            $json_aspek_id    = json_encode($aspek_id);
            $json_nilai_aspek = json_encode($nilai_aspek);
            $json_mandatory   = json_encode($mandatory);
            
            $standar                    = new StandarNilai;
            $standar->nama_jabatan      = $data['name'];
            $standar->slug_nama_jabatan = Str::slug($data['name']);
            $standar->aspek_id          = $json_aspek_id;
            $standar->nilai_aspek       = $json_nilai_aspek;
            $standar->mandatory         = $json_mandatory;
            $standar->save();

            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollBack();
            return false;
        }
    }

}
