<?php

namespace App\Talenthub\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use DB;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Result;
use App\SurveyReport;
use App\ResultWPA;
use App\ReportWPA;
use App\ResultPapi;
use App\ReportPapi;
use App\RawInputLAITIKI;

class ResultRepository extends AbstractRepository implements ResultRepositoryInterface
{


    protected $model;

    public function __construct(Result $result, SurveyReport $report, ResultWPA $result_wpa, ReportWPA $report_wpa, ResultPapi $result_papi, ReportPapi $report_papi, RawInputLAITIKI $raw_input)
    {
        $this -> result     = $result;
        $this -> report     = $report;
        $this -> result_wpa = $result_wpa;
        $this -> report_wpa = $report_wpa;
        $this -> result_papi= $result_papi;
        $this -> report_papi= $report_papi;
        $this -> raw_input = $raw_input;
    }

    public function get_point_survey_gti($survey_id, $section_id){
        $data = $this -> result -> select(DB::raw('sum(result.point) as sum, count(result.point) as count'), 'survey.category_survey_id as category_id')
                -> join('option_answer', 'option_answer.id', '=', 'result.id_answer')
                -> join('question', 'question.id', '=', 'option_answer.id_question')
                -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                -> join('survey', 'survey.id', '=', 'section_survey.survey_id')
                -> where('survey.id', '=', $survey_id)
                -> where('section_survey.id', '=', $section_id)
                -> where('result.user_id', '=', Auth::user() -> id)
                -> groupBy('survey.category_survey_id')
                -> get();
        if($data -> count() != 0){
            foreach($data as $key){
                $point['sum']   = $key['sum'];
                $point['count'] = $key['count'];
            }
        }else{ //jika user tidak menjawab sama sekali
            $point = 0;
        }
        return $point;
    }

    public function get_point_survey_tiki($survey_id, $section_id, $section_name){
        $point = 0;
        if($section_name != 'Berhitung Angka'){ //untuk tipe checkbox, jawaban dua
            $number = $this -> result -> select(DB::raw('count(result.id_question) as count'), 'result.id_question')
                    -> join('option_answer', 'option_answer.id', '=', 'result.id_answer')
                    -> join('question', 'question.id', '=', 'option_answer.id_question')
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id')
                    -> where('survey.id', '=', $survey_id)
                    -> where('section_survey.id', '=', $section_id)
                    -> where('result.user_id', '=', Auth::user() -> id)
                    -> groupBy('result.id_question')
                    -> get();
            foreach($number as $key){
                if($key -> count  == 2){
                    $score = $this -> result -> select('survey.category_survey_id as category_id', 'result.point', 'result.id_question')
                        -> join('option_answer', 'option_answer.id', '=', 'result.id_answer')
                        -> join('question', 'question.id', '=', 'option_answer.id_question')
                        -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                        -> join('survey', 'survey.id', '=', 'section_survey.survey_id')
                        -> where('survey.id', '=', $survey_id)
                        -> where('section_survey.id', '=', $section_id)
                        -> where('result.user_id', '=', Auth::user() -> id)
                        -> where('result.id_question', '=', $key -> id_question)
                        -> get();
                    $i = 0;
                    foreach ($score as $keys) {
                        $temp_score[$i] = $keys -> point;
                        // echo $keys -> id_question."-".$keys -> point."<br>";
                        $i++;
                    }
                    if($temp_score[0] == 1 && $temp_score[1] == 1){
                        $point++;
                    }
                }
            }

        }else{
            $data  = $this -> result -> select(DB::raw('sum(result.point) as sum, count(result.point) as count'), 'survey.category_survey_id as category_id')
                    -> join('option_answer', 'option_answer.id', '=', 'result.id_answer')
                    -> join('question', 'question.id', '=', 'option_answer.id_question')
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id')
                    -> where('survey.id', '=', $survey_id)
                    -> where('section_survey.id', '=', $section_id)
                    -> where('result.user_id', '=', Auth::user() -> id)
                    -> groupBy('survey.category_survey_id')
                    -> get();
                foreach($data as $key){
                    $point = $key -> sum;
                }

        }

        return $point;
    }

    public function get_gtq($table, $adj){
        $gtq = DB::table($table) -> where('adj', '=', $adj) -> first();
        return $gtq;
    }

    public function get_gti_job_preferences($match){
        $jobs = DB::table('lookup_gti_job_preferences') -> where('match', '=', $match) -> get();
        return $jobs;
    } 

    public function get_report_gti_tiki($id, $user_id){
        $data = $this -> report 
                -> join('section_survey', 'section_survey.id', '=', 'survey_report.section_id')
                -> where('survey_report.survey_id', '=', $id) 
                -> where('user_id', '=', $user_id) -> orderBy('section_id') -> get();
        $i = 0;
        if($data -> count() != 0){
            foreach ($data as $key) {

                //pengecekan section, LAI dibalik section ny kebawah. TIKI sesuai berurutan dari awal - akhir
                if($key['name'] == 'Subtest 1'){ //LAI
                    $report['subtest5'] = $key -> score;
                }elseif($key['name'] == 'Subtest 2'){
                    $report['subtest4'] = $key -> score;
                }elseif($key['name'] == 'Subtest 3'){
                    $report['subtest3'] = $key -> score;
                }elseif($key['name'] == 'Subtest 4'){
                    $report['subtest2'] = $key -> score;
                }elseif($key['name'] == 'Subtest 5'){
                    $report['subtest1'] = $key -> score;
                }elseif($key['name'] == 'Berhitung Angka'){ //TIKI
                    $report['subtest1'] = $key -> score;
                }elseif($key['name'] == 'Gabungan Bagian'){
                    $report['subtest2'] = $key -> score;
                }elseif($key['name'] == 'Hubungan Kata'){
                    $report['subtest3'] = $key -> score;
                }elseif($key['name'] == 'Abstraksi Non Verbal'){
                    $report['subtest4'] = $key -> score;
                }
                $i++;
            }
        }else{
            $report = null;
        }
        return $report;
    }

    public function get_criteria_gti_report($avg){
        $data = DB::table('lookup_gti_criteria_report')-> where('min', '<=', $avg)
                -> where('max', '>=', $avg)
                -> first();

        $criteria = $data -> criteria;

        return $criteria;
    }

    public function save_report($score){
        //cek apakah sudah tersimpan di laporan
        $is_saved = $this -> report -> where([
            'survey_id' => $score['survey_id'],
            'user_id'   => $score['user_id'],
            'section_id'=> $score['section_id'],
            'score'     => $score['score']
        ]) -> first();

        //jika belum ada maka simpan
        if(!$is_saved){
            $save = DB::table('survey_report')->insert([
                'survey_id'    => $score['survey_id'],
                'user_id'      => $score['user_id'],
                'section_id'   => $score['section_id'],
                'score'        => $score['score']
            ]);
        }

    }

    public function update_user_report($update, $id){
        DB::table('user_report') -> where('user_id', '=', Auth::user() -> id) -> where('tujuan', '=', $id) -> update($update);
    }

    public function get_report_survey_by_divisi($survey, $divisi){
        $report = $this -> report -> select(DB::raw('sum(survey_report.score) as score'))
            -> join('profiles', 'profiles.user_id', '=', 'survey_report.user_id')
            -> where('survey_report.survey_id', '=', $survey) -> where('profiles.id_division', '=', $divisi)
            -> groupBy('profiles.id_division')
            -> get();
        return $report;
    }

    public function get_division_corporate($id){
        $division = $this -> report -> select('division.id', 'division.name')
                -> join('profiles', 'profiles.user_id', '=', 'survey_report.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> where('survey_report.survey_id', '=', $id) -> distinct() -> get();
        return $division;
    }

    public function get_report_by_survey_filter($survey, $division){
        $report = $this -> report -> select(DB::raw('sum(survey_report.score) as score'))
                -> join('profiles', 'profiles.user_id', '=','survey_report.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> where('survey_report.survey_id', '=', $survey)
                -> where('division.id', '=', $division)
                -> get();
        return $report;
    }

    public function get_user_report($slug){

        $report = $this -> report -> select(DB::raw('sum(survey_report.score) as score, survey_report.user_id, profiles.first_name, survey.title, survey.slug, division.name as division, survey.id as survey_id'))
                -> join('survey', 'survey.id', '=', 'survey_report.survey_id')
                -> join('profiles', 'profiles.user_id', '=', 'survey_report.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> join('user_survey', 'user_survey.id_user', '=', 'profiles.user_id')
                -> where('survey.id', '=', $slug)
                -> where('survey.status', '!=', 0)
                -> where('user_survey.is_done', '=', 1)
                -> where('user_survey.id_survey', '=', $slug)
                -> groupBy('survey_report.user_id', 'profiles.first_name', 'survey.title', 'survey.slug', 'division.name', 'survey.id')
                -> orderBy('score', 'desc')
                -> get();

        return $report;

    }

    public function get_user_report_by_filter($slug, $divisi){
        $report = $this -> report -> select(DB::raw('sum(survey_report.score) as score, survey_report.user_id, profiles.first_name, survey.title, survey.slug, division.name as division, survey.id as survey_id'))
                -> join('profiles', 'profiles.user_id', '=','survey_report.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> join('survey', 'survey.id', '=', 'survey_report.survey_id')
                -> join('user_survey', 'user_survey.id_survey', '=', 'survey.id')
                -> where('survey.slug', '=', $slug)
                -> where('division.id', '=', $divisi)
                -> where('user_survey.is_done', '=', 1)
                -> groupBy('survey_report.user_id', 'profiles.first_name', 'survey.title', 'survey.slug', 'division.name', 'survey.id')
                -> orderBy('score', 'desc')
                -> paginate(10);
        return $report;
    }

    public function get_report_by_survey($id){
        $report = $this -> report -> select(DB::raw('sum(score) as score, user_id')) -> where('survey_id', '=', $id) -> groupBy('user_id') -> get();
        return $report;
    }

    public function get_detail_report_per_section($user_id, $survey_id){
        $report = $this -> report -> select('profiles.first_name', 'profiles.user_id', 'section_survey.name as section', 'survey_report.score')
                -> join('section_survey', 'section_survey.id', '=', 'survey_report.section_id')
                -> join('profiles', 'profiles.user_id', '=', 'survey_report.user_id')
                -> where('survey_report.user_id', '=', $user_id)
                -> where('survey_report.survey_id', '=', $survey_id)
                -> get();
        return $report;
    }

    public function average_data($id){
        $average = $this -> report -> select(DB::raw('survey_report.survey_id, section_survey.name, avg(survey_report.score) as score'))
                -> join('section_survey', 'section_survey.id', '=', 'survey_report.section_id')
                -> where('survey_report.survey_id', '=', $id)
                -> groupBy('survey_report.survey_id', 'section_survey.name')
                -> get();
        return $average;
    }

    public function result_sub($section, $survey){
        $data = $this -> report -> select('survey_report.score', 'survey_report.survey_id', 'survey_report.section_id')
                -> join('section_survey', 'section_survey.id', '=', 'survey_report.section_id')
                -> join('bank_section', 'bank_section.name', '=', 'section_survey.name')
                -> where('survey_report.survey_id', '=', $survey)
                -> where('bank_section.id', '=', $section)
                -> get();

        if ($data->count() > 0) {
            $results = array();
            foreach ($data as $row) {
                $results[] = $row['score'];
            }
            return $results;
        } else {
            return false;
        }
    }

    public function deviation($survey){
        $data = $this -> report -> select(DB::raw('section_id, stddev(score) as deviation'))
                -> where('survey_id', '=', $survey) -> groupBy('section_id') -> get();

        if ($data->count() > 0){
            $results = array();
            foreach ($data as $row) {
                $results[$row['section_id']] = $row['deviation'];
            }
            return $results;
        }else{
            return false;
        }
    }

    public function result($id){
        $data = $this -> report -> select(DB::raw('avg(survey_report.score) as score, survey_report.user_id, profiles.first_name'))
                -> join('profiles', 'profiles.user_id', 'survey_report.user_id')
                -> groupBy('survey_report.user_id', 'profiles.first_name')
                -> where('survey_report.survey_id', '=', $id) -> get();

        if ($data->count() > 0) {
            $results = array();
            $i = 0;
            foreach ($data as $row) {
                $results[$row['user_id']] = array(
                    "score"     => $row['score'],
                    'user_id'   => $row['user_id'],
                    'name'      => $row['first_name']
                );
                $i++;
            }

            return $results;
        } else {
            return false;
        }
    }

    public function list_group($filter){
        if (!empty($filter)) {
            $data = DB::table('profiles') -> select('profiles.id', 'profiles.first_name', 'division.name as division')
                    -> join('division', 'division.id', '=', 'profiles.id_division')
                    -> whereIn('profiles.id', $filter)
                    -> get();

            if ($data->count() > 0) {
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function get_analysis(){
        $analysis = DB::table('analysis') -> get();
        return $analysis;
    }

    public function update_score_essay($data, $section, $survey, $count_user){
        DB::beginTransaction();
        try{


            $i = 0;
            foreach ($data as $key[]) {

                DB::table('option_answer') -> whereId($key[$i]['id_answer']) -> update(['point' => $key[$i]['point_essay']]);
                DB::table('result') -> where(['id_answer' => $key[$i]['id_answer'], 'user_id' => $key[$i]['user_id']]) -> update(['point' => $key[$i]['point_essay'], 'is_checked' => 1]);

                $i++;
            }

            for($i = 0; $i <= $count_user; $i++){
                $point = $this -> result -> select(DB::raw('sum(result.point) as avg'), 'result.user_id as user_id', 'section_survey.id as section_id', 'survey.id as survey_id')
                    -> join('option_answer', 'option_answer.id', '=', 'result.id_answer')
                    -> join('question', 'question.id', '=', 'option_answer.id_question')
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id')
                    -> where('survey.id', '=', $survey)
                    -> where('section_survey.id', '=', $section)
                    -> where('result.user_id', '=', $data[$i]['user_id'])
                    -> groupBy('result.user_id')
                    -> groupBy('section_survey.id')
                    -> groupBy('survey.id')
                    -> get();

                foreach($point as $point){
                    DB::table('survey_report') -> where(['section_id' => $section, 'user_id' => $point['user_id']])
                    -> update(['score' => $point['avg']]);
                }

            }

            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
        die();
    }

    public function get_final_score_user($survey_id){
        $score = $this -> report -> select(DB::raw('sum(score) as score')) -> where('user_id', '=', Auth::user() -> id)
                -> where('survey_id', '=', $survey_id) -> get();
        foreach ($score as $key) {
            $final_score = $key['score'];
        }

        return $final_score;
    }

    public function get_count_answered($id_section){
        $data = $this -> result -> select('result.id_question')
                -> join('question', 'question.id', '=', 'result.id_question')
                -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                -> where('result.user_id', '=', Auth::user() -> id)
                -> where('section_survey.id', '=', $id_section)
                -> distinct()
                -> get();

            $count['count'] = $data -> count();

        return $count['count'];
    }

    public function get_count_answered_wpa($id_section){
        $data = $this -> result_wpa -> select('result_wpa.number')
                -> join('master_wpa', 'master_wpa.number', '=', 'result_wpa.number')
                -> join('section_survey', 'section_survey.id', '=', 'master_wpa.id_section')
                -> where('result_wpa.user_id', '=', Auth::user() -> id)
                -> where('section_survey.id', '=', $id_section)
                -> distinct()
                -> get();

            $count['count'] = $data -> count();

        return $count['count'];
    }

    public function get_count_answered_papi($id_section){
        $data = $this -> result_papi -> select('result_papikostick.number')
                -> join('master_papikostick', 'master_papikostick.number', '=', 'result_papikostick.number')
                -> join('section_survey', 'section_survey.id', '=', 'master_papikostick.id_section')
                -> where('result_papikostick.user_id', '=', Auth::user() -> id)
                -> where('section_survey.id', '=', $id_section)
                -> distinct()
                -> get();

            $count['count'] = $data -> count();

        return $count['count'];
    }

    public function save_unfinished_question($data){
        try{

            //save to result
            $result = new Result;
            $result -> id_answer    = $data['id_answer'];
            $result -> user_id      = $data['user_id'];
            $result -> id_question  = $data['id_question'];
            $result -> point        = $data['point'];
            $result -> is_checked   = $data['is_checked'];

            $result -> save();
            return true;
        }catch(exception $e){
            return false;
        }
    }

    public function save_result_wpa_papi($data){
        DB::beginTransaction();
        try{
            $is_saved = RawInputLAITIKI::where($data) -> first();
            if(!$is_saved){
                $insert = RawInputLAITIKI::insert($data);
            }
            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function get_result_wpa($id_survey){
        $data = $this -> raw_input -> where('user_id', '=', Auth::user() -> id) -> where('survey_id', '=', $id_survey) -> first();
        $i = 0;
        foreach (json_decode($data['raw_input'], true) as $key) {
            $result['most'][$i] = $key['M'];
            $result['lest'][$i] = $key['L'];
            $i++;
        }
        return $result;
    }

    public function get_result_papi($id_survey){
        $data = $this -> raw_input -> where('user_id', '=', Auth::user() -> id) -> where('survey_id', '=', $id_survey) -> first();
        $i = 0;
        foreach (json_decode($data['raw_input'], true) as $key) {
            $result[$i] = $key['key'];
            $i++;
        }
        return $result;
    }

    public function save_report_wpa($most, $lest, $change, $survey_id, $hasil, $type){
        $is_saved = DB::table('report_wpa') -> where('user_id', '=', Auth::user() -> id) -> where('survey_id', '=', $survey_id) -> first();
        if(!$is_saved){
            DB::table('report_wpa') -> insert([
                'user_id'   => Auth::user() -> id,
                'survey_id' => $survey_id,
                'most'      => $most,
                'lest'      => $lest,
                'change'    => $change,
                'hasil'     => $hasil,
                'type'      => $type,
                'created_at'=> Carbon::now('Asia/Jakarta')
            ]);
        }
    }

    public function save_report_papi($data){

        $is_saved = DB::table('report_papikostick') -> where('user_id', '=', $data['user_id']) -> where('survey_id', '=', $data['survey_id']) 
        -> first();
        if(sizeof($is_saved) == 0){
            DB::table('report_papikostick') -> insert([
                $data
            ]);
        }
    }

    public function get_report_wpa($id){
        $report = $this -> report_wpa -> select(DB::raw('avg(D) as D, avg(I) as I, avg(S) as S, avg(C) as C'))
                -> where('survey_id', '=', $id)
                -> where('criteria', '=', 'change')
                -> get();
        return $report;
    }

    public function get_report_wpa_filter($survey, $division){
        $report = $this -> report_wpa -> select(DB::raw('avg(D) as D, avg(I) as I, avg(S) as S, avg(C) as C'))
                -> join('profiles', 'profiles.user_id', '=','report_wpa.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> where('report_wpa.survey_id', '=', $survey)
                -> where('criteria', '=', 'change')
                -> where('division.id', '=', $division)
                -> get();
        return $report;
    }

    public function get_user_report_wpa($slug){
        $report = $this -> report_wpa -> select('report_wpa.D', 'report_wpa.I', 'report_wpa.S', 'report_wpa.C', 'report_wpa.X', 'report_wpa.user_id', 'profiles.first_name', 'survey.title', 'survey.slug', 'division.name  as division', 'survey.id as survey_id')
                -> join('survey', 'survey.id', '=', 'report_wpa.survey_id')
                -> join('profiles', 'profiles.user_id', '=', 'report_wpa.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> where('survey.slug', '=', $slug)
                -> where('survey.status', '=', 1)
                -> get();
        return $report;

    }

    public function get_report_wpa_papi_detail($table, $id_survey){

        $report = DB::table($table) -> select('profiles.first_name', 'profiles.user_id', 'division.name', 'survey.title', $table.'.survey_id') -> where('survey_id', '=', $id_survey)
                -> join('survey', 'survey.id', '=', $table.'.survey_id')
                -> join('profiles', 'profiles.user_id', '=', $table.'.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> groupBy('profiles.first_name', 'profiles.user_id', 'division.name', 'survey.title', $table.'.survey_id') -> get();

        return $report;
    }

    public function get_report_wpa_papi_detail_by_filter($table, $id_survey, $division){

        $report = DB::table($table) -> select('profiles.first_name', 'profiles.user_id', 'division.name', 'survey.title', $table.'.survey_id') -> where('survey_id', '=', $id_survey)
                -> join('survey', 'survey.id', '=', $table.'.survey_id')
                -> join('profiles', 'profiles.user_id', '=', $table.'.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> where('division.id', '=', $division)
                -> groupBy('profiles.first_name', 'profiles.user_id', 'division.name', 'survey.title', $table.'.survey_id') -> get();

        return $report;
    }


    public function get_report_wpa_by_graph($user_id){

        $report = $this -> report_wpa -> select('profiles.first_name', 'profiles.user_id', 'report_wpa.*')
            -> join('profiles', 'profiles.user_id', '=', 'report_wpa.user_id')
            -> where('report_wpa.user_id', '=', $user_id)
            -> get();
        return $report;
    }

    public function insert_to_rekap_wpa($content){
        $is_saved = DB::table('report_wpa_detail') -> where('user_id', '=', $content['user_id']) -> where('survey_id', '=', $content['survey_id']) -> first();
        if(!$is_saved){
            DB::table('report_wpa_detail') -> insert($content);
        }
    }
    //======================================= REPORT USER =========================================//
    public function get_score_wpa($id_survey, $id_user){

        $report = $this -> report_wpa
                -> join('survey', 'survey.id', '=', 'report_wpa.survey_id')
                -> join('profiles', 'profiles.user_id', '=', 'report_wpa.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> where('profiles.user_id', '=', $id_user)
                -> where('survey_id', '=', $id_survey)
                -> get();

        return $report;
    }

    public function get_report_papi($id_survey, $id_user){
        $report = $this -> report_papi
                -> join('survey', 'survey.id', '=', 'report_papikostick.survey_id')
                -> join('profiles', 'profiles.user_id', '=', 'report_papikostick.user_id')
                -> join('division', 'division.id', '=', 'profiles.id_division')
                -> where('profiles.user_id', '=', $id_user)
                -> where('survey_id', '=', $id_survey)
                -> get();

        return $report;
    }

    public function get_lookup_karakteristik($table, $column){
        $data = DB::table($table) -> select($column) -> inRandomOrder() -> get();
        return $data;
    }

    public function get_lookup_pos_neg($table, $column, $category){
        $data = DB::table($table) -> select($column) -> where('category', '=', $category) -> inRandomOrder() -> get();
        return $data;
    }

    public function get_hasil_interpretasi_papi($filter){
        foreach ($filter as $key => $value) {
            $data[$key] = DB::table('lookup_papi_acuan_report') -> where('key', '=', $key) -> where('count', '=', $value) -> get();
        }
        return $data;
    }

    public function get_standard_score_tiki($column, $raw_score){
        $data = DB::table('lookup_tiki_raw_score') -> select($column) -> where('raw_score', '=', $raw_score) -> first();
        return $data;
    }

    public function get_iq_tiki($total){
        $data   = DB::table('lookup_tiki_iq') -> where('total', '=', $total) -> first();
        $iq     = $data -> iq;
        
        return $iq;
    }

    public function get_lookup_tipe_kepribadian($profile){
        $data   = DB::table('lookup_wpa_tipe_kepribadian') -> where('profile', '=', $profile) -> first();
        $lookup['type']     = $data->type;
        $lookup['count']    = $data->count;
        return $lookup;
    }

    public function get_lookup_uraian_kepribadian($type, $column){
        $data = DB::table('lookup_wpa_uraian_kepribadian') -> select('description', $column) -> where('type', '=', $type) -> first();
        $lookup['description'] = $data->description;
        $lookup['detail']      = $data->detail;
        return $lookup;
    }

    public function get_hasil_wpa($user_id, $survey_id){
        $data = $this -> report_wpa -> select('hasil', 'type')
                -> where('user_id', '=', $user_id)
                -> where('survey_id', '=', $survey_id)
                -> first();
        $hasil['type']  = $data['type'];
        $hasil['hasil'] = $data['hasil'];

        return $hasil;
    }

    public function get_rekap_wpa_detail($user_id, $survey_id){
        $data = DB::table('report_wpa_detail') -> where('user_id', '=', $user_id) -> where('survey_id', '=', $survey_id) -> first();
        return $data;
    }

    public function get_kekuatan_kelemahan_gti($column){
        $data = DB::table('lookup_gti_kekuatan_kelemahan') -> select($column) -> inRandomOrder() -> first();

        return $data->$column;
    }

    public function get_tanggal_pengerjaan($id_survey, $id_user){
        $date = DB::table('user_survey') -> select('updated_at') -> where('id_user', '=', $id_user) -> where('id_survey', '=', $id_survey) -> first();

        return $date -> updated_at;
    }


    //======================================= JPM =======================================//

    public function getJPMLink(){
        $link = DB::table('survey')
                -> select('user_survey.updated_at', 'survey.slug')
                -> join('user_survey', 'user_survey.id_survey', '=', 'survey.id')
                -> where('user_survey.is_done', '=', 1)
                -> where('survey.category_survey_id', '=', 3)
                -> where('id_user', '=', Auth::user()->id) -> get();

        return $link;
    }

    public function get_job_profile(){
        $data = DB::table('jobs') -> get();
        return $data;
    }

    public function convert_total_to_kuadran($column, $grafik, $total){
        $data = DB::table('lookup_wpa_kuadran_user') -> select($column) -> where('grafik', '=', $grafik) -> where('total', '=', $total) -> first();
        $kuadran = $data -> $column;
        return $kuadran;
    }

    public function get_jpm_job($id){
        $data = DB::table('job_karakteristiks') -> where('jobs_id', '=', $id) -> get();
        return $data;
    }

    public function get_compatibility($user, $job){
        $data = DB::table('jobs_compatibility') -> select ('kriteria_value') -> where('job_profil', '=', $job) -> where('job_personal', '=', $user) -> first();
        $compatibility = $data -> kriteria_value;
        return $compatibility;
    }

    public function get_kuadran_job($jpm){
        $data = DB::table('lookup_wpa_kuadran_job') -> where('jpm', '=', $jpm) -> first();
        $kuadran = $data -> kuadran;

        return $kuadran;
    }

    public function get_gender_user($id){
        $data = DB::table('profiles') -> where('user_id', '=', $id) -> first();
        $gender = $data -> gender;

        return $gender;
    }

    public function get_aggreement($avg, $gender){
        if($gender == null){
            $gender = 'pria';
        }
        $data = DB::table('job_agreement') -> select($gender) -> where('agreement', '=', $avg) -> first();
        $agreement = $data -> $gender;

        return $agreement;
    }

    //============================================= RAW DATA =============================================//
    public function ambil_row_survey($id){
        $row = $this -> report -> select('survey.*','project.name', 'project.slug as slug_project') -> join('survey', 'survey.id', '=', 'survey_report.survey_id') 
                -> join('project', 'project.id', '=', 'survey.id_project')
                -> where('survey.user_id','=', $id) -> groupBy('survey.id') -> get();
        return $row;
    }

    public function ambil_row_peserta_survey($id){
        $row = $this -> report -> where('survey_report.survey_id', '=', $id) 
                -> join('profiles', 'profiles.user_id', '=', 'survey_report.user_id') 
                -> groupBy('survey_report.user_id') -> get();
        return $row;
    }

    public function ambil_nilai_peserta($user_id, $survey_id){
        $skor = $this -> report -> where('survey_report.user_id', '=', $user_id) -> where('survey_report.survey_id', '=', $survey_id) -> orderBy('survey_report.section_id') -> get();
        return $skor;
    }

    public function ambil_row_result_lai($user_id, $section_id){
        $raw_jawaban = $this -> raw_input -> where('user_id', '=', $user_id) -> where('section_id', '=', $section_id) -> first();
        $jawaban = json_decode($raw_jawaban['raw_input'], true);
        $i = 0;
        foreach ($jawaban as $key) {
            $jawaban[$i]['question']= DB::table('question') -> where('id', '=', $key['id_question']) -> first() -> question;
            $jawaban[$i]['answer']  = DB::table('option_answer') -> where('id', '=', $key['id_answer']) -> first() -> answer;
            $i++;
        }
        return $jawaban;
    }

     public function ambil_row_result_tiki($user_id, $section_id){

        $raw_jawaban = $this -> raw_input -> where('user_id', '=', $user_id) -> where('section_id', '=', $section_id) -> first();
        $jawaban = json_decode($raw_jawaban['raw_input'], true);
        $i = 0;
        foreach ($jawaban as $key) {
            $jawaban[$i]['question']= DB::table('question') -> where('id', '=', $key['id_question']) -> first() -> question;
            $jawaban[$i]['answer']  = DB::table('option_answer') -> where('id', '=', $key['id_answer']) -> first() -> answer;
            $i++;
        }
        return $jawaban;
    }

    public function ambil_row_pertanyaan_tiki($user_id, $section_id){
        $raw_jawaban = $this -> raw_input -> where('user_id', '=', $user_id) -> where('section_id', '=', $section_id) -> first();
        $jawaban = json_decode($raw_jawaban['raw_input'], true);
        $i = 0;
        if($jawaban){
            foreach ($jawaban as $key) {
                $jawaban[$i]['question']= DB::table('question') -> where('id', '=', $key['id_question']) -> first() -> question;
                $jawaban[$i]['answer']  = DB::table('option_answer') -> where('id', '=', $key['id_answer']) -> first() -> answer;
                $i++;
            }
            
        }
        return $jawaban;
    }
    public function ambil_kunci_jawaban($id){
        $data = DB::table('option_answer') -> select('answer') -> where('id_question', '=', $id) -> where('point', '=', 1) -> orderBy('option_answer.id') -> get();
        $i = 0;
        foreach ($data as $key) {
            $kunci[$i] = $key->answer;
            $i++;
        }
        return $kunci;
    }

    //======================================= RAW INPUT =======================================//

    public function get_raw_input($survey_id, $section_id){ //dipanggil saat savetoreport
        $raw_input = $this -> raw_input -> where('survey_id', '=', $survey_id) -> where('section_id', '=', $section_id) -> where('user_id', '=', Auth::user() -> id) -> first();
        return $raw_input;
    }

    public function get_raw_input_by_tools($tools_id){
        $raw = $this -> raw_input -> where('survey_id', '=', $tools_id) -> orderBy('user_id') -> orderBy('section_id') ->  get();
        return $raw;
    }
}
