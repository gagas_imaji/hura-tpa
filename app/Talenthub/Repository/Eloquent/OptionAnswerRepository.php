<?php

namespace App\Talenthub\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use DB; 
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\OptionAnswer;

class OptionAnswerRepository extends AbstractRepository implements OptionAnswerRepositoryInterface
{
    
    
    protected $model;

    /**
     * @param Question     $question
     */
    public function __construct(OptionAnswer $option_answer)
    {
        $this->option_answer = $option_answer;
    }

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */
    public function get_by_question_unhide($id, $random){
        if($random == 1){
            $option_answer = $this -> option_answer -> where('id_question', '=', $id) -> where('hidden', '=', 0) -> inRandomOrder() -> get();
        }else{
            $option_answer = $this -> option_answer -> where('id_question', '=', $id) -> where('hidden', '=', 0) -> get();
        }
        return $option_answer;
    }

    public function get_no_answer($id){
        $option_answer = $this -> option_answer -> where('id_question', '=', $id) -> where('answer', '=', 'No Answer') -> get();
        return $option_answer;

    }

    public function get_answer_text($id){
        $option_answer = $this -> option_answer -> where('id_question', '=', $id) -> where('hidden', '=', 0) -> get();
        return $option_answer;
    }

    public function get_all_by_question($id){
        $option_answer = $this -> option_answer -> where('id_question', '=', $id) -> where('hidden', '=', 0) -> get();
        return $option_answer;
    }

    public function add_true_answer_text($insert){
        
        $new_answer = DB::table('option_answer')->insertGetId([ 
            'id_question'   => $insert['id_question'],
            'answer'        => $insert['answer'],
            'image'         => $insert['image'],
            'display_answer'=> $insert['display_answer'],
            'hidden'        => $insert['hidden'],
            'point'         => $insert['point']
        ]);

        return $new_answer;
    }

    public function get_option_matching($section, $random, $section_limit, $total, $arr_id_question){
        if($random == 1){
            if($section_limit == 0 || $section_limit >= $total){
                $option_answer = $this -> option_answer -> select('option_answer.answer', 'option_answer.id')
                            -> join('question', 'question.id', '=', 'option_answer.id_question')
                            -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                            -> where('section_survey.id', '=', $section) -> where('hidden', '=', 0) 
                            -> inRandomOrder() -> get();
            }elseif($section_limit != 0 && $section_limit < $total){
                $option_answer = $this -> option_answer -> select('option_answer.answer', 'option_answer.id')
                            -> join('question', 'question.id', '=', 'option_answer.id_question')
                            -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                            -> where('section_survey.id', '=', $section) -> where('hidden', '=', 0) 
                            -> whereIn('option_answer.id_question', $arr_id_question)
                            -> inRandomOrder() 
                            -> limit($section_limit) -> get();
            }
        }else{
            if($section_limit == 0 || $section_limit >= $total){
                $option_answer = $this -> option_answer -> select('option_answer.answer', 'option_answer.id')
                            -> join('question', 'question.id', '=', 'option_answer.id_question')
                            -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                            -> where('section_survey.id', '=', $section) -> where('hidden', '=', 0) -> get();
            }elseif($section_limit != 0 && $section_limit < $total){
                $option_answer = $this -> option_answer -> select('option_answer.answer', 'option_answer.id')
                            -> join('question', 'question.id', '=', 'option_answer.id_question')
                            -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                            -> where('section_survey.id', '=', $section) -> where('hidden', '=', 0) 
                            -> whereIn('option_answer.id_question', $arr_id_question)
                            -> limit($section_limit) -> get();
            }
        }
        return $option_answer;
    }

    public function get_point_of_answer($id){
        $data = $this -> option_answer -> whereId($id) -> get();
        foreach ($data as $key) {
            $point = $key['point'];
        }
        return $point;
    }

    public function get_answer_essay($question){
        $answer = $this -> option_answer -> select('option_answer.id as id_answer', 'option_answer.answer', 'profiles.first_name', 'profiles.user_id')
                    -> join('result', 'result.id_answer', '=', 'option_answer.id')
                    -> join('profiles', 'profiles.user_id', '=', 'result.user_id')
                    -> where('option_answer.id_question', '=', $question)
                    -> get();
        return $answer;
    }

    public function update_answer_info_on_question($id, $answer, $point){
        for($i = 0; $i < count($id); $i++){
            $this -> option_answer -> where('id', '=', $id[$i]) -> update(['answer' => $answer[$i], 'point' => $point[$i]]);
        }
    }
}
