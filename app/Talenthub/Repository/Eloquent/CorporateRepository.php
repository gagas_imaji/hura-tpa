<?php

namespace App\Talenthub\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use DB; 
use App\Talenthub\Repository\CorporateRepositoryInterface;
use App\Corporate;
use App\Division;

class CorporateRepository extends AbstractRepository implements CorporateRepositoryInterface
{
    
    
    protected $model;

    /**
     * @param Corporate     $corporate
     */
    public function __construct(Corporate $corporate, Division $division)
    {
        $this->corporate = $corporate;
        $this->division = $division;
    }

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */
    public function getById($id)
    {
        $corporate = $this -> corporate -> whereId($id) -> first();
        return $corporate;
    }

    public function getAll(){
        $corporate = $this -> corporate -> get();
        return $corporate;
    }

    public function getCorporateByUser($id){
        $corp = DB::table('profiles')->select('profiles.id_corporate', 'corporate.name')->join('corporate', 'corporate.id', '=', 'profiles.id_corporate')->where('profiles.user_id', '=', $id)->first();
        $corporate['id']     = $corp->id_corporate;
        $corporate['name']   = $corp->name;
        return $corporate;
    }

    public function get_division_corporate(){
        $division = $this -> division -> select('division.id', 'division.name') 
                -> join('corporate', 'corporate.id', '=', 'division.id_corporate') 
                -> join('profiles', 'profiles.id_corporate', '=', 'corporate.id') 
                -> where('profiles.user_id', '=', Auth::user() -> id) -> get();

        return $division;

    }

    public function get_user_on_corporate($id){
        $data = DB::table('profiles') -> where('id_corporate', '=', $id) -> get();
        $i = 0;
        foreach ($data as $key) {
            $users[$i]['id'] = $key ->id;
            $users[$i]['first_name'] = $key ->first_name;
            $i++;
        }
        return $users;
    }

    public function get_list_division_by_corporate($id){
        $data = $this -> division -> where('id_corporate', '=', $id) -> get();

        return $data;
    }

    public function set_credit($id, $credit){
        DB::beginTransaction();
        try{
            DB::table('credit_corporate') -> insert([
                'id_corporate' => $id,
                'credit' => $credit
            ]);
            DB::commit();
            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }

    public function get_credit($id){
        $credit = DB::table('credit_corporate') -> where('id_corporate', '=', $id) -> get() -> sum('credit');

        return $credit;
    }

    public function get_kuota_aktif($id){
        $kuota_aktif = DB::table('user_project') -> join('project', 'project.id', '=','user_project.id_project') 
                        -> join('profiles', 'profiles.user_id', '=', 'project.user_id')
                        -> where('project.is_done', '!=', 1) -> where('profiles.id_corporate', '=', $id) -> get() -> count();    
        return $kuota_aktif;
    }

    public function get_kuota_terpakai($id){ //kuota terpakai pada project yang 

        $kuota_terpakai = DB::table('user_project') -> join('project', 'project.id', '=', 'user_project.id_project') 
                        -> join('profiles', 'profiles.user_id', '=', 'project.user_id')
                        -> where('project.is_done', '=', 1) -> where('user_project.is_done', '=', 1) -> where('profiles.id_corporate', '=', $id) -> get() -> count(); 
        return $kuota_terpakai;
    }
}
