<?php

namespace App\Talenthub\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use DB; 
use App\Talenthub\Repository\TestingCorporateRepositoryInterface;
use App\Corporate;
use App\Division;

class TestingCorporateRepository extends AbstractRepository implements TestingCorporateRepositoryInterface
{
    
    
    protected $model;

    /**
     * @param Corporate     $corporate
     */
    public function __construct(Corporate $corporate, Division $division)
    {
        $this->corporate = $corporate;
        $this->division = $division;
    }

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */
    public function getById($id)
    {
        $corporate = $this -> corporate -> whereId($id) -> first();
        return $corporate;
    }

    public function getAll(){
        $corporate = $this -> corporate -> get();
        return $corporate;
    }

    public function getCorporateByUser($id){
        $corp = DB::table('profiles')->select('profiles.id_corporate', 'corporate.name')->join('corporate', 'corporate.id', '=', 'profiles.id_corporate')->where('profiles.user_id', '=', $id)->get();
        foreach($corp as $key){
            $corporate['id']     = $key -> id_corporate;
            $corporate['name']   = $key -> name;
        }
        return $corporate;
    }

    public function get_division_corporate(){
        $division = $this -> division -> select('division.id', 'division.name') 
                -> join('corporate', 'corporate.id', '=', 'division.id_corporate') 
                -> join('profiles', 'profiles.id_corporate', '=', 'corporate.id') 
                -> where('profiles.user_id', '=', Auth::user() -> id) -> get();

        return $division;

    }

    public function get_user_on_corporate($id){
        $data = DB::table('profiles') -> where('id_corporate', '=', $id) -> get();
        $i = 0;
        foreach ($data as $key) {
            $users[$i]['id'] = $key ->id;
            $users[$i]['first_name'] = $key ->first_name;
            $i++;
        }
        return $users;
    }

    public function get_list_division_by_corporate($id){
        $data = $this -> division -> where('id_corporate', '=', $id) -> get();

        return $data;
    }
}
