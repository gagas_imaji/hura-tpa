<?php

namespace App\Talenthub\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use DB; 
use App\Talenthub\Repository\TestingQuestionRepositoryInterface;
use App\Question;
use App\Result;
use App\QuestionType;
use App\Survey;
use App\MasterWPA;
use App\MasterPapi;

class TestingQuestionRepository extends AbstractRepository implements TestingQuestionRepositoryInterface
{
    
    
    protected $model;

    /**
     * @param Question     $question
     */
    public function __construct(Question $question, MasterWPA $master_wpa, MasterPapi $master_papi)
    {
        $this->question     = $question;
        $this->master_wpa   = $master_wpa;
        $this->master_papi  = $master_papi;
    }

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */
    public function get_question_survey($slug, $section, $section_limit, $total){
        $survey = DB::table('survey')->where('slug', '=', $slug)->first();
        if($survey -> is_random == 1){
            if($section_limit == 0 || $section_limit >= $total){
                $done_section = DB::table('survey_report') -> select('survey_report.section_id') -> from('survey_report')
                        -> where('survey_report.survey_id', '=', $survey -> id)
                        -> where('survey_report.user_id', '=', 0) -> get();
                $i = 0;
                $not_in = [];
                foreach ($done_section as $key) {
                    foreach ($key as $value) {
                        $not_in[$i] = $value;
                        $i++;
                    }
                }
                $question = $this -> question -> select('question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> whereNotIn('question.id_section', $not_in)
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> inRandomOrder()
                    // -> limit(1)
                    -> get();
            }elseif($section_limit != 0 && $section_limit < $total){
                $question = $this -> question -> select('question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> whereNotIn('question.id', function($q){
                        $q -> select('option_answer.id_question') -> from('option_answer')
                        -> join('result', 'result.id_answer', '=', 'option_answer.id')
                        -> where('result.user_id', '=', 0);
                    })
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> inRandomOrder()
                    -> limit($section_limit)
                    -> get();
            }
        }else{
            if($section_limit == 0 || $section_limit >= $total){

                $question = $this -> question -> select('question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> whereNotIn('question.id', function($q){
                        $q -> select('option_answer.id_question') -> from('option_answer')
                        -> join('result', 'result.id_answer', '=', 'option_answer.id')
                        -> where('result.user_id', '=', 0);
                    })
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    // -> limit(1)
                    -> get();
            }elseif($section_limit != 0 && $section_limit < $total){
                $question = $this -> question -> select('question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> whereNotIn('question.id', function($q){
                        $q -> select('option_answer.id_question') -> from('option_answer')
                        -> join('result', 'result.id_answer', '=', 'option_answer.id')
                        -> where('result.user_id', '=', 0);
                    })
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> limit($section_limit)
                    -> get();
            }
        }
        
        if($question == null){
            return "kosong";
        }else{
            return $question;
        }
    }
 
    public function get_question_survey_wpa($slug, $section){
        $survey = DB::table('survey')->where('slug', '=', $slug)->first();
        if($survey -> is_random == 1){
            $number = $this -> master_wpa -> select('number') -> distinct() -> inRandomOrder() -> get();
        }else{
            $number = $this -> master_wpa -> select('number') -> distinct() -> get();
        }
        $question = $this -> master_wpa -> select('master_wpa.number', 'master_wpa.M as most', 'master_wpa.L as lest', 'master_wpa.statement', 'survey.title as survey_title', 'survey.id as id_survey', 'survey.timer as timer_survey') 
                -> join('section_survey', 'section_survey.id', '=', 'master_wpa.id_section') 
                -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                -> where('survey.slug', '=', $slug) 
                -> where('section_survey.id', '=', $section)
                // -> orderBy('number')
                -> inRandomOrder('number')
                -> get();
        if($question == null){
            return "kosong";
        }else{
            return $question;
        }
    }

    public function get_question_survey_papi($slug, $section){
        $survey = DB::table('survey')->where('slug', '=', $slug)->first();
        
        if($survey -> is_random == 1){

            $question = $this -> master_papi -> select('master_papikostick.number', 'master_papikostick.key as key', 'master_papikostick.statement', 'survey.title as survey_title', 'survey.id as id_survey', 'survey.timer as timer_survey', 'survey.is_random as random') 
                    -> join('section_survey', 'section_survey.id', '=', 'master_papikostick.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> inRandomOrder('number')
                    -> get();
        }else{
            $question = $this -> master_papi -> select('master_papikostick.number', 'master_papikostick.key as key', 'master_papikostick.statement', 'survey.title as survey_title', 'survey.id as id_survey', 'survey.timer as timer_survey', 'survey.is_random as random') 
                    -> join('section_survey', 'section_survey.id', '=', 'master_papikostick.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> get();
        }
        if($question == null){
            return "kosong";
        }else{
            return $question;
        }
    }

    public function get_question_survey_reading($slug, $section, $section_limit, $total){
        $survey = DB::table('survey')->where('slug', '=', $slug)->first();
        if($survey -> is_random == 1){
            if($section_limit == 0 || $section_limit >= $total){
                $question = $this -> question -> select('question_paragraph.paragraph', 'question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> join('question_paragraph', 'question_paragraph.section_id', '=', 'section_survey.id')
                    -> whereNotIn('question.id', function($q){
                        $q -> select('option_answer.id_question') -> from('option_answer')
                        -> join('result', 'result.id_answer', '=', 'option_answer.id')
                        -> where('result.user_id', '=', 0);
                    })
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> inRandomOrder()
                    -> get();
            }elseif($section_limit != 0 && $section_limit < $total){
                $question = $this -> question -> select('question_paragraph.paragraph', 'question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> join('question_paragraph', 'question_paragraph.section_id', '=', 'section_survey.id')
                    -> whereNotIn('question.id', function($q){
                        $q -> select('option_answer.id_question') -> from('option_answer')
                        -> join('result', 'result.id_answer', '=', 'option_answer.id')
                        -> where('result.user_id', '=', 0);
                    })
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> limit($section_limit)
                    -> inRandomOrder()
                    -> get();
            }
        }else{
            if($section_limit == 0 || $section_limit >= $total){
                $question = $this -> question -> select('question_paragraph.paragraph', 'question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> join('question_paragraph', 'question_paragraph.section_id', '=', 'section_survey.id')
                    -> whereNotIn('question.id', function($q){
                        $q -> select('option_answer.id_question') -> from('option_answer')
                        -> join('result', 'result.id_answer', '=', 'option_answer.id')
                        -> where('result.user_id', '=', 0);
                    })
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> get();
            }elseif($section_limit != 0 && $section_limit < $total){
                $question = $this -> question -> select('question_paragraph.paragraph', 'question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                    -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                    -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                    -> join('question_paragraph', 'question_paragraph.section_id', '=', 'section_survey.id')
                    -> whereNotIn('question.id', function($q){
                        $q -> select('option_answer.id_question') -> from('option_answer')
                        -> join('result', 'result.id_answer', '=', 'option_answer.id')
                        -> where('result.user_id', '=', 0);
                    })
                    -> where('survey.slug', '=', $slug) 
                    -> where('section_survey.id', '=', $section)
                    -> limit($section_limit)
                    -> get();
            }
        }
        
        if($question == null){
            return "kosong";
        }else{
            return $question;
        }
    }

    public function get_question_survey_matching($slug, $section, $section_limit, $total){

        $survey = DB::table('survey')->where('slug', '=', $slug)->first();
        if($section_limit == 0 || $section_limit >= $total){
            $question = $this -> question -> select('question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                -> whereNotIn('question.id', function($q){
                    $q -> select('option_answer.id_question') -> from('option_answer')
                    -> join('result', 'result.id_answer', '=', 'option_answer.id')
                    -> where('result.user_id', '=', 0);
                })
                -> where('survey.slug', '=', $slug) 
                -> where('section_survey.id', '=', $section)
                -> inRandomOrder()
                -> get();
        }elseif($section_limit != 0 && $section_limit < $total){
            $question = $this -> question -> select('question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                -> whereNotIn('question.id', function($q){
                    $q -> select('option_answer.id_question') -> from('option_answer')
                    -> join('result', 'result.id_answer', '=', 'option_answer.id')
                    -> where('result.user_id', '=', 0);
                })
                -> where('survey.slug', '=', $slug) 
                -> where('section_survey.id', '=', $section)
                -> limit($section_limit)
                -> inRandomOrder()
                -> get();
        }
        
        
        if($question == null){
            return "kosong";
        }else{
            return $question;
        }
    }

    public function get_question_survey_blank_in_paragraph($slug, $section, $section_limit, $total){

        $survey = DB::table('survey')->where('slug', '=', $slug)->first();
        if($section_limit == 0 || $section_limit >= $total){
            $question = $this -> question -> select('question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                -> whereNotIn('question.id', function($q){
                    $q -> select('option_answer.id_question') -> from('option_answer')
                    -> join('result', 'result.id_answer', '=', 'option_answer.id')
                    -> where('result.user_id', '=', 0);
                })
                -> where('survey.slug', '=', $slug) 
                -> where('section_survey.id', '=', $section)
                -> get();
        }elseif($section_limit != 0 && $section_limit < $total){
            $question = $this -> question -> select('question.id as id_question', 'question.question', 'question.id_type_question', 'question.id_section', 'question.is_random', 'question.timer', 'question.is_mandatory', 'section_survey.name as name_section', 'section_survey.id as id_section', 'survey.title as survey_title', 'survey.timer as timer_survey', 'survey.id as id_survey', 'section_survey.timer as timer_section') 
                -> join('section_survey', 'section_survey.id', '=', 'question.id_section') 
                -> join('survey', 'survey.id', '=', 'section_survey.survey_id') 
                -> whereNotIn('question.id', function($q){
                    $q -> select('option_answer.id_question') -> from('option_answer')
                    -> join('result', 'result.id_answer', '=', 'option_answer.id')
                    -> where('result.user_id', '=', 0);
                })
                -> where('survey.slug', '=', $slug) 
                -> where('section_survey.id', '=', $section)
                -> limit($section_limit)
                -> get();
        }
        
        
        if($question == null){
            return "kosong";
        }else{
            return $question;
        }
    }

    public function save_answer($data){
        try{
            if($data['section_name'] == 'Reading Text'){ 
                $i = 0;              
                foreach ($data['id_answer'] as $key) {
                   //save to result
                    $answer = new Result;
                    $answer -> user_id      = $data['id_user'];
                    $answer -> id_answer    = $key;
                    $answer -> id_question  = $data['id_question'][$i];
                    $answer -> point = $data['point'][$i];
                    $answer -> timer = $data['timer'];

                    $answer -> save();
                    $i++;
                }
            }elseif($data['section_name'] == 'Matching Question'){
                $i = 0;
                
                foreach ($data['id_answer'] as $key) {
                    $true_answer = DB::table('option_answer') 
                                -> select('id')
                                -> where('id_question', '=', $data['id_question'][$i]) 
                                -> where('hidden', '=', 0)
                                -> first();

                    if($true_answer -> id == $key){
                        $point = 1 * $data['point'][$i];
                    }else{
                        $point = 0 * $data['point'][$i];
                    }
                    
                   // //save to result
                    $answer = new Result;
                    $answer -> user_id      = $data['id_user'];
                    $answer -> id_answer    = $key;
                    $answer -> id_question  = $data['id_question'][$i];
                    $answer -> point = $point;
                    $answer -> timer = 0;

                    $answer -> save();
                    $i++;
                }
                die();
            }elseif($data['section_name'] == 'Blank in Paragraph'){ 
                $i = 0;              
                foreach ($data['id_answer'] as $key) {
                   //save to result
                    $answer = new Result;
                    $answer -> user_id = $data['id_user'];
                    $answer -> id_answer = $key;
                    $answer -> id_question = $data['id_question'][$i];
                    $answer -> point = $data['point'][$i];
                    $answer -> timer = $data['timer'];

                    $answer -> save();
                    $i++;
                }
            }else{ 

                //save to result
                $answer = new Result;
                $answer -> user_id     = $data['id_user'];
                $answer -> id_answer   = $data['id_answer'];
                $answer -> id_question = $data['id_question'];
                $answer -> point = $data['point'];
                $answer -> timer = $data['timer'];
                
                //jika type pertanyaan essay, maka is_checked = 0 karna belum diperiksa
                if($data['id_type_question'] == 5){
                    $answer -> is_checked = 0;    
                }

                $answer -> save();
            }
            
            return true;
        }catch(exception $e){
            return false;
        }
    }

    public function get_total_question($section){
        $total = $this -> question -> where('id_section', '=', $section) -> get();
        return $total;
    }

    public function get_list_question_by_survey($id){
        $question = $this -> question -> select('question.id', 'question.question', 'question.is_random', 'question.timer', 'question.is_mandatory', 'question.point', 'section_survey.name as section', 'question_type.name as type')
            ->join('question_type', 'question_type.id', '=', 'question.id_type_question')
            ->join('section_survey', 'section_survey.id', '=', 'question.id_section')
            ->where('section_survey.survey_id', '=', $id)
            ->get();

        return $question;
    }

    public function get_detail_question_by_survey($id){
        $question = $this -> question -> select('question.id', 'question.question', 'question.is_random', 'question.timer', 'question.is_mandatory', 'question.point', 'section_survey.name as section', 'question_type.name as type', 'survey.id as survey_id', 'survey.title')
            ->join('question_type', 'question_type.id', '=', 'question.id_type_question')
            ->join('section_survey', 'section_survey.id', '=', 'question.id_section')
            ->join('survey', 'section_survey.survey_id', '=', 'survey.id')
            ->where('question.id', '=', $id)
            ->get(); 

        return $question;
    }

    public function get_section_essay($id){
        $data = $this -> question -> select('section_survey.id', 'section_survey.name')
                -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
                -> where('question.id_type_question', '=', 5)
                -> where('section_survey.survey_id', '=', $id)
                -> whereIn('question.id', DB::table('result')->select('id_question')->where('is_checked', '=', 0))
                -> groupBy('section_survey.id', 'section_survey.name')
                -> get();
        return $data;
    }

    public function get_question_by_section($section){
        $question = $this -> question -> where('id_section', '=', $section) -> get();
        return $question;
    }

    public function get_point_of_question($id){
        $data = $this -> question -> whereId($id) -> get();
        foreach ($data as $key) {
            $point = $key['point'];
        }
        return $point;
    }

    public function get_total_question_on_section($section){
        $total = $this -> question -> select(DB::raw('count(id) as total')) -> where('id_section', '=', $section) -> first();

        return $total['total'];
    }

    public function get_unfinished_question($id_survey){
        $data_unfinished = $this -> question -> select('question.id as id_question', 'question.id_section', 'option_answer.id as id_option_answer') 
            -> join('option_answer', 'option_answer.id_question', '=', 'question.id')
            -> join('section_survey', 'section_survey.id', '=', 'question.id_section')
            -> join('survey', 'survey.id', '=', 'section_survey.survey_id')
            -> where('survey.id', '=', $id_survey)
            -> whereNotIn('question.id', DB::table('result') -> select('result.id_question') -> where('result.user_id', '=', 0))
            -> where('option_answer.hidden', '=', 1)
            -> get();

        return $data_unfinished;
    }

    public function get_unfinished_number_wpa($id_survey){
        $data_unfinished = $this -> master_wpa -> select('master_wpa.number', 'master_wpa.M', 'master_wpa.L') 
            -> join('section_survey', 'section_survey.id', '=', 'master_wpa.id_section')
            -> join('survey', 'survey.id', '=', 'section_survey.survey_id')
            -> where('survey.id', '=', $id_survey)
            -> whereNotIn('master_wpa.number', DB::table('result_wpa') -> select('result_wpa.number') -> where('result_wpa.user_id', '=', 0) -> where('result_wpa.survey_id', '=', $id_survey))
            -> distinct()
            -> get();

        return $data_unfinished;
    }


    public function update_info_question($id, $question, $is_random, $is_mandatory, $timer, $point){
        $this -> question -> whereId($id) -> update(['question' => $question, 'is_random' => $is_random, 'is_mandatory' => $is_mandatory, 'timer' => $timer, 'point' => $point]);
    }
}
