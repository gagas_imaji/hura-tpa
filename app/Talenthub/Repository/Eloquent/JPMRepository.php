<?php

namespace App\Talenthub\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use DB; 
use App\Talenthub\Repository\JPMRepositoryInterface;
use App\MasterJobAnalysis;
use App\Jobs_karakteristik;
use App\Jobs;



class JPMRepository extends AbstractRepository implements JPMRepositoryInterface
{
    
    
    protected $model;

    /**
     * @param Corporate     $corporate
     */
    public function __construct(MasterJobAnalysis $master_job_analysis, Jobs_karakteristik $jobkar, Jobs $job)
    {
        $this -> master_job_analysis    = $master_job_analysis;
        $this -> jobkar                 = $jobkar;
        $this -> job                    = $job;
    }

    public function get_all_jobkar(){
        $data = $this -> jobkar -> select('job_karakteristiks.id','jobs.jobs_name', 'job_karakteristiks.D','job_karakteristiks.I','job_karakteristiks.S','job_karakteristiks.C') -> join('jobs', 'job_karakteristiks.jobs_id', '=', 'jobs.id') -> get(); 

        return $data;
    }

    public function get_detail_jobkar($id){
        $data = DB::table('job_karakteristiks')->where('id', '=', $id) -> first();

        return $data;
    }

    public function get_kuadran_job($jpm){
        $data = DB::table('lookup_wpa_kuadran_job') -> where('jpm', '=', $jpm) -> first();
        $kuadran = $data -> kuadran;
        
        return $kuadran;
    }

    public function get_jpm_job($id){
        $data = DB::table('job_karakteristiks') -> where('jobs_id', '=', $id) -> get();
        return $data;
    }

    public function get_master_job_analysis(){
       $data = $this -> master_job_analysis -> get();
       return $data;
    }

    public function add_new_job_characteristic($data){
        DB::beginTransaction();
        try{
            $job_id = $this -> job -> insertGetId([
                'jobs_name' => $data['nama'],
                'created_at'=> Carbon::now()
            ]);

            $jobkar_id = $this -> jobkar -> insertGetId([
                'jobs_id'   => $job_id,
                'D'         => $data['D'],
                'I'         => $data['I'],
                'S'         => $data['S'],
                'C'         => $data['C'],
                'created_at'=> Carbon::now()
            ]);

            return true;
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }
}
