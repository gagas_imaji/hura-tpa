<?php

namespace App\Talenthub\Repository;

use App\Survey;

interface TestingSurveyRepositoryInterface
{

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */ 
    public function get_daftar_survey();

    public function get_info_survey_by_slug($slug);

    public function survey_complete($slug);

    public function save_temp($data);

    public function get_temp($id);

    public function delete_temp_survey($slug);

    public function getHistory();

    public function get_first_section_on_survey($id);

    public function get_next_section($slug, $section);

    public function get_count_answered($section);

    public function get_survey_by_id($id);

    public function get_list_survey_data_table();

    public function check_quarter_survey($category, $quarter);

    public function get_ongoing_survey();
} 
