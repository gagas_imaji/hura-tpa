<?php

namespace App\Talenthub\Repository;

use App\StandarNilaiProject;

interface StandarNilaiRepositoryInterface
{

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */ 

    public function save_temp($data);
    public function get_list_standar_nilai_by_project($id);
    public function update_save_temp($data);
    public function create_standar_nilai($data);
} 