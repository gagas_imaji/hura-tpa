<?php

namespace App\Talenthub\Repository;

use App\OptionAnswer;

interface OptionAnswerRepositoryInterface
{

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */ 
    public function get_by_question_unhide($id, $random);

    public function get_no_answer($id);

    public function get_answer_text($id);

    public function get_all_by_question($id);
    
    public function add_true_answer_text($insert);
} 
