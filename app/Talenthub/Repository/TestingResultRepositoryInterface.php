<?php

namespace App\Talenthub\Repository;

use App\Result;

interface TestingResultRepositoryInterface
{

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */ 
    public function get_point_survey_gti($survey_id, $section_id);
    public function save_report($score);
    public function get_report_survey_by_divisi($survey, $divisi);
    public function get_division_corporate($id);
    public function get_report_by_survey_filter($survey, $division);
    public function get_user_report($slug);
    public function get_user_report_by_filter($slug, $divisi);
    public function get_report_by_survey($id);
    public function average_data($id);
    



} 
