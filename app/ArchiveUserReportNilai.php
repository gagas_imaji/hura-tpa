<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchiveUserReportNilai extends Model
{
    protected $table = "archive_user_report_nilais";
    protected $guarded = ['id'];

    public $fillable = ['user_id','project_id', 'jabatan_id','aspek_id','nilai_aspek'];
}
