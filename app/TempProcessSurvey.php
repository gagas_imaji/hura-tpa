<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempProcessSurvey extends Model
{
    protected $table = 'temp_process_survey';
    public $timestamps = false;
}
