<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KriteriaPsikograph extends Model
{
    use SoftDeletes;

    protected $table = "kriteria_psikographs";
    protected $guarded = ['id'];

    public $fillable = ['nama','slug', 'minimal', 'maksimal'];

    public function user_report(){
    	return $this->hasMany("App\UserReport", "kriteria_psikograph_id");
    }
}
