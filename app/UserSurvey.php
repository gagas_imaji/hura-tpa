<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSurvey extends Model
{
    protected $table = 'user_survey';  
    

    public function survey() 
    {
        return $this->belongsTo('App\Survey', 'id_survey');
    }
}
