<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProject extends Model
{
    protected $table = 'user_project';

    protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'id_user');
    }

    public function project()
    {
    	return $this->belongsTo('App\Project', 'id_project');
    }
}
