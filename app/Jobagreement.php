<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobagreement extends Model
{
    protected $table='job_agreement';
    protected $fillable = ['id',
    					   'agreement',
    					   'pria',
    					   'wanita'];
}
