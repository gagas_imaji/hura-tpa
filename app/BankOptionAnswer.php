<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankOptionAnswer extends Model
{
    protected $table = 'bank_option_answer';
}
