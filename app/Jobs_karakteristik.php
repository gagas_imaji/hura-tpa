<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs_karakteristik extends Model
{
    protected $table='job_karakteristiks';
    protected $fillable = ['id',
    						'jobs_id',
    						'D',
    						'I',
    						'S',
    						'C'];

    public function jobs(){
    	return $this->belongsTo('App\Jobs', 'jobs_id');
    }
}
