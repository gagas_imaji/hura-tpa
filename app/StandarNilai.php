<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StandarNilai extends Model
{
    use SoftDeletes;

    protected $table = "standar_nilais";
    protected $guarded = ['id'];

    public $fillable = ['project_id', 'jabatan_id', 'nama_jabatan', 'slug_nama_jabatan', 'aspek_id', 'nilai_aspek','mandatory'];

    public function tpaAspekPsikologis(){
    	return $this->belongsTo('App\AspekPsikologis','aspek_id');
    }

    public function tpaStandarNilaiProject(){
    	return $this->hasMany('App\StandarNilaiProject','standar_nilai_id');
    }
}
