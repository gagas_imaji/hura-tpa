<?php

use App\Profile;
use App\User;
use App\Jobs;

use App\Division;
use App\RoleUser;
use App\Survey;
use App\SectionSurvey;
use App\SurveyReport;
use Carbon\Carbon;


function get_firstname(){
	$profiles = Profile::join('users', 'users.id', '=','profiles.user_id')->where('user_id', '=', Auth::user() -> id)->first();
	return $profiles['first_name'];
}

function get_firstname_by_id($id){
	$profiles = Profile::join('users', 'users.id', '=','profiles.user_id')->where('user_id', '=', $id)->first();
	return $profiles['first_name'];
}

function get_email(){
	$profiles = Profile::join('users', 'users.id', '=','profiles.user_id')->where('user_id', '=', Auth::user() -> id)->first();
	return $profiles['email'];
}

function get_email_by_id($id){
	$profiles = Profile::join('users', 'users.id', '=','profiles.user_id')->where('user_id', '=', $id)->first();
	return $profiles['email'];
}

function get_name_of_job($id){
	$data = Jobs::select('jobs_name')->where('id', '=', $id)->first();

	return $data->jobs_name;
}
function save_activity($log){
	DB::table('log_activity')->insert($log);
}

function check_project_complete($id_project, $id_user){
	$is_complete = DB::table('user_survey') -> where('id_project', '=', $id_project) -> where('id_user', '=', $id_user) -> where('is_done', '=', 0) -> get();

	if($is_complete -> count() != 0){ //masih ada yang belum done
		return false;
	}else{ // semua survey untuk project ini sudah selesai
		DB::table('user_project') -> where('id_project', '=', $id_project) -> where('id_user', '=', $id_user) -> update(['is_done' => 1]);
		return true;
	}

}

function generateRandomString($length = 6){
	$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
//====================================== SEMENTARA DUMMY TRIAL ======================================//

function create_trial_user($corp_id, $corp_name){

	//create division
	$division = new Division;
    $division -> id_corporate   = $corp_id;
    $division -> name           = "Sumber Daya Manusia";
    $division -> slug           = strtolower(str_replace(' ', '-', $division -> name));
    $division -> save();

    //create master
    // $explode 		= explode(" ", $corp_name);
    // $corp 			= strtolower($explode[0]);
    $corp 			= strtolower(str_replace(' ', '', $corp_name));
    $user 			= new User;
    $user->email 	= $corp."master@email.com";
    $user->username = (config('config.login')) ? null : $user->email;
    $user->password = bcrypt('654321');
    $user->status 	= 'active';
    $user->save();

    $profile = new Profile;
    $profile->user()->associate($user);
    // $profile->first_name = "Master ".$explode[0];
    $profile->first_name = "Master ".ucfirst($corp_name);
    $profile->id_corporate = $corp_id;
    $profile->id_division = $division -> id;
    $profile->save();


    $role 	= new RoleUser;
    $role->role_id = 3;
    $role->user_id = $user->id;
    $role->save();

    //create user staff
    // for($i = 1; $i <= 5; $i++){
    // 	$explode 		= explode(" ", $corp_name);
	   //  $corp 			= strtolower($explode[0]);
	   //  $user 			= new User;
	   //  if($i < 10){
	   //  	$user->email 	= $corp."0".$i."@email.com";
    // 	}else{
	   //  	$user->email 	= $corp.$i."@email.com";
    // 	}
	   //  $user->username = (config('config.login')) ? null : $user->email;
	   //  $user->password = bcrypt('654321');
	   //  $user->status 	= 'active';
	   //  $user->save();

	   //  $profile = new Profile;
	   //  $profile->user()->associate($user);
	   //  if($i < 10){
	   //  	$profile->first_name = "User ".$explode[0]." 0".$i;
	   //  }else{
	   //  	$profile->first_name = "User ".$explode[0]." ".$i;
	   //  }
	   //  $profile->id_corporate = $corp_id;
	   //  $profile->id_division = $division -> id;

	   //  $profile->save();

	   //  $role 	= new RoleUser;
	   //  $role->role_id = 2;
	   //  $role->user_id = $user->id;
	   //  $role->save();
    // }
}

function create_dummy_report_lai($corp_id){

	$survey_id 	= Survey::select('survey.id') -> join('profiles', 'profiles.user_id', '=', 'survey.user_id') -> where('profiles.id_corporate', '=', $corp_id) -> orderBy('survey.created_at', 'desc') -> first() -> id;
	$section 	= SectionSurvey::where('survey_id', '=', $survey_id) -> get();
	$user 		= Profile::where('id_corporate', '=', $corp_id) -> get();
	$i = 0;
	foreach ($section as $key) {
		$section_id[$i] = $key -> id;
		$i++;
	}

	$i = 0;
	foreach ($user as $key) {
		$user_id[$i] = $key -> user_id;
		$i++;
	}
	DB::table('survey_report')->insert([
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[0], 'score'=>87.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[0], 'score'=>66.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[0], 'score'=>120.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[0], 'score'=>110.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[4], 'user_id'=>$user_id[0], 'score'=>82.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[1], 'score'=>87.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[1], 'score'=>88.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[1], 'score'=>98.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[1], 'score'=>60.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[4], 'user_id'=>$user_id[1], 'score'=>78.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[2], 'score'=>118.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[2], 'score'=>116.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[2], 'score'=>98.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[2], 'score'=>110.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[4], 'user_id'=>$user_id[2], 'score'=>100.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[3], 'score'=>78.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[3], 'score'=>78.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[3], 'score'=>128.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[3], 'score'=>110.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[4], 'user_id'=>$user_id[3], 'score'=>117.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[4], 'score'=>116.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[4], 'score'=>112.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[4], 'score'=>99.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[4], 'score'=>100.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[4], 'user_id'=>$user_id[4], 'score'=>134.0],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[5], 'score'=>85.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[5], 'score'=>116.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[5], 'score'=>98.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[5], 'score'=>97.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[4], 'user_id'=>$user_id[5], 'score'=>100.00]
	]);

	// DB::table('user_report') -> where('user_id', '=', $user_id[0]) -> update([
	// 	'GTQ1' 		=> 87.00,
	// 	'GTQ2' 		=> 66.00,
	// 	'GTQ3' 		=> 120.00,
	// 	'GTQ4' 		=> 110.00,
	// 	'GTQ5' 		=> 82.00,
	// 	'kriteria'	=> 'Average'
	// ]);
	DB::table('user_report') -> where('user_id', '=', $user_id[1]) -> update([
		'GTQ1' 		=> 87.00,
		'GTQ2' 		=> 88.00,
		'GTQ3' 		=> 98.00,
		'GTQ4' 		=> 60.00,
		'GTQ5' 		=> 78.00,
		'kriteria'	=> 'Below Average'
	]);
	DB::table('user_report') -> where('user_id', '=', $user_id[2]) -> update([
		'GTQ1' 		=> 118.00,
		'GTQ2' 		=> 116.00,
		'GTQ3' 		=> 98.00,
		'GTQ4' 		=> 110.00,
		'GTQ5' 		=> 100.00,
		'kriteria'	=> 'Above Average'
	]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[3]) -> update([
	// 	'GTQ1' 		=> 78.00,
	// 	'GTQ2' 		=> 78.00,
	// 	'GTQ3' 		=> 128.00,
	// 	'GTQ4' 		=> 110.00,
	// 	'GTQ5' 		=> 117.00,
	// 	'kriteria'	=> 'Average'
	// ]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[4]) -> update([
	// 	'GTQ1' 		=> 116.00,
	// 	'GTQ2' 		=> 112.00,
	// 	'GTQ3' 		=> 99.00,
	// 	'GTQ4' 		=> 100.00,
	// 	'GTQ5' 		=> 134.0,
	// 	'kriteria'	=> 'Above Average'
	// ]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[5]) -> update([
	// 	'GTQ1' 		=> 85.00,
	// 	'GTQ2' 		=> 116.00,
	// 	'GTQ3' 		=> 98.00,
	// 	'GTQ4' 		=> 97.00,
	// 	'GTQ5' 		=> 100.00,
	// 	'kriteria'	=> 'Average'
	// ]);

	DB::table('user_survey') -> where('id_survey', '=', $survey_id) -> update(['is_done' => 1, 'updated_at' => Carbon::now()]);

}

function create_dummy_report_tiki($corp_id){

	$survey_id 	= Survey::select('survey.id') -> join('profiles', 'profiles.user_id', '=', 'survey.user_id') -> where('profiles.id_corporate', '=', $corp_id) -> orderBy('survey.created_at', 'desc') -> first() -> id;
	$section 	= SectionSurvey::where('survey_id', '=', $survey_id) -> get();
	$user 		= Profile::where('id_corporate', '=', $corp_id) -> get();
	$i = 0;
	foreach ($section as $key) {
		$section_id[$i] = $key -> id;
		$i++;
	}

	$i = 0;
	foreach ($user as $key) {
		$user_id[$i] = $key -> user_id;
		$i++;
	}

	DB::table('survey_report')->insert([
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[0], 'score'=>21.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[0], 'score'=>24.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[0], 'score'=>21.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[0], 'score'=>11.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[1], 'score'=>21.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[1], 'score'=>20.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[1], 'score'=>25.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[1], 'score'=>22.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[2], 'score'=>18.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[2], 'score'=>11.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[2], 'score'=>15.00],
		['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[2], 'score'=>16.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[3], 'score'=>21.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[3], 'score'=>23.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[3], 'score'=>22.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[3], 'score'=>14.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[4], 'score'=>14.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[4], 'score'=>21.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[4], 'score'=>21.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[4], 'score'=>19.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[0], 'user_id'=>$user_id[5], 'score'=>22.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[1], 'user_id'=>$user_id[5], 'score'=>18.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[2], 'user_id'=>$user_id[5], 'score'=>19.00],
		// ['survey_id'=> $survey_id, 'section_id'=>$section_id[3], 'user_id'=>$user_id[5], 'score'=>22.00],
	]);

	// DB::table('user_report') -> where('user_id', '=', $user_id[0]) -> update([
	// 	'SS1' 		=> 21.00,
	// 	'SS2' 		=> 24.00,
	// 	'SS3' 		=> 21.00,
	// 	'SS4' 		=> 11.00,
	// 	'jumlah' 	=> 65,
	// 	'IQ'		=> 97
	// ]);
	DB::table('user_report') -> where('user_id', '=', $user_id[1]) -> update([
		'SS1' 		=> 21.00,
		'SS2' 		=> 20.00,
		'SS3' 		=> 25.00,
		'SS4' 		=> 22.00,
		'jumlah' 	=> 78.00,
		'IQ'		=> 111
	]);
	DB::table('user_report') -> where('user_id', '=', $user_id[2]) -> update([
		'SS1' 		=> 18.00,
		'SS2' 		=> 11.00,
		'SS3' 		=> 15.00,
		'SS4' 		=> 16.00,
		'jumlah' 	=> 51,
		'IQ'		=> 81
	]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[3]) -> update([
	// 	'SS1' 		=> 21.00,
	// 	'SS2' 		=> 23.00,
	// 	'SS3' 		=> 22.00,
	// 	'SS4' 		=> 14.00,
	// 	'jumlah' 	=> 69,
	// 	'IQ'		=> 102
	// ]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[4]) -> update([
	// 	'SS1' 		=> 14.00,
	// 	'SS2' 		=> 21.00,
	// 	'SS3' 		=> 21.00,
	// 	'SS4' 		=> 19.00,
	// 	'jumlah' 	=> 64,
	// 	'IQ'		=> 96
	// ]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[5]) -> update([
	// 	'SS1' 		=> 22.00,
	// 	'SS2' 		=> 18.00,
	// 	'SS3' 		=> 19.00,
	// 	'SS4' 		=> 22.00,
	// 	'jumlah' 	=> 67,
	// 	'IQ'		=> 99
	// ]);

	DB::table('user_survey') -> where('id_survey', '=', $survey_id) -> update(['is_done' => 1, 'updated_at' => Carbon::now()]);

}

function create_dummy_report_wba($corp_id){

	$survey_id 	= Survey::select('survey.id') -> join('profiles', 'profiles.user_id', '=', 'survey.user_id') -> where('profiles.id_corporate', '=', $corp_id) -> orderBy('survey.created_at', 'desc') -> first() -> id;
	$section 	= SectionSurvey::where('survey_id', '=', $survey_id) -> get();
	$user 		= Profile::where('id_corporate', '=', $corp_id) -> get();
	foreach ($section as $key) {
		$section_id = $key -> id;
	}

	$i = 0;
	foreach ($user as $key) {
		$user_id[$i] = $key -> user_id;
		$i++;
	}

		DB::table('report_papikostick') -> insert([
			// [
			// 	'survey_id'	=> $survey_id,
			// 	'user_id'	=> $user_id[0],
			// 	'N'			=> 6,
			// 	'G'			=> 4,
			// 	'A'			=> 5,
			// 	'L'			=> 7,
			// 	'P'			=> 8,
			// 	'I'			=> 3,
			// 	'T'			=> 5,
			// 	'V'			=> 4,
			// 	'X'			=> 6,
			// 	'S'			=> 8,
			// 	'B'			=> 9,
			// 	'O'			=> 4,
			// 	'R'			=> 0,
			// 	'D'			=> 5,
			// 	'C'			=> 4,
			// 	'Z'			=> 6,
			// 	'E'			=> 2,
			// 	'K'			=> 7,
			// 	'F'			=> 6,
			// 	'W'			=> 5
			// ],
			[
				'survey_id'	=> $survey_id,
				'user_id'	=> $user_id[1],
				'N'			=> 4,
				'G'			=> 5,
				'A'			=> 5,
				'L'			=> 7,
				'P'			=> 8,
				'I'			=> 5,
				'T'			=> 7,
				'V'			=> 7,
				'X'			=> 8,
				'S'			=> 9,
				'B'			=> 2,
				'O'			=> 0,
				'R'			=> 3,
				'D'			=> 4,
				'C'			=> 6,
				'Z'			=> 8,
				'E'			=> 5,
				'K'			=> 3,
				'F'			=> 5,
				'W'			=> 7
			],
			[
				'survey_id'	=> $survey_id,
				'user_id'	=> $user_id[2],
				'N'			=> 4,
				'G'			=> 5,
				'A'			=> 7,
				'L'			=> 9,
				'P'			=> 6,
				'I'			=> 8,
				'T'			=> 4,
				'V'			=> 6,
				'X'			=> 8,
				'S'			=> 5,
				'B'			=> 3,
				'O'			=> 4,
				'R'			=> 2,
				'D'			=> 9,
				'C'			=> 0,
				'Z'			=> 1,
				'E'			=> 3,
				'K'			=> 6,
				'F'			=> 3,
				'W'			=> 4
			],
			// [
			// 	'survey_id'	=> $survey_id,
			// 	'user_id'	=> $user_id[3],
			// 	'N'			=> 2,
			// 	'G'			=> 2,
			// 	'A'			=> 3,
			// 	'L'			=> 5,
			// 	'P'			=> 7,
			// 	'I'			=> 6,
			// 	'T'			=> 8,
			// 	'V'			=> 0,
			// 	'X'			=> 9,
			// 	'S'			=> 2,
			// 	'B'			=> 4,
			// 	'O'			=> 3,
			// 	'R'			=> 6,
			// 	'D'			=> 4,
			// 	'C'			=> 7,
			// 	'Z'			=> 9,
			// 	'E'			=> 4,
			// 	'K'			=> 6,
			// 	'F'			=> 8,
			// 	'W'			=> 3
			// ],
			// [
			// 	'survey_id'	=> $survey_id,
			// 	'user_id'	=> $user_id[4],
			// 	'N'			=> 3,
			// 	'G'			=> 5,
			// 	'A'			=> 1,
			// 	'L'			=> 1,
			// 	'P'			=> 4,
			// 	'I'			=> 7,
			// 	'T'			=> 6,
			// 	'V'			=> 8,
			// 	'X'			=> 9,
			// 	'S'			=> 5,
			// 	'B'			=> 7,
			// 	'O'			=> 4,
			// 	'R'			=> 6,
			// 	'D'			=> 9,
			// 	'C'			=> 7,
			// 	'Z'			=> 5,
			// 	'E'			=> 6,
			// 	'K'			=> 3,
			// 	'F'			=> 2,
			// 	'W'			=> 8
			// ],
			// [
			// 	'survey_id'	=> $survey_id,
			// 	'user_id'	=> $user_id[5],
			// 	'N'			=> 7,
			// 	'G'			=> 4,
			// 	'A'			=> 3,
			// 	'L'			=> 2,
			// 	'P'			=> 4,
			// 	'I'			=> 3,
			// 	'T'			=> 6,
			// 	'V'			=> 3,
			// 	'X'			=> 6,
			// 	'S'			=> 8,
			// 	'B'			=> 5,
			// 	'O'			=> 7,
			// 	'R'			=> 3,
			// 	'D'			=> 1,
			// 	'C'			=> 0,
			// 	'Z'			=> 8,
			// 	'E'			=> 4,
			// 	'K'			=> 3,
			// 	'F'			=> 6,
			// 	'W'			=> 7
			// ]
		]);

	// DB::table('user_report') -> where('user_id', '=', $user_id[0]) -> update([
	// 	'N'			=> 6,
	// 	'G'			=> 4,
	// 	'A'			=> 5,
	// 	'L'			=> 7,
	// 	'P'			=> 8,
	// 	'I'			=> 3,
	// 	'T'			=> 5,
	// 	'V'			=> 4,
	// 	'X'			=> 6,
	// 	'S'			=> 8,
	// 	'B'			=> 9,
	// 	'O'			=> 4,
	// 	'R'			=> 0,
	// 	'D'			=> 5,
	// 	'C'			=> 4,
	// 	'Z'			=> 6,
	// 	'E'			=> 2,
	// 	'K'			=> 7,
	// 	'F'			=> 6,
	// 	'W'			=> 5
	// ]);
	DB::table('user_report') -> where('user_id', '=', $user_id[1]) -> update([
		'N'			=> 4,
		'G'			=> 5,
		'A'			=> 5,
		'L'			=> 7,
		'P'			=> 8,
		'I'			=> 5,
		'T'			=> 7,
		'V'			=> 7,
		'X'			=> 8,
		'S'			=> 9,
		'B'			=> 2,
		'O'			=> 0,
		'R'			=> 3,
		'D'			=> 4,
		'C'			=> 6,
		'Z'			=> 8,
		'E'			=> 5,
		'K'			=> 3,
		'F'			=> 5,
		'W'			=> 7
	]);
	DB::table('user_report') -> where('user_id', '=', $user_id[2]) -> update([
		'N'			=> 4,
		'G'			=> 5,
		'A'			=> 7,
		'L'			=> 9,
		'P'			=> 6,
		'I'			=> 8,
		'T'			=> 4,
		'V'			=> 6,
		'X'			=> 8,
		'S'			=> 5,
		'B'			=> 3,
		'O'			=> 4,
		'R'			=> 2,
		'D'			=> 9,
		'C'			=> 0,
		'Z'			=> 1,
		'E'			=> 3,
		'K'			=> 6,
		'F'			=> 3,
		'W'			=> 4
	]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[3]) -> update([
	// 	'N'			=> 2,
	// 	'G'			=> 2,
	// 	'A'			=> 3,
	// 	'L'			=> 5,
	// 	'P'			=> 7,
	// 	'I'			=> 6,
	// 	'T'			=> 8,
	// 	'V'			=> 0,
	// 	'X'			=> 9,
	// 	'S'			=> 2,
	// 	'B'			=> 4,
	// 	'O'			=> 3,
	// 	'R'			=> 6,
	// 	'D'			=> 4,
	// 	'C'			=> 7,
	// 	'Z'			=> 9,
	// 	'E'			=> 4,
	// 	'K'			=> 6,
	// 	'F'			=> 8,
	// 	'W'			=> 3
	// ]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[4]) -> update([
	// 	'N'			=> 3,
	// 	'G'			=> 5,
	// 	'A'			=> 1,
	// 	'L'			=> 1,
	// 	'P'			=> 4,
	// 	'I'			=> 7,
	// 	'T'			=> 6,
	// 	'V'			=> 8,
	// 	'X'			=> 9,
	// 	'S'			=> 5,
	// 	'B'			=> 7,
	// 	'O'			=> 4,
	// 	'R'			=> 6,
	// 	'D'			=> 9,
	// 	'C'			=> 7,
	// 	'Z'			=> 5,
	// 	'E'			=> 6,
	// 	'K'			=> 3,
	// 	'F'			=> 2,
	// 	'W'			=> 8
	// ]);
	// DB::table('user_report') -> where('user_id', '=', $user_id[5]) -> update([
	// 	'N'			=> 7,
	// 	'G'			=> 4,
	// 	'A'			=> 3,
	// 	'L'			=> 2,
	// 	'P'			=> 4,
	// 	'I'			=> 3,
	// 	'T'			=> 6,
	// 	'V'			=> 3,
	// 	'X'			=> 6,
	// 	'S'			=> 8,
	// 	'B'			=> 5,
	// 	'O'			=> 7,
	// 	'R'			=> 3,
	// 	'D'			=> 1,
	// 	'C'			=> 0,
	// 	'Z'			=> 8,
	// 	'E'			=> 4,
	// 	'K'			=> 3,
	// 	'F'			=> 6,
	// 	'W'			=> 7
	// ]);

	DB::table('user_survey') -> where('id_survey', '=', $survey_id) -> update(['is_done' => 1, 'updated_at' => Carbon::now()]);

}

function get_tingkat_kecocokan($user_id, $project_id)
{
	$user_report = \App\HasilPeserta::where(["user_id"=> $user_id, "tujuan"=> $project_id])->first();
	return ( is_null($user_report) ? null : $user_report->kecocokan);
}

function get_class_standar_nilai($aspek_id, $posisiId, $nilai)
{
	$class = "";

	$getStandarNilai   = \App\StandarNilai::where('id',$posisiId)->first();

	if (!is_null($getStandarNilai)) {
		$nilaiAspekStandar = $getStandarNilai->nilai_aspek;
		$json_decode_nilai = json_decode($nilaiAspekStandar,true);
		$standar_nilai 	   = $json_decode_nilai[$aspek_id];
		$mandatoryStandar  = $getStandarNilai->mandatory;
		$json_decode_manda = json_decode($mandatoryStandar,true);
		$mandatory 		   = $json_decode_manda[$aspek_id];

		if ($standar_nilai == $nilai) {
			if ($mandatory == 1) {
				// $class = "bg-mandatory";
				$class = "bg-requirement";
			}else{
				$class = "bg-requirement";
			}
		}
	}

	return $class;
}

function getHasilKriteria($hasilKriteria)
{
	try {
		$kriteria = App\KriteriaPsikograph::where('id',$hasilKriteria)->value('nama');
		return $kriteria;
	} catch (Exception $e) {
		return "-";
	}
}

?>
