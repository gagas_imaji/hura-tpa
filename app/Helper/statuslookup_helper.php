<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of statuslookup_helper
 *
 * @author ARIES
 */

 
    //fungsi yang nanti akan diakses di controller
    function status_schedule($param) {

        //hitung semua peserta dalam satu survey
        $data_user  = DB::table('user_survey') -> where('id_survey', '=', $param) -> get();
        $count_user = $data_user -> count();

        //hitung yang sudah selesai
        $data_user_done   = DB::table('user_survey') -> where('id_survey', '=', $param) -> where('is_done', '=', 1) -> get();
        $count_finish     = $data_user_done -> count();
        
       if($count_finish == 0){
           $dec = 0;
       }else{
           $dec = $count_finish / $count_user;
       }
        //count decimal
        
        $percent = $dec * 100;
        return $percent;
    }


    //fungsi konversi score menjadi persentase
    function convert_percent($point, $max){
      $result   = $point / $max;
      $result   = $result * 100;
      return $result;
    }

    //fungsi yang nanti akan diakses di controller
    function mean($array) {
        if (is_array($array)) {
            $count = count($array);
            $sum = array_sum($array);
            $result = $sum / $count;
            return $result;
        } else {
            return FALSE;
        }
    }

    //fungsi yang nanti akan diakses di controller
    //rumusnya (0.25*(n+1))
    //n = banyaknya data
    function quartile_low($array) {
        $n = count($array);
        $count = $n + 1;
        $result = 0.25 * $count;
        return $result;
    }

    //fungsi yang nanti akan diakses di controller
    //rumusnya (0.25*(n+1))
    //n = banyaknya data
    function quartile_high($array) {
        //list kata2 yang nantinya akan disensor
        $n = count($array);
        $count = $n + 1;
        $result = 0.75 * $count;
        return $result;
    }

    //fungsi yang nanti akan diakses di controller
    function is_engaged($x, $y) {
        /* rumus asli
          a = ((y2 - y3)*(x - x3) + (x3 - x2)*(y - y3)) / ((y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3))
          b = ((y3 - y1)*(x - x3) + (x1 - x3)*(y - y3)) / ((y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3))
          c = 1 - a - b
         * 
         */

        $x1 = 3;
        $y1 = 5;

        $x2 = 6;
        $y2 = 5;
        
        $x3 = 6;
        $y3 = 2.5;

        $a = (($y2 - $y3) * ($x - $x3) + ($x3 - $x2) * ($y - $y3)) / (($y2 - $y3) * ($x1 - $x3) + ($x3 - $x2) * ($y1 - $y3));
        $b = (($y3 - $y1) * ($x - $x3) + ($x1 - $x3) * ($y - $y3)) / (($y2 - $y3) * ($x1 - $x3) + ($x3 - $x2) * ($y1 - $y3));
        $c = 1 - $a - $b;

        if ($a >= 0 and $a <= 1 and $b >= 0 and $b <= 1 and $c >= 0 and $c <= 1) {
            $result = TRUE;
        } else {
            $result = FALSE;
        }
        return $result;
    }


