<?php
function rowDataScoreLai($score = 0){
	if($score >= 95 ){
		return "success";
	}elseif($score >=75 && $score < 95 ){
		return "warning";
	}else{
		return "danger";
	}
}
function rowDataScoreTiki($score = 0){
	if($score >= 17.5 ){
		return "success";
	}elseif($score >=12.49 && $score < 17.49 ){
		return "warning";
	}else{
		return "danger";
	}
}

function partisipasi_peserta_project($param) {

	//hitung semua peserta dalam satu survey
	$data_user  = DB::table('user_project') -> where('id_project', '=', $param) -> get();
	$count_user = $data_user -> count();

	//hitung yang sudah selesai
	$data_user_done   = DB::table('user_project') -> where('id_project', '=', $param) -> where('is_done', '=', 1) -> get();
	$count_finish     = $data_user_done -> count();

	if($count_finish == 0){
	   $dec = 0;
	}else{
	   $dec = $count_finish / $count_user;
	}
	//count decimal

	$percent = $dec * 100;
	return $percent;
}

function partisipasi_peserta_survey($param) {

    //hitung semua peserta dalam satu survey
    $data_user  = DB::table('user_survey') -> where('id_survey', '=', $param) -> get();
    $count_user = $data_user -> count();

    //hitung yang sudah selesai
    $data_user_done   = DB::table('user_survey') -> where('id_survey', '=', $param) -> where('is_done', '=', 1) -> get();
    $count_finish     = $data_user_done -> count();
    
   if($count_finish == 0){
       $dec = 0;
   }else{
       $dec = $count_finish / $count_user;
   }
    //count decimal
    
    $percent = $dec * 100;
    return $percent;
}

function gtiStatus($score = 0)
{
	if($score >= 95 )
	{
		return "gti-high";
	}elseif($score >=63 && $score < 95 ){
		return "gti-mid";
	}else{
		return "gti-low";
	}
}
function gtiPercent($score)
{
	$max = 150;
	$percent = ($score/$max)*100;
	$result = round($percent,2);
	return $result."%" ;
}
function tikiPercent($score, $max)
{
	$percent = ($score/$max)*100;
	$result = round($percent,2);
	return $result."%" ;
}

//most
function WpaMostD($score)
{
	$index = [
		'0'=>'-6.2',
		'1'=>'-5',
		'2'=>'-4',
		'3'=>'-2.5',
		'4'=>'-1.8',
		'5'=>'-1',
		'6'=>'-0.2',
		'7'=>'0.5',
		'8'=>'1',
		'9'=>'2',
		'10'=>'3',
		'11'=>'3.2',
		'12'=>'3.6',
		'13'=>'4.2',
		'14'=>'4.5',
		'15'=>'6.5',
		'16'=>'7',
		'17'=>'7.2',
		'18'=>'7.4',
		'19'=>'7.6',
		'20'=>'7.7',
		'21'=>'7.8',
	];
	$realScore = $index[$score];
	return $realScore;
}
function WpaMostI($score)
{
	$index =[
		'0'=>'-6.5',
		'1'=>'-4.5',
		'2'=>'-2.5',
		'3'=>'-1',
		'4'=>'1',
		'5'=>'3',
		'6'=>'3.2',
		'7'=>'4.5',
		'8'=>'5',
		'9'=>'6.2',
		'10'=>'6.5',
		'11'=>'7',
		'12'=>'7.1',
		'13'=>'7.2',
		'14'=>'7.3',
		'15'=>'7.4',
		'16'=>'7.5',
		'17'=>'7.6',
		'18'=>'7.7',
		'19'=>'7.8',
		'20'=>'7.9'
	];
	$realScore = $index[$score];
	return $realScore;
}
function WpaMostS($score)
{
	$index =[
		'0'=>	'-5.6',
		'1'=>	'-4.5',
		'2'=>	'-3.5',
		'3'=>	'-1.5',
		'4'=>	'-0.5',
		'5'=>	'0.5',
		'6'=>	'1',
		'7'=>	'2.5',
		'8'=>	'3',
		'9'=>	'3.6',
		'10'=>	'4.2',
		'11'=>	'4.5',
		'12'=>	'5',
		'13'=>	'6',
		'14'=>	'6.5',
		'15'=>	'6.7',
		'16'=>	'6.9',
		'17'=>	'7.1',
		'18'=>	'7.3',
		'19'=>	'7.5',
		'20'=>	'7.8',
	];
	$realScore = $index[$score];
	return $realScore;
}
function WpaMostC($score)
{
	$index =[
		'0'=>'-6.2',
		'1'=>'-4.5',
		'2'=>'-3.5',
		'3'=>'-1.5',
		'4'=>'1',
		'5'=>'2',
		'6'=>'3',
		'7'=>'4.5',
		'8'=>'5',
		'9'=>'5.5',
		'10'=>'6',
		'11'=>'6.5',
		'13'=>'7',
		'12'=>'7.2',
		'13'=>'7.3',
		'14'=>'7.4',
		'15'=>'7.5',
		'16'=>'7.6',
		'17'=>'7.8',
			
	];
	$realScore = $index[$score];
	return $realScore;
}
//least
function WpaLeastD($score)
{
	$index =[
		'0'=>'7.8',
		'1'=>'6.5',
		'2'=>'4',
		'3'=>'2.5',
		'4'=>'1',
		'5'=>'0.5',
		'6'=>'-0.5',
		'7'=>'-1',
		'8'=>'-1.5',
		'9'=>'-2.5',
		'10'=>'-3',
		'11'=>'-3.5',
		'12'=>'-5',
		'13'=>'-5.5',
		'14'=>'-6',
		'15'=>'-6.5',
		'16'=>'-7',
		'17'=>'-7.2',
		'18'=>'-7.4',
		'19'=>'-7.6',
		'20'=>'-7.8',
		
	];
	$realScore = $index[$score];
	return $realScore;
}
function WpaLeastI($score)
{
	$index = [
		'0'=>'7',
		'1'=>'5.5',
		'2'=>'3.5',
		'3'=>'2.5',
		'4'=>'0.5',
		'5'=>'-0.5',
		'6'=>'-2.5',
		'7'=>'-3.5',
		'8'=>'-5',
		'9'=>'-5.5',
		'10'=>'-6.5',
		'11'=>'-7',
		'12'=>'-7.5',
		'13'=>'-7.6',
		'14'=>'-7.63',
		'15'=>'-7.65',
		'16'=>'-7.7',
		'17'=>'-7.73',
		'18'=>'-7.75',
		'19'=>'-7.8',
	];
	$realScore = $index[$score];
	return $realScore;
}
function WpaLeastS($score)
{
	$index = [
		'0'=>'7.8',
		'1'=>'7',
		'2'=>'5.5',
		'3'=>'3.5',
		'4'=>'2.5',
		'5'=>'1',
		'6'=>'0.5',
		'7'=>'-1',
		'8'=>'-2.5',
		'9'=>'-3',
		'10'=>'-5',
		'11'=>'-5.5',
		'12'=>'-6.5',
		'13'=>'-7',
		'14'=>'-7.3',
		'16'=>'-7.5',
		'19'=>'-7.8',
	];

	$realScore = $index[$score];
	return $realScore;
}
function WpaLeastC($score)
{
	$index =[
	'0'=>'7.8',
	'1'=>'7',
	'2'=>'5',
	'3'=>'3.5',
	'4'=>'2.5',
	'5'=>'1',
	'6'=>'0.5',
	'7'=>'-0.5',
	'8'=>'-1',
	'9'=>'-2.5',
	'10'=>'-3.5',
	'11'=>'-5',
	'12'=>'-6',
	'13'=>'-6.5',
	'14'=>'-7',
	'15'=>'-7.5',
	'16'=>'-7.7',
	'17'=>'-7.8',

	];


	$realScore = $index[$score];
	return $realScore;	
}
//change
function WpaChangeD($score)
{
	$index = [
		'-20'=>'-7.5',
		'-19'=>'-7.3',
		'-18'=>'-7',
		'-17'=>'-6.8',
		'-16'=>'-6.5',
		'-15'=>'-6.35',
		'-14'=>'-6.25',
		'-13'=>'-6.15',
		'-12'=>'-6',
		'-11'=>'-5.5',
		'-10'=>'-4.5',
		'-9'=>'-3.5',
		'-8'=>'-3.25',
		'-7'=>'-3',
		'-6'=>'-2.5',
		'-5'=>'-2',
		'-4'=>'-1.5',
		'-3'=>'-1',
		'-2'=>'-0.5',
		'-1'=>'-0.25',
		'0'=>'0',
		'1'=>'0.5',
		'2'=>'0.75',
		'3'=>'1',
		'4'=>'1.25',
		'5'=>'1.5',
		'6'=>'1.75',
		'7'=>'2',
		'8'=>'3',
		'9'=>'3.5',
		'10'=>'4.5',
		'11'=>'4.75',
		'12'=>'5',
		'13'=>'5.5',
		'14'=>'6',
		'15'=>'6.5',
		'16'=>'6.7',
		'17'=>'6.9',
		'18'=>'7',
		'19'=>'7.2',
		'20'=>'7.6',
		'21'=>'7.8',

	   	];

	$realScore = $index[$score];
	return $realScore;	
}
function WpaChangeI($score)
{
	$index = [
		'-18'=>'-7.5',
		'-17'=>'-7.45',
		'-16'=>'-7.4',
		'-15'=>'-7.35',
		'-14'=>'-7.3',
		'-13'=>'-7.25',
		'-12'=>'-7.2',
		'-11'=>'-7.15',
		'-10'=>'-7',
		'-9'=>'-6.5',
		'-8'=>'-6',
		'-7'=>'-5',
		'-6'=>'-4.5',
		'-5'=>'-3.5',
		'-4'=>'-3',
		'-3'=>'-2',
		'-2'=>'-1.5',
		'-1'=>'0',
		'0'=>'0.5',
		'1'=>'1',
		'2'=>'1.5',
		'3'=>'2.5',
		'4'=>'3.5',
		'5'=>'4',
		'6'=>'5',
		'7'=>'5.5',
		'8'=>'6.5',
		'9'=>'6.75',
		'10'=>'7',
		'11'=>'7.1',
		'12'=>'7.2',
		'13'=>'7.3',
		'14'=>'7.4',
		'15'=>'7.5',
		'16'=>'7.6',
		'17'=>'7.7',
		'18'=>'7.8',
	];

	$realScore = $index[$score];
	return $realScore;
}
function WpaChangeS($score)
{

	$index = [
		'-18'=>'-7.8',
		'-17'=>'-7.7',
		'-16'=>'-7.6',
		'-15'=>'-7.5',
		'-14'=>'-7.25',
		'-13'=>'-7',
		'-12'=>'-6.8',
		'-11'=>'-6.7',
		'-10'=>'-6.5',
		'-9'=>'-5',
		'-8'=>'-4.5',
		'-7'=>'-3.5',
		'-6'=>'-3',
		'-5'=>'-2',
		'-4'=>'-1.5',
		'-3'=>'-1',
		'-2'=>'-0.5',
		'-1'=>'0',
		'0'=>'1',
		'1'=>'1.5',
		'2'=>'2',
		'3'=>'2.5',
		'4'=>'3',
		'5'=>'3.5',
		'6'=>'4',
		'7'=>'4.5',
		'8'=>'5',
		'9'=>'5.5',
		'10'=>'6',
		'11'=>'6.5',
		'12'=>'6.6',
		'13'=>'6.7',
		'14'=>'6.8',
		'15'=>'7',
		'16'=>'7.2',
		'17'=>'7.4',
		'18'=>'7.5',
		'19'=>'7.7',
		'20'=>'7.8',
	];
	$realScore = $index[$score];
	return $realScore;
}
function WpaChangeC($score)
{

	$index = [
		'-22'=>'-7.8',
		'-21'=>'-7.7',
		'-20'=>'-7.6',
		'-19'=>'-7.5',
		'-18'=>'-7.4',
		'-17'=>'-7.3',
		'-16'=>'-7.2',
		'-15'=>'-7',
		'-14'=>'-6.75',
		'-13'=>'-6.5',
		'-12'=>'-6.4',
		'-11'=>'-6.3',
		'-10'=>'-6',
		'-9'=>'-5',
		'-8'=>'-4.5',
		'-7'=>'-4',
		'-6'=>'-3.5',
		'-5'=>'-3',
		'-4'=>'-0.5',
		'-3'=>'0',
		'-2'=>'0.5',
		'-1'=>'1',
		'0'=>'1.5',
		'1'=>'2.5',
		'2'=>'3.5',
		'3'=>'4',
		'4'=>'5',
		'5'=>'6',
		'6'=>'6.5',
		'7'=>'6.6',
		'8'=>'6.7',
		'9'=>'6.8',
		'10'=>'7',
		'11'=>'7.1',
		'12'=>'7.2',
		'13'=>'7.3',
		'14'=>'7.4',
		'15'=>'7.5',
		'16'=>'7.6',
		'17'=>'7.8',

	];
	$realScore = $index[$score];
	return $realScore;
}

function filterGarisNol($value){
    return ($value > 0);
}

function filterIsTight($value){
	return ($value < 2 && $value > -2);
}

function ReverseIndexPapi($score){
	$index = [
		
		'0'=>'9',
		'1'=>'8',
		'2'=>'7',
		'3'=>'6',
		'4'=>'5',
		'5'=>'4',
		'6'=>'3',
		'7'=>'2',
		'8'=>'1',
		'9'=>'0'
	];
	$realIndex = $index[$score];
	return $realIndex;
}

function ConvertJPMIndex($score){
	$index = [
		
		'1'=>'-6',
		'2'=>'-4',
		'3'=>'-2',
		'4'=>'0',
		'5'=>'2',
		'6'=>'4',
		'7'=>'6',
		'8'=>'8'
	];
	$realIndex = $index[$score];
	return $realIndex;
}
