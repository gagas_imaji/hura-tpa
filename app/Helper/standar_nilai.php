<?php

function skorStandar($project_id,$aspek_id)
{
	$getStandarNilai   = \App\StandarNilaiProject::with('tpaProject','tpaStandarNilai')->where('project_id',$project_id)->first();
	$nilaiAspekStandar = $getStandarNilai->tpaStandarNilai->nilai_aspek;
	$json_decode_nilai = json_decode($nilaiAspekStandar,true);
	$standar_nilai 	   = $json_decode_nilai[$aspek_id];
	if (!empty($standar_nilai)) {
		return $standar_nilai;
	} else {
		return 0;
	}

}

function skorStandarNilai($standar_nilai_id,$aspek_id)
{
	$getStandarNilai   = \App\StandarNilai::where('id',$standar_nilai_id)->first();
	$nilaiAspekStandar = $getStandarNilai->nilai_aspek;
	$json_decode_nilai = json_decode($nilaiAspekStandar,true);
	$standar_nilai 	   = $json_decode_nilai[$aspek_id];
	if (!empty($standar_nilai)) {
		return $standar_nilai;
	} else {
		return 0;
	}

}

function isMandatory($standar_nilai_id,$aspek_id)
{
	$getStandarNilai   = \App\StandarNilai::where('id',$standar_nilai_id)->first();
	$nilaiAspekStandar = $getStandarNilai->nilai_aspek;
	$json_decode_nilai = json_decode($nilaiAspekStandar,true);
	$standar_nilai 	   = $json_decode_nilai[$aspek_id];
	$mandatoryStandar  = $getStandarNilai->mandatory;
	$json_decode_manda = json_decode($mandatoryStandar,true);
	$mandatory 		   = $json_decode_manda[$aspek_id];
	if ($mandatory == 1) {
		return "mandatory";
	} else {
		return "";
	}
}

function checkedMandatory($project_id,$aspek_id)
{
	$getStandarNilai   = \App\StandarNilaiProject::with('tpaStandarNilai')->where('project_id',$project_id)->first();
	$nilaiAspekStandar = $getStandarNilai->tpaStandarNilai->nilai_aspek;
	$json_decode_nilai = json_decode($nilaiAspekStandar,true);
	$standar_nilai 	   = $json_decode_nilai[$aspek_id];
	$mandatoryStandar  = $getStandarNilai->tpaStandarNilai->mandatory;
	$json_decode_manda = json_decode($mandatoryStandar,true);
	$mandatory 		   = $json_decode_manda[$aspek_id];
	return $mandatory;
}

function checkedMandatoryNilai($standar_nilai_id,$aspek_id)
{
	$getStandarNilai   = \App\StandarNilai::where('id',$standar_nilai_id)->first();
	$nilaiAspekStandar = $getStandarNilai->nilai_aspek;
	$json_decode_nilai = json_decode($nilaiAspekStandar,true);
	$standar_nilai 	   = $json_decode_nilai[$aspek_id];
	$mandatoryStandar  = $getStandarNilai->mandatory;
	$json_decode_manda = json_decode($mandatoryStandar,true);
	$mandatory 		   = $json_decode_manda[$aspek_id];
	return $mandatory;
}