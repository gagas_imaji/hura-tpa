<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuarter extends Model
{
    protected $table = 'survey_quarter';
}
