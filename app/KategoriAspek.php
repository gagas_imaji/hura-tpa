<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KategoriAspek extends Model
{
    use SoftDeletes;

    protected $table = "kategori_aspeks";
    protected $guarded = ['id'];

    public $fillable = ['nama', 'slug'];

    public function tpaAspekPsikologi(){
    	return $this->hasMany('App\AspekPsikologis','kategori_aspek_id');
    }
}
