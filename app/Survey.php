<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
	protected $fillable = ['title', 'description', 'instruction', 'thankyou_text', 'start_date', 'end_date', 'user_id'];
	protected $table = 'survey';    

	public function user_survey() 
    {
        return $this->hasMany('App\UserSurvey', 'id_survey');
    }
}
