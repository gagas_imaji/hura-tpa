<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankWPA extends Model
{
    protected $table = 'bank_wpa';
}
