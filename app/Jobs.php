<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table='jobs';
    protected $fillable = ['id',
    						'jobs_name'];

    public function jobs_karakteristik(){
    	return $this->hasMany('App\Jobs_karakteristik', 'jobs_id');
    }
}
