<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserReportAspek extends Model
{
    use SoftDeletes;

    protected $table = "user_report_aspeks";
    protected $guarded = ['id'];

    public $fillable = ['user_id','project_id', 'jabatan_id','aspek_id','nilai_aspek'];
}
