<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReport extends Model
{
    protected $table = 'user_report';


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function kriteria_psikograph()
    {
    	return $this->belongsTo('App\KriteriaPsikograph', 'kriteria_psikograph_id');
    }
}
