<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corporate extends Model
{


    protected $fillable = ['name', 'description', 'contact', 'email', 'image'];
    protected $table = 'corporate';
    /**
     * @return mixed
     */
    public function users()
    {
        return $this->hasMany('App\Profile', 'id_corporate');
    }
}
 