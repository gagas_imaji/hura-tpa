<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportWPADetail extends Model
{
    protected $table = "report_wpa_detail";

    protected $guarded = ["id"];

    public function user(){
    	return $this->belongsTo("App\User");
    }

    public function project(){
    	return $this->belongsTo("App\Project", "survey_id");
    }
}
