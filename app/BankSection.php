<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankSection extends Model
{
    protected $fillable = ['id', 'bank_survey_id', 'name', 'slug'];
	protected $table = 'bank_section';  
}
