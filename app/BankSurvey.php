<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankSurvey extends Model
{
    protected $fillable = ['title', 'description', 'instruction', 'thankyou_text', 'start_date', 'end_date', 'user_id'];
	protected $table = 'bank_survey';    
}
