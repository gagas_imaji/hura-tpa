<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AspekPsikologis extends Model
{
    use SoftDeletes;

    protected $table = "aspek_psikologis";
    protected $guarded = ['id'];

    public $fillable = ['kategori_aspek_id','nama_aspek','slug','gambaran_taraf_rendah','gambaran_taraf_tinggi','status'];

    public function tpaKategoriAspek(){
    	return $this->belongsTo('App\KategoriAspek','kategori_aspek_id');
    }

    public function tpaStandarNilai(){
    	return $this->belongsTo('App\StandarNilai','aspek_id');
    }
}
