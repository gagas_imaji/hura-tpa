<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            //update project
            $list = DB::table("project") -> get();
            foreach ($list as $row) {
                $persentase = partisipasi_peserta_project($row->id);

                //status berdasarkan periode
                if (date("Y-m-d h:i:s") < $row->start_date) {
                    DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 0]);
                }elseif(date("Y-m-d h:i:s") >= $row->start_date && date("Y-m-d h:i:s") <= $row->end_date){
                    //status berdasarkan partisipasi
                    if ($persentase == 100) {
                        DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 1]);
                    }else{
                        DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 2]);
                    }
                }elseif(date("Y-m-d h:i:s") > $row->end_date){
                    DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 1]);
                }
            }

            //update survey
            $list = DB::table("survey") -> get();
            foreach ($list as $row) {
                $persentase = status_schedule($row->id);
                if ($persentase == 100) {
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 1]);
                }elseif ($persentase > 0 && $persentase < 100) {
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 2]);
                }
                if ($row->end_date < date("Y-m-d H:i:s")) {
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 1]);
                }
            }

        })->everyMinute();


    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
