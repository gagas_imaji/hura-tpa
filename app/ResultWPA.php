<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultWPA extends Model
{
    protected $table = 'result_wpa'; 


    protected $fillable = [
    	'survey_id',
    	'number',
    	'M',
    	'L',
    	'user_id',
    ];
}
