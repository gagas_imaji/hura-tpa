<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchiveUserReportAnalisis extends Model
{
    protected $table = "archive_user_report_analises";
    protected $guarded = ['id'];

    public $fillable = ['user_id','project_id','jabatan_id','user_report_id','kecocokan','kriteria_psikograph_id',
    					'uraian_kepribadian','kekuatan','kelemahan','karakteristik_pekerjaan'];
}
