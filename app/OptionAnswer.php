<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptionAnswer extends Model
{
	protected $fillable = ['id_user', 'id_answer', 'timer'];
    protected $table = 'option_answer';

    public function corporate() 
    {
        return $this->belongsTo('App\Question', 'id');
    }

     public function result() 
    {
        return $this->hasMany('App\Result', 'id_answer');
    }
}
