<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DefaultStandarNilaiProject extends Model
{
    use SoftDeletes;

    protected $table = "default_standar_nilai_projects";
    protected $guarded = ['id'];

    public $fillable = ['project_id', 'jabatan_id', 'nama_jabatan', 'aspek_id', 'nilai_aspek','mandatory'];
}
