<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterJobAnalysis extends Model
{
    protected $table = 'master_job_analysis';
}
