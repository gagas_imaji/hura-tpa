<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyReport extends Model
{
    protected $table = 'survey_report';
    
    public function survey() 
    {
        return $this->belongsTo('App\Survey', 'id_survey');
    }
}
