<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompleteTools extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $data;
     public function __construct($data)
     {
         $this->data = $data;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
     public function build()
     {
         $get = $this->data;
         return $this->view($get["view"])->from('maybank-asessment-tools@talentlytica.com', 'Maybank-Assessment')->subject($get["subject"])->with(["user"=>$get["user"],"project"=>$get["project"]]);

     }
}
