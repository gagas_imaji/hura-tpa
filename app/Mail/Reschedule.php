<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reschedule extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $data;
     public function __construct($data)
     {
         $this->data = $data;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $get = $this->data;
        return $this->view($get["view"])->from('info@talentlytica.com', 'Human Resource Assessment')->subject($get["subject"])->with([
                "token"=>$get["token"], 
                "project"=>$get["project_name"],
                "start_date"=>$get["start_date"],
                "end_date"=>$get["end_date"],
                "corporate_name"=>$get["corporate_name"]
            ]);

    }
}
