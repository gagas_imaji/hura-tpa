<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';

    protected $guarded = ["id"];

    public function userProjects()
    {
    	return $this->hasMany('App\UserProject', 'id_project');
    }
}
