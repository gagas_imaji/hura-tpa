<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StandarNilaiProject extends Model
{
    use SoftDeletes;

    protected $table = "standar_nilai_projects";
    protected $guarded = ['id'];

    public $fillable = ['project_id', 'standar_nilai_id'];

    public function tpaProject(){
    	return $this->belongsTo('App\Project','project_id');
    }

    public function tpaStandarNilai(){
    	return $this->belongsTo('App\StandarNilai','standar_nilai_id');
    }
}
