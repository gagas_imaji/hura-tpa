<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GTIJobPreferences extends Model
{
    protected $table = 'gti_job_preferences';
}
