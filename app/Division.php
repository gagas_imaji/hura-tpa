<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = ['name', 'slug'];
    protected $table = 'division';
    /**
     * @return mixed
     */
    public function users()
    {
        return $this->hasMany('App\Profile', 'id_division');
    }
}
