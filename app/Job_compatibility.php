<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_compatibility extends Model
{
    protected $table='job_compatibility';
    protected $fillable = ['id',
    					   'job_profil',
    					   'job_personal',
    					   'kriteria_value'];
}
