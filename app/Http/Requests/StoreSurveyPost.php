<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSurveyPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if('category' != 5){
            return [
                'title' => 'required',
                'description' => 'required',
                'instruction' => 'required',
                'thankyou' => 'required',
                'started' => 'required',
                'ended' => 'required',
                'section' => 'required',
                'participant' => 'required|array',
            ];    
        }else{
            return [
                'started' => 'required',
                'ended' => 'required',
                'participant' => 'required|array',
            ]; 
        }
        
    }
}
