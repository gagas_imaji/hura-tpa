<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Talenthub\Repository\TestingSurveyRepositoryInterface;
use App\Talenthub\Repository\TestingQuestionRepositoryInterface;
use App\Talenthub\Repository\TestingOptionAnswerRepositoryInterface;
use App\Talenthub\Repository\TestingResultRepositoryInterface;
use App\Survey;
use App\SurveyCategory;
use App\SurveyQuarter;
use App\Question;
use App\Result;
use App\Profile;
use App\Division;
use App\OptionSection;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
use Validator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;
class TestingController extends Controller
{
    public function __construct(TestingSurveyRepositoryInterface $survey, TestingQuestionRepositoryInterface $question, TestingOptionAnswerRepositoryInterface $option_answer, TestingResultRepositoryInterface $result)
    {
        $this -> survey         = $survey;
        $this -> question       = $question;
        $this -> option_answer  = $option_answer;
        $this -> result         = $result;
    }

    public function index(){

        $wba  = $this -> survey -> get_slug_section(4);
        $data['wba']['slug'] = $wba['slug'];
        $data['wba']['id_section'] = $wba['id_section'];

        $lai  = $this -> survey -> get_slug_section(1);
        $data['lai']['slug'] = $lai['slug'];
        $data['lai']['id_section'] = $lai['id_section'];


        $tiki  = $this -> survey -> get_slug_section(2);
        $data['tiki']['slug'] = $tiki['slug'];
        $data['tiki']['id_section'] = $tiki['id_section'];
        return view('testing.home', compact('data'));
    }

    public function load_by_section($slug){
        $data['slug'] = $slug;
        $info_survey = $this -> survey -> get_info_survey_by_slug($slug);
        $section = $this -> survey -> get_section_survey($info_survey['id']);
        $i = 0;
        foreach ($section as $key) {
            $data['section'][$i]['name'] = $key['name'];
            $data['section'][$i]['id'] = $key['id'];
            $i++;
        }
        if($info_survey['category_survey_id'] == 1){
            return view('testing.save-section', compact('data'));
        }elseif($info_survey['category_survey_id'] == 2){
            return view('testing.load-section', compact('data'));
        }
    }

    public function saveToReportPapi($slug, $section){

        $ignore_id      = ["-1"];
        $login_id       = 0;
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $thankyou_text  = $info_survey['thankyou_text'];
        $id_survey      = $info_survey['id'];

        if(in_array($login_id, $ignore_id))
        {
            $thankyou_text  = $thankyou_text;
            return view('survey.finish', compact('thankyou_text'));
        }

        $result = $this -> result -> get_result_papi($id_survey);
        $data['user_id']    = 0;
        $data['survey_id']  = $id_survey;
        $data['N'] = 9;
        $data['G'] = 0;
        $data['A'] = 7;
        $data['L'] = 2;
        $data['P'] = 7;
        $data['I'] = 2;
        $data['T'] = 3;
        $data['V'] = 4;
        $data['X'] = 6;
        $data['S'] = 5;
        $data['B'] = 7;
        $data['O'] = 4;
        $data['R'] = 5;
        $data['D'] = 8;
        $data['C'] = 5;
        $data['Z'] = 6;
        $data['E'] = 6;
        $data['K'] = 3;
        $data['F'] = 4;
        $data['W'] = 8;

        //proses update data pada tabel user_report
        $this -> result -> update_user_report('N', $data['N']);
        $this -> result -> update_user_report('G', $data['G']);
        $this -> result -> update_user_report('A', $data['A']);
        $this -> result -> update_user_report('L', $data['L']);
        $this -> result -> update_user_report('P', $data['P']);
        $this -> result -> update_user_report('I', $data['I']);
        $this -> result -> update_user_report('T', $data['T']);
        $this -> result -> update_user_report('V', $data['V']);
        $this -> result -> update_user_report('X', $data['X']);
        $this -> result -> update_user_report('S', $data['S']);
        $this -> result -> update_user_report('B', $data['B']);
        $this -> result -> update_user_report('O', $data['O']);
        $this -> result -> update_user_report('R', $data['R']);
        $this -> result -> update_user_report('D', $data['D']);
        $this -> result -> update_user_report('C', $data['C']);
        $this -> result -> update_user_report('Z', $data['Z']);
        $this -> result -> update_user_report('E', $data['E']);
        $this -> result -> update_user_report('K', $data['K']);
        $this -> result -> update_user_report('F', $data['F']);
        $this -> result -> update_user_report('W', $data['W']);

        //simpan data ke report_papikostick
        $this -> result -> save_report_papi($data);

        $this -> survey -> survey_complete($slug);
        $duration = $this -> survey -> get_temp($id_survey);
        $this -> survey -> delete_temp_survey($slug);

        $log['activity']        = 'Menyelesaikan survey '.$info_survey['title'];
        $log['user_id']         = 0;
        $log['reference_id']    = $id_survey;
        $log['ip']              = \Request::ip();
        $log['duration']        = $duration;
        $log['created_at']      = Carbon::now('Asia/Jakarta');
        $log['user_agent']      = \Request::header('User-Agent');
        save_activity($log);

        Cache::flush();
        return redirect('/testing-link')->withSuccess('Berhasil');
        // return view('survey.finish', compact('thankyou_text'));
    }

    public function saveToReport($slug, $section){


        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $thankyou_text  = $info_survey['thankyou_text'];
        $survey_id      = $info_survey['id'];
        $survey_category= $info_survey['category_survey_id'];
        $info_section   = $this -> survey -> get_info_section($section);
        $section_name   = $info_section['name'];
        $new_section    = 0;
        if($survey_category == 1){
            $ignore_id   = ["-1"];
            $login_id = 0;

            if(in_array($login_id, $ignore_id))
            {
                $point = 1;
            }else{

                // $point = $this -> result -> get_point_survey_gti($survey_id, $section);
                $point['sum'] = 30;
                $point['count'] = 50;
            }
            // if($point->count() != 0){
            if(is_array($point)){
                // foreach($point['data'] as $key){
                    $score['survey_id']     = $survey_id;
                    $score['section_id']    = $section;
                    $score['user_id']       = 0;

                    //perhitungan score GTI
                    $true   = $point['sum'];
                    $false  = $point['count'] - $point['sum'];
                    /*  
                        rumus adj setiap subtest :
                        adj = jumlah benar - (jumlah salah / opsi - 1)
                    */
                    if($section_name == 'Subtest 1'){ //letter check
                        $adj = round($true - ($false/4));
                        if($adj < 0){ //batas minimum
                            $gtq = 74;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq1', $adj);
                            $gtq = $tq -> tq;
                        }
                    }elseif($section_name == 'Subtest 2'){ //reasoning
                        $adj = round($true - ($false/2));
                        if($adj < 0){
                            $gtq = 69;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq2', $adj);
                            $gtq = $tq -> tq;
                        }
                    }elseif($section_name == 'Subtest 3'){ //letter distance
                        $adj = round($true - ($false/1));
                        if($adj < 0){
                            $gtq = 69;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq3', $adj);
                            $gtq = $tq -> tq;
                        }
                    }elseif($section_name == 'Subtest 4'){ //numerical
                        $adj = round($true - ($false/2));
                        if($adj < 0){
                            $gtq = 69;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq4', $adj);
                            $gtq = $tq -> tq;
                        }
                    }elseif($section_name == 'Subtest 5'){ //spatial
                        $adj = round($true - ($false/3));
                        if($adj < 0){
                            $gtq = 69;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq5', $adj);
                            $gtq = $tq -> tq;
                        }
                    }

                    $sum            = $gtq;
                    $score['score'] = $sum;
                // }
            }else{ //jika waktu habis dan user tidak menjawab sama sekali, maka skor diambil yang terendah
                $score['survey_id']     = $survey_id;
                $score['section_id']    = $section;
                $score['user_id']       = 0;
                $score['score']         = 69;
            }
            //update data di table user_report
            if($section_name == 'Subtest 1'){ //letter check
                $this -> result -> update_user_report('GTQ1', $score['score']);
            }elseif($section_name == 'Subtest 2'){ //reasoning
                $this -> result -> update_user_report('GTQ2', $score['score']);
            }elseif($section_name == 'Subtest 3'){ //letter distance
                $this -> result -> update_user_report('GTQ3', $score['score']);
            }elseif($section_name == 'Subtest 4'){ //numerical
                $this -> result -> update_user_report('GTQ4', $score['score']);
            }elseif($section_name == 'Subtest 5'){ //spatial
                $this -> result -> update_user_report('GTQ5', $score['score']);
            }
        }elseif($survey_category == 2){

            //hitung jumlah jawaban benar dari tabel result

            $ignore_id   = ["0"];
            $login_id = 0;
            if(in_array($login_id, $ignore_id))
            {
                $point = 1;
            }else{
               $point = $this -> result -> get_point_survey_tiki($survey_id, $section, $section_name);
            }

            if($section_name == 'Berhitung Angka'){
                $column = 'subtest1';
            }elseif($section_name == 'Gabungan Bagian'){
                $column = 'subtest2';
            }elseif($section_name == 'Hubungan Kata'){
                $column = 'subtest3';
            }elseif($section_name == 'Abstraksi Non Verbal'){
                $column = 'subtest4';
            }

            //lookup standar score berdasarkan section dan jumlah jawaban benar
            $standar_score = $this -> result -> get_standard_score_tiki($column, $point);
            foreach ($standar_score as $std) {
                $score['score']         = $std;
            }

            $score['survey_id']     = $survey_id;
            $score['section_id']    = $section;
            $score['user_id']       = 0;

            //update data di table user_report
            if($section_name == 'Berhitung Angka'){ //letter check
                $this -> result -> update_user_report('SS1', $score['score']);
            }elseif($section_name == 'Gabungan Bagian'){ //reasoning
                $this -> result -> update_user_report('SS2', $score['score']);
            }elseif($section_name == 'Hubungan Kata'){ //letter distance
                $this -> result -> update_user_report('SS3', $score['score']);
            }elseif($section_name == 'Abstraksi Non Verbal'){ //spatial
                $this -> result -> update_user_report('SS4', $score['score']);
            }

        }

        //proses masukkan hasil score ke tabel report
        if(in_array($login_id, $ignore_id))
        {
            $point = 0;
        }else{
            $save = $this -> result -> save_report($score);
            if($save == true){
                $message = 'Berhasil';
            }else{
                $message = 'Subtest ini sudah pernah disimpan';
            }
        }

        //cek section berikutnya
        $next_section = $this -> survey -> get_next_section($slug, $section);

        //jika masih ada section berikutnya, ambil id_section berikutnya sebagai new_section
        foreach ($next_section as $next) {
            $new_section = $next['id'];
            $instruction = $next['instruction'];
        }

        if($new_section == 0){ //jika sudah tidak ada section berikutnya

            $score_final = $this -> result -> get_final_score_user($survey_id);

            //ambil grade jika ada grade
            $is_grade = $this -> survey -> check_grade_survey($survey_id);

            //update grade user jika ada
            if($is_grade == true){
                $grade = $this -> survey -> get_grade($score_final, $survey_id);
                $this -> survey -> update_grade_user($survey_id, $grade, $score_final);
            }

            //update data di tabel user_report
            if(in_array($login_id, $ignore_id))
            {

            }else{

                $resume     = $this -> result -> get_report_gti_tiki($survey_id, 0);
                if($survey_category == 1){ //GTI, update kriteria
                    $avg        = round(array_sum($resume)/count($resume), 1);
                    $criteria   = $this -> result -> get_criteria_gti_report($avg);
                    $this -> result -> update_user_report('kriteria', $criteria);
                }elseif($survey_category == 2){
                    $total  = array_sum($resume);
                    if($total <= 29){ //batas bawah
        				$iq = 56;
        			}elseif($total > 107){ //batas atas
        				$iq = 145;
        			}else{ //lookup
        				$iq     = $this -> result -> get_iq_tiki($total);
        			}
                    $this -> result -> update_user_report('jumlah', $total);
                    $this -> result -> update_user_report('IQ', $iq);
                }

                $this -> survey -> survey_complete($slug);
                $duration   = $this -> survey -> get_temp($survey_id);
                $this -> survey -> delete_temp_survey($slug);
            }
            $is_back    = false;
        }else{
            $duration   = $this -> survey -> get_temp($survey_id);
            $this -> survey -> delete_temp_survey($slug);
            $is_back    = false; //untuk keperluan jika user klik back ketika selesai suatu section
        }
        Cache::flush();
        if($is_back == false){

            $log['activity']        = 'Menyelesaikan section '.$section_name;
            $log['user_id']         = 0;
            $log['reference_id']    = $survey_id;
            $log['ip']              = \Request::ip();
            $log['duration']        = $duration;
            $log['created_at']      = Carbon::now('Asia/Jakarta');
            $log['user_agent']      = \Request::header('User-Agent');
            save_activity($log);


            if($new_section == 0){
                $data['finish']         = 'finish';
                $data['slug']           = $slug;
                
                $log['activity']        = 'Menyelesaikan survey '.$info_survey['title'];
                $log['user_id']         = 0;
                $log['reference_id']    = $survey_id;
                $log['ip']              = \Request::ip();
                $log['duration']        = $duration;
                $log['created_at']      = Carbon::now('Asia/Jakarta');
                $log['user_agent']      = \Request::header('User-Agent');
                save_activity($log);
                $thankyou_text  = $thankyou_text;
                return view('survey.finish', compact('thankyou_text'));
            }else{
                $data['finish']         = 'next';
                $data['new_section']    = $new_section;
                $data['slug']           = $slug;
            }
            return redirect('/testing-load-section'.'/'.$slug)->withSuccess($message);;
        }else{
            return redirect('/start-next-section'.'/'.$slug.'/'.$new_section);
        }

    }

    public function getQuestion($slug, $section){
            $id_survey          = $this -> survey -> get_id_survey_by_slug($slug);
            $used_timer         = $this -> survey -> get_temp($id_survey);
            $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
            $id_category_survey = $info_survey['category_survey_id'];
            $expiresAt          = Carbon::now()->addMinutes(60);
            //cek jika survey merupakan kategory WPA, maka panggil fungsi test WPA. jika Papikostick, maka panggil fungsi Papikostic
            if($id_category_survey == 3){
                return redirect('survey/get-question-wpa/'.$slug.'/'.$section);
            }elseif($id_category_survey == 4){
                return redirect('survey/get-question-papi/'.$slug.'/'.$section);
            }else{

                //untuk keperluan progress bar
                $total          = $this -> question -> get_total_question_on_section($section);
                if(Cache::has('result-count-'.$slug.'-0')){
                    $count = Cache::get('result-count-'.$slug.'-0');
                }else{
                    Cache::put('result-count-'.$slug.'-0', 0, $expiresAt);
                    $count = Cache::get('result-count-'.$slug.'-0');
                }
                // $count          = $this -> result -> get_count_answered($section);
                //ambil info section.
                $section_info   = $this -> survey -> get_info_section($section);
                $instruction    = $section_info['instruction'];
                $section_name   = $section_info['name'];
                $section_slug   = $section_info['slug'];
                $folder_image   = '';
                for($i = 0; $i < strlen($section_slug)-11; $i++){
                    $folder_image .= $section_slug[$i];
                }
                $section_limit  = $section_info['limit'];

                //ambil pertanyaan selanjutnya berdasarkan section_name
                if($section_info['name'] == 'Reading Text'){
                    $questions = $this -> question -> get_question_survey_reading($slug, $section, $section_limit, $total);
                }elseif($section_info['name'] == 'Matching Question'){
                    $questions = $this -> question -> get_question_survey_matching($slug, $section, $section_limit, $total);
                }elseif($section_info['name'] == 'Blank in Paragraph'){
                    $questions = $this -> question -> get_question_survey_blank_in_paragraph($slug, $section, $section_limit, $total);
                }else{
                    $questions = $this -> question -> get_question_survey($slug, $section, $section_limit, $total);
                }

                //jika ada limit, dan limit < total soal yang ada maka batasi kemunculan soal sampai sebanyak limit, jika limit > total soal yang ada, maka dianggap limit nol
                if($section_limit != 0 && $section_limit < $total){ //jika section di limit
                    //jika jumlah soal yang dijawab belum sama dengan limit, maka munculkan soal seperti biasa
                    if($count < $section_limit){

                        if($section_info['name'] == 'Reading Text'){
                            foreach ($questions as $key) {
                                $id_type_question   = $key -> id_type_question;
                                $id_question        = $key -> id_question;
                                $is_random          = $key -> is_random;
                                $paragraph          = $key -> paragraph;
                                $timer              = $key -> timer;
                                $timer_survey       = $key -> timer_survey;
                                $timer_section      = $key -> timer_section;

                                $option_answer[$id_question] = $this -> option_answer -> get_by_question_unhide($id_question, $is_random);
                            }

                            return view('survey.detail_reading', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'paragraph', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
                        }elseif($section_info['name'] == 'Matching Question'){
                            $i = 0;
                            foreach ($questions as $key) {
                                $id_type_question   = $key -> id_type_question;
                                $id_question        = $key -> id_question;
                                $is_random          = $key -> is_random;
                                $timer              = $key -> timer;
                                $timer_survey       = $key -> timer_survey;
                                $timer_section      = $key -> timer_section;
                                $arr_id_question[$i]= $key -> id_question;
                                $i++;
                            }

                            $option_answer = $this -> option_answer -> get_option_matching($section, $is_random, $section_limit, $total, $arr_id_question);

                            return view('survey.detail_matching', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
                        }else{
                            $total = $section_limit; //untuk progress bar
                            $i = 0;

                            //check cache
                            if(Cache::has('cache-'.$slug)){
                                $timer                  = Cache::get('timer-'.$slug.'-0');
                                $timer_survey           = Cache::get('timer-survey-'.$slug.'-0');
                                $timer_section          = Cache::get('timer-section-'.$slug.'-0');
                                $survey_title           = Cache::get('survey-title-'.$slug.'-0');
                                $id_question            = Cache::get('id-question-'.$slug.'-0');
                                $content_question       = Cache::get('question-'.$slug.'-0');
                                $option_answer          = Cache::get('option-answer-'.$slug.'-0');
                            }else{
                                foreach ($questions as $key) {

                                    $timer              = $key -> timer;
                                    $timer_survey       = $key -> timer_survey;
                                    $timer_section      = $key -> timer_section;
                                    $survey_title       = $key -> survey_title;
                                    $id_questions[$i]   = $key -> id_question;
                                    $question[$i]['question']           = $key -> question;
                                    $question[$i]['id_type_question']   = $key -> id_type_question;
                                    $question[$i]['is_mandatory']       = $key -> is_mandatory;
                                    $option[$i]         = $this -> option_answer -> get_by_question_unhide($key -> id_question, $key -> is_random);

                                    $i++;
                                }
                                //setting cache
                                Cache::put('cache-'.$slug.'-0', 'yes', $expiresAt);
                                Cache::put('timer-'.$slug.'-0', $timer, $expiresAt);
                                Cache::put('timer-survey-'.$slug.'-0', $timer_survey, $expiresAt);
                                Cache::put('timer-section-'.$slug.'-0', $timer_section, $expiresAt);
                                Cache::put('survey-title-'.$slug.'-0', $survey_title, $expiresAt);

                                Cache::put('id-question-'.$slug.'-0', $id_questions, $expiresAt);
                                Cache::put('question-'.$slug.'-0', $question, $expiresAt);
                                Cache::put('option-answer-'.$slug.'-0', $option, $expiresAt);

                                Cache::put('result-slug-'.$slug.'-0', $slug, $expiresAt);
                                Cache::put('result-section-'.$slug.'-0', $section, $expiresAt);
                                Cache::put('result-section-name-'.$slug.'-0', $section_name, $expiresAt);
                                $content_question   = Cache::get('question-'.$slug.'-0');
                                $id_question        = Cache::get('id-question-'.$slug.'-0');
                                $option_answer      = Cache::get('option-answer-'.$slug.'-0');
                            }
                            if($section_info['name'] == 'Blank in Paragraph'){

                                return view('testing.detail_blank_paragraph', compact('id_survey', 'slug', 'questions', 'used_timer', 'section', 'count', 'total', 'section_name', 'section_slug', 'instruction', 'timer', 'timer_survey', 'timer_section'));
                            }else{

                                return view('testing.detail', compact('id_survey', 'slug', 'questions', 'option_answer', 'used_timer', 'section', 'count', 'total', 'section_name', 'section_slug', 'instruction', 'timer', 'timer_survey', 'timer_section', 'content_question', 'survey_title', 'id_question', 'id_type_question', 'folder_image'));
                            }

                        }
                    }else{ //jika jumlah soal yang dijawab sudah sama dengan limit, maka masuk ke section selanjutnya
                        return redirect('survey/save-report/'.$slug.'/'.$section);
                    }
                }elseif($section_limit == 0 || $section_limit >= $total){ //jika section ga di limit

                    //jika pertanyaan sudah dijawab semua, maka simpan score ke tabel report
                    if($questions -> count() == 0){
                        $new_section = 0;

                        //cek section berikutnya
                        $next_section = $this -> survey -> get_next_section($slug, $section);

                        //jika masih ada section berikutnya, ambil id_section berikutnya sebagai new_section
                        foreach ($next_section as $next) {
                            $new_section = $next['id'];
                            $instruction = $next['instruction'];
                        }

                        if($new_section == 0){

                            return redirect('/go-finish'.'/'.$slug);
                        }else{
                            return redirect('/start-next-section'.'/'.$slug.'/'.$new_section);
                        }
                        // return view('survey/save-report/'.$slug.'/'.$section);
                    }else{

                        if($section_info['name'] == 'Reading Text'){
                            foreach ($questions as $key) {
                                $id_type_question   = $key -> id_type_question;
                                $id_question        = $key -> id_question;
                                $is_random          = $key -> is_random;
                                $paragraph          = $key -> paragraph;
                                $timer              = $key -> timer;
                                $timer_survey       = $key -> timer_survey;
                                $timer_section      = $key -> timer_section;

                                $option_answer[$id_question] = $this -> option_answer -> get_by_question_unhide($id_question, $is_random);
                            }

                            return view('survey.detail_reading', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'paragraph', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
                        }elseif($section_info['name'] == 'Matching Question'){
                            $i = 0;
                            foreach ($questions as $key) {
                                $id_type_question   = $key -> id_type_question;
                                $id_question        = $key -> id_question;
                                $is_random          = $key -> is_random;
                                $timer              = $key -> timer;
                                $timer_survey       = $key -> timer_survey;
                                $timer_section      = $key -> timer_section;
                                $arr_id_question[$i]= $key -> id_question;
                                $i++;
                            }
                            $option_answer = $this -> option_answer -> get_option_matching($section, $is_random, $section_limit, $total, $arr_id_question);

                            return view('survey.detail_matching', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
                        }elseif($section_info['name'] == 'Blank in Paragraph'){
                            $i = 0;
                            foreach ($questions as $key) {
                                $id_type_question   = $key -> id_type_question;
                                $id_question        = $key -> id_question;
                                $is_random          = 1;
                                $timer              = $key -> timer;
                                $timer_survey       = $key -> timer_survey;
                                $timer_section      = $key -> timer_section;
                                $arr_id_question[$i]= $key -> id_question;
                                $i++;
                            }
                            $option_answer = $this -> option_answer -> get_option_matching($section, $is_random, $section_limit, $total, $arr_id_question);

                            return view('survey.detail_blank_paragraph', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
                        }else{
                            $i = 0;

                            //check cache, jika ada maka ambil cache simpan di variabel
                            if(Cache::has('cache-'.$slug)){
                                $timer                  = Cache::get('timer-'.$slug.'-0');
                                $timer_survey           = Cache::get('timer-survey-'.$slug.'-0');
                                $timer_section          = Cache::get('timer-section-'.$slug.'-0');
                                $survey_title           = Cache::get('survey-title-'.$slug.'-0');
                                $id_question            = Cache::get('id-question-'.$slug.'-0');
                                $content_question       = Cache::get('question-'.$slug.'-0');
                                $option_answer          = Cache::get('option-answer-'.$slug.'-0');
                            }else{
                                foreach ($questions as $key) {
                                    $timer              = $key -> timer;
                                    $timer_survey       = $key -> timer_survey;
                                    $timer_section      = $key -> timer_section;
                                    $survey_title       = $key -> survey_title;
                                    $id_questions[$i]   = $key -> id_question;
                                    $question[$i]['question']           = $key -> question;
                                    $question[$i]['id_type_question']   = $key -> id_type_question;
                                    $question[$i]['is_mandatory']       = $key -> is_mandatory;
                                    $option[$i]         = $this -> option_answer -> get_by_question_unhide($key -> id_question, $key -> is_random);
                                    $i++;
                                }
                                //setting cache
                                Cache::put('cache-'.$slug.'-0', 'yes', $expiresAt);
                                Cache::put('timer-'.$slug.'-0', $timer, $expiresAt);
                                Cache::put('timer-survey-'.$slug.'-0', $timer_survey, $expiresAt);
                                Cache::put('timer-section-'.$slug.'-0', $timer_section, $expiresAt);
                                Cache::put('survey-title-'.$slug.'-0', $survey_title, $expiresAt);

                                Cache::put('id-question-'.$slug.'-0', $id_questions, $expiresAt);
                                Cache::put('question-'.$slug.'-0', $question, $expiresAt);
                                Cache::put('option-answer-'.$slug.'-0', $option, $expiresAt);

                                Cache::put('result-slug-'.$slug.'-0', $slug, $expiresAt);
                                Cache::put('result-section-'.$slug.'-0', $section, $expiresAt);
                                Cache::put('result-section-name-'.$slug.'-0', $section_name, $expiresAt);
                                $id_question        = Cache::get('id-question-'.$slug.'-0');
                                $content_question   = Cache::get('question-'.$slug.'-0');
                                $option_answer      = Cache::get('option-answer-'.$slug.'-0');
                            }
                            return view('testing.detail', compact('id_survey', 'slug', 'questions', 'option_answer', 'used_timer', 'section', 'count', 'total', 'section_name', 'section_slug', 'instruction', 'timer', 'timer_survey', 'timer_section', 'survey_title', 'content_question', 'id_question', 'folder_image'));
                        }
                    }
                }
            }
    }

}
