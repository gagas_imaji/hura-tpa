<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use DB;
use Nexmo;
use Exception;
use App\Notifications\TwoFactorAuth;


use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Survey;
use App\User;
use App\SurveyCategory;
use App\UserSurvey;
use App\Profile;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Library\report_survey;

use Dingo\Api\Routing\Helpers;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\ValidationHttpException;
class ToolsController extends Controller
{
    	public function __construct(SurveyRepositoryInterface $survey, ResultRepositoryInterface $result, QuestionRepositoryInterface $question)
	    {
	        $this -> survey         = $survey;
	        $this -> result         = $result;
	        $this -> question       = $question;
	    }

	    public function getQuestionPapi(Request $request)
	    {
	    	$post 		= $request->all();
	    	$email 		= $post["email"];
	    	$survey_id 	= $post["survey_id"];
	    	$corporate_id= $post["corporate_id"];
	    	$divisi_id 	= $post["divisi_id"];
	    	$gender		= $post['gender'];
	    	$user 		= DB::table("users") -> where("email",$email) -> first();
	    	$survey_info= DB::table('survey') -> where('id', '=', $survey_id) -> first();
	    	$slug 		= $survey_info -> slug;
	    	if(is_null($user))
	    	{
	    		$user = new User;
	    		$user -> email = $email;
	    		$user -> password = bcrypt('654321');
	    		$user -> status = 'active';
	    		$user -> save();	

	    		$profile = new Profile;
	    		$profile -> first_name 	= $email;
	    		$profile -> user_id 	= $user -> id;
	    		$profile -> id_corporate= $corporate_id;
	    		$profile -> id_division	= $divisi_id;
	    		$profile -> gender		= $gender;
	    		$profile -> save();

	    		//proses simpan id user ke tabel user_survey
	    		$user_survey = new UserSurvey;
	    		$user_survey -> id_survey = $survey_id;
	    		$user_survey -> id_user   = $user -> id;
	    		$user_survey -> save();

	    	}

	    	$data['user_id']		= $user -> id;
	    	$data['email']			= $email;
	    	$section 	= DB::table('section_survey') -> where('survey_id', '=', $survey_id) -> first();
	    	$section_id = $section -> id;
	    	//ambil info section.
	        $section_info   		= $this -> survey -> get_info_section($section_id);
	        $data['instruction']    = $section_info['instruction'];
	        $data['section_name']   = $section_info['name'];
	        $data['section_id']   	= $section_info['id'];
        	$questions = $this -> question -> get_question_survey_papi($slug, $section_id);
        	$i = 0;
        	$j = 0;
        	foreach ($questions as $key) {
                if($j == 2){ //batasan 1 nomor ada 2 statement
                    $j = 0;
                }

                $data['random']         = $key -> random;
                $data['survey_title']   = $key -> survey_title;
                $data['survey_id']      = $key -> id_survey;
                $number[$i]             = $key -> number;
                $question[$number[$i]]['statement'][$key -> key]  = $key -> statement;
                $i++;
                $j++;
            }
            if($data['random'] == 1){
                shuffle($question);
            }

            $data['question'] = $question;	    	
	    	return $data;
	    }
}
