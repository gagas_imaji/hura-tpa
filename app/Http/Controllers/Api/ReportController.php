<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use DB;
use Nexmo;
use Exception;
use App\Notifications\TwoFactorAuth;


use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Survey;
use App\User;
use App\SurveyCategory;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Library\report_survey;

use Dingo\Api\Routing\Helpers;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\ValidationHttpException;
class ReportController extends Controller
{
    	public function __construct(SurveyRepositoryInterface $survey, ResultRepositoryInterface $result)
	    {
	        $this -> survey         = $survey;
	        $this -> result         = $result;
	    }

	    public function reportPapi(Request $request)
	    {
	    	$post = $request->all();
	    	$email = $post["email"];
	    	$survey_id = $post["survey_id"];
	    	$user = DB::table("users")->where("email",$email)->first();
	    	if(is_null($user))
	    	{
	    		$data["user"] = "User tidak ditemukan";
	    		return $data;
	    	}
	    	$papi = DB::table('report_papikostick')->where(["user_id"=>$user->id,"survey_id"=>$survey_id])->first();
	    	if(is_null($papi))
	    	{
	    		$data["user"] = [ "user_id"=>$user->id,"email"=>$email];
	    		$data["result"] = "User  belum memiliki report";
	    		return $data;
	    	}
	    	$data["user"] = [ "user_id"=>$user->id,"email"=>$email];
	    	$data["result"]['score'] =[
	    	"N"=>$papi->N,
	    	"G"=>$papi->G,
	    	"A"=>$papi->A,
	    	"L"=>$papi->L,
	    	"P"=>$papi->P,
	    	"I"=>$papi->I,
	    	"T"=>$papi->T,
	    	"V"=>$papi->V,
	    	"X"=>$papi->X,
	    	"S"=>$papi->S,
	    	"B"=>$papi->B,
	    	"O"=>$papi->O,
	    	"R"=>$papi->R,
	    	"D"=>$papi->D,
	    	"C"=>$papi->C,
	    	"Z"=>$papi->Z,
	    	"E"=>$papi->E,
	    	"K"=>$papi->K,
	    	"F"=>$papi->F,
	    	"W"=>$papi->W,

	    	];

	    	$data["result"]["detail"] =[
	    		"arah_kerja"=>[
	    			"positive"=>[    				
	    				"N"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"N","count"=>$papi->N])->first()->positive,
	    				"G"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"G","count"=>$papi->G])->first()->positive,
	    				"A"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"A","count"=>$papi->A])->first()->positive,
	    				],
	    			"negative"=>[    				
	    				"N"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"N","count"=>$papi->N])->first()->negative,
	    				"G"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"G","count"=>$papi->G])->first()->negative,
	    				"A"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"A","count"=>$papi->A])->first()->negative,
	    				]
	    			],
	    		"gaya_kerja"=>[
	    			"positive"=>[    				
	    				"R"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"R","count"=>$papi->R])->first()->positive,
	    				"D"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"D","count"=>$papi->D])->first()->positive,
	    				"C"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"C","count"=>$papi->C])->first()->positive,
	    				],
	    			"negative"=>[    				
	    				"R"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"R","count"=>$papi->R])->first()->negative,
	    				"D"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"D","count"=>$papi->D])->first()->negative,
	    				"C"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"C","count"=>$papi->C])->first()->negative,
	    				]
	    			],
	    		"aktivitas"=>[
	    			"positive"=>[    				
	    				"T"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"T","count"=>$papi->T])->first()->positive,
	    				"V"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"V","count"=>$papi->V])->first()->positive,
	    				],
	    			"negative"=>[    				
	    				"T"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"T","count"=>$papi->T])->first()->negative,
	    				"V"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"V","count"=>$papi->V])->first()->negative,
	    				]
	    			],
			"keikutsertaan "=>[
	    			"positive"=>[    				
	    				"F"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"F","count"=>$papi->F])->first()->positive,
	    				"W"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"W","count"=>$papi->W])->first()->positive,
	    				],
	    			"negative"=>[    				
	    				"F"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"F","count"=>$papi->F])->first()->negative,
	    				"W"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"W","count"=>$papi->W])->first()->negative,
	    				]
	    			] ,
	    		"sikap_sosial "=>[
	    			"positive"=>[    				
	    				"O"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"O","count"=>$papi->O])->first()->positive,
	    				"B"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"B","count"=>$papi->B])->first()->positive,
	    				"S"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"S","count"=>$papi->S])->first()->positive,
	    				"X"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"X","count"=>$papi->X])->first()->positive,
	    				],
	    			"negative"=>[    				
	    				"O"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"O","count"=>$papi->O])->first()->negative,
	    				"B"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"B","count"=>$papi->B])->first()->negative,
	    				"S"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"S","count"=>$papi->S])->first()->negative,
	    				"X"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"X","count"=>$papi->X])->first()->negative,
	    				]
	    			] ,
	    		"tempramen "=>[
	    			"positive"=>[    				
	    				"Z"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"Z","count"=>$papi->Z])->first()->positive,
	    				"E"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"E","count"=>$papi->E])->first()->positive,
	    				"K"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"K","count"=>$papi->K])->first()->positive,
	    				],
	    			"negative"=>[    				
	    				"Z"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"Z","count"=>$papi->Z])->first()->negative,
	    				"E"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"E","count"=>$papi->E])->first()->negative,
	    				"K"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"K","count"=>$papi->K])->first()->negative,
	    				]
	    			] ,
	    		"kepemimpinan "=>[
	    			"positive"=>[    				
	    				"L"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"L","count"=>$papi->L])->first()->positive,
	    				"P"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"P","count"=>$papi->P])->first()->positive,
	    				"I"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"I","count"=>$papi->I])->first()->positive,
	    				],
	    			"negative"=>[    				
	    				"L"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"L","count"=>$papi->L])->first()->negative,
	    				"P"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"P","count"=>$papi->P])->first()->negative,
	    				"I"=>DB::table('lookup_papi_acuan_report')->where(["key"=>"I","count"=>$papi->I])->first()->negative,
	    				]
	    			] ,   	  	
	    	];
	    	return $data;
	    }
}
