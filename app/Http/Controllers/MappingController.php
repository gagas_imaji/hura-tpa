<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;
use App\Talenthub\Repository\ResultRepositoryInterface;

class MappingController extends Controller
{
	public function __construct(ResultRepositoryInterface $result)
    {
        $this -> result         = $result;
    }

    public function history_section(){

    	$survey_report = DB::table('survey_report') -> get();
    	$i = 0;
    	foreach($survey_report as $key){
    		$insert[$i]['user_id'] 		= $key -> user_id;
    		$insert[$i]['section_id'] 	= $key -> section_id;
    		$i++;
    	}
    	DB::beginTransaction();
        try{
	    	foreach($insert as $key){
	    		DB::table('history_section') -> insert([
	    			'user_id' 		=> $key['user_id'],
	    			'section_id' 	=> $key['section_id']
	    		]);
	    	}
	     	DB::commit();
	     	return 'berhasil';
        }catch(exception $e){
            DB::rollback();
            return 'gagal';
        }
    }

    public function report_wpa(){
    	$report_wpa = DB::table('report_wpa') -> get();
    	$i = 0;
    	foreach($report_wpa as $key){
    		$insert[$i]['user_id'] 	= $key -> user_id;
    		$insert[$i]['survey_id']= $key -> survey_id;
    		if($key -> criteria == 'most'){
    			$most['d'] = $key -> D;
    			$most['i'] = $key -> I;
    			$most['s'] = $key -> S;
    			$most['c'] = $key -> C;
    			$insert[$i]['most'] = json_encode($most);
    		}elseif($key -> criteria == 'lest'){
    			$lest['d'] = $key -> D;
    			$lest['i'] = $key -> I;
    			$lest['s'] = $key -> S;
    			$lest['c'] = $key -> C;
    			$insert[$i]['lest'] = json_encode($lest);
    		}elseif($key -> criteria == 'change'){
    			$change['d'] = $key -> D;
    			$change['i'] = $key -> I;
    			$change['s'] = $key -> S;
    			$change['c'] = $key -> C;
    			$insert[$i]['change'] 	= json_encode($change);
    			$insert[$i]['hasil'] 	= $key -> hasil;
    			$insert[$i]['type'] 	= $key -> type;
    			$i++;
    		}
    	}
    	DB::beginTransaction();
        try{
	    	DB::table('report_wpa_2') -> insert($insert);
	     	DB::commit();
	     	return 'berhasil';
        }catch(exception $e){
            DB::rollback();
            return 'gagal';
        }
    }

    public function report_wpa_detail(){


    	$get_k_pekerjaan = DB::table('report_wpa_karakteristik_pekerjaan') -> get();
    	$get_k_umum = DB::table('report_wpa_karakteristik_umum') -> get();
    	$get_kekuatan = DB::table('report_wpa_kekuatan') -> get();
    	$get_kelemahan = DB::table('report_wpa_kelemahan') -> get();
    	$get_p_k_kekuatan = DB::table('report_wpa_perilaku_kerja_kekuatan') -> get();
    	$get_p_k_kelemahan = DB::table('report_wpa_perilaku_kerja_kelemahan') -> get();
    	$get_s_e_kekuatan = DB::table('report_wpa_suasana_emosi_kekuatan') -> get();
    	$get_s_e_kelemahan = DB::table('report_wpa_suasana_emosi_kelemahan') -> get();
    	$get_uraian_kepribadian = DB::table('report_wpa_uraian_kepribadian') -> get();

    	//=============== KARAKTERISTIK PEKERJAAN ===============//
    	$user_old = null;
    	$survey_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_k_pekerjaan as $key){
    		$user_id = $key -> user_id;
    		$survey_id = $key -> survey_id;

    		//pengecekan data pertama
    		if($user_old == null && $survey_old == null){
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}
    		if($user_old != $user_id || $survey_old != $survey_id){
    			$insert[$i]['karakteristik_pekerjaan'] = json_encode($karakteristik_pekerjaan);
    			$j = 0;
    			$karakteristik_pekerjaan = null; //kosongin lagi array nya setiap ganti user_id
    			$i++;
    			$karakteristik_pekerjaan[$j] = $key -> text;
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}else{
    			$insert[$i]['user_id'] 		= $key -> user_id;
    			$insert[$i]['survey_id'] 	= $key -> survey_id;
    			$karakteristik_pekerjaan[$j]= $key -> text;
    		}
    		$j++;
    		$baris_data++;

    		//pengecekan data terakhir
    		if($baris_data == count($get_k_pekerjaan)){
    			$insert[$i]['karakteristik_pekerjaan'] = json_encode($karakteristik_pekerjaan);
    		}
    	}

    	//=============== KARAKTERISTIK UMUM ===============//
    	$user_old = null;
    	$survey_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_k_umum as $key){
    		$user_id = $key -> user_id;
    		$survey_id = $key -> survey_id;

    		//pengecekan data pertama
    		if($user_old == null && $survey_old == null){
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}
    		if($user_old != $user_id || $survey_old != $survey_id){
    			$insert[$i]['karakteristik_umum'] = json_encode($karakteristik_umum);
    			$j = 0;
    			$karakteristik_umum = null; //kosongin lagi array nya setiap ganti user_id
    			$i++;
    			$karakteristik_umum[$j] = $key -> text;
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}else{
    			$insert[$i]['user_id'] 		= $key -> user_id;
    			$insert[$i]['survey_id'] 	= $key -> survey_id;
    			$karakteristik_umum[$j]= $key -> text;
    		}
    		$j++;
    		$baris_data++;

    		//pengecekan data terakhir
    		if($baris_data == count($get_k_umum)){
    			$insert[$i]['karakteristik_umum'] = json_encode($karakteristik_umum);
    		}
    	}

    	//=============== KEKUATAN ===============//
    	$user_old = null;
    	$survey_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_kekuatan as $key){
    		$user_id = $key -> user_id;
    		$survey_id = $key -> survey_id;

    		//pengecekan data pertama
    		if($user_old == null && $survey_old == null){
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}
    		if($user_old != $user_id || $survey_old != $survey_id){
    			$insert[$i]['kekuatan'] = json_encode($kekuatan);
    			$j = 0;
    			$kekuatan = null; //kosongin lagi array nya setiap ganti user_id
    			$i++;
    			$kekuatan[$j] = $key -> text;
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}else{
    			$insert[$i]['user_id'] 		= $key -> user_id;
    			$insert[$i]['survey_id'] 	= $key -> survey_id;
    			$kekuatan[$j]= $key -> text;
    		}
    		$j++;
    		$baris_data++;

    		//pengecekan data terakhir
    		if($baris_data == count($get_kekuatan)){
    			$insert[$i]['kekuatan'] = json_encode($kekuatan);
    		}
    	}

    	//=============== KELEMAHAN ===============//
    	$user_old = null;
    	$survey_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_kelemahan as $key){
    		$user_id = $key -> user_id;
    		$survey_id = $key -> survey_id;

    		//pengecekan data pertama
    		if($user_old == null && $survey_old == null){
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}
    		if($user_old != $user_id || $survey_old != $survey_id){
    			$insert[$i]['kelemahan'] = json_encode($kelemahan);
    			$j = 0;
    			$kelemahan = null; //kosongin lagi array nya setiap ganti user_id
    			$i++;
    			$kelemahan[$j] = $key -> text;
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}else{
    			$insert[$i]['user_id'] 		= $key -> user_id;
    			$insert[$i]['survey_id'] 	= $key -> survey_id;
    			$kelemahan[$j]= $key -> text;
    		}
    		$j++;
    		$baris_data++;

    		//pengecekan data terakhir
    		if($baris_data == count($get_kelemahan)){
    			$insert[$i]['kelemahan'] = json_encode($kelemahan);
    		}
    	}

    	//=============== PERILAKU KERJA KEKUATAN ===============//
    	$user_old = null;
    	$survey_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_p_k_kekuatan as $key){
    		$user_id = $key -> user_id;
    		$survey_id = $key -> survey_id;

    		//pengecekan data pertama
    		if($user_old == null && $survey_old == null){
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}
    		if($user_old != $user_id || $survey_old != $survey_id){
    			$insert[$i]['perilaku_kerja_kekuatan'] = json_encode($perilaku_kerja_kekuatan);
    			$j = 0;
    			$perilaku_kerja_kekuatan = null; //kosongin lagi array nya setiap ganti user_id
    			$i++;
    			$perilaku_kerja_kekuatan[$j] = $key -> text;
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}else{
    			$insert[$i]['user_id'] 		= $key -> user_id;
    			$insert[$i]['survey_id'] 	= $key -> survey_id;
    			$perilaku_kerja_kekuatan[$j]= $key -> text;
    		}
    		$j++;
    		$baris_data++;

    		//pengecekan data terakhir
    		if($baris_data == count($get_p_k_kekuatan)){
    			$insert[$i]['perilaku_kerja_kekuatan'] = json_encode($perilaku_kerja_kekuatan);
    		}
    	}

    	//=============== PERILAKU KERJA KELEMAHAN ===============//
    	$user_old = null;
    	$survey_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_p_k_kelemahan as $key){
    		$user_id = $key -> user_id;
    		$survey_id = $key -> survey_id;

    		//pengecekan data pertama
    		if($user_old == null && $survey_old == null){
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}
    		if($user_old != $user_id || $survey_old != $survey_id){
    			$insert[$i]['perilaku_kerja_kelemahan'] = json_encode($perilaku_kerja_kelemahan);
    			$j = 0;
    			$perilaku_kerja_kelemahan = null; //kosongin lagi array nya setiap ganti user_id
    			$i++;
    			$perilaku_kerja_kelemahan[$j] = $key -> text;
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}else{
    			$insert[$i]['user_id'] 		= $key -> user_id;
    			$insert[$i]['survey_id'] 	= $key -> survey_id;
    			$perilaku_kerja_kelemahan[$j]= $key -> text;
    		}
    		$j++;
    		$baris_data++;

    		//pengecekan data terakhir
    		if($baris_data == count($get_p_k_kelemahan)){
    			$insert[$i]['perilaku_kerja_kelemahan'] = json_encode($perilaku_kerja_kelemahan);
    		}
    	}

    	//=============== SUASANA EMOSI KEKUATAN ===============//
    	$user_old = null;
    	$survey_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_s_e_kekuatan as $key){
    		$user_id = $key -> user_id;
    		$survey_id = $key -> survey_id;

    		//pengecekan data pertama
    		if($user_old == null && $survey_old == null){
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}
    		if($user_old != $user_id || $survey_old != $survey_id){
    			$insert[$i]['suasana_emosi_kekuatan'] = json_encode($suasana_emosi_kekuatan);
    			$j = 0;
    			$suasana_emosi_kekuatan = null; //kosongin lagi array nya setiap ganti user_id
    			$i++;
    			$suasana_emosi_kekuatan[$j] = $key -> text;
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}else{
    			$insert[$i]['user_id'] 		= $key -> user_id;
    			$insert[$i]['survey_id'] 	= $key -> survey_id;
    			$suasana_emosi_kekuatan[$j]= $key -> text;
    		}
    		$j++;
    		$baris_data++;

    		//pengecekan data terakhir
    		if($baris_data == count($get_s_e_kekuatan)){
    			$insert[$i]['suasana_emosi_kekuatan'] = json_encode($suasana_emosi_kekuatan);
    		}
    	}

    	//=============== SUASANA EMOSI KELEMAHAN ===============//
    	$user_old = null;
    	$survey_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_s_e_kelemahan as $key){
    		$user_id = $key -> user_id;
    		$survey_id = $key -> survey_id;

    		//pengecekan data pertama
    		if($user_old == null && $survey_old == null){
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}
    		if($user_old != $user_id || $survey_old != $survey_id){
    			$insert[$i]['suasana_emosi_kelemahan'] = json_encode($suasana_emosi_kelemahan);
    			$j = 0;
    			$suasana_emosi_kelemahan = null; //kosongin lagi array nya setiap ganti user_id
    			$i++;
    			$suasana_emosi_kelemahan[$j] = $key -> text;
    			$user_old = $user_id;
    			$survey_old = $survey_id;
    		}else{
    			$insert[$i]['user_id'] 		= $key -> user_id;
    			$insert[$i]['survey_id'] 	= $key -> survey_id;
    			$suasana_emosi_kelemahan[$j]= $key -> text;
    		}
    		$j++;
    		$baris_data++;

    		//pengecekan data terakhir
    		if($baris_data == count($get_s_e_kelemahan)){
    			$insert[$i]['suasana_emosi_kelemahan'] = json_encode($suasana_emosi_kelemahan);
    		}
    	}

    	
    	//=============== URAIAN KEPRIBADIAN ===============//
    	$user_old = null;
    	$i = 0;
    	$j = 0;
    	$baris_data = 0; //untuk ngecek baris terakhir data
    	foreach($get_uraian_kepribadian as $key){
    		$insert[$i]['uraian_kepribadian'] = "<p>Beliau ".$key -> text;
    		$i++;
    	}

		DB::beginTransaction();
        try{
	    	DB::table('report_wpa_detail') -> insert($insert);
	     	DB::commit();
	     	return 'berhasil';
        }catch(exception $e){
            DB::rollback();
            return 'gagal';
        }
    	
    }

    public function report_user(){
    	$user_project = DB::table('user_project') -> get();
    	$gagal = 0;

    	DB::beginTransaction();
        try{
	    	foreach ($user_project as $key) {
	    		$check_invite_report = DB::table('user_report') -> select(DB::raw('count(user_report.user_id) as count')) -> where('user_id', '=', $key->id_user) -> where('tujuan', '=', $key->id_project) -> first();

	    		if($check_invite_report -> count == 0){
		    		DB::table('user_report') -> insert([
		    			'user_id' => $key->id_user,
		    			'tujuan'  => $key->id_project,
		    			'created_at' => Carbon::now('Asia/Jakarta')
		    		]);
	    		}
	    	}
	    	DB::commit();
    	}catch(exception $e){
            DB::rollback();
            $gagal++;
        }

        // =============== LAI ===============//
        $survey_report = DB::table('survey_report') -> select('survey_report.*', 'survey.id_project', 'survey.category_survey_id') -> join('survey', 'survey.id', '=', 'survey_report.survey_id') -> orderBy('survey_id','asc') -> orderBy('user_id', 'asc') -> orderBy('section_id', 'asc') -> get();

        
        if($survey_report){
	        DB::beginTransaction();
	        try{
                $user_old = null;
                $survey_old = null;
                $project_old = null;
                $j = 1;
                $baris_data = 0; //untuk ngecek baris terakhir data
                $total = 0;
		        foreach ($survey_report as $key) {
		        	$user_id = $key -> user_id;
		    		$survey_id = $key -> survey_id;
                    $project_id = $key -> id_project;

		    		//pengecekan data pertama
		    		if($user_old == null && $survey_old == null){
		    			$user_old = $user_id;
                        $survey_old = $survey_id;
		    			$project_old = $project_id;
		    		}

		    		if($user_old != $user_id || $survey_old != $survey_id){
		    			//untuk update kriteria LAI
		    			if($key -> category_survey_id == 1){
			    			$avg 		= round($total/5,1);
			    			$criteria 	= $this -> result -> get_criteria_gti_report($avg);

			    			DB::table('user_report') -> where('user_id', '=', $user_old) -> where('tujuan', '=', $project_old) -> update([
				    			'kriteria' => $criteria
				    		]);

			    			$total 		= 0; //balikin jadi nol lagi utk user selanjutnya
		    			}elseif($key -> category_survey_id == 2){ //untuk update jumlah dan iq TIKI
		    				if($total <= 29){ //batas bawah
					            $iq = 56;
					        }elseif($total > 107){ //batas atas
					            $iq = 145;
					        }else{ //lookup
					            $iq = $this -> result -> get_iq_tiki($total);
					        }

					        DB::table('user_report') -> where('user_id', '=', $user_old) -> where('tujuan', '=', $project_old) -> update([
				    			'jumlah' => $total,
				    			'IQ' => $iq
				    		]);

				    		$total = 0; //balikin jadi nol lagi utk user selanjutnya
		    			}

		    			$j = 1; //balikin jadi 1 lagi utk user selanjutnya
		    			$user_old = $user_id;
                        $survey_old = $survey_id;
		    			$project_old = $project_id;
		    		}

		    		if($key -> category_survey_id == 1){ //kalau LAI update GTQ
			    		DB::table('user_report') -> where('user_id', '=', $key->user_id) -> where('tujuan', '=', $key->id_project) -> update([
			    			'GTQ'.$j => $key->score
			    		]);
		    		}elseif($key -> category_survey_id == 2){ //kalai TIKI update TIKI
		    			if($j < 5){
			    			DB::table('user_report') -> where('user_id', '=', $key->user_id) -> where('tujuan', '=', $key->id_project) -> update([
				    			'SS'.$j => $key->score
				    		]);
		    			}
		    		}

		    		$total = $total + $key->score;
		    		$j++;
		    		$baris_data++;
		        }
	        	DB::commit();
	        }catch(exception $e){
	            DB::rollback();
	            $gagal++;
	        }
        }
        

        //=============== WPA ===============//
        $report_wpa = DB::table('report_wpa') -> select('report_wpa.*', 'survey.id_project') -> join('survey', 'survey.id', '=', 'report_wpa.survey_id') -> get();
        if($report_wpa){
        	DB::beginTransaction();
	        try{
	        	foreach ($report_wpa as $key) {

	        		$update['hasil']  = $key -> hasil;
	        		$most = json_decode($key -> most, true);
	        		$update['most_d'] = $most['d'];
	        		$update['most_i'] = $most['i'];
	        		$update['most_s'] = $most['s'];
	        		$update['most_c'] = $most['c'];

	        		$lest = json_decode($key -> lest, true);
	        		$update['lest_d'] = $lest['d'];
	        		$update['lest_i'] = $lest['i'];
	        		$update['lest_s'] = $lest['s'];
	        		$update['lest_c'] = $lest['c'];

	        		$change = json_decode($key -> change, true);
	        		$update['change_d'] = $change['d'];
	        		$update['change_i'] = $change['i'];
	        		$update['change_s'] = $change['s'];
	        		$update['change_c'] = $change['c'];
					
					DB::table('user_report') -> where('user_id', '=', $key->user_id) -> where('tujuan', '=', $key -> id_project) -> update($update);
	        	}
        		DB::commit();
	        }catch(exception $e){
	            DB::rollback();
	            $gagal++;
	        }
        }

        //=============== WBA ===============//
        $report_wba = DB::table('report_papikostick') -> select ('report_papikostick.*', 'survey.id_project') -> join('survey', 'survey.id', '=', 'report_papikostick.survey_id') -> get();
        if($report_wba){
        	DB::beginTransaction();
	        try{
	        	foreach ($report_wba as $key) {
	        		$update['N'] = $key -> N;
	        		$update['G'] = $key -> G;
	        		$update['A'] = $key -> A;
	        		$update['L'] = $key -> L;
	        		$update['P'] = $key -> P;
	        		$update['I'] = $key -> I;
	        		$update['T'] = $key -> T;
	        		$update['V'] = $key -> V;
	        		$update['X'] = $key -> X;
	        		$update['S'] = $key -> S;
	        		$update['B'] = $key -> B;
	        		$update['O'] = $key -> O;
	        		$update['R'] = $key -> R;
	        		$update['D'] = $key -> D;
	        		$update['C'] = $key -> C;
	        		$update['Z'] = $key -> Z;
	        		$update['E'] = $key -> E;
	        		$update['K'] = $key -> K;
	        		$update['F'] = $key -> F;
	        		$update['W'] = $key -> W;
	        		DB::table('user_report') -> where('user_id', '=', $key->user_id) -> where('tujuan', '=', $key -> id_project) -> update($update);
	        	}
        		DB::commit();
	        }catch(exception $e){
	            DB::rollback();
	            $gagal++;
	     	}
        }

        if($gagal != 0){
        	return 'Terjadi kesalahan';
        }else{
        	return 'Berhasil Mapping Data';
        }
    }

    public function kuota_project(){
        // $kuota_project = DB::table('kuota_survey') -> get();
        // DB::beginTransaction();
        // try{
        //     foreach ($kuota_project as $key) {
        //         DB::table('kuota_survey') -> where('id', '=', $key -> id) -> update(['total' => $key -> jlh_user]);
        //     }
        //     DB::commit();
        //     return 'Berhasil update kuota';
        // }catch(exception $e){
        //     DB::rollback();
        //     return 'Gagal update kuota';
        // }

        $survey = DB::table('survey') -> select(DB::raw('count(id) as count_survey'), 'id_project') -> groupBy('id_project') -> get();

        $i = 0;
        $not_in = [];
        $not_in = [1,4,5,8,10,12,19,21,31,36,45,47]; //id_project yang sudah dihapus (sudah termasuk project susulan [36,45,47])
        foreach($survey as $key){
            $user = DB::table('user_project') -> select(DB::raw('count(id_user) as count_user'), 'id_project') -> groupBy('id_project') -> where('id_project', '=', $key -> id_project) -> whereNotIn('id_project', $not_in) -> get();

            if($user -> count() != 0){
                $insert[$i]['user_id'] = Auth::user() -> id;
                $insert[$i]['jlh_survey'] = $key -> count_survey;
                // $insert[$i]['id_project'] = $key -> id_project;
                foreach ($user as $value) {
                    $insert[$i]['jlh_user'] = $value -> count_user;
                    $insert[$i]['total']    = $value -> count_user;
                    $insert[$i]['created_at'] = Carbon::now('Asia/Jakarta');
                }
                
            }
            $i++;
        }
        DB::beginTransaction();
        try{
            DB::table('kuota_survey') -> insert($insert);
            DB::commit();
            return 'Berhasil Kalkulasi Kuota';
        }catch(exception $e){
            DB::rollback();
        }
    }
}
