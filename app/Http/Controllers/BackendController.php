<?php

namespace App\Http\Controllers;
use Entrust;
use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
use Nexmo;
use Exception;
use App\Notifications\TwoFactorAuth;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\CorporateRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\Survey;
use App\User;
use App\UserReport;
use App\SurveyReport;
use App\StandarNilaiProject;
use App\StandarNilai;

use App\SurveyCategory;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Library\report_survey;

class BackendController extends Controller
{

	public function __construct(SurveyRepositoryInterface $survey, CorporateRepositoryInterface $corporate, ResultRepositoryInterface $result, QuestionRepositoryInterface $question, OptionAnswerRepositoryInterface $option_answer)
    {
        $this -> survey         = $survey;
        $this -> corporate      = $corporate;
        $this -> result         = $result;
        $this -> question       = $question;
        $this -> option_answer  = $option_answer;
    }

    public function history(Request $request){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

    	//ambil survey terbaru yang statusnya 1
        $history_survey     = $this -> survey -> get_all_history_survey();
        $score_total = 0;
        $i = 0; //indeks survey
        if($history_survey -> count() != 0){
            foreach ($history_survey as $history) {
                $list_all[$i]['name']         = $history -> title;
                $list_all[$i]['slug']         = $history -> slug;
                $list_all[$i]['end_date']     = $history -> end_date;
                $list_all[$i]['category']     = $history -> category_survey_id;
                /*
                report survey per divisi ambil daftar divisi berdasarkan corporate
                */
                $div = $this -> corporate -> get_division_corporate();
                $j = 0; //indeks divisi
                foreach ($div as $key) {
                    $division[$j]['id']     = $key -> id;
                    $division[$j]['name']   = $key -> name;
                    $j++;
                }
                $i++;
                $score_total = 0;
            }

            $paginate = 10;
            $page = $request -> input('page');
            $offSet = ($page * $paginate) - $paginate;
            $itemsForCurrentPage = array_slice($list_all, $offSet, $paginate, true);
            $report = new LengthAwarePaginator($itemsForCurrentPage, count($list_all), $paginate);
            $report->setPath('history');

            return view('backend.history',compact('report', 'division'));
        }else{
            return redirect() -> back() -> withErrors('Anda belum memiliki laporan test yang sudah selesai dilaksanakan oleh seluruh peserta');
        }

    }

    public function reportUserBySurvey($slug){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $score_total            = 0;
        $info_survey            = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey              = $info_survey['id'];
        $survey['title']        = $info_survey['title'];
        $survey['category_id']  = $info_survey['category_survey_id'];

        if($info_survey['category_survey_id'] == 3){ //report WPA
            return redirect('backend/report-wpa-with-user/'.$slug);
        }elseif($info_survey['category_survey_id'] == 4){ //report WBA
            return redirect('backend/report-papi-with-user/'.$slug);
        }else{

            $i      = 0;
            $report = $this -> result -> get_user_report($id_survey);
            $rowset = null;
            if($report){
                foreach ($report as $key) {
                    $rowset[$i]['user_id']  = $key -> user_id;
                    $rowset[$i]['name']     = $key -> first_name;
                    $rowset[$i]['sum']      = $key -> score;

                    //untuk hasil di tabel berdasarkan jenis survey
                    if($info_survey['category_survey_id'] == 1){
                        $rowset[$i]['nilai']  = round($rowset[$i]['sum']/5, 1); //bagi lima karna LAI terdiri dari 5 subtest
                        $rowset[$i]['hasil']  = $this -> result -> get_criteria_gti_report($rowset[$i]['nilai']);
                    }elseif($info_survey['category_survey_id'] == 2){
                        if($rowset[$i]['sum'] <= 29){ //batas bawah
                            $rowset[$i]['nilai'] = 56;
                        }elseif($rowset[$i]['sum'] > 107){ //batas atas
                            $rowset[$i]['nilai'] = 145;
                        }else{ //lookup
                            $rowset[$i]['nilai']     = $this -> result -> get_iq_tiki($rowset[$i]['sum']);
                        }

                        if($rowset[$i]['nilai'] <= 80){
                            $rowset[$i]['hasil'] = "Di bawah Rata-Rata";
                        }elseif($rowset[$i]['nilai'] <= 90){
                            $rowset[$i]['hasil'] = "Rata-Rata Rendah";
                        }elseif($rowset[$i]['nilai'] <= 100){
                            $rowset[$i]['hasil'] = "Rata-Rata";
                        }elseif($rowset[$i]['nilai'] <= 110){
                            $rowset[$i]['hasil'] = "Rata-Rata Atas";
                        }elseif($rowset[$i]['nilai'] <= 125){
                            $rowset[$i]['hasil'] = "Di Atas Rata-Rata";
                        }elseif($rowset[$i]['nilai'] >= 126){
                            $rowset[$i]['hasil'] = "Jauh Di Atas Rata-Rata";
                        }
                    }
                    $i++;
                }
            }
            return view('backend.report',compact('rowset', 'survey', 'slug'));
        }

    }

    public function reportWPAPapiWithUser($slug){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey            = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey              = $info_survey['id'];
        $survey['title']        = $info_survey['title'];
        $survey['category_id']  = $info_survey['category_survey_id'];

        //tentukan akan ambil data ke tabel mana berdasarkan category survey
        if($info_survey['category_survey_id'] == 3){
            $table = 'report_wpa';
        }elseif($info_survey['category_survey_id'] == 4){
            $table = 'report_papikostick';
        }
        
        $i      = 0;
        $report = $this -> result -> get_report_wpa_papi_detail($table, $id_survey);
        $rowset = null;
        if($report -> count() != 0){
            foreach ($report as $key){
                $rowset[$i]['first_name'] = $key -> first_name;
                $rowset[$i]['user_id']    = $key -> user_id;
                $rowset[$i]['detail']     = $this -> result -> get_report_wpa_by_graph($key -> user_id);

                //untuk hasil di tabel berdasarkan jenis survey
                if($info_survey['category_survey_id'] == 3){
                    $result     = $this -> result -> get_hasil_wpa($key -> user_id, $id_survey);
                    $rowset[$i]['hasil']  = $result['hasil'];
                    $rowset[$i]['type']   = $result['type'];
                }elseif($info_survey['category_survey_id'] == 4){
                    $rowset[$i]['hasil']  = 'Silahkan Lihat Detail';
                    $rowset[$i]['type']   = '-';
                }
                $i++;
            }
            
        }

        return view('backend.report_wpa_papi',compact('rowset', 'slug', 'survey'));
    }

    public function getPesertaSurvey($slug){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $user = $this -> survey -> get_peserta_survey($slug);
        $i = 0;
        foreach ($user as $key) {
            $survey['title']        = $key -> title;
            $i++;
        }
        return view('backend.detail_peserta', compact('data', 'survey', 'user'));
    }

    public function rekapReportUser(Request $request){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $col_heads = array(trans('messages.email'));
        $user_id = Auth::user() -> id;
        $request_filter = $request -> input('filter');
        $heads_hura = [trans('messages.name'),'Project','mD','mI','mS','mC','lD','lI','lS','lC','cD','cI','cS','cC','Hasil','GTQ1','GTQ2','GTQ3','GTQ4','GTQ5','Average GTQ','SS1','SS2','SS3','SS4','Jumlah',
                        'IQ','N','G','A','L','P','I','T','V','X','S','B','O','R','D','C','Z','E','K','F','W'];
        foreach ($heads_hura as $value) {
            array_push($col_heads, $value);
        }

        $heads_tpa = ['log_berpikir','k_numerikal','d_analisa','k_verbal','o_hasil','fleks','s_kerja','m_prestasi','kerjasama','k_interpersonal','pil_jabatan','prosen_kecocokan','hasil_rekomendasi'];
        foreach ($heads_tpa as $value) {
            array_push($col_heads, $value);
        }

        //ambil id_corporate HRD
        $corp = $this -> corporate -> getCorporateByUser($user_id);
        $corporate = $corp['name'];

        //ambil project terbaru
        $last_project = $this -> survey -> get_last_project($user_id);
        if($request_filter){
            $tujuan = $request_filter;
        }else{
            $tujuan = $last_project['id'];
        }
        $info_project_tujuan = $this -> survey -> get_info_project_by_id($tujuan);
        if (!is_null($info_project_tujuan)) {
            $projectname = $info_project_tujuan['name'];
        } else {
            $projectname = '-';
        }
        if ($tujuan!="all") {
            //ambil user yang corporatenya sama dengan HRD
            $user_report = UserReport::join('profiles', 'profiles.user_id', '=', 'user_report.user_id')
                    -> join('users', 'profiles.user_id', '=', 'users.id')
                    -> join('division', 'division.id', '=', 'profiles.id_division')
                    -> leftJoin('user_report_aspeks', function($join)
                    {
                        $join->on('user_report_aspeks.user_id', '=', 'user_report.user_id')->on('user_report_aspeks.project_id', '=', 'user_report.tujuan');
                    })
                    -> leftJoin('hasil_pesertas', function($join)
                    {
                        $join->on('hasil_pesertas.user_id', '=', 'user_report.user_id')->on('hasil_pesertas.tujuan', '=', 'user_report.tujuan');
                    })
                    -> select('user_report.*', 'profiles.*', 'users.email', 'division.name', 'user_report_aspeks.nilai_aspek','hasil_pesertas.kecocokan','hasil_pesertas.kriteria_psikograph_id')
                    -> where('profiles.id_corporate', '=', $corp['id'])
                    -> where('user_report.tujuan', '=', $tujuan)
                    -> orderBy('users.id')
                    -> get();
        } else {
            //ambil user yang corporatenya sama dengan HRD
            $user_report = UserReport::join('profiles', 'profiles.user_id', '=', 'user_report.user_id')
                    -> join('users', 'profiles.user_id', '=', 'users.id')
                    -> join('division', 'division.id', '=', 'profiles.id_division')
                    -> join('project','project.id','=','user_report.tujuan')
                    -> leftJoin('user_report_aspeks', function($join)
                    {
                        $join->on('user_report_aspeks.user_id', '=', 'user_report.user_id')->on('user_report_aspeks.project_id', '=', 'user_report.tujuan');
                    })
                    -> leftJoin('hasil_pesertas', function($join)
                    {
                        $join->on('hasil_pesertas.user_id', '=', 'user_report.user_id')->on('hasil_pesertas.tujuan', '=', 'user_report.tujuan');
                    })
                    -> select('user_report.*', 'profiles.*', 'users.email', 'division.name', 'user_report_aspeks.nilai_aspek','hasil_pesertas.kecocokan','hasil_pesertas.kriteria_psikograph_id','project.name as project_name')
                    -> where('profiles.id_corporate', '=', $corp['id'])
                    -> orderBy('users.id')
                    -> get();
        }
        $jabatan = StandarNilaiProject::with('tpaStandarNilai')->where('project_id',$tujuan)->first();
        if (!is_null($jabatan)) {
            $namajabatan = $jabatan->tpaStandarNilai->nama_jabatan;
        } else {
            $namajabatan = '-';
        }

        $rowset = null;
        $i = 0;
        if(sizeof($user_report)>0){
            foreach ($user_report as $key) {
                $json_nilai_user = json_decode($key->nilai_aspek,true);
                $rowset[$i]['email'] = $key -> email;
                $rowset[$i]['first_name'] = $key -> first_name;
                if ($projectname!= "-") {
                    $rowset[$i]['tujuan'] = $projectname;    
                } else {
                    $rowset[$i]['tujuan'] = $key -> project_name;    
                }
                
                $rowset[$i]['most_d'] = $key -> most_d;
                $rowset[$i]['most_i'] = $key -> most_i;
                $rowset[$i]['most_s'] = $key -> most_s;
                $rowset[$i]['most_c'] = $key -> most_c;
                $rowset[$i]['lest_d'] = $key -> lest_d;
                $rowset[$i]['lest_i'] = $key -> lest_i;
                $rowset[$i]['lest_s'] = $key -> lest_s;
                $rowset[$i]['lest_c'] = $key -> lest_c;
                $rowset[$i]['change_d'] = $key -> change_d;
                $rowset[$i]['change_i'] = $key -> change_i;
                $rowset[$i]['change_s'] = $key -> change_s;
                $rowset[$i]['change_c'] = $key -> change_c;
                $rowset[$i]['hasil'] = $key -> hasil;
                $rowset[$i]['GTQ1'] = $key -> GTQ1;
                $rowset[$i]['GTQ2'] = $key -> GTQ2;
                $rowset[$i]['GTQ3'] = $key -> GTQ3;
                $rowset[$i]['GTQ4'] = $key -> GTQ4;
                $rowset[$i]['GTQ5'] = $key -> GTQ5;
                $rowset[$i]['average'] = ($key -> GTQ1 + $key -> GTQ2 + $key -> GTQ3 + $key -> GTQ4 + $key -> GTQ5) / 5;
                $rowset[$i]['SS1'] = $key -> SS1;
                $rowset[$i]['SS2'] = $key -> SS2;
                $rowset[$i]['SS3'] = $key -> SS3;
                $rowset[$i]['SS4'] = $key -> SS4;
                $rowset[$i]['jumlah'] = $key -> jumlah;
                $rowset[$i]['IQ'] = $key -> IQ;
                $rowset[$i]['N'] = $key -> N;
                $rowset[$i]['G'] = $key -> G;
                $rowset[$i]['A'] = $key -> A;
                $rowset[$i]['L'] = $key -> L;
                $rowset[$i]['P'] = $key -> P;
                $rowset[$i]['I'] = $key -> I;
                $rowset[$i]['T'] = $key -> T;
                $rowset[$i]['V'] = $key -> V;
                $rowset[$i]['X'] = $key -> X;
                $rowset[$i]['S'] = $key -> S;
                $rowset[$i]['B'] = $key -> B;
                $rowset[$i]['O'] = $key -> O;
                $rowset[$i]['R'] = $key -> R;
                $rowset[$i]['D'] = $key -> D;
                $rowset[$i]['C'] = $key -> C;
                $rowset[$i]['Z'] = $key -> Z;
                $rowset[$i]['E'] = $key -> E;
                $rowset[$i]['K'] = $key -> K;
                $rowset[$i]['F'] = $key -> F;
                $rowset[$i]['W'] = $key -> W;
                $rowset[$i]['log_berpikir'] = $json_nilai_user[1]-1;
                $rowset[$i]['k_numerikal'] = $json_nilai_user[2]-1;
                $rowset[$i]['d_analisa'] = $json_nilai_user[3]-1;
                $rowset[$i]['k_verbal'] = $json_nilai_user[4]-1;
                $rowset[$i]['o_hasil'] = $json_nilai_user[5]-1;
                $rowset[$i]['fleks'] = $json_nilai_user[6]-1;
                $rowset[$i]['s_kerja'] = $json_nilai_user[7]-1;
                $rowset[$i]['m_prestasi'] = $json_nilai_user[8]-1;
                $rowset[$i]['kerjasama'] = $json_nilai_user[9]-1;
                $rowset[$i]['k_interpersonal'] = $json_nilai_user[10]-1;
                $rowset[$i]['pil_jabatan'] = $namajabatan;
                $rowset[$i]['prosen_kecocokan'] = $key->kecocokan;
                $rowset[$i]['hasil_rekomendasi'] = $key->kriteria_psikograph_id;
                $i++;
            }
        }

        //ambil project yang pernah dibuat untuk filter project
        $all_project = $this -> survey -> get_all_project();
        $i = 0;
        if($all_project -> count() != 0){
            foreach ($all_project as $key) {
                $filter_project[$i]['name'] = $key->name;
                $filter_project[$i]['id']   = $key->id;
                $i++;
            }
        }else{
            $filter_project = null;
        }
        return view('backend.report_user',compact('table_data','rowset', 'col_heads', 'filter_project', 'request_filter', 'projectname', 'namajabatan'));
    }

    //========================================== TAMBAHAN VERSI 1,5 ==========================================
    public function history_project(){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        //ambil project terbaru yang sudah ada laporan surveynya
        $id_user = Auth::user() -> id;
        $history_project = $this -> survey -> get_all_project();
        $rowset = null;
        $i = 0; 
        if($history_project){
            foreach($history_project as $key){
                $rowset[$i]['nama_project'] = $key -> name;
                $rowset[$i]['batas_akhir']  = $key -> end_date;
                $rowset[$i]['detail']  = "<a href='history-project/".$key->slug."' class='btn btn-xs btn-primary'>Lihat Laporan</a>";
                $i++;
            }
        }
        return view('backend.project',compact('rowset'));
    }

    public function history_survey_by_project($slug){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        //ambil survey yang sudah memiliki laporan berdasarkan project
        $id_user        = Auth::user() -> id;
        $info_project   = $this -> survey -> get_info_project_by_slug($slug);
        $id_project     = $info_project['id'];
        $history_survey = $this -> survey -> get_all_history_survey_by_project($id_project, $id_user);
        return view('backend.history', compact('history_survey', 'info_project'));
    }
}
