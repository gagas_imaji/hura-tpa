<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use Validator;
use File;
use Image;
use Auth;
use Entrust;
use App\Notifications\ActivationToken;
use App\Notifications\UserStatusChange;

use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\QuestionType;
use App\Survey;
use App\SectionSurvey;
use App\Question;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use DB;
use SimpleExcel\SimpleExcel;
use Carbon\Carbon;

class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(QuestionRepositoryInterface $question, SurveyRepositoryInterface $survey, OptionAnswerRepositoryInterface $option_answer)
    {
        $this -> question   = $question;
        $this -> survey     = $survey;
        $this -> option_answer     = $option_answer;
    }
 
    public function index(Request $request){
        if(Entrust::can('create-question')){
            $id_survey = $request -> input('id_survey');
            $survey = $this -> survey -> get_survey_by_id($id_survey);
            // $survey    = Survey::where('id', '=', $request -> input('id_survey'))->get();
            return view('question.index', compact('survey')); 
        }else{
            return redirect('/home');
        }
    }

    public function import(Request $request){
        if(Entrust::can('create-question')){

            $id_survey = $request -> input('id_survey');
            $question_type = QuestionType::get();
            $section = SectionSurvey::where('survey_id', '=', $id_survey) -> get();
            return view('question.import', compact('question_type', 'id_survey', 'section'));
        }else{
            return redirect('/home');
        }
    }

    public function doImport(Request $request){

        if(Entrust::can('create-question')){
            $input = $request->all();
            if(isset($input['submit'])){
                
                DB::beginTransaction();
                try{

                    $excel          = new SimpleExcel('CSV');
                    $target         = basename($_FILES['file_question']['name']) ;
                    $file_move      = $request->file('file_question')->move(public_path("uploads\data"), $target);
                    $lines          = file($file_move);    
                    $count          = count($lines);
                    $files          = $excel->parser->loadFile($file_move, 'CSV');

                    //ambil data dari file csv
                    for($i = 1; $i <= $count; $i++){
                        $isi_file[$i]   = $excel->parser->getCell($i, 1);
                        $data[$i]       = explode(';', $isi_file[$i]);
                    }

                    //cek format file csv
                    if($input['question_type'] == 1 || $input['question_type'] == 2){

                        if(count($data[1]) != 18){ //jumlah kolom
                            return redirect()->back()->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                        }else{ //urutan kolom
                            if($data[1][0] != 'Kategori Survey' || 
                                $data[1][1] != 'Judul Survey' || 
                                $data[1][2] != 'Tanggal Awal' || 
                                $data[1][3] != 'Tanggal Akhir' || 
                                $data[1][4] != 'Pertanyaan' || 
                                $data[1][5] != 'Random' || 
                                $data[1][6] != 'Timer' || 
                                $data[1][7] != 'Mandatory' || 
                                $data[1][8] != 'Jawaban1' || 
                                $data[1][9] != 'Bobot1' ||
                                $data[1][10] != 'Jawaban2' || 
                                $data[1][11] != 'Bobot2' ||
                                $data[1][12] != 'Jawaban3' || 
                                $data[1][13] != 'Bobot3' ||
                                $data[1][14] != 'Jawaban4' || 
                                $data[1][15] != 'Bobot4' ||
                                $data[1][16] != 'Jawaban5' || 
                                $data[1][17] != 'Bobot5'){
                                return redirect()->back()->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                            }
                        }
                    }elseif($input['question_type'] == 3){
                        if(count($data[1]) != 9){ //jumlah kolom
                            return redirect()->back()->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                        }else{ //urutan kolom
                            if($data[1][0] != 'Kategori Survey' || 
                                $data[1][1] != 'Judul Survey' || 
                                $data[1][2] != 'Tanggal Awal' || 
                                $data[1][3] != 'Tanggal Akhir' || 
                                $data[1][4] != 'Pertanyaan' || 
                                $data[1][5] != 'Timer' || 
                                $data[1][6] != 'Mandatory' || 
                                $data[1][7] != 'Jawaban'  ||
                                $data[1][8] != 'Bobot'){
                                return redirect()->back()->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                            }
                        }
                    }

                    //ambil data survey dari baris 2 file csv, karena baris pertama adalah header
                    if($data[2][0] == '360-Test'){
                        $category_survey    = 1;
                    }elseif($data[2][0] == 'Rec-Test'){
                        $category_survey    = 2;
                    }elseif($data[2][0] == 'Psy-Test'){
                        $category_survey    = 3;
                    }elseif($data[2][0] == 'Apt-Test'){
                        $category_survey    = 4;
                    }

                    $survey             = $data[2][1];
                    $description        = 'Survey ini dilakukan untuk meningkatkan kinerja dan dapat juga untuk memperbaiki komunikasi dengan orang lain';
                    $thankyou_text      = 'Terima kasih telah menyelesaikan survey ini';
                    
                    if($input['question_type'] == 1){
                        $instruction        = 'Silahkan pilih salah satu jawaban yang benar.';
                    }elseif($input['question_type'] == 2){
                        $instruction        = 'Silahkan pilih jawaban yang benar. Jawaban boleh lebih dari satu';
                    }elseif($input['question_type'] == 3){
                        $instruction        = 'Silahkan jawab semua pertanyaan dengan benar. ';
                    }  

                    $start      = strtotime($data[2][2]);
                    $start_date = date('Y-m-d', $start);
                    $end        = strtotime($data[2][3]);
                    $end_date   = date('Y-m-d', $end);
                    //cek format tanggal pada csv file [m-d-Y]
                    if(!$end || !$start){
                        return redirect()->back()->withErrors('Silahkan perbaiki format tanggal pada file .csv');
                    }
                    $add_survey         = DB::table('survey')->insertGetId([
                        'category_survey_id'=> $category_survey,
                        'title'             => $survey,
                        'slug'              => strtolower(str_replace(' ', '-', $survey)),
                        'description'       => $description,
                        'instruction'       => $instruction,
                        'thankyou_text'     => $thankyou_text,
                        'user_id'           => Auth::user()->id,
                        'start_date'        => $start_date,
                        'end_date'          => $end_date
                    ]);

                    $add_section        = DB::table('section_survey')->insertGetId([
                        'survey_id'     => $add_survey,
                        'name'          => '',
                        'slug'          => ''
                    ]);

                    //insert user_survey untuk survey yang telah ditambahkan
                    $id_corporate = DB::table('profiles')->select('id_corporate')->where('user_id', '=', Auth::user()->id)->get();
                    foreach ($id_corporate as $key){
                        $id_corporate = $key -> id_corporate;
                        $user = DB::table('profiles')->where('id_corporate', '=', $id_corporate)->get();
                        foreach ($user as $users) {
                            // echo $users->user_id;
                            DB::table('user_survey')->insert([
                                'id_survey'         => $add_survey,
                                'id_user'           => $users->user_id
                            ]);
                        }
                    }
                    
                    //ambil data pertanyaan mulai baris 2 file csv
                    for($i = 2; $i <= $count; $i++) {
                        
                        //input data pertanyaan untuk tipe multiple choice
                        if($input['question_type'] == 1 || $input['question_type'] == 2){
                            $add_question   = DB::table('question')->insertGetId([
                                'id_type_question'  => $request['question_type'],
                                'id_section'        => $add_section,
                                'question'          => $data[$i][4],
                                'is_random'         => $data[$i][5],
                                'timer'             => $data[$i][6],
                                'is_mandatory'      => $data[$i][7]
                            ]);
                        }elseif($input['question_type'] == 3){ //input data pertanyaan untuk tipe open text
                            $add_question   = DB::table('question')->insertGetId([
                                'id_type_question'  => $request['question_type'],
                                'id_section'        => $add_section,
                                'question'          => $data[$i][4],
                                'is_random'         => 0,
                                'timer'             => $data[$i][5],
                                'is_mandatory'      => $data[$i][6]
                            ]);
                        }

                        //input pilihan jawaban untuk tipe multiple choice
                        if($input['question_type'] == 1 || $input['question_type'] == 2){
                            $k = 9; //untuk indeks point pilihan jawaban
                            //ambil data pilihan jawaban mulai dari kolom 8 pada baris pertanyaan
                            for($j = 8; $j <= 17;){
                                /*$answer[$i][$j]     = $data[$i][$j];
                                $point[$i][$j]      = $data[$i][$k];*/

                                DB::table('option_answer')->insertGetId([
                                    'id_question'   => $add_question,
                                    'answer'        => $data[$i][$j],
                                    'point'         => $data[$i][$k],
                                    'image'         => '',
                                    'hidden'        => 0
                                ]);
                                $j = $j+2;
                                $k = $k+2;
                            }
                        }elseif($input['question_type'] == 3){
                            
                            $lowercase          = strtolower($data[$i][7]);
                            $no_space           = str_replace(' ', '-', $lowercase); // Replaces all spaces with hyphens.
                            $alphanumeric       = preg_replace('/[^A-Za-z0-9\-]/', '', $no_space); // Removes special chars.
                            $answer             = preg_replace('/-+/', '', $no_space); // Replaces multiple hyphens with single one

                            DB::table('option_answer')->insertGetId([
                                'id_question'   => $add_question,
                                'answer'        => $answer, //kolom 5 untuk jawaban
                                'point'         => $data[$i][8], //kolom 6 untuk point
                                'image'         => '',
                                'hidden'        => 0,
                                'display_answer'=> $data[$i][7]
                            ]);
                        }
                        
                        //input pilihan jawaban default [No Answer]
                        DB::table('option_answer')->insertGetId([
                            'id_question'   => $add_question,
                            'answer'        => 'No Answer',
                            'point'         => 0,
                            'image'         => '',
                            'hidden'        => 1
                        ]);
                    }

                    DB::commit();
                    return redirect()->back()->withSuccess('Question Inserted');
                }catch (Exception $e){
                    DB::rollback();
                    return $e;
                }
            }
        }else{
            return redirect('/home');
        }
    }

    public function doImportQuestionToSection(Request $request){

        if(Entrust::can('create-question')){
            $input = $request->all();
            if(isset($input['submit'])){
                
                DB::beginTransaction();
                try{
                    
                    $excel          = new SimpleExcel('CSV');
                    $target         = basename($_FILES['file_question']['name']) ;
                    $file_move      = $request->file('file_question')->move(public_path("uploads\data"), $target);
                    $lines          = file($file_move);    
                    $count          = count($lines);
                    $files          = $excel->parser->loadFile($file_move, 'CSV');

                    //ambil data dari file csv
                    for($i = 1; $i <= $count; $i++){
                        $isi_file[$i]   =  $excel->parser->getCell($i, 1);
                        $data[$i]       = explode(';', $isi_file[$i]);
                    }

                    //cek format file csv
                    if($input['question_type'] == 1 || $input['question_type'] == 2){
                        if($input['number_choice'] == 2){
                            if(count($data[1]) != 8){ //jumlah kolom
                                return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                            }else{ //urutan kolom
                                if( $data[1][0] != 'Pertanyaan' || 
                                    $data[1][1] != 'Random' || 
                                    $data[1][2] != 'Timer' || 
                                    $data[1][3] != 'Mandatory' || 
                                    $data[1][4] != 'Jawaban1' || 
                                    $data[1][5] != 'Bobot1' ||
                                    $data[1][6] != 'Jawaban2' || 
                                    $data[1][7] != 'Bobot2'
                                    ){
                                    return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                                }
                            }
                        }elseif($input['number_choice'] == 3){
                            if(count($data[1]) != 10){ //jumlah kolom
                                return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                            }else{ //urutan kolom
                                if( $data[1][0] != 'Pertanyaan' || 
                                    $data[1][1] != 'Random' || 
                                    $data[1][2] != 'Timer' || 
                                    $data[1][3] != 'Mandatory' || 
                                    $data[1][4] != 'Jawaban1' || 
                                    $data[1][5] != 'Bobot1' ||
                                    $data[1][6] != 'Jawaban2' || 
                                    $data[1][7] != 'Bobot2' ||
                                    $data[1][8] != 'Jawaban3' || 
                                    $data[1][9] != 'Bobot3'
                                    ){
                                    return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                                }
                            }
                        }elseif($input['number_choice'] == 4){
                            if(count($data[1]) != 12){ //jumlah kolom
                                return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                            }else{ //urutan kolom
                                if( $data[1][0] != 'Pertanyaan' || 
                                    $data[1][1] != 'Random' || 
                                    $data[1][2] != 'Timer' || 
                                    $data[1][3] != 'Mandatory' || 
                                    $data[1][4] != 'Jawaban1' || 
                                    $data[1][5] != 'Bobot1' ||
                                    $data[1][6] != 'Jawaban2' || 
                                    $data[1][7] != 'Bobot2' ||
                                    $data[1][8] != 'Jawaban3' || 
                                    $data[1][9] != 'Bobot3' ||
                                    $data[1][10] != 'Jawaban4' || 
                                    $data[1][11] != 'Bobot4'
                                    ){
                                    return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                                }
                            }
                        }elseif($input['number_choice'] == 5){
                            if(count($data[1]) != 14){ //jumlah kolom
                                return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                            }else{ //urutan kolom
                                if( $data[1][0] != 'Pertanyaan' || 
                                    $data[1][1] != 'Random' || 
                                    $data[1][2] != 'Timer' || 
                                    $data[1][3] != 'Mandatory' || 
                                    $data[1][4] != 'Jawaban1' || 
                                    $data[1][5] != 'Bobot1' ||
                                    $data[1][6] != 'Jawaban2' || 
                                    $data[1][7] != 'Bobot2' ||
                                    $data[1][8] != 'Jawaban3' || 
                                    $data[1][9] != 'Bobot3' ||
                                    $data[1][10] != 'Jawaban4' || 
                                    $data[1][11] != 'Bobot4' ||
                                    $data[1][12] != 'Jawaban5' || 
                                    $data[1][13] != 'Bobot5'
                                    ){
                                    return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                                }
                            }
                        }elseif($input['number_choice'] == 6){
                            if(count($data[1]) != 16){ //jumlah kolom
                                return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                            }else{ //urutan kolom
                                if( $data[1][0] != 'Pertanyaan' || 
                                    $data[1][1] != 'Random' || 
                                    $data[1][2] != 'Timer' || 
                                    $data[1][3] != 'Mandatory' || 
                                    $data[1][4] != 'Jawaban1' || 
                                    $data[1][5] != 'Bobot1' ||
                                    $data[1][6] != 'Jawaban2' || 
                                    $data[1][7] != 'Bobot2' ||
                                    $data[1][8] != 'Jawaban3' || 
                                    $data[1][9] != 'Bobot3' ||
                                    $data[1][10] != 'Jawaban4' || 
                                    $data[1][11] != 'Bobot4' ||
                                    $data[1][12] != 'Jawaban5' || 
                                    $data[1][13] != 'Bobot5' ||
                                    $data[1][14] != 'Jawaban6' || 
                                    $data[1][15] != 'Bobot6'
                                    ){
                                    return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                                }
                            }
                        }elseif($input['number_choice'] == 7){
                            if(count($data[1]) != 18){ //jumlah kolom
                                return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                            }else{ //urutan kolom
                                if( $data[1][0] != 'Pertanyaan' || 
                                    $data[1][1] != 'Random' || 
                                    $data[1][2] != 'Timer' || 
                                    $data[1][3] != 'Mandatory' || 
                                    $data[1][4] != 'Jawaban1' || 
                                    $data[1][5] != 'Bobot1' ||
                                    $data[1][6] != 'Jawaban2' || 
                                    $data[1][7] != 'Bobot2' ||
                                    $data[1][8] != 'Jawaban3' || 
                                    $data[1][9] != 'Bobot3' ||
                                    $data[1][10] != 'Jawaban4' || 
                                    $data[1][11] != 'Bobot4' ||
                                    $data[1][12] != 'Jawaban5' || 
                                    $data[1][13] != 'Bobot5' ||
                                    $data[1][14] != 'Jawaban6' || 
                                    $data[1][15] != 'Bobot6' ||
                                    $data[1][16] != 'Jawaban7' || 
                                    $data[1][17] != 'Bobot7'
                                    ){
                                    return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                                }
                            }
                        }else{
                            return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan pilih jumlah opsi jawaban sesuai dengan pilihan yang ada');
                        }
                    }elseif($input['question_type'] == 3 || $input['question_type'] == 4){
                        if(count($data[1]) != 5){ //jumlah kolom
                            return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format kolom pada file .csv sesuai contoh yang disediakan');
                        }else{ //urutan kolom
                            if( $data[1][0] != 'Pertanyaan' || 
                                $data[1][1] != 'Timer' || 
                                $data[1][2] != 'Mandatory' || 
                                $data[1][3] != 'Jawaban'  ||
                                $data[1][4] != 'Bobot'){
                                return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan perbaiki format urutan kolom pada file .csv sesuai contoh yang disediakan');
                            }
                        }
                    }

                    //ambil data pertanyaan dari baris 2 file csv, karena baris pertama adalah header
                    for($i = 2; $i <= $count; $i++) {
                        
                        //input data pertanyaan untuk tipe multiple choice
                        if($input['question_type'] == 1 || $input['question_type'] == 2){
                            $add_question   = DB::table('question')->insertGetId([
                                'id_type_question'  => $input['question_type'],
                                'id_section'        => $input['section'],
                                'question'          => $data[$i][0],
                                'is_random'         => $data[$i][1],
                                'timer'             => $data[$i][2],
                                'is_mandatory'      => $data[$i][3]
                            ]);
                        }elseif($input['question_type'] == 3){ //input data pertanyaan untuk tipe open text
                            $add_question   = DB::table('question')->insertGetId([
                                'id_type_question'  => $input['question_type'],
                                'id_section'        => $input['section'],
                                'question'          => $data[$i][0],
                                'is_random'         => 0,
                                'timer'             => $data[$i][1],
                                'is_mandatory'      => $data[$i][2]
                            ]);
                        }elseif($input['question_type'] == 4){ //input data pertanyaan untuk tipe matching
                            $add_question   = DB::table('question')->insertGetId([
                                'id_type_question'  => $input['question_type'],
                                'id_section'        => $input['section'],
                                'question'          => $data[$i][0],
                                'is_random'         => 1,
                                'timer'             => $data[$i][1],
                                'is_mandatory'      => $data[$i][2]
                            ]);
                        }

                        //input pilihan jawaban untuk tipe multiple choice
                        if($input['question_type'] == 1 || $input['question_type'] == 2){
                            $k = 5; //untuk indeks point pilihan jawaban
                            if($input['number_choice'] == 2){
                                $count_option = 7;
                            }elseif($input['number_choice'] == 3){
                                $count_option = 9;
                            }elseif($input['number_choice'] == 4){
                                $count_option = 11;
                            }elseif($input['number_choice'] == 5){
                                $count_option = 13;
                            }elseif($input['number_choice'] == 6){
                                $count_option = 15;
                            }elseif($input['number_choice'] == 7){
                                $count_option = 17;
                            }else{
                                return redirect('survey/'.$input['id_survey'])->withErrors('Silahkan pilih jumlah opsi jawaban sesuai dengan pilihan yang ada');
                            }

                            for($j = 4; $j <= $count_option;){
                                /*$answer[$i][$j]     = $data[$i][$j];
                                $point[$i][$j]      = $data[$i][$k];*/
                                $extension = '';
                                //cek apakah opsi merupakan image
                                if(strlen($data[$i][$j]) > 4){ // 4 (empat) untuk ekstensi
                                    for($a = strlen($data[$i][$j])-4; $a < strlen($data[$i][$j]); $a++){
                                        $extension .= $data[$i][$j][$a];
                                    }
                                }

                                if($extension == '.png' || $extension == '.jpg' || $extension == '.PNG' || $extension == '.JPG'){
                                    $image = $data[$i][$j];
                                }else{
                                    $image = '';
                                }
                                DB::table('option_answer')->insertGetId([
                                    'id_question'   => $add_question,
                                    'answer'        => $data[$i][$j],
                                    'point'         => $data[$i][$k],
                                    'image'         => $image,
                                    'hidden'        => 0
                                ]);
                                $j = $j+2;
                                $k = $k+2;
                            }
                        }elseif($input['question_type'] == 3){
                            
                            $lowercase          = strtolower($data[$i][3]);
                            $no_space           = str_replace(' ', '-', $lowercase); // Replaces all spaces with hyphens.
                            $alphanumeric       = preg_replace('/[^A-Za-z0-9\-]/', '', $no_space); // Removes special chars.
                            $answer             = preg_replace('/-+/', '', $no_space); // Replaces multiple hyphens with single one

                            DB::table('option_answer')->insertGetId([
                                'id_question'   => $add_question,
                                'answer'        => $answer, //kolom 3 untuk jawaban
                                'point'         => $data[$i][4], //kolom 4 untuk point
                                'image'         => '',
                                'hidden'        => 0,
                                'display_answer'=> $data[$i][3]
                            ]);
                        }elseif($input['question_type'] == 4){
                          
                            DB::table('option_answer')->insertGetId([
                                'id_question'   => $add_question,
                                'answer'        => $data[$i][3], //kolom 3 untuk jawaban
                                'point'         => $data[$i][4], //kolom 4 untuk point
                                'image'         => '',
                                'hidden'        => 0
                            ]);
                        }
                        
                        //input pilihan jawaban default [No Answer]
                        DB::table('option_answer')->insertGetId([
                            'id_question'   => $add_question,
                            'answer'        => 'No Answer',
                            'point'         => 0,
                            'image'         => '',
                            'hidden'        => 1
                        ]);
                    }
                    //check nama section
                    $name_section = $this -> survey -> get_info_section($input['section']);
                    if($name_section['name'] == 'Reading Text'){
                        //insert paragraph article
                        $insert_paragraph = $this -> survey -> insert_paragraph($input['paragraph'], $input['section']);
                    }
                    DB::commit();
                    return redirect('survey/'.$input['id_survey'])->withSuccess('Question Inserted');
                }catch (Exception $e){
                    DB::rollback();
                    return $e;
                }
            }
        }else{
            return redirect('/home');
        }
    }

    public function addQuestion(Request $request){
        if(Entrust::can('create-question')){
            $id_survey      = $request->input('id_survey');
            $survey         = Survey::select('title') -> whereId($id_survey) -> first();
            $name_survey    = $survey['title'];
            $option         = QuestionType::get();
            $section        = SectionSurvey::where('survey_id', '=', $id_survey) -> pluck('name', 'id');
            return view('question.create',compact('option', 'id_survey', 'section', 'name_survey'));
        }else{
            return redirect('/home');
        }
    }

    public function lists(Request $request){

        if(Entrust::can('manage-survey')){
            $question = $this -> question -> get_list_question_by_survey($request->id);
           
        }else{
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $rows = array();

        foreach($question as $questions){
            $row = array(
                '<div class="btn-group btn-group-xs">'.
                '<a href="/question-by-survey/'.$questions->id.'" class="btn btn-xs btn-default" id="view_question_'.$questions->id.'"> <i class="fa fa-arrow-circle-o-right" data-toggle="tooltip" title="'.trans('messages.view').'"></i></a>'.
                (Entrust::can('delete-question') ? delete_form(['question.destroy',$questions->id,'id'=>'question']) : '').
                '</div>',
                );

            if($questions->is_random == 0){
                $questions->is_random = 'No';
            }elseif($questions->is_random == 1){
                $questions->is_random = 'Yes';
            }

            if($questions->is_mandatory == 0){
                $questions->is_mandatory = 'No';
            }elseif($questions->is_mandatory == 1){
                $questions->is_mandatory = 'Yes';
            }

            array_push($row,$questions->type);
            array_push($row,$questions->section);
            array_push($row,$questions->question);
            array_push($row,$questions->is_random);
            array_push($row,$questions->timer);
            array_push($row,$questions->is_mandatory);
            array_push($row,$questions->point);

            $rows[] = $row;
        }
        $list['aaData'] = $rows;
        return json_encode($list);
    }

    public function show(Request $request, Question $question){

        if(!Entrust::can('manage-question')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }


        //data-table list question
        $col_heads = array(
            trans('messages.option')
        );
        $question = $this -> question -> get_detail_question_by_survey($request->id);
        if(!config('config.login')){
            array_push($col_heads, trans('messages.username'));
        }

        $option_answer = $this -> option_answer -> get_all_by_question($request->id);
        return view('question.show',compact('question', 'option_answer'));
    }

    public function destroy(Question $question, Request $request, $param=''){
        $question->delete();
        if($request->has('ajax_submit')){
            $response = ['message' => 'Question deleted', 'status' => 'success']; 
            return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
        return redirect()->back()->withSuccess('Survey deleted');
    }

    public function detailUpdate(Request $request, $id){

       /* if(!Entrust::can('update-user') || (!Entrust::hasRole(DEFAULT_ROLE) && $user->hasRole(DEFAULT_ROLE)) )
            return redirect('/home')->withErrors(trans('messages.permission_denied'));*/
        
        if(!Entrust::can('update-question')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $validator = Validator::make(Input::all(), [
            'question'      => 'required',
            'is_random'     => 'required',
            'timer'         => 'required|numeric|min:0',
            'is_mandatory'  => 'required',
            'point'         => 'required'
        ]);

        if ($validator->fails()) {
            return redirect() -> back() -> withErrors($validator);
        }       
        
        DB::beginTransaction();
        try{
            $question = $request -> input('question');
            $is_random = $request -> input('is_random');
            $timer = $request -> input('timer');
            $is_mandatory = $request -> input('is_mandatory');
            $point = $request -> input('point');

            $this -> question -> update_info_question($id, $question, $is_random, $is_mandatory, $timer, $point);
            
            DB::commit();
            if($request->has('ajax_submit')){
                $response = ['Question successfully updated', 'status' => 'success']; 
                return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
            }
            return redirect()->back()->withSuccess('Question successfully updated');
        }catch(exception $e){
            DB::rollback();
            return false;
        }
        
    }

    public function optionAnswerUpdate(Request $request){

        $validator = Validator::make(Input::all(), [
            'id_option_answer'      => 'required',
            'option_answer'         => 'required',
            'point_option_answer'   => 'required'
        ]);

        if ($validator->fails()) {
            return redirect() -> back() -> withErrors($validator);
        }  

        DB::beginTransaction();
        try{
            $id_option_answer       = $request -> input('id_option_answer');
            $option_answer          = $request -> input('option_answer');
            $point_option_answer    = $request -> input('point_option_answer');

            $this -> option_answer -> update_answer_info_on_question($id_option_answer, $option_answer, $point_option_answer);

            if($request->has('ajax_submit')){
                $response = ['Option answer successfully updated', 'status' => 'success']; 
                return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
            }
            return redirect()->back()->withSuccess('Option answer successfully updated');
        }catch(exception $e){
            DB::rollback();
            return false;
        }
    }
}
