<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AspekPsikologis;
use DB;
use Illuminate\Support\Str;
use Entrust;
use Auth;
use App\UserReport;
use App\UserReportAspek;
use App\Library\Kalkulasi;
use App\StandarNilai;
use Validator;

use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Talenthub\Repository\StandarNilaiRepositoryInterface;

class AspekPsikographController extends Controller
{
    public function __construct(SurveyRepositoryInterface $survey, QuestionRepositoryInterface $question, OptionAnswerRepositoryInterface $option_answer, ResultRepositoryInterface $result, StandarNilaiRepositoryInterface $standar_nilai)
    {
        $this->survey        = $survey;
        $this->question      = $question;
        $this->option_answer = $option_answer;
        $this->result        = $result;
        $this->standar_nilai = $standar_nilai;
    }

    public function index()
    {
    	if(Auth::check() && !Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $listAspeks = AspekPsikologis::with('tpaKategoriAspek')->get();
        $datas      = StandarNilai::with('tpaStandarNilaiProject')->get();
        return view('aspek_psikograph.aspek', compact('listAspeks','datas'));
    }

    public function editStandarNilaiPsikograph($id)
    {
        $detail = StandarNilai::where('id',$id)->first();
        if (!is_null($detail)) {
            $listAspeks = AspekPsikologis::get();
            $namaJabatan = $detail->nama_jabatan;
            $standarNilaiId  = $id;
            return view('standar_nilai.edit_standar_nilai_jabatan',compact('listAspeks','namaJabatan','standarNilaiId', 'detail'));
        } else {
            return redirect()->back()->withErrors('Data Not Found');
        }
    }

    public function saveStandarNilaiPsikograph(Request $request)
    {
        $param = $request->all();
        $validator = Validator::make($param, [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $save_standar = $this->standar_nilai->update_save_standar($param);
        if($save_standar == false){
            return redirect()->back()->withErrors('Failed to save');
        } else {
            $standarNilaiId = $param['posisi'];
            $kalkulasi      = Kalkulasi::updateNilaiKecocokanAfterUpdateStandarNilaiPsikograph($standarNilaiId);
            return redirect()->back()->withSuccess('Save Standar Nilai Succeed');
        }

    }

    public function addStandarNilai()
    {
        if(Entrust::can('create-survey')){
            $listAspeks              = AspekPsikologis::get();
            $listStandarNilaiProject = StandarNilai::get();
            return view('standar_nilai.add_standar_nilai_jabatan', compact('listAspeks','listStandarNilaiProject'));
        }else{
            return redirect('/home');
        }
    }

    public function createStandarNilai(Request $request)
    {
        $param = $request->all();
        $validator = Validator::make($param, [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $save_standar = $this->standar_nilai->create_standar_nilai($param);
        if($save_standar == false){
            return redirect()->back()->withErrors('Failed to save');
        } else {
            return redirect()->back()->withSuccess('Save Standar Nilai Succeed');
        }
    }
}
