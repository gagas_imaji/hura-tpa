<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Entrust;
use Auth;
use App\Http\Requests;

use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Survey;
use App\SurveyCategory;
use App\SurveyQuarter;
use App\Question;
use App\Result;
use App\Profile;
use App\Division;
use App\OptionSection;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
use Validator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;
use App\Mail\CompleteTools;

class SurveyController extends Controller
{
    public function __construct(SurveyRepositoryInterface $survey, QuestionRepositoryInterface $question, OptionAnswerRepositoryInterface $option_answer, ResultRepositoryInterface $result)
    {
        $this -> survey         = $survey;
        $this -> question       = $question;
        $this -> option_answer  = $option_answer;
        $this -> result         = $result;
    }

    public function index(){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $col_heads = array(
            trans('messages.option')
        );

        array_push($col_heads, trans('messages.title'));
        array_push($col_heads, 'Status');
        array_push($col_heads, 'Created Date');
        array_push($col_heads, 'Start Date');
        array_push($col_heads, 'End Date');


        $table_data['survey-table'] = array(
            'source' => 'survey',
            'title' => 'Survey List',
            'id' => 'survey_table',
            'data' => $col_heads
            );

        $assets = ['recaptcha'];

        return view('survey.index',compact('table_data','assets'));
    }

    public function lists(Request $request){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $survey = $this -> survey -> get_list_survey_data_table();
        $rows = array();
        foreach($survey as $survey){
            $row = array(
                '<div class="btn-group btn-group-xs">
                    <a href="/survey/'.$survey->id.'" class="btn btn-xs btn-default" id="view-tools-'.$survey->id.'">
                        <i class="fa fa-arrow-circle-o-right" data-toggle="tooltip" title="'.trans('messages.view').'"></i>
                    </a>
                </div>',
                );

            array_push($row,$survey->title);
            if($survey->status == 1){
                $status = '<span class="label label-info">Finished</span>';
            }else{
                $status = '<span class="label label-success">Ongoing</span>';
            }
            array_push($row,$status);
            array_push($row,date('Y-m-d H:i:s', strtotime($survey->created_at)));
            array_push($row,$survey->start_date);
            array_push($row,$survey->end_date);

            $rows[] = $row;
        }
        $list['aaData'] = $rows;
        return json_encode($list);
    }

    public function show(Survey $survey){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $project = DB::table('project')->where('id', '=', $survey->id_project)->first();
        $custom_social_field_values = getCustomFieldValues('survey-social-form',$survey->id);
        $custom_register_field_values = getCustomFieldValues('survey-registration-form-form',$survey->id);

        //data-table list question
        $col_heads = array(
            trans('messages.option')
        );

        if(!config('config.login')){
            array_push($col_heads, trans('messages.username'));
        }

        array_push($col_heads, 'Type');
        array_push($col_heads, 'Section');
        array_push($col_heads, 'Question');
        array_push($col_heads, 'Random Option');
        array_push($col_heads, 'Timer');
        array_push($col_heads, 'Mandatory');
        array_push($col_heads, 'Point');

        $table_data['question-table'] = array(
            'source' => 'question/'.$survey->id,
            'title' => 'Question List',
            'id' => 'question_table',
            'data' => $col_heads
            );

        $assets = ['recaptcha'];

        $section = $this -> survey -> get_all_section_on_survey($survey -> id);
        return view('survey.show',compact('survey','custom_social_field_values','custom_register_field_values', 'table_data', 'section', 'project'));
    }

    public function destroy(Survey $survey, Request $request, $param=''){
        $survey->delete();
        if($request->has('ajax_submit')){
            $response = ['message' => 'Tools deleted', 'status' => 'success'];
            return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
        return redirect()->back()->withSuccess('Survey deleted');
    }

    public function detailUpdate(Request $request, $id){

       /* if(!Entrust::can('update-user') || (!Entrust::hasRole(DEFAULT_ROLE) && $user->hasRole(DEFAULT_ROLE)) )
            return redirect('/home')->withErrors(trans('messages.permission_denied'));*/

        if(!Entrust::can('update-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $validator = Validator::make(Input::all(), [
            'title'         => 'required',
            'description'   => 'required',
            'instruction'   => 'required',
            'thankyou_text' => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect() -> back() -> withErrors($validator);
        }

        DB::beginTransaction();
        try{
            $survey = Survey::find($id);

            if(!$survey)
                return redirect('/survey')->withErrors(trans('messages.invalid_link'));

            $start   = strtotime($request -> input('start_date'));
            $end     = strtotime($request -> input('end_date'));
            $request['start_date']  = date('Y-m-d', $start);
            $request['end_date']    = date('Y-m-d', $end);

            $survey->fill($request->all());
            $survey->save();

            $section_id             = $request -> input('section_id');
            $section_name           = $request -> input('section_name');
            $section_limit          = $request -> input('section_limit');
            $section_instruction    = $request -> input('section_instruction');
            $section_timer          = $request -> input('section_timer');

            $this -> survey -> update_section_info_on_survey($section_id, $section_name, $section_limit, $section_instruction, $section_timer);

            DB::commit();
            if($request->has('ajax_submit')){
                $response = ['Test successfully updated', 'status' => 'success'];
                return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
            }
            return redirect()->back()->withSuccess('Test successfully updated');
        }catch(exception $e){
            DB::rollback();
            return false;
        }

    }



    public function create(){
        if(Entrust::can('create-survey')){
            $category    = SurveyCategory::select('name', 'id') -> get();
            $i = 0;
            foreach ($category as $key) {
                $survey_category[$i]['id']  = $key['id'];
                $survey_category[$i]['name']  = $key['name'];
                $i++;
            }
            return view('survey.create', compact('survey_category'));
        }else{
            return redirect('/home');
        }
    }

    public function getDescription($slug, $section){
        if(Entrust::can('take-test')){
            $data = $this -> survey -> get_info_survey_by_slug($slug);
            $info['description'] = $data['description'];
            $info['title']       = $data['title'];

            return view('survey.description', compact('slug', 'info', 'section'));
        }else{
            return redirect('/home');
        }
    }

    public function getInstruction($slug){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }
            $data['slug']           = $slug;
            $data['user_id']        = Auth::user() -> id;
            $survey_info            = $this -> survey -> get_info_survey_by_slug($slug);
            $data['title']          = $survey_info['title'];
            $thankyou_text          = $survey_info['thankyou_text'];

            //cek section yang belum dikerjakan
            $next_section = $this -> survey -> get_next_section($slug);
            //jika masih ada section berikutnya, ambil id_section berikutnya sebagai new_section
            if($next_section){
                $data['section_name'] = $next_section['name'];
                $data['section']      = $next_section['id'];

                if($data['section_name'] ==  'Subtest 1'){
                    $page_instruction = 'survey.tool-instruction-gti01';
                }elseif($data['section_name'] ==  'Subtest 2'){
                    $page_instruction = 'survey.tool-instruction-gti02';
                }elseif($data['section_name'] ==  'Subtest 3'){
                    $page_instruction = 'survey.tool-instruction-gti03';
                }elseif($data['section_name'] ==  'Subtest 4'){
                    $page_instruction = 'survey.tool-instruction-gti04';
                }elseif($data['section_name'] ==  'Subtest 5'){
                    $page_instruction = 'survey.tool-instruction-gti05';
                }elseif($data['section_name'] ==  'Berhitung Angka'){
                    $page_instruction = 'survey.tool-instruction-tiki01';
                }elseif($data['section_name'] ==  'Gabungan Bagian'){
                    $page_instruction = 'survey.tool-instruction-tiki02';
                }elseif($data['section_name'] ==  'Hubungan Kata'){
                    $page_instruction = 'survey.tool-instruction-tiki03';
                }elseif($data['section_name'] ==  'Abstraksi Non Verbal'){
                    $page_instruction = 'survey.tool-instruction-tiki04';
                }elseif($data['section_name'] ==  'Work Personality Analytics'){
                    $page_instruction = 'survey.tool-instruction-wpa';
                }elseif($data['section_name'] ==  'Work Behavioural Assessment'){
                    $page_instruction = 'survey.tool-instruction-papikostik';
                }elseif($data['section_name'] ==  'Task Performance'){
                    return redirect('survey/'.$slug.'/'.$section.'/start');
                }elseif($data['section_name'] ==  'Vigorous'){
                    return redirect('survey/'.$slug.'/'.$section.'/start');
                }

                Cache::flush();
                return view($page_instruction, compact('data'));
            }else{
                $info_survey            = $this -> survey -> get_info_survey_by_slug($slug);
                $data['slug']           = $slug;
                $data['thankyou_text']  = $info_survey['thankyou_text'];
                $data['category']       = $info_survey['category_survey_id'];
                $data['section']        = 0;
                $data['id_user']        = Auth::user() -> id;
                return view('survey.finish', compact('data'));
            }

    }

    public function getSimulasi($slug, $section, $simulasi){
        $data['slug']           = $slug;
        $data['section']        = $section;

        $page_instruction = 'survey.'.$simulasi;

        return view($page_instruction, compact('data'));
    }

    public function startNextSection($slug, $new_section){
        if(Entrust::can('take-test')){

            $section_info   = $this -> survey -> get_info_section($new_section);
            $instruction    = $section_info['instruction'];
            $data['section']= $new_section;
            $data['slug']   = $slug;
            $data['section_name'] = $section_info['name'];

            if($data['section_name'] ==  'Subtest 1'){
                $page_instruction = 'survey.tool-instruction-gti01';
            }elseif($data['section_name'] ==  'Subtest 2'){
                $page_instruction = 'survey.tool-instruction-gti02';
            }elseif($data['section_name'] ==  'Subtest 3'){
                $page_instruction = 'survey.tool-instruction-gti03';
            }elseif($data['section_name'] ==  'Subtest 4'){
                $page_instruction = 'survey.tool-instruction-gti04';
            }elseif($data['section_name'] ==  'Subtest 5'){
                $page_instruction = 'survey.tool-instruction-gti05';
            }elseif($data['section_name'] ==  'Berhitung Angka'){
                $page_instruction = 'survey.tool-instruction-tiki01';
            }elseif($data['section_name'] ==  'Gabungan Bagian'){
                $page_instruction = 'survey.tool-instruction-tiki02';
            }elseif($data['section_name'] ==  'Hubungan Kata'){
                $page_instruction = 'survey.tool-instruction-tiki03';
            }elseif($data['section_name'] ==  'Abstraksi Non Verbal'){
                $page_instruction = 'survey.tool-instruction-tiki04';
            }elseif($data['section_name'] ==  'Contextual Performance'){
                return redirect('survey/'.$slug.'/'.$new_section.'/start');
            }elseif($data['section_name'] ==  'Adaptive Performance'){
                return redirect('survey/'.$slug.'/'.$new_section.'/start');
            }elseif($data['section_name'] ==  'Counter Behaviour'){
                return redirect('survey/'.$slug.'/'.$new_section.'/start');
            }elseif($data['section_name'] ==  'Dedication'){
                return redirect('survey/'.$slug.'/'.$new_section.'/start');
            }elseif($data['section_name'] ==  'Absorbtion'){
                return redirect('survey/'.$slug.'/'.$new_section.'/start');
            }

            Cache::flush();
            return view($page_instruction, compact('data'));
        }else{
            return redirect('/home');
        }
    }

    public function saveToReport($slug, $user_id){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $survey_id      = $info_survey['id'];
        $survey_category= $info_survey['category_survey_id'];
        $sections        = $this -> survey -> get_section_by_survey($survey_id);

        foreach($sections as $section){
            $info_section   = $this -> survey -> get_info_section($section['id']);
            $section_name   = $info_section['name'];

            if($survey_category == 1){
                $ignore_id   = ["0"];
                $login_id = Auth::user()->id;

                if(in_array($login_id, $ignore_id)){
                    $point = 1;
                }else{

                    $point = $this -> result -> get_point_survey_gti($survey_id, $section['id']);
                }
                if(is_array($point)){
                    // foreach($point as $key){
                        $score['survey_id']     = $survey_id;
                        $score['section_id']    = $section['id'];
                        $score['user_id']       = $user_id;

                        //perhitungan score GTI
                        $true   = $point['sum'];
                        $false  = $point['count'] - $point['sum'];

                        /*
                            rumus adj setiap subtest :
                            adj = jumlah benar - (jumlah salah / opsi - 1)
                        */
                        if($section_name == 'Subtest 1'){ //letter check
                            $adj = floor($true - ($false/4));
                            if($adj < 0){ //batas minimum
                                $gtq = 74;
                            }else{
                                $tq = $this -> result -> get_gtq('lookup_gtq1', $adj);
                                $gtq = $tq -> tq;
                            }
                        }elseif($section_name == 'Subtest 2'){ //reasoning
                            $adj = floor($true - ($false/2));
                            if($adj < 0){
                                $gtq = 69;
                            }else{
                                $tq = $this -> result -> get_gtq('lookup_gtq2', $adj);
                                $gtq = $tq -> tq;
                            }
                        }elseif($section_name == 'Subtest 3'){ //letter distance
                            $adj = floor($true - ($false/1));
                            if($adj < 0){
                                $gtq = 69;
                            }else{
                                $tq = $this -> result -> get_gtq('lookup_gtq3', $adj);
                                $gtq = $tq -> tq;
                            }
                        }elseif($section_name == 'Subtest 4'){ //numerical
                            $adj = floor($true - ($false/2));
                            if($adj < 0){
                                $gtq = 69;
                            }else{
                                $tq = $this -> result -> get_gtq('lookup_gtq4', $adj);
                                $gtq = $tq -> tq;
                            }
                        }elseif($section_name == 'Subtest 5'){ //spatial
                            $adj = floor($true - ($false/3));
                            if($adj < 0){
                                $gtq = 69;
                            }else{
                                $tq = $this -> result -> get_gtq('lookup_gtq5', $adj);
                                $gtq = $tq -> tq;
                            }
                        }

                        $sum            = $gtq;
                        $score['score'] = $sum;
                    // }
                }else{ //jika waktu habis dan user tidak menjawab sama sekali, maka skor diambil yang terendah
                    $score['survey_id']     = $survey_id;
                    $score['section_id']    = $section['id'];
                    $score['user_id']       = $user_id;
                    $score['score']         = 69;
                }

                //update data di table user_report
                if($section_name == 'Subtest 1'){ //letter check
                    $this -> result -> update_user_report('GTQ1', $score['score']);
                }elseif($section_name == 'Subtest 2'){ //reasoning
                    $this -> result -> update_user_report('GTQ2', $score['score']);
                }elseif($section_name == 'Subtest 3'){ //letter distance
                    $this -> result -> update_user_report('GTQ3', $score['score']);
                }elseif($section_name == 'Subtest 4'){ //numerical
                    $this -> result -> update_user_report('GTQ4', $score['score']);
                }elseif($section_name == 'Subtest 5'){ //spatial
                    $this -> result -> update_user_report('GTQ5', $score['score']);
                }
            }elseif($survey_category == 2){

                //hitung jumlah jawaban benar dari tabel result

                $ignore_id  = ["0"];
                $login_id   = $user_id;
                if(in_array($login_id, $ignore_id))
                {
                    $point = 1;
                }else{
                   $point = $this -> result -> get_point_survey_tiki($survey_id, $section['id'], $section_name);
                }

                if($section_name == 'Berhitung Angka'){
                    $column = 'subtest1';
                }elseif($section_name == 'Gabungan Bagian'){
                    $column = 'subtest2';
                }elseif($section_name == 'Hubungan Kata'){
                    $column = 'subtest3';
                }elseif($section_name == 'Abstraksi Non Verbal'){
                    $column = 'subtest4';
                }

                //lookup standar score berdasarkan section dan jumlah jawaban benar
                $standar_score = $this -> result -> get_standard_score_tiki($column, $point);
                foreach ($standar_score as $std) {
                    $score['score']         = $std;
                }

                $score['survey_id']     = $survey_id;
                $score['section_id']    = $section['id'];
                $score['user_id']       = $user_id;

                //update data di table user_report
                if($section_name == 'Berhitung Angka'){ //letter check
                    $this -> result -> update_user_report('SS1', $score['score']);
                }elseif($section_name == 'Gabungan Bagian'){ //reasoning
                    $this -> result -> update_user_report('SS2', $score['score']);
                }elseif($section_name == 'Hubungan Kata'){ //letter distance
                    $this -> result -> update_user_report('SS3', $score['score']);
                }elseif($section_name == 'Abstraksi Non Verbal'){ //spatial
                    $this -> result -> update_user_report('SS4', $score['score']);
                }

            }

            //proses masukkan hasil score ke tabel report
            if(in_array($login_id, $ignore_id)){
                $point = 0;
            }else{
                $this -> result -> save_report($score);
            }
        }

        //ambil grade jika ada grade
        $score_final= $this -> result -> get_final_score_user($survey_id);
        $is_grade   = $this -> survey -> check_grade_survey($survey_id);

        //update grade user jika ada
        if($is_grade == true){
            $grade = $this -> survey -> get_grade($score_final, $survey_id);
            $this -> survey -> update_grade_user($survey_id, $grade, $score_final);
        }

        //update data di tabel user_report
        if(in_array($login_id, $ignore_id))
        {

        }else{

            $resume     = $this -> result -> get_report_gti_tiki($survey_id, $user_id);
            if($survey_category == 1){ //LAI, update kriteria
                $avg        = round(array_sum($resume)/count($resume), 1);
                $criteria   = $this -> result -> get_criteria_gti_report($avg);
                $this -> result -> update_user_report('kriteria', $criteria);
            }elseif($survey_category == 2){ //TIKI, update IQ
                $total  = array_sum($resume);
                if($total <= 29){ //batas bawah
                    $iq = 56;
                }elseif($total > 107){ //batas atas
                    $iq = 145;
                }else{ //lookup
                    $iq     = $this -> result -> get_iq_tiki($total);
                }
                $this -> result -> update_user_report('jumlah', $total);
                $this -> result -> update_user_report('IQ', $iq);
            }

            $this -> survey -> survey_complete($slug);
            $duration   = $this -> survey -> get_temp($survey_id);
            $this -> survey -> delete_temp_survey($slug);
        }

        //LAI dan TIKI
        $complete = check_project_complete($info_survey['id_project'], $user_id);
        if($complete == true){
          $this->completeEmail(Auth::user() -> email,$info_survey['id_project']);
        }

        Cache::flush();

        $data['finish']         = 'finish';
        $data['slug']           = $slug;
        $log['activity']        = 'Menyelesaikan survey '.$info_survey['title'];
        $log['user_id']         = $user_id;
        $log['reference_id']    = $survey_id;
        $log['ip']              = \Request::ip();
        $log['duration']        = $duration;
        $log['created_at']      = Carbon::now();
        $log['user_agent']      = \Request::header('User-Agent');
        save_activity($log);

        return 'true';

    }

    public function finish($slug, $section, $id_user){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey            = $this -> survey -> get_info_survey_by_slug($slug);
        $data['slug']           = $slug;
        $data['thankyou_text']  = $info_survey['thankyou_text'];
        $data['category']       = $info_survey['category_survey_id'];
        $data['section']        = $section;
        $data['id_user']        = $id_user;
        return view('survey.finish', compact('data'));
    }

    public function getQuestion($slug, $section){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey          = $info_survey['id'];
        $used_timer         = $this -> survey -> get_temp($id_survey);
        $id_category_survey = $info_survey['category_survey_id'];
        $expiresAt          = Carbon::now()->addMinutes(60);
        $user_id            = Auth::user() -> id;
        //cek jika survey merupakan kategory WPA, maka panggil fungsi test WPA. jika Papikostick, maka panggil fungsi Papikostic
        if($id_category_survey == 3){
            return redirect('survey/get-question-wpa/'.$slug.'/'.$section);
        }elseif($id_category_survey == 4){
            return redirect('survey/get-question-papi/'.$slug.'/'.$section);
        }else{

            //untuk keperluan progress bar
            $total          = $this -> question -> get_total_question_on_section($section);
            if(Cache::has('result-count-'.$slug.'-'.$user_id)){
                $count = Cache::get('result-count-'.$slug.'-'.$user_id);
            }else{
                Cache::put('result-count-'.$slug.'-'.$user_id, 0, $expiresAt);
                $count = Cache::get('result-count-'.$slug.'-'.$user_id);
            }

            //ambil info section.
            $section_info   = $this -> survey -> get_info_section($section);
            $instruction    = $section_info['instruction'];
            $section_name   = $section_info['name'];
            $section_slug   = $section_info['slug'];
            $section_limit  = $section_info['limit'];
            $folder_image   = '';
            for($i = 0; $i < strlen($section_slug)-11; $i++){
                $folder_image .= $section_slug[$i];
            }

            //ambil pertanyaan selanjutnya berdasarkan section_name
            if($section_info['name'] == 'Reading Text'){
                $questions = $this -> question -> get_question_survey_reading($slug, $section, $section_limit, $total);
            }elseif($section_info['name'] == 'Matching Question'){
                $questions = $this -> question -> get_question_survey_matching($slug, $section, $section_limit, $total);
            }elseif($section_info['name'] == 'Blank in Paragraph'){
                $questions = $this -> question -> get_question_survey_blank_in_paragraph($slug, $section, $section_limit, $total);
            }else{
                $questions = $this -> question -> get_question_survey($slug, $section);
            }



            if($section_info['name'] == 'Reading Text'){
                foreach ($questions as $key) {
                    $id_type_question   = $key -> id_type_question;
                    $id_question        = $key -> id_question;
                    $is_random          = $key -> is_random;
                    $paragraph          = $key -> paragraph;
                    $timer              = $key -> timer;
                    $timer_survey       = $key -> timer_survey;
                    $timer_section      = $key -> timer_section;

                    $option_answer[$id_question] = $this -> option_answer -> get_by_question_unhide($id_question, $is_random);
                }

                return view('survey.detail_reading', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'paragraph', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
            }elseif($section_info['name'] == 'Matching Question'){
                $i = 0;
                foreach ($questions as $key) {
                    $id_type_question   = $key -> id_type_question;
                    $id_question        = $key -> id_question;
                    $is_random          = $key -> is_random;
                    $timer              = $key -> timer;
                    $timer_survey       = $key -> timer_survey;
                    $timer_section      = $key -> timer_section;
                    $arr_id_question[$i]= $key -> id_question;
                    $i++;
                }
                $option_answer = $this -> option_answer -> get_option_matching($section, $is_random, $section_limit, $total, $arr_id_question);

                return view('survey.detail_matching', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
            }elseif($section_info['name'] == 'Blank in Paragraph'){
                $i = 0;
                foreach ($questions as $key) {
                    $id_type_question   = $key -> id_type_question;
                    $id_question        = $key -> id_question;
                    $is_random          = 1;
                    $timer              = $key -> timer;
                    $timer_survey       = $key -> timer_survey;
                    $timer_section      = $key -> timer_section;
                    $arr_id_question[$i]= $key -> id_question;
                    $i++;
                }
                $option_answer = $this -> option_answer -> get_option_matching($section, $is_random, $section_limit, $total, $arr_id_question);

                return view('survey.detail_blank_paragraph', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
            }else{
                $i = 0;

                //check cache, jika ada maka ambil cache simpan di variabel
                if(Cache::has('cache-'.$slug.'-'.$user_id)){
                    $timer                  = Cache::get('timer-'.$slug.'-'.$user_id);
                    $timer_survey           = Cache::get('timer-survey-'.$slug.'-'.$user_id);
                    $timer_section          = Cache::get('timer-section-'.$slug.'-'.$user_id);
                    $survey_title           = Cache::get('survey-title-'.$slug.'-'.$user_id);
                    $id_question            = Cache::get('id-question-'.$slug.'-'.$user_id);
                    $content_question       = Cache::get('question-'.$slug.'-'.$user_id);
                    $option_answer          = Cache::get('option-answer-'.$slug.'-'.$user_id);
                    return view('survey.detail', compact('id_survey', 'slug', 'questions', 'option_answer', 'used_timer', 'section', 'count', 'total', 'section_name', 'section_slug', 'instruction', 'timer', 'timer_survey', 'timer_section', 'survey_title', 'content_question', 'id_question', 'folder_image'));
                }else{
                    return redirect('/survey'.'/'.$slug.'/intro');
                }
            }

        }

    }

    public function clearCacheResult($slug){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $expiresAt      = Carbon::now()->addMinutes(60);
        Cache::flush();
        Cache::put('result-count-'.$slug.'-'.Auth::user()->id, 0, $expiresAt);
        return redirect('/survey'.'/'.$slug.'/intro');

    }


    public function setResultCache(Request $request){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
            $status         = 'add'; //untuk status edit atau tambah jawaban
            $expiresAt      = Carbon::now()->addMinutes(60);
            $id_question    = $request->input('id_question');
            $id_option      = $request->input('id_option');
            $timer          = $request->input('timer');
            $slug           = $request->input('slug');
            $section        = $request->input('section');
            $id_user        = $request->input('id_user');
            $id_type_question   = $request->input('id_type_question');
            $length_question    = $request->input('length_question');

            Cache::put('result-id-type-question-'.$slug, $id_type_question, $expiresAt);
            $result = array(
                'question'  => $id_question,
                'answer'    => $id_option,
                'timer'     => $timer
            );

            $array_result = array();
            if(Cache::has('cache-result-'.$slug.'-'.$id_user)){
                $array_result = Cache::get('result-'.$slug.'-'.$id_user);
            }else{
                Cache::put('cache-result-'.$slug.'-'.$id_user, 'yes', $expiresAt);
            }
            array_push($array_result, $result);

            $duplicate;
            $updated =  array();
            //proses pengecekan apakah ada nomor soal yang sama, jika sama artinya user edit/ganti jawaban
            $temp_nomor = [];
            $i = 0;
            foreach ($array_result as $key) {
                array_push($temp_nomor, $key['question']);
            }

            foreach (array_count_values($temp_nomor) as $key => $value){
                if($value > 1){
                    $duplicate = $value;
                }
            }

            //jika ada nomor soal yang diedit, maka proses mengubah array jawaban dengan jawaban yang baru
            if($duplicate){
                for($i = 0; $i < count($array_result); $i++){
                    if($array_result[$i]['question'] == $duplicate){
                        array_push($updated, $array_result[$i]['answer']);
                    }
                }
                $temp_cache = Cache::get('result-'.$slug.'-'.$id_user);
                if($temp_cache){
                    for($i = 0; $i < count($temp_cache); $i++){
                        if($temp_cache[$i]['question'] == $duplicate){
                            $temp_cache[$i]['answer'] = $updated[1];
                            $status = 'update';
                        }
                    }
                }
            }

            //jika tidak mengganti jawaban, maka add to cache

            if($status == 'update'){
                Cache::put('result-'.$slug.'-'.$id_user, $temp_cache, $expiresAt); //replace jawaban
            }elseif($status != 'update'){
                //cek apakah semua soal sudah terjawab, jika belum maka simpan ke cache (jlh cache != jlh soal), jika sudah maka stop.
                if(Cache::get('result-count-'.$slug.'-'.$id_user)+1 <= $length_question){
                    Cache::put('result-'.$slug.'-'.$id_user, $array_result, $expiresAt);
                    Cache::put('result-count-'.$slug.'-'.$id_user, count($array_result), $expiresAt);
                    // Cache::put('result-timer-'.$slug.'-'.$id_user, $timer, $expiresAt);
                }
            }

            if(count($array_result)+1 > $length_question) {

                return 'finish';
                // return redirect('/survey/save-cache-to-db/'.$slug.'/'.$section);
            }else{
                return count($array_result);
            }

    }

    public function saveCacheToDataResult($slug, $section){
        if(!Entrust::can('take-test')){
             return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
            $ignore_id   = ["0"];
            $login_id    = Auth::user()->id;
            $user_id     = Auth::user()->id;
            $info_survey = $this -> survey -> get_info_survey_by_slug($slug);
            $survey_id   = $info_survey['id'];
            $info_section= $this -> survey -> get_info_section($section);

            if(Cache::has('cache-result-'.$slug.'-'.$user_id)){
                if(Cache::has('result-count-'.$slug.'-'.$user_id)){

                    $save = false;
                    $cache_jawaban  = Cache::get('result-'.$slug.'-'.$user_id);
                    foreach($cache_jawaban as $key){
                        $data['id_question']    = $key['question'];
                        $data['timer']          = $key['timer'];
                        $data['section_name']   = Cache::get('result-section-name-'.$slug.'-'.$user_id);
                        $data['id_user']        = $user_id;
                        $data['id_type_question'] = Cache::get('result-id_type_question-'.$slug.'-'.$user_id);
                        if($key['answer']){
                            foreach ($key['answer'] as $keys) {
                                $data['id_answer']  = $keys;
                                $data['point']      = $this -> option_answer -> get_point_of_answer($data['id_answer']) *
                                                      $this -> question -> get_point_of_question($key['question']);
                                if(in_array($login_id, $ignore_id))
                                {
                                    $save = true;
                                }else{
                                    $save = $this -> question -> save_answer($data);
                                }
                            }

                        }
                    }
                    if($save == true){
                        $this -> survey -> save_history_section($user_id, $section);
                        $duration               = $this -> survey -> get_temp($survey_id);
                        $this -> survey -> delete_temp_survey($slug);
                        $log['activity']        = 'Menyelesaikan section '.$info_section['name'];
                        $log['user_id']         = $user_id;
                        $log['reference_id']    = $survey_id;
                        $log['ip']              = \Request::ip();
                        $log['duration']        = $duration;
                        $log['created_at']      = Carbon::now();
                        $log['user_agent']      = \Request::header('User-Agent');
                        save_activity($log);
                    }
                    return 'true';
                }
            }else{ //kondisi jika waktu habis dan user belum sama sekali belum sempat menjawab
                $this -> survey -> save_history_section($user_id, $section);
                $duration               = $this -> survey -> get_temp($survey_id);
                $this -> survey -> delete_temp_survey($slug);
                $log['activity']        = 'Menyelesaikan section '.$info_section['name'];
                $log['user_id']         = $user_id;
                $log['reference_id']    = $survey_id;
                $log['ip']              = \Request::ip();
                $log['duration']        = $duration;
                $log['created_at']      = Carbon::now();
                $log['user_agent']      = \Request::header('User-Agent');
                save_activity($log);
                return 'true';
            }

    }

    public function saveToReportWPA($slug, $section, $id_user){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey= $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey  = $info_survey['id'];
        $ignore_id  = ["0"];
        $login_id   = Auth::user()->id;
        if(in_array($login_id, $ignore_id))
        {
            $thankyou_text  = "Terima kasih telah menyelesaikan test ini.";
            return view('survey.finish', compact('thankyou_text'));
        }

        $result = $this -> result -> get_result_wpa($id_survey);
        $result_most = $result['most'];
        $result_lest = $result['lest'];


        $data_most['d'] = 0;
        $data_most['i'] = 0;
        $data_most['s'] = 0;
        $data_most['c'] = 0;
        $data_most['x'] = 0;
        $data_most['total'] = 0;
        $data_lest['d'] = 0;
        $data_lest['i'] = 0;
        $data_lest['s'] = 0;
        $data_lest['c'] = 0;
        $data_lest['x'] = 0;
        $data_lest['total'] = 0;

        //input report most
        foreach ($result_most as $most) {
            if($most == 'D'){
                $data_most['d'] ++;
            }elseif($most == 'I'){
                $data_most['i'] ++;
            }elseif($most == 'S'){
                $data_most['s'] ++;
            }elseif($most == 'C'){
                $data_most['c'] ++;
            }elseif($most == '*'){
                $data_most['x'] ++;
            }
            $data_most['total'] ++;
        }

        //proses update data pada tabel user_report
        $this -> result -> update_user_report('most_d', $data_most['d']);
        $this -> result -> update_user_report('most_i', $data_most['i']);
        $this -> result -> update_user_report('most_s', $data_most['s']);
        $this -> result -> update_user_report('most_c', $data_most['c']);

        //simpan data ke report_wpa
        $this -> result -> save_report_wpa('most', $data_most, $id_survey, '', '');

        //input report lest
        foreach ($result_lest as $lest) {
            if($lest == 'D'){
                $data_lest['d'] ++;
            }elseif($lest == 'I'){
                $data_lest['i'] ++;
            }elseif($lest == 'S'){
                $data_lest['s'] ++;
            }elseif($lest == 'C'){
                $data_lest['c'] ++;
            }elseif($lest == '*'){
                $data_lest['x'] ++;
            }
            $data_lest['total'] ++;
        }

        //proses update data pada tabel user_report
        $this -> result -> update_user_report('lest_d', $data_lest['d']);
        $this -> result -> update_user_report('lest_i', $data_lest['i']);
        $this -> result -> update_user_report('lest_s', $data_lest['s']);
        $this -> result -> update_user_report('lest_c', $data_lest['c']);

        //simpan data ke report_wpa
        $this -> result -> save_report_wpa('lest', $data_lest, $id_survey, '', '');


        //input report change
        $data_change['d']       = $data_most['d'] - $data_lest['d'];
        $data_change['i']       = $data_most['i'] - $data_lest['i'];
        $data_change['s']       = $data_most['s'] - $data_lest['s'];
        $data_change['c']       = $data_most['c'] - $data_lest['c'];
        $data_change['x']       = $data_most['x'] + $data_lest['x'];
        $data_change['total']   = $data_most['total'] - $data_lest['total'];

        //untuk kebutuhan data pada tabel user_report
        $hasil['D']       = WpaChangeD($data_change['d']);
        $hasil['I']       = WpaChangeI($data_change['i']);
        $hasil['S']       = WpaChangeS($data_change['s']);
        $hasil['C']       = WpaChangeC($data_change['c']);
        $filteredresult   = array_filter($hasil, 'filterArray'); //filter di atas garis nol grafik
        arsort($filteredresult); //sorting berdasarkan index terbesar
        $i = 0;
        $data['result']    = '';
        foreach($filteredresult as $k => $v){

            if($i < 3){ //maksimal 3 huruf
                //untuk pencarian perilaku kerja
                if($i < 2){
                    $column_job[$i] = $k;
                }
                $data['result']   .= $k;
                $column_lookup[$i] = $k;
            }

            $i++;
        }

        //proses update data pada tabel user_report
        $this -> result -> update_user_report('change_d', $data_change['d']);
        $this -> result -> update_user_report('change_i', $data_change['i']);
        $this -> result -> update_user_report('change_s', $data_change['s']);
        $this -> result -> update_user_report('change_c', $data_change['c']);
        $this -> result -> update_user_report('hasil', $data['result']);


        //proses menyimpan ke tabel hasil lookup WPA
        $tipe_kepribadian           = $this -> result -> get_lookup_tipe_kepribadian($data['result']);
        $lookup_desc                = 'desc'.rand(1, $tipe_kepribadian['count']).' as detail';
        $lookup_type                = $tipe_kepribadian['type'];
        $uraian_kepribadian         = $this -> result -> get_lookup_uraian_kepribadian($lookup_type, $lookup_desc);
        $karakteristik_umum         = $this -> result -> get_lookup_karakteristik('lookup_wpa_karakteristik_umum', $column_job);
        $karakteristik_pekerjaan    = $this -> result -> get_lookup_karakteristik('lookup_wpa_karakteristik_pekerjaan', $column_lookup);
        $perilaku_kerja_kekuatan    = $this -> result -> get_lookup_pos_neg('lookup_wpa_perilaku_kerja', $column_job, 'kekuatan');
        $perilaku_kerja_kelemahan   = $this -> result -> get_lookup_pos_neg('lookup_wpa_perilaku_kerja', $column_job, 'kelemahan');
        $suasana_emosi_kekuatan     = $this -> result -> get_lookup_pos_neg('lookup_wpa_suasana_emosi', $column_job, 'kekuatan');
        $suasana_emosi_kelemahan    = $this -> result -> get_lookup_pos_neg('lookup_wpa_suasana_emosi', $column_job, 'kelemahan');
        $kekuatan                   = $this -> result -> get_lookup_karakteristik('lookup_wpa_kekuatan', $column_lookup[0]);
        $kelemahan                  = $this -> result -> get_lookup_karakteristik('lookup_wpa_kelemahan', $column_lookup[0]);
        $kepribadian                = $uraian_kepribadian['detail'].''.$uraian_kepribadian['description'];

        //simpan data ke report_wpa
        $this -> result -> save_report_wpa('change', $data_change, $id_survey, $data['result'], $lookup_type);

        //karakteristik umum
        $i = 0;
        foreach ($karakteristik_umum as $key => $value) {
            foreach ($value as $keys => $values) {
                $k_umum[$i] = $values;
                $i++;
            }
        }

        //perilaku kerja (kekuatan)
        $i = 0;
        foreach ($perilaku_kerja_kekuatan as $key => $value) {
            foreach ($value as $keys => $values) {
                $perilaku_kekuatan[$i] = $values;
                $i++;
            }
        }

        //perilaku kerja (kelemahan)
        $i = 0;
        foreach ($perilaku_kerja_kelemahan as $key => $value) {
            foreach ($value as $keys => $values) {
                $perilaku_kelemahan[$i] = $values;
                $i++;
            }
        }

        //suasana emosi (kekuatan)
        $i = 0;
        foreach ($suasana_emosi_kekuatan as $key => $value) {
            foreach ($value as $keys => $values) {
                $emosi_kekuatan[$i] = $values;
                $i++;
            }
        }

        //suasana emosi (kelemahan)
        $i = 0;
        foreach ($suasana_emosi_kelemahan as $key => $value) {
            foreach ($value as $keys => $values) {
                $emosi_kelemahan[$i] = $values;
                $i++;
            }
        }

        //karakteristik pekerjaan
        $i = 0;
        foreach ($karakteristik_pekerjaan as $key => $value) {
            foreach ($value as $keys => $values) {
                $k_pekerjaan[$i] = $values;
                $i++;
            }
        }

        //kekuatan
        $i = 0;
        foreach ($kekuatan as $key => $value) {
            foreach ($value as $keys => $values) {
                $kuat[$i] = $values;
                $i++;
            }
        }

        //kelemahan
        $i = 0;
        foreach ($kelemahan as $key => $value) {
            foreach ($value as $keys => $values) {
                $lemah[$i] = $values;
                $i++;
            }
        }

        shuffle($k_umum);
        shuffle($perilaku_kekuatan);
        shuffle($perilaku_kelemahan);
        shuffle($emosi_kekuatan);
        shuffle($emosi_kelemahan);
        shuffle($k_pekerjaan);
        shuffle($kuat);
        shuffle($lemah);
        $this -> result -> insert_to_rekap_wpa('report_wpa_uraian_kepribadian', $kepribadian, $id_survey);

        foreach ($k_umum as $key) {
            $this -> result -> insert_to_rekap_wpa('report_wpa_karakteristik_umum', $key, $id_survey);
        }
        foreach ($perilaku_kekuatan as $key) {
            $this -> result -> insert_to_rekap_wpa('report_wpa_perilaku_kerja_kekuatan', $key, $id_survey);
        }
        foreach ($perilaku_kelemahan as $key) {
            $this -> result -> insert_to_rekap_wpa('report_wpa_perilaku_kerja_kelemahan', $key, $id_survey);
        }
        foreach ($emosi_kekuatan as $key) {
            $this -> result -> insert_to_rekap_wpa('report_wpa_suasana_emosi_kekuatan', $key, $id_survey);
        }
        foreach ($emosi_kelemahan as $key) {
            $this -> result -> insert_to_rekap_wpa('report_wpa_suasana_emosi_kelemahan', $key, $id_survey);
        }
        foreach ($k_pekerjaan as $key) {
            $this -> result -> insert_to_rekap_wpa('report_wpa_karakteristik_pekerjaan', $key, $id_survey);
        }
        foreach ($kuat as $key) {
            $this -> result -> insert_to_rekap_wpa('report_wpa_kekuatan', $key, $id_survey);
        }
        foreach ($lemah as $key) {
            $this -> result -> insert_to_rekap_wpa('report_wpa_kelemahan', $key, $id_survey);
        }

        $this -> survey -> survey_complete($slug);
        $duration = $this -> survey -> get_temp($id_survey);
        $this -> survey -> delete_temp_survey($slug);

        $log['activity']        = 'Menyelesaikan survey '.$info_survey['title'];
        $log['user_id']         = $id_user;
        $log['reference_id']    = $id_survey;
        $log['ip']              = \Request::ip();
        $log['duration']        = $duration;
        $log['created_at']      = Carbon::now();
        $log['user_agent']      = \Request::header('User-Agent');
        save_activity($log);
        //wpa
        $complete = check_project_complete($info_survey['id_project'], $id_user);
        if($complete == true)
        {
          $this->completeEmail(Auth::user() -> email,$info_survey['id_project']);
        }
        Cache::flush();
        return 'true';
    }

    public function getQuestionWPA($slug, $section){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey          = $info_survey['id'];
        $used_timer         = $this -> survey -> get_temp($id_survey);

        //ambil info section.
        $section_info   = $this -> survey -> get_info_section($section);
        $data['slug']   = $slug;
        $data['instruction']    = $section_info['instruction'];
        $data['section_name']   = $section_info['name'];
        $data['section_id']     = $section_info['id'];
        $data['user_id']        = Auth::user() -> id;

        $ignore_id      = ["0"];
        $login_id       = Auth::user()->id;
        if(in_array($login_id, $ignore_id))
        {
            Cache::put('wpa-dummy', 'yes', $expiresAt);
        }

        if(Cache::has('cache-'.$slug.'-'.$data['user_id'])){
            $data['timer_survey'] = Cache::get('timer-survey-'.$slug.'-'.$data['user_id']);
            $data['survey_title'] = Cache::get('survey-title-'.$slug.'-'.$data['user_id']);
            $data['id_survey']    = Cache::get('id-survey-'.$slug.'-'.$data['user_id']);
            $data['number']       = Cache::get('number-'.$slug.'-'.$data['user_id']);
            $data['content']      = Cache::get('question-'.$slug.'-'.$data['user_id']);
            return view('survey.detail_wpa', compact('data', 'slug', 'section', 'used_timer'));
        }else{
            return redirect('/survey'.'/'.$slug.'/intro');
        }
    }

    public function setResultCacheWPA(Request $request){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $status         = 'add'; //untuk status edit atau tambah jawaban
        $expiresAt      = Carbon::now()->addMinutes(60);
        $number         = $request->input('number');
        $id_user        = $request->input('id_user');
        $slug           = $request->input('slug');
        $section        = $request->input('section');
        $key_most       = $request->input('key_most');
        $key_lest       = $request->input('key_lest');
        $ignore_id      = ["0"];
        $login_id       = Auth::user()->id;

        if(in_array($login_id, $ignore_id))
        {
            if(!Cache::has('wpa-dummy')){
                Cache::put('wpa-dummy', 'yes', $expiresAt);
                $dummychace = Cache::get('wpa-dummy');
                echo $dummychace;
                if($dummychace  == "yes")
                {
                    return 'dummy';
                }
            }else{
                $dummychace = Cache::get('wpa-dummy');
                echo $dummychace;
                if($dummychace  == "yes")
                {
                    return 'dummy';
                }
            }
        }

        $result = array(
            'number'  => $number,
            'key_most'    => $key_most[0],
            'key_lest'    => $key_lest[0]
        );

        $array_result = array();
        if(Cache::has('cache-result-wpa-'.$slug.'-'.$id_user)){
            $array_result = Cache::get('result-'.$slug.'-'.$id_user);
        }else{
            Cache::put('cache-result-wpa-'.$slug.'-'.Auth::user()->id, 'yes', $expiresAt);
        }
        array_push($array_result, $result);

        //proses pengecekan apakah ada nomor soal yang sama, jika sama artinya user edit/ganti jawaban
        for($i = 0; $i <count($array_result); $i++){
            $temp_cache = Cache::get('result-'.$slug.'-'.$id_user);
            if($temp_cache){
                if($temp_cache[$i]['number'] ==  $array_result[$i]['number']){
                    Cache::put('result-'.$slug.'-'.$id_user, $array_result, $expiresAt); //replace jawaban
                    $status = 'update';
                }
            }
        }

        //jika tidak mengganti jawaban, maka add to cache
        if($status != 'update'){
            //cek apakah semua soal sudah terjawab, jika belum maka simpan ke cache (jlh cache != jlh soal), jika sudah maka stop.
            if(Cache::get('result-count-'.$slug.'-'.$id_user)+1 <= 24){
                Cache::put('result-'.$slug.'-'.$id_user, $array_result, $expiresAt);
                Cache::put('result-count-'.$slug.'-'.$id_user, count($array_result), $expiresAt);
            }
        }

        if(count($array_result)+1 > 24) {
            return 'finish';
        }else{
            return count($array_result);
        }
    }

    public function saveCacheToDataResultWPA($slug, $section){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $ignore_id   = ["0"];
        $login_id    = Auth::user()->id;
        $user_id     = Auth::user()->id;
        $info_survey = $this -> survey -> get_info_survey_by_slug($slug);

        if(Cache::has('cache-result-wpa-'.$slug.'-'.$user_id)){
            if(Cache::has('result-count-'.$slug.'-'.$user_id)){

                $save = false;
                $cache_jawaban  = Cache::get('result-'.$slug.'-'.$user_id);

                foreach ($cache_jawaban as $key) {
                    $data['survey_id']  = $info_survey['id'];
                    $data['number']     = $key['number'];
                    $data['key_most']   = $key['key_most'];
                    $data['key_lest']   = $key['key_lest'];
                    $data['user_id']    = $user_id;

                    if(in_array($login_id, $ignore_id))
                    {
                        return 'dummy';
                    }else{
                        $save = $this -> result -> save_result_wpa($data);
                    }
                }

                return 'true';
            }
        }else{
            return redirect('survey/save-report-wpa/'.$slug.'/'.$section);
        }

    }

    public function getQuestionPapi($slug, $section){

        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey      = $info_survey['id'];
        $used_timer     = $this -> survey -> get_temp($id_survey);
        $user_id        = Auth::user() -> id;

        //ambil info section.
        $section_info   = $this -> survey -> get_info_section($section);
        $data['slug']   = $slug;
        $data['instruction']    = $section_info['instruction'];
        $data['section_name']   = $section_info['name'];
        $data['section_id']     = $section_info['id'];
        $data['user_id']        = $user_id;

        if(Cache::has('cache-'.$slug.'-'.$user_id)){
            $data['timer_survey'] = Cache::get('timer-survey-'.$slug.'-'.$user_id);
            $data['survey_title'] = Cache::get('survey-title-'.$slug.'-'.$user_id);
            $data['id_survey']    = Cache::get('id-survey-'.$slug.'-'.$user_id);
            $data['number']       = Cache::get('number-'.$slug.'-'.$user_id);
            $data['content']      = Cache::get('question-'.$slug.'-'.$user_id);

            return view('survey.detail_papi', compact('data', 'slug', 'section', 'used_timer'));
        }else{
            return redirect('papikostick-instruction/'.$slug.'/'.$section.'/'.$user_id);
        }

    }

    public function setResultCachePapi(Request $request){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $status     = 'add'; //untuk status edit atau tambah jawaban
        $user_id    = Auth::user() -> id;
        $expiresAt  = Carbon::now()->addMinutes(60);
        $number     = $request->input('number');
        $slug       = $request->input('slug');
        $key        = $request->input('key');
        $ignore_id  = ["0"];
        $login_id   = Auth::user()->id;

        if(in_array($login_id, $ignore_id))
        {
            if(!Cache::has('papi-dummy')){
                Cache::put('papi-dummy', 'yes', $expiresAt);
                $dummychace = Cache::get('papi-dummy');
                if($dummychace  == "yes")
                {
                    return 'dummy';
                }
            }else{
                $dummychace = Cache::get('papi-dummy');
                if($dummychace  == "yes")
                {
                    return 'dummy';
                }
            }
        }

        $result = array(
            'number'    => $number,
            'key'       => $key[0],
        ); //jawaban baru (tambahan / editan)

        $array_result = array(); //jawaban yang lama (sebelumnya)
        if(Cache::has('cache-result-papi-'.$slug.'-'.$user_id)){
            $array_result = Cache::get('result-'.$slug.'-'.$user_id);
        }else{
            Cache::put('cache-result-papi-'.$slug.'-'.$user_id, 'yes', $expiresAt);
        }
        array_push($array_result, $result);

        //proses pengecekan apakah ada nomor soal yang sama, jika sama artinya user edit/ganti jawaban
        for($i = 0; $i < count($array_result); $i++){
            $temp_cache = Cache::get('result-'.$slug.'-'.$user_id);
            if($temp_cache){
                if($temp_cache[$i]['number'] ==  $array_result[$i]['number']){
                    Cache::put('result-'.$slug.'-'.$user_id, $array_result, $expiresAt); //replace jawaban
                    $status = 'update';
                }
            }
        }

        //jika tidak mengganti jawaban, maka add to cache
        if($status != 'update'){
            //cek apakah semua soal sudah terjawab, jika belum maka simpan ke cache (jlh cache != jlh soal), jika sudah maka stop.
            if((Cache::get('result-count-'.$slug.'-'.$user_id)+1) <= 90){
                Cache::put('result-'.$slug.'-'.$user_id, $array_result, $expiresAt);
                Cache::put('result-count-'.$slug.'-'.$user_id, count($array_result), $expiresAt);
            }
        }
        if(count($array_result)+1 > 90) {
            return 'finish';
        }else{
            return count($array_result);
        }
    }

    public function saveCacheToDataResultPapi($slug, $section){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $ignore_id   = ["0"];
        $login_id    = Auth::user()->id;
        $user_id     = Auth::user()->id;
        $info_survey = $this -> survey -> get_info_survey_by_slug($slug);
        if(Cache::has('cache-result-papi-'.$slug.'-'.$user_id)){
            if(Cache::has('result-count-'.$slug.'-'.$user_id)){

                $save = false;
                $cache_jawaban  = Cache::get('result-'.$slug.'-'.$user_id);
                foreach ($cache_jawaban as $key) {

                    $data['survey_id']  = $info_survey['id'];
                    $data['number']     = $key['number'];
                    $data['keys']       = $key['key'];
                    $data['user_id']    = $user_id;

                    if(in_array($login_id, $ignore_id))
                    {
                        return 'dummy';
                    }else{
                        $save = $this -> result -> save_result_papi($data);
                    }
                }
                return 'true';
            }
        }else{
            return redirect('survey/save-report-papi/'.$slug.'/'.$section);
        }

    }

    public function saveToReportPapi($slug, $section, $id_user){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $ignore_id      = ["0"];
        $login_id       = Auth::user()->id;
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $thankyou_text  = $info_survey['thankyou_text'];
        $id_survey      = $info_survey['id'];
        if(in_array($login_id, $ignore_id))
        {
            $thankyou_text  = $thankyou_text;
            return view('survey.finish', compact('thankyou_text'));
        }

        $result = $this -> result -> get_result_papi($id_survey);
        $data['user_id']    = $id_user;
        $data['survey_id']  = $id_survey;
        $data['created_at'] = Carbon::now('Asia/Jakarta');
        $data['N'] = 0;
        $data['G'] = 0;
        $data['A'] = 0;
        $data['L'] = 0;
        $data['P'] = 0;
        $data['I'] = 0;
        $data['T'] = 0;
        $data['V'] = 0;
        $data['X'] = 0;
        $data['S'] = 0;
        $data['B'] = 0;
        $data['O'] = 0;
        $data['R'] = 0;
        $data['D'] = 0;
        $data['C'] = 0;
        $data['Z'] = 0;
        $data['E'] = 0;
        $data['K'] = 0;
        $data['F'] = 0;
        $data['W'] = 0;

        //perhitungan masing-masing key
        foreach ($result as $key) {
            if($key -> key == 'N'){
                $data['N'] ++;
            }elseif($key -> key == 'G'){
                $data['G'] ++;
            }elseif($key -> key == 'A'){
                $data['A'] ++;
            }elseif($key -> key == 'L'){
                $data['L'] ++;
            }elseif($key -> key == 'P'){
                $data['P'] ++;
            }elseif($key -> key == 'I'){
                $data['I'] ++;
            }elseif($key -> key == 'T'){
                $data['T'] ++;
            }elseif($key -> key == 'V'){
                $data['V'] ++;
            }elseif($key -> key == 'X'){
                $data['X'] ++;
            }elseif($key -> key == 'S'){
                $data['S'] ++;
            }elseif($key -> key == 'B'){
                $data['B'] ++;
            }elseif($key -> key == 'O'){
                $data['O'] ++;
            }elseif($key -> key == 'R'){
                $data['R'] ++;
            }elseif($key -> key == 'D'){
                $data['D'] ++;
            }elseif($key -> key == 'C'){
                $data['C'] ++;
            }elseif($key -> key == 'Z'){
                $data['Z'] ++;
            }elseif($key -> key == 'E'){
                $data['E'] ++;
            }elseif($key -> key == 'K'){
                $data['K'] ++;
            }elseif($key -> key == 'F'){
                $data['F'] ++;
            }elseif($key -> key == 'W'){
                $data['W'] ++;
            }
        }

        $data['Z'] = 9 - $data['Z'];
        $data['K'] = 9 - $data['K'];
        //proses update data pada tabel user_report
        $this -> result -> update_user_report('N', $data['N']);
        $this -> result -> update_user_report('G', $data['G']);
        $this -> result -> update_user_report('A', $data['A']);
        $this -> result -> update_user_report('L', $data['L']);
        $this -> result -> update_user_report('P', $data['P']);
        $this -> result -> update_user_report('I', $data['I']);
        $this -> result -> update_user_report('T', $data['T']);
        $this -> result -> update_user_report('V', $data['V']);
        $this -> result -> update_user_report('X', $data['X']);
        $this -> result -> update_user_report('S', $data['S']);
        $this -> result -> update_user_report('B', $data['B']);
        $this -> result -> update_user_report('O', $data['O']);
        $this -> result -> update_user_report('R', $data['R']);
        $this -> result -> update_user_report('D', $data['D']);
        $this -> result -> update_user_report('C', $data['C']);
        $this -> result -> update_user_report('Z', $data['Z']);
        $this -> result -> update_user_report('E', $data['E']);
        $this -> result -> update_user_report('K', $data['K']);
        $this -> result -> update_user_report('F', $data['F']);
        $this -> result -> update_user_report('W', $data['W']);

        //simpan data ke report_papikostick
        $this -> result -> save_report_papi($data);

        $this -> survey -> survey_complete($slug);
        $duration = $this -> survey -> get_temp($id_survey);
        $this -> survey -> delete_temp_survey($slug);

        $log['activity']        = 'Menyelesaikan survey '.$info_survey['title'];
        $log['user_id']         = $data['user_id'];
        $log['reference_id']    = $id_survey;
        $log['ip']              = \Request::ip();
        $log['duration']        = $duration;
        $log['created_at']      = Carbon::now();
        $log['user_agent']      = \Request::header('User-Agent');
        save_activity($log);
        //wba
        $complete = check_project_complete($info_survey['id_project'], $data['user_id']);
        if($complete == true)
        {
          $this->completeEmail(Auth::user() -> email,$info_survey['id_project']);
        }
        Cache::flush();
        return 'true';
    }

    public function saveTemp(Request $request){
        if(Entrust::can('take-test')){
            $data['survey_id']      = $request->input('survey_id');
            $data['question_id']    = $request->input('question_id');
            $data['user_id']        = $request->input('user_id');
            $data['timer']          = $request->input('timer');
            $data['slug']           = $request->input('slug');
            $data['section']        = $request->input('section');


            $save = $this -> survey -> save_temp($data);
            if($save == true){
                $this -> getQuestion($data['slug'], $data['section']);
            }
        }
    }

    public function getHistorySurvey(){
        if(Entrust::can('take-test')){
            $history = $this -> survey -> getHistory();
            return view('survey.history', compact('history'));
        }
    }

    public function addNewSurvey(){
        if(Entrust::can('create-survey')){
            // $data = Input::all();
            $data['name']                   = Input::get('name');
            $data['slug']                   = strtolower(str_replace(' ', '-', $data['name'].'-'.strtotime('now')));
            $data['description']            = Input::get('description');
            $start                          = strtotime(Input::get('started'));
            $end                            = strtotime(Input::get('ended'));
            $data['started']                = date('Y-m-d 00:00:00', $start);
            $data['ended']                  = date('Y-m-d 23:59:59', $end);
            $data['randomized']             = Input::get('randomized');
            $data['tools']                  = Input::get('tools');
            $data['user_id']                = Auth::user()->id;

            $validator = Validator::make(Input::all(), [
                'name'          => 'required',
                'description'   => 'required',
                'started'       => 'required',
                'ended'         => 'required',
                'tools'         => 'array|required'
            ]);
            if ($validator->fails()) {
                return redirect() -> back() -> withErrors($validator);
            }

            /*$survey_quarter = $this -> survey -> check_quarter_survey($data['category'], $data['quarter']);

            //cek apakah sudah ada kategori survey dan quarter yang sama yang sudah dibuat dalam tahun ini.
            if($survey_quarter -> count() > 0){
                return redirect()->back()->withErrors('Pastikan kuarter dan kategori yang dipilih belum pernah dibuat tahun ini');
            }*/

            $project            = $this -> survey -> save_project($data);
            $data['id_project'] = $project;
            if($data['randomized'] == null){
                $data['randomized'] = 0;
            }

            //cek apakah kategori survey merupakan survey yang harus diambil dari bank soal atau bukan
            foreach($data['tools'] as $key){
                if($key == 5 || $key == 2 || $key == 1){
                    $data['category'] = $key;
                    $save_survey = $this -> survey -> save_from_bank_survey($data);
                }elseif($key == 3){
                    $data['category'] = $key;
                    $save_survey = $this -> survey -> save_wpa_survey($data);
                }elseif($key == 4){
                    $data['category'] = $key;
                    $save_survey = $this -> survey -> save_papi_survey($data);
                }else{
                    $data['category'] = $key;
                    $save_survey = $this -> survey -> save_survey($data);
                }

            }

            if($save_survey == false){
                return redirect()->back()->withErrors('Failed to save');
            }else{
                return redirect('/project')->withSuccess('Survey Created');
            }

        }
    }

    public function testTimeOut(Request $request){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $id_survey = $request -> input('id_survey');

        //ambil pertanyaan yang belum dijawab
        $unfinished_question = $this -> question -> get_unfinished_question($id_survey);
        foreach ($unfinished_question as $key) {
            $data['id_answer']  = $key['id_option_answer'];
            $data['user_id']    = Auth::user() -> id;
            $data['id_question']= $key['id_question'];
            $data['point']      = 0;
            $data['is_checked'] = 1;

            $this -> result -> save_unfinished_question($data);
        }

        //ambil section yang belum selesai
        $unfinished_section = $this -> survey -> get_unfinished_section($id_survey);
        foreach ($unfinished_section as $key) {

            $point = $this -> result -> get_point_survey_gti($survey_id, $key['id']);
            if($point -> count() != 0){
                foreach($point as $point){
                    $score['survey_id']     = $id_survey;
                    $score['section_id']    = $point['section_id'];
                    $score['user_id']       = $point['user_id'];

                    //perhitungan score GTI
                    if($point['category_id'] == 1){

                        $true   = $point['sum'];
                        $false  = $point['count'] - $point['sum'];

                        /*
                            rumus adj setiap subtest :
                            adj = jumlah benar - (jumlah salah / opsi - 1)
                        */
                        if($true > $false){

                            if($section == 1){ //letter check
                                $adj = round($true - ($false/4));
                                $gtq = $this -> result -> get_gtq('gtq1', $adj);
                            }elseif($section == 2){ //reasoning
                                $adj = round($true - ($false/2));
                                $gtq = $this -> result -> get_gtq('gtq2', $adj);
                            }elseif($section == 3){ //letter distance
                                $adj = round($true - ($false/1));
                                $gtq = $this -> result -> get_gtq('gtq3', $adj);
                            }elseif($section == 4){ //numerical
                                $adj = round($true - ($false/2));
                                $gtq = $this -> result -> get_gtq('gtq4', $adj);
                            }elseif($section == 5){ //spatial
                                $adj = round($true - ($false/3));
                                $gtq = $this -> result -> get_gtq('gtq5', $adj);
                            }

                            $point['sum'] = $gtq -> tq;
                        }elseif($true <= $false){
                            $point['sum'] = 0;
                        }

                    }

                    $score['score']         = $point['sum'];
                }

            }else{
                $score['survey_id']     = $survey_id;
                $score['section_id']    = $section;
                $score['user_id']       = Auth::user() -> id;
                $score['score']         = $point['sum'];
            }

            //proses masukkan hasil score ke tabel report
            $this -> result -> save_report($score);
        }
    }

    public function testTimeOutWPA(Request $request){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $id_survey = $request -> input('id_survey');
        $unfinished_number = $this -> question -> get_unfinished_number_wpa($id_survey);
        foreach($unfinished_number as $key){
            $data['user_id']    = Auth::user() -> id;
            $data['number']     = $key['number'];
            $data['M']          = $key['M'];
            $data['L']          = $key['L'];

            $this -> result -> save_unfinished_number();
        }

    }

    public function deleteTempSurvey($slug){
        $this -> survey -> delete_temp_survey($slug);
    }

    public function cekSudah($slug, $section){
        $survey_info = $this -> survey -> get_info_survey_by_slug($slug);
        $id = $survey_info['id'];
        $data = DB::table('survey_report') -> where('section_id', '=', $section) -> where('user_id', '=', Auth::user() -> id) -> where('survey_id', '=', $id)-> first();
        if($data == null){
            return 'yet';
        }else{
            return 'done';
            // return view('survey.start_next_section');
        }
    }

    //============================================ TAMBAHAN HURA VERSI 1,5 ============================================//

    public function index_project(){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        //check status penyelesaian survey
        $this -> current_project_status();
        $this -> current_survey_status();

        $projects = $this -> survey -> get_all_project();
        return view('/survey.project', compact('projects'));
    }

    private function current_project_status() {
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $list = DB::table("project") -> get();
        foreach ($list as $row) {
            $persentase = partisipasi_peserta_project($row->id);

            //status berdasarkan periode
            if (date("Y-m-d h:i:s") < $row->start_date) {
                DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 0]);
            }elseif(date("Y-m-d h:i:s") >= $row->start_date && date("Y-m-d h:i:s") <= $row->end_date){
                //status berdasarkan partisipasi
                if ($persentase == 100) {
                    DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 1]);
                }else{
                    DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 2]);
                }
            }elseif(date("Y-m-d h:i:s") > $row->end_date){
                DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 1]);
            }
        }
    }

    private function current_survey_status() {
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $list = DB::table("survey") -> get();
        foreach ($list as $row) {
            $persentase = partisipasi_peserta_survey($row->id);

            //status berdasarkan periode
                // echo date("Y-m-d h:i:s")." - ".$row->tanggal_awal."<br/>";
            if (date("Y-m-d h:i:s") < $row->start_date) {
                DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 0]);
            }elseif(date("Y-m-d h:i:s") >= $row->start_date && date("Y-m-d h:i:s") <= $row->end_date){
                //status berdasarkan partisipasi
                if ($persentase == 100) {
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 1]);
                }else{
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 2]);
                }
            }elseif(date("Y-m-d h:i:s") > $row->end_date){
                DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 1]);
            }
        }
    }

    public function assign_peserta($id){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $data['info_project'] = $this -> survey -> get_info_project_by_id($id);
        $data['list_peserta'] = $this -> survey -> get_peserta_available($id);
        return view('/survey.assign', compact('data'));
    }

    public function do_assign(Request $request){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $input              = $request -> all();
        $data['id']         = $input['id'];
        $data['participant']= $input['participant'];
        $i                  = 0;
        $list_survey        = $this -> survey -> get_list_survey_by_project($data['id']);

        $kuota['survey']   = 0;
        $kuota['user']     = 0;

        //ambil id survey berdasarkan project
        foreach ($list_survey as $key) {
            $data['survey'][$i] = $key['id'];
            $kuota['survey']++;
            $i++;
        }
        $explode = explode(", ", $data['participant'][0]);

        if($explode[0] == null){ //jika user tidak copas (tidak ada hasil explode email), maka langsung assign user
            foreach($data['participant'] as $key){
                if($key != ""){
                    $assign = $this -> survey -> assign_project($data, $key);
                    $kuota['user']++;
                    $email = DB::table("users")->where(["id"=>$key])->first();
                    $this->notifEmail($email->email,$data['id']);
                }
            }
        }else{ //jika user merupakan hasil copas, maka cek apakah user sudah terdaftar apa belum
            foreach ($explode as $key) {
                // Validate email
                if (filter_var($key, FILTER_VALIDATE_EMAIL)) {
                    $is_registered  = $this -> survey -> is_registered($key);

                    if($is_registered == false){
                        $user       = $this -> survey -> insert_new_user($key);
                        $assign     = $this -> survey -> assign_project($data, $user->id);

                    }else{
                        $is_assigned    = $this -> survey -> is_assigned($data, $is_registered);
                        if($is_assigned == false){
                            $assign = $this -> survey -> assign_project($data, $is_registered);
                        }

                    }
                     $this->notifEmail($key,$data['id']);
                } else {
                    return redirect('project/assign/'.$data['id'])->withErrors('Pastikan format email user benar');
                }

                $kuota['user']++;
            }
        }

        //setting kuota terpakai
        $kuota['terpakai'] = $kuota['survey'] * $kuota['user'];
        $this -> survey -> set_kuota_terpakai($kuota);

        return redirect('project')->withSuccess('Peserta berhasil diassign');
    }

    public function show_project($id){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $project = $this -> survey -> get_detail_project($id);
        $i = 0;
        foreach ($project as $key) {
            $detail['id']               = $key['id'];
            $detail['nama']             = $key['name'];
            $detail['deskripsi']        = $key['description'];
            $detail['tanggal_awal']     = $key['start_date'];
            $detail['tanggal_akhir']    = $key['end_date'];
            $detail['is_done']          = $key['is_done'];
            $detail['tools'][$i]['judul']       = $key['title'];
            $detail['tools'][$i]['id']          = $key['id_survey'];
            $detail['tools'][$i]['deskripsi']   = $key['instruction_survey'];
            $detail['tools'][$i]['status']      = $key['status_survey'];
            $detail['tools'][$i]['is_random']   = $key['is_random'];
            $i++;
        }
        $detail['peserta'] = $this -> survey -> get_peserta_project($detail['id']);
        $detail['count_peserta'] = $detail['peserta'] -> count();
        return view('/survey.detail_project', compact('detail'));
    }

    public function edit_project($slug){
        if(!Entrust::can('update-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $info_project   = $this -> survey -> get_info_project_by_slug($slug);
        $id             = $info_project['id'];
        $data = $this -> survey -> get_info_project_by_id($id);
        return view('/survey.edit_project',compact('data'));
    }

    public function do_edit_project(Request $request){
        if(!Entrust::can('update-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $input                          = Input::all();
        $data['id']                     = $input['id'];
        $data['name']                   = $input['name'];
        $data['description']            = $input['description'];
        $data['slug']                   = strtolower(str_replace(' ', '-', $data['name'].'-'.strtotime('now')));
        $start                          = $input['start_date'];
        $end                            = $input['end_date'];
        $data['start_date']             = date('Y-m-d', strtotime($start));
        $data['end_date']               = date('Y-m-d', strtotime($end));
        $data['user_id']                = Auth::user()->id;
        $this -> validate($request,[
            'name'        => 'required',
            'description' => 'required',
            'start_date'  => 'required',
            'end_date'    => 'required',
        ]);
        $edit = $this -> survey -> edit_project($data);
        return redirect('project/')->withSuccess('Data Project Berhasil Diubah');
    }

    public function hapus_project($slug){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_project   = $this -> survey -> get_info_project_by_slug($slug);
        $id             = $info_project['id'];
        $delete = $this -> survey -> hapus_project($id);
        return redirect('project')->withSuccess('Data Project Berhasil Dihapus');
    }

    public function detail_project_user($slug){
        $info_project           = $this -> survey -> get_info_project_by_slug($slug);
        $id_project             = $info_project['id'];
        $id_peserta             = Auth::user() -> id;
        $project                = $this -> survey -> get_detail_project_wajib($id_project, $id_peserta);
        $tools                  = $this -> survey -> get_survey_wajib_by_project($id_project, $id_peserta);
        $i                      = 0;
        $detail['count_tools']  = $tools -> count();
        $detail['nama']                     = $project['name'];
        $detail['start_date']               = $project['start_date'];
        $detail['end_date']                 = $project['end_date'];
        $tools = $this -> survey -> get_survey_wajib_by_project($id_project, $id_peserta);

        foreach($tools as $keys){
            $section    = $this -> survey -> get_first_section_on_survey($keys['id'], $id_peserta);
            $detail['tools'][$i]['title']       = $keys['title'];
            $detail['tools'][$i]['slug']        = $keys['slug'];
            $detail['tools'][$i]['instruction'] = $keys['instruction'];
            $detail['tools'][$i]['status']      = $keys['status'];
            $detail['tools'][$i]['is_random']   = $keys['is_random'];
            $detail['tools'][$i]['end_date']    = $keys['end_date'];
            $detail['tools'][$i]['category_survey_id']   = $keys['category_survey_id'];

            $detail['tools'][$i]['section']     = $section['id'];
            $i++;
        }
        return view('/user.home', compact('detail'));
    }

    //send email
    public function notifEmail($email,$project_id)
    {
      $token = $email.'+'.$project_id;
      $token =  encrypt_decrypt("encrypt",$token);
      $data = [
        "token"=>$token,
        "subject"=>"Undangan",
        "view"=>"emails.notif"
      ];
      Mail::to($email)->send(new Notification($data));

    }
    public function completeEmail($email,$project_id)
    {
      // $email2 = \Config::get('app.admin_survey');
      // $token = $email.'+'.$project_id;
      // $token =  encrypt_decrypt("encrypt",$token);
      // //user
      // $project =DB::table("project")->where(["id"=>$project_id])->first();
      //
      // $data = [
      //   "user"=>$email,
      //   "project"=>$project->name,
      //   "subject"=>"Pemberitahuan",
      //   "view"=>"emails.finish"
      // ];
      // Mail::to($email2)->send(new CompleteTools($data));

    }

    public function flush(){
        Cache::flush();
    }
}
