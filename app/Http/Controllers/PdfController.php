<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
use Nexmo;
use Exception;
use App\Notifications\TwoFactorAuth;


use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Survey;
use App\User;
use App\SurveyCategory;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Library\report_survey;
use App\Jobs_karakteristik;
use App\Jobs;
use PDF;
use Entrust;
use App\HasilPeserta;
use App\Project;
use App\StandarNilai;
use App\StandarNilaiProject;
use App\Library\Kalkulasi;

class PdfController extends Controller
{
    public function __construct(SurveyRepositoryInterface $survey, ResultRepositoryInterface $result)
    {
        $this -> survey         = $survey;
        $this -> result         = $result;
    }
    public function pdfGti($slug, $id_user)
    {
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey          = $info_survey['id'];
        $report             = $this -> result -> get_report_gti_tiki($id_survey, $id_user);
        $avg                = round(array_sum($report)/count($report), 1);
        $report['date']     = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
        $report['start_date']   = $info_survey['start_date'];
        $report['end_date']     = $info_survey['end_date'];

        //proses pencarian job references berdasarkan model
        $model['1']=round(($report['subtest1'] + $report['subtest2'] + $report['subtest4'])/3,2);
        $model['2']=round(($report['subtest3'] + $report['subtest4'] + $report['subtest5'])/3,2);
        $model['3']=round(($report['subtest2'] + $report['subtest4'] + $report['subtest5'])/3,2);
        $model['4']=round(($report['subtest1'] + $report['subtest2'] + $report['subtest3'] + $report['subtest4'] + $report['subtest5'])/5,2);
        $match = array_search(max($model), $model);
        $report['slug']     = $slug;
        $report['title']    = $info_survey['title'];
        $report['avg']      = $avg;
        $report['jobs']     = $this -> result -> get_gti_job_preferences($match);
        $report['criteria'] = $this -> result -> get_criteria_gti_report($report['avg']);
        $report['id_user']  = $id_user;

        //dari sini
        $kekuatan_kelemahan = DB::table('report_gti_detail')->where('user_id', '=', $id_user)->where('survey_id', '=', $id_survey)->first();
        if($kekuatan_kelemahan){
            $kelemahan = json_decode($kekuatan_kelemahan->kelemahan);
            $kekuatan  = json_decode($kekuatan_kelemahan->kekuatan);
        }else{
            for($i = 1; $i <= 5; $i++){
                if($report['subtest'.$i] < 92){
                    $kelemahan[$i]   = $this -> result -> get_kekuatan_kelemahan_gti('GTQ'.$i);
                    $kekuatan[$i]    = '';
                }elseif($report['subtest'.$i] > 105.9){
                    $kekuatan[$i]    = $this -> result -> get_kekuatan_kelemahan_gti('GTQ'.$i);
                    $kelemahan       = '';
                }else{
                    $kekuatan[$i]    = '';
                    $kelemahan[$i]   = '';
                }
            }
            $insert['kekuatan'] = json_encode($kekuatan);
            $insert['kelemahan'] = json_encode($kelemahan);
            $insert['user_id']  = $id_user;
            $insert['survey_id'] = $id_survey;
            DB::table('report_gti_detail')->insert($insert);
        }

        // return view('pdf.gti',compact('report', 'kekuatan', 'kelemahan'));
        $pdf = PDF::loadView('pdf.gti',compact('report', 'kekuatan', 'kelemahan'));
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 200);
        $pdf->setOption('enable-smart-shrinking', true);
        return $pdf->download('report-gti-'.get_firstname_by_id($id_user).'.pdf');
        // $pdf->save(storage_path('files/report-hura/'.$slug.'-'.$id_user.'.pdf'));
    }
    public function pdfTiki($slug, $id_user)
    {
          if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
          }
          $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
          $id_survey          = $info_survey['id'];
          $raw_score          = $this -> result -> get_report_gti_tiki($id_survey, $id_user);
          $data['date']       = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
          $data['start_date'] = $info_survey['start_date'];
          $data['end_date']   = $info_survey['end_date'];

          $total  = 0;
          $i      = 0;
          foreach ($raw_score as $key => $value) {
              if($key != 'subtest5'){
                  // $standar_score      = $this -> result -> get_standard_score_tiki($key, $value);
                  // foreach ($standar_score as $std) {
                      $data['nilai_standar'][$i] = $value;
                      $total = $total + $value;
                      $i++;
                  // }
              }
          }
          $data['total_tiki'] = $total;
          $data['title']      = $info_survey['title'];
          $data['slug']       = $slug;
          $data['id_user']    = $id_user;
          if($total <= 29){ //batas bawah
              $data['iq'] = 56;
          }elseif($total > 107){ //batas atas
              $data['iq'] = 145;
          }else{ //lookup
              $data['iq']     = $this -> result -> get_iq_tiki($total);
          }          

          if($data['iq'] <= 80){
              $data['golongan_iq'] = "Di bawah Rata-Rata";
          }elseif($data['iq'] <= 90){
              $data['golongan_iq'] = "Rata-Rata Rendah";
          }elseif($data['iq'] <= 100){
              $data['golongan_iq'] = "Rata-Rata";
          }elseif($data['iq'] <= 110){
              $data['golongan_iq'] = "Rata-Rata Atas";
          }elseif($data['iq'] <= 125){
              $data['golongan_iq'] = "Di Atas Rata-Rata";
          }elseif($data['iq'] >= 126){
              $data['golongan_iq'] = "Jauh Di Atas Rata-Rata";
          }

          $data['f1'] = round(($data['nilai_standar'][1] + $data['nilai_standar'][3]) / 2, 2);
          $data['f3'] = round(($data['nilai_standar'][0] + $data['nilai_standar'][2]) / 2, 2);
          $data['f2'] = round(($data['nilai_standar'][0] + $data['nilai_standar'][2] + $data['nilai_standar'][3]) / 3, 2);

          if($data['f1'] <= 7.49){
              $data['golongan_f1'] = "Kurang Sekali";
          }elseif($data['f1'] <= 12.49){
              $data['golongan_f1'] = "Kurang";
          }elseif($data['f1'] <= 14.49){
              $data['golongan_f1'] = "Sedang Bawah";
          }elseif($data['f1'] <= 17.49){
              $data['golongan_f1'] = "Sedang Atas";
          }elseif($data['f1'] <= 22.49){
              $data['golongan_f1'] = "Cukup Baik";
          }elseif($data['f1'] >= 22.5){
              $data['golongan_f1'] = "Baik";
          }

          if($data['f2'] <= 7.49){
              $data['golongan_f2'] = "Kurang Sekali";
          }elseif($data['f2'] <= 12.49){
              $data['golongan_f2'] = "Kurang";
          }elseif($data['f2'] <= 14.49){
              $data['golongan_f2'] = "Sedang Bawah";
          }elseif($data['f2'] <= 17.49){
              $data['golongan_f2'] = "Sedang Atas";
          }elseif($data['f2'] <= 22.49){
              $data['golongan_f2'] = "Cukup Baik";
          }elseif($data['f2'] >= 22.5){
              $data['golongan_f2'] = "Baik";
          }

          if($data['f3'] <= 7.49){
              $data['golongan_f3'] = "Kurang Sekali";
          }elseif($data['f3'] <= 12.49){
              $data['golongan_f3'] = "Kurang";
          }elseif($data['f3'] <= 14.49){
              $data['golongan_f3'] = "Sedang Bawah";
          }elseif($data['f3'] <= 17.49){
              $data['golongan_f3'] = "Sedang Atas";
          }elseif($data['f3'] <= 22.49){
              $data['golongan_f3'] = "Cukup Baik";
          }elseif($data['f3'] >= 22.5){
              $data['golongan_f3'] = "Baik";
          }

        // return view('pdf.tiki',compact('data'));
        $pdf = PDF::loadView('pdf.tiki',compact('data'));
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 1000);
        $pdf->setOption('enable-smart-shrinking', true);
        return $pdf->download('report-tiki-'.get_firstname_by_id($id_user).'.pdf');
        // $pdf->save(storage_path('files/report-hura/'.$slug.'-'.$id_user.'.pdf'));
    }
    public function pdfWpa($slug, $id_user)
    {
      if(!Entrust::can('manage-survey')){
          return redirect('/home')->withErrors(trans('messages.permission_denied'));
      }
      $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
      $id_survey      = $info_survey['id'] ;
      $data['slug']   = $slug;
      $data['title']  = $info_survey['title'];
      $data['id_user']= $id_user;
      $data['date']       = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
      $data['start_date'] = $info_survey['start_date'];
      $data['end_date']   = $info_survey['end_date'];

      $report_wpa   = $this -> result -> get_score_wpa($id_survey, $id_user);
      foreach ($report_wpa as $key) {
          $most   = json_decode($key['most']);
          $lest   = json_decode($key['lest']);
          $change = json_decode($key['change']);
      }
      //most graph
      $most_d = $most -> d;
      $most_i = $most -> i;
      $most_s = $most -> s;
      $most_c = $most -> c;
      $data['most_d']  = WpaMostD($most_d);
      $data['most_i']  = WpaMostI($most_i);
      $data['most_s']  = WpaMostS($most_s);
      $data['most_c']  = WpaMostC($most_c);

      //lest graph
      $lest_d = $lest -> d;
      $lest_i = $lest -> i;
      $lest_s = $lest -> s;
      $lest_c = $lest -> c;
      $data['lest_d']  = WpaLeastD($lest_d);
      $data['lest_i']  = WpaLeastI($lest_i);
      $data['lest_s']  = WpaLeastS($lest_s);
      $data['lest_c']  = WpaLeastC($lest_c);

      //change graph
      $change_d = $change -> d;
      $change_i = $change -> i;
      $change_s = $change -> s;
      $change_c = $change -> c;
      $data['change_d']  = WpaChangeD($change_d);
      $data['change_i']  = WpaChangeI($change_i);
      $data['change_s']  = WpaChangeS($change_s);
      $data['change_c']  = WpaChangeC($change_c);
      $result                     = $this -> result -> get_hasil_wpa($id_user, $id_survey);
      $rekap_detail   = $this -> result -> get_rekap_wpa_detail($id_user, $id_survey);

      $k_umum = json_decode($rekap_detail->karakteristik_umum);
      $perilaku_kekuatan = json_decode($rekap_detail->perilaku_kerja_kekuatan);
      $perilaku_kelemahan = json_decode($rekap_detail->perilaku_kerja_kelemahan);
      $emosi_kekuatan = json_decode($rekap_detail->suasana_emosi_kekuatan);
      $emosi_kelemahan = json_decode($rekap_detail->suasana_emosi_kelemahan);
      $k_pekerjaan = json_decode($rekap_detail->karakteristik_pekerjaan);
      $kuat = json_decode($rekap_detail->kekuatan);
      $lemah = json_decode($rekap_detail->kelemahan);
      $uraian_kepribadian = $rekap_detail->uraian_kepribadian;

      $data['karakteristik_umum']         = $k_umum;
      $data['perilaku_kerja_kekuatan']    = $perilaku_kekuatan;
      $data['perilaku_kerja_kelemahan']   = $perilaku_kelemahan;
      $data['suasana_emosi_kekuatan']     = $emosi_kekuatan;
      $data['suasana_emosi_kelemahan']    = $emosi_kelemahan;
      $data['karakteristik_pekerjaan']    = $k_pekerjaan;
      $data['kekuatan']                   = $kuat;
      $data['kelemahan']                  = $lemah;
      $data['uraian_kepribadian']         = $uraian_kepribadian;
      $data['type_kepribadian']           = $result['type'];
      $data['result']                     = $result['hasil'];

      //return view('pdf.wpa',compact('data'));
      $pdf = PDF::loadView('pdf.wpa',compact('data'));
      $pdf->setOption('enable-javascript', true);
      $pdf->setOption('javascript-delay', 1000);
      $pdf->setOption('enable-smart-shrinking', true);
      return $pdf->download('report-wpa-'.get_firstname_by_id($id_user).'.pdf');
      // $pdf->save(storage_path('files/report-hura/'.$slug.'-'.$id_user.'.pdf'));
    }
    public function pdfWba($slug, $id_user)
    {
          if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
          }
          $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
          $id_survey      = $info_survey['id'] ;
          $report         = $this -> result -> get_report_papi($id_survey, $id_user);
          $data['slug']   = $slug;
          $data['title']  = $info_survey['title'];
          $data['id_user']= $id_user;
          $data['date']       = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
          $data['start_date'] = $info_survey['start_date'];
          $data['end_date']   = $info_survey['end_date'];
          foreach ($report as $key) {
              $data['N'] = $key -> N;
              $data['G'] = $key -> G;
              $data['A'] = $key -> A;
              $data['L'] = $key -> L;
              $data['P'] = $key -> P;
              $data['I'] = $key -> I;
              $data['T'] = $key -> T;
              $data['V'] = $key -> V;
              $data['X'] = $key -> X;
              $data['S'] = $key -> S;
              $data['B'] = $key -> B;
              $data['O'] = $key -> O;
              $data['R'] = $key -> R;
              $data['D'] = $key -> D;
              $data['C'] = $key -> C;
              $data['Z'] = $key -> Z;
              $data['E'] = $key -> E;
              $data['K'] = $key -> K;
              $data['F'] = $key -> F;
              $data['W'] = $key -> W;
          }
          $result = $this -> result -> get_hasil_interpretasi_papi($data);
          $data['result'] = $result;
        // return view('pdf.wba',compact('data'));
        $pdf = PDF::loadView('pdf.wba',compact('data'));
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 1000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        $pdf->setOption('images', true);
        return $pdf->download('report-wba-'.get_firstname_by_id($id_user).'.pdf');
        // $pdf->save(storage_path('files/report-hura/'.$slug.'-'.$id_user.'.pdf'));
    }

    public function download_report($slug, $id_user){
      if(!Entrust::can('manage-survey')){
          return redirect('/home')->withErrors(trans('messages.permission_denied'));
      }
      return response()->download("../storage/files/report-hura/".$slug."-".$id_user.".pdf");
    }

    //=============================================== BATAS UNTUK LAPORAN KESELURUHAN ===============================================//

    public function printAllReport($id_project, $id_user){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $detail_project = $this -> survey -> get_detail_project($id_project);

        $data['first_name']   = get_firstname_by_id($id_user);
        $data['project_name'] = $detail_project[0]['name'];
        $data['tpa']  = null;
        $data['lai']  = null;
        $data['tiki'] = null;
        $data['wpa']  = null;
        $data['wba']  = null;

        $data['tpa']  = $this -> generate_pdf_tpa($id_project, $id_user);
        foreach ($detail_project as $key) {
            if($key['category_survey_id'] == 1){
                $data['lai']  = $this -> generate_pdf_lai($key['slug_survey'], $id_user);
            }elseif($key['category_survey_id'] == 2){
                $data['tiki'] = $this -> generate_pdf_tiki($key['slug_survey'], $id_user);
            }elseif($key['category_survey_id'] == 3){
                $data['wpa'] = $this -> generate_pdf_wpa($key['slug_survey'], $id_user);
            }elseif($key['category_survey_id'] == 4){
                $data['wba'] = $this -> generate_pdf_wba($key['slug_survey'], $id_user);
            }
        }

        // return view('pdf.report-all',compact('data'));
        $pdf = PDF::loadView('pdf.report-all',compact('data'));
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 1000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('margin-top', 0);
        $pdf->setOption('margin-bottom', 0);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('margin-right', 0);

        return $pdf->download('laporan-keseluruhan-'.strtolower(get_firstname_by_id($id_user)).'.pdf');
    }

    public function generate_pdf_tpa($project_id, $user_id)
    {
      if(!Entrust::can('manage-survey')){
          return redirect('/home')->withErrors(trans('messages.permission_denied'));
      }

      $user      = User::with('profile')->find($user_id);
      $fullName  = $user->profile->first_name;
      $refUserId = $user_id;
      $projectId = $project_id;
      $user_report = HasilPeserta::select("id","user_id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["user_id"=> $refUserId, "tujuan"=> $projectId])->first();
      $report['aspeks'] = \App\AspekPsikologis::all();
      
      if (sizeof($user_report)>0) {
          $user_report_aspek = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $projectId])->first();
          $json_nilai_aspek  = json_decode($user_report_aspek->nilai_aspek,true);
          //get survey_id
          $survey_id      = Survey::where('id_project',$projectId)->where('category_survey_id',3)->value('id');
          $analisis       = \App\ReportWPADetail::where(['user_id'=>$refUserId, 'survey_id'=> $survey_id])->select("id", "kekuatan", "kelemahan", "karakteristik_pekerjaan")->first();
          $projectName    = Project::where('id',$projectId)->value('name');
          $promosiIdPrint = StandarNilaiProject::where('project_id',$project_id)->value('standar_nilai_id');

          $posisiId    = $promosiIdPrint;
          $rekomendasi = $user_report['kriteria_psikograph_id'];
          $kecocokan   = $user_report['kecocokan'];

          $report["user_id"]       = $user->id;
          $report["project_id"]    = $projectId;
          $report["nama"]          = $user->profile->first_name;
          $report["email"]         = "$user->email";
          $report["sign_up"]       = $user->created_at;
          $report["last_login"]    = $user->last_login;
          $report["avatar"]        = (is_null($user->profile->avatar) ? "default-avatar.png" : $user->profile->avatar);
          $report["user_id"]       = $refUserId;
          $report["rekomendasi"]   = null;
          $report["kecocokan"]     = $kecocokan;
          $report["nilais"]        = $json_nilai_aspek;
          $report["analisis"]      = (is_null($user_report['uraians']) ? "Data belum tersedia" : $user_report['uraians']);
          $report["kelebihan"]     = (is_null($analisis) ? [] : json_decode($analisis->kekuatan));
          $report["kelemahan"]     = (is_null($analisis) ? [] : json_decode($analisis->kelemahan));
          $report["karakteristik"] = (is_null($analisis) ? [] : json_decode($analisis->karakteristik_pekerjaan));
          $report["hasilKriteria"] = $rekomendasi;
          $report["projectName"]   = $projectName;
          $report["posisiId"]      = $posisiId;

          $report['listJabatan']     = StandarNilai::get();
          $report['current_jabatan'] = StandarNilai::where('id',$posisiId)->value('nama_jabatan');

          return $report;

      } else {
          $dataKalkulasi["user_id"]    = $refUserId;
          $dataKalkulasi["project_id"] = $projectId;
          $dataKalkulasi["fullName"]   = $fullName;
          $kalkulasi = Kalkulasi::prosesKalkulasi($dataKalkulasi);

          try {
            $user_report_aspek = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $projectId])->first();
            $json_nilai_aspek  = json_decode($user_report_aspek->nilai_aspek,true);
            //get survey_id
            $survey_id      = Survey::where('id_project',$projectId)->where('category_survey_id',3)->value('id');
            $analisis       = \App\ReportWPADetail::where(['user_id'=>$refUserId, 'survey_id'=> $survey_id])->select("id", "kekuatan", "kelemahan", "karakteristik_pekerjaan")->first();
            $projectName    = Project::where('id',$projectId)->value('name');
            $promosiIdPrint = StandarNilaiProject::where('project_id',$project_id)->value('standar_nilai_id');

            $posisiId    = $promosiIdPrint;
            $rekomendasi = $user_report['kriteria_psikograph_id'];
            $kecocokan   = $user_report['kecocokan'];

            $report["user_id"]       = $user->id;
            $report["project_id"]    = $projectId;
            $report["nama"]          = $user->profile->first_name;
            $report["email"]         = "$user->email";
            $report["sign_up"]       = $user->created_at;
            $report["last_login"]    = $user->last_login;
            $report["avatar"]        = (is_null($user->profile->avatar) ? "default-avatar.png" : $user->profile->avatar);
            $report["user_id"]       = $refUserId;
            $report["rekomendasi"]   = null;
            $report["kecocokan"]     = $kecocokan;
            $report["nilais"]        = $json_nilai_aspek;
            $report["analisis"]      = (is_null($user_report['uraians']) ? "Data belum tersedia" : $user_report['uraians']);
            $report["kelebihan"]     = (is_null($analisis) ? [] : json_decode($analisis->kekuatan));
            $report["kelemahan"]     = (is_null($analisis) ? [] : json_decode($analisis->kelemahan));
            $report["karakteristik"] = (is_null($analisis) ? [] : json_decode($analisis->karakteristik_pekerjaan));
            $report["hasilKriteria"] = $rekomendasi;
            $report["projectName"]   = $projectName;
            $report["posisiId"]      = $posisiId;

            $report['listJabatan']     = StandarNilai::get();
            $report['current_jabatan'] = StandarNilai::where('id',$posisiId)->value('nama_jabatan');

            return $report;
          } catch (\Exception $e) {
            $report == null;
            return $report;
          }
      }
    }

    public function generate_pdf_lai($slug, $id_user){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey          = $info_survey['id'];
        $report             = $this -> result -> get_report_gti_tiki($id_survey, $id_user);
        $avg                = round(array_sum($report)/count($report), 1);
        $report['date']     = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);

        //proses pencarian job references berdasarkan model
        $model['1']=round(($report['subtest1'] + $report['subtest2'] + $report['subtest4'])/3,2);
        $model['2']=round(($report['subtest3'] + $report['subtest4'] + $report['subtest5'])/3,2);
        $model['3']=round(($report['subtest2'] + $report['subtest4'] + $report['subtest5'])/3,2);
        $model['4']=round(($report['subtest1'] + $report['subtest2'] + $report['subtest3'] + $report['subtest4'] + $report['subtest5'])/5,2);
        $match = array_search(max($model), $model);
        $report['slug']       = $slug;
        $report['title']      = $info_survey['title'];
        $report['avg']        = $avg;
        $report['jobs']       = $this -> result -> get_gti_job_preferences($match);
        $report['criteria']   = $this -> result -> get_criteria_gti_report($report['avg']);

        //dari sini
        $kekuatan_kelemahan = DB::table('report_gti_detail')->where('user_id', '=', $id_user)->where('survey_id', '=', $id_survey)->first();
        if($kekuatan_kelemahan){
            $report['kelemahan'] = json_decode($kekuatan_kelemahan->kelemahan);
            $report['kekuatan']  = json_decode($kekuatan_kelemahan->kekuatan);
        }else{
            for($i = 1; $i <= 5; $i++){
                if($report['subtest'.$i] < 92){
                    $report['kelemahan'][$i]   = $this -> result -> get_kekuatan_kelemahan_gti('GTQ'.$i);
                    $report['kekuatan'][$i]    = '';
                }elseif($report['subtest'.$i] > 105.9){
                    $report['kekuatan'][$i]    = $this -> result -> get_kekuatan_kelemahan_gti('GTQ'.$i);
                    $report['kelemahan']       = '';
                }else{
                    $report['kekuatan'][$i]    = '';
                    $report['kelemahan'][$i]   = '';
                }
            }
        }

        return $report;
    }

    public function generate_pdf_tiki($slug, $id_user){
          if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
          }
          $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
          $id_survey          = $info_survey['id'];
          $raw_score          = $this -> result -> get_report_gti_tiki($id_survey, $id_user);
          $report['date']       = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);

          $total  = 0;
          $i      = 0;
          foreach ($raw_score as $key => $value) {
              if($key != 'subtest5'){
                  $report['nilai_standar'][$i] = $value;
                  $total = $total + $value;
                  $i++;
              }
          }
          $report['total_tiki'] = $total;
          $report['title']      = $info_survey['title'];
          if($total <= 29){ //batas bawah
              $report['iq'] = 56;
          }elseif($total > 107){ //batas atas
              $report['iq'] = 145;
          }else{ //lookup
              $report['iq']     = $this -> result -> get_iq_tiki($total);
          }          

          if($report['iq'] <= 80){
              $report['golongan_iq'] = "Di bawah Rata-Rata";
          }elseif($report['iq'] <= 90){
              $report['golongan_iq'] = "Rata-Rata Rendah";
          }elseif($report['iq'] <= 100){
              $report['golongan_iq'] = "Rata-Rata";
          }elseif($report['iq'] <= 110){
              $report['golongan_iq'] = "Rata-Rata Atas";
          }elseif($report['iq'] <= 125){
              $report['golongan_iq'] = "Di Atas Rata-Rata";
          }elseif($report['iq'] >= 126){
              $report['golongan_iq'] = "Jauh Di Atas Rata-Rata";
          }

          $report['f1'] = round(($report['nilai_standar'][1] + $report['nilai_standar'][3]) / 2, 2);
          $report['f3'] = round(($report['nilai_standar'][0] + $report['nilai_standar'][2]) / 2, 2);
          $report['f2'] = round(($report['nilai_standar'][0] + $report['nilai_standar'][2] + $report['nilai_standar'][3]) / 3, 2);

          if($report['f1'] <= 7.49){
              $report['golongan_f1'] = "Kurang Sekali";
          }elseif($report['f1'] <= 12.49){
              $report['golongan_f1'] = "Kurang";
          }elseif($report['f1'] <= 14.49){
              $report['golongan_f1'] = "Sedang Bawah";
          }elseif($report['f1'] <= 17.49){
              $report['golongan_f1'] = "Sedang Atas";
          }elseif($report['f1'] <= 22.49){
              $report['golongan_f1'] = "Cukup Baik";
          }elseif($report['f1'] >= 22.5){
              $report['golongan_f1'] = "Baik";
          }

          if($report['f2'] <= 7.49){
              $report['golongan_f2'] = "Kurang Sekali";
          }elseif($report['f2'] <= 12.49){
              $report['golongan_f2'] = "Kurang";
          }elseif($report['f2'] <= 14.49){
              $report['golongan_f2'] = "Sedang Bawah";
          }elseif($report['f2'] <= 17.49){
              $report['golongan_f2'] = "Sedang Atas";
          }elseif($report['f2'] <= 22.49){
              $report['golongan_f2'] = "Cukup Baik";
          }elseif($report['f2'] >= 22.5){
              $report['golongan_f2'] = "Baik";
          }

          if($report['f3'] <= 7.49){
              $report['golongan_f3'] = "Kurang Sekali";
          }elseif($report['f3'] <= 12.49){
              $report['golongan_f3'] = "Kurang";
          }elseif($report['f3'] <= 14.49){
              $report['golongan_f3'] = "Sedang Bawah";
          }elseif($report['f3'] <= 17.49){
              $report['golongan_f3'] = "Sedang Atas";
          }elseif($report['f3'] <= 22.49){
              $report['golongan_f3'] = "Cukup Baik";
          }elseif($report['f3'] >= 22.5){
              $report['golongan_f3'] = "Baik";
          }

          return $report;
    }

    public function generate_pdf_wpa($slug, $id_user){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey      = $info_survey['id'] ;
        $report['title']= $info_survey['title'];
        $report['date'] = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);

        $report_wpa   = $this -> result -> get_score_wpa($id_survey, $id_user);
        foreach ($report_wpa as $key) {
            $most   = json_decode($key['most']);
            $lest   = json_decode($key['lest']);
            $change = json_decode($key['change']);
        }
        //most graph
        $most_d = $most -> d;
        $most_i = $most -> i;
        $most_s = $most -> s;
        $most_c = $most -> c;
        $report['most_d']  = WpaMostD($most_d);
        $report['most_i']  = WpaMostI($most_i);
        $report['most_s']  = WpaMostS($most_s);
        $report['most_c']  = WpaMostC($most_c);

        //lest graph
        $lest_d = $lest -> d;
        $lest_i = $lest -> i;
        $lest_s = $lest -> s;
        $lest_c = $lest -> c;
        $report['lest_d']  = WpaLeastD($lest_d);
        $report['lest_i']  = WpaLeastI($lest_i);
        $report['lest_s']  = WpaLeastS($lest_s);
        $report['lest_c']  = WpaLeastC($lest_c);

        //change graph
        $change_d = $change -> d;
        $change_i = $change -> i;
        $change_s = $change -> s;
        $change_c = $change -> c;
        $report['change_d']  = WpaChangeD($change_d);
        $report['change_i']  = WpaChangeI($change_i);
        $report['change_s']  = WpaChangeS($change_s);
        $report['change_c']  = WpaChangeC($change_c);
        $result                     = $this -> result -> get_hasil_wpa($id_user, $id_survey);
        $rekap_detail   = $this -> result -> get_rekap_wpa_detail($id_user, $id_survey);

        $k_umum = json_decode($rekap_detail->karakteristik_umum);
        $perilaku_kekuatan = json_decode($rekap_detail->perilaku_kerja_kekuatan);
        $perilaku_kelemahan = json_decode($rekap_detail->perilaku_kerja_kelemahan);
        $emosi_kekuatan = json_decode($rekap_detail->suasana_emosi_kekuatan);
        $emosi_kelemahan = json_decode($rekap_detail->suasana_emosi_kelemahan);
        $k_pekerjaan = json_decode($rekap_detail->karakteristik_pekerjaan);
        $kuat = json_decode($rekap_detail->kekuatan);
        $lemah = json_decode($rekap_detail->kelemahan);
        $uraian_kepribadian = $rekap_detail->uraian_kepribadian;

        $report['karakteristik_umum']         = $k_umum;
        $report['perilaku_kerja_kekuatan']    = $perilaku_kekuatan;
        $report['perilaku_kerja_kelemahan']   = $perilaku_kelemahan;
        $report['suasana_emosi_kekuatan']     = $emosi_kekuatan;
        $report['suasana_emosi_kelemahan']    = $emosi_kelemahan;
        $report['karakteristik_pekerjaan']    = $k_pekerjaan;
        $report['kekuatan']                   = $kuat;
        $report['kelemahan']                  = $lemah;
        $report['uraian_kepribadian']         = $uraian_kepribadian;
        $report['type_kepribadian']           = $result['type'];
        $report['result']                     = $result['hasil'];

        return $report;
    }

    public function generate_pdf_wba($slug, $id_user){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey      = $info_survey['id'] ;
        $report_wba         = $this -> result -> get_report_papi($id_survey, $id_user);
        $report['title']= $info_survey['title'];
        $report['date'] = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);

        foreach ($report_wba as $key) {
            $report['N'] = $key -> N;
            $report['G'] = $key -> G;
            $report['A'] = $key -> A;
            $report['L'] = $key -> L;
            $report['P'] = $key -> P;
            $report['I'] = $key -> I;
            $report['T'] = $key -> T;
            $report['V'] = $key -> V;
            $report['X'] = $key -> X;
            $report['S'] = $key -> S;
            $report['B'] = $key -> B;
            $report['O'] = $key -> O;
            $report['R'] = $key -> R;
            $report['D'] = $key -> D;
            $report['C'] = $key -> C;
            $report['Z'] = $key -> Z;
            $report['E'] = $key -> E;
            $report['K'] = $key -> K;
            $report['F'] = $key -> F;
            $report['W'] = $key -> W;
        }
        $result = $this -> result -> get_hasil_interpretasi_papi($report);
        $report['result'] = $result;

        return $report;
    }
}
