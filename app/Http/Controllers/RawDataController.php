<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use Carbon\Carbon;
use Entrust;

class RawDataController extends Controller
{
	public function __construct(SurveyRepositoryInterface $survey, ResultRepositoryInterface $result)
    {
        $this -> survey         = $survey;
        $this -> result         = $result;
    }
    //============================================= RAW DATA =============================================//

    public function row_survey(){ //tampilkan list tools
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $user_id= Auth::user()-> id;
        $i      = 0;
        $survey = $this -> result -> ambil_row_survey($user_id);
        $rowset = null;
        if($survey){
            foreach($survey as $key){
                $rowset[$i]['title'] = $key -> title;
                $rowset[$i]['name'] = $key -> name;
                $rowset[$i]['detail'] = "<a href='/backend/raw-data/nilai/".$key -> slug."/".$key -> slug_project."'><strong class='btn btn-xs btn-primary'>Lihat Detil</strong></a>";
                $i++;
            }
        }
        return view('row_data.row_survey', compact('rowset'));
    }

    public function row_data_nilai($slug, $slug_project){ //tampilkan list user dan nilai per subtest
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey = $this -> survey -> get_info_survey_by_slug($slug);
        $info_project = $this -> survey -> get_info_project_by_slug($slug_project);
        $i = 0;
        $user = $this -> result -> ambil_row_peserta_survey($info_survey['id']);
        $rowset = null;

        if($user){
            foreach ($user as $key) {
                $skor = $this -> result -> ambil_nilai_peserta($key['user_id'], $info_survey['id']);
                $j = 1;
                foreach ($skor as $value) {
                    $nilai['score_'.$j] = $value['score'];
                    $nilai['section_'.$j] = $value['section_id'];
                    $j++;
                }  

                $rowset[$i]['subtest_1'] = "<a href='/backend/raw-data/nilai/".$slug."/".$slug_project."/".$key['user_id']."/".$nilai['section_1']."' class='btn btn-".rowDataScoreLai($nilai['score_1'])." btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Lihat Detail'>".$nilai['score_1']."</a>";
                $rowset[$i]['subtest_2'] = "<a href='/backend/raw-data/nilai/".$slug."/".$slug_project."/".$key['user_id']."/".$nilai['section_2']."' class='btn btn-".rowDataScoreLai($nilai['score_2'])." btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Lihat Detail'>".$nilai['score_2']."</a>";
                $rowset[$i]['subtest_3'] = "<a href='/backend/raw-data/nilai/".$slug."/".$slug_project."/".$key['user_id']."/".$nilai['section_3']."' class='btn btn-".rowDataScoreLai($nilai['score_3'])." btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Lihat Detail'>".$nilai['score_3']."</a>";
                $rowset[$i]['subtest_4'] = "<a href='/backend/raw-data/nilai/".$slug."/".$slug_project."/".$key['user_id']."/".$nilai['section_4']."' class='btn btn-".rowDataScoreLai($nilai['score_4'])." btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Lihat Detail'>".$nilai['score_4']."</a>";

                //untuk LAI sampai subtest 5
                if($info_survey['category_survey_id'] == 1){
                    $rowset[$i]['subtest_5'] = "<a href='/backend/raw-data/nilai/".$slug."/".$slug_project."/".$key['user_id']."/".$nilai['section_5']."' class='btn btn-".rowDataScoreLai($nilai['score_5'])." btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Lihat Detail'>".$nilai['score_5']."</a>";
                    $rowset[$i]['kesimpulan'] = "<a href='/backend/report/gti/".$slug."/".$key['user_id']."' class='pull-right' target='blank'><strong> <span class='glyphicon glyphicon-stats'></span> Kesimpulan</strong></a>";
                }elseif($info_survey['category_survey_id'] == 2){
                    $rowset[$i]['kesimpulan'] = "<a href='/backend/report/tiki/".$slug."/".$key['user_id']."' class='pull-right' target='blank'><strong> <span class='glyphicon glyphicon-stats'></span> Kesimpulan</strong></a>";
                }
                $rowset[$i]['first_name'] = "<strong> <span class='glyphicon glyphicon-user'></span> ".$key['first_name']."</strong>";
                $rowset[$i]['user_id']    = $key['user_id'];
                $i++;
            }
        }
        if($info_survey['category_survey_id'] == 1){
            return view('row_data.row_user_lai', compact('rowset', 'info_project'));
        }elseif($info_survey['category_survey_id'] == 2){
            return view('row_data.row_user_tiki', compact('rowset', 'info_project'));
        }

    }

    public function row_data_jawaban($slug, $slug_project, $user_id, $section_id){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey = $this -> survey -> get_info_survey_by_slug($slug);
        if($info_survey['category_survey_id'] == 1){
            return redirect('/backend/raw-data/jawaban/lai/'.$slug.'/'.$slug_project.'/'.$user_id.'/'.$section_id);
        }elseif($info_survey['category_survey_id'] == 2){
            return redirect('/backend/raw-data/jawaban/tiki/'.$slug.'/'.$slug_project.'/'.$user_id.'/'.$section_id);
        }
    }

    public function row_jawaban_lai($slug, $slug_project, $user_id, $section_id){ //tampilkan list jawaban user tools LAI
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $result = $this -> result -> ambil_row_result_lai($user_id, $section_id);
        $info['section_name']   = DB::table('section_survey') -> where('id', '=', $section_id) -> first() -> name;
        $info['slug']           = $slug;
        $info['slug_project']   = $slug_project;
        $info['user_id']        = $user_id;
        $i = 0;

        if(sizeof($result) > 0){
            foreach ($result as $key) {
                $true_answer = $this -> result -> ambil_kunci_jawaban($key['id_question']);
                $j = 0;
                foreach ($true_answer as $jawaban_benar) {
                    $data[$i]['kunci'][$j]      = $jawaban_benar;
                    $j++;
                }
                $data[$i]['nomor']      = $key['number'];
                $data[$i]['pertanyaan'] = $key['question'];
                $data[$i]['jawaban']    = $key['answer'];
                $data[$i]['hasil']      = $key['point'];
                $i++;
            }
        }else{
            $data = '-';
        }
        return view('row_data.row_jawaban_lai', compact('data', 'info'));
    }

    public function row_jawaban_tiki($slug, $slug_project, $user_id, $section_id){ //tampilkan list jawaban user tools TIKI
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        //untuk info di view dan keperluan path gambar
        $section_info   = $this -> survey -> get_info_section($section_id);
        $section_slug   = $section_info['slug'];
        $info['section_name']   = DB::table('section_survey') -> where('id', '=', $section_id) -> first() -> name;
        $info['slug']           = $slug;
        $info['slug_project']   = $slug_project;
        $info['user_id']        = $user_id;
        $info['folder_image']   = '';
        for($i = 0; $i < strlen($section_slug)-11; $i++){
            $info['folder_image'] .= $section_slug[$i];
        }

        $get_question = $this -> result -> ambil_row_pertanyaan_tiki($user_id, $section_id);
        $question = null;
        if($get_question){
            foreach ($get_question as $key) {
                $id_question = $key['id_question'];
                if(isset($question[$id_question])){
                    $question[$id_question][] = $key;
                }else{
                    $question[$id_question] = array($key);
                }
            }
        }

        if(sizeof($question) > 0){
            $i = 0;
            foreach ($question as $pertanyaan) {
                $j = 0;
                foreach ($pertanyaan as $key) {
                    $true_answer= $this -> result -> ambil_kunci_jawaban($key['id_question']);
                    $k = 0;
                    foreach ($true_answer as $kunci) {
                        $data[$i]['kunci'][$k]      = $kunci;
                        $k++;
                    }
                    $data[$i]['nomor']      = $key['number'];
                    $data[$i]['pertanyaan'] = $key['question'];
                    $data[$i]['jawaban'][$j]= $key['answer'];
                    $data[$i]['hasil'][$j]  = $key['point'];
                    $j++;
                }
                $i++;
            }
        }else{
            $data = '-';
        }

        return view('row_data.row_jawaban_tiki', compact('data', 'info'));
    }

    public function raw_input_project(){ //tampilkan list project
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $project = $this -> survey -> get_all_project();
        $i = 0;
        $rowset = null;
        if($project){
            foreach($project as $key){
                $rowset[$i]['name'] = "<span class='glyphicon glyphicon-search'></span> ".$key->name;
                $tools = $this -> survey -> get_list_survey_by_project($key -> id);
                $j = 0;
                foreach ($tools as $keys) {
                	if($keys -> category_survey_id == 1){
                		$title = 'LAI';
                		$btn = 'btn-success';
                	}elseif($keys -> category_survey_id == 2){
                		$title = 'TIKI';
                		$btn = 'btn-info';
                	}elseif($keys -> category_survey_id == 3){
                		$title = 'WPA';
                		$btn = 'btn-warning';
                	}elseif($keys -> category_survey_id == 4){
                		$title = 'WBA';
                		$btn = 'btn-danger';
                	}
                	$rowset[$i]['tools'][$j] = "<a href='/backend/raw-input/".$title."/".$key->slug."/".$keys->slug."' ><strong class='btn btn-xs ".$btn."'>".$title."</strong></a>";
                	$j++;
                }
                $i++;
            }
        }
        
        return view('raw_input.list_project', compact('rowset'));
    }

    public function raw_input_peserta_tiki($slug_project, $slug_tools){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_project 	= $this -> survey -> get_info_project_by_slug($slug_project);
        $info_tools		= $this -> survey -> get_info_survey_by_slug($slug_tools);
        $tools_id 		= $info_tools['id'];

        $raw = $this -> result -> get_raw_input_by_tools($tools_id);
        $data = null;
        $i = 0;
        $j = 0;
        $old_user = null;
        foreach ($raw as $key) {
        	$user_id = $key['user_id'];
        	if($old_user == null){ //cek jika user pertama
        		$old_user = $user_id;
        	}
        	if($old_user != $user_id){ //cek jika ganti user
        		$i++;
        		$j=0;
        		$old_user = $user_id;
        	}

            //setting index subtest berdasarkan nama subtest
            $subtest_name = DB::table('section_survey') -> where('id', '=', $key['section_id']) -> first() -> name;
            if($subtest_name == 'Berhitung Angka'){
                $subt = 1;
            }elseif($subtest_name == 'Gabungan Bagian'){
                $subt = 2;
            }elseif($subtest_name == 'Hubungan Kata'){
                $subt = 3;
            }elseif($subtest_name == 'Abstraksi Non Verbal'){
                $subt = 4;
            }

        	$data[$i]['nama_peserta'] = DB::table('profiles')-> where('user_id', '=', $key['user_id']) -> first() -> first_name;
        	$k = 0; //index untuk pilihan jawaban (pakai index karna ada yang multiple answer)
        	$old_number = null;
        	foreach (json_decode($key['raw_input'], true) as $keys) {
        		$number = $keys['number'];
        		if($old_number == null){ //cek jika nomor pertama
        			$old_number = $number;
        		}
        		if($old_number != $number){ //cek jika ganti nomor
        			$k=0;
        			$old_number = $number;
        		}else{ //jika nomor masih sama, maka index pilihan jawaban nambah
        			$k++;
        		}
        		$data[$i][$subt][$keys['number']][$k] = DB::table('option_answer') -> where('id', '=', $keys['id_answer']) -> first() -> opsi;
        		$j++;
        	}
        }

        //inisialisai array jawaban sebanyak jumlah soal pada tiap subtest dengan '-'
		$subtest_1 = array_fill(1,40,'-');
		$subtest_2 = array_fill(1,26,'-');
		$subtest_3 = array_fill(1,40,'-');
		$subtest_4 = array_fill(1,30,'-');
		$i = 0;

        if (sizeof($data)>0) {
            foreach ($data as $key) {
                $jwbn_subtest = array();
                $rowset[$i]['nama_peserta'] = $key['nama_peserta'];

                //cek apakah user menjawab subtest 1, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('1', $key)){
                    foreach ($key[1] as $keys => $value) {
                        $jwbn_subtest[1][$keys] = $value;
                    }
                }else{
                    $jwbn_subtest[1] = array_fill(1,40,'-');
                }

                //cek apakah user menjawab subtest 1, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('2', $key)){
                    foreach ($key[2] as $keys => $value) {
                        $jwbn_subtest[2][$keys] = $value;
                    }
                }else{
                    $jwbn_subtest[2] = array_fill(1,26,'-');
                }

                //cek apakah user menjawab subtest 1, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('3', $key)){
                    foreach ($key[3] as $keys => $value) {
                        $jwbn_subtest[3][$keys] = $value;
                    }
                }else{
                    $jwbn_subtest[3] = array_fill(1,40,'-');
                }

                //cek apakah user menjawab subtest 1, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('4', $key)){
                    foreach ($key[4] as $keys => $value) {
                        $jwbn_subtest[4][$keys] = $value;
                    }
                }else{
                    $jwbn_subtest[4] = array_fill(1,30,'-');
                }

                //proses mengganti array yang tadi '-' dengan jawaban user sesuai nomor soal yang dijawab
                $raw_input_1 = array_replace($subtest_1, $jwbn_subtest[1]);
                $raw_input_2 = array_replace($subtest_2, $jwbn_subtest[2]);
                $raw_input_3 = array_replace($subtest_3, $jwbn_subtest[3]);
                $raw_input_4 = array_replace($subtest_4, $jwbn_subtest[4]);
                $rowset[$i]['subtest_1'] = $raw_input_1;
                $rowset[$i]['subtest_2'] = $raw_input_2;
                $rowset[$i]['subtest_3'] = $raw_input_3;
                $rowset[$i]['subtest_4'] = $raw_input_4;
                $i++;
            }
        } else {
            $rowset = null;
        }
        
        return view('raw_input.raw_input_tiki', compact('rowset', 'info_project', 'info_tools'));
    }

    public function raw_input_peserta_lai($slug_project, $slug_tools){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_project   = $this -> survey -> get_info_project_by_slug($slug_project);
        $info_tools     = $this -> survey -> get_info_survey_by_slug($slug_tools);
        $tools_id       = $info_tools['id'];

        $raw = $this -> result -> get_raw_input_by_tools($tools_id);
        $data = null;
        $i = 0;
        $j = 0;
        $old_user = null;
        foreach ($raw as $key) {
            $user_id = $key['user_id'];
            if($old_user == null){ //cek jika user pertama
                $old_user = $user_id;
            }
            if($old_user != $user_id){ //cek jika ganti user
                $i++;
                $j=0;
                $old_user = $user_id;
            }

            //setting index subtest berdasarkan nama subtest
            $subtest_name = DB::table('section_survey') -> where('id', '=', $key['section_id']) -> first() -> name;
            if($subtest_name == 'Subtest 1'){
                $subt = 1;
            }elseif($subtest_name == 'Subtest 2'){
                $subt = 2;
            }elseif($subtest_name == 'Subtest 3'){
                $subt = 3;
            }elseif($subtest_name == 'Subtest 4'){
                $subt = 4;
            }elseif($subtest_name == 'Subtest 5'){
                $subt = 5;
            }

            $data[$i]['nama_peserta'] = DB::table('profiles')-> where('user_id', '=', $key['user_id']) -> first() -> first_name;
            $k = 0; //index untuk pilihan jawaban (pakai index utk mengakomodir yang multiple answer)
            $old_number = null;
            foreach (json_decode($key['raw_input'], true) as $keys) {
                $number = $keys['number'];
                if($old_number == null){ //cek jika nomor pertama
                    $old_number = $number;
                }
                if($old_number != $number){ //cek jika ganti nomor
                    $k=0;
                    $old_number = $number;
                }
                
                $data[$i][$subt][$keys['number']][$k] = DB::table('option_answer') -> where('id', '=', $keys['id_answer']) -> first() -> opsi;
                $j++;
            }
        }
        //inisialisai array jawaban sebanyak jumlah soal pada tiap subtest dengan '-'
        $subtest_1 = array_fill(1,60,'-');
        $subtest_2 = array_fill(1,50,'-');
        $subtest_3 = array_fill(1,72,'-');
        $subtest_4 = array_fill(1,60,'-');
        $subtest_5 = array_fill(1,60,'-');
        $rowset = null;
        $i = 0;

        if(sizeof($data)>0){
            foreach ($data as $key) {
                $jwbn_subtest = array();
                $rowset[$i]['nama_peserta'] = $key['nama_peserta'];

                //cek apakah user menjawab subtest 1, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('1', $key)){
                    foreach ($key[1] as $keys => $value) {
                        $jwbn_subtest[1][$keys] = $value;
                    }
                }else{
                    $jwbn_subtest[1] = array_fill(1,60,'-');
                }
                //cek apakah user menjawab subtest 2, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('2', $key)){
                    foreach ($key[2] as $keys => $value) {
                        $jwbn_subtest[2][$keys] = $value;
                    }
                }else{
                    $jwbn_subtest[2] = array_fill(1,50,'-');
                }

                //cek apakah user menjawab subtest 3, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('3', $key)){
                    foreach ($key[3] as $keys => $value) {
                        $jwbn_subtest[3][$keys] = $value;
                    }
                }else{
                    $jwbn_subtest[3] = array_fill(1,72,'-');
                }

                //cek apakah user menjawab subtest 4, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('4', $key)){
                    foreach ($key[4] as $keys => $value) {
                        $jwbn_subtest[4][$keys] = $value;
                    }
                    
                }else{
                    $jwbn_subtest[4] = array_fill(1,60,'-');
                }

                //cek apakah user menjawab subtest 5, jika ya maka ambil jawaban, jika tidak maka set '-' pada setiap nomor
                if(array_key_exists('5', $key)){
                    foreach ($key[5] as $keys => $value) {
                        $jwbn_subtest[5][$keys] = $value;
                    }
                    
                }else{
                    $jwbn_subtest[5] = array_fill(1,60,'-');
                }

                //proses mengganti array yang tadi '-' dengan jawaban user sesuai nomor soal yang dijawab
                $raw_input_1 = array_replace($subtest_1, $jwbn_subtest[1]);
                $raw_input_2 = array_replace($subtest_2, $jwbn_subtest[2]);
                $raw_input_3 = array_replace($subtest_3, $jwbn_subtest[3]);
                $raw_input_4 = array_replace($subtest_4, $jwbn_subtest[4]);
                $raw_input_5 = array_replace($subtest_5, $jwbn_subtest[5]);
                $rowset[$i]['subtest_1'] = $raw_input_1;
                $rowset[$i]['subtest_2'] = $raw_input_2;
                $rowset[$i]['subtest_3'] = $raw_input_3;
                $rowset[$i]['subtest_4'] = $raw_input_4;
                $rowset[$i]['subtest_5'] = $raw_input_5;
                $i++;
            }
        } else {
            $rowset = null;
        }

        return view('raw_input.raw_input_lai', compact('rowset', 'info_project', 'info_tools'));
    }

    public function raw_input_peserta_wpa($slug_project, $slug_tools){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_project   = $this -> survey -> get_info_project_by_slug($slug_project);
        $info_tools     = $this -> survey -> get_info_survey_by_slug($slug_tools);
        $tools_id       = $info_tools['id'];

        $raw = $this -> result -> get_raw_input_by_tools($tools_id);
        $data = null;
        $i = 0;
        $j = 0;
        $old_user = null;
        if (sizeof($raw)>0) {
            foreach ($raw as $key) {
                $user_id = $key['user_id'];
                if($old_user == null){ //cek jika user pertama
                    $old_user = $user_id;
                }
                if($old_user != $user_id){ //cek jika ganti user
                    $i++;
                    $j=0;
                    $old_user = $user_id;
                }

                $rowset[$i]['nama_peserta'] = DB::table('profiles')-> where('user_id', '=', $key['user_id']) -> first() -> first_name;
                $k = 0; //index untuk pilihan jawaban (pakai index utk mengakomodir yang multiple answer)
                $old_number = null;
                foreach (json_decode($key['raw_input'], true) as $keys) {
                    $number = $keys['number'];
                    if($old_number == null){ //cek jika nomor pertama
                        $old_number = $number;
                    }
                    if($old_number != $number){ //cek jika ganti nomor
                        $k=0;
                        $old_number = $number;
                    }
                    
                    $rowset[$i]['jawaban'][$keys['number']]['M'] = $keys['M'];
                    $rowset[$i]['jawaban'][$keys['number']]['L'] = $keys['L'];
                    $j++;
                }
            }
        } else {
            $rowset = null;
        }
        
        return view('raw_input.raw_input_wpa', compact('rowset', 'info_project', 'info_tools'));
    }

    public function raw_input_peserta_wba($slug_project, $slug_tools){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_project   = $this -> survey -> get_info_project_by_slug($slug_project);
        $info_tools     = $this -> survey -> get_info_survey_by_slug($slug_tools);
        $tools_id       = $info_tools['id'];

        $raw = $this -> result -> get_raw_input_by_tools($tools_id);
        $data = null;
        $i = 0;
        $j = 0;
        $old_user = null;
        if (sizeof($raw)>0) {
            foreach ($raw as $key) {
                $user_id = $key['user_id'];
                if($old_user == null){ //cek jika user pertama
                    $old_user = $user_id;
                }
                if($old_user != $user_id){ //cek jika ganti user
                    $i++;
                    $j=0;
                    $old_user = $user_id;
                }

                $rowset[$i]['nama_peserta'] = DB::table('profiles')-> where('user_id', '=', $key['user_id']) -> first() -> first_name;
                $k = 0; //index untuk pilihan jawaban (pakai index utk mengakomodir yang multiple answer)
                $old_number = null;
                foreach (json_decode($key['raw_input'], true) as $keys) {
                    $number = $keys['number'];
                    if($old_number == null){ //cek jika nomor pertama
                        $old_number = $number;
                    }
                    if($old_number != $number){ //cek jika ganti nomor
                        $k=0;
                        $old_number = $number;
                    }
                    
                    $rowset[$i]['jawaban'][$number] = $keys['key'];
                    $j++;
                }
            }
            return view('raw_input.raw_input_wba', compact('rowset', 'info_project', 'info_tools'));
        } else {
            $rowset = null;
            return view('raw_input.raw_input_wba', compact('rowset', 'info_project', 'info_tools'));
        }
    }
}
