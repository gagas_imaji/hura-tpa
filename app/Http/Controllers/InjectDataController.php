<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use Entrust;

class InjectDataController extends Controller
{
    public function inject_laporan_panjang(){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $gagal = 0;

        //=============== Supervisor Ulang 2 (id_project = 47) ===============//
        $supervisor_ulang_2 = DB::table('user_report') -> where('tujuan', '=', 47) -> get();
        $i = 0;
        $update = null;
        DB::beginTransaction();
	    try{
	        foreach($supervisor_ulang_2 as $key){
	        	$update[$i]['user_id'] = $key -> user_id;
	        	$update[$i]['SS1'] = $key -> SS1;
	        	$update[$i]['SS2'] = $key -> SS2;
	        	$update[$i]['SS3'] = $key -> SS3;
	        	$update[$i]['SS4'] = $key -> SS4;
	        	$update[$i]['jumlah'] = $key -> jumlah;
	        	$update[$i]['IQ'] = $key -> IQ;
	        	$i++;
	        }

	        foreach($update as $key){
	        	DB::table('user_report') -> where('tujuan', '=', 29) -> where('user_id', '=', $key['user_id']) -> update([
	        		'SS1' => $key['SS1'],
	        		'SS2' => $key['SS2'],
	        		'SS3' => $key['SS3'],
	        		'SS4' => $key['SS4'],
	        		'jumlah' => $key['jumlah'],
	        		'IQ'  => $key['IQ'],
	        		'updated_at' => Carbon::now('Asia/Jakarta')

	        	]);
	        }
	        DB::commit();
    	}catch(exception $e){
            DB::rollback();
            $gagal++;
        }

        //=============== Supervisor Ulang (id_project = 45) ===============//
        $supervisor_ulang = DB::table('user_report') -> where('tujuan', '=', 45) -> get();
        $i = 0;
        $update = null;
        DB::beginTransaction();
	    try{
	        foreach($supervisor_ulang as $key){
	        	$update[$i]['user_id'] = $key -> user_id;
	        	$update[$i]['GTQ1'] = $key -> GTQ1;
	        	$update[$i]['GTQ2'] = $key -> GTQ2;
	        	$update[$i]['GTQ3'] = $key -> GTQ3;
	        	$update[$i]['GTQ4'] = $key -> GTQ4;
	        	$update[$i]['GTQ5'] = $key -> GTQ5;
	        	$update[$i]['kriteria'] = $key -> kriteria;
	        	$i++;
	        }

	        foreach($update as $key){
	        	DB::table('user_report') -> where('tujuan', '=', 29) -> where('user_id', '=', $key['user_id']) -> update([
	        		'GTQ1' => $key['GTQ1'],
	        		'GTQ2' => $key['GTQ2'],
	        		'GTQ3' => $key['GTQ3'],
	        		'GTQ4' => $key['GTQ4'],
	        		'GTQ5' => $key['GTQ5'],
	        		'kriteria' => $key['kriteria'],
	        		'updated_at' => Carbon::now('Asia/Jakarta')
	        	]);
	        }
	        DB::commit();
    	}catch(exception $e){
            DB::rollback();
            $gagal++;
        }

        //=============== Assessment Supervisor Ulang (id_project = 36) ===============//
        $assessment_supervisor_ulang = DB::table('user_report') -> where('tujuan', '=', 36) -> where('user_id', '=', 380) -> get();
        $i = 0;
        $update = null;
        DB::beginTransaction();
	    try{
	        foreach($assessment_supervisor_ulang as $key){
	        	$update[$i]['user_id'] = $key -> user_id;
	        	$update[$i]['SS1'] = $key -> SS1;
	        	$update[$i]['SS2'] = $key -> SS2;
	        	$update[$i]['SS3'] = $key -> SS3;
	        	$update[$i]['SS4'] = $key -> SS4;
	        	$update[$i]['jumlah'] = $key -> jumlah;
	        	$update[$i]['IQ'] = $key -> IQ;
	        	$update[$i]['GTQ1'] = $key -> GTQ1;
	        	$update[$i]['GTQ2'] = $key -> GTQ2;
	        	$update[$i]['GTQ3'] = $key -> GTQ3;
	        	$update[$i]['GTQ4'] = $key -> GTQ4;
	        	$update[$i]['GTQ5'] = $key -> GTQ5;
	        	$update[$i]['kriteria'] = $key -> kriteria;
	        	$i++;
	        }
	        foreach($update as $key){ //cuma Intan yang diinject utk project assessment supervisor ulang (TIKI dan LAI)
	        	DB::table('user_report') -> where('tujuan', '=', 29) -> where('user_id', '=', 380) -> update([
	        		'SS1' => $key['SS1'],
	        		'SS2' => $key['SS2'],
	        		'SS3' => $key['SS3'],
	        		'SS4' => $key['SS4'],
	        		'jumlah' => $key['jumlah'],
	        		'IQ' => $key['IQ'],
	        		'GTQ1' => $key['GTQ1'],
	        		'GTQ2' => $key['GTQ2'],
	        		'GTQ3' => $key['GTQ3'],
	        		'GTQ4' => $key['GTQ4'],
	        		'GTQ5' => $key['GTQ5'],
	        		'kriteria' => $key['kriteria'],
	        		'updated_at' => Carbon::now('Asia/Jakarta')

	        	]);
	        }
	        DB::table('user_project') -> where('id_project', '=', 29) -> where('id_user', '=', 380) -> update(['is_done' => 1]);
	        DB::commit();
    	}catch(exception $e){
            DB::rollback();
            $gagal++;
        }

        if($gagal != 0){
        	return 'Terjadi kesalahan';
        }else{
        	return 'Berhasil Inject Data';
        }
    }



    public function inject_laporan_subtest(){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $gagal = 0;

        //=============== Supervisor Ulang 2 (id_survey = 172) TIKI ===============//
        
        //proses ambil nilai dari project Supervisor Ulang 2 dan tampung ke array
        $survey_baru = DB::table('survey_report') -> where('survey_id', '=', 172) -> orderBy('user_id', 'asc') -> orderBy('section_id', 'asc') -> get();
        $i = 0;
        $j = 0;
        foreach($survey_baru as $key){
        	if($j == 4){
        		$j = 0;
        		$i++;
        	}
        	$tampung[$i]['user_id']  	= $key -> user_id;
        	$tampung[$i]['score'][$j]	= $key -> score;
        	$j++;
        }

        //proses update nilai di project induk
        DB::beginTransaction();
	    try{
	        foreach($tampung as $key){

	        	//ambil dulu section id dari project induk, kemudian masukkan ke array untuk jadi parameter yang akan diupdate score ny
	        	$project_induk = DB::table('survey_report') -> where('survey_id', '=', 106) -> where('user_id', '=', $key['user_id']) -> orderBy('section_id', 'asc') -> get();
	        	$k = 0;
	        	foreach ($project_induk as $value) {
	        		$section[$k] = $value -> section_id;
	        		$k++;
	        	}

	        	$j = 0;
	        	foreach ($key['score'] as $score) {
	        		$is_save = DB::table('survey_report') -> where('survey_id', '=', 106) -> where('user_id', '=', $key['user_id']) -> where('section_id', '=', $section[$j]) -> first();

	        		if($is_save){
	        			DB::table('survey_report') -> where('survey_id', '=', 106) -> where('user_id', '=', $key['user_id']) -> where('section_id', '=', $section[$j]) -> update(['score' => $score, 'updated_at' => Carbon::now('Asia/Jakarta')]);
	        		}else{
	        			DB::table('survey_report') -> insert([
	        				'survey_id' => 106,
	        				'user_id' => $key['user_id'],
	        				'section_id' => $section[$j],
	        				'score' => $score, 
	        				'updated_at' => Carbon::now('Asia/Jakarta')
	        			]);
	        		}
	        		$j++;
	        	}
	        }
	        DB::commit();
    	}catch(exception $e){
            DB::rollback();
            $gagal++;
        }

        //=============== Supervisor Ulang (id_survey = 167) LAI ===============//
        $tampung 		= null;
        $project_induk 	= null;
        $section 		= null;

        //proses ambil nilai dari project Supervisor Ulang dan tampung ke array
        $survey_baru = DB::table('survey_report') -> where('survey_id', '=', 167) -> orderBy('user_id', 'asc') -> orderBy('section_id', 'asc') -> get();
        $i = 0;
        $j = 0;
        foreach($survey_baru as $key){
        	if($j == 5){
        		$j = 0;
        		$i++;
        	}
        	$tampung[$i]['user_id']  	= $key -> user_id;
        	$tampung[$i]['score'][$j]	= $key -> score;
        	$j++;
        }

        //proses update nilai di project induk
        DB::beginTransaction();
	    try{
	        foreach($tampung as $key){

	        	//ambil dulu section id dari project induk, kemudian masukkan ke array untuk jadi parameter yang akan diupdate score ny
	        	$project_induk = DB::table('survey_report') -> where('survey_id', '=', 105) -> where('user_id', '=', $key['user_id']) -> orderBy('section_id', 'asc') -> get();
	        	$k = 0;
	        	foreach ($project_induk as $value) {
	        		$section[$k] = $value -> section_id;
	        		$k++;
	        	}

	        	$j = 0;
	        	foreach ($key['score'] as $score) {
	        		$is_save = DB::table('survey_report') -> where('survey_id', '=', 105) -> where('user_id', '=', $key['user_id']) -> where('section_id', '=', $section[$j]) -> first();

	        		if($is_save){
	        			DB::table('survey_report') -> where('survey_id', '=', 105) -> where('user_id', '=', $key['user_id']) -> where('section_id', '=', $section[$j]) -> update(['score' => $score, 'updated_at' => Carbon::now('Asia/Jakarta')]);
	        		}else{
	        			DB::table('survey_report') -> insert([
	        				'survey_id' => 105,
	        				'user_id' => $key['user_id'],
	        				'section_id' => $section[$j],
	        				'score' => $score, 
	        				'updated_at' => Carbon::now('Asia/Jakarta')
	        			]);
	        		}
	        		$j++;
	        	}
	        }
	        DB::commit();
    	}catch(exception $e){
            DB::rollback();
            $gagal++;
        }

        //=============== Assessment Supervisor Ulang (id_survey = 132) TIKI ===============//
        $tampung 		= null;
        $project_induk 	= null;
        $section 		= null;

        //proses ambil nilai dari project Assessment Supervisor Ulang dan tampung ke array CUMA UNTUK INTAN
        $survey_baru = DB::table('survey_report') -> where('survey_id', '=', 132) -> where('user_id', '=', 380) -> orderBy('user_id', 'asc') -> orderBy('section_id', 'asc') -> get();
        $i = 0;
        $j = 0;
        foreach($survey_baru as $key){
        	if($j == 4){
        		$j = 0;
        		$i++;
        	}
        	$tampung[$i]['user_id']  	= $key -> user_id;
        	$tampung[$i]['score'][$j]	= $key -> score;
        	$j++;
        }

        //proses update nilai di project induk
        DB::beginTransaction();
	    try{
	        foreach($tampung as $key){

	        	//ambil dulu section id dari project induk, kemudian masukkan ke array untuk jadi parameter yang akan diupdate score ny
	        	$project_induk = DB::table('survey_report') -> where('survey_id', '=', 106) -> where('user_id', '=', 389) -> orderBy('section_id', 'asc') -> get();
	        	$k = 0;
	        	foreach ($project_induk as $value) {
	        		$section[$k] = $value -> section_id;
	        		$k++;
	        	}
	        	$j = 0;
	        	foreach ($key['score'] as $score) { //CUMA UNTUK INTAN
	        		$is_save = DB::table('survey_report') -> where('survey_id', '=', 106) -> where('user_id', '=', 380) -> where('section_id', '=', $section[$j]) -> first();

	        		if($is_save){
	        			DB::table('survey_report') -> where('survey_id', '=', 106) -> where('user_id', '=', 380) -> where('section_id', '=', $section[$j]) -> update(['score' => $score, 'updated_at' => Carbon::now('Asia/Jakarta')]);
	        		}else{
	        			DB::table('survey_report') -> insert([
	        				'survey_id' => 106,
	        				'user_id' => 380,
	        				'section_id' => $section[$j],
	        				'score' => $score, 
	        				'updated_at' => Carbon::now('Asia/Jakarta')
	        			]);
	        		}
	        		$j++;
	        	}
	        }
	        DB::table('user_survey') -> where('id_survey', '=', 106) -> where('id_user', '=', 380) -> update(['is_done' => 1]);
	        DB::commit();
    	}catch(exception $e){
            DB::rollback();
            $gagal++;
        }

        //=============== Assessment Supervisor Ulang (id_survey = 131) LAI ===============//
        $tampung 		= null;
        $project_induk 	= null;
        $section 		= null;

        //proses ambil nilai dari project Supervisor Ulang dan tampung ke array
        $survey_baru = DB::table('survey_report') -> where('survey_id', '=', 131) -> where('user_id', '=', 380) -> orderBy('user_id', 'asc') -> orderBy('section_id', 'asc') -> get();
        $i = 0;
        $j = 0;
        foreach($survey_baru as $key){
        	if($j == 5){
        		$j = 0;
        		$i++;
        	}
        	$tampung[$i]['user_id']  	= $key -> user_id;
        	$tampung[$i]['score'][$j]	= $key -> score;
        	$j++;
        }

        //proses update nilai di project induk
        DB::beginTransaction();
	    try{
	        foreach($tampung as $key){

	        	//ambil dulu section id dari project induk, kemudian masukkan ke array untuk jadi parameter yang akan diupdate score ny
	        	$project_induk = DB::table('survey_report') -> where('survey_id', '=', 105) -> where('user_id', '=', 389) -> orderBy('section_id', 'asc') -> get();
	        	$k = 0;
	        	foreach ($project_induk as $value) {
	        		$section[$k] = $value -> section_id;
	        		$k++;
	        	}

	        	$j = 0;
	        	foreach ($key['score'] as $score) {
	        		$is_save = DB::table('survey_report') -> where('survey_id', '=', 105) -> where('user_id', '=', 380) -> where('section_id', '=', $section[$j]) -> first();

	        		if($is_save){
	        			DB::table('survey_report') -> where('survey_id', '=', 105) -> where('user_id', '=', 380) -> where('section_id', '=', $section[$j]) -> update(['score' => $score, 'updated_at' => Carbon::now('Asia/Jakarta')]);
	        		}else{
	        			DB::table('survey_report') -> insert([
	        				'survey_id' => 105,
	        				'user_id' => 380,
	        				'section_id' => $section[$j],
	        				'score' => $score, 
	        				'updated_at' => Carbon::now('Asia/Jakarta')
	        			]);
	        		}
	        		$j++;
	        	}
	        }
	        DB::table('user_survey') -> where('id_survey', '=', 105) -> where('id_user', '=', 380) -> update(['is_done' => 1]);
	        DB::commit();
    	}catch(exception $e){
            DB::rollback();
            $gagal++;
        }

        if($gagal != 0){
        	return 'Terjadi kesalahan';
        }else{
        	return 'Berhasil Inject Data Subtest';
        }
    }

    public function add_number_on_question(){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

    	$section_survey = DB::table('section_survey') -> get();
    	$i = 0;
    	foreach($section_survey as $key){
    		$section[$i] = $key -> id;
    		$i++;
    	}
    	foreach ($section as $sect) {
    		$question = DB::table('question') -> where('id_section', '=', $sect) -> get();
    		if($question -> count() != 0){
    			DB::beginTransaction();
	    		try{
	    			$i = 1;
	    			foreach ($question as $quest) {
	    				DB::table('question') -> where('id', '=', $quest -> id) -> update(['number' => $i]);
	    				$i++;
	    			}
    				DB::commit();
		    	}catch(exception $e){
		            DB::rollback();
		            return 'Gagal menambahkan nomor soal';
		        }
    		}
    	}
    		return 'Berhasil tambahkan nomor soal';
    }

    public function add_opsi_on_option_answer(){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $survey = DB::table('survey') -> whereIn('category_survey_id', [1,2]) -> get();
        DB::beginTransaction();
	   	try{
	        foreach($survey as $surv){
	        	$section = DB::table('section_survey') -> where('survey_id', '=', $surv -> id) -> get();
	        	foreach ($section as $sect) {
	        		$question = DB::table('question') -> where('id_section', '=', $sect -> id) -> get();
	        		foreach ($question as $quest) {
	        			$option = DB::table('option_answer') -> where('id_question', '=', $quest -> id) -> get();
	        			$opsi = 'a';
	        			foreach ($option as $opt) {
	        				DB::table('option_answer') -> where('id', '=', $opt -> id) -> update(['opsi' => $opsi]);
	        				$opsi++;
	        			}
	        		}
	        	}
	        }
	        DB::commit();
    	}catch(exception $e){
            DB::rollback();
            return 'Gagal menambahkan nomor soal';
        }
    	
    	return 'Berhasil tambahkan opsi';
    }

    public function serialize_lai_tiki(){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $in_id = [26,27,28,29]; /*1,3,4,5,7,12,13,14,16,17,18,19,20,21,22,23,24,25,26,27,28,29*/
        $project = DB::table('project') -> whereIn('id', $in_id) -> get();

        DB::beginTransaction();
	    try{
	        foreach($project as $pro){
	        	$survey = null;
	        	$section = null;
	        	$question = null;
	        	$result = null;
	        	$between = null;
	        	$id_quest = null;
	        	$survey = DB::table('survey') -> where('id_project', '=', $pro -> id) -> whereIn('category_survey_id', [1,2]) -> get();
	        	foreach($survey as $surv){
	        		$section = DB::table('section_survey') -> where('survey_id', '=', $surv -> id) -> get();
	        		foreach($section as $sect){
	        			$section_id = $sect -> id;
	        			$question = DB::table('question') -> where('id_section', '=', $section_id) -> get();
	        			$k = 0;
	        			foreach($question as $quest){
	        				$id_quest[$k] = $quest -> id;
	        				$k++;
	        			}
	        			$between = [$id_quest[0], $id_quest[$k-1]];
	        			$result = DB::table('result') -> select('result.*', 'question.number') -> join('question', 'question.id', '=', 'result.id_question') -> whereBetween('id_question', $between) -> orderBy('user_id') -> orderBy('id_question') -> get();
	        			$j = 0;
	        			$baris_data = 0;
	        			$user_old = null;
	        			$section_old = null;
	        			foreach ($result as $res) {
	        				$user_id = $res -> user_id;

	        				if($user_old == null || $section_old == null){
	        					$user_old = $user_id;
	        					$section_old = $section_id;
	        				}

	        				if($user_old != $user_id || $section_old != $section_id){
	        					$raw_input['raw_input'] = json_encode($inputan);
	        					$j = 0;
	        					$inputan = null;
	        					DB::table('raw_input') -> insert($raw_input);
	        					$raw_input = null;
	        					$inputan[$j] = [
	        						'number' 	=> $res -> number,
	        						'id_answer' => $res -> id_answer,
		        					'id_question' => $res -> id_question,
		        					'point' => $res -> point,
		        					'timer' => $res -> timer,
	        					];
	        					$user_old = $user_id;
	        					$section_old = $section_id;
	        				}else{
	        					$inputan[$j] = [
	        						'number' 	=> $res -> number,
	        						'id_answer' => $res -> id_answer,
		        					'id_question' => $res -> id_question,
		        					'point' => $res -> point,
		        					'timer' => $res -> timer,
	        					];
	        				}
	        				$raw_input['section_id'] = $sect -> id;
	        				$raw_input['survey_id'] = $surv -> id;
	        				$raw_input['category_id'] = $surv -> category_survey_id;
	        				$raw_input['user_id'] = $res -> user_id;
	        				$j++;
	        				$baris_data++;

	        				//pengecekan data terakhir
				    		if($baris_data == count($result)){
				    			$raw_input['raw_input'] = json_encode($inputan);
				    			$j = 0;
	        					$inputan = null;
	        					DB::table('raw_input') -> insert($raw_input);
				    		}
	        			}
	        		}
	        	}
	        }
	        DB::commit();
	        return 'Berhasil serialize inputan LAI TIKI';
    	}catch(exception $e){
            DB::rollback();
            return 'gagal';
        }
    }	

    public function serialize_wpa(){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        
        $in_id = [22,23,24,25,26,27,28]; /*1,3,4,5,7,8,9,12,13,14,16,17,18,19,20,21,22,23,24,25,26,27,28*/
        $project = DB::table('project') -> whereIn('id', $in_id) -> get();
        DB::beginTransaction();
	    try{
	        foreach($project as $pro){
	        	$survey 	= null;
	        	$section 	= null;
	        	$result_wpa = null;
    			$j 			= 0;
    			$baris_data = 0;
    			$user_old 	= null;
    			$survey_old = null;
	        	$survey 	= DB::table('survey') -> where('id_project', '=', $pro -> id) -> where('category_survey_id', '=', 3) -> first();
	        	$section 	= DB::table('section_survey') -> where('survey_id', '=', $survey -> id) -> first();
	        	$result_wpa = DB::table('result_wpa') -> select('result_wpa.*') -> where('result_wpa.survey_id', '=', $survey -> id) -> orderBy('user_id') -> orderBy('number') -> get();
	        			
	        	$section_id = $section -> id;
	        	$survey_id 	= $survey -> id;
    			foreach ($result_wpa as $res) {
    				$user_id = $res -> user_id;

    				if($user_old == null || $survey_old == null){
    					$user_old = $user_id;
    					$survey_old = $survey_id;
    				}

    				if($user_old != $user_id || $survey_old != $survey_id){
    					$raw_input['raw_input'] = json_encode($inputan);
    					$j = 0;
    					$inputan = null;
    					DB::table('raw_input') -> insert($raw_input);
    					$raw_input 	= null;
    					$inputan[$j]= [
    						'number' 	=> $res -> number,
    						'M' 		=> $res -> M,
        					'L' 		=> $res -> L
    					];
    					$user_old = $user_id;
    					$survey_old = $survey_id;
    				}else{
    					$inputan[$j] = [
    						'number'	=> $res -> number,
    						'M' 		=> $res -> M,
        					'L' 		=> $res -> L
    					];
    				}
    				$raw_input['section_id']	= $section_id;
    				$raw_input['survey_id']		= $survey_id;
    				$raw_input['category_id'] 	= 3;
    				$raw_input['user_id'] 		= $user_id;
    				$j++;
    				$baris_data++;

    				//pengecekan data terakhir
		    		if($baris_data == count($result_wpa)){
		    			$raw_input['raw_input'] = json_encode($inputan);
		    			$j = 0;
    					$inputan = null;
    					DB::table('raw_input') -> insert($raw_input);
		    		}
    			}
	        }
	        DB::commit();
	        return 'Berhasil serialize inputan WPA';
    	}catch(exception $e){
            DB::rollback();
            return 'gagal';
        }
    }

    public function serialize_wba(){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        
        $in_id = [16,18,19,20,21,22,24,25,27,28,29]; /*16,18,19,20,21,22,24,25,27,28,29*/
        $project = DB::table('project') -> whereIn('id', $in_id) -> get();
        DB::beginTransaction();
	    try{
	        foreach($project as $pro){
	        	$survey 	= null;
	        	$section 	= null;
	        	$result_papikostick = null;
    			$j 			= 0;
    			$baris_data = 0;
    			$user_old 	= null;
    			$survey_old = null;
	        	$survey 	= DB::table('survey') -> where('id_project', '=', $pro -> id) -> where('category_survey_id', '=', 4) -> first();
	        	$section 	= DB::table('section_survey') -> where('survey_id', '=', $survey -> id) -> first();
	        	$result_papikostick = DB::table('result_papikostick') -> select('result_papikostick.*') -> where('result_papikostick.survey_id', '=', $survey -> id) -> orderBy('user_id') -> orderBy('number') -> get();
	        			
	        	$section_id = $section -> id;
	        	$survey_id 	= $survey -> id;
    			foreach ($result_papikostick as $res) {
    				$user_id = $res -> user_id;

    				if($user_old == null || $survey_old == null){
    					$user_old = $user_id;
    					$survey_old = $survey_id;
    				}

    				if($user_old != $user_id || $survey_old != $survey_id){
    					$raw_input['raw_input'] = json_encode($inputan);
    					$j = 0;
    					$inputan = null;
    					DB::table('raw_input') -> insert($raw_input);
    					$raw_input 	= null;
    					$inputan[$j]= [
    						'number' 	=> $res -> number,
        					'key' 		=> $res -> key
    					];
    					$user_old = $user_id;
    					$survey_old = $survey_id;
    				}else{
    					$inputan[$j] = [
    						'number'	=> $res -> number,
        					'key' 		=> $res -> key
    					];
    				}
    				$raw_input['section_id']	= $section_id;
    				$raw_input['survey_id']		= $survey_id;
    				$raw_input['category_id']	= 4;
    				$raw_input['user_id'] 		= $user_id;
    				$j++;
    				$baris_data++;

    				//pengecekan data terakhir
		    		if($baris_data == count($result_papikostick)){
		    			$raw_input['raw_input'] = json_encode($inputan);
		    			$j = 0;
    					$inputan = null;
    					DB::table('raw_input') -> insert($raw_input);
		    		}
    			}
	        }
	        DB::commit();
	        return 'Berhasil serialize inputan WBA';
    	}catch(exception $e){
            DB::rollback();
            return 'gagal';
        }
    }


    //=================================================== GABUNG PROJECT DANAMON ===================================================//

    public function gabung_project_ho_trial($id_project_lama){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        //id project barunya 14, id survey-survey nya 52,53,54,55
        $info_project = DB::table('project') -> where('id', '=', $id_project_lama) -> first();

        DB::beginTransaction();
	    try{
	        DB::table('user_project') -> where('id_project', '=', $id_project_lama) -> update(['id_project' => 14]);
	        DB::table('user_survey') -> where('id_project', '=', $id_project_lama) -> update(['id_project' => 14]);
	        DB::table('user_report') -> where('tujuan', '=', $id_project_lama) -> update(['tujuan' => 14]);
	        DB::table('user_report_aspeks') -> where('project_id', '=', $id_project_lama) -> delete();
	        DB::table('hasil_pesertas') -> where('tujuan', '=', $id_project_lama) -> delete();
	        DB::table('standar_nilai_projects') -> where('project_id', '=', $id_project_lama) -> delete();

	        $survey_project = DB::table('survey') -> where('id_project', '=', $id_project_lama) -> get();
	        foreach ($survey_project as $key) {
	        	if($key -> category_survey_id == 1){
	        		DB::table('user_survey') -> where('id_survey', '=', $key -> id) -> update(['id_survey' => 52]);
	        		DB::table('survey_report') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 52]);
	        		DB::table('report_gti_detail') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 52]);
	        	}elseif($key -> category_survey_id == 2){
	        		DB::table('user_survey') -> where('id_survey', '=', $key -> id) -> update(['id_survey' => 53]);
	        		DB::table('survey_report') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 53]);
	        	}elseif($key -> category_survey_id == 3){
	        		DB::table('user_survey') -> where('id_survey', '=', $key -> id) -> update(['id_survey' => 54]);
	        		DB::table('report_wpa') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 54]);
	        		DB::table('report_wpa_detail') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 54]);
	        	}elseif($key -> category_survey_id == 4){
	        		DB::table('user_survey') -> where('id_survey', '=', $key -> id) -> update(['id_survey' => 55]);
	        		DB::table('report_papikostick') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 55]);
	        	}
	        }
	    	DB::commit();
	        return '<center><h2>Berhasil gabung project '.$info_project->name.'<br/>ke project HO Trial</h2></center>';
    	}catch(exception $e){
            DB::rollback();
            return '<center><h2>Gagal gabung project '.$info_project->name.'<br/>ke project HO Trial</h2></center>';
        }
    }

    public function gabung_project_development_program($id_project_lama){
    	if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        //id project barunya 15, id survey-survey nya 56,57,58,59
        $info_project = DB::table('project') -> where('id', '=', $id_project_lama) -> first();

        DB::beginTransaction();
	    try{
	        DB::table('user_project') -> where('id_project', '=', $id_project_lama) -> update(['id_project' => 15]);
	        DB::table('user_survey') -> where('id_project', '=', $id_project_lama) -> update(['id_project' => 15]);
	        DB::table('user_report') -> where('tujuan', '=', $id_project_lama) -> update(['tujuan' => 15]);
	        DB::table('user_report_aspeks') -> where('project_id', '=', $id_project_lama) -> delete();
	        DB::table('hasil_pesertas') -> where('tujuan', '=', $id_project_lama) -> delete();
	        DB::table('standar_nilai_projects') -> where('project_id', '=', $id_project_lama) -> delete();

	        $survey_project = DB::table('survey') -> where('id_project', '=', $id_project_lama) -> get();
	        foreach ($survey_project as $key) {
	        	if($key -> category_survey_id == 1){
	        		DB::table('user_survey') -> where('id_survey', '=', $key -> id) -> update(['id_survey' => 56]);
	        		DB::table('survey_report') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 56]);
	        		DB::table('report_gti_detail') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 56]);
	        	}elseif($key -> category_survey_id == 2){
	        		DB::table('user_survey') -> where('id_survey', '=', $key -> id) -> update(['id_survey' => 57]);
	        		DB::table('survey_report') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 57]);
	        	}elseif($key -> category_survey_id == 3){
	        		DB::table('user_survey') -> where('id_survey', '=', $key -> id) -> update(['id_survey' => 58]);
	        		DB::table('report_wpa') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 58]);
	        		DB::table('report_wpa_detail') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 58]);
	        	}elseif($key -> category_survey_id == 4){
	        		DB::table('user_survey') -> where('id_survey', '=', $key -> id) -> update(['id_survey' => 59]);
	        		DB::table('report_papikostick') -> where('survey_id', '=', $key -> id) -> update(['survey_id' => 59]);
	        	}
	        }
	    	DB::commit();
	        return '<center><h2>Berhasil gabung project '.$info_project->name.'<br/>ke project HO Trial</h2></center>';
    	}catch(exception $e){
            DB::rollback();
            return '<center><h2>Gagal gabung project '.$info_project->name.'<br/>ke project HO Trial</h2></center>';
        }
    }
}
