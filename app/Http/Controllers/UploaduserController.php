<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Role;
use DB;
use Excel;
use Auth;
use Entrust;
use App\Http\Requests\ExcelRequestPost;

class UploaduserController extends Controller
{
    public function index()
    {

      if(Auth::check() && !Entrust::can('create-user'))
          return redirect('/home')->withErrors(trans('messages.permission_denied'));

      return view('user.upload');
    }
    public function downloadFormat(){

       return response()->download("files/format_user.xlsx");
    }
    public function downloadFormatAdd(){

       return response()->download("files/format_user_add.xlsx");
    }
    public function upload(ExcelRequestPost $request)
    {
      if(Auth::check() && !Entrust::can('create-user'))
          return redirect('/home')->withErrors(trans('messages.permission_denied'));

        $file_path = $_FILES['file']['tmp_name'];
        $list_data = Excel::load($file_path, function($reader){});
        $main = $list_data->get();

        foreach($main as $row)
        {
          if($row->email || $row->password || $row->name){
            if($row->email === NULL)
            {
                return redirect()->back()->withInput()->withErrors('Email is null');
            }
            if($row->password === NULL)
            {
                return redirect()->back()->withInput()->withErrors('Password is null');
            }if($row->name === NULL)
            {
                return redirect()->back()->withInput()->withErrors('Name is null');
            }
            $emails = strtolower($row->email);
            $emails = trim($emails, " ");
            if(filter_var($emails, FILTER_VALIDATE_EMAIL) == false)
            {
                return redirect()->back()->withInput()->withErrors('Wrong Email');
            }

            $raw = [
              "user"=>[
                "email"=>strtolower($row->email),
                "username"=>str_slug($row->name, '_'),
                "password"=>bcrypt(strtolower((string)$row->password)),
                "status"=>'active',
                "custom_token"=>encrypt_decrypt("encrypt",strtolower($row->email)),
              ],
              "profile"=>[
                "first_name"=>$row->name,
              ]
            ];

            $process = $this->addUser($raw);
          }else{
            return redirect()->back()->withInput()->withErrors('Pastikan konten sesuai dengan template yang disediakan');
          }
          
        }
        return redirect('/user')->withSuccess(trans('messages.user').' '.trans('messages.added'));
    }

    private function addUser($data)
    {

       if(sizeof($data) > 1)
       {
        $user = User::where("email",$data["user"]["email"])->first();
        if(is_null ($user))
        {
          DB::beginTransaction();
           try {

               $user = User::create($data["user"]);
               $data_profile = $data["profile"];
               $data_profile["user_id"] = $user->id;
               $data_profile["id_corporate"] =  Auth::user()->profile->id_corporate;
               $data_profile["id_division"] = Auth::user()->profile->id_division;
               $profile = Profile::create($data_profile);

               $role = Role::find(2);
 			        $user->attachRole($role);

               DB::commit();
               return true;
           // all good
           } catch (\Exception $e) {
               DB::rollback();
               // something went wrong
               return $e;
           }
        }

       }
    }

    
}
