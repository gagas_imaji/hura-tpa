<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Talenthub\Repository\JPMRepositoryInterface;
use App\MasterJobAnalysis;
use App\Jobs_karakteristik;
use Auth;
use Entrust;
use Validator;
use File;
use Image;
use Illuminate\Support\Facades\Input;
use App\Jobs;

class JPMController extends Controller
{
    public function __construct(JPMRepositoryInterface $jpm)
    {
        $this -> jpm = $jpm;
    }

    public function index()
    {

        if(!Entrust::can('manage-jpm')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $col_heads = array(
            trans('messages.option')
        );

        if(!config('config.login')){
            array_push($col_heads, trans('messages.username'));
        }

        array_push($col_heads, trans('messages.name'));
        array_push($col_heads, 'D');
        array_push($col_heads, 'I');
        array_push($col_heads, 'S');
        array_push($col_heads, 'C');

        $table_data['jobcharacteristic-table'] = array(
            'source' => 'job-characteristic',
            'title' => 'Job Characteristics List',
            'id' => 'jobcharacteristic_table',
            'data' => $col_heads
            );
        
        $assets = ['recaptcha'];
        
        return view('jpm.index',compact('table_data','assets'));
    }

    public function lists(Request $request){

    	if(!Entrust::can('manage-jpm')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $jobCharacteristic = $this -> jpm -> get_all_jobkar();
        $rows = array();

        foreach($jobCharacteristic as $key){
            $row = array(
                '<div class="btn-group btn-group-xs">'.
                '<a href="/job-characteristic/'.$key->id.'" class="btn btn-xs btn-default"> <i class="fa fa-arrow-circle-o-right" data-toggle="tooltip" title="'.trans('messages.view').'"></i></a>'.
                (Entrust::can('delete-job-characteristic') ? delete_form(['job-characteristic.destroy',$key->id]) : '').'</div>',
                );


            array_push($row, $key->jobs_name);
            array_push($row,$key->D);
            array_push($row,$key->I);
            array_push($row,$key->S);
            array_push($row,$key->C);

            $rows[] = $row;
        }
        $list['aaData'] = $rows;
        return json_encode($list);
    }

    public function show($id){

        if(!Entrust::can('manage-jpm')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $jobkar = $this -> jpm -> get_jpm_job($id);

        foreach ($jobkar as $key) {
            $data['D']  = $this -> jpm -> get_kuadran_job($key -> D);
            $data['I']  = $this -> jpm -> get_kuadran_job($key -> I);
            $data['S']  = $this -> jpm -> get_kuadran_job($key -> S);
            $data['C']  = $this -> jpm -> get_kuadran_job($key -> C);
            $data['job_id'] = $key -> jobs_id;
        }
        return view('jpm.show',compact('jobkar', 'data'));
    }

    public function create(){
        if(!Entrust::can('create-job-characteristic')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $master_analysis = $this -> jpm -> get_master_job_analysis();

        $i = 0;
        foreach ($master_analysis as $key) {
        	$data['questionnaire'][$i] = $key -> questionnaire;
        	$i++;
        }

        return view('jpm.create', compact('data'));
    }

    public function store(Request $request){

        
        if(!Entrust::can('create-job-characteristic')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $validator = Validator::make(Input::all(), [
            'nama'      => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect() -> back() -> withErrors($validator);
        }

        $input['nama'] = $request -> input('nama');
        $input['D'] = 0;
        $input['I'] = 0;
        $input['S'] = 0;
        $input['C'] = 0;
        $index = 0;

        $master_analysis = $this -> jpm -> get_master_job_analysis();


        foreach ($master_analysis as $key) {
        	if($key -> key == 'D'){
        		$input['D'] = $input['D'] + $request -> input('answer_'.$index);
        	}elseif($key -> key == 'I'){
        		$input['I'] = $input['I'] + $request -> input('answer_'.$index);
        	}elseif($key -> key == 'S'){
        		$input['S'] = $input['S'] + $request -> input('answer_'.$index);
        	}elseif($key -> key == 'C'){
        		$input['C'] = $input['C'] + $request -> input('answer_'.$index);
        	}
        	$index++;
        }


        $this -> jpm -> add_new_job_characteristic($input);
        
        return redirect('/job-characteristic')->withSuccess('Data berhasil disimpan'); 
    }

    public function destroy(Jobs $jobs, Request $request, $param=''){
    	Jobs_karakteristik::where('jobs_id', '=', $jobs['id'])->delete();
    	
        $delete_job = $jobs->delete();

        if($request->has('ajax_submit')){
            $response = ['message' => 'Pekerjaan Berhasil dihapus', 'status' => 'success']; 
            return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
        return redirect()->back()->withSuccess('Pekerjaan Berhasil dihapus');
    }
}
