<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class ErrorController extends Controller
{
    

    public function saveError(Request $request)
    {
    	$post = $request->all();
    	$data["name"] = $post["name"];
    	$data["email"] = $post["email"];
    	$msg = "Penyebab : ".$post["clause"]."<br> kejadian : ".$post["detail"]."<br> Error : ".$post["error-real"];
    	$data["error_msg"] = $msg;
        $data["created_at"] = Carbon::now('Asia/Jakarta');
    	DB::table('error_log')->insert($data);
    	return view('errors.thanks');
    }
}
