<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Talenthub\Repository\CorporateRepositoryInterface;
use App\Corporate;
use App\Division;
use Auth;
use Entrust;
use Validator;
use File;
use Image;
use DB;
use Illuminate\Support\Facades\Input;

class CorporateController extends Controller
{

    public function __construct(CorporateRepositoryInterface $corporate)
    {
        $this -> corporate = $corporate;
    }

    public function index()
    {

        if(!Entrust::can('manage-corporate')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $col_heads = array(
            trans('messages.option')
        );

        if(!config('config.login')){
            array_push($col_heads, trans('messages.username'));
        }

        array_push($col_heads, trans('messages.name'));
        array_push($col_heads, trans('messages.description'));
        array_push($col_heads, trans('messages.contact'));
        array_push($col_heads, trans('messages.email'));
        array_push($col_heads, 'Logo');

        $table_data['corporate-table'] = array(
            'source' => 'corporate',
            'title' => 'Corporate List',
            'id' => 'corporate_table',
            'data' => $col_heads
            );

        $assets = ['recaptcha'];

        return view('corporate.index',compact('table_data','assets'));
    }

    public function lists(Request $request){

        if(defaultRole()){
            $corporates = Corporate::all();
        }else{
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $rows = array();

        foreach($corporates as $corporate){
            $row = array(
                '<div class="btn-group btn-group-xs">'.
                '<a href="/corporate/'.$corporate->id.'" class="btn btn-xs btn-default"> <i class="fa fa-arrow-circle-o-right" data-toggle="tooltip" title="'.trans('messages.view').'"></i></a>'.
                (Entrust::can('delete-corporate') ? delete_form(['corporate.destroy',$corporate->id]) : '').
                '</div>',
                );

            if(!config('config.login')){
                array_push($row,$corporate->username);
            }

            array_push($row, $corporate->name);

            array_push($row,$corporate->description);
            array_push($row,$corporate->contact);
            array_push($row,$corporate->email);
            array_push($row,$corporate->image);

            $rows[] = $row;
        }
        $list['aaData'] = $rows;
        return json_encode($list);
    }

    public function show(Corporate $corporate){

        if(!Entrust::can('manage-corporate') || (!Entrust::hasRole(DEFAULT_ROLE) && $corporate->hasRole(DEFAULT_ROLE)) ){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $custom_social_field_values = getCustomFieldValues('corporate-social-form',$corporate->id);
        $custom_register_field_values = getCustomFieldValues('corporate-registration-form-form',$corporate->id);

        //data-table list division
        $division   = $this -> corporate -> get_list_division_by_corporate($corporate->id);
        $credit     = DB::table('credit_corporate')->where('id_corporate', '=', $corporate->id)->first()->credit;
        $col_heads  = [];
        array_push($col_heads, 'No');
        array_push($col_heads, 'Nama Divisi');
        $rowset = null;
        $i = 0;
        if($division){
            foreach ($division as $key) {
                $rowset[$i]['name'] = $key -> name;
                $i++;
            }
        }

        return view('corporate.show',compact('corporate','custom_social_field_values','custom_register_field_values', 'rowset', 'col_heads', 'credit'));
    }

    public function profileUpdate(Request $request, $id){
       /* if(!Entrust::can('update-user') || (!Entrust::hasRole(DEFAULT_ROLE) && $user->hasRole(DEFAULT_ROLE)) )
            return redirect('/home')->withErrors(trans('messages.permission_denied'));*/

        if(!Entrust::can('update-corporate')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $corporate = Corporate::find($id);

        if(!$corporate){
            return redirect('/corporate')->withErrors(trans('messages.invalid_link'));
        }

        $credit     = $request->input('credit');
        $corporate->fill($request->all());
        $corporate->save();
        DB::table('credit_corporate')->where('id_corporate', '=', $id)->update(['credit' => $credit]);

        if($request->has('ajax_submit')){
            $response = ['message' => trans('messages.profile').' '.trans('messages.updated'), 'status' => 'success'];
            return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
        return redirect()->back()->withSuccess(trans('messages.profile').' '.trans('messages.updated'));
    }

    public function avatar(Request $request, $id){

        /*if(!Entrust::can('update-corporate') || (!Entrust::hasRole(DEFAULT_ROLE) && $user->hasRole(DEFAULT_ROLE)) )
            return redirect('/home')->withErrors(trans('messages.permission_denied'));*/

        if(!Entrust::can('update-corporate'))
        	return redirect('/home')->withErrors(trans('messages.permission_denied'));

        $corporate = \App\Corporate::find($id);

        if(!$corporate)
            return redirect('/corporate')->withErrors("salah");


        $validation = Validator::make($request->all(),[
            'avatar' => 'image'
        ]);

        if($validation->fails()){
            if($request->has('ajax_submit')){
                $response = ['message' => $validation->messages()->first(), 'status' => 'error'];
                return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
            }

            return redirect()->back()->withErrors($validation->messages()->first());
        }

        $filename = uniqid();

        if ($request->hasFile('avatar') && $request->input('remove_avatar') != 1){
            if(File::exists(config('constant.upload_path.avatar').config('config.avatar')))
                File::delete(config('constant.upload_path.avatar').config('config.avatar'));
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $file = $request->file('avatar')->move(config('constant.upload_path.avatar'), $filename.".".$extension);
            $img = Image::make(config('constant.upload_path.avatar').$filename.".".$extension);
            $img->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(config('constant.upload_path.avatar').$filename.".".$extension);
            $corporate->image = $filename.".".$extension;
        } elseif($request->input('remove_avatar') == 1){
            if(File::exists(config('constant.upload_path.avatar').config('config.avatar')))
                File::delete(config('constant.upload_path.avatar').config('config.avatar'));
            $corporate->image = null;
        }

        $corporate->save();


        if($request->has('ajax_submit')){
            $response = ['message' => trans('messages.profile').' '.trans('messages.updated'), 'status' => 'success'];
            return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
        return redirect()->back()->withSuccess(trans('messages.profile').' '.trans('messages.updated'));
    }

    public function create(){
        if(!Entrust::can('create-corporate'))
            return redirect('/home')->withErrors(trans('messages.permission_denied'));

        $corporate = $this -> corporate -> getAll();
        return view('corporate.create', compact('corporate'));
    }

    public function store(Request $request, Corporate $corporate){


        if(Auth::check() && !Entrust::can('create-corporate'))
            return redirect('/home')->withErrors(trans('messages.permission_denied'));


        $validator = Validator::make(Input::all(), [
            'name'       => 'required',
            'contact'    => 'required'
        ]);

        if ($validator->fails()) {
            return redirect() -> back() -> withErrors($validator);
        }

        $corporate->name = $request->input('name');
        $corporate->description = $request->input('description');
        $corporate->contact = $request->input('contact');
        $corporate->email = $request->input('email');
        $corporate->image = '';
        $corporate->save();

        $data = $request->all();
        storeCustomField('corporate-registration-form',$corporate->id, $data);

        create_trial_user($corporate -> id, $corporate -> name); //sementara untuk bikin trial
        $set_credit = $this -> corporate -> set_credit($corporate -> id, $request -> input('credit'));

        if($set_credit == true){
            if($request->has('ajax_submit')){
                $response = ['message' => 'Corporate'.trans('messages.added'), 'status' => 'success'];
                return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
            }
            return redirect('/corporate')->withSuccess('Berhasil menambahkan corporate');
        }else{
            return redirect('/corporate')->withSuccess('Terjadi kesalahan');
        }
    }

    public function destroy(Corporate $corporate, Request $request){
        print_r($request->id);
        // DB::table('corporate')->where('id', '=', $request)->delete();

    }

    public function addDivision($id){
        return view('corporate.add_division', compact('id'));
    }

    public function addNewDivision(Request $request){
        $name   = $request -> input('name');
        $id     = $request -> input('id');

        $division = new Division;
        $division -> id_corporate   = $id;
        $division -> name           = $name;
        $division -> slug           = strtolower(str_replace(' ', '-', $name));
        $division -> save();

        return redirect('corporate/'.$id)->withSuccess('Division Added');
    }



    //====================================== SEMENTARA DUMMY TRIAL ======================================//

    public function createDummyReportLAI($corp_id){
        create_dummy_report_lai($corp_id);
        return redirect('/survey')->withSuccess('Dummy LAI Added');
    }

    public function createDummyReportTIKI($corp_id){
        create_dummy_report_tiki($corp_id);
        return redirect('/survey')->withSuccess('Dummy TIKI Added');
    }

    public function createDummyReportWBA($corp_id){
        create_dummy_report_wba($corp_id);
        return redirect('/survey')->withSuccess('Dummy WBA Added');
    }

}
