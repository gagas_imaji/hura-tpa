<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Talenthub\Repository\SurveyRepositoryInterface;

class TokenController extends Controller
{
  public function __construct(SurveyRepositoryInterface $survey)
  {
      $this -> survey         = $survey;
  }
    public function login(Request $request)
    {
      $all = $request->all();
      if(isset($all["token"]))
      {
          $token =$all["token"];
          $email = encrypt_decrypt("decrypt",$token);

          if($email == false)
          {
            return response()->json(["status"=>'invalid emails',"msg"=>"contact your admin for further information."]);
          }
          //explode
          $explode    = explode("+",$email);
          $userEmail  = $explode[0];
          $projectId  = $explode[1];
          $list_survey= $this -> survey -> get_list_survey_by_project($projectId);

          if(sizeof($list_survey) == 0 )
          {
            return response()->json(["status"=>'invalid token',"msg"=>"contact your admin for further information."]);
          }
          $kuota['survey']   = 0;
          $kuota['user']     = 1;
          $i =0;
          $data['id']         = $projectId;
          foreach ($list_survey as $key) {
              $data['survey'][$i] = $key['id'];
              $kuota['survey']++;
              $i++;
          }
          //validasi email
          $userEmail = trim($userEmail, " ");
          if (filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
              $is_registered  = $this -> survey -> is_registered($userEmail);

              if($is_registered == false){ //belum terdaftar di sistem. daftarkan dan assign
                  $user       = $this -> survey -> insert_new_user($userEmail,'2');
                  $assign     = $this -> survey -> assign_project($data, $user->id);
                  $kuota['terpakai'] = $kuota['user'];
                  $this -> survey -> set_kuota_terpakai($kuota);
                  $login = Auth::loginUsingId($user->id);
              }else{

                  $is_assigned    = $this -> survey -> is_assigned($data, $is_registered);
                  if($is_assigned == false){ //sudah terdaftar di sistem, tapi belum terdaftar di project, assign peserta ke project
                      $assign = $this -> survey -> assign_project($data, $is_registered);
                      $kuota['terpakai'] = $kuota['user'];
                      $this -> survey -> set_kuota_terpakai($kuota);
                  }
                  $login = Auth::loginUsingId($is_registered);
              }
          } else {
              return response()->json(["status"=>'invalid email:  '.$userEmail.'-',"msg"=>"Invalid email format, contact your admin for further information."]);
          }

          return redirect('/home');
      }else{
        return redirect('/');
      }
    }





}
