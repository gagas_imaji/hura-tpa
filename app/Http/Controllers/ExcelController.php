<?php

namespace App\Http\Controllers;

use Entrust;
use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
use Nexmo;
use Exception;
use App\Notifications\TwoFactorAuth;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\CorporateRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\Survey;
use App\User;
use App\UserReport;
use App\SurveyReport;

use App\SurveyCategory;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Library\report_survey;
use Excel;
class ExcelController extends Controller
{

  public function __construct(SurveyRepositoryInterface $survey, CorporateRepositoryInterface $corporate, ResultRepositoryInterface $result, QuestionRepositoryInterface $question, OptionAnswerRepositoryInterface $option_answer)
    {
        $this -> survey         = $survey;
        $this -> corporate      = $corporate;
        $this -> result         = $result;
        $this -> question       = $question;
        $this -> option_answer  = $option_answer;
    }

  public function excelGtiTiki(Request $request)
  {
    $post = $request->all();
    $slug = $post["slug"];
    $info_survey  = $this -> survey -> get_info_survey_by_slug($slug);
    $id_survey    = $info_survey['id'];
    $report       = $this -> result -> get_user_report($id_survey);

    $reportArray  = [];
    $reportArray[] = [
                  "Nama Peserta",
                  "Skor",
                  "Kriteria"
              ];
    if(sizeof($report) < 1)
     {
         return redirect() -> back()->with('status', 'Data tidak ditemukan');
     };

     foreach ($report as $row) {
       if($info_survey['category_survey_id'] == 1 )
       {
         $score =round($row->score/5, 1);
         $kriteria =  $this -> result -> get_criteria_gti_report($score);
       }else{
       	 	if($row->score <= 29){ //batas bawah
           		$score = 56;
       		}elseif($row->score > 107){ //batas atas
           		$score = 145;
       		}else{ //lookup
           		$score     = $this -> result -> get_iq_tiki($row->score);
       		}
         	$kriteria = $this->lookupTiki($score);
       }
       $reportArray[] = [
                  "Nama Peserta"=>$row->first_name,
                  "Skor"=> $score,
                  "Kriteria"=>$kriteria,
                ];
     }
     Excel::create($slug, function($excel) use ($reportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Daftar Peserta');
            $excel->setCreator('Talentlytica')->setCompany('Talentlytica');
            $excel->setDescription('Daftar pelamar');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($reportArray) {
                $sheet->fromArray($reportArray, null, 'A1', false, false);
            });

        })->download('xlsx');
  }

  public function excelWpaPapi(Request $request)
  {
      $post = $request->all();
      $slug = $post["slug"];
      $info_survey  = $this -> survey -> get_info_survey_by_slug($slug);
      $id_survey    = $info_survey['id'];
      if($info_survey['category_survey_id'] == 3){
          $table = 'report_wpa';
      }elseif($info_survey['category_survey_id'] == 4){
          $table = 'report_papikostick';
      }
      $report = $this -> result -> get_report_wpa_papi_detail($table, $id_survey);
      $reportArray = [];
      $reportArray[] = [
                    "Nama Peserta",
                    "Hasil",
                    "Tipe"
                ];
      if(sizeof($report) < 1)
       {
           returnredirect() -> back()->with('status', 'Data tidak ditemukan');
       };

       foreach ($report as $row) {
         if($info_survey['category_survey_id'] == 3 )
         {
           $data = $this -> result -> get_hasil_wpa($row -> user_id, $id_survey);
           $hasil = $data["hasil"];
           $tipe =  $data['type'];
         }
         $reportArray[] = [
                    "Nama Peserta"=>$row->first_name,
                    "Hasil"=> $hasil,
                    "Tipe"=>$tipe,
                  ];
       }
       Excel::create($slug, function($excel) use ($reportArray) {

              // Set the spreadsheet title, creator, and description
              $excel->setTitle('Daftar Peserta');
              $excel->setCreator('Talentlytica')->setCompany('Talentlytica');
              $excel->setDescription('Daftar pelamar');

              // Build the spreadsheet, passing in the payments array
              $excel->sheet('sheet1', function($sheet) use ($reportArray) {
                  $sheet->fromArray($reportArray, null, 'A1', false, false);
              });

          })->download('xlsx');
  }

  private function lookupTiki($score)
  {
      if($score <= 80){
        $kriteria = "Di bawah Rata-Rata";
      }elseif($score <= 90){
        $kriteria = "Rata-Rata Rendah";
      }elseif($score <= 100){
        $kriteria = "Rata-Rata";
      }elseif($score <= 110){
        $kriteria = "Rata-Rata Atas";
      }elseif($score <= 125){
        $kriteria = "Di Atas Rata-Rata";
      }elseif($score >= 126){
        $kriteria = "Jauh Di Atas Rata-Rata";
      }

      return $kriteria;
  }
}
