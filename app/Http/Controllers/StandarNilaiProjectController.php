<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StandarNilai;
use App\StandarNilaiProject;
use App\AspekPsikologis;
use App\DefaultStandarNilaiProject;
use Auth;
use DB;
use Entrust;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
use Validator;
use App\Library\Kalkulasi;

use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Talenthub\Repository\StandarNilaiRepositoryInterface;

class StandarNilaiProjectController extends Controller
{
	public function __construct(SurveyRepositoryInterface $survey, QuestionRepositoryInterface $question, OptionAnswerRepositoryInterface $option_answer, ResultRepositoryInterface $result, StandarNilaiRepositoryInterface $standar_nilai)
    {
        $this->survey        = $survey;
        $this->question      = $question;
        $this->option_answer = $option_answer;
        $this->result        = $result;
        $this->standar_nilai = $standar_nilai;
    }

    public function index()
    {
        $aspeks = AspekPsikologis::with('tpaKategoriAspek')->get();
        $datas  = StandarNilai::with('tpaStandarNilaiProject')->get();
        return view('standar_nilai.index',compact('aspeks','datas'));
    }

    public function set_standar_nilai($id)
    {
    	if(Entrust::can('create-survey')){
    		$data['info_project'] = $this->survey->get_info_project_by_id($id);
    		if ($data['info_project']==null) {
    			return redirect()->back()->withErrors('Project Tidak Ditemukan');
    		}
    		$listAspeks	             = AspekPsikologis::get();
            $listStandarNilaiProject = StandarNilai::get();
            return view('survey.set_standar_nilai', compact('data','listAspeks','listStandarNilaiProject'));
        }else{
            return redirect('/home');
        }
    }

    public function edit_standar_nilai($id)
    {
        if(Entrust::can('create-survey')){
            $data['info_project'] = $this->survey->get_info_project_by_id($id);
            if ($data['info_project']==null) {
                return redirect()->back()->withErrors('Project Tidak Ditemukan');
            }
            $listAspeks              = AspekPsikologis::get();
            $listStandarNilai        = $this->standar_nilai->get_list_standar_nilai_by_project($id);
            $listStandarNilaiProject = StandarNilai::get();
            $listNilaiAspek          = $listStandarNilai->tpaStandarNilai->nilai_aspek;
            $current_jabatan         = $listStandarNilai->standar_nilai_id;
            $project_id              = $id;
            return view('survey.edit_standar_nilai', compact('data','listAspeks','listStandarNilai','listStandarNilaiProject','current_jabatan','project_id'));
        }else{
            return redirect('/home');
        }
    }

    public function save_standar_nilai(Request $request)
    {
    	if(!Entrust::can('create-survey')){
            return redirect('/home');
        }
    	$param = $request->all();
    	$validator = Validator::make($param, [
            'posisi' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        if ($param['posisi']=='default') {
            return redirect()->back()->withErrors('Failed to save. Please select the position first');
        }
        $project_id   = $param['project_id'];
        $save_standar = $this->standar_nilai->save_temp($param);

        if($save_standar == false){
            return redirect()->back()->withErrors('Failed to save');
        } else {
            $kalkulasi = Kalkulasi::updateNilaiKecocokanByStandarNilaiUpdated($project_id);
            return redirect('project')->withSuccess('Save Standar Nilai Succeed');
        }
        
    }

    public function update_standar_nilai(Request $request)
    {
    	if(!Entrust::can('create-survey')){
            return redirect('/home');
        }
    	$param     = $request->all();
    	$validator = Validator::make($param, [
            'posisi' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        if ($param['posisi']=='default') {
            return redirect()->back()->withErrors('Failed to save. Please select the position first');
        }
        $project_id   = $param['project_id'];
        $save_standar = $this->standar_nilai->update_save_temp($param);

        if($save_standar == false){
            return redirect()->back()->withErrors('Failed to save');
        } else {
            $kalkulasi = Kalkulasi::updateNilaiKecocokanByStandarNilaiUpdated($project_id);
            return redirect()->back()->withSuccess('Save Standar Nilai Succeed');
        }
        
    }

    public function archive_standar_nilai($project_id)
    {
        $standar = StandarNilai::where('project_id',$project_id)->get();
        $check   = DefaultStandarNilaiProject::where('project_id',$project_id)->get();

        if (!is_null($standar) && sizeof($check) == 0)
        {
            try {
                DB::beginTransaction();
                foreach ($standar as $key => $value) {
                    $standarNilai = DefaultStandarNilaiProject::updateOrCreate(
                        ['project_id'=>$value->project_id,'aspek_id'=>$value->aspek_id],
                        ['project_id'=>$value->project_id,'nama_jabatan'=>$value->nama_jabatan,'aspek_id'=>$value->aspek_id,'nilai_aspek'=>$value->nilai_aspek,'mandatory'=>$value->mandatory]
                    );
                }
                DB::commit();
            } catch (exception $e) {
                DB::rollBack();
                return redirect()->back()->withErrors('Standar Nilai tidak terarsip');
            }
        }
    }

    public function revert_standar_nilai()
    {
        DB::beginTransaction();
        try {
            $all_defaults = DefaultStandarNilaiProject::all();
            foreach ($all_defaults as $def) {
                $standar_nilai = StandarNilai::updateOrCreate(
                    ['project_id'=>$def->project_id, 'aspek_id'=>$def->aspek_id],
                    ['nilai_aspek'=>$def->nilai_aspek,'mandatory'=>$def->mandatory]
                );
            }
            //untuk kalkulasi ulang
            /*$getPromosiId = HasilPeserta::select("promosi_id","user_id")->get()->toArray();
            foreach ($getPromosiId as $value) {
                $promosiId = $value["promosi_id"];
                $userId    = $value["user_id"];
                $getNilaiStandarByJabatan = StandarNilaiJabatan::where('jabatan_id','=',$promosiId)->pluck("nilai_kompetensi","aspek_id")->toArray();
                $getNilaiPeserta = UserReport::where('user_id','=',$userId)->where('promosi_id','=',$promosiId)->pluck("nilai_aspek","aspek_id")->toArray();
                $gaps      = [];
                $skor_gaps = [];
                foreach ($getNilaiStandarByJabatan as $keyStd => $standarNilai) {
                    foreach ($getNilaiPeserta as  $keyPst => $nilaiPeserta) {
                        if ($keyStd == $keyPst) {
                            if ($standarNilai == 0) {
                                array_push($gaps, "");
                            } else {
                                if (($standarNilai-$nilaiPeserta)<0) {
                                    array_push($gaps, 0);
                                } else {
                                    $gapNilai = $standarNilai-$nilaiPeserta;
                                    array_push($gaps, $gapNilai);
                                }
                            }
                        }
                    }
                }
                //Hitung gap skor
                $i = 0;
                foreach ($getNilaiStandarByJabatan as $keyStandar => $standar) {
                    foreach ($gaps as $keyGaps => $value) {
                        if (($keyStandar-1) == $keyGaps) {
                            if (strlen($value) == 0) {
                                array_push($skor_gaps, "");
                                $i++;
                            } elseif ($value == 0) {
                                array_push($skor_gaps, 1);
                            } else {
                                $skor = 1-($value/$standar);
                                array_push($skor_gaps, $skor);
                            }
                        }
                    }
                }
                //Check jika count gaps = 0 artinya standar nilai jabatan 0 semua
                $counts = count($gaps)-$i;
                if ($counts != 0) {
                    $sumSkorGaps   = array_sum($skor_gaps)*100;
                    $totalNilaiJPM = round($sumSkorGaps/$counts,2);
                    if (($totalNilaiJPM) >= 0.00 && ($totalNilaiJPM) <= 70.00) {
                        $hasilKriteria = 1;
                    } elseif (($totalNilaiJPM) >= 70.10 && ($totalNilaiJPM) <= 90.00) {
                        $hasilKriteria = 2;
                    } elseif (($totalNilaiJPM) >= 90.10 && ($totalNilaiJPM) <= 100.00) {
                        $hasilKriteria = 3;
                    }
                    //Update data Hasil peserta
                    $hasilPeserta = HasilPeserta::where(["user_id"=>$userId,"promosi_id"=>$promosiId])->first();
                    $hasilPeserta->kecocokan      = $totalNilaiJPM;
                    $hasilPeserta->hasil_kriteria = $hasilKriteria;
                    $hasilPeserta->update();
                } else {
                    continue;
                }
            }*/
            DB::commit();
            return redirect('standar_nilai')->withSuccess('Standar Nilai Telah Dikembalikan ke Nilai Semula');
        } catch (exception $e) {
            DB::rollBack();
            return redirect('standar_nilai')->withErrors('Standar Nilai Tidak Berhasil Dikembalikan ke Nilai Semula');
        }
    }
}
