<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
use Nexmo;
use Exception;
use App\Notifications\TwoFactorAuth;


use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Survey;
use App\User;
use App\SurveyCategory;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Library\report_survey;
use App\Jobs_karakteristik;
use App\Jobs;
use Entrust;

class ReportController extends Controller
{
    public function __construct(SurveyRepositoryInterface $survey, ResultRepositoryInterface $result)
    {
        $this -> survey         = $survey;
        $this -> result         = $result;
    }

    public function get_report($category, $slug, $id_user){
        if($category == 1){
            return redirect('/backend/report/gti/'.$slug.'/'.$id_user);
        }elseif($category == 2){
            return redirect('/backend/report/tiki/'.$slug.'/'.$id_user);
        }elseif($category == 3){
            return redirect('/backend/report/wpa/'.$slug.'/'.$id_user);
        }elseif($category == 4){
            return redirect('/backend/report/kostick/'.$slug.'/'.$id_user);
        }
    }

    public function gti_graph($slug, $id_user)
    {
        $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey          = $info_survey['id'];
        $report             = $this -> result -> get_report_gti_tiki($id_survey, $id_user);
        $avg                = round(array_sum($report)/count($report), 1);
        $report['date']     = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
        $report['start_date']   = $info_survey['start_date'];
        $report['end_date']     = $info_survey['end_date'];

        //proses pencarian job references berdasarkan model
        $model['1']=round(($report['subtest1'] + $report['subtest2'] + $report['subtest4'])/3,2);
        $model['2']=round(($report['subtest3'] + $report['subtest4'] + $report['subtest5'])/3,2);
        $model['3']=round(($report['subtest2'] + $report['subtest4'] + $report['subtest5'])/3,2);
        $model['4']=round(($report['subtest1'] + $report['subtest2'] + $report['subtest3'] + $report['subtest4'] + $report['subtest5'])/5,2);
        $match = array_search(max($model), $model);
        $report['slug']     = $slug;
        $report['title']    = $info_survey['title'];
        $report['avg']      = $avg;
        $report['jobs']     = $this -> result -> get_gti_job_preferences($match);
        $report['criteria'] = $this -> result -> get_criteria_gti_report($report['avg']);
        $report['id_user']  = $id_user;

        //dari sini
        $kekuatan_kelemahan = DB::table('report_gti_detail')->where('user_id', '=', $id_user)->where('survey_id', '=', $id_survey)->first();
        if($kekuatan_kelemahan){
            $kelemahan = json_decode($kekuatan_kelemahan->kelemahan);
            $kekuatan  = json_decode($kekuatan_kelemahan->kekuatan);
        }else{
            for($i = 1; $i <= 5; $i++){
                if($report['subtest'.$i] < 92){
                    $kelemahan[$i]   = $this -> result -> get_kekuatan_kelemahan_gti('GTQ'.$i);
                    $kekuatan[$i]    = '';
                }elseif($report['subtest'.$i] > 105.9){
                    $kekuatan[$i]    = $this -> result -> get_kekuatan_kelemahan_gti('GTQ'.$i);
                    $kelemahan       = '';
                }else{
                    $kekuatan[$i]    = '';
                    $kelemahan[$i]   = '';
                }
            }
            $insert['kekuatan'] = json_encode($kekuatan);
            $insert['kelemahan'] = json_encode($kelemahan);
            $insert['user_id']  = $id_user;
            $insert['survey_id'] = $id_survey;
            DB::table('report_gti_detail')->insert($insert);
        }

    	return view('graph.detail-gti',compact('report', 'kekuatan', 'kelemahan'));
    }

    public function tiki_graph($slug, $id_user)
    {
        $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey          = $info_survey['id'];
        $raw_score          = $this -> result -> get_report_gti_tiki($id_survey, $id_user);
        $data['date']       = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
        $data['start_date'] = $info_survey['start_date'];
        $data['end_date']   = $info_survey['end_date'];

        $total  = 0;
        $i      = 0;
        foreach ($raw_score as $key => $value) {
            if($key != 'subtest5'){
                // $standar_score      = $this -> result -> get_standard_score_tiki($key, $value);
                // foreach ($standar_score as $std) {
                    $data['nilai_standar'][$i] = $value;
                    $total = $total + $value;
                    $i++;
                // }
            }
        }
        $data['total_tiki'] = $total;
        $data['title']      = $info_survey['title'];
        $data['slug']       = $slug;
        $data['id_user']    = $id_user;
        if($total <= 29){ //batas bawah
            $data['iq'] = 56;
        }elseif($total > 107){ //batas atas
            $data['iq'] = 145;
        }else{ //lookup
            $data['iq']     = $this -> result -> get_iq_tiki($total);
        }

        if($data['iq'] <= 80){
            $data['golongan_iq'] = "Di bawah Rata-Rata";
        }elseif($data['iq'] <= 90){
            $data['golongan_iq'] = "Rata-Rata Rendah";
        }elseif($data['iq'] <= 100){
            $data['golongan_iq'] = "Rata-Rata";
        }elseif($data['iq'] <= 110){
            $data['golongan_iq'] = "Rata-Rata Atas";
        }elseif($data['iq'] <= 125){
            $data['golongan_iq'] = "Di Atas Rata-Rata";
        }elseif($data['iq'] >= 126){
            $data['golongan_iq'] = "Jauh Di Atas Rata-Rata";
        }

        $data['f1'] = round(($data['nilai_standar'][1] + $data['nilai_standar'][3]) / 2, 2);
        $data['f3'] = round(($data['nilai_standar'][0] + $data['nilai_standar'][2]) / 2, 2);
        $data['f2'] = round(($data['nilai_standar'][0] + $data['nilai_standar'][2] + $data['nilai_standar'][3]) / 3, 2);

        if($data['f1'] <= 7.49){
            $data['golongan_f1'] = "Kurang Sekali";
        }elseif($data['f1'] <= 12.49){
            $data['golongan_f1'] = "Kurang";
        }elseif($data['f1'] <= 14.49){
            $data['golongan_f1'] = "Sedang Bawah";
        }elseif($data['f1'] <= 17.49){
            $data['golongan_f1'] = "Sedang Atas";
        }elseif($data['f1'] <= 22.49){
            $data['golongan_f1'] = "Cukup Baik";
        }elseif($data['f1'] >= 22.5){
            $data['golongan_f1'] = "Baik";
        }

        if($data['f2'] <= 7.49){
            $data['golongan_f2'] = "Kurang Sekali";
        }elseif($data['f2'] <= 12.49){
            $data['golongan_f2'] = "Kurang";
        }elseif($data['f2'] <= 14.49){
            $data['golongan_f2'] = "Sedang Bawah";
        }elseif($data['f2'] <= 17.49){
            $data['golongan_f2'] = "Sedang Atas";
        }elseif($data['f2'] <= 22.49){
            $data['golongan_f2'] = "Cukup Baik";
        }elseif($data['f2'] >= 22.5){
            $data['golongan_f2'] = "Baik";
        }

        if($data['f3'] <= 7.49){
            $data['golongan_f3'] = "Kurang Sekali";
        }elseif($data['f3'] <= 12.49){
            $data['golongan_f3'] = "Kurang";
        }elseif($data['f3'] <= 14.49){
            $data['golongan_f3'] = "Sedang Bawah";
        }elseif($data['f3'] <= 17.49){
            $data['golongan_f3'] = "Sedang Atas";
        }elseif($data['f3'] <= 22.49){
            $data['golongan_f3'] = "Cukup Baik";
        }elseif($data['f3'] >= 22.5){
            $data['golongan_f3'] = "Baik";
        }

    	return view('graph.detail-tiki',compact('data'));
    }

    public function kostick_graph($slug, $id_user)
    {
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey      = $info_survey['id'] ;
        $report         = $this -> result -> get_report_papi($id_survey, $id_user);
        $data['slug']   = $slug;
        $data['title']  = $info_survey['title'];
        $data['id_user']= $id_user;
        $data['date']       = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
        $data['start_date'] = $info_survey['start_date'];
        $data['end_date']   = $info_survey['end_date'];
        foreach ($report as $key) {
            $data['N'] = $key -> N;
            $data['G'] = $key -> G;
            $data['A'] = $key -> A;
            $data['L'] = $key -> L;
            $data['P'] = $key -> P;
            $data['I'] = $key -> I;
            $data['T'] = $key -> T;
            $data['V'] = $key -> V;
            $data['X'] = $key -> X;
            $data['S'] = $key -> S;
            $data['B'] = $key -> B;
            $data['O'] = $key -> O;
            $data['R'] = $key -> R;
            $data['D'] = $key -> D;
            $data['C'] = $key -> C;
            $data['Z'] = $key -> Z;
            $data['E'] = $key -> E;
            $data['K'] = $key -> K;
            $data['F'] = $key -> F;
            $data['W'] = $key -> W;
        }
        $result = $this -> result -> get_hasil_interpretasi_papi($data);
        $data['result'] = $result;
    	return view('graph.detail-kostick',compact('data'));
    }

    public function wpa_graph($slug, $id_user)
    {
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey      = $info_survey['id'] ;
        $data['slug']   = $slug;
        $data['title']  = $info_survey['title'];
        $data['id_user']= $id_user;
        $data['date']       = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
        $data['start_date'] = $info_survey['start_date'];
        $data['end_date']   = $info_survey['end_date'];

        $report_wpa   = $this -> result -> get_score_wpa($id_survey, $id_user);
        foreach ($report_wpa as $key) {
            $most   = json_decode($key['most']);
            $lest   = json_decode($key['lest']);
            $change = json_decode($key['change']);
        }

        //most graph
        $most_d = $most->d;
        $most_i = $most->i;
        $most_s = $most->s;
        $most_c = $most->c;
        $data['most_d']  = WpaMostD($most_d);
        $data['most_i']  = WpaMostI($most_i);
        $data['most_s']  = WpaMostS($most_s);
        $data['most_c']  = WpaMostC($most_c);
        //lest graph
        $lest_d = $lest->d;
        $lest_i = $lest->i;
        $lest_s = $lest->s;
        $lest_c = $lest->c;
        $data['lest_d']  = WpaLeastD($lest_d);
        $data['lest_i']  = WpaLeastI($lest_i);
        $data['lest_s']  = WpaLeastS($lest_s);
        $data['lest_c']  = WpaLeastC($lest_c);
        //change graph
        $change_d = $change->d;
        $change_i = $change->i;
        $change_s = $change->s;
        $change_c = $change->c;
        $data['change_d']  = WpaChangeD($change_d);
        $data['change_i']  = WpaChangeI($change_i);
        $data['change_s']  = WpaChangeS($change_s);
        $data['change_c']  = WpaChangeC($change_c);

        $result         = $this -> result -> get_hasil_wpa($id_user, $id_survey);
        $rekap_detail   = $this -> result -> get_rekap_wpa_detail($id_user, $id_survey);

        if($result['hasil'] != "Under Shift" && $result['hasil'] != "Upper Shift"){
        	$k_umum = json_decode($rekap_detail->karakteristik_umum);
            $perilaku_kekuatan = json_decode($rekap_detail->perilaku_kerja_kekuatan);
            $perilaku_kelemahan = json_decode($rekap_detail->perilaku_kerja_kelemahan);
            $emosi_kekuatan = json_decode($rekap_detail->suasana_emosi_kekuatan);
            $emosi_kelemahan = json_decode($rekap_detail->suasana_emosi_kelemahan);
            $k_pekerjaan = json_decode($rekap_detail->karakteristik_pekerjaan);
            $kuat = json_decode($rekap_detail->kekuatan);
            $lemah = json_decode($rekap_detail->kelemahan);
            $uraian_kepribadian = $rekap_detail->uraian_kepribadian;

            $data['karakteristik_umum']         = $k_umum;
            $data['perilaku_kerja_kekuatan']    = $perilaku_kekuatan;
            $data['perilaku_kerja_kelemahan']   = $perilaku_kelemahan;
            $data['suasana_emosi_kekuatan']     = $emosi_kekuatan;
            $data['suasana_emosi_kelemahan']    = $emosi_kelemahan;
            $data['karakteristik_pekerjaan']    = $k_pekerjaan;
            $data['kekuatan']                   = $kuat;
            $data['kelemahan']                  = $lemah;
            $data['uraian_kepribadian']         = $uraian_kepribadian;
            $data['type_kepribadian']           = $result['type'];
        }else{
            $data['karakteristik_umum']         = '-';
            $data['perilaku_kerja_kekuatan']    = '-';
            $data['perilaku_kerja_kelemahan']   = '-';
            $data['suasana_emosi_kekuatan']     = '-';
            $data['suasana_emosi_kelemahan']    = '-';
            $data['karakteristik_pekerjaan']    = '-';
            $data['kekuatan']                   = '-';
            $data['kelemahan']                  = '-';
            $data['uraian_kepribadian']         = '-';
            $data['type_kepribadian']           = "-";
        }
        $data['result']                     = $result['hasil'];
        // dd($data);
    	return view('graph.detail-wpa',compact('data'));
    }

    public function get_jpm($slug, $id_user, Request $request)
    {
        $data['request']= $request -> input('profile');
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey      = $info_survey['id'] ;
        $data['slug']   = $slug;
        $data['title']  = $info_survey['title'];
        $data['id_user']= $id_user;
        $data['date']       = $this -> result -> get_tanggal_pengerjaan($id_survey, $id_user);
        $data['start_date'] = $info_survey['start_date'];
        $data['end_date']   = $info_survey['end_date'];
        $gender         = $this -> result -> get_gender_user($id_user);

        //most graph
        $most   = $this -> result -> get_score_wpa($id_survey, 'most', $id_user);
        foreach ($most as $key) {
            $most_d = $key -> D;
            $most_i = $key -> I;
            $most_s = $key -> S;
            $most_c = $key -> C;
        }
        $data['most_d']  = WpaMostD($most_d);
        $data['most_i']  = WpaMostI($most_i);
        $data['most_s']  = WpaMostS($most_s);
        $data['most_c']  = WpaMostC($most_c);

        //lest graph
        $lest   = $this -> result -> get_score_wpa($id_survey, 'lest', $id_user);
        foreach ($lest as $key) {
            $lest_d = $key -> D;
            $lest_i = $key -> I;
            $lest_s = $key -> S;
            $lest_c = $key -> C;
        }
        $data['lest_d']  = WpaLeastD($lest_d);
        $data['lest_i']  = WpaLeastI($lest_i);
        $data['lest_s']  = WpaLeastS($lest_s);
        $data['lest_c']  = WpaLeastC($lest_c);

        //change graph
        $change   = $this -> result -> get_score_wpa($id_survey, 'change', $id_user);
        foreach ($change as $key) {
            $change_d = $key -> D;
            $change_i = $key -> I;
            $change_s = $key -> S;
            $change_c = $key -> C;
        }
        $data['change_d']  = WpaChangeD($change_d);
        $data['change_i']  = WpaChangeI($change_i);
        $data['change_s']  = WpaChangeS($change_s);
        $data['change_c']  = WpaChangeC($change_c);
        $result                    = $this -> result -> get_hasil_wpa($id_user, $id_survey);
        $data['type_kepribadian']  = $result['type'];
        $data['result']            = $result['hasil'];
        $data['job_profile']       = $this -> result -> get_job_profile();

        if($data['request'] == null){
            $data['request'] = 1;
        }

        /* perhitungan persentase JPM */

        //ambil nilai kuadran user
        $grafik1['D']   = $this -> result -> convert_total_to_kuadran('D', 1, $most_d);
        $grafik1['I']   = $this -> result -> convert_total_to_kuadran('I', 1, $most_i);
        $grafik1['S']   = $this -> result -> convert_total_to_kuadran('S', 1, $most_s);
        $grafik1['C']   = $this -> result -> convert_total_to_kuadran('C', 1, $most_c);

        $grafik2['D']   = $this -> result -> convert_total_to_kuadran('D', 2, $lest_d);
        $grafik2['I']   = $this -> result -> convert_total_to_kuadran('I', 2, $lest_i);
        $grafik2['S']   = $this -> result -> convert_total_to_kuadran('S', 2, $lest_s);
        $grafik2['C']   = $this -> result -> convert_total_to_kuadran('C', 2, $lest_c);

        $grafik3['D']   = $this -> result -> convert_total_to_kuadran('D', 3, $change_d);
        $grafik3['I']   = $this -> result -> convert_total_to_kuadran('I', 3, $change_i);
        $grafik3['S']   = $this -> result -> convert_total_to_kuadran('S', 3, $change_s);
        $grafik3['C']   = $this -> result -> convert_total_to_kuadran('C', 3, $change_c);

        //ambil nilai kuadran job
        $kuadran_job = $this -> result -> get_jpm_job($data['request']);

        //cek jika parameter request job_id tidak ada di database, maka pilih job_id = 1
        if($kuadran_job -> count() == 0){
            $kuadran_job = $this -> result -> get_jpm_job(1);
        }

        foreach ($kuadran_job as $key) {
            $jpm_d  = $this -> result -> get_kuadran_job($key -> D);
            $jpm_i  = $this -> result -> get_kuadran_job($key -> I);
            $jpm_s  = $this -> result -> get_kuadran_job($key -> S);
            $jpm_c  = $this -> result -> get_kuadran_job($key -> C);
        }

        $data['jpm_d']  = ConvertJPMIndex($jpm_d);
        $data['jpm_i']  = ConvertJPMIndex($jpm_i);
        $data['jpm_s']  = ConvertJPMIndex($jpm_s);
        $data['jpm_c']  = ConvertJPMIndex($jpm_c);


        $compatibility1['D']    = $this -> result -> get_compatibility($grafik1['D'], $jpm_d);
        $compatibility1['I']    = $this -> result -> get_compatibility($grafik1['I'], $jpm_i);
        $compatibility1['S']    = $this -> result -> get_compatibility($grafik1['S'], $jpm_s);
        $compatibility1['C']    = $this -> result -> get_compatibility($grafik1['C'], $jpm_c);

        $compatibility2['D']    = $this -> result -> get_compatibility($grafik2['D'], $jpm_d);
        $compatibility2['I']    = $this -> result -> get_compatibility($grafik2['I'], $jpm_i);
        $compatibility2['S']    = $this -> result -> get_compatibility($grafik2['S'], $jpm_s);
        $compatibility2['C']    = $this -> result -> get_compatibility($grafik2['C'], $jpm_c);

        $compatibility3['D']    = $this -> result -> get_compatibility($grafik3['D'], $jpm_d);
        $compatibility3['I']    = $this -> result -> get_compatibility($grafik3['I'], $jpm_i);
        $compatibility3['S']    = $this -> result -> get_compatibility($grafik3['S'], $jpm_s);
        $compatibility3['C']    = $this -> result -> get_compatibility($grafik3['C'], $jpm_c);

        $avg1 = array_sum($compatibility1)/4;
        $avg2 = array_sum($compatibility2)/4;
        $avg3 = array_sum($compatibility3)/4;

        $agreement['1']   = $this -> result -> get_aggreement($avg1, $gender);
        $agreement['2']   = $this -> result -> get_aggreement($avg2, $gender);
        $agreement['3']   = $this -> result -> get_aggreement($avg3, $gender);

        $data['agreement_1']    = $agreement['1'];
        $data['agreement_2']    = $agreement['2'];
        $data['agreement_3']    = $agreement['3'];
        $data['result_jpm']    = array_sum($agreement)/3;
        return view('graph.jpm',compact('data'));
    }

    //============================================= Tambahan Anggoro =============================================//
    public function index_report_user()
    {
        return view("index-report-user");
    }

    public function report_user($user_id)
    {
        return view("report-user");
    }

}
