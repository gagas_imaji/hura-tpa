<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Entrust;
use Auth;
use App\Http\Requests;

use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Talenthub\Repository\CorporateRepositoryInterface;
use App\Survey;
use App\SurveyCategory;
use App\SurveyQuarter;
use App\Question;
use App\Result;
use App\Profile;
use App\User;
use App\Role;
use App\Division;
use App\OptionSection;
use DB;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
use Validator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;
use App\Mail\Reschedule;
use App\Mail\NotificationPilihan;
use App\Mail\CompleteTools;
use App\Http\Requests\ExcelRequestPost;
use App\Library\Kalkulasi;
use App\StandarNilaiProject;
use App\StandarNilai;
use App\HasilPeserta;
use App\Project;
use PDF;
use Exception;
use App\UserReportAspek;
use App\UserProject;

class SurveyController extends Controller
{
    public function __construct(SurveyRepositoryInterface $survey, QuestionRepositoryInterface $question, OptionAnswerRepositoryInterface $option_answer, ResultRepositoryInterface $result, CorporateRepositoryInterface $corporate)
    {
        $this -> survey         = $survey;
        $this -> question       = $question;
        $this -> option_answer  = $option_answer;
        $this -> result         = $result;
        $this -> corporate      = $corporate;
    }

    //======================================================= ADMIN =======================================================//

    public function index(){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        return redirect('/project');
        $col_heads = array(
            trans('messages.option')
        );

        array_push($col_heads, trans('messages.title'));
        array_push($col_heads, 'Status');
        array_push($col_heads, 'Created Date');
        array_push($col_heads, 'Start Date');
        array_push($col_heads, 'End Date');


        $table_data['survey-table'] = array(
            'source' => 'survey',
            'title' => 'Survey List',
            'id' => 'survey_table',
            'data' => $col_heads
            );

        $assets = ['recaptcha'];

        return view('survey.index',compact('table_data','assets'));
    }

    public function lists(Request $request){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $survey = $this -> survey -> get_list_survey_data_table();
        $rows = array();
        foreach($survey as $survey){
            $row = array(
                '<div class="btn-group btn-group-xs">
                    <a href="/survey/'.$survey->id.'" class="btn btn-xs btn-default" id="view-tools-'.$survey->id.'">
                        <i class="fa fa-arrow-circle-o-right" data-toggle="tooltip" title="'.trans('messages.view').'"></i>
                    </a>
                </div>',
                );

            array_push($row,$survey->title);
            if($survey->status == 1){
                $status = '<span class="label label-info">Finished</span>';
            }else{
                $status = '<span class="label label-success">Ongoing</span>';
            }
            array_push($row,$status);
            array_push($row,date('Y-m-d H:i:s', strtotime($survey->created_at)));
            array_push($row,$survey->start_date);
            array_push($row,$survey->end_date);

            $rows[] = $row;
        }
        $list['aaData'] = $rows;
        return json_encode($list);
    }

    public function show(Survey $survey){

        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $project = DB::table('project')->where('id', '=', $survey->id_project)->first();
        $custom_social_field_values = getCustomFieldValues('survey-social-form',$survey->id);
        $custom_register_field_values = getCustomFieldValues('survey-registration-form-form',$survey->id);

        //data-table list question
        $col_heads = array(
            trans('messages.option')
        );

        if(!config('config.login')){
            array_push($col_heads, trans('messages.username'));
        }

        array_push($col_heads, 'Type');
        array_push($col_heads, 'Section');
        array_push($col_heads, 'Question');
        array_push($col_heads, 'Random Option');
        array_push($col_heads, 'Timer');
        array_push($col_heads, 'Mandatory');
        array_push($col_heads, 'Point');

        $table_data['question-table'] = array(
            'source' => 'question/'.$survey->id,
            'title' => 'Question List',
            'id' => 'question_table',
            'data' => $col_heads
            );

        $assets = ['recaptcha'];

        $section = $this -> survey -> get_all_section_on_survey($survey -> id);
        return view('survey.show',compact('survey','custom_social_field_values','custom_register_field_values', 'table_data', 'section', 'project'));
    }

    public function destroy(Survey $survey, Request $request, $param=''){
        $survey->delete();
        if($request->has('ajax_submit')){
            $response = ['message' => 'Tools deleted', 'status' => 'success'];
            return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
        return redirect()->back()->withSuccess('Survey deleted');
    }

    public function detailUpdate(Request $request, $id){

       /* if(!Entrust::can('update-user') || (!Entrust::hasRole(DEFAULT_ROLE) && $user->hasRole(DEFAULT_ROLE)) )
            return redirect('/home')->withErrors(trans('messages.permission_denied'));*/

        if(!Entrust::can('update-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $validator = Validator::make(Input::all(), [
            'title'         => 'required',
            'description'   => 'required',
            'instruction'   => 'required',
            'thankyou_text' => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect() -> back() -> withErrors($validator);
        }

        DB::beginTransaction();
        try{
            $survey = Survey::find($id);

            if(!$survey)
                return redirect('/survey')->withErrors(trans('messages.invalid_link'));

            $start   = strtotime($request -> input('start_date'));
            $end     = strtotime($request -> input('end_date'));
            $request['start_date']  = date('Y-m-d', $start);
            $request['end_date']    = date('Y-m-d', $end);

            $survey->fill($request->all());
            $survey->save();

            $section_id             = $request -> input('section_id');
            $section_name           = $request -> input('section_name');
            $section_limit          = $request -> input('section_limit');
            $section_instruction    = $request -> input('section_instruction');
            $section_timer          = $request -> input('section_timer');

            $this -> survey -> update_section_info_on_survey($section_id, $section_name, $section_limit, $section_instruction, $section_timer);

            DB::commit();
            if($request->has('ajax_submit')){
                $response = ['Test successfully updated', 'status' => 'success'];
                return response()->json($response, 200, array('Access-Controll-Allow-Origin' => '*'));
            }
            return redirect()->back()->withSuccess('Test successfully updated');
        }catch(exception $e){
            DB::rollback();
            return false;
        }

    }

    public function create(){
        if(Entrust::can('create-survey')){
            $category    = SurveyCategory::select('name', 'id') -> get();
            $i = 0;
            foreach ($category as $key) {
                $survey_category[$i]['id']  = $key['id'];
                $survey_category[$i]['name']  = $key['name'];
                $i++;
            }
            return view('survey.create', compact('survey_category'));
        }else{
            return redirect('/home');
        }
    }

    public function addNewSurvey(){
        if(!Entrust::can('create-survey')){
            return redirect('/home');
        }
        $data['name']           = Input::get('name');
        $data['slug']           = strtolower(str_replace(' ', '-', $data['name'].'-'.strtotime('now')));
        $data['description']    = Input::get('description');
        $start                  = strtotime(Input::get('started'));
        $end                    = strtotime(Input::get('ended'));
        $data['started']        = date('Y-m-d 00:00:00', $start);
        $data['ended']          = date('Y-m-d 23:59:59', $end);
        $data['randomized']     = Input::get('randomized');
        $data['tools']          = Input::get('tools');
        $data['user_id']        = Auth::user()->id;

        $validator = Validator::make(Input::all(), [
            'name'          => 'required',
            'description'   => 'required',
            'started'       => 'required',
            'ended'         => 'required',
            'tools'         => 'array|required'
        ]);
        if ($validator->fails()) {
            return redirect() -> back() -> withErrors($validator);
        }
        if (strpos($data['name'], '/') == true) {
            return redirect() -> back() -> withErrors('Nama project tidak boleh memakai "/"');
        }
        if($data['started'] > $data['ended']){
            return redirect() -> back() -> withErrors('Tanggal akhir harus setelah tanggal awal');
        }

        $project            = $this -> survey -> save_project($data);
        $data['id_project'] = $project;
        if($data['randomized'] == null){
            $data['randomized'] = 0;
        }
        //cek apakah kategori survey merupakan survey yang harus diambil dari bank soal atau bukan
        foreach($data['tools'] as $key){
            if($key == 1 || $key == 2){
                $data['category'] = $key;
                $save_survey = $this -> survey -> save_from_bank_survey($data);
            }elseif($key == 3){
                $data['category'] = $key;
                $save_survey = $this -> survey -> save_wpa_survey($data);
            }elseif($key == 4){
                $data['category'] = $key;
                $save_survey = $this -> survey -> save_papi_survey($data);
            }

        }
        if($save_survey == false){
            return redirect()->back()->withErrors('Failed to save');
        }else{
            return redirect('/project')->withSuccess('Survey Created');
        }
    }

    /*
        Batas awal fungsi tambahan dari HURA 1.5 ke atas (pakai project, optimasi, dll)
    */
    public function index_project(){

        if(Auth::check() && !Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        //check status penyelesaian survey
        $this -> current_project_status();
        $this -> current_survey_status();
        $corporate_id   = Auth::user() -> profile -> id_corporate;
        $projects = $this -> survey -> get_all_project_with_total_assign($corporate_id);
        $col_heads = [];
        array_push($col_heads, 'Id');
        array_push($col_heads, 'Nama Project');
        array_push($col_heads, 'Tanggal Awal');
        array_push($col_heads, 'Tanggal Akhir');
        array_push($col_heads, 'Status');
        array_push($col_heads, 'Total User Assigned');
        // array_push($col_heads, 'Pembuat');
        array_push($col_heads, 'Pilihan');

        $rowset = null;
        $i = 0;
        if($projects){
            foreach($projects as $key){

                $start      = strtotime($key -> start_date);
                $end        = strtotime($key -> end_date);
                $start_date = date('d-m-Y',$start);
                $end_date   = date('d-m-Y',$end);

                $countUser = count($key->userProjects);
                $rowset[$i]['id'] = $key -> id;
                $rowset[$i]['nama_project'] = "<a href='/project/detail/".$key -> id."'><strong> <span class='glyphicon glyphicon-search'></span> ".$key -> name."</strong></a>";
                $rowset[$i]['tanggal_awal'] = $start_date;
                $rowset[$i]['tanggal_akhir'] = $end_date;
                if($key -> is_done == 0){
                    $rowset[$i]['status'] = "<span class='label label-warning'>Belum Berjalan</span>";
                }elseif($key -> is_done == 1){
                    $rowset[$i]['status'] = "<span class='label label-danger'>Selesai</span>";
                }elseif($key -> is_done == 2){
                    $rowset[$i]['status'] = "<span class='label label-success'>Sedang Berjalan</span>";
                }
                $rowset[$i]['total_user_assigned'] = $countUser;
                // $rowset[$i]['pembuat'] = $key -> first_name;
                //Check Standar Nilai dari project
                $checkStandarNilaiProject = StandarNilaiProject::where('project_id',$key->id)->first();
                if (sizeof($checkStandarNilaiProject)>0) {
                    $rowset[$i]['pilihan'] = "<a href='/project/standar_nilai/".$key->id."/edit' class='btn btn-info btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Edit Standar Nilai Aspek'><span class='glyphicon glyphicon-pencil'></span></a> ";
                } else {
                    $rowset[$i]['pilihan'] = "<a href='/project/standar_nilai/".$key->id."' class='btn btn-info btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Add Standar Nilai Aspek'><span class='glyphicon glyphicon-pencil'></span></a> ";
                }
                $rowset[$i]['pilihan'] .= "<a href='/project/assign/".$key->id."' class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Assign Peserta'><span class='glyphicon glyphicon-envelope'></span></a> ";
                $rowset[$i]['pilihan'] .= "<a href='/project/edit/".$key->slug."' class='edit-modal btn btn-warning btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Edit'><span class='glyphicon glyphicon-edit'></span></a>";
                // $rowset[$i]['pilihan'] .= "<a href='/project/hapus/".$key->slug."' class='edit-modal btn btn-danger btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Hapus' onclick='return confirm(`Apakah Anda yakin menghapus project ini?`)'><span class='glyphicon glyphicon-trash'></span></a>";
                $i++;
            }
        }
        return view('/survey.project', compact('col_heads', 'rowset'));
    }

    private function current_project_status() {
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $list = DB::table("project") -> get();
        foreach ($list as $row) {
            $persentase = partisipasi_peserta_project($row->id);

            //status berdasarkan periode
            if (date("Y-m-d h:i:s") < $row->start_date) {
                DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 0]);
            }elseif(date("Y-m-d h:i:s") >= $row->start_date && date("Y-m-d h:i:s") <= $row->end_date){
                //status berdasarkan partisipasi
                if ($persentase == 100) {
                    DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 1]);
                }else{
                    DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 2]);
                }
            }elseif(date("Y-m-d h:i:s") > $row->end_date){
                DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 1]);
            }
        }
    }

    private function current_survey_status() {
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $list = DB::table("survey") -> get();
        foreach ($list as $row) {
            $persentase = partisipasi_peserta_survey($row->id);

            //status berdasarkan periode
                // echo date("Y-m-d h:i:s")." - ".$row->tanggal_awal."<br/>";
            if (date("Y-m-d h:i:s") < $row->start_date) {
                DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 0]);
            }elseif(date("Y-m-d h:i:s") >= $row->start_date && date("Y-m-d h:i:s") <= $row->end_date){
                //status berdasarkan partisipasi
                if ($persentase == 100) {
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 1]);
                }else{
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 2]);
                }
            }elseif(date("Y-m-d h:i:s") > $row->end_date){
                DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 1]);
            }
        }
    }

    public function assign_peserta($id){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $id_corporate = Auth::user() -> profile -> id_corporate;

        $data['info_project'] = $this -> survey -> get_info_project_by_id($id);
        $data['list_peserta'] = $this -> survey -> get_peserta_available($id, $id_corporate);
        $credit                 = $this -> corporate -> get_credit($id_corporate);
        $peserta_diassign       = $this -> corporate -> get_kuota_aktif($id_corporate); //kuota project aktif
        $peserta_menyelesaikan  = $this -> corporate -> get_kuota_terpakai($id_corporate); //kuota terpakai project selesai
        $data['jatah']          = $credit - $peserta_diassign - $peserta_menyelesaikan;
        return view('/survey.assign', compact('data'));
    }

    public function do_assign(Request $request){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $input              = $request -> all();
        $data['id']         = $input['id'];
        $data['participant']= $input['participant'];
        $data['jatah']      = $input['jatah'];
        $data['jlh_user']   = count($data['participant'])-1;
        $i                  = 0;
        $list_survey        = $this -> survey -> get_list_survey_by_project($data['id']);
        $kuota['survey']    = 0;
        $kuota['user']      = 0;
        $explode            = explode(", ", $data['participant'][0]);
        $sudah              = 0;
        //ambil id survey berdasarkan project
        foreach ($list_survey as $key) {
            $data['survey'][$i] = $key['id'];
            $kuota['survey']++;
            $i++;
        }

        if($data['jlh_user'] > $data['jatah']){
            return redirect('project')->withErrors('Sisa kuota anda tidak mencukupi');
        }

        foreach($data['participant'] as $key){
            if($key != ""){
                $assign = $this -> survey -> assign_project($data, $key);

                if($assign == true){ //jika berhasil nambah user assign, maka kuota bertambah
                    $kuota['user']++;
                }
                $email = DB::table("users")->where(["id"=>$key])->first();
                // $this->notifEmailpilihan($email->email,$data['id']);
            }
        }

        //setting kuota terpakai
        $kuota['terpakai'] = /*$kuota['survey'] * $kuota['user'];*/ $kuota['user'];
        $this -> survey -> set_kuota_terpakai($kuota);
        if($sudah == count($explode)){
            return redirect('project')->withErrors('Semua peserta sudah pernah di assign');
        }
        if(count($data['participant']) == 1){
            if($data['participant'][0] == ""){
                return redirect('project')->withErrors('Tidak ada peserta yang Anda assign');
            }
        }
        return redirect('project')->withSuccess('Peserta berhasil diassign');
    }

    public function do_assign_upload(ExcelRequestPost $request){

        if(Auth::check() && !Entrust::can('create-user')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $post       = $request -> all();
        $id_project = $post['id_project'];
        $jatah      = $post['jatah'];
        $file_path  = $_FILES['file']['tmp_name'];
        $list_data  = Excel::load($file_path, function($reader){});
        $main       = $list_data->get();
        $sudah      = 0;
        $expiresAt  = Carbon::now()->addMinutes(60);
        if(count($main) > $jatah){
            return redirect('project')->withErrors('Sisa kuota anda tidak mencukupi');
        }

        $arrInSystem    = [];
        $arrInProject   = [];
        $arrNew         = [];
        $result_assign  = [];
        foreach($main as $row){
            if($row->email || $row->name){ //jika email dan nama dalam satu row ada yang tidak empty
                if($row->email === NULL){
                    return redirect()->back()->withInput()->withErrors('Email is null');
                }

                if($row->name === NULL){
                    return redirect()->back()->withInput()->withErrors('Name is null');
                }

                $emails = strtolower($row->email);
                $emails = trim($emails, " ");
                if(filter_var($emails, FILTER_VALIDATE_EMAIL) == false){
                    return redirect()->back()->withInput()->withErrors('Wrong Email: '.$emails);
                }
                //proses generate password OTP
                $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < 6; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }

                $raw = [
                    "user"=>[
                        "email"         => $emails,
                        "username"      => str_slug($row->name, '_'),
                        "password"      => bcrypt(strtolower((string)$randomString)),
                        "status"        => 'active',
                        "custom_token"  => encrypt_decrypt("encrypt",strtolower($row->email)),
                        "pswd"          => $randomString,
                    ],
                    "profile"=>[
                        "first_name"    => $row->name,
                    ]
                ];

                //cek apakah user sudah terdaftar di sistem
                $qInSystem = User::where("email",$raw["user"]["email"])->first();

                if($qInSystem){ //jika sudah, cek apakah user sudah terdaftar di project. simpan tiap user sesuai kondisi
                    $qInProject = DB::table('user_project')->where('id_user', '=', $qInSystem->id)->where('id_project', '=', $id_project)->first();

                    if($qInProject){
                        array_push($arrInProject, $raw);
                        array_push($result_assign, ["result" => true, "status" => "in project", "email" => $qInSystem->email]);
                    }else{
                        array_push($arrInSystem, ["email" => $qInSystem -> email, "id" => $qInSystem -> id]);
                    }
                }else{
                    array_push($arrNew, $raw);
                }
            }else{ //jika email dan nama dalam satu row semua empty
                // return redirect()->back()->withInput()->withErrors('Pastikan konten sesuai dengan template yang disediakan');
                continue;
            }
        }

        $do_assign_in_system= $this -> exec_array_in_system($arrInSystem, $id_project);
        if($do_assign_in_system != null){
            foreach ($do_assign_in_system as $key) {
                array_push($result_assign, ["result" => $key['result'], "status" => $key['status'], "email" => $key['email']]);
            }
        }

        $do_assign_new      = $this -> exec_array_new($arrNew, $id_project);
        if($do_assign_new != null){
            foreach ($do_assign_new as $key) {
                array_push($result_assign, ["result" => $key['result'], "status" => $key['status'], "email" => $key['email']]);
            }
        }

        Cache::put('report-assign-'.$id_project, $result_assign, $expiresAt);
        return redirect('project/report-assign/'.$id_project);
        // return view('survey/laporan_assign', compact('result_assign', 'info_project'));
    }

    public function report_assign_peserta($id_project){
        $result_assign = null;

        if(Cache::has('report-assign-'.$id_project)){
            $result_assign = Cache::get('report-assign-'.$id_project);

        }
        $info_project = $this -> survey -> get_info_project_by_id($id_project);

        return view('survey/laporan_assign', compact('result_assign', 'info_project'));
    }

    public function exec_array_in_system($data, $id_project){
        $data_project['id'] = $id_project;
        $i                  = 0;
        $list_survey        = $this -> survey -> get_list_survey_by_project($id_project);

        //ambil id survey berdasarkan project
        $i = 0;
        foreach ($list_survey as $key) {
            $data_project['survey'][$i] = $key['id'];
            $i++;
        }

        $result_assign = [];
        foreach($data as $key) {
            $do_assign = $this -> survey -> assign_project($data_project, $key['id']);
            if($do_assign == true){ //jika berhasil nambah user assign, maka kirim email
                // $this->notifEmailpilihan($key['email'],$id_project);
            }
            array_push($result_assign, ["result" => $do_assign, "status" => "in system", "email" => $key['email']]);
        }
        return $result_assign;
    }

    public function exec_array_new($data, $id_project){
        $result_assign = [];
        foreach ($data as $key) {
            $user = User::create($key["user"]);
            User::where('id', '=', $user->id)->update(['pswd' => $key['user']['pswd']]);

            $data_profile = $key["profile"];
            $data_profile["user_id"] = $user->id;
            $data_profile["id_corporate"] =  Auth::user()->profile->id_corporate;
            $data_profile["id_division"] = Auth::user()->profile->id_division;
            $profile = Profile::create($data_profile);

            $role = Role::find(2);
            $user->attachRole($role);

            DB::commit();
            $password = $key["user"]["pswd"];
            //proses assign peserta ke project terkait
            $assign['id']       = $id_project;
            $i                  = 0;
            $list_survey        = $this -> survey -> get_list_survey_by_project($id_project);

            //ambil id survey berdasarkan project
            foreach ($list_survey as $key) {
                $assign['survey'][$i] = $key['id'];
                $i++;
            }

            $do_assign = $this -> survey -> assign_project($assign, $user->id);
            if($do_assign == true){ //jika berhasil nambah user assign, maka kirim email
                // $this->notifEmail($user->email,$password,$id_project);
            }

            array_push($result_assign, ["result" => $do_assign, "status" => "new", "email" => $user->email]);
        }
        return $result_assign;
    }

    public function show_project($id){
        ini_set('max_execution_time', 480);
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $sudah = $this -> survey -> get_penyelesaian_project_sudah($id);
        $belum = $this -> survey -> get_penyelesaian_project_belum($id);

        $project = $this -> survey -> get_detail_project($id);
        $i = 0;
        foreach ($project as $key) {
            $detail['id']               = $key['id'];
            $detail['slug']             = $key['slug'];
            $detail['nama']             = $key['name'];
            $detail['deskripsi']        = $key['description'];
            $detail['tanggal_awal']     = $key['start_date'];
            $detail['tanggal_akhir']    = $key['end_date'];
            $detail['is_done']          = $key['is_done'];
            $detail['tools'][$i]['judul']       = $key['title'];
            $detail['tools'][$i]['id']          = $key['id_survey'];
            $detail['tools'][$i]['deskripsi']   = $key['instruction_survey'];
            $detail['tools'][$i]['status']      = $key['status_survey'];
            $detail['tools'][$i]['is_random']   = $key['is_random'];
            $i++;
        }
        $peserta = $this -> survey -> get_peserta_project($detail['id']);
        $rowset = null;
        $i = 0;
        if($peserta){
            foreach($peserta as $key){
                $rowset[$i]['name'] = $key -> first_name;
                if($key -> is_done == 0){
                    $rowset[$i]['status'] = "<span class='label label-danger'>Belum menyelesaikan</span>";
                }else{
                    $rowset[$i]['status'] = "<span class='label label-success'>Sudah menyelesaikan</span>";
                }

                //proses untuk munculkan ceklis survey yang dikerjakan
                $ceklis_survey = DB::table('user_survey') -> join('survey', 'survey.id', '=', 'user_survey.id_survey')
                                -> where('user_survey.id_project', '=', $id)
                                -> where('id_user', '=', $key -> id_user) -> get();
                $j = 0;
                $belum_tools = 0;
                foreach($ceklis_survey as $value){
                    if($value -> category_survey_id == 1){
                        $survey = "LAI";
                    }elseif($value -> category_survey_id == 2){
                        $survey = "TIKI";
                    }elseif($value -> category_survey_id == 3){
                        $survey = "WPA";
                    }elseif($value -> category_survey_id == 4){
                        $survey = "WBA";
                    }
                    if($value -> is_done == 0){
                        $belum_tools++;
                        $rowset[$i]['ceklis'][$j] = "<span class='label label-warning pull-right' data-toggle='tooltip' data-placement='top' title='' data-original-title='Belum'>".$survey."</span>";
                    }else{
                        $rowset[$i]['ceklis'][$j] = "<span class='label label-success pull-right' data-toggle='tooltip' data-placement='top' title='' data-original-title='Sudah'>".$survey."</span>";
                    }
                    $j++;
                }

                if($ceklis_survey->count() == $belum_tools){
                    $rowset[$i]['delete'] = "<a href='/project/hapus-peserta/".$id."/".$key->id_user."' class='edit-modal btn btn-danger btn-sm' data-toggle='tooltip' data-placement='top' title='' data-original-title='Hapus Peserta' onclick='return confirm(`Apakah Anda yakin menghapus peserta dari project ini?`)'><span class='glyphicon glyphicon-trash'></span></a>";
                }else{
                    $rowset[$i]['delete'] = "";
                }
                $i++;
            }
        }

        if($sudah == 0 && $belum == count($peserta)){
            $sebagian = 0;
        }else{
            $sebagian = count($peserta) - $sudah - $belum;
        }

        // dd($sudah, $belum, $sebagian);
        return view('/survey.detail_project', compact('detail', 'rowset', 'sudah', 'belum', 'sebagian'));
    }

    public function hapus_peserta_project($id_project, $id_user){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $do_hapus = $this -> survey -> hapus_peserta_project($id_project, $id_user);
        if($do_hapus == true){
            return redirect('project/detail/'.$id_project)->withSuccess('Peserta Berhasil Dihapus');
        }
    }

    public function edit_project($slug){
        if(!Entrust::can('update-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $data   = $this -> survey -> get_info_project_by_slug($slug);
        return view('/survey.edit_project',compact('data'));
    }

    public function do_edit_project(Request $request){
        if(!Entrust::can('update-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $input                          = Input::all();
        $data['id']                     = $input['id'];
        $data['name']                   = $input['name'];
        $data['description']            = $input['description'];
        $data['slug']                   = strtolower(str_replace(' ', '-', $data['name'].'-'.strtotime('now')));
        $start                          = $input['start_date'];
        $end                            = $input['end_date'];
        $data['start_date']             = date('Y-m-d 00:00:00', strtotime($start));
        $data['end_date']               = date('Y-m-d 23:59:59', strtotime($end));
        $data['user_id']                = Auth::user()->id;
        $validator = Validator::make(Input::all(), [
            'name'        => 'required',
            'description' => 'required',
            'start_date'  => 'required',
            'end_date'    => 'required',
        ]);
        if ($validator->fails()) {
            return redirect() -> back() -> withErrors($validator);
        }

        if($data['start_date'] > $data['end_date']){
            return redirect() -> back() -> withErrors('Tanggal akhir harus setelah tanggal awal');
        }
        $edit = $this -> survey -> edit_project($data);

        if($edit == 'reschedule'){
            // $this -> email_reschedule($data['id']);
        }
        return redirect('project/')->withSuccess('Data Project Berhasil Diubah');
    }

    public function hapus_project($slug){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_project   = $this -> survey -> get_info_project_by_slug($slug);
        $id             = $info_project['id'];
        $delete = $this -> survey -> hapus_project($id);
        return redirect('project')->withSuccess('Data Project Berhasil Dihapus');
    }

    public function get_project_diassign($user_id){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $project_diassign = $this -> survey -> get_project_diassign($user_id);
        $rowset = null;
        $i = 0;
        foreach ($project_diassign as $key) {
            $rowset[$i]['nama_project'] = "<a href='/user/history/".$key->slug."/".$key->id_user."' data-toggle='tooltip' data-placement='top' title='' data-original-title='Lihat lebih detil'><span class='glyphicon glyphicon-search'></span> ". $key -> name."</a>";
            $rowset[$i]['tanggal_akhir']       = $key -> end_date;
            if($key -> is_selesai == 1){
                $rowset[$i]['is_done'] = "<span class='label label-info'>Sudah Menyelesaikan</span>";
            }else{
                $rowset[$i]['is_done'] = "<span class='label label-warning'>Belum Menyelesaikan</span>";
            }
            $i++;
        }
        return view('user.history-project', compact('rowset', 'user_id'));
    }

    public function get_tools_diassign($slug, $user_id){
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_project   = $this -> survey -> get_info_project_by_slug($slug);
        $id_project     = $info_project['id'];
        $detail_project = $this -> survey -> get_tools_diassign($id_project, $user_id);
        $i = 0;
        foreach ($detail_project as $key) {
            $detail['nama']             = $key['name'];
            $detail['deskripsi']        = $key['description'];
            $detail['tanggal_awal']     = $key['start_date'];
            $detail['tanggal_akhir']    = $key['end_date'];
            $detail['is_done']          = $key['is_done'];
            $detail['tools'][$i]['judul']       = $key['title'];
            $detail['tools'][$i]['slug']        = $key['slug_tools'];
            if($key['category_survey_id'] == 1){
                $detail['tools'][$i]['category'] = 'gti';
            }elseif($key['category_survey_id'] == 2){
                $detail['tools'][$i]['category'] = 'tiki';
            }elseif($key['category_survey_id'] == 3){
                $detail['tools'][$i]['category'] = 'wpa';
            }elseif($key['category_survey_id'] == 4){
                $detail['tools'][$i]['category'] = 'kostick';
            }

            $detail['tools'][$i]['status']      = $key['is_done_tools'];
            $i++;
        }

        return view('/user.history-tools', compact('detail', 'rowset', 'user_id', 'slug'));

    }
    //======================================================= USER =======================================================//
    public function getInstruction($slug){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }
        $data['slug']           = $slug;
        $data['user_id']        = Auth::user() -> id;
        $survey_info            = $this -> survey -> get_info_survey_by_slug($slug);
        $data['title']          = $survey_info['title'];
        $thankyou_text          = $survey_info['thankyou_text'];

        //cek section yang belum dikerjakan
        $next_section = $this -> survey -> get_next_section($slug);
        //jika masih ada section berikutnya, ambil id_section berikutnya sebagai new_section
        if($next_section){
            $data['section_name'] = $next_section['name'];
            $data['section']      = $next_section['id'];

            if($data['section_name'] ==  'Subtest 1'){
                $page_instruction = 'survey.tool-instruction-gti01';
            }elseif($data['section_name'] ==  'Subtest 2'){
                $page_instruction = 'survey.tool-instruction-gti02';
            }elseif($data['section_name'] ==  'Subtest 3'){
                $page_instruction = 'survey.tool-instruction-gti03';
            }elseif($data['section_name'] ==  'Subtest 4'){
                $page_instruction = 'survey.tool-instruction-gti04';
            }elseif($data['section_name'] ==  'Subtest 5'){
                $page_instruction = 'survey.tool-instruction-gti05';
            }elseif($data['section_name'] ==  'Berhitung Angka'){
                $page_instruction = 'survey.tool-instruction-tiki01';
            }elseif($data['section_name'] ==  'Gabungan Bagian'){
                $page_instruction = 'survey.tool-instruction-tiki02';
            }elseif($data['section_name'] ==  'Hubungan Kata'){
                $page_instruction = 'survey.tool-instruction-tiki03';
            }elseif($data['section_name'] ==  'Abstraksi Non Verbal'){
                $page_instruction = 'survey.tool-instruction-tiki04';
            }elseif($data['section_name'] ==  'Work Personality Analytics'){
                $page_instruction = 'survey.tool-instruction-wpa';
            }elseif($data['section_name'] ==  'Work Behavioural Assessment'){
                $page_instruction = 'survey.tool-instruction-papikostik';
            }

            // Cache::flush();
            Cache::forget('result-id-type-question-'.$slug.'-'.$data['user_id']);
            Cache::forget('cache-result-wpa-'.$slug.'-'.$data['user_id']);
            Cache::forget('cache-result-papi-'.$slug.'-'.$data['user_id']);

            $old_section = $data['section']-1;
            //pengecekan apakah ada section sebelumnya atau tidak. Jika ada, maka simpan data jawaban dari section sebelumnya lewat ajax
            if(Cache::has('cache-result-'.$slug.'-'.$old_section.'-'.$data['user_id'])){
                $data['has_result'] = 'yes';
                $data['old_section']= $old_section;
            }else{
                $data['has_result'] = 'no';
                $data['old_section']= 0;
            }

            return view($page_instruction, compact('data'));
        }else{ //jika tidak ada section berikutnya maka masuk halaman finish
            $info_survey            = $this -> survey -> get_info_survey_by_slug($slug);
            $data['slug']           = $slug;
            $data['thankyou_text']  = $info_survey['thankyou_text'];
            $data['category']       = $info_survey['category_survey_id'];
            $data['section']        = 0;
            $data['id_user']        = Auth::user() -> id;
            return view('survey.finish', compact('data'));
        }

    }

    public function getSimulasi($slug, $section, $simulasi){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }
        $data['slug']     = $slug;
        $data['section']  = $section;
        $data['user_id']  = Auth::user() -> id;
        $page_instruction = 'survey.'.$simulasi;

        return view($page_instruction, compact('data'));
    }

    public function startNextSection($slug, $new_section){
        if(Entrust::can('take-test')){

            $section_info   = $this -> survey -> get_info_section($new_section);
            $instruction    = $section_info['instruction'];
            $data['section']= $new_section;
            $data['slug']   = $slug;
            $data['section_name'] = $section_info['name'];
            $user_id        = Auth::user() -> id;

            if($data['section_name'] ==  'Subtest 1'){
                $page_instruction = 'survey.tool-instruction-gti01';
            }elseif($data['section_name'] ==  'Subtest 2'){
                $page_instruction = 'survey.tool-instruction-gti02';
            }elseif($data['section_name'] ==  'Subtest 3'){
                $page_instruction = 'survey.tool-instruction-gti03';
            }elseif($data['section_name'] ==  'Subtest 4'){
                $page_instruction = 'survey.tool-instruction-gti04';
            }elseif($data['section_name'] ==  'Subtest 5'){
                $page_instruction = 'survey.tool-instruction-gti05';
            }elseif($data['section_name'] ==  'Berhitung Angka'){
                $page_instruction = 'survey.tool-instruction-tiki01';
            }elseif($data['section_name'] ==  'Gabungan Bagian'){
                $page_instruction = 'survey.tool-instruction-tiki02';
            }elseif($data['section_name'] ==  'Hubungan Kata'){
                $page_instruction = 'survey.tool-instruction-tiki03';
            }elseif($data['section_name'] ==  'Abstraksi Non Verbal'){
                $page_instruction = 'survey.tool-instruction-tiki04';
            }

            // Cache::flush();
            Cache::forget('result-id-type-question-'.$slug.'-'.$user_id);
            Cache::forget('cache-result-wpa-'.$slug.'-'.$user_id);
            Cache::forget('cache-result-papi-'.$slug.'-'.$user_id);
            return view($page_instruction, compact('data'));
        }else{
            return redirect('/home');
        }
    }

    public function saveToReport($slug, $user_id){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $survey_id      = $info_survey['id'];
        $survey_category= $info_survey['category_survey_id'];
        $sections        = $this -> survey -> get_section_by_survey($survey_id);

        foreach($sections as $section){
            $info_section   = $this -> survey -> get_info_section($section['id']);
            $section_name   = $info_section['name'];

            if($survey_category == 1){

                $point = null; //array utk nanti kalkulasi gtq (indexnya sum dan count)
                $nilai = null; //array benar salah utk tiap jawaban (isinya 1 atau 0 berdasar raw input)
                $adj = 0; //rumus dari excel
                $true = 0; //jumlah jawaban benar
                $false = 0; //jumlah jawaban salah

                $raw_input = $this -> result -> get_raw_input($survey_id, $section['id']);
                if($raw_input){
                    $arr_raw_input = json_decode($raw_input['raw_input'], true);
                    $i = 0;
                    foreach ($arr_raw_input as $raw) {
                        $nilai[$i] = $raw['point'];
                        $i++;
                    }
                    $point['sum'] = array_sum($nilai);
                    $point['count'] = count($nilai);
                }else{
                    $point = 0;
                }

                // $point = $this -> result -> get_point_survey_gti($survey_id, $section['id']);
                if(is_array($point)){


                    //perhitungan score GTI
                    $true   = $point['sum'];
                    $false  = $point['count'] - $point['sum'];

                    /*
                        rumus adj setiap subtest :
                        adj = jumlah benar - (jumlah salah / opsi - 1)
                    */
                    if($section_name == 'Subtest 1'){ //letter check
                        $adj = floor($true - ($false/4));
                        if($adj < 0){ //batas minimum
                            $gtq = 74;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq1', $adj);
                            $gtq = $tq -> tq;
                        }
                    }elseif($section_name == 'Subtest 2'){ //reasoning
                        $adj = floor($true - ($false/2));
                        if($adj < 0){
                            $gtq = 69;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq2', $adj);
                            $gtq = $tq -> tq;
                        }
                    }elseif($section_name == 'Subtest 3'){ //letter distance
                        $adj = floor($true - ($false/1));
                        if($adj < 0){
                            $gtq = 69;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq3', $adj);
                            $gtq = $tq -> tq;
                        }
                    }elseif($section_name == 'Subtest 4'){ //numerical
                        $adj = floor($true - ($false/2));
                        if($adj < 0){
                            $gtq = 69;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq4', $adj);
                            $gtq = $tq -> tq;
                        }
                    }elseif($section_name == 'Subtest 5'){ //spatial
                        $adj = floor($true - ($false/3));
                        if($adj < 0){
                            $gtq = 69;
                        }else{
                            $tq = $this -> result -> get_gtq('lookup_gtq5', $adj);
                            $gtq = $tq -> tq;
                        }
                    }
                    $score['survey_id']     = $survey_id;
                    $score['section_id']    = $section['id'];
                    $score['user_id']       = $user_id;
                    $sum            = $gtq;
                    $score['score'] = $sum;
                }else{ //jika waktu habis dan user tidak menjawab sama sekali, maka skor diambil yang terendah
                    $score['survey_id']     = $survey_id;
                    $score['section_id']    = $section['id'];
                    $score['user_id']       = $user_id;
                    $score['score']         = 69;
                }

                //update data di table user_report
                if($section_name == 'Subtest 1'){ //letter check
                    $user_report['GTQ1'] = $score['score'];
                }elseif($section_name == 'Subtest 2'){ //reasoning
                    $user_report['GTQ2'] = $score['score'];
                }elseif($section_name == 'Subtest 3'){ //letter distance
                    $user_report['GTQ3'] = $score['score'];
                }elseif($section_name == 'Subtest 4'){ //numerical
                    $user_report['GTQ4'] = $score['score'];
                }elseif($section_name == 'Subtest 5'){ //spatial
                    $user_report['GTQ5'] = $score['score'];
                }

                $this -> result -> update_user_report($user_report, $info_survey['id_project']);
            }elseif($survey_category == 2){
                $score = null;
                //ambil raw inputan tiki
                $raw_input = $this -> result -> get_raw_input($survey_id, $section['id']);
                if($raw_input){
                    $arr_raw_input = json_decode($raw_input['raw_input'], true);
                    $i = 0;
                    foreach ($arr_raw_input as $raw) {
                        $score[$i] = $raw['point'];
                        $i++;
                    }

                    if($section_name != "Berhitung Angka"){ //untuk yang multiple answer
                        $point = 0;
                        $i = 0;
                        $j = 0;
                        for($i = 0; $i < count($score)-1; $i++){
                            $j = $i + 1;
                            if($j % 2 != 0){
                                if($score[$i] == 1 && $score[$j] == 1){
                                    $point++;
                                }
                            }
                        }
                        // $opsi = 0;
                        // $point= 0;
                        // foreach ($arr_raw_input as $arr_raw) {
                        //     if($opsi == 2){
                        //         if($score[0] == 1 && $score[1] == 1){ //jika jawaban ke 1 dan 2 benar, maka point nambah 1
                        //             $point++;
                        //         }
                        //         $opsi = 0;
                        //         $score = null;
                        //     }
                        //     $score[$opsi] = $arr_raw['point'];
                        //     $opsi++;
                        // }
                    }else{
                        $point = array_sum($score);
                    }

                }else{
                    $point = 0;
                }
                // $point = $this -> result -> get_point_survey_tiki($survey_id, $section['id'], $section_name);


                if($section_name == 'Berhitung Angka'){
                    $column = 'subtest1';
                }elseif($section_name == 'Gabungan Bagian'){
                    $column = 'subtest2';
                }elseif($section_name == 'Hubungan Kata'){
                    $column = 'subtest3';
                }elseif($section_name == 'Abstraksi Non Verbal'){
                    $column = 'subtest4';
                }

                //lookup standar score berdasarkan section dan jumlah jawaban benar
                $standar_score = $this -> result -> get_standard_score_tiki($column, $point);
                foreach ($standar_score as $std) {
                    $score['score']         = $std;
                }
                $score['survey_id']     = $survey_id;
                $score['section_id']    = $section['id'];
                $score['user_id']       = $user_id;

                //update data di table user_report
                if($section_name == 'Berhitung Angka'){ //letter check
                    $user_report['SS1'] = $score['score'];
                }elseif($section_name == 'Gabungan Bagian'){ //reasoning
                    $user_report['SS2'] = $score['score'];
                }elseif($section_name == 'Hubungan Kata'){ //letter distance
                    $user_report['SS3'] = $score['score'];
                }elseif($section_name == 'Abstraksi Non Verbal'){ //spatial
                    $user_report['SS4'] = $score['score'];
                }

                $this -> result -> update_user_report($user_report, $info_survey['id_project']);
            }

            //proses masukkan hasil score ke tabel report
            $this -> result -> save_report($score);

        }
        //ambil grade jika ada grade
        $score_final= $this -> result -> get_final_score_user($survey_id);
        $is_grade   = $this -> survey -> check_grade_survey($survey_id);

        //update grade user jika ada
        if($is_grade == true){
            $grade = $this -> survey -> get_grade($score_final, $survey_id);
            $this -> survey -> update_grade_user($survey_id, $grade, $score_final);
        }

        //update data di tabel user_report
        $resume     = $this -> result -> get_report_gti_tiki($survey_id, $user_id);
        if($survey_category == 1){ //LAI, update kriteria
            $avg        = round(array_sum($resume)/count($resume), 1);
            $criteria   = $this -> result -> get_criteria_gti_report($avg);
            $user_report['kriteria'] = $criteria;
            $this -> result -> update_user_report($user_report, $info_survey['id_project']);
        }elseif($survey_category == 2){ //TIKI, update IQ
            $total  = array_sum($resume);
            if($total <= 29){ //batas bawah
                $iq = 56;
            }elseif($total > 107){ //batas atas
                $iq = 145;
            }else{ //lookup
                $iq     = $this -> result -> get_iq_tiki($total);
            }
            $user_report['jumlah']  = $total;
            $user_report['IQ']      = $iq;
            $this -> result -> update_user_report($user_report, $info_survey['id_project']);
        }

        $this -> survey -> survey_complete($slug);
        $duration   = $this -> survey -> get_temp($survey_id);
        $this -> survey -> delete_temp_survey($slug);


        //LAI dan TIKI
        $complete = check_project_complete($info_survey['id_project'], $user_id);
        if($complete == true){
            $this->completeEmail(Auth::user() -> email,$info_survey['id_project']);
        }

        // Cache::flush();
        Cache::forget('result-id-type-question-'.$slug.'-'.$user_id);

        $data['finish']         = 'finish';
        $data['slug']           = $slug;
        $log['activity']        = 'Menyelesaikan survey '.$info_survey['title'];
        $log['user_id']         = $user_id;
        $log['reference_id']    = $survey_id;
        $log['ip']              = \Request::ip();
        $log['duration']        = $duration;
        $log['created_at']      = Carbon::now();
        $log['user_agent']      = \Request::header('User-Agent');
        save_activity($log);

        return 'true';

    }

    public function finish($slug, $section, $id_user){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey            = $this -> survey -> get_info_survey_by_slug($slug);
        $data['slug']           = $slug;
        $data['thankyou_text']  = $info_survey['thankyou_text'];
        $data['category']       = $info_survey['category_survey_id'];
        $data['section']        = $section;
        $data['id_user']        = $id_user;
        return view('survey.finish', compact('data'));
    }

    public function getQuestion($slug, $section){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey          = $info_survey['id'];
        $used_timer         = $this -> survey -> get_temp($id_survey);
        $id_category_survey = $info_survey['category_survey_id'];
        $expiresAt          = Carbon::now()->addMinutes(60);
        $user_id            = Auth::user() -> id;
        //cek jika survey merupakan kategory WPA, maka panggil fungsi test WPA. jika Papikostick, maka panggil fungsi Papikostic
        if($id_category_survey == 3){
            return redirect('survey/get-question-wpa/'.$slug.'/'.$section);
        }elseif($id_category_survey == 4){
            return redirect('survey/get-question-papi/'.$slug.'/'.$section);
        }else{

            //untuk keperluan progress bar
            $total          = $this -> question -> get_total_question_on_section($section);
            if(Cache::has('result-count-'.$slug.'-'.$user_id)){
                $count = Cache::get('result-count-'.$slug.'-'.$user_id);
            }else{
                Cache::put('result-count-'.$slug.'-'.$user_id, 0, $expiresAt);
                $count = Cache::get('result-count-'.$slug.'-'.$user_id);
            }

            //ambil info section.
            $section_info   = $this -> survey -> get_info_section($section);
            $instruction    = $section_info['instruction'];
            $section_name   = $section_info['name'];
            $section_slug   = $section_info['slug'];
            $section_limit  = $section_info['limit'];
            $folder_image   = '';
            for($i = 0; $i < strlen($section_slug)-11; $i++){
                $folder_image .= $section_slug[$i];
            }

            //ambil pertanyaan selanjutnya berdasarkan section_name
            if($section_info['name'] == 'Reading Text'){
                $questions = $this -> question -> get_question_survey_reading($slug, $section, $section_limit, $total);
            }elseif($section_info['name'] == 'Matching Question'){
                $questions = $this -> question -> get_question_survey_matching($slug, $section, $section_limit, $total);
            }elseif($section_info['name'] == 'Blank in Paragraph'){
                $questions = $this -> question -> get_question_survey_blank_in_paragraph($slug, $section, $section_limit, $total);
            }else{
                $questions = $this -> question -> get_question_survey($slug, $section);
            }

            if($section_info['name'] == 'Reading Text'){
                foreach ($questions as $key) {
                    $id_type_question   = $key -> id_type_question;
                    $id_question        = $key -> id_question;
                    $is_random          = $key -> is_random;
                    $paragraph          = $key -> paragraph;
                    $timer              = $key -> timer;
                    $timer_survey       = $key -> timer_survey;
                    $timer_section      = $key -> timer_section;

                    $option_answer[$id_question] = $this -> option_answer -> get_by_question_unhide($id_question, $is_random);
                }

                return view('survey.detail_reading', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'paragraph', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
            }elseif($section_info['name'] == 'Matching Question'){
                $i = 0;
                foreach ($questions as $key) {
                    $id_type_question   = $key -> id_type_question;
                    $id_question        = $key -> id_question;
                    $is_random          = $key -> is_random;
                    $timer              = $key -> timer;
                    $timer_survey       = $key -> timer_survey;
                    $timer_section      = $key -> timer_section;
                    $arr_id_question[$i]= $key -> id_question;
                    $i++;
                }
                $option_answer = $this -> option_answer -> get_option_matching($section, $is_random, $section_limit, $total, $arr_id_question);

                return view('survey.detail_matching', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
            }elseif($section_info['name'] == 'Blank in Paragraph'){
                $i = 0;
                foreach ($questions as $key) {
                    $id_type_question   = $key -> id_type_question;
                    $id_question        = $key -> id_question;
                    $is_random          = 1;
                    $timer              = $key -> timer;
                    $timer_survey       = $key -> timer_survey;
                    $timer_section      = $key -> timer_section;
                    $arr_id_question[$i]= $key -> id_question;
                    $i++;
                }
                $option_answer = $this -> option_answer -> get_option_matching($section, $is_random, $section_limit, $total, $arr_id_question);

                return view('survey.detail_blank_paragraph', compact('id_survey', 'slug', 'questions', 'option_answer', 'timer', 'section', 'section_name', 'section_slug', 'instruction', 'timer_survey', 'used_timer', 'timer_section'));
            }else{
                $i = 0;

                //check cache, jika ada maka ambil cache simpan di variabel
                if(Cache::has('cache-'.$slug.'-'.$user_id)){
                    $timer                  = Cache::get('timer-'.$slug.'-'.$user_id);
                    $timer_survey           = Cache::get('timer-survey-'.$slug.'-'.$user_id);
                    $timer_section          = Cache::get('timer-section-'.$slug.'-'.$user_id);
                    $survey_title           = Cache::get('survey-title-'.$slug.'-'.$user_id);
                    $id_question            = Cache::get('id-question-'.$slug.'-'.$user_id);
                    $content_question       = Cache::get('question-'.$slug.'-'.$user_id);
                    $option_answer          = Cache::get('option-answer-'.$slug.'-'.$user_id);

                    //setting class untuk sprite gambar pilihan jawaban
                    if($section_name == 'Berhitung Angka'){
                        $class = 'tiki1';
                        $src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
                    }elseif($section_name == 'Gabungan Bagian'){
                        $class = 'tiki2';
                        $src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
                    }elseif($section_name == 'Abstraksi Non Verbal'){
                        $class = 'tiki4';
                        $src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAABVAQMAAACFL4Q2AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABxJREFUeNrtwYEAAAAAw6D5U1/gCFUBAAAAAPAMFUAAAYQO3CcAAAAASUVORK5CYII=';
                    }
                    return view('survey.detail', compact('id_survey', 'slug', 'questions', 'option_answer', 'used_timer', 'section', 'count', 'total', 'section_name', 'section_slug', 'instruction', 'timer', 'timer_survey', 'timer_section', 'survey_title', 'content_question', 'id_question', 'folder_image', 'class', 'src'));
                }else{
                    return redirect('/survey'.'/'.$slug.'/intro');
                }
            }
        }

    }

    public function clearCacheResult($slug, $section){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $expiresAt      = Carbon::now()->addMinutes(60);
        $user_id        = Auth::user() -> id;
        $section_info   = $this -> survey -> get_info_section($section);
        $section_name   = $section_info['name'];
        // Cache::flush();
        Cache::forget('result-count-'.$slug.'-'.$section.'-'.$user_id);
        Cache::forget('result-id-type-question-'.$slug.'-'.$user_id);
        Cache::forget('cache-result-'.$slug.'-'.$section.'-'.$user_id);
        Cache::forget('cache-result-wpa-'.$slug.'-'.$user_id);
        Cache::forget('cache-result-papi-'.$slug.'-'.$user_id);
        Cache::forget('result-'.$slug.'-'.$section.'-'.$user_id);

        Cache::put('result-count-'.$slug.'-'.$section.'-'.$user_id, 0, $expiresAt);
        return redirect('/survey'.'/'.$slug.'/intro');

    }

    public function setResultCache(Request $request){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $status         = 'add'; //untuk status edit atau tambah jawaban
        $expiresAt      = Carbon::now()->addMinutes(60);
        $id_question    = $request->input('id_question');
        $id_option      = $request->input('id_option');
        $timer          = $request->input('timer');
        $slug           = $request->input('slug');
        $section        = $request->input('section');
        $id_user        = $request->input('id_user');
        $id_type_question   = $request->input('id_type_question');
        $length_question    = $request->input('length_question');

        Cache::put('result-id-type-question-'.$slug.'-'.$id_user, $id_type_question, $expiresAt);
        $result = array(
            'question'  => $id_question,
            'answer'    => $id_option,
            'timer'     => $timer
        );
        $array_result = [];
        if(Cache::has('cache-result-'.$slug.'-'.$section.'-'.$id_user)){
            $array_result = Cache::get('result-'.$slug.'-'.$section.'-'.$id_user);
        }else{
            Cache::put('cache-result-'.$slug.'-'.$section.'-'.$id_user, 'yes', $expiresAt);
        }

        array_push($array_result, $result);

        /*
            batas awal proses pengecekan apakah ada nomor soal yang sama,
            jika sama artinya user edit/ganti jawaban
        */
        $duplicate  = null;
        $updated    =  array();
        $temp_nomor = [];
        $i = 0;
        foreach ($array_result as $key) {
            array_push($temp_nomor, $key['question']);
        }
        foreach (array_count_values($temp_nomor) as $key => $value){
            if($value > 1){
                $duplicate = $key;
            }
        }
        if($duplicate){
            $j = 0;
            for($i = 0; $i < count($array_result); $i++){
                if($array_result[$i]['question'] == $duplicate){
                    array_push($updated, $array_result[$i]['answer']);
                }
            }

            $temp_cache = Cache::get('result-'.$slug.'-'.$section.'-'.$id_user);
            if($temp_cache){
                for($i = 0; $i < count($temp_cache); $i++){
                    if($temp_cache[$i]['question'] == $duplicate){
                        $temp_cache[$i]['answer'] = $updated[1]; //index 1 jawaban baru, 0 jawaban lama
                        $status = 'update';
                    }
                }
                $array_result = $temp_cache; //replace jawaban
            }
        }

        /*
            batas akhir edit array jawaban
        */

        // pakah semua soal sudah terjawab, jika belum simpan ke cache (jlh cache <= soal), jika sudah stop.
        if(Cache::get('result-count-'.$slug.'-'.$section.'-'.$id_user)+1 <= $length_question){
            Cache::put('result-'.$slug.'-'.$section.'-'.$id_user, $array_result, $expiresAt);
            Cache::put('result-count-'.$slug.'-'.$section.'-'.$id_user, count($array_result), $expiresAt);
        }

        if(count($array_result)+1 > $length_question) {
            return 'finish';
            // return redirect('/survey/save-cache-to-db/'.$slug.'/'.$section);
        }else{
            return count($array_result);
        }

    }

    public function saveCacheToDataResult($slug, $section){
        if(!Entrust::can('take-test')){
             return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $user_id     = Auth::user()->id;
        $info_survey = $this -> survey -> get_info_survey_by_slug($slug);
        $survey_id   = $info_survey['id'];
        $info_section= $this -> survey -> get_info_section($section);

        if(Cache::has('cache-result-'.$slug.'-'.$section.'-'.$user_id)){
            if(Cache::has('result-count-'.$slug.'-'.$section.'-'.$user_id)){
                $save = false;
                $cache_jawaban  = Cache::get('result-'.$slug.'-'.$section.'-'.$user_id);
                $x = 0;
                foreach($cache_jawaban as $key){
                    foreach ($key['answer'] as $keys) {
                        // $data[$x]['user_id']        = $user_id;
                        $number = $this -> question -> get_number_question($key['question']);
                        $data[$x]['number']         = $number;
                        $data[$x]['id_answer']      = $keys;
                        $data[$x]['id_question']    = $key['question'];
                        $data[$x]['point']          = $this -> option_answer -> get_point_of_answer($data[$x]['id_answer']);
                        $data[$x]['timer']          = $key['timer'];
                        $x++;
                    }
                }
                $raw_input['user_id']       = $user_id;
                $raw_input['section_id']    = $section;
                $raw_input['survey_id']     = $survey_id;
                $raw_input['raw_input']     = json_encode($data);
                $raw_input['created_at']    = Carbon::now('Asia/Jakarta');

                $save = $this -> question -> save_answer($raw_input);
                if($save == true){
                    $this -> survey -> save_history_section($user_id, $section);
                    $duration               = $this -> survey -> get_temp($survey_id);
                    $this -> survey -> delete_temp_survey($slug);
                    $log['activity']        = 'Menyelesaikan section '.$info_section['name'];
                    $log['user_id']         = $user_id;
                    $log['reference_id']    = $survey_id;
                    $log['ip']              = \Request::ip();
                    $log['duration']        = $duration;
                    $log['created_at']      = Carbon::now();
                    $log['user_agent']      = \Request::header('User-Agent');
                    save_activity($log);
                    Cache::forget('result-count-'.$slug.'-'.$section.'-'.$user_id);
                    Cache::forget('cache-result-'.$slug.'-'.$section.'-'.$user_id);
                    Cache::forget('result-'.$slug.'-'.$section.'-'.$user_id);
                    return 'true';
                }else{
                    return 'false';
                }
            }
        }else{ //kondisi jika waktu habis dan user belum sama sekali belum sempat menjawab
            $this -> survey -> save_history_section($user_id, $section);
            $duration               = $this -> survey -> get_temp($survey_id);
            $this -> survey -> delete_temp_survey($slug);
            $log['activity']        = 'Menyelesaikan section '.$info_section['name'];
            $log['user_id']         = $user_id;
            $log['reference_id']    = $survey_id;
            $log['ip']              = \Request::ip();
            $log['duration']        = $duration;
            $log['created_at']      = Carbon::now();
            $log['user_agent']      = \Request::header('User-Agent');
            save_activity($log);
            Cache::forget('result-count-'.$slug.'-'.$section.'-'.$user_id);
            Cache::forget('cache-result-'.$slug.'-'.$section.'-'.$user_id);
            Cache::forget('result-'.$slug.'-'.$section.'-'.$user_id);

            return 'true';
        }

    }

    public function saveToReportWPA($slug, $section, $id_user){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey= $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey  = $info_survey['id'];
        $info_user = DB::table('profiles') -> where('user_id', '=', Auth::user() -> id) -> first();


        $result = $this -> result -> get_result_wpa($id_survey);
        $result_most = $result['most'];
        $result_lest = $result['lest'];

        $data_most['d'] = 0;
        $data_most['i'] = 0;
        $data_most['s'] = 0;
        $data_most['c'] = 0;
        $data_most['x'] = 0;
        $data_lest['d'] = 0;
        $data_lest['i'] = 0;
        $data_lest['s'] = 0;
        $data_lest['c'] = 0;
        $data_lest['x'] = 0;

        //hitung report most
        foreach ($result_most as $most) {
            if($most == 'D'){
                $data_most['d'] ++;
            }elseif($most == 'I'){
                $data_most['i'] ++;
            }elseif($most == 'S'){
                $data_most['s'] ++;
            }elseif($most == 'C'){
                $data_most['c'] ++;
            }elseif($most == '*'){
                $data_most['x'] ++;
            }
        }
        $user_report['most_d']  =   $data_most['d'];
        $user_report['most_i']  =   $data_most['i'];
        $user_report['most_s']  =   $data_most['s'];
        $user_report['most_c']  =   $data_most['c'];

        $grafik_most['D']       = WPAMostD($data_most['d']);
        $grafik_most['I']       = WPAMostI($data_most['i']);
        $grafik_most['S']       = WPAMostS($data_most['s']);
        $grafik_most['C']       = WPAMostC($data_most['c']);

        //hitung report lest
        foreach ($result_lest as $lest) {
            if($lest == 'D'){
                $data_lest['d'] ++;
            }elseif($lest == 'I'){
                $data_lest['i'] ++;
            }elseif($lest == 'S'){
                $data_lest['s'] ++;
            }elseif($lest == 'C'){
                $data_lest['c'] ++;
            }elseif($lest == '*'){
                $data_lest['x'] ++;
            }
        }
        $user_report['lest_d']  = $data_lest['d'];
        $user_report['lest_i']  = $data_lest['i'];
        $user_report['lest_s']  = $data_lest['s'];
        $user_report['lest_c']  = $data_lest['c'];

        $grafik_lest['D']       = WPALeastD($data_lest['d']);
        $grafik_lest['I']       = WPALeastI($data_lest['i']);
        $grafik_lest['S']       = WPALeastS($data_lest['s']);
        $grafik_lest['C']       = WPALeastC($data_lest['c']);

        //hitung report change
        $data_change['d']       = $data_most['d'] - $data_lest['d'];
        $data_change['i']       = $data_most['i'] - $data_lest['i'];
        $data_change['s']       = $data_most['s'] - $data_lest['s'];
        $data_change['c']       = $data_most['c'] - $data_lest['c'];
        $data_change['x']       = $data_most['x'] + $data_lest['x'];

        //untuk kebutuhan hasil wpa
        $grafik_change['D']       = WpaChangeD($data_change['d']);
        $grafik_change['I']       = WpaChangeI($data_change['i']);
        $grafik_change['S']       = WpaChangeS($data_change['s']);
        $grafik_change['C']       = WpaChangeC($data_change['c']);

        //cek apakah grafik change tight
        $tight_change = array_filter($grafik_change, 'filterIsTight');
        //jika grafik change tight, cek grafik most
        if(count($tight_change) == 4){
            $tight_most = array_filter($grafik_most, 'filterIsTight');
            //jika grafik most tight, cek grafik lest
            if(count($tight_most) == 4){
                $filter_garis_nol   = array_filter($grafik_lest, 'filterGarisNol');
                arsort($filter_garis_nol); //sorting berdasarkan nilai terbesar
            }else{ //jika tidak tight, ambil kesimpulan grafik most di atas garis nol
                $filter_garis_nol   = array_filter($grafik_most, 'filterGarisNol');
                arsort($filter_garis_nol); //sorting berdasarkan nilai terbesar
            }
            $is_tight = true;
        }else{ //jika tidak tight, ambil kesimpulan grafik change di atas garis nol
            $filter_garis_nol   = array_filter($grafik_change, 'filterGarisNol');
            arsort($filter_garis_nol); //sorting berdasarkan nilai terbesar
            $is_tight = false;
        }
        $i = 0;
        $data['hasil']    = '';
        // dd($filter_garis_nol);
        if($filter_garis_nol){
            foreach($filter_garis_nol as $k => $v){
                //untuk pencarian perilaku kerja, max 2 huruf
                if($i < 2){
                    $column_job[$i] = $k;
                }
                $data['hasil']   .= $k;
                $column_lookup[$i] = $k;
                $i++;
            }
            if($is_tight == true){
                $data['hasil'] .= " (tight)";
            }
        }else{
            $data['hasil'] = "Under Shift";
        }

        if(strlen($data['hasil']) == 4){
            $data['hasil'] = "Upper Shift";
        }


        $user_report['change_d']    = $data_change['d'];
        $user_report['change_i']    = $data_change['i'];
        $user_report['change_s']    = $data_change['s'];
        $user_report['change_c']    = $data_change['c'];
        $user_report['hasil']       = $data['hasil'];

        //proses update data pada tabel user_report
        $this -> result -> update_user_report($user_report, $info_survey['id_project']);
        $rekap_wpa['user_id']       = Auth::user() -> id;
        if($data['hasil'] != "Under Shift" && $data['hasil'] != "Upper Shift"){
            //lookup semua data laporan berdasarkan hasil
            $tipe_kepribadian           = $this -> result -> get_lookup_tipe_kepribadian($data['hasil']);
            $deskripsi_pertama            = rand(1, $tipe_kepribadian['count']-1)+1;
            if($deskripsi_pertama == 1){
                $deskripsi_pertama = rand(1, $tipe_kepribadian['count']-2)+2;
            }

            $lookup_desc                = 'desc'.$deskripsi_pertama.' as detail';
            $lookup_type                = $tipe_kepribadian['type'];
            $uraian_kepribadian         = $this -> result -> get_lookup_uraian_kepribadian($lookup_type, $lookup_desc);
            $karakteristik_umum         = $this -> result -> get_lookup_karakteristik('lookup_wpa_karakteristik_umum', $column_job);
            $karakteristik_pekerjaan    = $this -> result -> get_lookup_karakteristik('lookup_wpa_karakteristik_pekerjaan', $column_lookup);
            $kekuatan                   = $this -> result -> get_lookup_karakteristik('lookup_wpa_kekuatan', $column_lookup[0]);
            $kelemahan                  = $this -> result -> get_lookup_karakteristik('lookup_wpa_kelemahan', $column_lookup[0]);
            $perilaku_kerja_kekuatan    = $this -> result -> get_lookup_pos_neg('lookup_wpa_perilaku_kerja', $column_job, 'kekuatan');
            $perilaku_kerja_kelemahan   = $this -> result -> get_lookup_pos_neg('lookup_wpa_perilaku_kerja', $column_job, 'kelemahan');
            $suasana_emosi_kekuatan     = $this -> result -> get_lookup_pos_neg('lookup_wpa_suasana_emosi', $column_job, 'kekuatan');
            $suasana_emosi_kelemahan    = $this -> result -> get_lookup_pos_neg('lookup_wpa_suasana_emosi', $column_job, 'kelemahan');
            $kepribadian                = '<p>'.$info_user->first_name.' '.$uraian_kepribadian['detail'].'</p>'.$uraian_kepribadian['description'];

            $i = 0;//karakteristik umum
            foreach ($karakteristik_umum as $key => $value) {
                foreach ($value as $keys => $values) {
                    $k_umum[$i] = $values;
                    $i++;
                }
            }

            $i = 0;//perilaku kerja (kekuatan)
            foreach ($perilaku_kerja_kekuatan as $key => $value) {
                foreach ($value as $keys => $values) {
                    $perilaku_kekuatan[$i] = $values;
                    $i++;
                }
            }

            $i = 0;//perilaku kerja (kelemahan)
            foreach ($perilaku_kerja_kelemahan as $key => $value) {
                foreach ($value as $keys => $values) {
                    $perilaku_kelemahan[$i] = $values;
                    $i++;
                }
            }

            $i = 0;//suasana emosi (kekuatan)
            foreach ($suasana_emosi_kekuatan as $key => $value) {
                foreach ($value as $keys => $values) {
                    $emosi_kekuatan[$i] = $values;
                    $i++;
                }
            }

            $i = 0;//suasana emosi (kelemahan)
            foreach ($suasana_emosi_kelemahan as $key => $value) {
                foreach ($value as $keys => $values) {
                    $emosi_kelemahan[$i] = $values;
                    $i++;
                }
            }

            $i = 0;//karakteristik pekerjaan
            foreach ($karakteristik_pekerjaan as $key => $value) {
                foreach ($value as $keys => $values) {
                    $k_pekerjaan[$i] = $values;
                    $i++;
                }
            }

            $i = 0;//kekuatan
            foreach ($kekuatan as $key => $value) {
                foreach ($value as $keys => $values) {
                    $kuat[$i] = $values;
                    $i++;
                }
            }

            $i = 0;//kelemahan
            foreach ($kelemahan as $key => $value) {
                foreach ($value as $keys => $values) {
                    $lemah[$i] = $values;
                    $i++;
                }
            }

            //simpan data ke report_wpa_detail, encode dulu variabel array untuk masuk ke database
            $json_k_umum            = json_encode($k_umum);
            $json_perilaku_kekuatan = json_encode($perilaku_kekuatan);
            $json_perilaku_kelemahan= json_encode($perilaku_kelemahan);
            $json_emosi_kekuatan    = json_encode($emosi_kekuatan);
            $json_emosi_kelemahan   = json_encode($emosi_kelemahan);
            $json_k_pekerjaan       = json_encode($k_pekerjaan);
            $json_kuat              = json_encode($kuat);
            $json_lemah             = json_encode($lemah);

            $rekap_wpa['uraian_kepribadian']        = $kepribadian;
            $rekap_wpa['karakteristik_umum']        = $json_k_umum;
            $rekap_wpa['perilaku_kerja_kekuatan']   = $json_perilaku_kekuatan;
            $rekap_wpa['perilaku_kerja_kelemahan']  = $json_perilaku_kelemahan;
            $rekap_wpa['suasana_emosi_kekuatan']    = $json_emosi_kekuatan;
            $rekap_wpa['suasana_emosi_kelemahan']   = $json_emosi_kelemahan;
            $rekap_wpa['karakteristik_pekerjaan']   = $json_k_pekerjaan;
            $rekap_wpa['kekuatan']                  = $json_kuat;
            $rekap_wpa['kelemahan']                 = $json_lemah;
            $rekap_wpa['survey_id']                 = $id_survey;
        }else{
            $lookup_type                = $data['hasil'];
            $rekap_wpa['uraian_kepribadian']        = '-';
            $rekap_wpa['karakteristik_umum']        = '-';
            $rekap_wpa['perilaku_kerja_kekuatan']   = '-';
            $rekap_wpa['perilaku_kerja_kelemahan']  = '-';
            $rekap_wpa['suasana_emosi_kekuatan']    = '-';
            $rekap_wpa['suasana_emosi_kelemahan']   = '-';
            $rekap_wpa['karakteristik_pekerjaan']   = '-';
            $rekap_wpa['kekuatan']                  = '-';
            $rekap_wpa['kelemahan']                 = '-';
            $rekap_wpa['survey_id']                 = '-';
        }

        //simpan data ke report_wpa, encode dulu variabel array untuk masuk ke database
        $json_most  = json_encode($data_most);
        $json_lest  = json_encode($data_lest);
        $json_change= json_encode($data_change);
        $this -> result -> save_report_wpa($json_most, $json_lest, $json_change, $id_survey, $data['hasil'], $lookup_type);


        $this -> result -> insert_to_rekap_wpa($rekap_wpa);
        $this -> survey -> survey_complete($slug);
        $duration = $this -> survey -> get_temp($id_survey);
        $this -> survey -> delete_temp_survey($slug);

        $log['activity']        = 'Menyelesaikan survey '.$info_survey['title'];
        $log['user_id']         = $id_user;
        $log['reference_id']    = $id_survey;
        $log['ip']              = \Request::ip();
        $log['duration']        = $duration;
        $log['created_at']      = Carbon::now();
        $log['user_agent']      = \Request::header('User-Agent');
        save_activity($log);
        //wpa
        $complete = check_project_complete($info_survey['id_project'], $id_user);
        if($complete == true)
        {
          $this->completeEmail(Auth::user() -> email,$info_survey['id_project']);
        }
        // Cache::flush();
        Cache::forget('result-id-type-question-'.$slug.'-'.$id_user);
        Cache::forget('cache-result-wpa-'.$slug.'-'.$id_user);
        return 'true';
    }

    public function getQuestionWPA($slug, $section){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey        = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey          = $info_survey['id'];
        $used_timer         = $this -> survey -> get_temp($id_survey);
        $expiresAt      = Carbon::now()->addMinutes(60);
        //ambil info section.
        $section_info   = $this -> survey -> get_info_section($section);
        $data['slug']   = $slug;
        $data['instruction']    = $section_info['instruction'];
        $data['section_name']   = $section_info['name'];
        $data['section_id']     = $section_info['id'];
        $data['user_id']        = Auth::user() -> id;

        Cache::put('wpa-dummy', 'yes', $expiresAt);

        if(Cache::has('cache-'.$slug.'-'.$data['user_id'])){
            $data['timer_survey'] = Cache::get('timer-survey-'.$slug.'-'.$data['user_id']);
            $data['survey_title'] = Cache::get('survey-title-'.$slug.'-'.$data['user_id']);
            $data['id_survey']    = Cache::get('id-survey-'.$slug.'-'.$data['user_id']);
            $data['number']       = Cache::get('number-'.$slug.'-'.$data['user_id']);
            $data['content']      = Cache::get('question-'.$slug.'-'.$data['user_id']);
            return view('survey.detail_wpa', compact('data', 'slug', 'section', 'used_timer'));
        }else{
            return redirect('/survey'.'/'.$slug.'/intro');
        }
    }

    public function setResultCacheWPA(Request $request){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $status         = 'add'; //untuk status edit atau tambah jawaban
        $expiresAt      = Carbon::now()->addMinutes(60);
        $number         = $request->input('number');
        $id_user        = $request->input('id_user');
        $slug           = $request->input('slug');
        $section        = $request->input('section');
        $key_most       = $request->input('key_most');
        $key_lest       = $request->input('key_lest');

        $result = array(
            'number'  => $number,
            'key_most'    => $key_most[0],
            'key_lest'    => $key_lest[0]
        );

        $array_result = array();
        if(Cache::has('cache-result-wpa-'.$slug.'-'.$id_user)){
            $array_result = Cache::get('result-'.$slug.'-'.$id_user);
        }else{
            Cache::put('cache-result-wpa-'.$slug.'-'.Auth::user()->id, 'yes', $expiresAt);
        }
        array_push($array_result, $result);

        /*
            batas awal proses pengecekan apakah ada nomor soal yang sama,
            jika sama artinya user edit/ganti jawaban
        */

        $duplicate       = null;
        $updated_most    = [];
        $updated_lest    = [];
        $temp_nomor      = [];
        $i = 0;
        foreach ($array_result as $key) {
            array_push($temp_nomor, $key['number']);
        }
        foreach (array_count_values($temp_nomor) as $key => $value){
            if($value > 1){
                $duplicate = $key; //tampung nomor yang sama (double)
            }
        }
        if($duplicate){
            $j = 0;
            for($i = 0; $i < count($array_result); $i++){
                if($array_result[$i]['number'] == $duplicate){
                    array_push($updated_most, $array_result[$i]['key_most']);
                    array_push($updated_lest, $array_result[$i]['key_lest']);
                }
            }
            $temp_cache = Cache::get('result-'.$slug.'-'.$id_user);
            if($temp_cache){
                for($i = 0; $i < count($temp_cache); $i++){
                    if($temp_cache[$i]['number'] == $duplicate){
                        $temp_cache[$i]['key_most'] = $updated_most[1]; //index 1 jawaban baru, 0 jawaban lama
                        $temp_cache[$i]['key_lest'] = $updated_lest[1]; //index 1 jawaban baru, 0 jawaban lama
                        $status = 'update';
                    }
                }
                $array_result = $temp_cache; //replace jawaban
            }
        }

        /*
            batas akhir edit array jawaban
        */

        //apakah semua soal sudah terjawab, jika belum simpan ke cache (jlh cache <= jlh soal), jika sudah stop.
        if(Cache::get('result-count-'.$slug.'-'.$id_user)+1 <= 24){
            Cache::put('result-'.$slug.'-'.$id_user, $array_result, $expiresAt);
            Cache::put('result-count-'.$slug.'-'.$id_user, count($array_result), $expiresAt);
        }

        if(count($array_result)+1 > 24) {
            return 'finish';
        }else{
            return count($array_result);
        }
    }

    public function saveCacheToDataResultWPA($slug, $section){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $user_id     = Auth::user()->id;
        $info_survey = $this -> survey -> get_info_survey_by_slug($slug);

        if(Cache::has('cache-result-wpa-'.$slug.'-'.$user_id)){
            if(Cache::has('result-count-'.$slug.'-'.$user_id)){
                $save = false;
                $cache_jawaban  = Cache::get('result-'.$slug.'-'.$user_id);
                $i = 0;
                foreach ($cache_jawaban as $key) {
                    $data[$i]['number']     = $key['number'];
                    $data[$i]['M']   = $key['key_most'];
                    $data[$i]['L']   = $key['key_lest'];

                    $i++;
                }
                $raw_input['survey_id']     = $info_survey['id'];
                $raw_input['section_id']    = $section;
                $raw_input['category_id']   = 3;
                $raw_input['user_id']       = $user_id;
                $raw_input['raw_input']     = json_encode($data);
                $save = $this -> result -> save_result_wpa_papi($raw_input);
                return 'true';
            }
        }else{
            return redirect('survey/save-report-wpa/'.$slug.'/'.$section);
        }

    }

    public function getQuestionPapi($slug, $section){
        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $id_survey      = $info_survey['id'];
        $used_timer     = $this -> survey -> get_temp($id_survey);
        $user_id        = Auth::user() -> id;

        //ambil info section.
        $section_info   = $this -> survey -> get_info_section($section);
        $data['slug']   = $slug;
        $data['instruction']    = $section_info['instruction'];
        $data['section_name']   = $section_info['name'];
        $data['section_id']     = $section_info['id'];
        $data['user_id']        = $user_id;

        //jika cache sudah ada, tampilkan data soal. jika belum balik ke intro utk set cache
        if(Cache::has('cache-'.$slug.'-'.$user_id)){
            $data['timer_survey'] = Cache::get('timer-survey-'.$slug.'-'.$user_id);
            $data['survey_title'] = Cache::get('survey-title-'.$slug.'-'.$user_id);
            $data['id_survey']    = Cache::get('id-survey-'.$slug.'-'.$user_id);
            $data['number']       = Cache::get('number-'.$slug.'-'.$user_id);
            $data['content']      = Cache::get('question-'.$slug.'-'.$user_id);

            return view('survey.detail_papi', compact('data', 'slug', 'section', 'used_timer'));
        }else{
            return redirect('survey/'.$slug.'/intro');
        }

    }

    public function setResultCachePapi(Request $request){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $status     = 'add'; //untuk status edit atau tambah jawaban
        $user_id    = Auth::user() -> id;
        $expiresAt  = Carbon::now()->addMinutes(60);
        $number     = $request->input('number');
        $slug       = $request->input('slug');
        $key        = $request->input('key');

        $result = array(
            'number'    => $number,
            'key'       => $key[0],
        );

        $array_result = array();
        if(Cache::has('cache-result-papi-'.$slug.'-'.$user_id)){
            $array_result = Cache::get('result-'.$slug.'-'.$user_id);
        }else{
            Cache::put('cache-result-papi-'.$slug.'-'.$user_id, 'yes', $expiresAt);
        }
        array_push($array_result, $result);

        /*
            batas awal proses pengecekan apakah ada nomor soal yang sama,
            jika sama artinya user edit/ganti jawaban
        */

        $duplicate  = null;
        $updated    = [];
        $temp_nomor = [];
        $i = 0;
        foreach ($array_result as $key) {
            array_push($temp_nomor, $key['number']);
        }
        foreach (array_count_values($temp_nomor) as $key => $value){
            if($value > 1){
                $duplicate = $key;
            }
        }
        if($duplicate){
            $j = 0;
            for($i = 0; $i < count($array_result); $i++){
                if($array_result[$i]['number'] == $duplicate){
                    array_push($updated, $array_result[$i]['key']);
                }
            }

            $temp_cache = Cache::get('result-'.$slug.'-'.$user_id);
            if($temp_cache){
                for($i = 0; $i < count($temp_cache); $i++){
                    if($temp_cache[$i]['number'] == $duplicate){
                        $temp_cache[$i]['key'] = $updated[1]; //index 1 jawaban baru, 0 jawaban lama
                        $status = 'update';
                    }
                }
                $array_result = $temp_cache; //replace jawaban
            }
        }

        /*
            batas akhir edit array jawaban
        */

        //apakah semua soal sudah terjawab, jika belum simpan ke cache (jlh cache <= jlh soal), jika sudah stop.
        if((Cache::get('result-count-'.$slug.'-'.$user_id)+1) <= 90){
            Cache::put('result-'.$slug.'-'.$user_id, $array_result, $expiresAt);
            Cache::put('result-count-'.$slug.'-'.$user_id, count($array_result), $expiresAt);
        }
        if(count($array_result)+1 > 90) {
            return 'finish';
        }else{
            return count($array_result);
        }
    }

    public function saveCacheToDataResultPapi($slug, $section){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        $user_id     = Auth::user()->id;
        $info_survey = $this -> survey -> get_info_survey_by_slug($slug);
        if(Cache::has('cache-result-papi-'.$slug.'-'.$user_id)){
            if(Cache::has('result-count-'.$slug.'-'.$user_id)){
                $save = false;
                $cache_jawaban  = Cache::get('result-'.$slug.'-'.$user_id);
                $i = 0;
                foreach ($cache_jawaban as $key) {

                    $data[$i]['number'] = $key['number'];
                    $data[$i]['key']    = $key['key'];
                    $i++;

                }
                $raw_input['user_id']       = $user_id;
                $raw_input['section_id']    = $section;
                $raw_input['survey_id']     = $info_survey['id'];
                $raw_input['category_id']   = 4;
                $raw_input['raw_input']     = json_encode($data);
                $save = $this -> result -> save_result_wpa_papi($raw_input);
                return 'true';
            }
        }else{
            return redirect('survey/save-report-papi/'.$slug.'/'.$section);
        }

    }

    public function saveToReportPapi($slug, $section, $id_user){

        if(!Entrust::can('take-test')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $thankyou_text  = $info_survey['thankyou_text'];
        $id_survey      = $info_survey['id'];

        $result = $this -> result -> get_result_papi($id_survey);

        $data['user_id']    = $id_user;
        $data['survey_id']  = $id_survey;
        $data['created_at'] = Carbon::now('Asia/Jakarta');
        $data['N'] = 0;
        $data['G'] = 0;
        $data['A'] = 0;
        $data['L'] = 0;
        $data['P'] = 0;
        $data['I'] = 0;
        $data['T'] = 0;
        $data['V'] = 0;
        $data['X'] = 0;
        $data['S'] = 0;
        $data['B'] = 0;
        $data['O'] = 0;
        $data['R'] = 0;
        $data['D'] = 0;
        $data['C'] = 0;
        $data['Z'] = 0;
        $data['E'] = 0;
        $data['K'] = 0;
        $data['F'] = 0;
        $data['W'] = 0;
        //perhitungan masing-masing key
        foreach ($result as $key) {
            if($key == 'N'){
                $data['N'] ++;
            }elseif($key == 'G'){
                $data['G'] ++;
            }elseif($key == 'A'){
                $data['A'] ++;
            }elseif($key == 'L'){
                $data['L'] ++;
            }elseif($key == 'P'){
                $data['P'] ++;
            }elseif($key == 'I'){
                $data['I'] ++;
            }elseif($key == 'T'){
                $data['T'] ++;
            }elseif($key == 'V'){
                $data['V'] ++;
            }elseif($key == 'X'){
                $data['X'] ++;
            }elseif($key == 'S'){
                $data['S'] ++;
            }elseif($key == 'B'){
                $data['B'] ++;
            }elseif($key == 'O'){
                $data['O'] ++;
            }elseif($key == 'R'){
                $data['R'] ++;
            }elseif($key == 'D'){
                $data['D'] ++;
            }elseif($key == 'C'){
                $data['C'] ++;
            }elseif($key == 'Z'){
                $data['Z'] ++;
            }elseif($key== 'E'){
                $data['E'] ++;
            }elseif($key== 'K'){
                $data['K'] ++;
            }elseif($key== 'F'){
                $data['F'] ++;
            }elseif($key== 'W'){
                $data['W'] ++;
            }
        }
        $data['Z'] = 9 - $data['Z'];
        $data['K'] = 9 - $data['K'];

        //proses update data pada tabel user_report
        $user_report['N'] = $data['N'];
        $user_report['G'] = $data['G'];
        $user_report['A'] = $data['A'];
        $user_report['L'] = $data['L'];
        $user_report['P'] = $data['P'];
        $user_report['I'] = $data['I'];
        $user_report['T'] = $data['T'];
        $user_report['V'] = $data['V'];
        $user_report['X'] = $data['X'];
        $user_report['S'] = $data['S'];
        $user_report['B'] = $data['B'];
        $user_report['O'] = $data['O'];
        $user_report['R'] = $data['R'];
        $user_report['D'] = $data['D'];
        $user_report['C'] = $data['C'];
        $user_report['Z'] = $data['Z'];
        $user_report['E'] = $data['E'];
        $user_report['K'] = $data['K'];
        $user_report['F'] = $data['F'];
        $user_report['W'] = $data['W'];
        $this -> result -> update_user_report($user_report, $info_survey['id_project']);

        //simpan data ke report_papikostick
        $this -> result -> save_report_papi($data);

        $this -> survey -> survey_complete($slug);
        $duration = $this -> survey -> get_temp($id_survey);
        $this -> survey -> delete_temp_survey($slug);

        $log['activity']        = 'Menyelesaikan survey '.$info_survey['title'];
        $log['user_id']         = $data['user_id'];
        $log['reference_id']    = $id_survey;
        $log['ip']              = \Request::ip();
        $log['duration']        = $duration;
        $log['created_at']      = Carbon::now();
        $log['user_agent']      = \Request::header('User-Agent');
        save_activity($log);
        //wba
        $complete = check_project_complete($info_survey['id_project'], $data['user_id']);
        if($complete == true)
        {
          $this->completeEmail(Auth::user() -> email,$info_survey['id_project']);
        }
        // Cache::flush();
        Cache::forget('result-id-type-question-'.$slug.'-'.$id_user);
        Cache::forget('cache-result-papi-'.$slug.'-'.$id_user);
        return 'true';
    }

    public function saveTemp(Request $request){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }
        $data['survey_id']      = $request->input('survey_id');
        $data['question_id']    = $request->input('question_id');
        $data['user_id']        = $request->input('user_id');
        $data['timer']          = $request->input('timer');
        $data['slug']           = $request->input('slug');
        $data['section']        = $request->input('section');

        $save = $this -> survey -> save_temp($data);
        if($save == true){
            $this -> getQuestion($data['slug'], $data['section']);
        }
    }

    public function getHistorySurvey(){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }
        $history = $this -> survey -> getHistory();
        return view('survey.history', compact('history'));
    }

    public function deleteTempSurvey($slug){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }

        $this -> survey -> delete_temp_survey($slug);
    }

    public function cekSudah($slug, $section){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }
        $survey_info = $this -> survey -> get_info_survey_by_slug($slug);
        $id = $survey_info['id'];
        $data = DB::table('survey_report') -> where('section_id', '=', $section) -> where('user_id', '=', Auth::user() -> id) -> where('survey_id', '=', $id)-> first();
        if($data == null){
            return 'yet';
        }else{
            return 'done';
            // return view('survey.start_next_section');
        }
    }

    /*
        Batas awal fungsi tambahan dari HURA 1.5 ke atas (pakai project, optimasi, dll)
    */
    public function detail_project_user($slug){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }
        $project                = $this -> survey -> get_info_project_by_slug($slug);
        $id_project             = $project['id'];
        $id_peserta             = Auth::user() -> id;
        $tools                  = $this -> survey -> get_survey_wajib_by_project($id_project, $id_peserta);
        $i                      = 0;
        $detail['count_tools']  = $tools -> count();
        $detail['nama']                     = $project['name'];
        $detail['start_date']               = $project['start_date'];
        $detail['end_date']                 = $project['end_date'];
        $detail['tools'] = null;
        foreach($tools as $keys){
            $section    = $this -> survey -> get_first_section_on_survey($keys['id'], $id_peserta);
            $detail['tools'][$i]['title']       = $keys['title'];
            $detail['tools'][$i]['slug']        = $keys['slug'];
            $detail['tools'][$i]['instruction'] = $keys['instruction'];
            $detail['tools'][$i]['status']      = $keys['status'];
            $detail['tools'][$i]['is_random']   = $keys['is_random'];
            $detail['tools'][$i]['end_date']    = $keys['end_date'];
            $detail['tools'][$i]['section']     = $section['id'];
            $detail['tools'][$i]['category_survey_id']   = $keys['category_survey_id'];
            $i++;
        }
        return view('/user.home', compact('detail'));
    }

    public function completeSection($section){
        if(!Entrust::can('take-test')){
            return redirect('/home');
        }
        $user_id = Auth::user() -> id;
        $this -> survey -> save_history_section($user_id, $section);
        return 'true';
    }

    public function flush(){
        Cache::flush();
    }

    //======================================================= EMAIL =======================================================//


    //send email
    public function notifEmail($email,$password,$project_id)
    {
        if(!Entrust::can('manage-survey')){
            return redirect('/home');
        }
        $token = $email.'+'.$project_id;
        $info_project = $this -> survey -> get_info_project_by_id($project_id);
        $project_name = $info_project['name'];
        $start_date = $info_project['start_date'];
        $end_date = $info_project['end_date'];
        $token =  encrypt_decrypt("encrypt",$token);
        $jlh_tools = $this -> survey -> get_list_survey_by_project($project_id) -> count();
        $corporate = $this -> corporate -> getById(Auth::user()->profile->id_corporate);
        $corporate_name = $corporate['name'];
        $data = [
            "token"=>$token,
            "subject"=>"Undangan Assessment",
            "view"=>"emails.notif",
            "project_name"=>$project_name,
            "start_date"=>$start_date,
            "end_date"=>$end_date,
            "jlh_tools"=>$jlh_tools,
            "corporate_name"=>$corporate_name,
            "email"=>$email,
            "password"=>$password

        ];
        Mail::to($email)->send(new Notification($data));
    }

    public function notifEmailpilihan($email,$project_id)
    {
        if(!Entrust::can('manage-survey')){
            return redirect('/home');
        }
        $token = $email.'+'.$project_id;
        $info_project = $this -> survey -> get_info_project_by_id($project_id);
        $project_name = $info_project['name'];
        $start_date = $info_project['start_date'];
        $end_date = $info_project['end_date'];
        $token =  encrypt_decrypt("encrypt",$token);
        $jlh_tools = $this -> survey -> get_list_survey_by_project($project_id) -> count();
        $corporate = $this -> corporate -> getById(Auth::user()->profile->id_corporate);
        $corporate_name = $corporate['name'];
        $data = [
            "token"=>$token,
            "subject"=>"Undangan Assessment",
            "view"=>"emails.notif-pilihan",
            "project_name"=>$project_name,
            "start_date"=>$start_date,
            "end_date"=>$end_date,
            "jlh_tools"=>$jlh_tools,
            "corporate_name"=>$corporate_name
        ];
        Mail::to($email)->send(new NotificationPilihan($data));
    }

    public function completeEmail($email,$project_id)
    {
      // $email2 = \Config::get('app.admin_survey');
      // $token = $email.'+'.$project_id;
      // $token =  encrypt_decrypt("encrypt",$token);
      // //user
      // $project =DB::table("project")->where(["id"=>$project_id])->first();
      //
      // $data = [
      //   "user"=>$email,
      //   "project"=>$project->name,
      //   "subject"=>"Pemberitahuan",
      //   "view"=>"emails.finish"
      // ];
      // Mail::to($email2)->send(new CompleteTools($data));

    }

    public function email_reschedule($project_id){
        if(!Entrust::can('manage-survey')){
            return redirect('/home');
        }
        $info_project   = $this -> survey -> get_info_project_by_id($project_id);
        $project_name   = $info_project['name'];
        $start_date     = $info_project['start_date'];
        $end_date       = $info_project['end_date'];
        $corporate      = $this -> corporate -> getById(Auth::user()->profile->id_corporate);
        $corporate_name = $corporate['name'];

        $peserta        = $this -> survey -> get_peserta_project($project_id);
        foreach ($peserta as $key) {
            if($key['is_done'] == 0){
                $email = $key['email'];
                $token = $email.'+'.$project_id;
                $token = encrypt_decrypt("encrypt",$token);
                $data = [
                    "token"=>$token,
                    "subject"=>"Perubahan Jadwal Online Assessment",
                    "view"=>"emails.reschedule",
                    "project_name"=>$project_name,
                    "start_date"=>$start_date,
                    "end_date"=>$end_date,
                    "corporate_name"=>$corporate_name,

                ];
                // Mail::to($email)->send(new Reschedule($data));
            }else{
                echo "jangan masuk ".$key['email']."<br/>";
            }
        }
    }

    // ====================================================== Anggoro ========================================================//
    public function list_report_user()
    {
        if(Auth::check() && !Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }
        //check status penyelesaian survey
        $this -> current_project_status();
        $this -> current_survey_status();

        $projects = $this -> survey -> get_all_project();
        $col_heads = [];
        array_push($col_heads, 'Id');
        array_push($col_heads, 'Nama Project');
        array_push($col_heads, 'Tanggal Awal');
        array_push($col_heads, 'Tanggal Akhir');
        array_push($col_heads, 'Status');
        array_push($col_heads, 'Pilihan');

        $rowset = null;
        $i = 0;

        if($projects){
            foreach($projects as $key){
                $rowset[$i]['id'] = $key -> id;
                // $rowset[$i]['nama_project'] = "<a href='/project/detail/".$key -> id."'><strong> <span class='glyphicon glyphicon-search'></span> ".$key -> name."</strong></a>";
                $rowset[$i]['nama_project'] = $key->name;
                $rowset[$i]['tanggal_awal'] = $key -> start_date;
                $rowset[$i]['tanggal_akhir'] = $key -> end_date;
                if($key -> is_done == 0){
                    $rowset[$i]['status'] = "<span class='label label-warning'>Belum Berjalan</span>";
                }elseif($key -> is_done == 1){
                    $rowset[$i]['status'] = "<span class='label label-danger'>Selesai</span>";
                }elseif($key -> is_done == 2){
                    $rowset[$i]['status'] = "<span class='label label-success'>Sedang Berjalan</span>";
                }
                // $rowset[$i]['pilihan'] = "<a href='/project/standar_nilai/".$key->id."' class='btn btn-info btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Standar Nilai Aspek'><span class='glyphicon glyphicon-pencil'></span></a> ";
                // $rowset[$i]['pilihan'] .= "<a href='/project/assign/".$key->id."' class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Assign Peserta'><span class='glyphicon glyphicon-envelope'></span></a> ";
                // $rowset[$i]['pilihan'] .= "<a href='/project/edit/".$key->slug."' class='edit-modal btn btn-warning btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Edit'><span class='glyphicon glyphicon-edit'></span></a>";
                // $rowset[$i]['pilihan'] .= "<a href='/project/hapus/".$key->slug."' class='edit-modal btn btn-danger btn-xs' data-toggle='tooltip' data-placement='top' title='' data-original-title='Hapus' onclick='return confirm(`Apakah Anda yakin menghapus project ini?`)'><span class='glyphicon glyphicon-trash'></span></a>";
                $i++;
            }
        }
        return view('user_report.list_report_user', compact('col_heads', 'rowset'));
    }

    public function index_report_user($id, Request $request)
    {
        if(!Entrust::can('manage-survey')){
            return redirect('/home')->withErrors(trans('messages.permission_denied'));
        }

        $project = $this -> survey -> get_detail_project($id);

        $i = 0;
        foreach ($project as $key) {
            $detail['id']               = $key['id'];
            $detail['nama']             = $key['name'];
            $detail['deskripsi']        = $key['description'];
            $detail['tanggal_awal']     = $key['start_date'];
            $detail['tanggal_akhir']    = $key['end_date'];
            $detail['is_done']          = $key['is_done'];
            $detail['tools'][$i]['judul']       = $key['title'];
            $detail['tools'][$i]['id']          = $key['id_survey'];
            $detail['tools'][$i]['deskripsi']   = $key['instruction_survey'];
            $detail['tools'][$i]['status']      = $key['status_survey'];
            $detail['tools'][$i]['is_random']   = $key['is_random'];
            $detail['slugs'][$i]        = $key['slug_survey'];
            $i++;
        }

        switch ($detail['is_done']) {
            case '0':
                $status_project = "<span class='label label-warning'>Belum berjalan</span>";
                break;
            case '1':
                $status_project = "<span class='label label-danger'>Selesai</span>";
                break;
            default:
                $status_project = "<span class='label label-success'>Sedang berjalan</span>";
                break;
        }
        $peserta = $this -> survey -> get_peserta_project_laporan($detail['id']);
        $project_id = $detail['id'];

        $rowset = null;
        $i = 0;

        if(sizeof($peserta)>0){
            foreach($peserta as $key){

                $rowset[$i]['name'] = $key -> first_name;

                // Tambahan Anggoro
                    $rowset[$i]['email'] = $key-> user-> email;
                    $rowset[$i]['avatar'] = (is_null($key->user->profile->avatar) ? "default-avatar.png" : $key->user->profile->avatar);
                    $rowset[$i]['user_id'] = $key->id_user;
                    $rowset[$i]['tingkat_kecocokan'] = (is_null(get_tingkat_kecocokan($key->id_user, $project_id)) ? "Data belum tersedia" : get_tingkat_kecocokan($key->id_user, $project_id)."%");
                // End ofTambahan Anggoro

                if($key -> is_done == 0){
                    $rowset[$i]['status'] = "<span class='label label-danger'>Belum menyelesaikan</span>";
                    $rowset[$i]['status-test'] = 0;
                    $rowset[$i]['status-check'] = "<input type='checkbox' class='compare-cb' value=".$key->id_user." disabled>";
                    $rowset[$i]['status-test-user'] = "<h5>".$key->first_name."</h5>";
                    $rowset[$i]['print-all'] = "";
                }else{
                    $rowset[$i]['status'] = "<span class='label label-success'>Sudah menyelesaikan</span>";
                    $rowset[$i]['status-test'] = 1;
                    $rowset[$i]['status-check'] = "<input type='checkbox' class='compare-cb' value=".$key->id_user.">";
                    $rowset[$i]['status-test-user'] = "<h5><a href='/backend/report-user/detail/".$key->id_user."/".$project_id."'>".$key->first_name."</a></h5>";
                    $rowset[$i]['print-all'] = "<a href='/print/all/".$id."/".$key -> id_user."'><span class='label label-success' data-toggle='tooltip' data-placement='top' title='' data-original-title='Laporan Keseluruhan'><i class='fa fa-download' aria-hidden='true'></i> Report</span></a>";
                }

                //proses untuk munculkan ceklis survey yang dikerjakan
                $ceklis_survey = DB::table('user_survey') -> join('survey', 'survey.id', '=', 'user_survey.id_survey')
                                -> where('user_survey.id_project', '=', $id)
                                -> where('id_user', '=', $key -> id_user) -> get();
                $j = 0;
                foreach($ceklis_survey as $value){
                    switch ($value -> category_survey_id) {
                        case '1':
                            $survey         = "LAI";
                            $slugLAI        = $detail["slugs"][0];
                            $link_download  = '/print/gti/'.$slugLAI.'/'.$rowset[$i]["user_id"];
                            $link_view      = '/backend/report/gti/'.$slugLAI.'/'.$rowset[$i]["user_id"];
                            break;
                        case '2':
                            $survey         = "TIKI";
                            $slugTIKI       = $detail["slugs"][1];
                            $link_download  = '/print/tiki/'.$slugTIKI.'/'.$rowset[$i]["user_id"];
                            $link_view     = '/backend/report/tiki/'.$slugTIKI.'/'.$rowset[$i]["user_id"];
                            break;
                        case '3':
                            $survey         = "WPA";
                            $slugWPA        = $detail["slugs"][2];
                            $link_download  = '/print/wpa/'.$slugWPA.'/'.$rowset[$i]["user_id"];
                            $link_view      = '/backend/report/wpa/'.$slugWPA.'/'.$rowset[$i]["user_id"];
                            break;
                        case '4':
                            $survey         = "WBA";
                            $slugWBA        = $detail["slugs"][3];
                            $link_download  = '/print/wba/'.$slugWBA.'/'.$rowset[$i]["user_id"];
                            $link_view      = '/backend/report/kostick/'.$slugWBA.'/'.$rowset[$i]["user_id"];
                            break;
                    }
                    if($value -> is_done == 0){
                        $rowset[$i]['ceklis'][$j] = "<span class='label label-warning' data-toggle='tooltip' data-placement='top' title='' data-original-title='Belum'>".$survey."</span>";
                        $rowset[$i]['view'][$j] = null;
                    }else{
                        $rowset[$i]['ceklis'][$j] = "<a href='".$link_download."' id='detail_".$rowset[$i]['user_id']."'><span class='label label-success' data-toggle='tooltip' data-placement='top' title='' data-original-title='Download'> <i class='fa fa-download' aria-hidden='true'></i> ".$survey."</span></a>";
                        $rowset[$i]['view'][$j] = "<a href='".$link_view."' id='detail_".$rowset[$i]['user_id']."'><span class='label label-info' data-toggle='tooltip' data-placement='top' title='' data-original-title='Lihat'> <i class='fa fa-eye' aria-hidden='true'></i> ".$survey."</span></a>";
                    }
                    $j++;
                }

                $i++;
            }
        }
        return view("user_report.index-report-user", compact('detail', 'rowset', 'status_project'));
    }

    public function report_user($user_id, $project_id)
    {
        $aspeks = \App\AspekPsikologis::all();
        $user   = User::with('profile')->find($user_id);
        $fullName = $user->profile->first_name;

        $user_report = \App\HasilPeserta::select("id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["user_id"=> $user->id, "tujuan"=> $project_id])->first();
        $user_report_aspek = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $project_id])->first();

        if (sizeof($user_report_aspek) == 0) {
            $dataKalkulasi["user_id"]    = $user_id;
            $dataKalkulasi["project_id"] = $project_id;
            $dataKalkulasi["fullName"]   = $fullName;
            $kalkulasi = Kalkulasi::prosesKalkulasi($dataKalkulasi);

        }

        try {
            $user_report = \App\HasilPeserta::select("id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["user_id"=> $user->id, "tujuan"=> $project_id])->first();
            $user_report_aspek = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $project_id])->first();
            $json_nilai_aspek  = json_decode($user_report_aspek->nilai_aspek,true);
            //get survey_id
            $survey_id = Survey::where('id_project',$project_id)->where('category_survey_id',3)->value('id');
            $analisis = \App\ReportWPADetail::where(['user_id'=>$user->id, 'survey_id'=> $survey_id])->select("id", "kekuatan", "kelemahan", "karakteristik_pekerjaan")->first();
            $namaProject = Project::where('id',$project_id)->value('name');

            $data["user_id"] = $user->id;
            $data["project_id"] = $project_id;
            $data["nama"] = $user->profile->first_name;
            $data["email"] = "$user->email";
            $data["sign_up"] = $user->created_at;
            $data["last_login"] = $user->last_login;
            $data["avatar"] = (is_null($user->profile->avatar) ? "default-avatar.png" : $user->profile->avatar);
            $data["user_id"] = $user_id;
            $data["rekomendasi"] = (is_null($user_report['kriteria_psikograph_id']) ? null : $user_report->kriteria_psikograph);
            $data["kecocokan"] = get_tingkat_kecocokan($user->id, $project_id);
            $data["nilais"] = $json_nilai_aspek;
            $data["analisis"] = (is_null($user_report['uraians']) ? "Data belum tersedia" : $user_report['uraians']);
            $data["kelebihan"] = (is_null($analisis) ? [] : json_decode($analisis->kekuatan));
            $data["kelemahan"] = (is_null($analisis) ? [] : json_decode($analisis->kelemahan));
            $data["karakteristik"] = (is_null($analisis) ? [] : json_decode($analisis->karakteristik_pekerjaan));
            $data["hasilKriteria"] = null;
            $data["nama_project"] = $namaProject;

            $listJabatan = StandarNilai::get();
            $current_jabatan = StandarNilaiProject::where('project_id',$project_id)->value('standar_nilai_id');

            return view("user_report.report-user", compact("data", "aspeks","listJabatan","current_jabatan"));
        } catch (Exception $e) {
            return view('errors.404');
        }

    }

    public function compare_user($user_ids = null, $project_id = null)
    {
        $user_ids = explode("-", $user_ids);

        $aspeks = \App\AspekPsikologis::all();

        $users = User::whereIn('id' ,$user_ids)->get();

        if (is_null($users)) {
            $data = null;
        }else{
            foreach ($users as $user) {
                $up = \App\HasilPeserta::select("id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["user_id"=> $user->id, "tujuan"=> $project_id])->first();
                $usp = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $project_id])->first();

                if (sizeof($usp) == 0) {
                    $dataKalkulasi["user_id"]    = $user->id;
                    $dataKalkulasi["project_id"] = $project_id;
                    $kalkulasi = Kalkulasi::prosesKalkulasi($dataKalkulasi);
                }

                $user_report = \App\HasilPeserta::select("id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["user_id"=> $user->id, "tujuan"=> $project_id])->first();
                $user_report_aspek = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $project_id])->first();
                $json_nilai_aspek  = json_decode($user_report_aspek->nilai_aspek,true);
                //get survey_id
                $survey_id = Survey::where('id_project',$project_id)->where('category_survey_id',3)->value('id');
                $analisis = \App\ReportWPADetail::where(['user_id'=>$user->id, 'survey_id'=> $survey_id])->select("id", "kekuatan", "kelemahan", "karakteristik_pekerjaan")->first();

                $data[$user->id]["user_id"] = $user->id;
                $data[$user->id]["project_id"] = $project_id;
                $data[$user->id]["nama"] = $user->profile->first_name;
                $data[$user->id]["email"] = "$user->email";
                $data[$user->id]["sign_up"] = $user->created_at;
                $data[$user->id]["last_login"] = $user->last_login;
                $data[$user->id]["avatar"] = (is_null($user->profile->avatar) ? "default-avatar.png" : $user->profile->avatar);
                $data[$user->id]["rekomendasi"] = (is_null($user_report['kriteria_psikograph_id']) ? null : $user_report->kriteria_psikograph);
                $data[$user->id]["kecocokan"] = get_tingkat_kecocokan($user->id, $project_id);
                $data[$user->id]["nilais"] = $json_nilai_aspek;
                $data[$user->id]["analisis"] = (is_null($user_report['uraians']) ? "Data belum tersedia" : $user_report['uraians']);
                $data[$user->id]["kelebihan"] = (is_null($analisis) ? [] : json_decode($analisis->kekuatan));
                $data[$user->id]["kelemahan"] = (is_null($analisis) ? [] : json_decode($analisis->kelemahan));
                $data[$user->id]["karakteristik"] = (is_null($analisis) ? [] : json_decode($analisis->karakteristik_pekerjaan));
            }
        }
        return view("user_report.compare", compact("data", "aspeks"));
    }

    // ====================================================== Kurniawan ========================================================//

    public function print_report_user(Request $request)
    {
        $datas       = $request->all();
        $refUserId   = $datas['refUserIdPrint'];
        $projectId   = $datas['projectIdPrint'];
        $posisiId    = $datas['promosiIdPrint'];
        $rekomendasi = $datas['hasilKriteriaPrint'];
        $kecocokan   = $datas['totalNilaiPrint'];

        $user   = User::find($refUserId);
        $aspeks = \App\AspekPsikologis::all();

        $checkHasilPeserta = HasilPeserta::where('user_id',$refUserId)->where('tujuan',$projectId)->get();
        if (sizeof($checkHasilPeserta)>0) {
            $user_report = \App\HasilPeserta::select("id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["user_id"=> $refUserId, "tujuan"=> $projectId])->first();
            $user_report_aspek = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $projectId])->first();
            $json_nilai_aspek  = json_decode($user_report_aspek->nilai_aspek,true);
            //get survey_id
            $survey_id = Survey::where('id_project',$projectId)->where('category_survey_id',3)->value('id');
            $analisis = \App\ReportWPADetail::where(['user_id'=>$refUserId, 'survey_id'=> $survey_id])->select("id", "kekuatan", "kelemahan", "karakteristik_pekerjaan")->first();
            $projectName = Project::where('id',$projectId)->value('name');

            $data["user_id"]        = $user->id;
            $data["project_id"]     = $projectId;
            $data["nama"]           = $user->profile->first_name;
            $data["email"]          = "$user->email";
            $data["sign_up"]        = $user->created_at;
            $data["last_login"]     = $user->last_login;
            $data["avatar"]         = (is_null($user->profile->avatar) ? "default-avatar.png" : $user->profile->avatar);
            $data["user_id"]        = $refUserId;
            $data["rekomendasi"]    = null;
            $data["kecocokan"]      = $kecocokan;
            $data["nilais"]         = $json_nilai_aspek;
            $data["analisis"]       = (is_null($user_report['uraians']) ? "Data belum tersedia" : $user_report['uraians']);
            $data["kelebihan"]      = (is_null($analisis) ? [] : json_decode($analisis->kekuatan));
            $data["kelemahan"]      = (is_null($analisis) ? [] : json_decode($analisis->kelemahan));
            $data["karakteristik"]  = (is_null($analisis) ? [] : json_decode($analisis->karakteristik_pekerjaan));
            $data["hasilKriteria"]  = $rekomendasi;
            $data["projectName"]    = $projectName;
            $data["posisiId"]       = $posisiId;

            $listJabatan     = StandarNilai::get();
            $current_jabatan = StandarNilai::where('id',$posisiId)->value('nama_jabatan');

            // return view('pdf.print-report-tpa',compact("data", "aspeks","listJabatan","current_jabatan"));
            $pdf = PDF::loadView('pdf.print-report-tpa',compact("data", "aspeks","listJabatan","current_jabatan"));
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('javascript-delay', 900);
            $pdf->setOption('enable-smart-shrinking', true);
            return $pdf->download("Hasil TPA Peserta_".$data["nama"].".pdf");
        } else {
            return redirect()->back()->withErrors('Data Not Found');
        }
    }

    public function kalkulasi_report_user(Request $request)
    {
        $datas = $request->all();
        $refUserId  = $datas['refUserId'];
        $projectId  = $datas['projectId'];
        $posisiId   = $datas['posisi'];

        if ($posisiId=="default") {
            return redirect('backend/report-user/detail/'.$refUserId.'/'.$projectId)->withErrors('Data Not Found');
        } else {
            $user   = User::find($refUserId);
            $aspeks = \App\AspekPsikologis::all();

            $checkHasilPeserta = HasilPeserta::where('user_id',$refUserId)->where('tujuan',$projectId)->get();
            if (sizeof($checkHasilPeserta)>0) {
                $user_report = \App\HasilPeserta::select("id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["user_id"=> $refUserId, "tujuan"=> $projectId])->first();
                $user_report_aspek = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $projectId])->first();
                $json_nilai_aspek  = json_decode($user_report_aspek->nilai_aspek,true);
                //get survey_id
                $survey_id = Survey::where('id_project',$projectId)->where('category_survey_id',3)->value('id');
                $analisis  = \App\ReportWPADetail::where(['user_id'=>$refUserId, 'survey_id'=> $survey_id])->select("id", "kekuatan", "kelemahan", "karakteristik_pekerjaan")->first();
                $namaProject = Project::where('id',$projectId)->value('name');

                $kalkulasi   = Kalkulasi::hitungKecocokan($datas);
                $kecocokan   = $kalkulasi["totalNilaiJPM"];
                $rekomendasi = $kalkulasi["hasilKriteria" ];

                $data["user_id"]       = $user->id;
                $data["project_id"]    = $projectId;
                $data["nama"]          = $user->profile->first_name;
                $data["email"]         = "$user->email";
                $data["sign_up"]       = $user->created_at;
                $data["last_login"]    = $user->last_login;
                $data["avatar"]        = (is_null($user->profile->avatar) ? "default-avatar.png" : $user->profile->avatar);
                $data["user_id"]       = $refUserId;
                $data["rekomendasi"]   = null;
                $data["kecocokan"]     = $kecocokan;
                $data["nilais"]        = $json_nilai_aspek;
                $data["analisis"]      = (is_null($user_report['uraians']) ? "Data belum tersedia" : $user_report['uraians']);
                $data["kelebihan"]     = (is_null($analisis) ? [] : json_decode($analisis->kekuatan));
                $data["kelemahan"]     = (is_null($analisis) ? [] : json_decode($analisis->kelemahan));
                $data["karakteristik"] = (is_null($analisis) ? [] : json_decode($analisis->karakteristik_pekerjaan));
                $data["hasilKriteria"] = $rekomendasi;
                $data["nama_project"]  = $namaProject;

                $listJabatan     = StandarNilai::get();
                $current_jabatan = $posisiId;
                return view("user_report.report-user", compact("data", "aspeks","listJabatan","current_jabatan"));
            } else {
                return redirect()->back()->withErrors('Data Not Found');
            }
        }
    }

    public function update_uraian_user(Request $request)
    {
        $posts        = $request->all();
        $idUserUraian = $posts['idUserUraian'];
        $idProject    = $posts['idProject'];
        $uraian       = $posts['uraian'];
        $detail       = HasilPeserta::where('user_id',$idUserUraian)->where('tujuan',$idProject)->first();
        if (!is_null($detail)) {
            DB::beginTransaction();
            try {
                $detail->uraians = $uraian;
                $detail->update();
                DB::commit();
                return redirect('backend/report-user/detail/'.$idUserUraian.'/'.$idProject)->withSuccess('Uraian Analisis Peserta Berhasil Diubah');
            } catch (\Exception $e) {
                DB::rollback();
                return redirect('backend/report-user/detail/'.$idUserUraian.'/'.$idProject)->withErrors('Uraian Analisis Tidak Berhasil Diubah');
            }
        } else{
            return redirect()->back()->withErrors('Data Not Found');
        }
    }

    public function print_report_user_depan($user_id, $project_id)
    {
        $refUserId = $user_id;
        $projectId = $project_id;

        $user   = User::find($refUserId);
        $aspeks = \App\AspekPsikologis::all();

        $user_report = HasilPeserta::select("id","user_id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["user_id"=> $refUserId, "tujuan"=> $projectId])->first();
        if (sizeof($user_report)>0) {
            $user_report_aspek = \App\UserReportAspek::select("id", "aspek_id", "nilai_aspek")->where(["user_id"=> $user->id, "project_id"=> $projectId])->first();
            $json_nilai_aspek  = json_decode($user_report_aspek->nilai_aspek,true);
            //get survey_id
            $survey_id      = Survey::where('id_project',$projectId)->where('category_survey_id',3)->value('id');
            $analisis       = \App\ReportWPADetail::where(['user_id'=>$refUserId, 'survey_id'=> $survey_id])->select("id", "kekuatan", "kelemahan", "karakteristik_pekerjaan")->first();
            $projectName    = Project::where('id',$projectId)->value('name');
            $promosiIdPrint = StandarNilaiProject::where('project_id',$project_id)->value('standar_nilai_id');

            $posisiId    = $promosiIdPrint;
            $rekomendasi = $user_report['kriteria_psikograph_id'];
            $kecocokan   = $user_report['kecocokan'];

            $data["user_id"]       = $user->id;
            $data["project_id"]    = $projectId;
            $data["nama"]          = $user->profile->first_name;
            $data["email"]         = "$user->email";
            $data["sign_up"]       = $user->created_at;
            $data["last_login"]    = $user->last_login;
            $data["avatar"]        = (is_null($user->profile->avatar) ? "default-avatar.png" : $user->profile->avatar);
            $data["user_id"]       = $refUserId;
            $data["rekomendasi"]   = null;
            $data["kecocokan"]     = $kecocokan;
            $data["nilais"]        = $json_nilai_aspek;
            $data["analisis"]      = (is_null($user_report['uraians']) ? "Data belum tersedia" : $user_report['uraians']);
            $data["kelebihan"]     = (is_null($analisis) ? [] : json_decode($analisis->kekuatan));
            $data["kelemahan"]     = (is_null($analisis) ? [] : json_decode($analisis->kelemahan));
            $data["karakteristik"] = (is_null($analisis) ? [] : json_decode($analisis->karakteristik_pekerjaan));
            $data["hasilKriteria"] = $rekomendasi;
            $data["projectName"]   = $projectName;
            $data["posisiId"]      = $posisiId;

            $listJabatan     = StandarNilai::get();
            $current_jabatan = StandarNilai::where('id',$posisiId)->value('nama_jabatan');

            // return view('pdf.print-report-tpa',compact("data", "aspeks","listJabatan","current_jabatan"));
            $pdf = PDF::loadView('pdf.print-report-tpa',compact("data", "aspeks","listJabatan","current_jabatan"));
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('javascript-delay', 900);
            $pdf->setOption('enable-smart-shrinking', true);
            return $pdf->stream("Hasil TPA Peserta_".$data["nama"].".pdf");

        } else {
            return redirect()->back()->withErrors('Data Not Found');
        }
    }

    public function update_nilai_aspek(Request $request)
    {
        $params     = $request->all();
        $userId     = $params['idUser'];
        $jabatanId  = $params['idJabatan'];
        if ($jabatanId=="") {
            $jabatanId = NULL;
        }
        $aspekId    = $params['idAspek'];
        $nilaiAspek = intval($params['options'])+1;
        $projectId  = $params['projectId'];

        $checkData = UserReportAspek::where(['user_id'=>$userId, 'project_id'=>$projectId])->first();
        if (sizeof($checkData)>0) {
            $nilais     = $checkData['nilai_aspek'];
            $nilaisOld  = json_decode($nilais,true);
            $replaceNew = array($aspekId=>$nilaiAspek);
            $nilaisNew  = array_replace($nilaisOld, $replaceNew);
            $json_nilai_aspek = json_encode($nilaisNew);

            DB::beginTransaction();
            try {
                //save perubahan nilai ke dalam tabel user_report_aspeks
                $checkData->nilai_aspek = $json_nilai_aspek;
                $checkData->save();
                $datas = [];
                $datas['posisi']    = $jabatanId;
                $datas['refUserId'] = $userId;
                $datas['projectId'] = $projectId;
                $kalkulasi   = Kalkulasi::hitungKecocokan($datas);
                $kecocokan   = $kalkulasi["totalNilaiJPM"];
                $rekomendasi = $kalkulasi["hasilKriteria" ];

                $hasilPesertas = HasilPeserta::where(['user_id'=>$userId,'tujuan'=>$projectId])->first();
                if (sizeof($hasilPesertas)>0) {
                    $uraians = Kalkulasi::updateUraianByScoreEdited($nilaisNew,$userId,$projectId);
                    //save data terbaru
                    $hasilPesertas->kecocokan = $kecocokan;
                    $hasilPesertas->kriteria_psikograph_id = $rekomendasi;
                    $hasilPesertas->uraians = $uraians;
                    $hasilPesertas->save();
                } else {
                    DB::rollback();
                    // return redirect()->back()->withErrors('Score Data Not Saved, Something Wrong!');
                    return redirect('backend/report-user/detail/'.$userId.'/'.$projectId)->withErrors('Score Data Not Saved, Something Wrong!');
                }

                DB::commit();
                // return redirect()->back()->withSuccess('Score Data Has Saved.');
                return redirect('backend/report-user/detail/'.$userId.'/'.$projectId)->withSuccess('Score Data Has Saved.');
            } catch (\Exception $e) {
                DB::rollback();
                // return redirect()->back()->withErrors('Score Data Not Saved, Something Wrong!');
                return redirect('backend/report-user/detail/'.$userId.'/'.$projectId)->withErrors('Score Data Not Saved, Something Wrong!');
            }
        } else {
            // return redirect()->back()->withErrors('Score Data Not Saved, Something Wrong! Data Not Found.');
            return redirect('backend/report-user/detail/'.$userId.'/'.$projectId)->withErrors('Score Data Not Saved, Something Wrong! Data Not Found.');
        }
    }

    public function generate_all_tpa($project_id)
    {
        //select user dalam project terpilih where is_done = 1 (telah mengerjakan semua test dalam project)
        $dataUsersProject = UserProject::join('profiles','profiles.user_id','=','user_project.id_user')
            ->where(['id_project'=>$project_id,'is_done'=>1])->select('id_user','id_project','first_name')
            ->whereNotIn('id_user',HasilPeserta::where('tujuan',$project_id)->pluck('user_id'))->get()->toArray();

        if (sizeof($dataUsersProject)==0) {
            return redirect()->back()->withErrors('Semua data sudah dikalkulasi.');
        } else {
            foreach ($dataUsersProject as $key => $data) {
                $dataKalkulasi["user_id"]    = $data['id_user'];
                $dataKalkulasi["project_id"] = $data['id_project'];
                $dataKalkulasi["fullName"]   = $data['first_name'];
                //proses kalkulasi
                $kalkulasi = Kalkulasi::prosesKalkulasi($dataKalkulasi);
                if ($kalkulasi=="true") {
                    continue;
                } else {
                    $fullName = $data['first_name'];
                    return redirect()->back()->withErrors('Something Wrong! TPA Peserta, '.$fullName." Tidak Berhasil Terkalkulasi.");
                }
            }
            //jika semua true
            return redirect()->back()->withSuccess('Semua data berhasil dikalkulasi.');
        }
    }
}
