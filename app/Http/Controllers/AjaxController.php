<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Entrust;
use App\Http\Requests;

use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\QuestionRepositoryInterface;
use App\Talenthub\Repository\OptionAnswerRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Survey;
use App\SurveyCategory;
use App\SurveyQuarter;
use App\Question;
use App\Result;
use App\Profile;
use App\Division;
use App\OptionSection;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
use Validator;
use Illuminate\Support\Facades\Cache;

class AjaxController extends Controller
{

	public function __construct(SurveyRepositoryInterface $survey, QuestionRepositoryInterface $question, OptionAnswerRepositoryInterface $option_answer, ResultRepositoryInterface $result)
    {
        $this -> survey         = $survey;
        $this -> question       = $question;
        $this -> option_answer  = $option_answer;
        $this -> result         = $result;
    }


    public function loadSoalWBA($slug, $section, $user_id){
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $info_section   = $this -> survey -> get_info_section($section);
        $id_survey      = $info_survey['id'];
        $expiresAt      = Carbon::now()->addMinutes(60);

        //inisialisasi jumlah terjawab ke cache
        if(Cache::has('result-count-'.$slug.'-'.$user_id)){
            $count = Cache::get('result-count-'.$slug.'-'.$user_id);
        }else{
            Cache::put('result-count-'.$slug.'-'.$user_id, 0, $expiresAt);
            $count = Cache::get('result-count-'.$slug.'-'.$user_id);
        }

        //ambil info section.
        $data['user_id']		= $user_id;
        $data['instruction']    = $info_section['instruction'];
        $data['section_name']   = $info_section['name'];

            $questions = $this -> question -> get_question_survey_papi($slug, $section);

        $i = 0;
        $j = 0;
        if($questions -> count() != 0){
            foreach ($questions as $key) {
                if($j == 2){ //batasan 1 nomor ada 2 statement
                    $j = 0;
                }

                $data['timer_survey']   = $key -> timer_survey;
                $data['random']         = $key -> random;
                $data['survey_title']   = $key -> survey_title;
                $data['id_survey']      = $key -> id_survey;
                $number[$i]             = $key -> number;
                $question[$number[$i]]['statement'][$j]  = $key -> statement;
                $question[$number[$i]]['key'][$j]        = $key -> key;
                $question[$number[$i]]['number'][$j]     = $key -> number;
                $i++;
                $j++;
            }
            if($data['random'] == 1){
                shuffle($question);
            }
            $i = 0;
            $j = 0;
            //proses menyimpan array nomor agar tidak double
            foreach ($question as $number) {
                if($j < 90){
                    foreach ($number['number'] as $number['number'][]) {
                        $numbers[$j] = $number['number'][$i];
                    }
                    $j++;
                }
                $i+2;
            }

            //setting cache
            Cache::put('cache-'.$slug.'-'.$user_id, 'yes', $expiresAt);
            Cache::put('timer-survey-'.$slug.'-'.$user_id, $data['timer_survey'], $expiresAt);
            Cache::put('survey-title-'.$slug.'-'.$user_id, $data['survey_title'], $expiresAt);
            Cache::put('id-survey-'.$slug.'-'.$user_id, $data['id_survey'], $expiresAt);

            Cache::put('number-'.$slug.'-'.$user_id, $numbers, $expiresAt);
            Cache::put('question-'.$slug.'-'.$user_id, $question, $expiresAt);

            Cache::put('result-slug-'.$slug.'-'.$user_id, $slug, $expiresAt);
            Cache::put('result-section-'.$slug.'-'.$user_id, $section, $expiresAt);
            Cache::put('result-section-name-'.$slug.'-'.$user_id, $data['section_name'], $expiresAt);

            return 'true';
        }else{
            return 'false';
        }
    }

    public function loadSoalWPA($slug, $section, $user_id){
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $info_section   = $this -> survey -> get_info_section($section);
        $id_survey      = $info_survey['id'];
        $expiresAt      = Carbon::now()->addMinutes(60);

        //inisialisasi jumlah terjawab ke cache
        if(Cache::has('result-count-'.$slug.'-'.$user_id)){
            $count = Cache::get('result-count-'.$slug.'-'.$user_id);
        }else{
            Cache::put('result-count-'.$slug.'-'.$user_id, 0, $expiresAt);
            $count = Cache::get('result-count-'.$slug.'-'.$user_id);
        }

        //ambil info section.
        $data['user_id']        = $user_id;
        $data['instruction']    = $info_section['instruction'];
        $data['section_name']   = $info_section['name'];

        $questions = $this -> question -> get_question_survey_wpa($slug, $section);

        $i = 0;
        $j = 0;
        if($questions -> count() != 0){
            foreach ($questions as $key) {
                if($j == 4){ //batasan 1 nomor ada 4 statement
                    $j = 0;
                }
                $data['timer_survey']   = $key -> timer_survey;
                $data['survey_title']   = $key -> survey_title;
                $data['id_survey']      = $key -> id_survey;
                $number[$i]            = $key -> number;
                $question[$number[$i]]['statement'][$j]  = $key -> statement;
                $question[$number[$i]]['most'][$j]       = $key -> most;
                $question[$number[$i]]['lest'][$j]       = $key -> lest;
                $question[$number[$i]]['number'][$j]     = $key -> number;
                $i++;
                $j++;
            }
            if($info_survey['is_random'] == 1){
                shuffle($question);
            }
            $i = 0;
            $j = 0;
            //proses menyimpan array nomor agar tidak double
            foreach ($question as $number) {
                if($j < 24){
                    foreach ($number['number'] as $number['number'][]) {
                        $numbers[$j] = $number['number'][$i];
                    }
                    $j++;
                }
                $i+4;
            }

            //setting cache
            Cache::put('cache-'.$slug.'-'.$user_id, 'yes', $expiresAt);
            Cache::put('timer-survey-'.$slug.'-'.$user_id, $data['timer_survey'], $expiresAt);
            Cache::put('survey-title-'.$slug.'-'.$user_id, $data['survey_title'], $expiresAt);
            Cache::put('id-survey-'.$slug.'-'.$user_id, $data['id_survey'], $expiresAt);

            Cache::put('number-'.$slug.'-'.$user_id, $numbers, $expiresAt);
            Cache::put('question-'.$slug.'-'.$user_id, $question, $expiresAt);

            Cache::put('result-slug-'.$slug.'-'.$user_id, $slug, $expiresAt);
            Cache::put('result-section-'.$slug.'-'.$user_id, $section, $expiresAt);
            Cache::put('result-section-name-'.$slug.'-'.$user_id, $data['section_name'], $expiresAt);

            return 'true';
        }else{
            return 'false';
        }
    }

    public function loadSoalTikiLai($slug, $section, $user_id){
        $info_survey    = $this -> survey -> get_info_survey_by_slug($slug);
        $info_section   = $this -> survey -> get_info_section($section);
        $id_survey      = $info_survey['id'];
        $expiresAt      = Carbon::now()->addMinutes(60);

        //inisialisasi jumlah terjawab ke cache
        if(Cache::has('result-count-'.$slug.'-'.$user_id)){
            $count = Cache::get('result-count-'.$slug.'-'.$user_id);
        }else{
            Cache::put('result-count-'.$slug.'-'.$user_id, 0, $expiresAt);
            $count = Cache::get('result-count-'.$slug.'-'.$user_id);
        }

        //ambil info section.
        $data['user_id']        = $user_id;
        $data['instruction']    = $info_section['instruction'];
        $data['section_name']   = $info_section['name'];
        $data['section_slug']   = $info_section['name'];
        $folder_image   = '';
        for($i = 0; $i < strlen($data['section_slug'])-11; $i++){
            $folder_image .= $data['section_slug'][$i];
        }

        $i = 0;
        $questions = $this -> question -> get_question_survey($slug, $section);
        if($questions -> count() != 0){
            foreach ($questions as $key) {
                $timer              = $key -> timer;
                $timer_survey       = $key -> timer_survey;
                $timer_section      = $key -> timer_section;
                $survey_title       = $key -> survey_title;
                $id_questions[$i]   = $key -> id_question;
                $question[$i]['question']           = $key -> question;
                $question[$i]['id_type_question']   = $key -> id_type_question;
                $question[$i]['is_mandatory']       = $key -> is_mandatory;
                $option[$i]         = $this -> option_answer -> get_by_question_unhide($key -> id_question, $key -> is_random);
                $i++;
            }

            //setting cache
            Cache::put('cache-'.$slug.'-'.$user_id, 'yes', $expiresAt);
            Cache::put('timer-'.$slug.'-'.$user_id, $timer, $expiresAt);
            Cache::put('timer-survey-'.$slug.'-'.$user_id, $timer_survey, $expiresAt);
            Cache::put('timer-section-'.$slug.'-'.$user_id, $timer_section, $expiresAt);
            Cache::put('survey-title-'.$slug.'-'.$user_id, $survey_title, $expiresAt);

            Cache::put('id-question-'.$slug.'-'.$user_id, $id_questions, $expiresAt);
            Cache::put('question-'.$slug.'-'.$user_id, $question, $expiresAt);
            Cache::put('option-answer-'.$slug.'-'.$user_id, $option, $expiresAt);

            Cache::put('result-slug-'.$slug.'-'.$section.'-'.$user_id, $slug, $expiresAt);
            Cache::put('result-section-'.$slug.'-'.$user_id, $section, $expiresAt);
            Cache::put('result-section-name-'.$slug.'-'.$user_id, $data['section_name'], $expiresAt);

            return 'true';
        }else{
            return 'false';
        }
    }

    public function getNilaiStandar($standar_nilai_id)
    {
        $getStandarNilai = \App\StandarNilai::with('tpaAspekPsikologis')->where('id',$standar_nilai_id)->first();
        if (is_null($getStandarNilai)) {
            return "null";
        } else {
            $nilai_aspek = $getStandarNilai->nilai_aspek;
            $json_decode_nilai = json_decode($nilai_aspek,true);
            $mandatory   = $getStandarNilai->mandatory;
            $json_decode_manda = json_decode($mandatory,true);
            $array['nilai'] = $json_decode_nilai;
            $array['mandatory'] = $json_decode_manda;            
            return $array;
        }
        
    }
}
