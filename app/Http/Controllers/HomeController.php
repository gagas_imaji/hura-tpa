<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
use Nexmo;
use Exception;
use App\Notifications\TwoFactorAuth;


use App\Talenthub\Repository\SurveyRepositoryInterface;
use App\Talenthub\Repository\CorporateRepositoryInterface;
use App\Talenthub\Repository\ResultRepositoryInterface;
use App\Survey;
use App\User;
use App\SurveyCategory;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use App\Library\report_survey;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SurveyRepositoryInterface $survey, CorporateRepositoryInterface $corporate, ResultRepositoryInterface $result)
    {
        $this -> survey = $survey;
        $this -> corporate = $corporate;
        $this -> result = $result;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $user_id = Auth::user() -> id;

        //check user's role
        $role           = DB::table('role_user')->where('user_id', '=', $user_id) -> first();
        $role_id        = $role -> role_id;
        $corporate_id   = Auth::user() -> profile -> id_corporate;

        if($role_id == 2){ //role user
            
            //ambil daftar project yang wajib diikuti dari project_user
            $data['project_wajib']          = $this -> survey -> get_project_wajib($user_id);
            $data['count_project_wajib']    = $data['project_wajib'] -> count();
            return view('user/project',compact('data'));

        }else{ //role admin

            //ambil daftar survey yang wajib diikuti dari user_survey
            $survey_wajib   = $this -> survey -> get_daftar_survey();
            $count          = $survey_wajib -> count();

            //ambil id section pertama dari setiap user_survey untuk memulai survey
            $i = 0;
            foreach ($survey_wajib as $key) {
                $section    = $this -> survey -> get_first_section_on_survey($key->id_survey);

                foreach ($section as $section) {
                    $section_id[$i] = $section['id'];
                }
                $i++;
            }

            //untuk kebutuhan dashboard
            $participant_done   = $this -> survey -> get_participant_done($corporate_id);
            $all_participant    = $this -> survey -> get_all_participant($corporate_id);

            if($all_participant != 0){
                $dashboard['participant'] = round(($participant_done / $all_participant)* 100, 1);
            }else{
                $dashboard['participant'] = 0;
            }
            
            $dashboard['total_project'] = DB::table('project') -> select(DB::raw('count(project.id) as count')) -> join('profiles', 'profiles.user_id', '=', 'project.user_id') -> where('profiles.id_corporate', '=', $corporate_id) -> first() -> count;

            $dashboard['credit']                 = $this -> corporate -> get_credit($corporate_id);
            $dashboard['peserta_diassign']       = $this -> corporate -> get_kuota_aktif($corporate_id); //kuota project aktif
            $dashboard['peserta_menyelesaikan']  = $this -> corporate -> get_kuota_terpakai($corporate_id); //kuota terpakai
            $dashboard['jatah'] = $dashboard['credit'] - $dashboard['peserta_diassign'] - $dashboard['peserta_menyelesaikan'];
            
            $projects = $this -> index_project();
            $dashboard['credit']             = $this -> corporate -> get_credit($corporate_id);

            //ambil daftar survey yang sedang berjalan
            $ongoing_survey = $this -> survey -> get_ongoing_survey($corporate_id);
            return view('home',compact('survey_wajib', 'count', 'section_id', 'user', 'ongoing_survey','projects', 'dashboard'));
        }
    }

    private function current_survey_status() {
        $list = DB::table("survey") -> get();
            foreach ($list as $row) {
                $persentase = status_schedule($row->id);
                if ($persentase == 100) {
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 1]);
                }elseif ($persentase > 0 && $persentase < 100) {
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 2]);
                }
                if ($row->end_date < date("Y-m-d H:i:s")) {
                    DB::table('survey') -> where('id', '=', $row->id) -> update(['status' => 1]);
                }
            }
    }

    public function tnc(){
        if(!config('config.enable_tnc'))
            return redirect('/');

        return view('tnc');
    }

    public function maintenance(){
        if(!config('config.maintenance_mode'))
            return redirect('/');

        return view('maintenance');
    }

    public function lock(){
        if(session('locked'))
            return view('auth.lock');
        else
            return redirect('/home');
    }

    public function unlock(Request $request){
        if(!Auth::check())
            return redirect('/');

        $password = $request->input('password');

        if(\Hash::check($password,Auth::user()->password)){
            session()->forget('locked');
            return redirect('/home');
        }

        return redirect('/lock')->trans('messages.failed');
    }

    public function activityLog(){
        $table_data['activity-log-table'] = array(
            'source' => 'activity-log',
            'title' => 'Activity Log List',
            'id' => 'activity_log_table',
            'disable-sorting' => 1,
            'data' => array(
                'S No',
                trans('messages.user'),
                trans('messages.activity'),
                'IP',
                trans('messages.date'),
                'User Agent',
                )
            );

        return view('activity_log.index',compact('table_data'));
    }

    public function activityLogList(Request $request){
        $activities = \App\Activity::orderBy('created_at','desc')->get();

        $rows = array();
        $i = 0;
        foreach($activities as $activity){
            $i++;

            $activity_detail = ($activity->activity == 'activity_added') ? trans('messages.new').' '.trans('messages.'.$activity->module).' '.trans('messages.'.$activity->activity) : trans('messages.'.$activity->module).' '.trans('messages.'.$activity->activity);
            $row = array(
                $i,
                $activity->User->full_name,
                $activity_detail,
                $activity->ip,
                showDateTime($activity->created_at),
                $activity->user_agent
                );

            $rows[] = $row;
        }

        $list['aaData'] = $rows;
        return json_encode($list);
    }

    public function result($id_survey) {
        // rata rata skor survey kinerja/performance
        $data['performance'] = $this -> result -> average_data($id_survey);

        $performance = array();
        foreach ($data['performance'] as $row) {
            $performance[] = round($row['score'], 1);
        }
        $maxperf            = count($data['performance']) * 5;

        $data['avg_perf']   = convert_percent(array_sum($performance), $maxperf);

        //rata rata skor survey keterlibatan/engagement.
        //id_survey ditambah 1 karna survey keterlibatan berada setelah survey kinerja
        $data['engagement'] = $this -> result -> average_data($id_survey+1);
        $engagement = array();
        foreach ($data['engagement'] as $row) {
            $engagement[] = round($row['score'], 1);
        }

        $maxeng             = count($data['engagement']) * 7;
        $data['avg_eng']    = convert_percent(array_sum($engagement), $maxeng);

        //y = performance
        $tp = $this -> result -> result_sub(1, $id_survey);
        $meanTP = round(mean($tp), 1);
        $data['meantp'] = $meanTP;
        $lowTp          = quartile_low($tp);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeksLowtp    = count($tp) - 1;
        $lowTp          = $tp[$indeksLowtp];
        $data['lowtp']  = $lowTp;
        $highTp         = quartile_high($tp);
        $indeksHigh     = count($highTp) - 1;
        $highTp         = $tp[$indeksHigh];
        $data['hightp'] = $highTp;
        //
        $cp             = $this -> result -> result_sub(2, $id_survey);
        $meanCp         = round(mean($cp), 1);
        $data['meancp'] = $meanCp;
        $lowCp          = quartile_low($cp);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowcp    = count($cp) - 1;
        $lowCp          = $cp[$indeskLowcp];
        $data['lowcp']  = $lowCp;
        $highCp         = quartile_high($cp);
        $indeksHighcp   = count($highCp) - 1;
        $highCp         = $cp[$indeksHighcp];
        $data['highcp'] = $highCp;
        //
        $ap             = $this -> result -> result_sub(3, $id_survey);
        $meanap         = round(mean($ap), 1);
        $data['meanap'] = $meanap;
        $lowap          = quartile_low($ap);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeksLowap    = count($ap) - 1;
        $lowap          = $ap[$indeksLowap];
        $data['lowap']  = $lowap;
        $highap         = quartile_high($ap);
        $indeksHighap   = count($highap) - 1;
        $highap         = $ap[$indeksHighap];
        $data['highap'] = $highap;
        //
        $cb             = $this -> result -> result_sub(4, $id_survey);
        $meanCb         = round(mean($cb), 1);
        $data['meancb'] = $meanCb;
        $lowCb          = quartile_low($cb);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowcb    = count($cb) - 1;
        $lowCb          = $cb[$indeskLowcb];
        $data['lowcb']  = $lowCb;
        $highCb         = quartile_high($cb);
        $indeksHighcb   = count($highCb) - 1;
        $highCb         = $cb[$indeksHighcb];
        $data['highcb'] = $highCb;

        $deviationPerformance   = $this -> result -> deviation($id_survey);
        $deviationEngage        = $this -> result -> deviation($id_survey+1);
        $data['performance_dev']= $deviationPerformance;
        $data['engage_dev']     = $deviationEngage;
        $tableperformance = "";

        //x = engagement. id_survey ditambah 1 karna survey keterlibatan berada setelah survey kinerja
        $vg             = $this -> result -> result_sub(5, $id_survey+1);
        $meanvg         = round(mean($vg), 1);
        $data['meanvg'] = $meanvg;
        $lowvg          = quartile_low($vg);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowvg    = count($lowvg) - 1;
        $lowvg          = $cb[$indeskLowvg];
        $data['lowvg']  = $lowvg;
        $highvg         = quartile_high($vg);
        $indeksHighvg   = count($highvg) - 1;
        $highvg         = $vg[$indeksHighvg];
        $data['highvg'] = $highvg;

        $dd             = $this -> result -> result_sub(6, $id_survey+1);
        $meandd         = round(mean($dd), 1);
        $data['meandd'] = $meandd;
        $lowdd          = quartile_low($dd);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowdd    = count($lowdd) - 1;
        $lowdd          = $dd[$indeskLowvg];
        $data['lowdd']  = $lowdd;
        $highdd         = quartile_high($dd);
        $indeksHighdd   = count($highdd) - 1;
        $highdd         = $dd[$indeksHighdd];
        $data['highdd'] = $highdd;

        $ab             = $this -> result -> result_sub(7, $id_survey+1);
        $meanab         = round(mean($ab), 1);
        $data['meanab'] = $meanab;
        $lowab          = quartile_low($ab);
        //karena inabeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowab    = count($lowab) - 1;
        $lowab          = $ab[$indeskLowvg];
        $data['lowab']  = $lowab;
        $highab         = quartile_high($ab);
        $indeksHighab   = count($highab) - 1;
        $highab         = $ab[$indeksHighab];
        $data['highab'] = $highab;

        return $data;
    }
    //============================================ TAMBAHAN VERSI 1,5 ============================================

    // public function index_project($id){
    //     //check status penyelesaian survey
    //     $this -> current_project_status();
    //     $this -> current_survey_status();

    //     $projects = DB::table('project') 
    //                 -> join('profiles', 'profiles.user_id', '=', 'project.user_id')
    //                 -> where('profiles.id_corporate', '=', $id) -> where('project.is_done', '=', 2) -> orderBy('project.created_at', 'desc') -> limit(5) -> get();
    //     return $projects;
    // }

    public function index_project(){
        //check status penyelesaian survey
        $this -> current_project_status();
        $this -> current_survey_status();

        $projects = DB::table('project') -> latest() -> where('user_id', '=', Auth::user() -> id) -> where('is_done', '=', 2) -> orderBy('created_at', 'desc') -> limit(5) -> get();
        return $projects;
    }

    private function current_project_status() {

        $list = DB::table("project") -> get();
        foreach ($list as $row) {
            $persentase = partisipasi_peserta_project($row->id);

            //status berdasarkan periode
            if (date("Y-m-d h:i:s") < $row->start_date) {
                DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 0]);
            }elseif(date("Y-m-d h:i:s") >= $row->start_date && date("Y-m-d h:i:s") <= $row->end_date){
                //status berdasarkan partisipasi
                if ($persentase == 100) {
                    DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 1]);
                }else{
                    DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 2]);
                }
            }elseif(date("Y-m-d h:i:s") > $row->end_date){
                DB::table('project') -> where('id', '=', $row->id) -> update(['is_done' => 1]);
            }
        }
    }


}
