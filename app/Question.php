<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'question';

    protected $fillable = [
    	'id_type_question',
    	'id_section',
    	'question',
    	'is_random',
    	'timer',
    	'is_mandatory'
    ];
    
    public function option_answer() 
    {
        return $this->hasMany('App\OptionAnswer', 'id_question');
    }
}

