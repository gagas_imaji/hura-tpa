<?php


namespace App\Library;
use Illuminate\Support\Facades\Config;
use Helper\simplestatistic_helper;
use App\Talenthub\Repository\ResultRepositoryInterface;


class report_survey {


    function __construct(ResultRepositoryInterface $result) {
        $this -> result = $result;
    }

    public static function result($corporate, $id_survey, $heat_survey) {
        

        // rata rata skor survey
        if($heat_survey == 'Kinerja'){
            $data['performance'] = $this -> result -> average_data($id_survey);
        }
        
        $data['performance'] = $this-> result -> average_data($id_survey);
        $performance = array();
        if (is_array($data['performance'])) {
            foreach ($data['performance'] as $row) {
                $performance[] = round($row['score'], 1);
            }
        }
        $maxperf = count($data['performance']) * 5;
        $data['avg_perf'] = convert_percent(array_sum($performance), $maxperf);
        //performance perusahaan lainya
        $data['another_performance'] = $this->CI->crud->average_company(1, $anotherfilter);
        
        $anotherperformance = array();
        if (is_array($data['another_performance'])) {
            foreach ($data['another_performance'] as $row) {
                $anotherperformance[] = round($row['score'], 1);
            }
        }
        $anothermaxperf = count($data['another_performance']) * 5;
        $data['anotheravg_perf'] = convert_percent(array_sum($anotherperformance), $anothermaxperf);

        //engagement
        $data['engagement'] = $this->CI->crud->average_data($corporate, 2, $filter);
        $engagement = array();
        if (is_array($data['engagement'])) {
            foreach ($data['engagement'] as $row) {
                $engagement[] = round($row['score'], 1);
            }
        }
        $maxeng = count($data['engagement']) * 7;
        $data['avg_eng'] = convert_percent(array_sum($engagement), $maxeng);
        //engagement perusahaan lainya
        $data['anotherengagement'] = $this->CI->crud->average_company( 2, $anotherfilter);
        $anotherengagement = array();
        if (is_array($data['anotherengagement'])) {
            foreach ($data['anotherengagement'] as $row) {
                $anotherengagement[] = round($row['score'], 1);
            }
        }
        $anothermaxeng = count($data['anotherengagement']) * 7;
        $data['anotheravg_eng'] = convert_percent(array_sum($anotherengagement), $anothermaxeng);


        //x = performance
        $tp = $this->CI->crud->result_sub(1, $corporate, $filter);
        $meanTP = round(mean($tp), 1);
        $data['meantp'] = $meanTP;
        $lowTp = quartile_low($tp);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLow = $lowTp - 1;
        $lowTp = $tp[$indeskLow];
        $data['lowtp'] = $lowTp;
        $highTp = quartile_high($tp);
        $indeksHigh = $highTp - 1;
        $highTp = $tp[$indeksHigh];
        $data['hightp'] = $highTp;
        //
        $cp = $this->CI->crud->result_sub(2, $corporate, $filter);
        $meanCp = round(mean($cp), 1);
        $data['meancp'] = $meanCp;
        $lowCp = quartile_low($cp);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowcp = $lowCp - 1;
        $lowCp = $cp[$indeskLowcp];
        $data['lowcp'] = $lowCp;
        $highCp = quartile_high($cp);
        $indeksHighcp = $highCp - 1;
        $highCp = $cp[$indeksHighcp];
        $data['highcp'] = $highCp;
        //
        $ap = $this->CI->crud->result_sub(3, $corporate, $filter);
        $meanap = round(mean($ap), 1);
        $data['meanap'] = $meanap;
        $lowap = quartile_low($ap);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeksLowap = $lowap - 1;
        $lowap = $ap[$indeksLowap];
        $data['lowap'] = $lowap;
        $highap = quartile_high($ap);
        $indeksHighap = $highap - 1;
        $highap = $ap[$indeksHighap];
        $data['highap'] = $highap;
        //
        $cb = $this->CI->crud->result_sub(4, $corporate, $filter);
        $meanCb = round(mean($cb), 1);
        $data['meancb'] = $meanCb;
        $lowCb = quartile_low($cb);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowcb = $lowCb - 1;
        $lowCb = $cb[$indeskLowcb];
        $data['lowcb'] = $lowCb;
        $highCb = quartile_high($cb);
        $indeksHighcb = $highCb - 1;
        $highCb = $cb[$indeksHighcb];
        $data['highcb'] = $highCb;

        $deviationPerformance = $this->CI->crud->deviation(1, $corporate, $filter);
        $deviationEngage = $this->CI->crud->deviation(2, $corporate, $filter);
        #$deviationCapacity = $this->CI->crud->deviation(3, $corporate);
        $data['performance_dev'] = $deviationPerformance;
        $data['engage_dev'] = $deviationEngage;
        #$data['capacity_dev'] = $deviationCapacity;
        $tableperformance = "";


        //y = engagement
        $vg = $this->CI->crud->result_sub(5, $corporate, $filter);
        $meanvg = round(mean($vg), 1);
        $data['meanvg'] = $meanvg;
        $lowvg = quartile_low($vg);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowvg = $lowvg - 1;
        $lowvg = $cb[$indeskLowvg];
        $data['lowvg'] = $lowvg;
        $highvg = quartile_high($vg);
        $indeksHighvg = $highvg - 1;
        $highvg = $vg[$indeksHighvg];
        $data['highvg'] = $highvg;

        $dd = $this->CI->crud->result_sub(6, $corporate, $filter);
        $meandd = round(mean($dd), 1);
        $data['meandd'] = $meandd;
        $lowdd = quartile_low($dd);
        //karena inddeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowdd = $lowdd - 1;
        $lowdd = $dd[$indeskLowvg];
        $data['lowdd'] = $lowdd;
        $highdd = quartile_high($dd);
        $indeksHighdd = $highdd - 1;
        $highdd = $dd[$indeksHighdd];
        $data['highdd'] = $highdd;
        $ab = $this->CI->crud->result_sub(7, $corporate, $filter);
        $meanab = round(mean($ab), 1);
        $data['meanab'] = $meanab;
        $lowab = quartile_low($ab);
        //karena inabeks array dimulai dari 0 maka index quartile kurangi 1
        $indeskLowab = $lowab - 1;
        $lowab = $ab[$indeskLowvg];
        $data['lowab'] = $lowab;
        $highab = quartile_high($ab);
        $indeksHighab = $highab - 1;
        $highab = $ab[$indeksHighab];
        $data['highab'] = $highab;


        // rata rata performance dan engagement
        $data['performance'] = $this->CI->crud->average_data($corporate, 1, $filter);
        $performance = array();
        if (is_array($data['performance'])) {
            foreach ($data['performance'] as $row) {
                $performance[] = round($row['score'], 1);
            }
        }
        $maxperf = count($data['performance']) * 5;

        $data['avg_perf'] = convert_percent(array_sum($performance), $maxperf);
        //performance perusahaan lainya
        $data['another_performance'] = $this->CI->crud->average_company( 1, $anotherfilter);
        $anotherperformance = array();
        if (is_array($data['another_performance'])) {
            foreach ($data['another_performance'] as $row) {
                $anotherperformance[] = round($row['score'], 1);
            }
        }
        $anothermaxperf = count($data['another_performance']) * 5;
        $data['anotheravg_perf'] = convert_percent(array_sum($anotherperformance), $anothermaxperf);

        //engagement
        $data['engagement'] = $this->CI->crud->average_data($corporate, 2, $filter);
        $engagement = array();
        if (is_array($data['engagement'])) {
            foreach ($data['engagement'] as $row) {
                $engagement[] = round($row['score'], 1);
            }
        }

        $maxeng = count($data['engagement']) * 7;
        $data['avg_eng'] = convert_percent(array_sum($engagement), $maxeng);
        //engagement perusahaan lainya
        $data['anotherengagement'] = $this->CI->crud->average_company( 2, $anotherfilter);
        $anotherengagement = array();
        if (is_array($data['anotherengagement'])) {
            foreach ($data['anotherengagement'] as $row) {
                $anotherengagement[] = round($row['score'], 1);
            }
        }
        $anothermaxeng = count($data['anotherengagement']) * 7;
        $data['anotheravg_eng'] = convert_percent(array_sum($anotherengagement), $anothermaxeng);

        return $data;
    }

}
