<?php
namespace App\Library;

use Illuminate\Http\Request;
use App\AspekPsikologis;
use DB;
use Illuminate\Support\Str;
use Entrust;
use Auth;
use App\UserReport;
use App\UserReportAspek;
use App\HasilPeserta;
use App\StandarNilaiProject;
use App\StandarNilai;
use App\Survey;
use App\ReportWPADetail;

class Kalkulasi {

	public static function prosesKalkulasi($data)
	{
	    DB::beginTransaction();
	    try {
	    	$fullName = $data["fullName"];
	        $data = UserReport::where('user_id',$data['user_id'])->where('tujuan',$data['project_id'])->first();
	        if (!is_null($data)) {
	        	$GTQ1 = $data['GTQ1'];
		        $GTQ2 = $data['GTQ2'];
		        $GTQ3 = $data['GTQ3'];
		        $GTQ4 = $data['GTQ4'];
		        $GTQ5 = $data['GTQ5'];

		        $SUBTEST1 = $data['SS1'];
		        $SUBTEST2 = $data['SS2'];
		        $SUBTEST3 = $data['SS3'];
		        $SUBTEST4 = $data['SS4'];

		        $GTI1 = DB::table('lookup_norma_gti')->where('min','<=',$GTQ1)->where('max','>=',$GTQ1)->value('nilai');
		        $GTI2 = DB::table('lookup_norma_gti')->where('min','<=',$GTQ2)->where('max','>=',$GTQ2)->value('nilai');
		        $GTI3 = DB::table('lookup_norma_gti')->where('min','<=',$GTQ3)->where('max','>=',$GTQ3)->value('nilai');
		        $GTI4 = DB::table('lookup_norma_gti')->where('min','<=',$GTQ4)->where('max','>=',$GTQ4)->value('nilai');
		        $GTI5 = DB::table('lookup_norma_gti')->where('min','<=',$GTQ5)->where('max','>=',$GTQ5)->value('nilai');

		        $TIKI1 = DB::table('lookup_norma_tiki')->where('RW',$SUBTEST1)->value('SW1');
		        $TIKI2 = DB::table('lookup_norma_tiki')->where('RW',$SUBTEST2)->value('2');
		        $TIKI3 = DB::table('lookup_norma_tiki')->where('RW',$SUBTEST3)->value('3');
		        $TIKI4 = DB::table('lookup_norma_tiki')->where('RW',$SUBTEST4)->value('4');

		        $converted_tiki1 = DB::table('lookup_subtes_tiki')->where('min','<=',$TIKI1)->where('max','>=',$TIKI1)->value('nilai');
		        $converted_tiki1 = intval($converted_tiki1);
		        $converted_tiki2 = DB::table('lookup_subtes_tiki')->where('min','<=',$TIKI2)->where('max','>=',$TIKI2)->value('nilai');
		        $converted_tiki2 = intval($converted_tiki2);
		        $converted_tiki3 = DB::table('lookup_subtes_tiki')->where('min','<=',$TIKI3)->where('max','>=',$TIKI3)->value('nilai');
		        $converted_tiki3 = intval($converted_tiki3);
		        $converted_tiki4 = DB::table('lookup_subtes_tiki')->where('min','<=',$TIKI4)->where('max','>=',$TIKI4)->value('nilai');
		        $converted_tiki4 = intval($converted_tiki4);
		        $arrayNilaiUser = [];
		        $disc_hasil = $data['hasil'];

		        //Kemampuan Numerikal========================================================================
		        $NUME_GTI  = $GTI4;
		        $NUME_TIKI = $converted_tiki1;
		        if (!is_null($NUME_GTI) && !is_null($NUME_TIKI)) {
		            $NUME = ($NUME_GTI+$NUME_TIKI)/2;
		        } elseif (!is_null($NUME_GTI) && is_null($NUME_TIKI)) {
		            $NUME = $NUME_GTI;
		        } elseif (is_null($NUME_GTI) && !is_null($NUME_TIKI)) {
		            $NUME = $NUME_TIKI;
		        }
		        $final_nume = intval(round($NUME));

		        //Kemampuan Verbal===========================================================================
		        $verbal_GTI  = $GTI2;
		        $verbal_TIKI = $converted_tiki3;
		        if (!is_null($verbal_GTI) && !is_null($verbal_TIKI)) {
		            $VERBAL = ($verbal_GTI+$verbal_TIKI)/2;
		        } elseif (!is_null($verbal_GTI) && is_null($verbal_TIKI)) {
		            $VERBAL = $verbal_GTI;
		        } elseif (is_null($verbal_GTI) && !is_null($verbal_TIKI)) {
		            $VERBAL = $verbal_TIKI;
		        }
		        $final_verbal = intval(round($VERBAL));

		        //Logika Berpikir============================================================================
		        $bobotGTI3 = DB::table('master_bobot_nilai')->where('slug','=','gti3')->value('nilai');
		        $bobotGTI2 = DB::table('master_bobot_nilai')->where('slug','=','gti2')->value('nilai');
		        $sumBobotGTI23 = $bobotGTI3+$bobotGTI2;
		        $logXbobotGTI3 = log10($GTI3) * $bobotGTI3;
		        $logXbobotGTI2 = log10($GTI2) * $bobotGTI2;
		        $sumLogXbobotLobeGTI23 = $logXbobotGTI3+$logXbobotGTI2;
		        $subNilaiLobeGTI = $sumLogXbobotLobeGTI23/$sumBobotGTI23;
		        $LOBE_GTI = pow(10, $subNilaiLobeGTI);

		        $bobotTIKI1 = DB::table('master_bobot_nilai')->where('slug','=','tiki1')->value('nilai');
		        $bobotTIKI3 = DB::table('master_bobot_nilai')->where('slug','=','tiki3')->value('nilai');
		        $sumBobotTIKI13 = $bobotTIKI1+$bobotTIKI3;
		        $logXbobotTIKI1 = log10($converted_tiki1) * $bobotTIKI1;
		        $logXbobotTIKI3 = log10($converted_tiki3) * $bobotTIKI3;
		        $sumLogXbobotLobeTIKI13 = $logXbobotTIKI1+$logXbobotTIKI3;
		        $subNilaiLobeTIKI = $sumLogXbobotLobeTIKI13/$sumBobotTIKI13;
		        $LOBE_TIKI = pow(10, $subNilaiLobeTIKI);
		        $final_lobe = intval(round(($LOBE_GTI+$LOBE_TIKI)/2));

		        //Daya Analisa===============================================================================
		        $bobotGTI1 = DB::table('master_bobot_nilai')->where('slug','=','gti1')->value('nilai');
		        $bobotGTI5 = DB::table('master_bobot_nilai')->where('slug','=','gti5')->value('nilai');
		        $sumBobotGTI15 = $bobotGTI1+$bobotGTI5;
		        $logXbobotGTI1 = log10($GTI1) * $bobotGTI1;
		        $logXbobotGTI5 = log10($GTI5) * $bobotGTI5;
		        $sumLogXbobotLobeGTI15 = $logXbobotGTI1+$logXbobotGTI5;
		        $subNilaiDAGTI = $sumLogXbobotLobeGTI15/$sumBobotGTI15;
		        $DA_GTI = pow(10, $subNilaiDAGTI);

		        $bobotTIKI4 = DB::table('master_bobot_nilai')->where('slug','=','tiki4')->value('nilai');
		        $bobotTIKI2 = DB::table('master_bobot_nilai')->where('slug','=','tiki2')->value('nilai');
		        $sumBobotTIKI42 = $bobotTIKI4+$bobotTIKI2;
		        $logXbobotTIKI4 = log10($converted_tiki4) * $bobotTIKI4;
		        $logXbobotTIKI2 = log10($converted_tiki2) * $bobotTIKI2;
		        $sumLogXbobotDATIKI42 = $logXbobotTIKI4+$logXbobotTIKI2;
		        $subNilaiDATIKI = $sumLogXbobotDATIKI42/$sumBobotTIKI42;
		        $DA_TIKI = pow(10, $subNilaiDATIKI);
		        $final_DA = intval(round(($DA_GTI+$DA_TIKI)/2));
		        
		        //Orientasi Hasil============================================================================
		        $nilai_N 	= intval($data['N']);
		        $nilai_G 	= intval($data['G']);
		        $nilai_A 	= intval($data['A']);
		        $konversi_N = self::konversiPapi('N',$nilai_N);
		        $konversi_G = self::konversiPapi('G',$nilai_G);
		        $konversi_A = self::konversiPapi('A',$nilai_A);
		        $nilai_NGA  = $konversi_N.$konversi_G.$konversi_A;
		        $NGA_OH1    = DB::table('lookup_matriks_tiga_faktor')->where('nilai',$nilai_NGA)->value('hasil');    
		        $nilai_C    = intval($data['C']);
		        $nilai_D    = intval($data['D']);
		        $konversi_C = self::konversiPapi('C',$nilai_C);
		        $konversi_D = self::konversiPapi('D',$nilai_D);
		        $CD_OH2     = self::matriksDuaFaktor($konversi_C,$konversi_D);
		        $final_OH   = self::matriksDuaFaktor($NGA_OH1,$CD_OH2);

		        //Fleksibilitas
		        $nilai_Z 	= intval($data['Z']);
		        $nilai_W 	= intval($data['W']);
		        $konversi_Z = self::konversiPapi('Z',$nilai_Z);
		        $konversi_W = self::konversiPapi('W',$nilai_W);
		        $ZW_FL 		= self::matriksDuaFaktor($konversi_Z,$konversi_W);

		        if (is_null($disc_hasil) || $disc_hasil == "-") {
		            $final_FL = $ZW_FL;
		        } else {
		            $konversi_disc_fl = DB::table('lookup_disc_fleksibilitas')->where('hasil_disc',$disc_hasil)->value('nilai');
		            $final_FL = self::matriksDuaFaktor($ZW_FL,$konversi_disc_fl);
		        }

		        //Sistematika Kerja
		        $CD_SK = $CD_OH2;
		        if (is_null($disc_hasil) || $disc_hasil == "-") {
		            $final_SK = $CD_SK;
		        } else {
		            $konversi_disc_sk = DB::table('lookup_disc_sistematika_kerja')->where('hasil_disc',$disc_hasil)->value('nilai');
		            $final_SK = self::matriksDuaFaktor($CD_SK,$konversi_disc_sk);
		        }

		        //Motivasi Berprestasi
		        $GA_MB = self::matriksDuaFaktor($konversi_G,$konversi_A);
		        if (is_null($disc_hasil) || $disc_hasil == "-") {
		        	$final_MB = $GA_MB;
		        } else {
		        	$konversi_disc_mb = DB::table('lookup_disc_achievment')->where('hasil_disc',$disc_hasil)->value('nilai');
		            $final_MB = self::matriksDuaFaktor($konversi_disc_mb,$GA_MB);
		        }

		        //Kerjasama
		        $nilai_B 	  = intval($data['B']);
		        $nilai_O 	  = intval($data['O']);
		        $konversi_B   = self::konversiPapi('B',$nilai_B);
		        $konversi_O   = self::konversiPapi('O',$nilai_O);
		        $BO_KERJASAMA = self::matriksDuaFaktor($konversi_B,$konversi_O);

		        if (is_null($disc_hasil) || $disc_hasil == "-") {
		        	$final_KERJASAMA = $BO_KERJASAMA;
		        } else {
		        	$konversi_disc_kerjasama = DB::table('lookup_disc_kerjasama')->where('hasil_disc',$disc_hasil)->value('nilai');
		            $final_KERJASAMA = self::matriksDuaFaktor($BO_KERJASAMA,$konversi_disc_kerjasama);
		        }

		        //Interpersonal
		        $nilai_X 		  = intval($data['X']);
		        $nilai_S 		  = intval($data['S']);
		        $konversi_X 	  = self::konversiPapi('X',$nilai_X);
		        $konversi_S 	  = self::konversiPapi('S',$nilai_S);
		        $XS_INTERPERSONAL = self::matriksDuaFaktor($konversi_X,$konversi_S);

		        if (is_null($disc_hasil) || $disc_hasil == "-") {
		        	$final_INTERPERSONAL = $XS_INTERPERSONAL;
		        } else {
		        	$konversi_disc_interpersonal = DB::table('lookup_disc_interpersonal')->where('hasil_disc',$disc_hasil)->value('nilai');
		            $final_INTERPERSONAL = self::matriksDuaFaktor($konversi_disc_interpersonal,$XS_INTERPERSONAL);
		        }

		        //Kecerdasan Umum
		        //Get nilai C3 GTI terlebih dahulu
		        $bobotC1GTI = DB::table('master_bobot_nilai')->where('slug','=','c1_gti')->value('nilai');
		        $bobotC2GTI = DB::table('master_bobot_nilai')->where('slug','=','c2_gti')->value('nilai');
		        $sumBobotC12GTI = $bobotC1GTI+$bobotC2GTI;
		        $logXbobotC1GTI = log10($LOBE_GTI) * $bobotC1GTI;
		        $logXbobotC2GTI = log10($DA_GTI) * $bobotC2GTI;
		        $sumLogXbobotC12GTI = $logXbobotC1GTI+$logXbobotC2GTI;
		        $subNilaiC3GTI = $sumLogXbobotC12GTI/$sumBobotC12GTI;
		        $C3GTI = pow(10, $subNilaiC3GTI);

		        //Get nilai C4 GTI (Kecerdasan Umum GTI)
		        $bobotNumerikalGTI = DB::table('master_bobot_nilai')->where('slug','=','numerikal_gti')->value('nilai');
		        $bobotC3GTI = DB::table('master_bobot_nilai')->where('slug','=','c3_gti')->value('nilai');
		        $sumBobotC3GTINumerikalGTI = $bobotNumerikalGTI+$bobotC3GTI;
		        $logXbobotC3GTI = log10($C3GTI) * $bobotC3GTI;
		        $logXbobotNumerikalGTI = log10($final_nume) * $bobotNumerikalGTI;
		        $sumLogXbobotC3NumerikalGTI = $logXbobotC3GTI+$logXbobotNumerikalGTI;
		        $subNilaiC4GTI = $sumLogXbobotC3NumerikalGTI/$sumBobotC3GTINumerikalGTI;
		        $C4GTI = pow(10, $subNilaiC4GTI);
		        $kecerdasanUmumGTI  = intval($C4GTI);

		        //Get nilai C3 TIKI (Kecerdasan Umum TIKI)
		        $bobotC1TIKI = DB::table('master_bobot_nilai')->where('slug','=','c1_tiki')->value('nilai');
		        $bobotC2TIKI = DB::table('master_bobot_nilai')->where('slug','=','c2_tiki')->value('nilai');
		        $sumBobotC12TIKI = $bobotC1TIKI+$bobotC2TIKI;
		        $logXbobotC1TIKI = log10($DA_TIKI) * $bobotC1TIKI;
		        $logXbobotC2TIKI = log10($LOBE_TIKI) * $bobotC2TIKI;
		        $sumLogXbobotC12TIKI = $logXbobotC1TIKI+$logXbobotC2TIKI;
		        $subNilaiC3TIKI = $sumLogXbobotC12TIKI/$sumBobotC12TIKI;
		        $C3TIKI = pow(10, $subNilaiC3TIKI);
		        $kecerdasanUmumTIKI = intval($C3TIKI);
		        $final_kecerdasan = intval(round(($kecerdasanUmumGTI+$kecerdasanUmumGTI)/2));
		        $uraian_kecerdasan_umum  = DB::table('lookup_uraian_kecerdasan_umum')->where('nilai',$final_kecerdasan)->value('desc1');

		        //kategori kemampuan intelektual
		        $arrayKemampuanIntelektual['logika_berpikir-1-'.$final_lobe] = $final_lobe."1";
		        $arrayKemampuanIntelektual['daya_analisa-2-'.$final_DA] = $final_DA."2";
		        $arrayKemampuanIntelektual['kemampuan_numerikal-2-'.$final_nume] = $final_nume."3";
		        $arrayKemampuanIntelektual['kemampuan_verbal-2-'.$final_verbal] = $final_verbal."4";
		        arsort($arrayKemampuanIntelektual);

		        //Get kalimat dari tiap aspek kemampuan intelektual
		        $paragraf1 = "";
		        foreach ($arrayKemampuanIntelektual as $key => $value) {
		        	$dataAspek = explode("-", $key);
		        	$namaAspek = $dataAspek[0];
		        	$jumlahUraian = $dataAspek[1];
		        	$nilaiAspek = $dataAspek[2];
		        	$desc = 'desc'.$jumlahUraian;
		        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
		        	$paragraf1 .= $interpretasiAspek." ";
		        }

		        //kategori sikap dan cara kerja
		        $arraySikapCaraKerja['orientasi_hasil-1-'.$final_OH] = $final_OH."1";
		        $arraySikapCaraKerja['fleksibilitas-2-'.$final_FL] = $final_FL."2";
		        $arraySikapCaraKerja['sistematika_kerja-2-'.$final_SK] = $final_SK."3";
		        arsort($arraySikapCaraKerja);

		        //Get kalimat dari tiap aspek kategori sikap dan cera kerja
		        $paragraf2 = "";
		        foreach ($arraySikapCaraKerja as $key => $value) {
		        	$dataAspek = explode("-", $key);
		        	$namaAspek = $dataAspek[0];
		        	$jumlahUraian = $dataAspek[1];
		        	$nilaiAspek = $dataAspek[2];
		        	$desc = 'desc'.$jumlahUraian;
		        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
		        	$paragraf2 .= $interpretasiAspek." ";
		        }

		        //kategori kepribadian
		        $arrayKepribadian['achievement-1-'.$final_MB] = $final_MB."1";
		        $arrayKepribadian['kerjasama-1-'.$final_KERJASAMA] = $final_KERJASAMA."2";
		        $arrayKepribadian['interpersonal-1-'.$final_INTERPERSONAL] = $final_INTERPERSONAL."3";
		        arsort($arrayKepribadian);

		        if (!is_null($disc_hasil) || $disc_hasil != "-") {
		        	$random_desc1 = 'deskripsi_motivasi_berprestasi_'.rand(1,2);
			        $inter_tambah_berprestasi = DB::table('interpretasi_disc')->where('hasil_disc',$disc_hasil)->value($random_desc1);

			        $random_desc2 = 'deskripsi_kerjasama_'.rand(1,2);
		        	$inter_tambah_kerjasama = DB::table('interpretasi_disc')->where('hasil_disc',$disc_hasil)->value($random_desc2);
		        } else {
		        	$inter_tambah_berprestasi = "";
		        	$inter_tambah_kerjasama	  = "";
		        }

		        //Get kalimat dari tiap aspek kepribadian
		        $paragraf3 = "";
		        foreach ($arrayKepribadian as $key => $value) {
		        	$dataAspek = explode("-", $key);
		        	$namaAspek = $dataAspek[0];
		        	$jumlahUraian = $dataAspek[1];
		        	$nilaiAspek = $dataAspek[2];
		        	if ($namaAspek == 'achievement') {
		        		$desc = 'desc'.$jumlahUraian;
			        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
			        	$paragraf3 .= $inter_tambah_berprestasi." ";
			        	$paragraf3 .= $interpretasiAspek." ";
			        	continue;
		        	}
		        	if ($namaAspek == 'kerjasama') {
		        		$desc = 'desc'.$jumlahUraian;
			        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
			        	$paragraf3 .= $interpretasiAspek." ";
			        	$paragraf3 .= $inter_tambah_kerjasama." ";
			        	continue;
		        	} 
		        	if ($namaAspek == 'interpersonal') {
		        		$desc = 'desc'.$jumlahUraian;
			        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
			        	$paragraf3 .= $interpretasiAspek." ";
			        	continue;
		        	}
		        }

		        $dynamic = "";
            	$dynamic .= "<p>";
            	$dynamic .= "Sdr. ".$fullName." ";
            	$dynamic .= $uraian_kecerdasan_umum." ";
            	$dynamic .= $paragraf1;
            	$dynamic .= "</p>";
                $dynamic .= "<p>";
            	$dynamic .= "Sdr. ".$fullName." ";
            	$dynamic .= $paragraf2;
            	$dynamic .= $paragraf3;
            	$dynamic .= "</p>";

		        $project_id = $data['tujuan'];
		        $userId     = $data['user_id'];

		        $checkUserReport = UserReportAspek::where(['user_id'=>$userId,'project_id'=>$project_id])->delete();

		        $listAspeks = AspekPsikologis::get();

		        $i=1;
	            foreach ($listAspeks as $key => $value) {
	                $aspek_id[$i] = $value->id;
	                $i++;
	            }

		        $getStandarNilai   = StandarNilaiProject::with('tpaStandarNilai')->where('project_id',$project_id)->first();
		        if (!is_null($getStandarNilai)) {
		        	$jabatan_id = $getStandarNilai->standar_nilai_id;	
		        } else {
		        	$jabatan_id = NULL;
		        }

		        $arrayNilaiUser['1']  = $final_lobe;	        
		        $arrayNilaiUser['2']  = $final_nume;
		        $arrayNilaiUser['3']  = $final_DA;
		        $arrayNilaiUser['4']  = $final_verbal;
		        $arrayNilaiUser['5']  = $final_OH;
		        $arrayNilaiUser['6']  = $final_FL;
		        $arrayNilaiUser['7']  = $final_SK;
		        $arrayNilaiUser['8']  = $final_MB;
		        $arrayNilaiUser['9']  = $final_KERJASAMA;
		        $arrayNilaiUser['10'] = $final_INTERPERSONAL;

		        $json_nilai_aspek = json_encode($arrayNilaiUser);
		        $json_aspek_id    = json_encode($aspek_id);
		        //save user report aspeks nilai
		        $usrReport = new UserReportAspek;
		        $usrReport->user_id     = $userId;
	            $usrReport->project_id  = $project_id;
	            $usrReport->jabatan_id  = $jabatan_id;
	            $usrReport->aspek_id    = $json_aspek_id;
	            $usrReport->nilai_aspek = $json_nilai_aspek;
	            $usrReport->save();

		        if (!is_null($getStandarNilai)) {
		        	$nilaiAspekStandar = $getStandarNilai->tpaStandarNilai->nilai_aspek;
			        $json_decode_nilai = json_decode($nilaiAspekStandar,true);
			        $gaps = [];
			        $skor_gaps = [];

			        //Hitung gaps antara nilai peserta dengan standar nilai project yang dituju
		            foreach ($json_decode_nilai as $keyStd => $standarNilai) {
		                foreach ($arrayNilaiUser as $keyPst => $nilaiPeserta) {
		                    if (($keyStd) == $keyPst) {
		                        if ($standarNilai == 0) {
		                            array_push($gaps, "");
		                        } else {
		                            if (($standarNilai-($nilaiPeserta-1))<0) {
		                                array_push($gaps, 0);
		                            } else {
		                                $gapNilai = $standarNilai-($nilaiPeserta-1);
		                                array_push($gaps, $gapNilai);
		                            }
		                        }
		                    }
		                }
		            }

		            //Hitung gap skor
		            $j = 0;
		            foreach ($json_decode_nilai as $keyStandar => $standar) {
		                foreach ($gaps as $keyGaps => $value) {
		                    if (($keyStandar-1) == $keyGaps) {
		                        if (strlen($value) == 0) {
		                            array_push($skor_gaps, "");
		                            $j++;
		                        } elseif ($value == 0) {
		                            array_push($skor_gaps, 1);
		                        } else {
		                            $skor = 1-($value/$standar);
		                            array_push($skor_gaps, $skor);
		                        }
		                    }
		                }
		            }

		            $counts = count($gaps)-$j;
		            //Check jika count gaps = 0 (standar nilai 0 semua)
		            if ($counts != 0) {
		                $sumSkorGaps = array_sum($skor_gaps)*100;
		                $totalNilaiJPM = round($sumSkorGaps/$counts,2);

		                if (($totalNilaiJPM) >= 0.00 && ($totalNilaiJPM) <= 70.00) {
		                    $hasilKriteria = 1;
		                } elseif (($totalNilaiJPM) >= 70.10 && ($totalNilaiJPM) <= 90.00) {
		                    $hasilKriteria = 2;
		                } elseif (($totalNilaiJPM) >= 90.10 && ($totalNilaiJPM) <= 100.00) {
		                    $hasilKriteria = 3;
		                }

		                //Create or Update Hasil peserta
	                    $hasilPeserta = HasilPeserta::updateOrCreate(
	                        ['user_id'=>$userId,'tujuan'=>$project_id],
	                        ['user_id'=>$userId,'tujuan'=>$project_id,'kecocokan'=>$totalNilaiJPM,'kriteria_psikograph_id'=>$hasilKriteria,'uraians'=>$dynamic]
	                    );
		                
		            } else {
		                $hasilPeserta = HasilPeserta::updateOrCreate(
	                        ['user_id'=>$userId,'tujuan'=>$project_id],
	                        ['user_id'=>$userId,'tujuan'=>$project_id,'kecocokan'=>0,'kriteria_psikograph_id'=>1,'uraians'=>$dynamic]
	                    );
		            }
		        } else {
		        	$hasilPeserta = HasilPeserta::updateOrCreate(
                        ['user_id'=>$userId,'tujuan'=>$project_id],
                        ['user_id'=>$userId,'tujuan'=>$project_id,'kecocokan'=>0,'kriteria_psikograph_id'=>1,'uraians'=>$dynamic]
                    );
		        }
	        }
	        DB::commit();
	        $status = "true";
	        return $status;
	    } catch (\Exception $e) {
	        DB::rollBack();
	        $status = "false";
	        return $status;
	    }
	}

	public static function konversiPapi($kode_papi, $nilai)
	{
	    $konversi = DB::table('lookup_konversi_nilai_papi')->where('kode_papi',$kode_papi)->value($nilai);
	    return $konversi;
	}

	public static function matriksDuaFaktor($xy,$value)
	{
	    $matriksDuaFaktor = DB::table('lookup_matriks_dua_faktor')->where('xy',$xy)->value($value);
	    return $matriksDuaFaktor;
	}

	public static function updateNilaiKecocokanByStandarNilaiUpdated($project_id)
	{
		$getHasilPesertaByProject = HasilPeserta::select("id", "user_id", "tujuan", "kecocokan", "kriteria_psikograph_id", "uraians")->where(["tujuan"=> $project_id])->get();
		$getStandarNilai   = StandarNilaiProject::with('tpaStandarNilai')->where('project_id',$project_id)->first();
        $nilaiAspekStandar = $getStandarNilai->tpaStandarNilai->nilai_aspek;
        $json_decode_nilai = json_decode($nilaiAspekStandar,true);
		DB::beginTransaction();
		try {
			foreach ($getHasilPesertaByProject as $key => $hasilPeserta) {
				$projectId       = $hasilPeserta['tujuan'];
                $userId          = $hasilPeserta['user_id'];
                // $getNilaiPeserta = UserReportAspek::where('user_id','=',$userId)->where('project_id','=',$projectId)->pluck("nilai_aspek","aspek_id")->toArray();
                $getNilaiPeserta  = UserReportAspek::where('user_id','=',$userId)->where('project_id','=',$projectId)->first();
        		$json_nilai_aspek = json_decode($getNilaiPeserta->nilai_aspek,true);
                $gaps      = [];
                $skor_gaps = [];
                //Hitung gaps antara nilai peserta dengan standar nilai project yang dituju
	            foreach ($json_decode_nilai as $keyStd => $standarNilai) {
	                foreach ($json_nilai_aspek as $keyPst => $nilaiPeserta) {
	                    if (($keyStd) == $keyPst) {
	                        if ($standarNilai == 0) {
	                            array_push($gaps, "");
	                        } else {
	                            if (($standarNilai-($nilaiPeserta-1))<0) {
	                                array_push($gaps, 0);
	                            } else {
	                                $gapNilai = $standarNilai-($nilaiPeserta-1);
	                                array_push($gaps, $gapNilai);
	                            }
	                        }
	                    }
	                }
	            }

	            //Hitung gap skor
	            $j = 0;
	            foreach ($json_decode_nilai as $keyStandar => $standar) {
	                foreach ($gaps as $keyGaps => $value) {
	                    if (($keyStandar-1) == $keyGaps) {
	                        if (strlen($value) == 0) {
	                            array_push($skor_gaps, "");
	                            $j++;
	                        } elseif ($value == 0) {
	                            array_push($skor_gaps, 1);
	                        } else {
	                            $skor = 1-($value/$standar);
	                            array_push($skor_gaps, $skor);
	                        }
	                    }
	                }
	            }

	            $counts = count($gaps)-$j;
	            //Check jika count gaps = 0 (standar nilai 0 semua)
	            if ($counts != 0) {
	                $sumSkorGaps = array_sum($skor_gaps)*100;
	                $totalNilaiJPM = round($sumSkorGaps/$counts,2);

	                if (($totalNilaiJPM) >= 0.00 && ($totalNilaiJPM) <= 70.00) {
	                    $hasilKriteria = 1;
	                } elseif (($totalNilaiJPM) >= 70.10 && ($totalNilaiJPM) <= 90.00) {
	                    $hasilKriteria = 2;
	                } elseif (($totalNilaiJPM) >= 90.10 && ($totalNilaiJPM) <= 100.00) {
	                    $hasilKriteria = 3;
	                }

	                $hasilPeserta = HasilPeserta::updateOrCreate(
                        ['user_id'=>$userId,'tujuan'=>$project_id],
                        ['user_id'=>$userId,'tujuan'=>$project_id,'kecocokan'=>$totalNilaiJPM,'kriteria_psikograph_id'=>$hasilKriteria]
                    );
	            } else {
	                // continue;
	                $hasilPeserta = HasilPeserta::updateOrCreate(
                        ['user_id'=>$userId,'tujuan'=>$project_id],
                        ['user_id'=>$userId,'tujuan'=>$project_id,'kecocokan'=>0,'kriteria_psikograph_id'=>1]
                    );
	            }
			}
			DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
	}

	public static function hitungKecocokan(array $datas)
	{
		$posisiId  = $datas['posisi'];
        $refUserId = $datas['refUserId'];
        $projectId = $datas['projectId'];

        //get nilai peserta
        $getNilaiPeserta   = UserReportAspek::where('user_id','=',$refUserId)->where('project_id','=',$projectId)->first();
        $json_nilai_aspek  = json_decode($getNilaiPeserta->nilai_aspek,true);
        $getStandarNilai   = StandarNilai::where('id',$posisiId)->first();
        if (is_null($getStandarNilai))
         {
        	$totalNilaiJPM = 0;
	        $hasilKriteria = 1;
        	$result['totalNilaiJPM'] = $totalNilaiJPM;
	        $result['hasilKriteria'] = $hasilKriteria;
	        return $result;
        } else {
        	$nilaiAspekStandar = $getStandarNilai->nilai_aspek;
	        $json_decode_nilai = json_decode($nilaiAspekStandar,true);

	        $gaps      = [];
	        $skor_gaps = [];
	        $result    = [];
	        //Hitung gaps antara nilai peserta dengan standar nilai project yang dituju
	        foreach ($json_decode_nilai as $keyStd => $standarNilai) {
	            foreach ($json_nilai_aspek as $keyPst => $nilaiPeserta) {
	                if (($keyStd) == $keyPst) {
	                    if ($standarNilai == 0) {
	                        array_push($gaps, "");
	                    } else {
	                        if (($standarNilai-($nilaiPeserta-1))<0) {
	                            array_push($gaps, 0);
	                        } else {
	                            $gapNilai = $standarNilai-($nilaiPeserta-1);
	                            array_push($gaps, $gapNilai);
	                        }
	                    }
	                }
	            }
	        }

	        //Hitung gap skor
	        $j = 0;
	        foreach ($json_decode_nilai as $keyStandar => $standar) {
	            foreach ($gaps as $keyGaps => $value) {
	                if (($keyStandar-1) == $keyGaps) {
	                    if (strlen($value) == 0) {
	                        array_push($skor_gaps, "");
	                        $j++;
	                    } elseif ($value == 0) {
	                        array_push($skor_gaps, 1);
	                    } else {
	                        $skor = 1-($value/$standar);
	                        array_push($skor_gaps, $skor);
	                    }
	                }
	            }
	        }

	        $counts = count($gaps)-$j;
	        //Check jika count gaps = 0 (standar nilai 0 semua)
	        if ($counts != 0) {
	            $sumSkorGaps = array_sum($skor_gaps)*100;
	            $totalNilaiJPM = round($sumSkorGaps/$counts,2);

	            if (($totalNilaiJPM) >= 0.00 && ($totalNilaiJPM) <= 70.00) {
	                $hasilKriteria = 1;
	            } elseif (($totalNilaiJPM) >= 70.10 && ($totalNilaiJPM) <= 90.00) {
	                $hasilKriteria = 2;
	            } elseif (($totalNilaiJPM) >= 90.10 && ($totalNilaiJPM) <= 100.00) {
	                $hasilKriteria = 3;
	            }
	        } else {
	            $totalNilaiJPM = null;
	            $hasilKriteria = null;
	        }
	        $result['totalNilaiJPM'] = $totalNilaiJPM;
	        $result['hasilKriteria'] = $hasilKriteria;
	        return $result;
        }
        
	}

	public static function updateNilaiKecocokanAfterUpdateStandarNilaiPsikograph($standar_nilai_id)
	{
		$getProjectId = StandarNilaiProject::where('standar_nilai_id',$standar_nilai_id)->pluck('project_id');
		$getHasilPesertaByProject = HasilPeserta::whereIn('tujuan',$getProjectId)->get();
		if (sizeof($getHasilPesertaByProject)>0) {
			$getStandarNilaiBaru = StandarNilai::where('id',$standar_nilai_id)->first();
			$nilaiAspekStandar   = $getStandarNilaiBaru->nilai_aspek;
        	$json_decode_nilai   = json_decode($nilaiAspekStandar,true);
        	DB::beginTransaction();
        	try {
        		foreach ($getHasilPesertaByProject as $key => $hasilPeserta) {
        			$projectId = $hasilPeserta['tujuan'];
		            $userId    = $hasilPeserta['user_id'];
		            $getNilaiPeserta  = UserReportAspek::where('user_id','=',$userId)->where('project_id','=',$projectId)->first();
        			$json_nilai_aspek = json_decode($getNilaiPeserta->nilai_aspek,true);
		            $gaps      = [];
		            $skor_gaps = [];
		            //Hitung gaps antara nilai peserta dengan standar nilai project yang dituju
		            foreach ($json_decode_nilai as $keyStd => $standarNilai) {
		                foreach ($json_nilai_aspek as $keyPst => $nilaiPeserta) {
		                    if (($keyStd) == $keyPst) {
		                        if ($standarNilai == 0) {
		                            array_push($gaps, "");
		                        } else {
		                            if (($standarNilai-($nilaiPeserta-1))<0) {
		                                array_push($gaps, 0);
		                            } else {
		                                $gapNilai = $standarNilai-($nilaiPeserta-1);
		                                array_push($gaps, $gapNilai);
		                            }
		                        }
		                    }
		                }
		            }
		            //Hitung gap skor
		            $j = 0;
		            foreach ($json_decode_nilai as $keyStandar => $standar) {
		                foreach ($gaps as $keyGaps => $value) {
		                    if (($keyStandar-1) == $keyGaps) {
		                        if (strlen($value) == 0) {
		                            array_push($skor_gaps, "");
		                            $j++;
		                        } elseif ($value == 0) {
		                            array_push($skor_gaps, 1);
		                        } else {
		                            $skor = 1-($value/$standar);
		                            array_push($skor_gaps, $skor);
		                        }
		                    }
		                }
		            }

		            $counts = count($gaps)-$j;
		            //Check jika count gaps = 0 (standar nilai 0 semua)
		            if ($counts != 0) {
		                $sumSkorGaps = array_sum($skor_gaps)*100;
		                $totalNilaiJPM = round($sumSkorGaps/$counts,2);

		                if (($totalNilaiJPM) >= 0.00 && ($totalNilaiJPM) <= 70.00) {
		                    $hasilKriteria = 1;
		                } elseif (($totalNilaiJPM) >= 70.10 && ($totalNilaiJPM) <= 90.00) {
		                    $hasilKriteria = 2;
		                } elseif (($totalNilaiJPM) >= 90.10 && ($totalNilaiJPM) <= 100.00) {
		                    $hasilKriteria = 3;
		                }

		                $hasilPeserta = HasilPeserta::updateOrCreate(
		                    ['user_id'=>$userId,'tujuan'=>$projectId],
		                    ['user_id'=>$userId,'tujuan'=>$projectId,'kecocokan'=>$totalNilaiJPM,'kriteria_psikograph_id'=>$hasilKriteria]
		                );
		            } else {
		                // continue;
		                $hasilPeserta = HasilPeserta::updateOrCreate(
	                        ['user_id'=>$userId,'tujuan'=>$project_id],
	                        ['user_id'=>$userId,'tujuan'=>$project_id,'kecocokan'=>0,'kriteria_psikograph_id'=>1]
	                    );
		            }
        		}
        		DB::commit();
            	return true;
        	} catch (\Exception $e) {
        		DB::rollback();
            	return false;
        	}
		}

	}

	public static function updateUraianByScoreEdited($datas,$userId,$projectId)
	{
		$final_lobe = $datas[1];
        $final_nume = $datas[2];
        $final_DA = $datas[3];
        $final_verbal = $datas[4];
        $final_OH = $datas[5];
        $final_FL = $datas[6];
        $final_SK = $datas[7];
        $final_MB = $datas[8];
        $final_KERJASAMA = $datas[9];
        $final_INTERPERSONAL = $datas[10];
		//Kecerdasan Umum
        $final_kecerdasan = intval(round(($final_lobe+$final_nume+$final_DA+$final_verbal)/4));
        $uraian_kecerdasan_umum  = DB::table('lookup_uraian_kecerdasan_umum')->where('nilai',$final_kecerdasan)->value('desc1');

        //kategori kemampuan intelektual
        $arrayKemampuanIntelektual['logika_berpikir-1-'.$final_lobe] = $final_lobe."1";
        $arrayKemampuanIntelektual['daya_analisa-2-'.$final_DA] = $final_DA."2";
        $arrayKemampuanIntelektual['kemampuan_numerikal-2-'.$final_nume] = $final_nume."3";
        $arrayKemampuanIntelektual['kemampuan_verbal-2-'.$final_verbal] = $final_verbal."4";
        arsort($arrayKemampuanIntelektual);

        //Get kalimat dari tiap aspek kemampuan intelektual
        $paragraf1 = "";
        foreach ($arrayKemampuanIntelektual as $key => $value) {
        	$dataAspek = explode("-", $key);
        	$namaAspek = $dataAspek[0];
        	$jumlahUraian = $dataAspek[1];
        	$nilaiAspek = $dataAspek[2];
        	$desc = 'desc'.$jumlahUraian;
        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
        	$paragraf1 .= $interpretasiAspek." ";
        }

        //kategori sikap dan cara kerja
        $arraySikapCaraKerja['orientasi_hasil-1-'.$final_OH] = $final_OH."1";
        $arraySikapCaraKerja['fleksibilitas-2-'.$final_FL] = $final_FL."2";
        $arraySikapCaraKerja['sistematika_kerja-2-'.$final_SK] = $final_SK."3";
        arsort($arraySikapCaraKerja);

        //Get kalimat dari tiap aspek kategori sikap dan cera kerja
        $paragraf2 = "";
        foreach ($arraySikapCaraKerja as $key => $value) {
        	$dataAspek = explode("-", $key);
        	$namaAspek = $dataAspek[0];
        	$jumlahUraian = $dataAspek[1];
        	$nilaiAspek = $dataAspek[2];
        	$desc = 'desc'.$jumlahUraian;
        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
        	$paragraf2 .= $interpretasiAspek." ";
        }

        //kategori kepribadian
        $arrayKepribadian['achievement-1-'.$final_MB] = $final_MB."1";
        $arrayKepribadian['kerjasama-1-'.$final_KERJASAMA] = $final_KERJASAMA."2";
        $arrayKepribadian['interpersonal-1-'.$final_INTERPERSONAL] = $final_INTERPERSONAL."3";
        arsort($arrayKepribadian);

        $disc_hasil = DB::table('user_report')->where(['user_id'=>$userId,'tujuan'=>$projectId])->value('hasil');

        if (!is_null($disc_hasil) || $disc_hasil != "-") {
        	$random_desc1 = 'deskripsi_motivasi_berprestasi_'.rand(1,2);
	        $inter_tambah_berprestasi = DB::table('interpretasi_disc')->where('hasil_disc',$disc_hasil)->value($random_desc1);

	        $random_desc2 = 'deskripsi_kerjasama_'.rand(1,2);
        	$inter_tambah_kerjasama = DB::table('interpretasi_disc')->where('hasil_disc',$disc_hasil)->value($random_desc2);
        } else {
        	$inter_tambah_berprestasi = "";
        	$inter_tambah_kerjasama	  = "";
        }

        //Get kalimat dari tiap aspek kepribadian
        $paragraf3 = "";
        foreach ($arrayKepribadian as $key => $value) {
        	$dataAspek = explode("-", $key);
        	$namaAspek = $dataAspek[0];
        	$jumlahUraian = $dataAspek[1];
        	$nilaiAspek = $dataAspek[2];
        	if ($namaAspek == 'achievement') {
        		$desc = 'desc'.$jumlahUraian;
	        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
	        	$paragraf3 .= $inter_tambah_berprestasi." ";
	        	$paragraf3 .= $interpretasiAspek." ";
	        	continue;
        	}
        	if ($namaAspek == 'kerjasama') {
        		$desc = 'desc'.$jumlahUraian;
	        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
	        	$paragraf3 .= $interpretasiAspek." ";
	        	$paragraf3 .= $inter_tambah_kerjasama." ";
	        	continue;
        	} 
        	if ($namaAspek == 'interpersonal') {
        		$desc = 'desc'.$jumlahUraian;
	        	$interpretasiAspek = DB::table('lookup_uraian_'.$namaAspek)->where('nilai',$nilaiAspek)->value($desc);
	        	$paragraf3 .= $interpretasiAspek." ";
	        	continue;
        	}
        }

        $checkName = DB::table('profiles')->where('user_id',$userId)->first();
        if (!is_null($checkName)) {
        	$fullName = $checkName->first_name;
        } else {
        	$fullName = "Anonim";
        }

        $dynamic = "";
    	$dynamic .= "<p>";
    	$dynamic .= "Sdr. ".$fullName." ";
    	$dynamic .= $uraian_kecerdasan_umum." ";
    	$dynamic .= $paragraf1;
    	$dynamic .= "</p>";
        $dynamic .= "<p>";
    	$dynamic .= "Sdr. ".$fullName." ";
    	$dynamic .= $paragraf2;
    	$dynamic .= $paragraf3;
    	$dynamic .= "</p>";

    	return $dynamic;
	}
}