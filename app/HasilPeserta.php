<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilPeserta extends Model
{
    protected $table = "hasil_pesertas";
    protected $guarded = ['id'];

    public $fillable = ['user_id','tujuan','kecocokan','kriteria_psikograph_id','uraians'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function kriteria_psikograph()
    {
    	return $this->belongsTo('App\KriteriaPsikograph', 'kriteria_psikograph_id');
    }
}
