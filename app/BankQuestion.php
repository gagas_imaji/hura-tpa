<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankQuestion extends Model
{
    protected $table = 'bank_question';

    protected $fillable = [
    	'id_type_question',
    	'id_bank_section',
    	'question',
    	'is_random',
    	'timer',
    	'is_mandatory'
    ];
    
   
}
