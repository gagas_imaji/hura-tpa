<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyGrade extends Model
{
    protected $table = 'survey_grade';
}
