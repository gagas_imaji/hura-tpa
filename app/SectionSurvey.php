<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionSurvey extends Model
{
    protected $fillable = ['id', 'survey_id', 'name', 'slug'];
	protected $table = 'section_survey';  
}
