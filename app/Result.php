<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'result';

    public function option_answer() 
    {
        return $this->belongsTo('App\OptionAnswer');
    }

    public function user() 
    {
        return $this->belongsTo('App\User');
    }
}
